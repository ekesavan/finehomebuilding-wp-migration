<?php 
set_time_limit( 5 * 60 * 60 );
ini_set('memory_limit','2056M');
require_once( dirname(__FILE__) . "/lib/Config.php");
require_once( dirname(__FILE__) . "/lib/FHBMongo.php");

if( ! defined('__FHB__MIGRATION__') )
	define( '__FHB__MIGRATION__', true );

// WP Load
require_once( dirname(__FILE__) . "/../finehomebuilding-wordpress/web/wp/wp-load.php");
require_once( dirname(__FILE__) . "/../finehomebuilding-wordpress/web/wp/wp-admin/includes/admin.php");
require_once( dirname(__FILE__) . "/../finehomebuilding-wordpress/web/app/themes/finehomebuilding/functions.php");

require_once( dirname(__FILE__) . "/vendor/simple_html_dom.php");

$file_name = "FHB_Flat_Pages.csv";

$csv_file = dirname(__FILE__) ."/redirect/".$file_name;

$fh = fopen( $csv_file, 'r' );
$contents = fread( $fh, filesize($csv_file) );
fclose($fh);

foreach( explode( "\n", $contents ) as $row ){
	
	list ( $source, $target ) = preg_split( "/\|/", $row );
	
	$source = preg_replace( "|http:\/\/|i", '', $source );
	$source = preg_replace( "|www\.finehomebuilding\.com|i", '', $source );
	
	$target = preg_replace( "|http:\/\/|i", '', $target );
	$target = preg_replace( "|beta\.finehomebuilding\.com|i", '', $target );
	
	print "Redirect: $source ----> $target \n";
	add_redirect( $source, $target );
}


function add_redirect( $source, $target ){
		
		
		$item = Red_Item::get_for_url( $source, 'url' );
					
		if( empty( $item  ) ){
		
			$data = array(
					'source' => $source,
					'match' => 'url',
					'red_action' => 'url',
					'target' => $target,
					'group_id' => 1,
					'add' => 'Add Redirection',
					'group' => 0,
					'action' => 'red_redirect_add',
					'_wpnonce' => '541664d5f2',
					'_wp_http_referer' => '/wp-admin/tools.php?page=redirection.php',
			);
			print "Add redirect $source --> $target\n";
			$item = Red_Item::create( $data );
			
		}
		//print_R( $item );
	}
?>