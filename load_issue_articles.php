<?php 
$mongo = new MongoClient();
$db = $mongo->Ektron;

 $collections =  array(
		'article',
		'audio',
		'author',
		'book_excerpt',
		'contests',
		'department_article',
		'qa',
		'readers_tips',
		'review',
		'slide_show',
		'special_collection',
		'tool_guide',
		'video',
);


foreach( $collections as $c ){
	$cursor = $db->selectCollection( $c )->find( array(), array('Id' => 1, "META_DATA.GetContentMetadataListResult.CustomAttribute" => -1 ) );
	foreach( $cursor as $c ){
		if( isset( $c['META_DATA']['GetContentMetadataListResult']['CustomAttribute'] ) ){
			$meta_tags = $c['META_DATA']['GetContentMetadataListResult']['CustomAttribute'];
			foreach ( $meta_tags as $m ){
				if( $m['Name'] == "Title/Issue" ){
					if( !isset( $issues[$m['Value']] ) ){
						$issues[$m['Value']] = array();
					}
					$issues[$m['Value']][] = $c['Id'];
				}
			}
		}

	}
}

print_R( $issues );



?>