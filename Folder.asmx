

<html>

    <head><link rel="alternate" type="text/xml" href="/Workarea/webservices/Folder.asmx?disco" />

    <style type="text/css">
    
		BODY { color: #000000; background-color: white; font-family: Verdana; margin-left: 0px; margin-top: 0px; }
		#content { margin-left: 30px; font-size: .70em; padding-bottom: 2em; }
		A:link { color: #336699; font-weight: bold; text-decoration: underline; }
		A:visited { color: #6699cc; font-weight: bold; text-decoration: underline; }
		A:active { color: #336699; font-weight: bold; text-decoration: underline; }
		A:hover { color: cc3300; font-weight: bold; text-decoration: underline; }
		P { color: #000000; margin-top: 0px; margin-bottom: 12px; font-family: Verdana; }
		pre { background-color: #e5e5cc; padding: 5px; font-family: Courier New; font-size: x-small; margin-top: -5px; border: 1px #f0f0e0 solid; }
		td { color: #000000; font-family: Verdana; font-size: .7em; }
		h2 { font-size: 1.5em; font-weight: bold; margin-top: 25px; margin-bottom: 10px; border-top: 1px solid #003366; margin-left: -15px; color: #003366; }
		h3 { font-size: 1.1em; color: #000000; margin-left: -15px; margin-top: 10px; margin-bottom: 10px; }
		ul { margin-top: 10px; margin-left: 20px; }
		ol { margin-top: 10px; margin-left: 20px; }
		li { margin-top: 10px; color: #000000; }
		font.value { color: darkblue; font: bold; }
		font.key { color: darkgreen; font: bold; }
		font.error { color: darkred; font: bold; }
		.heading1 { color: #ffffff; font-family: Tahoma; font-size: 26px; font-weight: normal; background-color: #003366; margin-top: 0px; margin-bottom: 0px; margin-left: -30px; padding-top: 10px; padding-bottom: 3px; padding-left: 15px; width: 105%; }
		.button { background-color: #dcdcdc; font-family: Verdana; font-size: 1em; border-top: #cccccc 1px solid; border-bottom: #666666 1px solid; border-left: #cccccc 1px solid; border-right: #666666 1px solid; }
		.frmheader { color: #000000; background: #dcdcdc; font-family: Verdana; font-size: .7em; font-weight: normal; border-bottom: 1px solid #dcdcdc; padding-top: 2px; padding-bottom: 2px; }
		.frmtext { font-family: Verdana; font-size: .7em; margin-top: 8px; margin-bottom: 0px; margin-left: 32px; }
		.frmInput { font-family: Verdana; font-size: 1em; }
		.intro { margin-left: -15px; }
           
    </style>

    <title>
	Folder Web Service
</title></head>

  <body>

    <div id="content">

      <p class="heading1">Folder</p><br>

      

      <span>

          <p class="intro">The following operations are supported.  For a formal definition, please review the <a href="Folder.asmx?WSDL">Service Description</a>. </p>
          
          
              <ul>
            
              <li>
                <a href="Folder.asmx?op=AddFolder">AddFolder</a>
                
                <span>
                  <br>Adds a folder
                </span>
              </li>
              <p>
            
              <li>
                <a href="Folder.asmx?op=CreateFolder">CreateFolder</a>
                
                <span>
                  <br>Creates a folder with the given path
                </span>
              </li>
              <p>
            
              <li>
                <a href="Folder.asmx?op=DeleteFolder">DeleteFolder</a>
                
                <span>
                  <br>Deletes folder specified by folder id
                </span>
              </li>
              <p>
            
              <li>
                <a href="Folder.asmx?op=GetChildFolders">GetChildFolders</a>
                
                <span>
                  <br>Get all the subfolders for a given folder ID
                </span>
              </li>
              <p>
            
              <li>
                <a href="Folder.asmx?op=GetChildFolders_Obsolete">GetChildFolders</a>
                <span>
                  <br>MessageName="GetChildFolders_Obsolete"
                </span>
                <span>
                  <br>This method is obsolete. Use new GetChildFolders method with FolderOrderBy enum parameter
                </span>
              </li>
              <p>
            
              <li>
                <a href="Folder.asmx?op=GetFolder">GetFolder</a>
                
                <span>
                  <br>Retrieves the folder's details
                </span>
              </li>
              <p>
            
              <li>
                <a href="Folder.asmx?op=GetFolderByParam">GetFolder</a>
                <span>
                  <br>MessageName="GetFolderByParam"
                </span>
                <span>
                  <br>Retrieves the folder's details
                </span>
              </li>
              <p>
            
              <li>
                <a href="Folder.asmx?op=GetFolderID">GetFolderID</a>
                
                <span>
                  <br>Finds the folder ID by using the content's ID
                </span>
              </li>
              <p>
            
              <li>
                <a href="Folder.asmx?op=GetFolderIDByName">GetFolderID</a>
                <span>
                  <br>MessageName="GetFolderIDByName"
                </span>
                <span>
                  <br>Finds the folder ID by folder name and in a parent folder specified.
                </span>
              </li>
              <p>
            
              <li>
                <a href="Folder.asmx?op=GetPath">GetPath</a>
                
                <span>
                  <br>Gets path to folder by folder ID
                </span>
              </li>
              <p>
            
              <li>
                <a href="Folder.asmx?op=RenameFolder">RenameFolder</a>
                
                <span>
                  <br>Renames the folder by ID.
                </span>
              </li>
              <p>
            
              <li>
                <a href="Folder.asmx?op=UpdateFolder">UpdateFolder</a>
                
                <span>
                  <br>Updates a folder
                </span>
              </li>
              <p>
            
              <li>
                <a href="Folder.asmx?op=UpdateFolder_x005B_FolderData_x005D_">UpdateFolder</a>
                <span>
                  <br>MessageName="UpdateFolder_x005B_FolderData_x005D_"
                </span>
                <span>
                  <br>Updates a folder
                </span>
              </li>
              <p>
            
              </ul>
            
      </span>

      
      

    <span>
        <span>
            <hr>
            <h3><font class="error">This web service does not conform to WS-I Basic Profile v1.1.</font></h3>
            <p class="intro">Please examine each of the normative statement violations below. Follow the recommendations to remedy it, or add setting to the &lt;webServices&gt; config section to turn off BP 1.1 conformance warnings for the entire vroot.</p>
            <p class="intro">To turn off BP 1.1 conformance warnings for the entire vroot remove the 'BP1.1' value from the &lt;conformanceWarnings&gt; section of the configuration file of your application:</p>
            <pre>&lt;configuration&gt;
  &lt;system.web&gt;
    &lt;webServices&gt;
      &lt;conformanceWarnings&gt;
        &lt;<font class=value>remove name='BasicProfile1_1'</font>/&gt;
      &lt;/conformanceWarnings&gt;
    &lt;/webServices&gt;
  &lt;/system.web&gt;
&lt;/configuration&gt;</pre>

        <h3></h3>
        
        
                <br><font class=error>R2304</font>: Operation name overloading in a wsdl:portType is disallowed by the Profile. A wsdl:portType in a DESCRIPTION MUST have operations with distinct values for their name attributes. Note that this requirement applies only to the wsdl:operations within a given wsdl:portType. A wsdl:portType may have wsdl:operations with names that are the same as those found in other wsdl:portTypes.
                
                
                        <br>  
                        -  Operation 'GetChildFolders' on portType 'FolderSoap' from namespace 'http://tempuri.org/'.
                    
                        <br>  
                        -  Operation 'GetFolderID' on portType 'FolderSoap' from namespace 'http://tempuri.org/'.
                    
                        <br>  
                        -  Operation 'GetFolder' on portType 'FolderSoap' from namespace 'http://tempuri.org/'.
                    
                        <br>  
                        -  Operation 'UpdateFolder' on portType 'FolderSoap' from namespace 'http://tempuri.org/'.
                    

                <span>
                    <br><font class=key>Recommendation:</font> To make service conformant please make sure that all web methods belonging to the same binding have unique names.
                </span>
                <br>                
            
        <br>
            <p class="intro">For more details on Basic Profile Version 1.1, see the <a href="http://www.ws-i.org/Profiles/BasicProfile-1.1.html"> Basic Profile Specification</a>.</p>
        </span>
    </span>
    
      <span>
          <hr>
          <h3>This web service is using http://tempuri.org/ as its default namespace.</h3>
          <h3>Recommendation: Change the default namespace before the XML Web service is made public.</h3>
          <p class="intro">Each XML Web service needs a unique namespace in order for client applications to distinguish it from other services on the Web. http://tempuri.org/ is available for XML Web services that are under development, but published XML Web services should use a more permanent namespace.</p>
          <p class="intro">Your XML Web service should be identified by a namespace that you control. For example, you can use your company's Internet domain name as part of the namespace. Although many XML Web service namespaces look like URLs, they need not point to actual resources on the Web. (XML Web service namespaces are URIs.)</p>
          <p class="intro">For XML Web services creating using ASP.NET, the default namespace can be changed using the WebService attribute's Namespace property. The WebService attribute is an attribute applied to the class that contains the XML Web service methods. Below is a code example that sets the namespace to "http://microsoft.com/webservices/":</p>
          <p class="intro">C#</p>
          <pre>[WebService(Namespace="http://microsoft.com/webservices/")]
public class MyWebService {
    // implementation
}</pre>
          <p class="intro">Visual Basic</p>
          <pre>&lt;WebService(Namespace:="http://microsoft.com/webservices/")&gt; Public Class MyWebService
    ' implementation
End Class</pre>

          <p class="intro">C++</p>
          <pre>[WebService(Namespace="http://microsoft.com/webservices/")]
public ref class MyWebService {
    // implementation
};</pre>
          <p class="intro">For more details on XML namespaces, see the W3C recommendation on <a href="http://www.w3.org/TR/REC-xml-names/">Namespaces in XML</A>.</p>
          <p class="intro">For more details on WSDL, see the <a href="http://www.w3.org/TR/wsdl">WSDL Specification</a>.</p>
          <p class="intro">For more details on URIs, see <a href="http://www.ietf.org/rfc/rfc2396.txt">RFC 2396</a>.</p>
      </span>

      

    
  </body>
</html>
