<?php
set_time_limit( 5 * 60 * 60 );

ini_set('memory_limit','2056M');

require_once( dirname(__FILE__) . "/lib/Config.php");

require_once( dirname(__FILE__) . "/lib/FHBMongo.php");

require_once( dirname(__FILE__) . "/lib/CodeIgniter/CodeIgniterImport.php");

Print "Start Import \n";
print "System : ".WP_ENV. "\n";
print "MYSQL : ".__CI__MYSQL__DBHOST__. "\n";
print "MONGO : ".__MONGO__DBHOST__. "\n";

$t = new CodeIgniterImport();

$t->load();

/*
 
$t->create_users_mapping();

$t->create_posts_mapping();

*/

