<?php if( $Content_Section->find( 'paragraph_heading', 0 )->innertext ):?>
<h3><?php echo $Content_Section->find( 'paragraph_heading', 0 )->innertext; ?></h3>
<?php endif;?>
<?php echo $Content_Section->find( 'Paragraph', 0 )->innertext; ?>
<?php foreach( $Content_Section->find('Image') as $image ):?>
<?php if(  $image->find("img",0 ) ):?>
        <?php 
        $image_caption = $image->find('image_caption', 0) ? $image->find('image_caption', 0)->innertext : '';
		
		$image_credit =  $image->find('image_credit', 0) ? $image->find('image_credit', 0)->innertext : '';
		
		$image_src = $image->find("img",0 )->src;
		
		$attachment_id = $this->attachment->add_attachment(  $image_src , 0 );
		$section_image = wp_get_attachment_url( $attachment_id );
		//$section_image = __S3_URL__.CONTENT_DIR."/uploads". $this->attachment->lowercase_path( $image_src );
		$l_attachment_id = $this->attachment->add_attachment(  $this->attachment->enlarge_image_path( $image_src ) , 0 );
		$section_large_image = wp_get_attachment_url( $l_attachment_id );
		//$section_large_image = __S3_URL__.CONTENT_DIR."/uploads". $this->attachment->lowercase_path($this->attachment->enlarge_image_path( $image_src ));
		
		$click_action = $image->find('Click_Action', 0)->innertext;
			
		$href = $image->find("img",0 )->src;
			
		$slides[] = array(
				'image' => $image_src,
				'description' => implode( ' ', array($image_caption, $image_credit) )
		);
?>
<?php endif;?>
<?php endforeach;?>