<p>
<h2>Q:</h3><?php echo $dom->find( 'Question', 0 )->find( 'Paragraph_Text', 0 )->innertext; ?>
</p>
<?php 
$Contributor = '';
if( $dom->find( 'Contributor',0 ) ){
 	 $Contributor = $this->Contributor( $dom->find( 'Contributor',0 ) );
}
?>
<?php if( !empty( $Contributor )):?>
<p><em><?=$Contributor;?></em></p>
<?php endif;?>

<?php 			
	$Answer = $dom->find( 'Answer',0 ) ;

	foreach( $Answer->find( 'Type') as $Type ){
			
		$Type->outertext = '';
			
	}
	
	foreach( $Answer->find( 'Layout') as $Layout ){
			
		$Layout->outertext = '';
			
	}
		
	foreach( $Answer->find( 'Style') as $Style ){
			
		$Style->outertext = '';
			
	}
?>		
<?php $i=1; foreach( $Answer->find( 'Paragraph_Section') as $Paragraph_Section ):?>
	<?php if( $i == 1 ):?>
	<h3>A:</h3>
	<?php endif;?>
		<?php foreach( $Paragraph_Section->find('Paragraph') as $p ):?>
			<?php if( $p->find('Paragraph',0) ) continue; ?>
			<?php if( $p->find('p',0) ):?>
				<?=$p->innertext;?>
			<?php else:?>
				<p><?=$p->innertext;?></p>
			<?php endif;?>
		<?php endforeach;?>		
		<?php foreach( $Paragraph_Section->find('Image') as $image ):?>
		<?php if(  $image->find("img",0 ) && (  $image->find("img",0 )->src != $main_image_url )  ):?>
        <?php 
     
        
        $image_caption = $image->find('image_caption', 0) ? $image->find('image_caption', 0)->innertext : '';
		
		$image_credit =  $image->find('image_credit', 0) ? $image->find('image_credit', 0)->innertext : '';
		
		$image_src = $image->find("img",0 )->src;
		
		$original_image_src = $image->find("img",0 )->src;
		/*
		 * check for ld image
		 */
		$lg = $this->attachment->check_remote_image_exists($image_src, 'lg' );
		if( $lg ){
			$image_src = $lg;
		}
		
		
		$attachment_id = $this->attachment->add_attachment(  $image_src , 0 );
		$section_image = wp_get_attachment_url( $attachment_id );
		//$section_image = __S3_URL__.CONTENT_DIR."/uploads". $this->attachment->lowercase_path( $image_src );
		$l_attachment_id = $this->attachment->add_attachment(  $this->attachment->enlarge_image_path( $image_src ) , 0 );
		$section_large_image = wp_get_attachment_url( $l_attachment_id );
		//$section_large_image = __S3_URL__.CONTENT_DIR."/uploads". $this->attachment->lowercase_path($this->attachment->enlarge_image_path( $image_src ));
		
		$click_action = $image->find('Click_Action', 0) ? $image->find('Click_Action', 0)->innertext : 0;
			
		$href = $image->find("img",0 )->src;
			
		if( $click_action == 'Enlarge' ){
			$inline_images[] = array(
					'image_src' => $this->attachment->enlarge_image_path( $original_image_src ),
					'image_caption' => implode( ' ', array($image_caption, $image_credit) )
			);
		}
		
		$inline_images[] = array(
				'image_src' => $image_src,
				'image_caption' => implode( ' ', array($image_caption, $image_credit) )
		);
		
		?>
[caption id="attachment" align="aligncenter"]
<?php if( $click_action == 'Enlarge' ):?>
<a href="<?php echo $section_large_image?>" rel="lightbox"><img src="<?php echo $section_image;?>" alt=""></a>
<?php else:?>
<img src="<?php echo $section_image;?>" alt="">
<?php endif;?>
<?php echo $image_caption;?>
<?php echo $image_credit;?>
[/caption]
        <?php endif;?>
        <?php endforeach;?>
        <?php ++$i;?>
<?php endforeach;?>		