<?php if( $Content_Section->find( 'paragraph_heading', 0 )->innertext ):?>
<h3><?php echo $Content_Section->find( 'paragraph_heading', 0 )->innertext; ?></h3>
<?php endif;?>
<?php foreach( $Content_Section->find('Image') as $image ):?>

	<?php if(  $image->find("img",0 ) && !empty( $image->find("img",0 )->src ) ):?>
        <?php 
        $image_caption = $image->find('image_caption', 0) ? $image->find('image_caption', 0)->innertext : '';
		
		$image_credit =  $image->find('image_credit', 0) ? $image->find('image_credit', 0)->innertext : '';
		
		$image_src = $image->find("img",0 )->src;
		$original_image_src = $image->find("img",0 )->src;
		/*
		 * check for ld image
		 */
		$ld = $this->attachment->check_remote_image_exists($image_src, 'ld' );
		if( $ld ){
			$image_src = $ld;
		}
		
		$attachment_id = $this->attachment->add_attachment(  $image_src , 0 );
		$section_image = wp_get_attachment_url( $attachment_id );
		//$section_image = __S3_URL__.CONTENT_DIR."/uploads". $this->attachment->lowercase_path( $image_src );
		
		$l_attachment_id = $this->attachment->add_attachment(  $this->attachment->enlarge_image_path( $original_image_src ) , 0 );
		$section_large_image = wp_get_attachment_url( $l_attachment_id );
		//$section_large_image = __S3_URL__.CONTENT_DIR."/uploads". $this->attachment->lowercase_path($this->attachment->enlarge_image_path( $image_src ));
		
		$click_action = $image->find('Click_Action', 0)->innertext;
			
		$href = $image->find("img",0 )->src;
			
		if( $click_action == 'Enlarge' ){
			$inline_images[] = array(
					'image_src' => $this->attachment->enlarge_image_path( $original_image_src ),
					'image_caption' => implode( ' ', array($image_caption, $image_credit) ),
					'layout' => $layout,
			);
		}
		
		$inline_images[] = array(
				'image_src' => $image_src,
				'image_caption' => implode( ' ', array($image_caption, $image_credit) ),
				'layout' => $layout,
		);
		
?>
<?php $caption = $image_caption.$image_credit;?>
<?php if( $click_action == 'Enlarge' ):?>
[caption id="" align="alignright" width="300px"]<a href="<?php echo $section_large_image?>"><img src="<?php echo $section_image;?>" alt="" /></a><?=$caption;?>[/caption]
<?php else:?>
[caption id="" align="alignright" width="300px"]<img src="<?php echo $section_image;?>" alt=""><?=$caption;?>[/caption]
<?php endif;?>
<?php endif;?>     
<?php endforeach;?>        
<?php echo $Content_Section->find( 'Paragraph', 0 )->innertext; ?>