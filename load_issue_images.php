<?php 
set_time_limit( 5 * 60 * 60 );
require_once( dirname(__FILE__) . "/lib/Config.php");

if( ! defined('__FHB__MIGRATION__') )
	define( '__FHB__MIGRATION__', true );

// WP Load
require_once( dirname(__FILE__) . "/../finehomebuilding-wordpress/web/wp/wp-load.php");
require_once( dirname(__FILE__) . "/../finehomebuilding-wordpress/web/wp/wp-admin/includes/admin.php");
require_once( dirname(__FILE__) . "/../finehomebuilding-wordpress/web/app/themes/finehomebuilding/functions.php");

require_once( dirname(__FILE__) . "/lib/WP/WPAttachment.php");
require_once( dirname(__FILE__) . "/lib/ImageCrop.php");


$all_issues = array();

list( $total, $issues ) = get_issue_posts( 1 );

foreach( $issues as $issue ){
	$fields = get_fields( $issue->ID );
	if( !empty( $fields['issue_number'] )  ){
			$all_issues[] = array( 'ID' => $issue->ID, 'issue_number' => $fields['issue_number'] );
			set_issue_cover( $issue->ID, $fields['issue_number'] );
		}
}
	
$pages = ceil( $total/ 10 );
$i =1;

while( $i < $pages ){
	++$i;
	error_log( "page: $i");
	list( $total, $issues ) = get_issue_posts( $i );
	
	foreach( $issues as $issue ){
		$fields = get_fields( $issue->ID );
		if( !empty( $fields['issue_number'] )  ){
			$all_issues[] = array( 'ID' => $issue->ID, 'issue_number' => $fields['issue_number'] );
			set_issue_cover( $issue->ID, $fields['issue_number'] );
		}
	}
	
}

function get_issue_posts($paged){
	
	$args = array(
			'order'   =>  'desc',
			'orderby' => 'date',
			'posts_per_page' => 10,
			'post_type'		=> 'issue',
	);
	
	if( $paged > 1 ){
		$offset = ( $paged - 1 ) * 10;
		$args['page'] = $paged;
		$args['offset'] = $offset;
	}
	
	$query = new WP_Query( $args );
	
	$total = $query->found_posts;
	
	$posts = $query->posts;
	
	return array( $total, $posts );
}

function set_issue_cover( $issue_id, $issue_number ){
	
	$issue_number = sprintf( "%03d", $issue_number );
	
	$attachment = new WPAttachment();
	
	$image_api = new ImageCrop();
	
	$image_api->upload_path = "";
	
	$image = "/srv/sites/finehomebuilding/data/issue_images/issue_$issue_number.jpg";
	
	if( ! file_exists( $image ) ){
		$image = "/srv/sites/finehomebuilding/data/issue_images/Issue_$issue_number.jpg";
	}
	
	$issue_image = "/issue_images/issue_$issue_number.jpg";
	
	error_log( $image );
	
	if( file_exists( $image ) ){
		
	
		$upload_dir = wp_upload_dir();
			
		$file = $upload_dir['basedir'].$issue_image;

		$dir = dirname( $file );
					
		if( ! file_exists( $dir ) ){
				print mkdir( $dir, 0755, true );
				print "\n";
		}
	
		$image_api->crop_image( $image, $file , 360, 450 );
		
		$attachment_id = $attachment->get_attachment_id_by_url( $upload_dir['baseurl'].$issue_image, $issue_id );
		
		if( !empty( $attachment_id ) ){
			wp_delete_attachment( $attachment_id, true );
			$attachment_id = 0;
		}
		
		if( empty( $attachment_id ) ){
		
			$filetype = wp_check_filetype( basename( $issue_image ), null );
		
			$postinfo = array(
					'guid' => $upload_dir['baseurl'].$issue_image,
					'post_mime_type'	=> $filetype['type'],
					'post_title'		=> $issue_number,
					'post_excerpt' => '',
					'post_content'	=> '',
					'post_status'	=> 'inherit',
			);
		
			$attachment_id = wp_insert_attachment( $postinfo,  $upload_dir['basedir'].$issue_image, $issue_id );
			if( $attachment_id ){
					
				$attach_data = wp_generate_attachment_metadata( $attachment_id, $upload_dir['basedir'].$issue_image );
					
				$res = wp_update_attachment_metadata( $attachment_id,  $attach_data );
					
			}
		}
		
		error_log( "Add attachment $issue_number" .$attachment_id );
		
		update_field( 'field_56f16828ec4ce', $attachment_id, $issue_id );
		
	}
}