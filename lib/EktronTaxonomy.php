<?php 

class EktronTaxonomy {

	private $db;
	
	private $page_size = 100;
	
	private $csv;
	
	private $manager;

	function __construct( $topic_csv ){
		
		$this->service = new EktronWebService();
		$this->db = new FHBMongo('Ektron');
		$this->csv = $topic_csv;
		
	}
	
	public function load_content( $folder_id ){

		$this->db->drop( 'taxonomy' );
		
		$this->process_folder( $folder_id );
		
	}

	function load_taxonomy( $folder_id ){
		
		$csv_topics = $this->csv_topics();
			  
		$this->csv_wp_save( $csv_topics );
		
	}
	
	function csv_wp_save( $csv_topics ){
		
	
		$category_id = '';
		
		
		foreach( $csv_topics as $k => $t ){
			
			if( empty( $t['p'] ) ){
				
				$wp_category = array(
						'cat_name' => $t['t'],
						'category_parent' => 0,
						'taxonomy' => 'category',
				);
					
				$category_id = get_cat_ID( $t['t'] );

				 if( empty( $category_id ) ){
				 	$category_id =  wp_insert_category( $wp_category );
				 }
				  
				 $csv_topics[$k]['category_id'] = $category_id;
				 
			}else{
				
				$sub_topic = '';
				
				if( !empty( $t['n'] ) ){
					
					$sub_topic = $t['n'];
						
				}else{
					
					$sub_topic = $t['t'];
					
				}
				
				$wp_category = array(
						'cat_name' => $sub_topic,
						'category_parent' => $category_id,
						'taxonomy' => 'category',
				);
					
				$sub_category_id = get_cat_ID( $sub_topic );
				
				
				
				if( empty( $sub_category_id ) ){
					
					$sub_category_id =  wp_insert_category( $wp_category );
					
				}
				
				$csv_topics[$k]['category_id'] = $sub_category_id;
				
			}
		}
	
		$this->db->drop( 'csv_taxonomy' );
		
		foreach( $csv_topics as $ct ){
			
			$this->db->insert( 'csv_taxonomy', $ct );
			
		}
			
		
	}
	
	function csv_wp_save_sub( $parent, $category_id ){
	
		$query = array( 'parent' => $parent );
	
		$cursor = $this->db->find( 'taxonomy', $query );
	
		foreach( $cursor  as $t ){
				
			$sub_category_id = get_cat_ID( $t['title'] );
	
			if( empty($sub_category_id) ){
				$sub_category_id =  wp_insert_category(
						array(
								'cat_name' => $t['title'],
								'category_parent' => $category_id,
								'taxonomy' => 'category',
						)
						);
			}
	
			$this->db->update( 'taxonomy', array( '_id' => $t['_id'] ),
					array( '$set' => array( 'category_id' => $sub_category_id ) )
					);
	
				
			$this->wp_save_sub( $t['id'], $sub_category_id );
		}
	
	}
	
	
	function wp_save(){
	
		$query = array( 'level' => 1 );
		
		$cursor = $this->db->find( 'taxonomy', $query );
		
		
		foreach( $cursor  as $t ){
			
			
			$wp_category = array(
					'cat_name' => $t['title'],
					'category_parent' => 0,
					'taxonomy' => 'category',
			);
			
			$category_id = get_cat_ID( $t['title'] );
			
			if( empty( $category_id ) ){
				
				$category_id =  wp_insert_category( $wp_category );
				
			}
			
			$this->db->update( 'taxonomy', array( '_id' => $t['_id'] ), 
										 array( '$set' => array( 'category_id' => $category_id ) ) 
								);
			
			print "Category ID : ".$category_id."\n";
			
			
			$this->wp_save_sub( $t['id'], $category_id );
			
			
		}
		
	}
	
	function wp_save_sub( $parent, $category_id ){
		
		$query = array( 'parent' => $parent );
		
		$cursor = $this->db->find( 'taxonomy', $query );
		
		foreach( $cursor  as $t ){
			
			$sub_category_id = get_cat_ID( $t['title'] );
				
			if( empty($sub_category_id) ){
				$sub_category_id =  wp_insert_category(
						array(
								'cat_name' => $t['title'],
								'category_parent' => $category_id,
								'taxonomy' => 'category',
						)
						);
			}
				
			$this->db->update( 'taxonomy', array( '_id' => $t['_id'] ),
					array( '$set' => array( 'category_id' => $sub_category_id ) )
					);
				
			
			$this->wp_save_sub( $t['id'], $sub_category_id );
		}
		
	}

	function save( $item, $level, $parent  ){
		
		/*
		 * "Title" : "Additions",
		 * "Id" : NumberLong(61994),
		 */
		
		$taxonomy = array();
		if( @$item->Name )
			$taxonomy['title'] = $item->Name;
		
		if( @$item->Title )
			$taxonomy['title'] = $item->Title;
		
		$taxonomy['id'] = $item->Id;
		$taxonomy['level'] = $level;
		$taxonomy['parent'] = $parent;
		
		print_R( $taxonomy );
	
		$this->db->insert( 'taxonomy', $taxonomy );
			
	}

	
	function process_folder( $parent_id ){
	
		$GetChildFolders_Obsolete = new GetChildFolders_Obsolete( $parent_id, true, 'Id' );
	
		$result = $this->service->folder->GetChildFolders( $GetChildFolders_Obsolete );
	
		$folders = $result->GetChildFolders_ObsoleteResult->FolderData;
	
		$level = 0;
		
		$parent = 0;
		
		++$level;
		
		if( is_array( $folders ) ){
	
			foreach( $folders as $folder ){
				print 	"Folder:".$folder->Name ."\n";
				
				$this->save( $folder, $level, $parent );
	
				$this->process_pages( $folder->Id, $level,  $folder->Id   );
	
				if( $folder->ChildFolders ){
					$this->print_folder( $folder->ChildFolders, $level, $folder->Id );
				}
			}
	
		}else{
	
			$this->process_pages( $folders->Id, $level, $folder->Id   );
	
		}
	
	}
	
	

	
	function print_folder( &$folder, $level, $parent ){
	
		if( is_array( $folder->FolderData ) ){
	
			++$level;
			
			foreach( $folder->FolderData as $f ){
					
				print "Folder:".$f->Name."\n";
				
				$this->save( $f, $level, $parent );

				$this->process_pages( $f->Id , $level, $f->Id  );
	
				if( $f->HasChildren ){
					$this->print_folder( $f->ChildFolders,  $level, $f->Id );
				}
			}
		}else{
	
			if( property_exists( $folder, "FolderData") ) {
					
				$FolderData = $folder->FolderData;
					
				if( $FolderData && property_exists( $FolderData, "Name" )  ){
	
					print $t."Folder:".$FolderData->Name."\n";
					
					++$level;
					
					$this->save( $FolderData, $level, $parent );
					
					$this->process_pages( $FolderData->Id, $level, $FolderData->Id );
						
				}else{
						
					print_R( $FolderData );
					die;
	
				}
					
			}else{
				print_R( $folder );
				die;
			}
		}
	}
	
	
	
	function get_pages( $folder_id ){
	
		$pages = 0;
	
		$GetFolder = new GetFolder( $folder_id  );
	
		try{
			$result =  $this->service->folder->GetFolder( $GetFolder );
		}catch( Exception $e ){
			print "GetFolder: ".$e->getMessage()."\n";
		}
		
		if( $result ){
			$total = $result->GetFolderResult->TotalContent;
			$pages = ceil($total/$this->page_size);
			print "Total : $total, Pages:$pages \n";
		}
	
		return $pages;
	}
	
	
	function process_pages( $folder_id, $level, $parent ){
	
		$pages = $this->get_pages( $folder_id  );
	
		$i = 1;
	
		while( $i <= $pages  ){
				
			$this->get_page_items( $folder_id, $i, $level, $parent  );
			++$i;
				
		}
	}
	
	function get_page_items( $folder_id, $page, $level, $parent  ) {
	
		$pagingInfo = new PagingInfo( $this->page_size, $page, 1, 100 );

		$GetChildContentWithPaging = new GetChildContentWithPaging( $folder_id, true, 'id', $pagingInfo );
	
		$option = array('location' => "http://".__EKTRON__CMS_DOMAIN__."/Workarea/webservices/WebServiceAPI/Content/Content.asmx",
				'uri'      => "http://tempuri.org/",
				'trace' => 1,
		);
	
		$s = new EktronSoapClient( NULL, $option );
	
		$auth = new AuthenticationHeader( __EKTRON__USERNAME__, __EKTRON__PASSWORD__, __EKTRON__CMSURL__ );
	
		$RequestInfoParameters = new RequestInfoParameters( 1 );
	
	
		$headers[] = new SoapHeader("http://tempuri.org/",
				'AuthenticationHeader',
				$auth, false);
	
		$s->__setSoapHeaders($headers);
	
		$res = $s->__soapCall('GetChildContentWithPaging', array($GetChildContentWithPaging));
	
			
		if( $res && $res->ContentData ){
	
			$items = $res->ContentData;
	
			foreach( $items as $item ){
	
				if( !empty( $item ) && property_exists( $item, "Title" )  ){
					
					$this->save( $item, $level, $parent );
					
				}else{
					print_R( $item );
				}
			}
	
		}
	}
	
	function csv_topics(){
		
		$fh = fopen( $this->csv, 'r+');
		$content = fread($fh, filesize($this->csv));
		fclose($fh);
		$data = preg_split( "/\n/", $content );
		
		$m = '';
		
		foreach( $data as $r ){
			list ( $k, $v ) = preg_split( "/\|/", $r );
			
			if( preg_match( '/^\@/', $k ) ){
			
				$m = preg_replace( '/^@/', '', $k );
				$topics[] = array( 't' => trim($m) );
				
			}else{
			
				$multiple_topics = preg_split( "/,/", $v );
				
				if( count( $multiple_topics ) > 1 ){
					
					foreach( $multiple_topics as $t ){
						
						$topics[] = array( 't' => trim($k), 'p' => trim($m), 'n' => trim($t) );
						
					}
				
				}else{
				
					$topics[] = array( 't' => trim($k), 'p' => trim($m), 'n' => trim($v) );
				
				}
			
			}
							
		}
		
		return $topics;
		
	}
	
	
}
?>