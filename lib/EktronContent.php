<?php 

class EktronAudio extends EktronContent{
	function __construct(){
		parent::__construct();
	}
	
	function collection(){
		return 'audio';
	}
}

class EktronBookExcerpt extends EktronContent{
	function __construct(){
		parent::__construct();
	}

	function collection(){
		return 'book_excerpt';
	}
}

class EktronContests extends EktronContent{
	function __construct(){
		parent::__construct();
	}

	function collection(){
		return 'contests';
	}
}

class EktronSlideShow extends EktronContent{
	function __construct(){
		parent::__construct();
	}

	function collection(){
		return 'slide_show';
	}
}

class EktronSpecialCollection extends EktronContent{
	function __construct(){
		parent::__construct();
	}

	function collection(){
		return 'special_collection';
	}
}

class EktronManufacturers extends EktronContent{
	function __construct(){
		parent::__construct();
	}

	function collection(){
		return 'manufacturers';
	}
}


class EktronContent {
	
		public $db;
	
		function __construct(){
			
			$this->db = new FHBMongo('Ektron');
			
		}
				
		function save( $metadata, $item, $folders ){
						
			$article = (array) $item;
			
			$article['folders'] = $folders;
			
			$article['META_DATA'] = (array) $metadata;
			
			$query = array( 'Id' => $article["Id"] );
			
			if( !empty( $article['DisplayLastEditDate']  ) ){
					
				$time_ms = strtotime( $article['DisplayLastEditDate'] ) * 1000;
			
				$article['cms_mtime'] = new MongoDB\BSON\UTCDateTime( $time_ms ) ;
					
			}
			
			$existing = $this->db->findOne( $this->collection(),  $query );
			
			if( !empty( $existing ) ){
				
				$article['wp_post_id'] = $existing['wp_post_id'];
				
				$this->db->update( $this->collection(), array( '_id' => $existing['_id'] ) ,  array( '$set' => $article )  );
				
			}else{
				
				$this->db->insert( $this->collection(), $article );
				
			}

		}
		
		function html_data( $html ){
			
			$xml = simplexml_load_string($html);
			$json = json_encode($xml);
			$array = json_decode($json,TRUE);
			return $array;			
		}
		
		function insert_fhb_author(){
			
			$author = new EktronAuthor();
			
			$cursor = $this->db->find( $this->collection(), array() , array("META_DATA.GetContentMetadataListResult.CustomAttribute" => -1 ) );
			
			foreach( $cursor as $obj ){
				
				$c = json_decode(json_encode($obj), true);;
				
				if( isset( $c['META_DATA']['GetContentMetadataListResult']['CustomAttribute'] ) ){
					
					$meta_tags = $c['META_DATA']['GetContentMetadataListResult']['CustomAttribute'];
					
					foreach ( $meta_tags as $m ){
						
						if( $m['Name'] == "Author" && !empty( $m['Value'] ) ){
							
							$ids = preg_split( "/;/", $m['Value'] );
							
							foreach( $ids as $id ){
								
								$author->insert_clean_author( $id );
							}
						}
			
					}
				}
			}
			
		}
		
}


?>