<?php 
class EktronSoapClient extends SoapClient {
	
	function __doRequest($request, $location, $action, $version) {
		$namespace = "http://tempuri.com";
	
		/*
		$request = preg_replace('/<ns1:(\w+)/', '<$1 xmlns="'.$namespace.'"', $request, 1);
		$request = preg_replace('/<ns1:(\w+)/', '<$1', $request);
		$request = str_replace(array('/ns1:', 'xmlns:ns1="'.$namespace.'"'), array('/', ''), $request);
		*/
		
		//print "Location : $location\n";
		$location = preg_replace( '/cmsedit\.taunton\.dev\:(\d{4})/',__EKTRON__CMS_DOMAIN__, $location );
		
		$location = preg_replace( '/cmsedit\.taunton\.dev/', __EKTRON__CMS_DOMAIN__, $location );
		
		//$location = preg_replace('/'.preg_quote(__EKTRON__CMSURL__). '/', __EKTRON__CMS_DOMAIN__, $location );
		
		$action = preg_replace('/#/', '', $action );
		
		error_log(  "Location : $location, Action : $action, Version : $version" );
		//print $request."\n";
		
		if( $action == "http://tempuri.org/GetChildContentWithPaging" ){
		 	$request = $this->GetChildContentWithPagingXML( $request );	
		}
				
		// parent call
		$response = NULL;
		try{
			$response = parent::__doRequest($request, $location, $action, $version);
		}catch( Excpetion $e ){
			print $request;
			print "\n\n";
			print $e->getMessage()."\n";
			
		}
		return $response;
		
	}
	
	
	public function GetChildContentWithPagingXML( $xml ){
		
		$xml = preg_replace('/Username/si', 'ns1:Username', $xml );
		$xml = preg_replace('/Password/si', 'ns1:Password', $xml );
		
		$xml = preg_replace('/Domain/si', 'ns1:Domain', $xml );
		$xml = preg_replace('/ContentLanguage/si', 'ns1:ContentLanguage', $xml );
		
		$xml = preg_replace('/FolderID/si', 'ns1:FolderID', $xml );
		$xml = preg_replace('/Recursive/si', 'ns1:Recursive', $xml );
		$xml = preg_replace('/OrderBy/si', 'ns1:OrderBy', $xml );
		$xml = preg_replace('/pagingInfo/si', 'ns1:pagingInfo', $xml );
		$xml = preg_replace('/RecordsPerPage/si', 'ns1:RecordsPerPage', $xml );
		$xml = preg_replace('/CurrentPage/si', 'ns1:CurrentPage', $xml );
		$xml = preg_replace('/TotalPages/si', 'ns1:TotalPages', $xml );
		$xml = preg_replace('/TotalRecords/si', 'ns1:TotalRecords', $xml );
		
		$xml = preg_replace('/<param0 xsi:type="SOAP-ENC:Struct">/si', '', $xml );
		
		$xml = preg_replace('/<\/param0>/si', '', $xml );
		
		$xml = preg_replace('/ xsi:type="(.*?)"/si', '', $xml );
		
		return $xml;
		
	}
}
?>