<?php 

class EktronWebService {

	public $folder;
	
	public $content;
	
	
	function __construct(){
		
		$auth = new AuthenticationHeader( __EKTRON__USERNAME__, __EKTRON__PASSWORD__, __EKTRON__CMSURL__ );
		
		$this->folder = new Folder(array());
		
		$this->content = new Content(array());
		
		//$this->contentservice = new ContentService(array());
		
		$this->metadata = new Metadata(array());
		
		$headers[] = new SoapHeader("http://tempuri.org/",
				'AuthenticationHeader',
				$auth);
		
		$this->folder->__setSoapHeaders($headers);
		
		$this->content->__setSoapHeaders($headers);
		
		//$this->contentservice->__setSoapHeaders($headers);
		
		$this->metadata->__setSoapHeaders($headers);
		
	}
	
}

?>