<?php 
$host = php_uname('n');

switch ($host) {
	case 'wpappsrv01':
		define('WP_ENV', 'production');
		define('WP_SERVER', 'web1');
		break;
	case 'wpappsrv2':
		define('WP_ENV', 'production');
		define('WP_SERVER', 'web2');
		break;
	case 'appserver-1c':
		define('WP_ENV', 'development');
		break;
	case 'stage.appsrv1':
		define('WP_ENV', 'stage');
		break;
	default:
		define('WP_ENV', 'development');
		break;
}

if( WP_ENV == 'production' ){
	
	define( '__EKTRON__CMSURL__', 'cmsedit.taunton.com' );
	define( '__EKTRON__USERNAME__', 'ekesavan' );
	define( '__EKTRON__PASSWORD__', 'taunton' );
	
	define( '__EKTRON__FOLDER__WSDL__', 'http://cmsedit.taunton.com:8081/Workarea/webservices/WebServiceAPI/Folder.asmx?WSDL' );
	define( '__EKTRON__Content__WSDL__', 'http://cmsedit.taunton.com:8081/Workarea/webservices/Content.asmx?WSDL' );
	
	define( 'WP_UPLOAD_DIR', '/srv/sites/media/' );
	define( '__EKTRON__CMS_HOST__', 'http://cmsedit.taunton.com:8081/' );
	define( '__EKTRON__CMS_DOMAIN__', 'cmsedit.taunton.com:8081' );
	define( '__FHB_HOST__', 'http://www.finehomebuilding.com' );
	
	define( '__S3_URL__', 'https://s3.amazonaws.com/finehomebuilding.s3.tauntoncloud.com' );
	
	define( '__CI__MYSQL__DBHOST__', 'localhost' );
	define( '__CI__MYSQL__DBNAME__', 'taunton_ci_finehomebuilding_prod' );
	define( '__CI__MYSQL__DBUSER__', 'deploy' );
	define( '__CI__MYSQL__DBPASSWORD__', 'utU7d2GvmZTq' );
	
	define( '__MONGO__DBHOST__', 'wpmongo.tauntoncloud.com' );
	
}elseif( WP_ENV == 'stage' ){	
	
	define( '__EKTRON__CMSURL__', 'cmsedit.taunton.com' );
	define( '__EKTRON__USERNAME__', 'ekesavan' );
	define( '__EKTRON__PASSWORD__', 'taunton' );
	
	define( '__EKTRON__FOLDER__WSDL__', 'http://cmsedit.taunton.com:8081/Workarea/webservices/WebServiceAPI/Folder.asmx?WSDL' );
	define( '__EKTRON__Content__WSDL__', 'http://cmsedit.taunton.com:8081/Workarea/webservices/Content.asmx?WSDL' );
	
	define( 'WP_UPLOAD_DIR', '/backup/media/' );
	
	define( '__EKTRON__CMS_HOST__', 'http://cmsedit.taunton.com:8081/' );
	define( '__EKTRON__CMS_DOMAIN__', 'cmsedit.taunton.com:8081' );
	define( '__FHB_HOST__', 'http://www.finehomebuilding.com' );
	define( '__S3_URL__', 'https://s3.amazonaws.com/finehomebuilding.s3.tauntoncloud.com' );
	
	
	define( '__CI__MYSQL__DBHOST__', 'localhost' );
	define( '__CI__MYSQL__DBNAME__', 'taunton_ci_finehomebuilding_prod' );
	define( '__CI__MYSQL__DBUSER__', 'deploy' );
	define( '__CI__MYSQL__DBPASSWORD__', 'utU7d2GvmZTq' );
	
	define( '__MONGO__DBHOST__', 'localhost' );
		
}else{
	
	/*
	define( '__EKTRON__CMSURL__', 'cmsedit.taunton.dev' );
	define( '__EKTRON__USERNAME__', 'ekesavan' );
	define( '__EKTRON__PASSWORD__', 'Taunton1' );
	*/
	
	define( '__EKTRON__CMSURL__', 'cmsedit.taunton.com' );
	define( '__EKTRON__USERNAME__', 'ekesavan' );
	define( '__EKTRON__PASSWORD__', 'taunton' );
	
	
	define( '__EKTRON__FOLDER__WSDL__', 'http://cmsedit.taunton.dev:8081/Workarea/webservices/WebServiceAPI/Folder.asmx?WSDL' );
	
	define( '__EKTRON__Content__WSDL__', 'http://cmsedit.taunton.dev:8081/Workarea/webservices/Content.asmx?WSDL' );
	
	define( 'WP_UPLOAD_DIR', '/srv/sites/finehomebuilding/finehomebuilding-wordpress/web/app/uploads' );
	
	//define( '__EKTRON__CMS_HOST__', 'http://cmsedit.taunton.dev:8081/' );
	//define( '__EKTRON__CMS_DOMAIN__', 'cmsedit.taunton.dev:8081' );
	define( '__EKTRON__CMS_HOST__', 'http://cmsedit.taunton.com:8081/' );
	define( '__EKTRON__CMS_DOMAIN__', 'cmsedit.taunton.com:8081' );
	define( '__FHB_HOST__', 'http://www.finehomebuilding.com' );
	
	
	define( '__CI__MYSQL__DBHOST__', '10.0.2.203' );
	define( '__CI__MYSQL__DBNAME__', 'taunton_ci_finehomebuilding_prod' );
	define( '__CI__MYSQL__DBUSER__', 'deploy' );
	define( '__CI__MYSQL__DBPASSWORD__', 'utU7d2GvmZTq' );
	
	define( '__MONGO__DBHOST__', 'localhost' );
}

?>