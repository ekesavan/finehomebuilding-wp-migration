<?php 
/*
 Open Calais Token:
 
 PuJf90SlOAk6CiNIRpsdCDUP9cPWXHV1
 
*/

class CodeIgniterReport {

	private $db;

	private $mysql;
	
	private $token = 'PuJf90SlOAk6CiNIRpsdCDUP9cPWXHV1';

	function __construct(){

		$mongo = new MongoClient();

		$this->db = $mongo->CodeIgniter;
		
		$this->calais = $mongo->OpenCalais;

	}

	/*
	taunton_user_level 1 => contributer
	taunton_user_level 2 => Editor
	*/
	
	function contributors_list(){
		
		$query = array( "meta_key" => "taunton_user_level", "meta_value" => "1" );
		
		$user_ids = array();
		
		$cursor = $this->db->usermeta->find( $query );
		
		unlink( 'contributors-report.csv' );
		
		foreach( $cursor as $user_meta ){
			
			print_R( $user_meta );
			
			$u = $this->db->users->findOne( array( 'ID' => $user_meta['user_id'] ) );
			
			$data = array();
			$data[] = 1;
			$data[] = $u['user_first_name'];
			$data[] = $u['user_last_name'];
			$data[] = $u['user_email'];
			$data[] = $u['user_title'];
			
			$query = array( 'post_author' => $u['ID'],  'post_parent' => '0', 'post_status' => 'publish' );
			
			$article_count = $this->db->posts->find($query)->count();
			$data[] = $article_count;
			
			$query = array( 'post_author' => $u['ID'],  'post_parent' => '0', 'post_status' => 'draft' );
				
			$article_count = $this->db->posts->find($query)->count();
			$data[] = $article_count;
			
			$str = NULL;
			$str = implode( "|", $data )."\n";
			file_put_contents("contributors-report.csv", $str, FILE_APPEND );
			
		}
		
		$total = $cursor->count();
		print "Contributors Total : $total \n";
		
	}
	
	function editors_list(){
	
		$query = array( "meta_key" => "taunton_user_level", "meta_value" => "2" );
	
		$user_ids = array();
	
		$cursor = $this->db->usermeta->find( $query );
	
		unlink( 'editor-report.csv' );
	
		foreach( $cursor as $user_meta ){
				
			print_R( $user_meta );
				
			$u = $this->db->users->findOne( array( 'ID' => $user_meta['user_id'] ) );
				
			$data = array();
			$data[] = 2;
			$data[] = $u['user_first_name'];
			$data[] = $u['user_last_name'];
			$data[] = $u['user_email'];
			$data[] = $u['user_title'];
				
			$query = array( 'post_author' => $u['ID'],  'post_parent' => '0', 'post_status' => 'publish' );
				
			$article_count = $this->db->posts->find($query)->count();
			$data[] = $article_count;
				
			$query = array( 'post_author' => $u['ID'],  'post_parent' => '0', 'post_status' => 'draft' );
	
			$article_count = $this->db->posts->find($query)->count();
			$data[] = $article_count;
				
			$str = NULL;
				
			$str = implode( "|", $data )."\n";
			file_put_contents("editor-report.csv", $str, FILE_APPEND );
				
		}
	
		$total = $cursor->count();
		print "Total : $total \n";
	
	}
	
	function user_report(){
		
		/*
		 * users
		 *
		 * "_id" : ObjectId("56787376be6dfa8e799224b6"),
		 "ID" : "1700",
		 "TID" : "461958",
		 "user_first_name" : "Matt",
		 "user_last_name" : "Berger",
		 "user_email" : "mattberger10@gmail.com",
		 "user_url" : "",
		 "user_registered" : "2009-11-02 12:25:13",
		 "user_status" : "1",
		 "display_name" : "ManKnit",
		 "user_title" : "executive producer"

		 * posts
		 */
		$cursor = $this->db->users->find();
		$count = $cursor->count();

		print "Total Users : $count \n";

		foreach(  $cursor as $u ){
				
			print "Remaining: ". --$count . "\n";
				
			$data = array();
			$data[] = $u['user_first_name'];
			$data[] = $u['user_last_name'];
			$data[] = $u['user_email'];
			$data[] = $u['user_title'];
			$query = array( 'post_author' => $u['ID'] );
			$article_count = $this->db->posts->find($query)->count();
			$data[] = $article_count;
			$str = NULL;
				
			if(  $article_count > 0 ) {
				$str = implode( "|", $data )."\n";
				file_put_contents("codeigniter-report.csv", $str, FILE_APPEND );
			}

		}

	}

	function editorial_report(){

		$query = array( 'user_title' => array( '$ne' => 'member' ) );

		$cursor = $this->db->users->find( $query );

		$count = $cursor->count();

		print "Total Users : $count \n";

		foreach(  $cursor as $u ){

			print "Remaining: ". --$count . "\n";

			$data = array();
				
			$data[] = $u['user_first_name'];
			$data[] = $u['user_last_name'];
			$data[] = $u['user_email'];
			$data[] = $u['user_title'];
				
			$ektron_author = $this->ektron_db->fhb_author->findOne( array( 'emailaddress' => $u['user_email'] ) );
				
			print_R( $ektron_author );
				
				
			/*
			 *
			 $query = array( 'post_author' => $u['ID'] );
			 $article_count = $this->db->posts->find($query)->count();
			 $data[] = $article_count;
			 $str = NULL;

			 if( $article_count > 0 ) {

				$str = implode( "|", $data )."\n";
				file_put_contents("codeigniter-report.csv", $str, FILE_APPEND );
				}
					
				*/

		}
	}

	function spam_content_report(){

		$post_types = array(
				"gallery",
				"attachment",
				"placeholder",
				"post",
				"html",
				"house-gallery",
				"video",
				"kb-contest",
				"attachment-video",
				"digitalissue",
		);

		$query = array( 'post_parent' => '0', 'post_status' => 'publish'  );

		foreach( $post_types as $post_type ){
				
			$query['post_type'] = $post_type;
				
			$count = $this->db->posts->find( $query )->count();
				
			print "$post_type --> $count \n";
		}

	}
	
	
	function blog_terms(){
	
		$terms = $this->db->terms->distinct( 'name' );
		
		//print_R( $terms );
		
		
		$str = implode( "\n", $terms );
		
		print " Total: " .count($terms) ."\n";
		
		unlink( 'codeigniter-terms.csv' );
		
		file_put_contents("codeigniter-terms.csv", $str, FILE_APPEND );
		
		$pspell_link =pspell_new("en", PSPELL_FAST);    
		
		$remove_terms = array();
		
		$updated_terms = array();
		
		foreach ( $terms as $term ){
	
			$invalid = false;
			
			$word = explode(" ", $term);
			foreach($word as $k => $v) {
				if (pspell_check($pspell_link, $v)) {
					//echo $v."\n";
				} else {
					//echo "Sorry, wrong spelling";
					$invalid = true;
				};
			};
			
			if( $invalid ){
				//print $term."\n";
				$remove_terms[]=  $term;
			}else if( preg_match( '/\s+vs\.?\s/i', $term ) ){
				$remove_terms[]=  $term;
			}else if( preg_match( '/(escort|laptop|sale|Pandora|free|watch|weight|cheap|sex|fuck|live|sport|NFL|girl|Game|online|TV|dress|news|stream|canada)/si', $term ) ){
				$remove_terms[]=  $term;
			}else if( preg_match( '/\s+\w\w?\s+/si', $term ) ){
					$remove_terms[]=  $term;
			}else if( preg_match( '/^\w\w?\s+/si', $term ) ){
					$remove_terms[]=  $term;
			}elseif( strlen($term) <= 3 ){
				$remove_terms[]=  $term;
			}else{
				$updated_terms[] = $term;
			}
		}
		
		
		$updated_terms_str = implode( "\n", $updated_terms );
		unlink( 'codeigniter-updated-terms.csv' );
		file_put_contents("codeigniter-updated-terms.csv", $updated_terms_str, FILE_APPEND );
		print " Removed: " .count($remove_terms) ."\n";
		
		
	}

	
	
	
	/*
	 			"attachment",
		        "placeholder",
		        "gallery",
		        "post",
		        "html",
		        "house-gallery",
		        "video",
		        "kb-contest",
		        "attachment-video",
		        "digitalissue"
		        
		        [ "inherit", "draft", "publish" ]
		        post_status
	 */
	
	function post_type_report(){
		$post_types = array(
				"gallery",
				"post",
				"html",
				"house-gallery",
				"video",
				"kb-contest",
				"digitalissue"
		);
		
		foreach( $post_types as $post_type ){
			
		
			$query = array( 'post_parent' => '0', 'post_type' => $post_type  );
			$query['post_status'] = 'publish';
			print  "$post_type\tpublish : ". $this->db->posts->find( $query )->count() ."\n";
			
			$query['post_status'] = 'draft';
			print  "$post_type\tdraft :". $this->db->posts->find( $query )->count() ."\n";
		
		}
				
	}
	
	function open_calais_report(){
		//$query = array( 'post_parent' => '0', 'post_type' => 'gallery'  );
		
		$query = array( 'post_parent' => '0', 'post_type' => 'gallery', 'post_status' => 'publish'  );
		
		$posts = $this->db->posts->find( $query );
		
		$report = array();
		
		$report['topics'] = array();
		
		$report['socialTag'] = array();
		

		foreach( $posts as $p ){
			
			if( !empty( $p['calais'] ) ){
				foreach( $p['calais'] as $k => $v ){
					
					if( isset($v['_typeGroup']) && (  $v['_typeGroup'] == 'topics' ) ){
						++$report['topics'][$v['name']];
						
						if( $v['score'] > 0.7 ){
							$str = $p['ID'].",http://www.finehomebuilding.com/".$p['guid']."\n";
							file_put_contents( $this->to_slug( $v['name']) .".csv", $str, FILE_APPEND );
						}
					}
					
					
					if( isset($v['_typeGroup']) && (  $v['_typeGroup'] == 'socialTag' ) ){
						if( !isset( $report['socialTag'][$v['name']] ) )
							$report['socialTag'][$v['name']] = 0;
						
						++$report['socialTag'][$v['name']];
					}
					
					
							
				}
				
				
			}
		}
		$a = $report['socialTag'];
		asort( $a );
		$report['socialTag'] = array_reverse($a);
		
		foreach( $report['topics'] as $k => $v ){
			$str = implode( "|", array($k,$v ) )."\n";
			file_put_contents("codeigniter-topics.csv", $str, FILE_APPEND );
		}
		
		foreach( $report['socialTag'] as $k => $v ){
			$str = implode( "|", array($k,$v ) )."\n";
			file_put_contents("codeigniter-tags.csv", $str, FILE_APPEND );
		}
				
	}
	
	function to_slug($string){
		return strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $string)));
	}
	
	function duplicate_users(){
		$query = array( '$group' => array( '_id' =>  '$TID', 'count' => array( '$sum' => 1 ) ));
		$op = array( '$match' => array( 'count' => array( '$gt' => 1  ) ) );	
		$duplicates = $this->db->users->aggregate( $query, $op );
		unlink('codeigniter-user-duplicates.csv');
		foreach( $duplicates['result'] as $k => $d ){
			$str = implode( "|", $d )."\n";
			print $str;
			file_put_contents("codeigniter-user-duplicates.csv", $str, FILE_APPEND );
		}
	}
	
}

?>