<?php 
/*
 Open Calais Token:
 
 PuJf90SlOAk6CiNIRpsdCDUP9cPWXHV1
 
*/

class CodeIgniterContentAnalysis {

	private $db;

	private $mysql;
	
	private $token = 'PuJf90SlOAk6CiNIRpsdCDUP9cPWXHV1';

	private $spam_topcis = array( 'Health_Medical_Pharma', 'Entertainment_Culture', 'Sports' );
	
	function __construct(){

		$mongo = new MongoClient();

		$this->db = $mongo->CodeIgniter;
		
		$this->calais = $mongo->OpenCalais;

	}

	function open_calais(){
	
		$calais = new HTTPSClientCalaisPost();
	
		$this->db->posts->ensureIndex( array( 'post_parent' => 1, 'post_type' => 1, 'post_status' => 1  ) );
		
		$this->calais->posts->ensureIndex( array( 'ID' => 1 ) );
		
		$query = array( 'post_parent' => '0', 'post_type' => 'gallery', 'post_status' => 'publish'  );
	
		//$query = array(  "ID" =>  "148376" );
		
		$posts = $this->db->posts->find( $query );
	
		
		
		$total  = $this->db->posts->find( $query )->count();
	
		print "Total Published Articles : $total \n";
	
	
		$count = 0;
		
		foreach( $posts as $post ){
				
			++$count;
		
			$remain = $total - $count;
				
  		    print "Processed: $count --> Remaining: $remain ".$post['ID']."---".$post['post_title']."\n";
							
			if( empty( $post['post_content'] ) ) continue;
			
			$c = $this->calais->posts->findOne( array( 'ID' => $post['ID'] ) );
						
			if( !empty( $c ) ) continue;
				
			$response = '';
			
			$response = $calais->request( $this->token, strip_tags($post['post_content'] ));
				
			$data = '';
			if( !empty($response) ){				
				$data = json_decode($response,true );
			}
			
			if( empty( $data ) ) continue;
			
			$p = array();	
			$p['ID'] = $post['ID'];
			$p['calais'] = $data;
			
			$this->calais->posts->insert($p);
			
			

		}
	}
	
	function find_spam_content(){
		
		$cursor = $this->calais->posts->find()->limit(2);
		
		$count = $cursor->count();
		
		$i = 0;
		
		$step = ($count> 100) ? 100 : $count ;
			
				
		while( $i < $count ){
			++$i;
			
			$articles = $this->calais->posts->find()->skip($i)->limit($step);
		
			foreach( $articles as $a ){
				
				print "Processed $i, Remaining:". ( $count - $i ) ."\n";
				
				$spam = false;
				
				$spam = $this->spam_content( $a['calais'] );
				
				if( $spam ){
					print $a['ID']."\n";
					//die;
				}
				
				/*
				$this->db->posts->update(
						array( 'ID' => $a['ID'] ),
						array( '$set' => array( 'spam' => $spam ) ) );
				*/		
			}
		}
				
		
	}
	
	private function spam_content( $c ){
		$spam = false;
				
		if( !empty( $c['error'] ) ){
			
			if( $c['error']['status']['errorCode'] == 'Unsupported-Language' ){
				print "------------------Unsupported-Language------------------\n";
				return true;		
			}
		}
		/*
		if( !empty( $c['meta'] ) ){
			if( $c['meta']['language'] == "InputTextTooShort" ){
				
				return true;
			}
		}
		*/
		$all_topics = array();
		
		$social_tags = array();
		
		foreach( $c as $k => $v ){

			if( in_array( '_typeGroup', array_keys($v) ) ){
				if( $v['_typeGroup'] == 'topics'){
					
					$all_topics[$v['name']] = floatval($v['score'] );
					
				}
				
				if( $v['_typeGroup'] == 'socialTag'){
				
					$social_tags[] = $v['name'];
						
				}
				
			}
		}
		
		if( in_array( 'Hospitality_Recreation', array_keys($all_topics) ) ){
			return false;
		}
		
		if( in_array( 'Door', $social_tags ) ){
			return false;
		}
		
		if( in_array( 'Desks', $social_tags ) ){
			return false;
		}
	
		if( in_array( 'Shipbuilding', $social_tags ) ){
			return false;
		}
		
		
		foreach( $this->spam_topcis as $t ){
			if( isset( $all_topics[$t] ) && ( $all_topics[$t] >= 1 ) ) {
				
				print "----------------------".$t."==".$all_topics[$t]."------------------\n";
				
				$spam = true;
				break;
			}
		}
		
		return $spam;
	}
	
	/*
	 "_id" : ObjectId("56d46fb2be6dfa11538b47a4"),
        "ID" : "73441",
        "calais" : {
                "error" : {
                        "status" : {
                                "code" : "400",
                                "errorCode" : "Unsupported-Language",
                                "errorDescription" : "You've submitted a document in 'Arabic', which is not currently supported."
                        }
                }
        }
	 */
	
	/*
	calais" : {
                "doc" : {
                        "info" : {
                                "calaisRequestID" : "1db2595f-b7d1-e711-1532-dd647ab5ef40",
                                "id" : "http://id.opencalais.com/G1S1fI-htxdqYTPoV2H1bA",
	                                "ontology" : "http://mdaast-virtual-onecalais.int.thomsonreuters.com/owlschema/9.2/onecalais.owl.allmetadata.xml",
	                                "docId" : "http://d.opencalais.com/dochash-1/fecd7a93-6469-38ab-9b2c-6e9265fe6eaa",
	                                "document" : "8qIQ0l8JCLLPFdwbdqbxUJSVvs9G0s4SVOkwOZISMDzowOZWh",
	                                "docTitle" : "",
	                                "docDate" : "2016-02-29 16:22:11.191"
	},
	"meta" : {
	"contentType" : "text/raw",
	"processingVer" : "AllMetadata",
	"serverVersion" : "OneCalais_9.2-RELEASE:402",
	"stagsVer" : "OneCalais_9.2-RELEASE-b2-2015-12-31_01:34:35",
	"submissionDate" : "2016-02-29 16:22:11.115",
	"submitterCode" : "0ca6a864-5659-789d-5f32-f365f695e757",
	"signature" : "digestalg-1|IxexKwmmxErwNikW2Fz0iSk/cuk=|Zbe1ARsUWTS/2u4fnArYh0IEws4a/CXaKvO/CILdLd3dmKOkGk5mYQ==",
	"language" : "InputTextTooShort"
	}
	},
	*/
	
}
