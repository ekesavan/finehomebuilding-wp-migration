<?php

/*

 captcha            X                              
 comments                                         
 links      		X                                      
 log                                              
 marketplace                                      
 marketplace_addresses                            
 marketplace_images                               
 marketplace_stats       
                          
 options			X                                          
 
 
 orderdata                                        
 partner_optins                                   
 
 pool_module_relationships                        
 pool_modules                                     
 pool_optin                                       
 pool_post_relationships                          
 pool_term_relationships                          
 pool_users                                       
 pools                                            
 
 post_extended                                    
 postmeta                                         
 posts                                            
 posts_201109011142                               
 
 products                                         
 taxonomy_products   X                             
 term_products		 X                                    
 term_relationships                               
 term_taxonomy                                    
 terms                                            
 
 usermeta                                         
 usermeta_copy		X                                    
 users                                            
 users_profile                                    
 warmboard_optins	X                 

*/

class CodeIgniterImport {
	
	private $db;
	
	private $mysql;
	
	private $collection_index = array(
				'posts' => array( 'ID', 'post_parent', 'cms_mtime', 'post_type', 'post_status'),
				'users' => array( 'ID', 'TID', 'user_status', 'display_name'),
				'term_relationships' => array( 'object_id', 'term_taxonomy_id' ),
				'term_taxonomy' =>  array( 'term_taxonomy_id', 'term_id'),
				'terms' => array( 'term_id' ),
				'postmeta' => array( 'post_id' ),
				'post_extended' => array( 'post_id' ),
				'usermeta' => array( 'user_id' ),
				'comments' =>  array( 'comment_ID', 'comment_post_ID', 'user_id', 'comment_approved')
			);

	function __construct(){
		
		$this->db = new FHBMongo('CodeIgniter');
		
		$this->mapdb = new FHBMongo('WPMap');
		
		$this->mysql = new PDO("mysql:host=".__CI__MYSQL__DBHOST__.";dbname=".__CI__MYSQL__DBNAME__.";charset=utf8", __CI__MYSQL__DBUSER__, __CI__MYSQL__DBPASSWORD__ );
		
		$this->mysql->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, false);
		
	}
	
	function create_users_mapping(){

		$query = array(	'wp_user_id' => array( '$ne' => null ) );
		
		$count = $this->db->count( 'users', $query  );
		
		print "Total Users : $count \n";
				
		$i = 0;
		
		$step = ( $count > 100 )? 100 : $count ;
		
		while( $i < $count ){
				
			$option = array( 'skip' => $i, 'limit' => $step, 'noCursorTimeout' => true, 'sort' => array('ID' => 1 ), 'batchSize' => 100 );
				
			$cursor = $this->db->find( 'users', $query, $option );
		
			foreach(  $cursor as $obj ){
		
				$mongo_id = $obj->_id;
					
				$u = json_decode(json_encode($obj), true);
				error_log( ( $count - $i )."------>" . $u['ID']. "-------->". $u['wp_user_id'] );
				$this->mapdb->insert( 'users', array( 'ID' => $u['ID'], 'wp_user_id' => $u['wp_user_id'] ) );
				
				++$i;
			}
		}
		
	}
	
	function create_posts_mapping(){
	
		$query = array(	'wp_post_id' => array( '$ne' => null ) );
	
		$count = $this->db->count( 'posts', $query  );
	
		print "Total Users : $count \n";
	
		$i = 0;
	
		$step = ( $count > 100 )? 100 : $count ;
	
		while( $i < $count ){
	
			$option = array( 'skip' => $i, 'limit' => $step, 'noCursorTimeout' => true, 'sort' => array('ID' => 1 ), 'batchSize' => 100 );
	
			$cursor = $this->db->find( 'posts', $query, $option );
	
			foreach(  $cursor as $obj ){
	
				$mongo_id = $obj->_id;
					
				$p = json_decode(json_encode($obj), true);
					
				
				$this->mapdb->insert( 'posts', array( 'ID' => $p['ID'], 'wp_post_id' => $p['wp_post_id'] ) );
				
				++$i;
				
				error_log( $i."------>" . $p['ID']. "-------->". $p['wp_post_id'] );
			}
		}
	
	}
	
	function sync(){
		
		/* select all collection */
		$collections = $this->db->get_collections();
		
		foreach ($collections as $collection) {
			echo "\t * ", $collection->name, "\n";
			
			
		}
	}
	
	function load(){
		
		$tables = array();
		
		try {
			
			foreach( $this->mysql->query('SHOW TABLES') as $row) {
				$tables[] = $row['Tables_in_taunton_ci_finehomebuilding_prod'];
			}
						
			
		} catch (PDOException $e) {
			print "Error!: " . $e->getMessage() . "<br/>";
			die();
		}
		
		foreach( $tables as $t ){
			
			$mysql_count = 0;
			$mongo_count = 0;
			
			foreach( $this->mysql->query( "SELECT count(*) FROM $t") as $r ){
				$mysql_count = $r['count(*)'];
			}

			$mongo_count = $this->db->count( $t );
			
			print "----------------->".$t."---".$mysql_count."--".$mongo_count."\n";
			
			if( $mysql_count > $mongo_count ){
			
				$count = 0;
				
				/*
				 * Drop Collection
				 */				
				$this->db->drop( $t );
				
				/*
				 * Create Collection
				 */
				$this->db->create_collection( $t );

				
				/*
				 * Create Index
				 */
				if( !empty( $this->collection_index[$t] ) ){
					
					foreach( $this->collection_index[$t]  as $key ){
						
						$this->db->create_index( $t, $key );
						
					}
				}
				
				$result = $this->mysql->query( "SELECT * FROM $t" );
				
			
				if ($result) {
					
					$count = 0;
					
					while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
						
						if( !empty( $row['post_modified']  ) ){
							
							$time_ms = strtotime( $row['post_modified'] ) * 1000;
						
							$row['cms_mtime'] = new MongoDB\BSON\UTCDateTime( $time_ms ) ;
							
						}
						
						if( !empty( $row['user_registered']  ) ){
								
							$time_ms = strtotime( $row['user_registered'] ) * 1000;
						
							$row['cms_mtime'] = new MongoDB\BSON\UTCDateTime( $time_ms ) ;
								
						}
						
						
						$this->db->insert( $t,  $row );
						
						++$count;
						
						print "$t Processed:$count Remaining:".($mysql_count-$count)."\n";
						
					}
					
				}
				
			}
		}
		
	}
}

?>