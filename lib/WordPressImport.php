<?php 

/*
		article
		audio
		author
		book_excerpt
		contests
		department_article
		fhb_author
		menu_taxonomy
		qa
		readers_tips
		review
		slide_show
		special_collection
		system.indexes
		taxonomy
		tool_guide
		video
 */

class WordPressImport{
	
	private $db;
	
	private $collection;
	
	private $count;
	
	function __construct(){
		
		$this->db = new FHBMongo('Ektron');
		
		$this->codeigniter_db = new FHBMongo('CodeIgniter');
		
		$this->wp_map_db =  new FHBMongo('WPMap');
		
		$this->wp_user = new WPUser();
		$this->wp_author = new WPAuthor();
		$this->wp_article = new WPArticle();
		$this->wp_product = new WPProduct();
		$this->wp_review = new WPReview();
		$this->wp_video = new WPVideo();
		$this->wp_manufacturer = new WPManufacturer();
		$this->wp_digital_issue = new WPDigitalIssue();
		$this->wp_pool = new WPPool();
	}
	
	public function insert_authors( $query = array() ){
		
		$cursor = $this->db->find( 'fhb_author', $query );
		
		$count = 0;

		foreach( $cursor as $obj ){
			
			$mongo_id = $obj->_id;
			
			$a = json_decode(json_encode($obj), true);
			
			if( empty( $a['Title'] ) ) continue;		
			
			$wp_user_id = $this->wp_author->insert_ektron_author( $a );

			$this->db->update( 'fhb_author', array( '_id' => $mongo_id ), array( '$set' => array( 'wp_user_id' => $wp_user_id ) ) );
			
			print "Author: $wp_user_id ". $a['Title']."\n";
			
			++$count;
		}
		
		print "Total Authors: $count \n";

	}
	
	public function insert_codeigniter_authors(){
		
		/*
		 * Load CodeIgniter Contributers, Editors
		 */
		$query = array( 'user_title' => array( '$ne' => 'member' ) );
		
		//$query = array( 'ID' => '1700' );
		
		$count = $this->codeigniter_db->count( 'users', $query );
		
		print "Total Users : $count \n";
		
		$cursor = $this->codeigniter_db->find( 'users', $query );
		
		foreach(  $cursor as $obj ){
			
			$mongo_id = $obj->_id;
			
			$u = json_decode(json_encode($obj), true);
		
			error_log( $u['ID']."------------------------".$u['user_title'] );
			
			//$usermeta = $this->codeigniter_db->usermeta->findOne( array( 'user_id' => $u['ID'], 'meta_key' => 'taunton_user_level' ) );
			
			if( 1 ){
				
				$users_profile_obj = $this->codeigniter_db->findOne( 'users_profile',  array( 'user_id' => $u['ID'] ) );
				
				$u['users_profile'] = json_decode(json_encode($users_profile_obj), true);
				
				$wp_user_id = $this->wp_author->insert_codeigniter_author( $u );
				
				$this->codeigniter_db->update( 'users', array( '_id' => $mongo_id ), array( '$set' => array( 'wp_user_id' => $wp_user_id ) ) );
								
			}
		}
	}
	
	public function insert_articles( $query = array() ){
		$this->process_collection( 'article', 'Default', $query );
	}
	
	public function insert_department_articles( $query = array() ){
		$this->process_collection( 'department_article', 'Default', $query );
	}
	
	public function insert_book_excerpt( $query = array() ){
		$this->process_collection( 'book_excerpt', 'Book Excerpt', $query );
	}
	
	public function insert_book_contests( $query = array() ){
		$this->process_collection( 'contests', 'Book Contests', $query );
	}
	
	public function insert_question_answer( $query = array() ){
		$this->process_collection( 'qa', 'Question Answer', $query );
	}

	public function insert_readers_tips( $query = array() ){
		
		$this->process_collection( 'readers_tips', 'Readers Tips', $query );
	}
	
	public function insert_slide_show( $query = array() ){
		
		$this->process_collection( 'slide_show', 'Slide Show', $query  );
	}
	
	
	public function insert_special_collection( $query = array() ){
		
		$this->process_collection( 'special_collection', 'Special Collection', $query );
	}
	
	
	/* Video */
	public function insert_video( $query =  array() ){
		
		$this->process_collection( 'video', 'Video', $query );
	
	}
	
	
	public function insert_review( $query = array() ){

		$query["IsPublished"] =  "true";
		
		$collection = 'review';
		
		$i = 0;
		
		$step = 500;
		
		$count = $this->db->count( $collection, $query );
		
		
		while($i < $count ) {
				
				
			$option = array( 'skip' => $i, 'limit' => $step, 'noCursorTimeout' => true, 'sort' => array('Id' => 1 ), 'batchSize' => 500 );
				
			$cursor = $this->db->find( $collection, $query, $option );
				
				
			foreach( $cursor as $obj ){
					
				++$i;
				
				$mongo_id = $obj->_id;
		
				$a = json_decode(json_encode($obj), true);
		
				if( empty( $a['Title'] ) ) continue;
				
		/*
				if( $this->skip_wp_process( $a['Id'] ) ){
					print "------------------skip_wp_process: ".$a['Id']."\n";
					continue;
				}
		*/		
				$this->wp_map_db->insert( 'wp_process', array( 'ID' => $a['Id'] ) );
				
				
				$post_id = $this->wp_review->insert_post( $a );
				
				$this->db->update('review', 
						array( '_id' => $a['_id'] ),
						array( '$set' => array( 'wp_post_id' => $post_id, 'mtime' => new MongoDB\BSON\UTCDateTime( time() * 1000 ) ) )
						);
				
				//$this->update_cms_wp_mapping( 'ektron', 'review', $a['Id'],  $post_id );
				
				$r = $count - $i;
		
				error_log( "Review Processed:$i, Remaining:$r ------>". $a['Title']." post ID:$post_id");
				
				unset( $a );
					
			} 
		
		}
		
		print "Total Review: $count \n";
		
	}
	
	private function process_collection( $collection, $section, $query = array() ){
		
		print "process_collection : $collection \n";
		
		$this->collection = $collection;
		
		$query["IsPublished"] =  "true";
		
		//$query["mtime"] =  array( '$lt' => new MongoDB\BSON\UTCDateTime( ( time() - 2 * 60 *60 ) * 1000 ) );
		
		$i = 0;
		
		$step = 500;
		
		$count = $this->db->count( $collection, $query );
		
		print "Total $count \n";
		
		while($i < $count ) {
			
			
			$option = array( 'skip' => $i, 'limit' => $step, 'noCursorTimeout' => true, 'sort' => array('Id' => 1 ), 'batchSize' => 500 );
			
			$cursor = $this->db->find( $collection, $query, $option );
			
			
			foreach( $cursor as $obj ){
			
				$mongo_id = $obj->_id;
				
				$a = json_decode(json_encode($obj), true);
				
				++$i;
				
				if( empty( $a['Title'] ) ) continue;
				
				/*
				 * Check New / Existing Article
				 */
				$history_link = "http://www.finehomebuilding.com".$a['Quicklink'];
				error_log( "FHB ID=".$a['Id']." LINK : $history_link");
				
				$posts = get_posts(array(
						'numberposts'	=> 1,
						'post_type'		=> 'post',
						'meta_key'		=> 'fhb_history_link',
						'meta_value'	=> $history_link
				));
				
				if(  !empty( $posts )  ){
					
					error_log ("$i-----SKIP Existing Article: ".$a['Id']);
				
				}else{
				
					error_log( "$i-----INSERT New Article: ".$a['Id'] );
					
					$this->wp_map_db->insert( 'wp_process', array( 'ID' => $a['Id'] ) );
					
					
					$post_id = $this->wp_article->insert_article( $a, $section );
					
					if( !empty( $post_id ) ){
						
						 $this->db->update(
						 		$collection,
						 		array( '_id' => $mongo_id ),
						 		array( '$set' => array( 'wp_post_id' => $post_id, 'mtime' => new MongoDB\BSON\UTCDateTime( time() * 1000 ) ) )
						 );
								
					}
				}
				
				$r = $count - $i;
				
				error_log( "$collection Total: $count Processed:$i, Remaining:$r ------>". $a['Title']." post ID:$post_id");
			
			}

		}
		
		print "Total $section: $count \n";
	}
	
	
	
	
	/* tool_guide 	*/
	public function insert_tool_guide(){
	
		//$query = array( 'Id' => '129895');
	
		$query = array();
		
		$count = $this->db->tool_guide->find($query)->count();
	
		$i = 0;
		$step = 50;
	
		while( $i < $count ){
	
			$articles =$this->db->tool_guide->find($query)->skip($i)->limit($step);
	
			foreach( $articles as $a ){
	
				if( empty( $a['Title'] ) ) continue;
	
				$post_id = $this->wp_product->insert_post( $a );
				
				$this->db->tool_guide->update( 
						array( '_id' => $a['_id'] ),
						array( '$set' => array( 'wp_post_id' => $post_id, 'mtime' => new MongoDB\BSON\UTCDateTime( time() ) ) )
						);
				
				$this->update_cms_wp_mapping( 'ektron', 'tool_guide', $a['Id'],  $post_id );
				
				++$i;
	
				print " Tool Guide: $i ------>". $a['Title']."\n";
					
			}
	
		}
		print "Totlal Tool Guide : $count \n";
	
	}
	
	public function insert_manufacturer(){
	
		$query = array();
	
		$count = $this->db->count('manufacturers');
	
		$i = 0;
		$step = 100;
		$collection = 'manufacturers';
				
		while($i < $count ) {
					
			$option = array( 'skip' => $i, 'limit' => $step, 'noCursorTimeout' => true, 'sort' => array('Id' => 1 ), 'batchSize' => 500 );
					
			$cursor = $this->db->find( $collection, $query, $option );
				
			foreach( $cursor as $obj ){
			
				$mongo_id = $obj->_id;
			
				$a = json_decode(json_encode($obj), true);
		
				if( empty( $a['Title'] ) ) continue;
			
				$post_id = $this->wp_manufacturer->insert_post( $a );
				
				$this->db->update( 
					'manufacturers', 
					array( '_id' => $mongo_id ), array( '$set' => array( 'wp_post_id' => $post_id ) ) 
					);
			
				++$i;
			
				print "Manufacturer: $i ------>". $a['Title']."\n";
			
			}
		}

		print "Totlal Manufacturer : $count \n";
	
	}
	
	/*
	 
	  "gallery", =>  article  readers project
      "attachment", 
      "placeholder",
      "post", articel Blog
      "html", articel Blog
      "house-gallery",  article  readers project
      "video", article  video
      "kb-contest", Article Contest 
      "attachment-video",
      "digitalissue" => digital issue
  
	 */
	
	public function insert_codeigniter_content(){
		
		$query = array(
				'post_parent' => '0', 
				'post_status' => 'publish', 
				//'wp_post_id' => array( '$eq' => null )
		);
		
		
		/* March 15 2016 */
		$time = strtotime( '2016-03-15' );
		//$query['cms_mtime'] = array( '$gt' => new MongoDB\BSON\UTCDateTime( $time * 1000  )  );
		
		//$query['post_type'] = 'house-gallery';
		//$query['post_type'] = 'kb-contest';
		$query = array( 'ID' => "10812" );
		
		
		$count = $this->codeigniter_db->count('posts', $query );
		
		error_log( "Total Articles : $count");
		
		
		$i = 0;
		
		$step = 100 ;
		
		while( $i < $count ){
		
					
			$option = array( 'skip' => $i, 'limit' => $step, 'noCursorTimeout' => true, 'sort' => array('Id' => 1 ), 'batchSize' => 500 );
				
			$cursor = $this->codeigniter_db->find( 'posts', $query, $option );
			
			foreach( $cursor as $obj ){
				
				++$i;
				
				$mongo_id = $obj->_id;
					
				$a = json_decode(json_encode($obj), true);
				
				/*
				* Spam content
				*/
				
				if( ($a['post_type'] == 'gallery') &&  $this->wp_article->spam_content(  $a['post_title'] ) ){
					error_log( "----------------------------SPAM------------------------------------");
					continue;					
				}
				
				error_log( "Processed: $i Remaining:".($count-$i) );
				
				/*
				$map_post = $this->wp_map_db->findOne( 'posts', array( 'ID' => $a['ID']  )  );
				
				if( !empty( $map_post ) ){
						
					error_log( "$i:--------SKIP WP PROCESS: ".$a['ID']."----->".$map_post['wp_post_id'] );
						
					$this->codeigniter_db->update( 'posts',
							array( '_id' => $mongo_id ),
							array( '$set' => array( 'wp_post_id' =>$map_post['wp_post_id'], 'mtime' => new MongoDB\BSON\UTCDateTime( time() * 100 )  ) ) );
				
					continue;
						
				}
				*/
				
				
				//if( $this->skip_wp_process( 'wp_process', $a['ID'] ) ) continue;
				
				$this->wp_map_db->insert( 'wp_process', array( 'ID' => $a['ID'] ) );
								
				
				if( $a['post_type'] == 'digitalissue' ) continue;
				
				$data = array();
				
				/* Post */
				$data['post'] = $a;
				
				
				/* User */
				$user = $this->codeigniter_db->findOne( 'users', array( 'ID' => $a['post_author'] ) );
				
				error_log("CI_USER_ID=".$user['ID']."--------------->wp_post_id>".  $a['wp_post_id'] ." wp_user_id:".$user['wp_user_id'] );
				
				/*
				if( !empty( $a['wp_post_id'] ) && !empty( $user['wp_user_id'] ) ){
					
					update_field( 'field_56ce013c265a0', $user['wp_user_id'], $a['wp_post_id'] );
					
				}
				continue;
				*/
				
				/* Post Meta */
				$data['post_meta'] = $this->codeigniter_db->findOne( 'postmeta', array( 'post_id' => $a['ID'] ) );
				
				/* Post Extended */
				$data['post_extended'] = $this->codeigniter_db->findOne( 'post_extended', array( 'post_id' => $a['ID'] ) );
				
				/* User Meta */
				if( !empty( $user ) ){
					
					$usermeta = $this->codeigniter_db->findOne( 'usermeta', array( 'user_id' => $user['ID'], 'meta_key' => 'taunton_user_level' ) );
					$data['user_meta'] = $usermeta;
				
				}
										
				
				$data['user'] = $user;
				$data['attachments'] = array();
				$data['taxonomy'] = array();
				
				$term_taxonomy_ids = array();
				foreach(  $this->codeigniter_db->find( 'term_relationships', array( 'object_id' => $a['ID'] ) ) as $ptobj ){
					$pt = json_decode(json_encode($ptobj), true);
					$term_taxonomy_ids[] = $pt['term_taxonomy_id'];
				}
				
						
				$data['taxonomy'] = array();
				
				if( !empty( $term_taxonomy_ids ) ){
						
					$terms = $this->codeigniter_db->find( 'term_taxonomy', array( 'term_taxonomy_id' => array( '$in' => $term_taxonomy_ids ) ));
						
					foreach( $terms as $tobj ){
						$t = json_decode(json_encode($tobj), true);
						$term = $this->codeigniter_db->findOne( 'terms', array( 'term_id' => $t['term_id'] ) );
				
						if( !isset( $data['taxonomy'][$t['taxonomy']] ) ){
							$data['taxonomy'][$t['taxonomy']] = array();
						}
				
						$data['taxonomy'][$t['taxonomy']][] = $term['name'];
					}
				}
				
				
				foreach(  $this->codeigniter_db->find( 'posts', array( 'post_parent' => $a['ID'] ) ) as $cobj ){
					$c  = json_decode(json_encode($cobj), true);
					$data['attachments'][]  = $c;	
				}
				
				error_log( "$i Blog: CodeIgniter POST ID=". $a['ID']."  ". $a['guid'] );
				
				$wp_post_id = $this->wp_article->codeigniter_content( $data );

				
				$this->codeigniter_db->update(
					   'posts',
						array( '_id' => $mongo_id ), 
						array( '$set' => array( 'wp_post_id' => $wp_post_id, 'mtime' => new MongoDB\BSON\UTCDateTime( time() * 1000 )  ) ) );
				
			}
		
		}

		print "Total Blogs: $count \n";
	
	}
	
	public function insert_codeigniter_digital_issues(){
	
		
		$query = array( 'post_type' => 'digitalissue', 'post_parent' => '0' , 'post_status' => 'publish' );
		
		//$this->codeigniter_db->posts->ensureIndex( array( 'post_type' => 1, 'post_parent' => 1, 'post_status' => 1 ) );
		
		$count = $this->codeigniter_db->count( 'posts', $query );
	
		print "insert_codeigniter_digital_issues : $count \n";
		
		$i = 0;
		$step = $count > 50 ? 50 : $count;
	
		while( $i < $count ){
	
			$option = array( 'skip' => $i, 'limit' => $step, 'noCursorTimeout' => true, 'sort' => array('Id' => 1 ), 'batchSize' => 500 );
			
			$cursor = $this->codeigniter_db->find( 'posts', $query, $option );
								
			foreach( $cursor as $obj ){
				
				++$i;
				
				$mongo_id = $obj->_id;
					
				$a = json_decode(json_encode($obj), true);
								
				$data = array();
					
				$user = $this->codeigniter_db->findOne( 'users', array( 'ID' => $a['post_author'] ) );
	
				$data['post'] = $a;
				$data['user'] = $user;
				$data['attachments'] = array();
				$data['extended'] = array();
				
				foreach(  $this->codeigniter_db->find( 'posts', array( 'post_parent' => $a['ID'] ) ) as $cobj ){
					$c  = json_decode(json_encode($cobj), true);
					$data['attachments'][]  = $c;
						
				}
	
				
				foreach(  $this->codeigniter_db->find(  'post_extended', array( 'post_id' => $a['ID'] ) ) as $cobj ){
					$c  = json_decode(json_encode($cobj), true);
					$data['extended'][]  = $c;
				}
				
				print "$i Digital Issue: ". $a['post_title']."\n";
					
				$wp_post_id = $this->wp_digital_issue->insert_digital_issue( $data );
				
				
				$this->codeigniter_db->update( 'posts',
						array( '_id' => $mongo_id ), 
						array( '$set' => array( 'wp_post_id' => $wp_post_id,  'mtime' => new MongoDB\BSON\UTCDateTime( time() * 1000 ) ))
						);
								
				++$i;
					
			}
	
		}
	
		print "Total Digital Issues: $count \n";
	
	}
	
	public function insert_codeigniter_pools(){
		
		$query = array( 'pool_type' => array( '$in' => array( 'blog', 'contest' ) ));
		
		//$query = array( 'pool_id' => '51' );
		
		$count =  $this->codeigniter_db->count( 'pools', $query );
		
		print "insert_codeigniter_pools : $count \n";
			
		$i = 0;
		$step = 100;
		
		while( $i < $count ){
		
			$option = array( 'skip' => $i, 'limit' => $step, 'noCursorTimeout' => true, 'sort' => array('Id' => 1 ), 'batchSize' => 500 );
			
			$cursor = $this->codeigniter_db->find( 'pools', $query, $option );
								
			foreach( $cursor as $obj ){
				
				++$i;
				
				$mongo_id = $obj->_id;
					
				$p = json_decode(json_encode($obj), true);
				
				
				$history_link = "http://www.finehomebuilding.com/".$p['pool_guid'];
				
				$posts = get_posts(array(
						'numberposts'	=> -1,
						'post_type'		=> "pool",
						'meta_key'		=> 'history_link',
						'meta_value'	=> $history_link
				));
				
				$post_id = 0;
				if( !empty( $posts ) ){
					
					$post_id = $posts[0]->ID;
					//error_log( $posts[0]->post_title );
					
					continue;
					
				}else{
					error_log( "pool not found". $p['pool_guid'] );
					print_R( $p );
				}
		
				$data = array();

				// pool content
				$data['post'] = $p;
		
				// pool modules
				foreach(  $this->codeigniter_db->find( 'pool_module_relationships', array( 'pool_id' => $p['pool_id'] ) ) as $robj ){
					$r = json_decode(json_encode($robj), true);
					$r['module_content'] = unserialize( $r['module_content'] );
					$data['modules'][]  = $r;
				}
				
				// pool terms
				$term_taxonomy_ids = array();
				foreach(  $this->codeigniter_db->find( 'pool_term_relationships', array( 'object_id' => $p['pool_id'] ) ) as $ptobj ){
					$pt = json_decode(json_encode($ptobj), true);
					$term_taxonomy_ids[] = $pt['term_taxonomy_id'];					
				}
				
				$data['taxonomy'] = array();
				
				if( !empty( $term_taxonomy_ids ) ){
					
					$terms = $this->codeigniter_db->find( 'term_taxonomy', array( 'term_taxonomy_id' => array( '$in' => $term_taxonomy_ids ) ));
					
					foreach( $terms as $tobj ){
						$t = json_decode(json_encode($tobj), true);
						
						$term = $this->codeigniter_db->findOne( 'terms', array( 'term_id' => $t['term_id'] ) );
						
						if( !isset( $data['taxonomy'][$t['taxonomy']] ) ){
							$data['taxonomy'][$t['taxonomy']] = array();
						}
						
						$data['taxonomy'][$t['taxonomy']][] = $term['name'];
					}
				}
				
					
				$wp_post_id = $this->wp_pool->insert( $data );
				
				$this->codeigniter_db->update( 'pools', array( '_id' => $mongo_id  ), array( '$set' => array( 'wp_post_id' => $wp_post_id ) ) );
				
				//$this->update_cms_wp_mapping( 'codeigniter', 'pool', $p['pool_id'],  $wp_post_id );
				
				print "$i Pool: ". $p['pool_title']." WP POST ID : $wp_post_id \n";
								
				print_R(  $data['taxonomy'] );
				
				++$i;
					
			}
		
		}
		
	}
	
	
	public function update_cms_wp_mapping( $type, $collection, $cms_id, $wp_id ){
		
		$m = $this->wp_map_db->content->findOne( array( 'cms_id' => $cms_id ) );
		
		if ( !empty( $m  ) ){
			
			$this->wp_map_db->content->update( array( '_id' => $m['_id'] ) , array( 'type' => 'ektron', 'collection' => $collection,  'cms_id' => $cms_id , 'wp_id' => $wp_id ) );
			
		}else{
			
			$this->wp_map_db->content->insert( array( 'type' => 'ektron', 'collection' => $collection,  'cms_id' => $cms_id , 'wp_id' => $wp_id ) );
			
		}
		
	}
	
	
	public function insert_digital_issue_articles(){
		
		$issues = array();
	
		$collections =  array(
				'article',
				'audio',
				'book_excerpt',
				'contests',
				'department_article',
				'qa',
				'readers_tips',
				'review',
				'slide_show',
				'special_collection',
				'tool_guide',
				'video',
		);
		
		
		foreach( $collections as $collection ){
			
			$cursor = $this->db->find( $collection, array(), array('Id' => 1, 'DisplayDateCreated' => 1, "META_DATA.GetContentMetadataListResult.CustomAttribute" => 1 ) );
			
			foreach( $cursor as $cobj ){
				
				$c  = json_decode(json_encode($cobj), true);
				
				if( isset( $c['META_DATA']['GetContentMetadataListResult']['CustomAttribute'] ) ){
					$meta_tags = $c['META_DATA']['GetContentMetadataListResult']['CustomAttribute'];
					foreach ( $meta_tags as $m ){
						if( ($m['Name'] == "Title/Issue") && !empty($m['Value']) ){
							
							if( !isset( $issues[$m['Value']] ) ){
								$issues[$m['Value']] = array();
							}
							$issues[$m['Value']][] = array( 'collection' => $collection, 'Id' => $c['Id'], 'date' => $c['DisplayDateCreated'] );
						}
					}
				}
			}
			
		}
		
		/*
		 * combine issues
		 */
		$issues['231, Kitchens &amp; Baths, Winter 2012' ] = array_merge( $issues['231, Kitchens &amp; Baths, Winter 2012' ], $issues['231, Kitchen &amp; Baths, Winter 2012' ]);
		unset( $issues['231, Kitchen &amp; Baths, Winter 2012'] );
		
		$issues['151(Kitchens &amp; Baths)' ] = array_merge( $issues['151(Kitchens &amp; Baths)' ], $issues['151 (Kitchens &amp; Baths)' ]);
		unset( $issues['151 (Kitchens &amp; Baths)'] );
		
		
		foreach( $issues as $k => $i ){
		
			print $k."\n";
			
			if( count( $i ) > 5 ){
				
				$this->wp_digital_issue->update_digital_issue( $k, $i );
				
				
				//break;
			}
			
		}
		
	}
	
	
	public function insert_codeigniter_users(){
		
		/*
		 * Load CodeIgniter Users / Members
		 */
		//$query = array( "display_name" => "DorisCGonzalez" );
		$query = array(
				"user_status" => "1", 
				'user_title' => array( '$eq' => 'member' ),
				'wp_user_id' => array( '$eq' => null ) ,
		);
		
		$query = array( "ID" => "2532" );
		
		/* March 15 2016 */
		//$time = strtotime( '2016-03-15' );
		//$query['cms_mtime'] = array( '$gt' => new MongoDB\BSON\UTCDateTime( $time * 1000  )  );
		
	
		$count = $this->codeigniter_db->count( 'users', $query  );
		
		print "Total Users : $count \n";
		
		$i = 0;
		
		$step = ( $count > 10000 )? 10000 : $count ;
		
		while( $i < $count ){
		
			print "Fetch skip:$i, limit:$step \n";
			
			$option = array(
					'skip' => $i,
					'limit' => $step, 
					'noCursorTimeout' => true, 
					'sort' => array('ID' => 1 ),
					'batchSize' => 10000 
					
			);
			
			$cursor = $this->codeigniter_db->find( 'users', $query, $option );
			
		
			foreach(  $cursor as $obj ){
				
				++$i;
				
				$mongo_id = $obj->_id;
					
				$u = json_decode(json_encode($obj), true);
				
				
				$map_user = $this->wp_map_db->findOne( 'users', array( 'ID' => $u['ID']  )  );
				
				if( !empty( $map_user ) ){
					
					error_log( "$i:--------SKIP WP PROCESS: ".$u['ID']."----->".$map_user['wp_user_id'] );
					
					$this->codeigniter_db->update( 'users',
							array( '_id' => $mongo_id ),
							array( '$set' => array( 'wp_user_id' =>$map_user['wp_user_id'], 'mtime' => new MongoDB\BSON\UTCDateTime( time() * 100 )  ) ) );
				
					continue;
					
				}
				
				$this->wp_map_db->insert( 'users', array( 'ID' => $u['ID'] ) );
				
				
				error_log( "Processing user_id: " . $u['ID'] ."------------------$i, Remain".( $count - $i )."-----------------");
				
				//$usermeta = $this->codeigniter_db->findOne( 'usermeta', array( 'user_id' => $u['ID'], 'meta_key' => 'taunton_user_level' ) );
				
				$u['users_profile'] = $this->codeigniter_db->findOne( 'users_profile', array( 'user_id' => $u['ID'] ) );
				
				if( empty( $u['users_profile'] ) ){
				
					$user_post = $this->codeigniter_db->findOne( 'posts' ,
							array(
									"post_status" => "publish",
									"post_author" => $u['ID'],
							)
					);
					
					if( empty( $user_post ) ){
						
						
						$user_comment = $this->codeigniter_db->findOne('comments',
								array(
										"comment_approved" => "1",
										"user_id" => $u['ID'],
								)
								);
					
						if( empty( $user_comment ) )		
						{
							/*
							 * DO NOT create User
							 */
							error_log( "-----------------DO NOT create User--------------------------");
							$this->codeigniter_db->update( 'users',
									array( '_id' => $mongo_id ),
									array( '$set' => array( 'wp_user_id' => -1, 'mtime' => new MongoDB\BSON\UTCDateTime( time() * 100 )  ) ) );
							
							continue;
						}
				
					}
				}
				
				$wp_user_id = $this->wp_user->insert_user( $u );
	
				$this->codeigniter_db->update( 'users', 
						array( '_id' => $mongo_id ), 
						array( '$set' => array( 'wp_user_id' => $wp_user_id, 'mtime' => new MongoDB\BSON\UTCDateTime( time() )  ) ) );
				
			}
		}
	}
	
	
	/*
	  "_id" : ObjectId("56a0216abe6dfa826c8b4567"),
        "comment_ID" : "442",
        "comment_post_ID" : "3597",
        "comment_author" : "MBerger",
        "comment_author_email" : "mberger@taunton.com",
        "comment_author_url" : "",
        "comment_author_IP" : "205.139.9.179",
        "comment_date" : "2008-11-14 14:11:20",
        "comment_date_gmt" : "2008-11-14 14:11:20",
        "comment_content" : "Nice work JP. looks good.",
        "comment_karma" : "0",
        "comment_approved" : "1",
        "comment_agent" : "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.648; .NET CLR 3.5.30729)",
        "comment_type" : "",
        "comment_parent" : "0",
        "user_id" : "1700"
	 */
	
	public function insert_codeigniter_comments(){
		
		global $wpdb;
		
		$reader_project_types = array(
				'gallery' => 'Gallery',
				'house-gallery' => 'House Gallery',
				'kb-contest' => 'KB Contest',
		);
		
		
		/*
		 * Approved Comments
		 *  "comment_approved" : "1"
		 */
		//$query = array( "comment_post_ID" => "11226" );
		
		$query = array( 
				//'comment_approved' => array( '$ne' => 'spam' ),
				'comment_approved' => "1",
				'wp_comment_id' => array( '$exists' => false ) 
		);
		
		$cursor = $this->codeigniter_db->find( 'comments', $query );
		
		$count = $this->codeigniter_db->count( 'comments', $query );
		
		error_log( "Total Comments : $count");
				
		$i = 0;
		
		$step = 100;
		
		while( $i < $count ){
		
			$option = array( 'skip' => $i, 'limit' => $step, 'noCursorTimeout' => true, 'sort' => array('Id' => 1 ), 'batchSize' => 100 );
			
			$cursor = $this->codeigniter_db->find( 'comments', $query, $option );
								
			foreach( $cursor as $obj ){
				
				++$i;
				
				$mongo_id = $obj->_id;
					
				$c = json_decode(json_encode($obj), true);
				
				$p = $this->codeigniter_db->findOne( 'posts',  array ('ID' => $c['comment_post_ID'] ));
				
				if( empty( $p ) ) continue;
				
				if( isset( $reader_project_types[$p['post_type']] ) ){
					$post_type= 'readerproject';
				}else{
					$post_type = 'post';
				}
				
				$history_link = "http://www.finehomebuilding.com".$p['guid'];
				
				$wp_post_id = 0;
				
				
				$posts = get_posts(array(
						'numberposts'	=> -1,
						'post_type'		=> $post_type,
						'meta_key'		=> 'fhb_history_link',
						'meta_value'	=> $history_link
				));
					
				if( !empty( $posts ) ){
					error_log( $history_link );
					$wp_post_id = $posts[0]->ID;
				}else{
					error_log( "POST NOT FOUND: $history_link");
					continue;
				}
				
				$u = $this->codeigniter_db->findOne( 'users',  array ('ID' => $c['user_id'] ) );
				
				$tid = $u['TID'];
				
				$wp_user_id = '';
				
				$table_name = $wpdb->prefix . 'fhb_user';
				
				$m = $wpdb->get_row($wpdb->prepare("SELECT ID FROM $table_name WHERE TID= %d", intval($tid) ), ARRAY_A);
				
				if( !empty( $m ) ){
					$wp_user_id = $m['ID'];
				}
				
				error_log( "------------------------find_wp_member : TID".$user['TID']."    WP_ID:".$wp_user_id );
				
				error_log("ci_user_id=".$c['user_id'].", wp_comment_id=".$c['comment_ID'].", wp_user_id=".$wp_user_id );
										
				unset( $c['comment_ID'] );
				
				$c['comment_post_ID'] = $wp_post_id;
				
				$c['user_id'] = 0;
				
				print_R( $c );
				
				
				
				$wp_comment_id = wp_insert_comment($c);
				
				update_field( 'field_56cbda32d3a28', intval($wp_user_id), 'comment_'.$wp_comment_id );
				
				$this->codeigniter_db->update(
						'comments',
						array( '_id' => $mongo_id ),
						array( '$set' => array( 'wp_comment_id' => $wp_comment_id, 'mtime' => new MongoDB\BSON\UTCDateTime( time() )  ) ) );
				die;
			}
		
		}
			
	}
	
	function print_process(){
		error_log( $this->collection."---------------------->".$this->count );
	}
	
	function skip_wp_process( $collection, $cms_id ){
		
		$m = $this->wp_map_db->findOne( $collection, array( 'ID' => $cms_id  )  );
		
		if( !empty( $m ) ){
			return true;
		}
		
		return false;
	}
	
	
	
}

?>