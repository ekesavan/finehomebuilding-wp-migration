<?php 

class FHBSpider{
	
	public $db;
	
	function __construct(){
			
		$mongo = new MongoClient();
		$this->db = $mongo->spider;
		
	}
	
	function process_dir( $dir ){
		
		$this->getDirContents( $dir );
		
	}
	
	
	function getDirContents($dir){
		$files = scandir($dir);
	
		foreach($files as $key => $value){
			
			$path = realpath($dir.DIRECTORY_SEPARATOR.$value);
			
			if(!is_dir($path)) {
				
				$results[] = $path;
				
				$this->process_path( $path );
				
			} else if($value != "." && $value != "..") {
				
				$this->getDirContents($path);
				
				//$this->process_path( $path );
			}
		}
	}
	
	function process_path( $path ){
		
		$path_info = pathinfo( $path );
		
		if( $path_info['extension'] == 'asp' ) {
			
			//print_R( $path_info );
			
			$dir = $path_info['dirname'];
			
			$dir = preg_replace( '|/srv/sites/finehomebuilding/site_content|', '',  $dir);
			
			$url = __FHB_HOST__.$dir."/".$path_info['basename'];
			
			print $url."\n";

			// static page
			$this->process_page( $url );
		
			sleep(1);
		}
		
	}
	
	function process_page(  $url ){
		
		$html = $this->fetch_page( $url );
		
		$this->insert_page( $url, $html );
		
	}
	
	function insert_page( $url, $html ){
		
		$data = array();
		$data['url'] = $url;
		$data['html'] = $html;
		$this->db->pages->insert( $data );
		
	}
		
	
	function fetch_page( $url ){

		$ch = curl_init();
		
		$timeout = 5;
		
		curl_setopt($ch, CURLOPT_URL, $url);
		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
		
		$html = curl_exec($ch);
		
		curl_close($ch);
		
		return $html;
		
	}
	
	function wp_insert_pages(){
		
		//$query = array( 'url' => 'http://www.finehomebuilding.com/pages/fh_currentissue.asp' );
		
		$query = array();
		
		$pages = $this->db->pages->find( $query );
		
		
		foreach( $pages as $page ){
		
			print $page['url']."\n";
			
			$html = $page['html'];

			if( !empty( $html ) ){
				$html = $this->clean_html( $html );
				if( $html ){
					$this->create_static_page( $page['url'], $html );
				}else{
					$this->create_include_page( $page['url'], $html );
				}
			}
		}
		
	}
	
	function create_include_page( $url, $html  ){
		  
		$url = preg_replace( '|http://www.finehomebuilding.com|', '', $url );
		 
		$pathinfo = pathinfo( $url );
		 
		$file = '/srv/sites/finehomebuilding/development'. $pathinfo['dirname']."/".$pathinfo['filename'].".php";
		$dir = dirname( $file );
		if( ! file_exists( $dir ) ){
			print mkdir( $dir, 0755, true );
			print "\n";
		}
		 
		file_put_contents( $file, $html  );
		
	}
	
	function create_static_page( $url, $html ){
		
	   	ob_start();
	   	require(dirname(__FILE__)."/../templates/static-page.php");
	    $content = ob_get_clean();
	   	
	    $url = preg_replace( '|http://www.finehomebuilding.com|', '', $url );
	    
	    $pathinfo = pathinfo( $url );
	    
	    $file = '/srv/sites/finehomebuilding/development'. $pathinfo['dirname']."/".$pathinfo['filename'].".php";
	    
	    $dir = dirname( $file );

	    if( ! file_exists( $dir ) ){
	    	print mkdir( $dir, 0755, true );
	    	print "\n";
	    }
	    
	    file_put_contents( $file, $content  );
		
	}
	
	function create_redirect(){
		
	}
	
	function clean_html( $html ){
		print "###".$html."###\n";
		
		$dom = str_get_html( $html  );
		
		if( $dom->find('title',0) )
			$title = $dom->find('title',0)->innertext;
		if( ! $dom->find('body',0) ){
			return false;
		}
		
		$body = $dom->find('body',0);
		
		foreach( $body->find('script') as $s ){
			$s->outertext = '';
		}
		
		foreach( $body->find('noscript') as $s ){
			$s->outertext = '';
		}

		if( $body->find('#topnav-wrapper',0) )
			$body->find('#topnav-wrapper',0)->outertext = '';
		
		if( $body->find('#footer',0) )
			$body->find('#footer',0)->outertext = '';
		
		$html = $body->innertext ."\n";
		
		$dom->clear();
		unset( $dom );
		
		$clean = str_get_html( $html  );
		
		foreach( $clean->find('img') as $img ){
			
			$imge_src = $this->image_src( $img->src );
			
			$imge_dst = $this->image_dst( $img->src );

			print $imge_src."-->".$imge_dst."\n";
			
			$src = $this->copy_image( $imge_src, $imge_dst );
			
			$img->src = CONTENT_DIR.'/uploads'.$imge_dst;
			
		}
		
		$html = (string) $clean;
		
		$clean->clear();
		unset( $clean );
	
		// Specify configuration
		$config = array(
		           'indent'         => true,
		           'output-xhtml'   => true,
				   'show-body-only' => true,
		           'wrap'           => 200
				
		);
		
		// Tidy
		$tidy = new tidy;
		$tidy->parseString($html, $config, 'utf8');
		$tidy->cleanRepair();

		// Output
		$content = $tidy;
		
		return $content;
	}
	
	function image_src( $src ){
		
		$full_url = '';
		
		if( preg_match( '|^//modules|', $src  ) ){
			$full_url = 'http:'.$src;
		}elseif( preg_match( '|^images|', $src  ) ) {
			$full_url = 'http://www.finehomebuilding.com/pages/'.$src;
		}else{
			$full_url = 'http://www.finehomebuilding.com/'.$src;
		}
		
		return $full_url;
		
	}
	
	
	function image_dst( $src ){
		
		$pathinfo = pathinfo( $src );
		
		$dirname = $pathinfo['dirname'];
		$basename = $pathinfo['basename'];
		
		$dirname = preg_replace( '|^\.\.|', '', $dirname );
		
		$dirname = preg_replace( '|^\/|', '', $dirname );
		
		$dst = '/fhb_2015/'.$dirname."/".$basename;

		return $dst;		
	}
	
	function copy_image(  $image_src, $image_dst ){
	
		
		$file = WP_UPLOAD_DIR.$image_dst;
	
		
		//if( !file_exists( $file ) ){
		
			$dir = dirname( $file );

			if( ! file_exists( $dir ) ){
				print mkdir( $dir, 0775, true );
				print "\n";
			}
			
			$url = $image_src;
			print "GET $url \n";
			$ch = curl_init( $url  );
			$fp = fopen( $file, 'w');
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
			curl_setopt($ch, CURLOPT_FILE, $fp);
			//curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_exec($ch);
			curl_close($ch);
			fclose($fp);
		//}
	
		return $file;
	}
	
}
?>