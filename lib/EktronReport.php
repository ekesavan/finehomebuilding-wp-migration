<?php 
class EktronReport{
	
	private $db;
	
	private $collections =  array(
			
								'article',
								'audio',
								'author',
								'book_excerpt',
								'contests',
								'department_article',
								'qa',
								'readers_tips',
								'review',
								'slide_show',
								'special_collection',
								'tool_guide',
								'video',
							);
	
	
	function __construct(){
		$mongo = new MongoClient();
		$this->db = $mongo->Ektron;
		
	}
	
	public function find_channels(){
		$channels = array();
		foreach( $this->collections as $c ){
			$cursor = $this->db->selectCollection( $c )->find( array(), array( "Quicklink" => -1, "META_DATA.GetContentMetadataListResult.CustomAttribute" => -1 ) );
			foreach( $cursor as $c ){
				if( isset( $c['META_DATA']['GetContentMetadataListResult']['CustomAttribute'] ) ){ 
					$meta_tags = $c['META_DATA']['GetContentMetadataListResult']['CustomAttribute'];
					
					foreach ( $meta_tags as $m ){
						if( $m['Name'] == "Primary Channel - Fine Homebuilding" ){
							if( !isset( $channels[$m['Value']] ) ){
								$channels[$m['Value']] = 1;
							}else{
								++$channels[$m['Value']];
							}
							if( empty( $m['Value'] ) ){
								$content = "http://www.finehomebuilding.com".$c['Quicklink']."\n";
								file_put_contents( "empty-channel.txt", $content, FILE_APPEND );
							}
						}
														
					}
				}
			}
		
		}
		return $channels;
	}
	
	public function find_content_brands(){
		$brands = array();
		foreach( $this->collections as $c ){
			$cursor = $this->db->selectCollection( $c )->find( array(), array("META_DATA.GetContentMetadataListResult.CustomAttribute" => -1 ) );
			foreach( $cursor as $c ){
				if( isset( $c['META_DATA']['GetContentMetadataListResult']['CustomAttribute'] ) ){
					$meta_tags = $c['META_DATA']['GetContentMetadataListResult']['CustomAttribute'];
					foreach ( $meta_tags as $m ){
						if( $m['Name'] == "Content Brand - Fine Homebuilding" ){
							if( !isset( $brands[$m['Value']] ) ){
								$brands[$m['Value']] = 1;
							}else{
								++$brands[$m['Value']];
							}
						}
					}
				}
			}
		
		}
		return $brands;
		
	}
	
	
	
	public function find_issues(){
		$issues = array();
		foreach( $this->collections as $c ){
			$cursor = $this->db->selectCollection( $c )->find( array(), array("META_DATA.GetContentMetadataListResult.CustomAttribute" => -1 ) );
			foreach( $cursor as $c ){
				if( isset( $c['META_DATA']['GetContentMetadataListResult']['CustomAttribute'] ) ){
					$meta_tags = $c['META_DATA']['GetContentMetadataListResult']['CustomAttribute'];
					foreach ( $meta_tags as $m ){
						if( $m['Name'] == "Title/Issue" ){
							if( !isset( $issues[$m['Value']] ) ){
								$issues[$m['Value']] = 1;
							}else{
								++$issues[$m['Value']];
							}
						}
					}
				}
			}
		
		}
		return $issues;
	}
	
	public function find_keywords(){
		$keywords = array();
		foreach( $this->collections as $collection ){
			$cursor = $this->db->selectCollection( $collection  )->find( array(), array("Id" => -1, "Quicklink" => -1, "META_DATA.GetContentMetadataListResult.CustomAttribute" => -1 ) );
			foreach( $cursor as $c ){
				if( isset( $c['META_DATA']['GetContentMetadataListResult']['CustomAttribute'] ) ){
					$meta_tags = $c['META_DATA']['GetContentMetadataListResult']['CustomAttribute'];
					foreach ( $meta_tags as $m ){
						if( ($m['Name'] == "Content Access") && !empty( $m['Value'] )){
							if( !isset( $keywords[$m['Value']] ) ){
								
								$keywords[$m['Value']] = 1;
							}else{
								//print $m['Value']."\n";
								//print $collection."-->".$c['Id']."".$c['Quicklink']."\n";
								
								++$keywords[$m['Value']];
							}
						}
					}
				}
			}
	
		}
		return $keywords;
	}
	
	public function topic_report(){
		
		$topics = array();
		foreach( $this->collections as $c ){
			$cursor = $this->db->selectCollection( $c )->find( array(), array("META_DATA.GetContentMetadataListResult.CustomAttribute" => -1 ) );
			foreach( $cursor as $c ){
				if( isset( $c['META_DATA']['GetContentMetadataListResult']['CustomAttribute'] ) ){
					$meta_tags = $c['META_DATA']['GetContentMetadataListResult']['CustomAttribute'];
					foreach ( $meta_tags as $m ){
						if( $m['Name'] == "Taxonomy Terms" ){
							
							$ids = preg_split( "/;/", $m['Value'] );
							
							foreach( $ids as $id ){
								if( !isset( $topics[$id] ) ){
									$topics[$id] = 1;
								}else{
									++$topics[$id];
								}
							}
						}
					}
				}
			}
		
		}
				
		return $topics;
		
	}
	
	
	public function generate_artilce_topic_csv(){
		
		foreach( $this->collections as $c ){
			
			if( $c == 'author' ) continue;
			
			$cursor = $this->db->selectCollection( $c )->find( array( 'IsPublished' => "false" ) );//->limit(100);
			print "========================$c=================================\n";
			unlink( $c.".csv" );
			
			foreach( $cursor as $a ){						
				
				$data = array();
				$data[] = $a['Title'];
				$data[] = 'http://www.finehomebuilding.com'.$a['Quicklink'];
				$meta_tags = $a['META_DATA']['GetContentMetadataListResult']['CustomAttribute'];
				$meta_data = array();
				foreach ( $meta_tags as $m ){
					$meta_data[$m['Name']] = $m['Value']; 
				}
				
				if( $c == "tool_guide"){
					//print $meta_data['Primary Channel - Fine Woodworking']."\n";
					if( !empty( $meta_data['Primary Channel - Fine Woodworking'] ) ){
						continue;
					}
				}
				
				//print_R( $meta_data );	
				$data[] = $meta_data['Primary Channel - Fine Homebuilding'];
	//			$data[] = $meta_data['contenttype'];

				if( !empty( $meta_data['Taxonomy Terms'] ) ){
					$ids = preg_split( "/;/",  $meta_data['Taxonomy Terms'] );
					$topics = array();
					foreach( $ids as $id ){
						$t = $this->db->taxonomy->findOne(array( 'id' => $id ), array( 'title' => 1 ) );
						$topics[] = $t['title'];
					}
				//	$data[] = implode( ",", $topics );
				}else{
				//	$data[] = '';
				}
				$str = implode( "|", $data )."\n";
				file_put_contents( $c."-not-published.csv", $str, FILE_APPEND );
				
			}
			
		}
		
	}
	
	public function find_test_articles( $name ){
		
		$articles = array();
		
		foreach( $this->collections as $c ){
			
			$cursor = $this->db->selectCollection( $c )->find( array(), array( "Id" => -1, "META_DATA.GetContentMetadataListResult.CustomAttribute" => -1 ) )->limit(10);
			
			$articles[$c] = array();
			foreach( $cursor as $a ){
			
				if( isset( $a['META_DATA']['GetContentMetadataListResult']['CustomAttribute'] ) ){
					
					$meta_tags = $a['META_DATA']['GetContentMetadataListResult']['CustomAttribute'];
						
					foreach ( $meta_tags as $m ){
						if( ( $m['Name'] == $name ) && !empty( $m['Value'] ) ){
							print  $c."---------------->".$m['Value']."\n";
							$articles[$c][] = $a['Id'];
						}
	
					}
				}
				
			}
	
		}
		
		return $articles;
	}
	
	
	public function find_unpublished_articles(){
		
		$status_map =  array(
				'A' => 'Approved',
				'O' => 'Checked Out',
				'I' => 'Checked In',
				'S' => 'Submitted for Approval',
				'M' => 'Marked for Deletion',
				'P' => 'Pending Go Live Date',
				'T' => 'Awaiting Completion of Associated Tasks',
				'D' => 'Pending Deletion',				
		);
	
		$csv_file = 'ektron_not_published.csv';
		
		unlink( $csv_file );
		
		foreach( $this->collections as $c ){
				
			if( $c == 'author' ) continue;
				
			$cursor = $this->db->selectCollection( $c )->find( array( 'IsPublished' => "false" ) );
				
			foreach( $cursor as $a ){
	
				$data = array();
				$data[] = $a['Title'];
				$data[] = 'http://www.finehomebuilding.com'.$a['Quicklink'];
				
				$meta_tags = $a['META_DATA']['GetContentMetadataListResult']['CustomAttribute'];
				$meta_data = array();
				foreach ( $meta_tags as $m ){
					$meta_data[$m['Name']] = $m['Value'];
				}
	
				if( $c == "tool_guide"){
					if( !empty( $meta_data['Primary Channel - Fine Woodworking'] ) ){
						continue;
					}
				}
	
				$data[] = $a['DisplayDateCreated'];
				$data[] = $a['DisplayLastEditDate'];
				$data[] = $meta_data['First Published Date'];
				$data[] = $status_map[$a['Status']];
				$data[] = $meta_data['Primary Channel - Fine Homebuilding'];
				$data[] = $meta_data['contenttype'];
					
				$str = implode( "|", $data )."\n";
				file_put_contents( $csv_file, $str, FILE_APPEND );
	
			}
				
		}
	
	}
	
	public function find_checkout_articles(){
		

		$status_map =  array(
				'A' => 'Approved',
				'O' => 'Checked Out',
				'I' => 'Checked In',
				'S' => 'Submitted for Approval',
				'M' => 'Marked for Deletion',
				'P' => 'Pending Go Live Date',
				'T' => 'Awaiting Completion of Associated Tasks',
				'D' => 'Pending Deletion',
		);
		
		$csv_file = 'ektron_checkout_items.csv';
		
		unlink( $csv_file );
		
		foreach( $this->collections as $c ){
		
			if( $c == 'author' ) continue;
		
			$cursor = $this->db->selectCollection( $c )->find( array( 'Status' => "O" ) );
		
			foreach( $cursor as $a ){
		
				$data = array();
				$data[] = $a['Title'];
				$data[] = 'http://www.finehomebuilding.com'.$a['Quicklink'];
				$data[] = $a['EditorFirstName'];
				$data[] = $a['EditorLastName'];
				
				$meta_tags = $a['META_DATA']['GetContentMetadataListResult']['CustomAttribute'];
				$meta_data = array();
				foreach ( $meta_tags as $m ){
					$meta_data[$m['Name']] = $m['Value'];
				}
		
				if( $c == "tool_guide"){
					if( !empty( $meta_data['Primary Channel - Fine Woodworking'] ) ){
						continue;
					}
				}
		
				$data[] = $a['DisplayDateCreated'];
				$data[] = $a['DisplayLastEditDate'];
				$data[] = $meta_data['First Published Date'];
				$data[] = $status_map[$a['Status']];
				$data[] = $meta_data['Primary Channel - Fine Homebuilding'];
				$data[] = $meta_data['contenttype'];
					
				$str = implode( "|", $data )."\n";
				file_put_contents( $csv_file, $str, FILE_APPEND );
		
			}
		
		}
	}
	
}

?>