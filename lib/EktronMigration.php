<?php 

class EktronMigration {
	
	var $page_size = 100;
	
	var $service;
	
	var $collection;
	
	var $folder_count;
	
	function __construct(){
		
		$this->service = new EktronWebService();	
		
	}
	
	public function load_content( $module, $folder_id ){
		
		$this->collection = new $module;
		
		$this->process_pages( $folder_id );
		
		$this->process_folder( $folder_id );
		
	}
	
	public function load_fhb_authors( $module ){
		
		$this->collection = new $module;
		
		$this->collection->insert_fhb_author();
		
	}
	
	function process_folder( $parent_id ){
		
		$content_folders = array();
		
		$GetFolder = new GetFolder( $parent_id );
		
		try{
			$root_folder = $this->service->folder->GetFolder( $GetFolder );
		}catch(Exception $e ){
			print_R( $e );
		}
		if( empty($root_folder ) ) return;
		
		error_log( "Process Folder:" .$root_folder->GetFolderResult->Name );
				
		if( empty( $root_folder->GetFolderResult->ChildFolders  ) && ( $root_folder->GetFolderResult->Name == 'QA' ) ){
			return;
		}
		$content_folder = $root_folder->GetFolderResult->Name;
		
		$GetChildFolders_Obsolete = new GetChildFolders_Obsolete( $parent_id, true, 'Id' );
		
		try{
			$result = $this->service->folder->GetChildFolders( $GetChildFolders_Obsolete );
		}catch(Exception $e ){
			print_R( $e );
		}
		
		$folders = $result->GetChildFolders_ObsoleteResult->FolderData;
		
		if( is_array( $folders ) ){
			
			foreach( $folders as $folder ){
			
				error_log( "Process Folder:".$folder->Name );
				
				$item_folders = array();
				
				$item_folders[] = $content_folder;
				$item_folders[] = $folder->Name;
				
				$this->process_pages( $folder->Id,  $item_folders );
				
				if( $folder->ChildFolders ){
				
					$this->print_folder( $folder->ChildFolders, $t, $item_folders );
				
				}
			
			}
			
		}else{
			
			$item_folders[] = $content_folder;
			$item_folders[] = $folders->Name;
			
			$this->process_pages( $folders->Id, $item_folders  );
			
		}
		
	}
	
	function print_folder(&$folder, $t, $item_folders ){
	
		if( is_array( $folder->FolderData ) ){
	
			foreach( $folder->FolderData as $f ){
			
				print $t."Folder:".$f->Name."\n";
					
				// Folder ID KEY: Id
				/*
				 GET Folder Items
				 */
				$current_folder = $item_folders;
				
				$current_folder[] = $f->Name;
				
				$this->process_pages( $f->Id, $current_folder );
	
				if( $f->HasChildren ){
					
					$this->print_folder( $f->ChildFolders, $t."\t", $current_folder  );
					
				}
			}
		}else{
	
			if( property_exists( $folder, "FolderData") ) {
					
				$FolderData = $folder->FolderData;
					
				if( $FolderData && property_exists( $FolderData, "Name" )  ){
	
					print $t."Folder:".$FolderData->Name."\n";
					
					$current_folder = $item_folders;
					
					$current_folder[] = $FolderData->Name;
					
					$this->process_pages( $FolderData->Id, $current_folder );
					
				}else{
					
					print_R( $FolderData );
					die;
						
				}
					
			}else{
				print_R( $folder );
				die;
			}
		}
	}
	
	
	function print_folder_items( $folder_id, $t ) {
	
		$GetChildContent = new GetChildContent( $folder_id, false, 'id'  );
	
		$res = $this->service->content->GetChildContent( $GetChildContent );
	
		if( $res->GetChildContentResult ){
		
			$items = $res->GetChildContentResult->ContentData;
	
			$i = 1;
			foreach( $items as $item ){
								
				if( !empty( $item ) && property_exists( $item, "Title" )  ){
					print $i."----".$item->Id."---->Content:". $item->Title."\n";
					
					$GetContentMetadataList = new GetContentMetadataList( $item->Id );
				
					$metadata = $this->service->metadata->GetContentMetadataList( $GetContentMetadataList );
				
					$this->collection->save( $metadata, $item );
				}else{
					print_R( $item );
				}
				
				++$i;
			}
		
		}
				
	}
	
	
	
	
	function get_pages( $folder_id ){
	
		$pages = 0;
		
		$GetFolder = new GetFolder( $folder_id  );
	
		try{
			$result =  $this->service->folder->GetFolder( $GetFolder );
		}catch( Exception $e ){
			print "GetFolder: ".$e->getMessage()."\n";
			
		}
		if( $result ){
			
			$total = $result->GetFolderResult->TotalContent;
		
			$this->folder_count = $total;
			
			$pages = ceil($total/$this->page_size);
		
			$last_page = $total % $this->page_size;
			print "Total : $total, Pages:$pages \n";
		}

		return  $pages;
	}
		
	
	function process_pages( $folder_id, $folders = NULL ){
		
		$pages = $this->get_pages( $folder_id  );
		
		$i = 1;
		
		while( $i <= $pages  ){
			
						
			$this->get_page_items( $folder_id, $i, $folders );
			
			++$i;
			
		}
	}
		
	function get_page_items( $folder_id, $page, $folders ) {
		print "---------------------------------------------\n";
		print "get_page_items folder:$folder_id page:$page \n";
	
		/**
		 * @param int $RecordsPerPage
		 * @param int $CurrentPage
		 * @param int $TotalPages
		 * @param int $TotalRecords
		 * @access public
		 */
	
		$pagingInfo = new PagingInfo( $this->page_size, $page, 1, 100 );
		
		/**
		 * @param int $FolderID
		 * @param boolean $Recursive
		 * @param string $OrderBy
		 * @param PagingInfo $pagingInfo
		 * @access public
		 */
	
		$GetChildContentWithPaging = new GetChildContentWithPaging( $folder_id, true, 'id', $pagingInfo );
	
		$option = array('location' => "http://".__EKTRON__CMS_DOMAIN__."/Workarea/webservices/WebServiceAPI/Content/Content.asmx",
				'uri'      => "http://tempuri.org/",
				'trace' => 1,
		);
	
	
		$s = new EktronSoapClient( NULL, $option );
	
		$auth = new AuthenticationHeader( __EKTRON__USERNAME__, __EKTRON__PASSWORD__, __EKTRON__CMSURL__ );
	
		$RequestInfoParameters = new RequestInfoParameters( 1 );
	
	
		$headers[] = new SoapHeader("http://tempuri.org/",
				'AuthenticationHeader',
				$auth, false);
		/*
			$headers[] = new SoapHeader("http://tempuri.org/",
			'RequestInfoParameters',
			$RequestInfoParameters, false);
			
		*/

		$s->__setSoapHeaders($headers);
		
		
		
		try{
	
			$res = $s->__soapCall('GetChildContentWithPaging', array($GetChildContentWithPaging));
			
		}catch( Exception $e ){
			
			error_log( $e->getMessage() );
			
			error_log( "Response:\n" . $s->__getLastResponse() );
			
		}
		
		//echo "Response:\n" . $s->__getLastResponse() . "\n";
		
		//print_R( $res->ContentData );

		if( $res && $res->ContentData ){
	
			$items = $res->ContentData;
	
			if( is_array ($items ) ){
				foreach( $items as $item ){
		
					if( !empty( $item ) && property_exists( $item, "Title" )  ){
						
						error_log( $this->folder_count. " Content: ". $item->Id ."-------->".$item->Title );
		
						$GetContentMetadataList = new GetContentMetadataList( $item->Id );
						
						$metadata = array();
						try{
							$metadata = $this->service->metadata->GetContentMetadataList( $GetContentMetadataList );
						}catch( Exception $e ){
							print $e->getMessage()."\n";
						}
						
						$this->collection->save( $metadata, $item, $folders );
						
					}else{
						print_R( $item );
					}
					
					--$this->folder_count;
				}
			}else{
				
				$item = $res->ContentData;
				error_log( $this->folder_count. " Content: ". $item->Id ."-------->".$item->Title );
				$GetContentMetadataList = new GetContentMetadataList( $item->Id );
				
				$metadata = array();
				try{
					$metadata = $this->service->metadata->GetContentMetadataList( $GetContentMetadataList );
				}catch( Exception $e ){
					print $e->getMessage()."\n";
				}
				
				$this->collection->save( $metadata, $item, $folders );
				
				--$this->folder_count;
			}
		}
	
	}
	
	
	public function get_content_by_id( $content_id ){
		
		$GetContent = new GetContent( $content_id, 'Published'  );
		
		try{
			
			$result =  $this->service->content->GetContent( $GetContent );
			
			//print_R( $result );
			
			$GetContentMetadataList = new GetContentMetadataList( $content_id );
			
			$metadata = $this->service->metadata->GetContentMetadataList( $GetContentMetadataList );
			
			print_R( $metadata );
			
		}catch( Exception $e ){
			
			print "GetContent: ".$e->getMessage()."\n";
				
		}
		
	}
}

?>