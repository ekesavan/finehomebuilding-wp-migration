<?php 

class FHBMongo {
		
		private $manager;
		
		private $database;
	
		function __construct( $db ){
			/*
			$this->manager = new MongoDB\Driver\Manager("mongodb://localhost:27017");
			*/
			error_log( "connect to mongo :".__MONGO__DBHOST__ );
			$this->manager = new MongoDB\Driver\Manager("mongodb://".__MONGO__DBHOST__.":27017");
			//error_log( print_R( $this->manager, true ) );
			$this->database = $db;
		}
	
		public function drop( $collection){
			try{
				$this->manager->executeCommand( $this->database, new \MongoDB\Driver\Command(["drop" => $collection ]));
			}catch ( Exception $e ){
				print_R( $e );
			}
		}
		
		public function create_collection( $collection){
			try{
				$this->manager->executeCommand( $this->database, new \MongoDB\Driver\Command(["create" => $collection ]));
			}catch ( Exception $e ){
				print_R( $e );
			}
		}
		
		public function insert( $collection, $data ){

			$bulk = new MongoDB\Driver\BulkWrite(['ordered' => true]);
			
			try{
				$bulk->insert( $data );
			}catch( Exception $e ){
				print $e->getMessage()."\n";
			}
			
			
			
			$writeConcern = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);
			try{
				$result = $this->manager->executeBulkWrite($this->database .".". $collection, $bulk, $writeConcern);
			}catch( Exception $e ){
				print $e->getMessage()."\n";
			}
		}
		
		public function find( $collection , $query = array(), $option = array() ){
			
			$q = new MongoDB\Driver\Query( $query, $option );
			
			//print_R( $q );
			
			$cursor = $this->manager->executeQuery( $this->database .".". $collection, $q);
		
			return  $cursor->toArray();
		}
		
		public function findOne($collection , $query ){
			
			$query = new MongoDB\Driver\Query( $query );

			$cursor = $this->manager->executeQuery ($this->database .".". $collection, $query);

			$rows = (array) $cursor->toArray()[0];

			return $rows;
			
		}
		
		public function update(  $collection , $filter,  $data  ){
			
			$bulk = new MongoDB\Driver\BulkWrite(['ordered' => true]);
			
			$bulk->update( $filter, $data );
			
			$writeConcern = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);
			
			$result = $this->manager->executeBulkWrite($this->database .".". $collection, $bulk, $writeConcern);
				
		}
		
		public function delete($collection , $filter ){
			
			$bulk = new MongoDB\Driver\BulkWrite(['ordered' => true]);
				
			$bulk->delete( $filter );
				
			$writeConcern = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);
				
			$result = $this->manager->executeBulkWrite($this->database .".". $collection, $bulk, $writeConcern);
		}
		
		
		public function count( $collection, $query = array(), $option = array() ){
			
			$q = new MongoDB\Driver\Query( $query, $option );
			
			$cursor = $this->manager->executeQuery( $this->database .".". $collection, $q);
			
			$count = count($cursor->toArray());
			
			return $count;
			
		}
		
		public function create_index( $collection, $key ){ 
		
			$command = new MongoDB\Driver\Command([
					"createIndexes" => $collection,
					"indexes"       => [[
							"name" => $key,
							"key"  => [ $key => 1],
							"ns"   => $this->database .".". $collection,
					]],
			]);
			
			$result = $this->manager->executeCommand( $this->database, $command );
		}		
		
		public function get_collections() {
			
			$listcollections = new MongoDB\Driver\Command(["listCollections" => 1]);
			$result          = $this->manager->executeCommand( $this->database, $listcollections );
			
			/* The command returns a cursor, which we can iterate on to access
			 * information for each collection. */
			$collections     = $result->toArray();
			
			foreach ($collections as $collection) {
				echo "\t * ", $collection->name, "\n";
			}
			
			return $collections;
			
			
		}
}