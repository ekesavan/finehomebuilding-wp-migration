<?php 

class WPProduct extends WPContent{

	
	function __construct(){
				
		parent::__construct();
	}
	
	public function insert_post( $a ){
		
		
		$post = array();
		
		$metas = $a['META_DATA']['GetContentMetadataListResult']['CustomAttribute'];
		
		$meta_data = array();
		
		foreach( $metas as $meta ){
			$meta_data[$meta['Name']] = $meta['Value'];
		}
		
		if( !empty( $meta_data['Primary Channel - Fine Woodworking'] ) ){
			print "Skip...............\n";
			return;
		}
		
		if( !empty( $meta_data['title'] ) ){
			$post['post_title'] = $meta_data['title'];
		}else{
			$post['post_title'] = $a['Title'];
		}
		
		$post['post_name'] = sanitize_title( $a['Title'] );
		
		// set post status 
		if( $a["IsPublished"] == "true" ){
			$post['post_status'] = 'publish';
		}else{
			$post['post_status'] = 'draft';
		}
		
		$post['post_type'] = 'product';
		
		$post['post_date'] = $this->post_date( $a['DisplayDateCreated'] );
		
		$post['post_date_gmt'] = $this->post_date_gmt( $a['DisplayDateCreated'] );
		
		// set content
		list( $content, $inline_images ) = $this->clean_content( $a['Html'] );
		
		//print $content."\n\n";
		
		$product_specification = $this->product_specification( $a['Html'] );
					
		$specification_data = array();
		//print_R( $product_specification );
		foreach( $product_specification as $k => $v ){
			if( is_array( $v ) ){
				foreach( $v as $s ){
					
					if( is_array( $s ) ){
						$spec_row = array_values($s);
						
						$specification_data[] = array(
								'label' => $spec_row[0],
								'value' => $spec_row[1],
						);
					}else{
						$spec_row = array_values($v);
						
						$specification_data[] = array(
								'label' => $spec_row[0],
								'value' => $spec_row[1],
						);
					}
				}
				
			}
		}
		
		$post['post_content'] = '';
		
		//Set Excerpt / Abstract
		$post['post_excerpt'] = $this->post_excerpt( $a['Html'] );
		
		
		$tags = array();
		$category_ids = array();
		
		// category
		if( !empty( $meta_data['Taxonomy Terms'] ) ){
			list( $category_ids, $tags ) =  $this->post_category( $meta_data['Taxonomy Terms'] );
	
			if( !empty( $category_ids ) ){
				$post['post_category'] = $category_ids;
			}
		}
		
		// set tags		
		$post['tags_input'] = $this->tags_input( $tags, @$a['Keywords'] );		
				
		$channel = 'ToolGuide';
		
		$wp_error = '';
				
		$history_link = "http://www.finehomebuilding.com".$a['Quicklink'];
		
		print $a['Id'].":".$history_link."\n";
							
		$posts = get_posts(array(
				'numberposts'	=> -1,
				'post_type'		=> 'product',
				'meta_key'		=> 'fhb_history_link',
				'meta_value'	=> $history_link
		));
		
		$post_id = 0;
		if( !empty( $posts ) ){
			$post_id = $posts[0]->ID;
		}

		if( !empty( $post_id ) ){
			
			print "Update Post=$post_id--->".$post['post_title']."\n";
			// update post
			$post['ID'] = $post_id;
			wp_update_post( $post );
			
		}else{
			
			// create post
			$post_id = wp_insert_post( $post, $wp_error );
			print "Insert Post=$post_id--->".$post['post_title']."\n";
		}
		
	
		/*
		 * Set Mmain and Thumbnail Image
		 */
		$main_image_id = $this->main_image( $a['Html'], $post_id );
		if( !empty( $main_image_id ) ){
				
			$this->set_fhb_main_image( $main_image_id, $post_id );
			
		}
		
		
		/*
		 * Set Deck / SubHead
		 */
		$deck = $this->post_deck( $a['Html'] );
		if( !empty( $deck ) ){
		
			$this->set_fhb_deck( $deck, $post_id );
		
		}
		
		
		// set specification
		update_field( 'field_56840e65c742d', $specification_data, $post_id );
		
		// set anufacturer
		$manufacturer = $meta_data['Manufacturer'];
		if( !empty ( $manufacturer ) ){
			$m = $this->db->manufacturers->findOne( array( 'Id' => $manufacturer ));
			if( !empty( $m ) ){
				update_field( 'field_567b11cacd436', $m['wp_post_id'], $post_id );
			}
		}
		
		
		// set channel
		$this->wp_term->set_channel( $post_id, $channel );
		
		// set section
		$this->wp_term->set_section( $post_id ,'Default' );
		
		/*
		 * Set Author
		 */
		if( !empty( $meta_data['Author'] ) ){
		
			$post_authors = $this->post_author( $meta_data['Author'] );
		
			$this->set_fhb_author( $post_authors, $post_id );
		}
		
		
		/*
		 * Set History Link
		 */
		$this->set_fhb_history_link( $history_link, $post_id );
		
		
		/* [Average Price] => 1300 */
		update_field( 'field_567b119400e47', $meta_data['Average Price'], $post_id );
				
		/* [Product Availability] => Available */
		update_field( 'field_567b11ecc99c5', $meta_data['Product Availability'], $post_id );
		
		/* [Model Number] => JSS-MCA */
		update_field( 'field_567b1203b1826', $meta_data['Model Number'], $post_id );
		
		/* [Amazon ID - ASIN] => B00SAHUO38 */
		update_field( 'field_567b121c277fe', $meta_data['Amazon ID - ASIN'], $post_id );
		
		/* [CMS5 Content ID] => */
		update_field( 'field_567b1235596b7', $meta_data['CMS5 Content ID'], $post_id );
		
		/* [SKU] => */
		update_field( 'field_567b1248f6df4', $meta_data['SKU'], $post_id );
				
		
		/*
		 * Update Related Items
		 */
		if( __WP_UPDATE__ ){
			
			
			if( !empty( $meta_data["Companion Content"] ) ){
				$this->set_companion_content( $meta_data["Companion Content"], $post_id );
			}
			
			if( !empty( $meta_data["Associated Products"] ) ){
				$this->set_associated_products( $meta_data["Associated Products"], $post_id );
			}
			
			
			if( !empty( $meta_data["Taunton Product Attribution"] ) ){
				$this->taunton_product_attribution( $meta_data["Taunton Product Attribution"], $post_id );
			}
			
			
		}
		
		return $post_id;
	}
		
	/*
	 <Specifications>
	 <Specs>
	 <Generic_Manufacturer_Specs>Blade Style</Generic_Manufacturer_Specs>
	 <Generic_Manufacturer_Specs_Value>Pin</Generic_Manufacturer_Specs_Value>
	 </Specs>
	 <Specs>
	 <Generic_Manufacturer_Specs>Maximum Marking Reach</Generic_Manufacturer_Specs>
	 <Generic_Manufacturer_Specs_Value>N/A</Generic_Manufacturer_Specs_Value>
	 </Specs>
	 </Specifications>
	 
	 <Specifications>
	 <Materials>
	 <Manufacturer_Specs_Materials>All 1*Weight</Manufacturer_Specs_Materials>
	<Manufacturer_Specs_Materials_Value>N/A</Manufacturer_Specs_Materials_Value>
	</Materials>
	<Materials>
	<Manufacturer_Specs_Materials>All 2*Dimensions</Manufacturer_Specs_Materials>
	<Manufacturer_Specs_Materials_Value>1/2 in., 5/8 in., 3/4 in., and 1-in. thick</Manufacturer_Specs_Materials_Value>
	</Materials>
	</Specifications>

	*/
	
	public function product_specification( $html ){
						
		$content = '';
		
		$dom = str_get_html( $html  );
		
		$xml = simplexml_load_string($dom->find( "Specifications",0)->outertext);
		$json = json_encode( $xml );
		$data = json_decode( $json, TRUE );
		
		print_R( $data ); 
		
		$dom->clear();
		unset( $dom );
	
		return $data;
		
	}
}

?>

