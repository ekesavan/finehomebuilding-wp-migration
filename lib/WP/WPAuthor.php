<?php 

class WPAuthor extends WPContent{

	
	
	function __construct(){

		parent::__construct();
		
	}

	/*
	 * EKTROM CMS Author
	  "_id" : ObjectId("5699deefbe6dfae2038b6cbc"),
        "Id" : "65612",
        "Title" : "James Cameron",
        "Quicklink" : "/authors/profile.aspx?id=65612",
        "DisplayDateCreated" : "12/20/2006 03:56:55 PM",
        "authorname" : "James Cameron",
        "first_name" : "",
        "middle_name" : "",
        "last_name" : "",
        "suffix_name" : "",
        "address1" : "",
        "address2" : "",
        "city" : "",
        "state" : "(Select)",
        "zip" : "",
        "province" : "",
        "country" : "us",
        "websiteurl" : "",
        "emailaddress" : "",
        "full_bio" : "",
        "short_bio" : "",
        "specialties" : "",
        "article_image" : "",
        "click_action" : "None",
        "destination_url" : "",
        "image_credit" : "",
        "image_caption" : "",
        "expertforumteaser" : "",
        "forumid" : "",
        "folderid" : "",
        "wp_user_id" : NumberLong(85)
	*/

	public function insert_ektron_author( $a ){
		
		$author = array();
		
		$author['title'] = $a['Title'];
		
		list( $fn, $ln ) = explode ( " ", $a['Title'], 2 );
			
		if( empty( $a['first_name'] ) ){
			$author['first_name'] = $fn;
		}else{
			$author['first_name'] = $a['first_name'];
		}
			
		if( empty( $a['last_name'] ) ){
			$author['last_name'] = $ln;
		}else{
			$author['last_name'] = $a['last_name'];
		}
			
			
		if( !empty( $a['emailaddress'] ) ){
			$author['user_email'] = strtolower( $a['emailaddress'] );
		}
		
		$author['bio'] = $a['full_bio'];

		$author['websiteurl'] = $a['websiteurl'];
		
		$author['created'] = $a['DisplayDateCreated'];
		
		$author['history_link'] = "http://www.finehomebuilding.com".$a['Quicklink'];
		
		if( !empty( $a['article_image'] ) ){
			
			$author['image'] = array(
					'image_src' => $a['article_image'],
					"image_credit" => $a['image_credit'],
					"image_caption" => $a['image_caption'],
					"click_action" => $a['click_action'],
					"destination_url" => $a['destination_url'],
			);
			
		}
		
		$wp_author_id = $this->insert_author( $author );
		
		return $wp_author_id;
		
	}
	
	/*
	 * CodeIgniter CMS Author
	 *  [user_first_name] => Ron
    [user_last_name] => Brenner
    [user_email] => ron@simplyeleganthomedesigns.com
    [user_url] => www.simplyeleganthomedesigns.com
    [user_registered] => 2009-03-03 20:12:42
    [user_status] => 1
    [display_name] => ronlbrenner
    [user_title] => Blogger
    [users_profile] => Array
        (
            [_id] => MongoId Object
                (
                    [$id] => 56a02218be6dfa826c9d8175
                )

            [user_id] => 3450
            [user_profile_id] => 12446
            [profile_image] => Ron-Websquare.jpg => http://www.finehomebuilding.com/assets/avatars/Ron-Websquare.jpg
            [profile_city] => Marine on St.Croix
            [profile_state] => MN
            [profile_country] => us
            [profile_bio] => I am 
            
            guid: http://www.finehomebuilding.com/profile/ronlbrenner
            
	 */
	
	public function insert_codeigniter_author( $a ){
		
		print_R( $a );
		
		$author = array();
		
		$author['title'] = $a['user_first_name']." ". $a['user_last_name'];		

		$author['first_name'] = $a['user_first_name'];

		$author['last_name'] = $a['user_last_name'];
		
		$author['history_link'] = "http://www.finehomebuilding.com/profile/".$a['display_name'];
		
		
		$ektron_author = $this->db->findOne( 'fhb_author', array( 'Title' => $a['user_first_name']." ".$a['user_last_name'] )	);
		
		if( empty( $ektron_author ) ){
			$ektron_author = $this->db->findOne( 'fhb_author',
					array( 'first_name' => $a['user_first_name'] , 'last_name' =>  $a['user_first_name'] )
					);
		}
		if( !empty( $ektron_author )){
			
			error_log( "Find  Ektron Author :". $ektron_author['Title']);
			
			$posts = get_posts(array(
					'numberposts'	=> -1,
					'post_type'		=> 'author',
					'meta_key'		=> 'history_link',
					'meta_value'	=> $author['history_link']
			));
			
			$post_id = 0;
			if( !empty( $posts ) ){
				$post_id = $posts[0]->ID;
				error_log( "Delete Author Post :$post_id ");
				wp_delete_post( $post_id, true );
			}
			
			return $ektron_author['wp_user_id'];
		}
		
		
		$users_profile = $a['users_profile'];
		
			
		if( !empty( $a['user_email'] ) ){
			$author['user_email'] = strtolower( $a['user_email'] );
		}
		
		if( !empty( $a['user_url'] ) ){
			$author['websiteurl'] = $a['user_url'];
		}
		
		if( !empty(  $a['user_registered'] ) ){
			$author['created'] = $a['user_registered'];
		}
		
		
		
		if( !empty( $users_profile['profile_bio'] ) ){
			$author['bio'] = $users_profile['profile_bio'];
		}
		
		if( !empty( $users_profile['profile_image'] ) && ( $users_profile['profile_image']  != "default_avatar.jpg" ) ){
			
			$profile_image = $users_profile['profile_image'];
				
			$pathinfo = pathinfo( $profile_image );
			$image_src = "/assets/avatars/".$pathinfo['filename']."_sqm.".$pathinfo['extension'];
			
			$author['image'] = array(
					'image_src' => $image_src,
			);
		}
		
		/*
		 * Author Profile Data
		 */
		
		$wp_author_id = $this->insert_author( $author );
		
		return $wp_author_id;
		
	}
	
	
	private function insert_author( $author ){
		
		unset( $_POST['acf_nonce'] );
		
		$post = array();
		$post['ping_status'] = 'closed';
		$post['comment_status'] = 'closed';
		
		$post['post_title'] = $author['title'];
		
		$post['post_name'] = sanitize_title( $author['title'] );
		
		$post['post_status'] = 'publish';
		
		$post['post_type'] = 'author';
		
		$post['post_date'] = $this->post_date( $author['created'] );
		
		$post['post_date_gmt'] = $this->post_date_gmt( $author['created'] );
					
		$wp_error = '';
														
		$posts = get_posts(array(
				'numberposts'	=> -1,
				'post_type'		=> 'author',
				'meta_key'		=> 'history_link',
				'meta_value'	=> $author['history_link']
		));
		
		$post_id = 0;
		if( !empty( $posts ) ){
			$post_id = $posts[0]->ID;
		}

		if( !empty( $post_id ) ){
			
			print "Update Post=$post_id--->".$post['post_title']."\n";
			// update post
			$post['ID'] = $post_id;
			wp_update_post( $post );
			
		}else{
			
			// create post
			$post_id = wp_insert_post( $post, $wp_error );
			print "Insert Post=$post_id--->".$post['post_title']."\n";
		}
			
		//first name
		if( !empty( $author['first_name']) ){
			update_field( 'field_56980c4b06827', $author['first_name'], $post_id );
		}

		//last name
		if( !empty( $author['last_name']) ){
			update_field( 'field_56980c5ecd52c', $author['last_name'], $post_id );
		}
		
		//email
		if( !empty( $author['user_email']) ){
			update_field( 'field_56980c6a75c8a', $author['user_email'], $post_id );
		}
		
		//websiteurl
		if( !empty( $author['websiteurl']) ){
			update_field( 'field_56980c7e6dc69', $author['websiteurl'], $post_id );
		}
		
		//bio
		if( !empty( $author['bio']) ){
			update_field( 'field_56980c95f20d8', $author['bio'], $post_id );
		}
		
		update_field( 'field_5699e729e4f6a', $author['history_link'], $post_id );
		
		
		if( !empty( $author['image']  ) ){
			
			$attachment_id = $this->attachment->add_author_attachment(  $author['image'], $post_id );
			
			error_log( "--------add_author_attachment: $post_id, $attachment_id  ");
			
			update_field( 'field_56edc3ee28e46', $attachment_id, $post_id );
			
			
			//update_post_meta( $post_id, '_thumbnail_id', $attachment_id );
			/*
			if( set_post_thumbnail( $post_id, $attachment_id ) ){
				error_log( "--------set_post_thumbnail: $post_id, $attachment_id : SUCCESS ");
			}else{
				error_log( "--------set_post_thumbnail: $post_id, $attachment_id : FAILURE ");
			}
			*/
		
		}
		
		return $post_id;
		
	}
	
	
	/*
	 <root>\n
	 <Phone_Number>877-267-2499</Phone_Number>\n
	 <Web_Address>www.boschtools.com</Web_Address>\n
	 <Street_Address />\n<City />\n<State />\n<Country>us</Country>\n<Zip />\n
	 <Advertiser_ID>112302163</Advertiser_ID>\n
	 <Logo></Logo>\n</root>
	 
	 <img src="/CMS/uploadedimages/Images/Gardening/Web_Only/AMIShieldLogo.jpg" alt="American Meadows" title="American Meadows" />
	 
	 */
	
	public function clean_content( $html ){
		
		$data = array();
		
		$dom = str_get_html( $html  );
		
		$data['Phone_Number'] = $dom->find( 'Phone_Number', 0 )->innertext;
		$data['Web_Address'] = $dom->find( 'Web_Address', 0 )->innertext;
		$data['Street_Address'] = $dom->find( 'Street_Address', 0 )->innertext;
		$data['City'] = $dom->find( 'City', 0 )->innertext;
		$data['State'] = $dom->find( 'State', 0 )->innertext;
		$data['Country'] = $dom->find( 'Country', 0 )->innertext;
		$data['Advertiser_ID'] = $dom->find( 'Advertiser_ID', 0 )->innertext;
		
		if( $dom->find( 'Logo', 0 ) && $dom->find( 'Logo', 0 )->find('img',0 ) ){
			$data['Logo'] = $dom->find( 'Logo', 0 )->find('img',0 )->src;
		}
		
		$dom->clear();
		unset( $dom );
		return $data;
		
	}
	

}

?>

