<?php 

/*
 <?php wp_set_object_terms( $object_id, $terms, $taxonomy, $append ); ?> 

Parameters
$object_id : (int) (required) The object to relate to, such as post ID.
Default: None
$terms : (array/int/string) (required) The slug or id of the term (such as category or tag IDs), will replace all existing related terms in this taxonomy. To clear or remove all terms from an object, pass an empty string or NULL. Integers are interpreted as tag IDs. Warning: some functions may return term_ids as strings which will be interpreted as slugs consisting of numeric characters!
Default: None
$taxonomy : (array/string) (required) The context in which to relate the term to the object. This can be category, post_tag, or the name of another taxonomy.
Default: None
$append : (bool) (optional) If true, tags will be appended to the object. If false, tags will replace existing tags
Default: False

*/

class WPTerm {
	
	public function set_channel( $object_id, $channel ){
		
		if( $channel == "ToolGuide" ){
			$channel = "Tools & Materials";
		}
		if( $channel == "HowTo" ){
			$channel = "How-To";
		}
		
		wp_set_object_terms( $object_id, $channel, 'channel', false );
		
	}
	
	public function set_section(  $object_id, $section ){
	
		wp_set_object_terms( $object_id, array( $section ) , 'section', false );
	}
	
	public function set_topics( $object_id, $category_ids){
		
		wp_set_object_terms( $object_id, array( $category_ids ) , 'category', false );
		
	}
	
	public function set_tags(){
		
		wp_set_object_terms( $object_id, array( $tags ) , 'post_tag', false );
	
	}
	
}

?>