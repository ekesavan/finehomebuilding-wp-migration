<?php 
/*
  wp_insert_post( $post, $wp_error ); 
  
  $post = array(
  'ID'             => [ <post id> ] // Are you updating an existing post?
  'post_content'   => [ <string> ] // The full text of the post.
  'post_name'      => [ <string> ] // The name (slug) for your post
  'post_title'     => [ <string> ] // The title of your post.
  'post_status'    => [ 'draft' | 'publish' | 'pending'| 'future' | 'private' | custom registered status ] // Default 'draft'.
  'post_type'      => [ 'post' | 'page' | 'link' | 'nav_menu_item' | custom post type ] // Default 'post'.
  'post_author'    => [ <user ID> ] // The user ID number of the author. Default is the current user ID.
  'ping_status'    => [ 'closed' | 'open' ] // Pingbacks or trackbacks allowed. Default is the option 'default_ping_status'.
  'post_parent'    => [ <post ID> ] // Sets the parent of the new post, if any. Default 0.
  'menu_order'     => [ <order> ] // If new post is a page, sets the order in which it should appear in supported menus. Default 0.
  'to_ping'        => // Space or carriage return-separated list of URLs to ping. Default empty string.
  'pinged'         => // Space or carriage return-separated list of URLs that have been pinged. Default empty string.
  'post_password'  => [ <string> ] // Password for post, if any. Default empty string.
  'guid'           => // Skip this and let Wordpress handle it, usually.
  'post_content_filtered' => // Skip this and let Wordpress handle it, usually.
  'post_excerpt'   => [ <string> ] // For all your post excerpt needs.
  'post_date'      => [ Y-m-d H:i:s ] // The time post was made.
  'post_date_gmt'  => [ Y-m-d H:i:s ] // The time post was made, in GMT.
  'comment_status' => [ 'closed' | 'open' ] // Default is the option 'default_comment_status', or 'closed'.
  'post_category'  => [ array(<category id>, ...) ] // Default empty.
  'tags_input'     => [ '<tag>, <tag>, ...' | array ] // Default empty.
  'tax_input'      => [ array( <taxonomy> => <array | string>, <taxonomy_other> => <array | string> ) ] // For custom taxonomies. Default empty.
  'page_template'  => [ <string> ] // Requires name of template file, eg template.php. Default empty.
);  

	Ektron Meta
	
	[Blog Tags] =>
    [Blog Categories] =>
    [Blog TrackBack] =>
    [Blog Pingback] => No
    [Publication] => Fine Homebuilding
    [contenttype] => Article
    [title] => Hebel block
    [Keywords] =>
    [Content Access] => Free Content
    [Taxonomy Terms] => 61524;61670;65784;61942
    [Title/Issue] => 123
    [Page Number] => 82-87
    [Author] => 65612;65614
    [interestgroup] => fh
    [Exclude From Search] => No
    [Companion Content] =>
    [First Published Date] => 12/20/2006
    [Content Brand - Fine Homebuilding] =>
    [Taunton Product Attribution] =>
    [Pagination] => No
    [Page Count] =>
    [Region] =>
    [Location] =>
    [EditorsPick] => No
    [Associated Products] =>
    [Primary Channel - Fine Homebuilding] => HowTo


*/

class WPArticle extends WPContent{

	
	function __construct(){
				
		parent::__construct();
	}
	
	public function insert_article( $a, $section ){
		
		$post = array();
		
		$post['ping_status'] = 'closed';
		$post['comment_status'] = 'open';
		
		$post_id = 0;
		
		$history_link = "http://www.finehomebuilding.com".$a['Quicklink'];
		error_log( "FHB ID=".$a['Id']." LINK : $history_link");
		
		if( empty( $a['wp_post_id'] ) ) {
			$posts = get_posts(array(
					'numberposts'	=> 1,
					'post_type'		=> 'post',
					'meta_key'		=> 'fhb_history_link',
					'meta_value'	=> $history_link
			));
			
			$post_id = 0;
			if( !empty( $posts ) ){
				$post_id = $posts[0]->ID;
			}
		}else{
			$post_id = $a['wp_post_id'];
		}
		
		error_log( '-------------------------------start----------------------------------------');
		
		$post['post_title'] = $a['Title'];
		
		$post['post_name'] = sanitize_title( $a['Title'] );
		
		// set post status 
		if( $a["IsPublished"] == "true" ){
			
			$post['post_status'] = 'publish';
			
		}else{
			
			$post['post_status'] = 'draft';
		
		}
		
		$post['post_type'] = 'post';
						
		$meta_data = array();
		$metas = $a['META_DATA']['GetContentMetadataListResult']['CustomAttribute'];
		foreach( $metas as $meta ){
			$meta_data[$meta['Name']] = $meta['Value'];
		}
		
		if( __LOG__ )
			print_R( $meta_data );
		
		// published date
		$published_date = '';
		
		if( !empty(  $meta_data['First Published Date'] ) ){
			$published_date = $meta_data['First Published Date'];
		}else{
			$published_date = $a['DisplayDateCreated'];
		}
		
		$post['post_date'] = $this->post_date( $published_date );
		
		$post['post_date_gmt'] = $this->post_date_gmt( $published_date );
		
		
		if( $meta_data['contenttype'] == 'Slideshow' ){
			
			//set content
			list( $content, $slides ) = $this->slideshow_content( $a['Html'] );
				
			
		}else{
			
			//set content
			list( $content, $inline_images, $slides ) = $this->clean_content( $a['Html'] );
		}
		
		
			
		//Set Excerpt 
		$post['post_excerpt'] = $this->post_excerpt(  $a['Html'] );
						
		
		$post['post_content'] = $content;
		
		
		if( empty( trim($content) )){
			
			list( $content, $inline_images ) = $this->abstract_clean_content( $a['Html']  );
			$post['post_content'] = $content;
		}
		
		$tags = array();
		$category_ids = array();
		
		// category
		if( !empty( $meta_data['Taxonomy Terms'] ) ){
			list( $category_ids, $tags ) =  $this->post_category( $meta_data['Taxonomy Terms'] );
	
			if( !empty( $category_ids ) ){
				$post['post_category'] = $category_ids;
			}
		}
		
		// set tags		
		$post['tags_input'] = $this->tags_input( $tags, @$a['Keywords'] );	
		
		if( $section == 'Book Excerpt' ){
			$post['tags_input'][] = 'Book Excerpt';
			$section = 'Default';
		}
			
		if( $section == 'Book Contests' ){
			$post['tags_input'][] = 'Book Contests';
			$section = 'Default';
		}
		
		if( $section == 'Question Answer' ){
			$post['tags_input'][] = 'Question Answer';
			$section = 'Default';
		}
		
		if( $section == 'Readers Tips' ){
			$post['tags_input'][] = 'Reader Tips';
			$section = 'Default';
		}
		
		if( !empty( $meta_data["Title/Issue"] ) && ($section != "Video") ){
			$section = 'Magazine';
		}
		
		if( !empty($slides) ){
			$section = 'Slide Show';
		}
		
		$wp_error = '';		
		
		$post['tags_input'] = array_unique( $post['tags_input'] );
				
		if( !empty( $post_id ) ){

			$post['ID'] = $post_id;
			
			wp_update_post( $post );
			error_log( '-------------------------------end----------------------------------------');
			
		}else{
		
			$post_id = wp_insert_post( $post, $wp_error );
		
		}
		
		/*
		 * Set History Link
		 */
		$this->set_fhb_history_link( $history_link, $post_id );
		
		
		/*
		* Set Channel
		* Change ToolGuide => Tools & Materials
		*/
		$channel = $meta_data['Primary Channel - Fine Homebuilding'];
		$this->wp_term->set_channel( $post_id, $channel );
				
		
		/*
		 * Set Section
		 */
		$this->wp_term->set_section( $post_id , $section );
		
		
		/*
		 * Set Author
		 */
		if( !empty( $meta_data['Author'] ) ){
		
			$post_authors = $this->post_author( $meta_data['Author'] );
		
			$this->set_fhb_author( $post_authors, $post_id );
		}
		
		/*
		 * Add slides
		 */
		if( !empty( $slides ) ){
			
			$field_key = "field_56a96b47b244c";
					
			$slide_data =  array();
			
			foreach( $slides as $slide ){
			
				$s = array();
				if( !empty( $slide['image']  ) ){
					$slide_attachment_id = !empty( $slide['image'] ) ? $this->attachment->add_attachment(  array( 'image_src' => $slide['image'] )  , $post_id  ) : '';
					$s['slide_image'] = $slide_attachment_id;
				}else{
					continue;		
				}
				
				if( !empty( $slide['title']  ) ){
					$s['slide_title'] = $slide['title'];
				}
				if( !empty( $slide['description']  ) ){
					$s['slide_description'] = $slide['description'];
				}
				if( !empty( $slide['link']  ) ){
					$s['slide_link'] = $slide['link'];
				}
								
				$slide_data[] = $s;
				
			}
			
			//error_log( print_R( $slide_data, true ) );
			
			update_field( $field_key , $slide_data,  $post_id );
			
		}
		
		/*
		 * Add Image Attachments
		 */
		if( !empty( $inline_images ) ){
			foreach( $inline_images as $inline_image ){
				$this->attachment->add_attachment(  $inline_image, $post_id  );
			}
		}
		
		/*
		 * Set Main and Thumbnail Image
		 */
		$attachment_ids = array();
		
		$attachment_ids = $this->main_image( $a['Html'], $post_id );
		
		if( empty ($attachment_ids ) ){
			error_log("---------------------------abstarct_main_image---------------------------");
			$attachment_ids = $this->abstarct_main_image( $a['Html'], $post_id );
		}
		
		if( !empty( $attachment_ids ) ){
			
			$set_main_image = (count($inline_images) <= 1 ) ? true : false; 	
			
			$this->set_fhb_main_image( $attachment_ids, $post_id, $set_main_image );
			
		}else{
			$this->remove_fhb_main_image( $post_id );
		}
		
		/*
		 * Set Deck / SubHead
		 */
		$deck = $this->post_deck( $a['Html'] );
		if( !empty( $deck ) ){
		
			$this->set_fhb_deck( $deck, $post_id );
		
		}
		
		// set pdf link
		$pdf_attachement_id = $this->alternate_formats( $a['Html'],$post_id );
		if( !empty( $pdf_attachement_id ) ){
			$this->set_pdf_attachment( $pdf_attachement_id, $post_id );
		}
		
		/*
		 "Name" : "Content Access",
		 [Free Content] => 6154
		 [Subscription Required] => 1992
		 
		*/
		$this->set_content_access( $meta_data['Content Access'], $post_id );
		
		/*
		 "Name" : "Title/Issue",
		 "Value" : "123",
		*/
		/*
		if( !empty( "Title/Issue" ) ){
			$this->set_title_issue( $meta_data["Title/Issue"], $post_id );
		}
		*/
		/*
		 "Name" : "Page Number",
		 "Value" : "82-87",
		*/
		/*
		if( !empty( $meta_data["Page Number"] ) ){
			$this->set_page_number( $meta_data["Page Number"], $post_id );
		}
		*/
		
		/*
		 * Update Related Items
		 */
		if( __WP_UPDATE__ ){
			
			
			if( !empty( $meta_data["Companion Content"] ) ){
				$this->set_companion_content( $meta_data["Companion Content"], $post_id );
			}
			
			if( !empty( $meta_data["Associated Products"] ) ){
				$this->set_associated_products( $meta_data["Associated Products"], $post_id );
			}
			
			
			if( !empty( $meta_data["Taunton Product Attribution"] ) ){
				$this->taunton_product_attribution( $meta_data["Taunton Product Attribution"], $post_id );
			}
			
			
		}
		
		/*
		 * Video Clip
		 */
		update_field( 'field_56a3e3b949ae5', "", $post_id );
		if( $section == "Video"){
			$this->wp_video->set_ektron_video( $a['Html'], $post_id, $a['Id'] );
		}
		
		/*
		 * Add Redirect 
		 * 
		 */
		$this->add_redirects( $a['Quicklink'], get_permalink( $post_id ) );
		return  $post_id;
		
	}
	
	/*
	   
        "gallery",
        "attachment",
        "placeholder",
        "post",
        "html",
        "house-gallery",
        "video",
        "kb-contest",
        "brightcovevideo",
        "attachment-video",
        "digitalissue"
	 
	 */
	
	public function codeigniter_content( $data ){
	   
		$post = array();
		
		$post['ping_status'] = 'closed';
		$post['comment_status'] = 'open';
		
		$a = $data['post'];
		
		error_log( "---------------------codeigniter_content:".$a['post_type']);
		
		$taxonomy = $data['taxonomy'];
		
		$user = $data['user'];
		
		$post_meta = $data['post_meta'];
		
		$post_extended = $data['post_extended'];
		
		/*
		 * Skip Spam Content
		 */
		if( $this->spam_content( $a['post_title'] ) && ($a['post_type'] == 'gallery') ){
			return -1;
		}
		
		
		
		/*
		 * Check Member / Contributer Content
		 */
		
		
		/* Reader Projects
		 * house-gallery
		 * gallery
		 * kb-contest
		 */
		$reader_project_types = array(
				'gallery' => 'Gallery',
				'house-gallery' => 'House Gallery',
				'kb-contest' => 'KB Contest',
		);
				
		if( isset( $reader_project_types[$a['post_type']] ) ){
			
			$section = $reader_project_types[$a['post_type']];
			
			$post_type= 'readerproject';
			$alt = 'readersproject';
			
			$history_field = "field_56a3f2415eae1";
			
		}else{
			if( ($a['post_type'] == 'video') || ( $a['post_type'] == "brightcovevideo" ) ){
			
				$section = 'Video';
				
			}else{
				
				$section = 'Blog';
				
			}
			
			$post_type = 'post';
			$alt = 'readerproject';
			$history_field = "field_56a3abe1da42c";
		}
		
		
		$history_link = "http://www.finehomebuilding.com".$a['guid'];
		
		$post_id = 0;
		
		if( empty( $a['wp_post_id'] ) ){
		
			$posts = get_posts(array(
					'numberposts'	=> -1,
					'post_type'		=> $post_type,
					'meta_key'		=> 'fhb_history_link',
					'meta_value'	=> $history_link
			));
			
			if( !empty( $posts ) ){
				$post_id = $posts[0]->ID;
			
				/*
				if( has_post_thumbnail( $post_id ) ){
					return $post_id;
				}
				*/
			}
		}else{
			$post_id = $a['wp_post_id'];
		}
		
		if( empty( $a['guid'] ) || empty( $a['post_title'] ) ){
			return;
		}
	
		$post['post_title'] = $a['post_title'];
		$post['post_name'] = sanitize_title( $a['post_title'] );
		
		if( $this->spam_content( $a['post_title'] ) ){
			$post['post_status'] = 'draft';
		}else{
			$post['post_status'] = $a['post_status'];
		}
		
		$post['post_type'] = $post_type;
	
		$post['post_date'] = $a['post_date'];
	
		$post['post_date_gmt'] = $a['post_date_gmt'];
	
		
		// set content
		list( $content, $inline_images ) = $this->ci_clean_content( $a['post_content'], $post_type );
		
		$post['post_content'] = $content; 
	
		if( empty($post['post_content'] ) && !empty( $a['forpay_teaser']) ){
			
			$post['post_content'] = $a['forpay_teaser'];
			
		}
		
		//error_log( $post['post_content'] );
		
		//Set Excerpt / Abstract
		$post['post_excerpt'] = $a['post_excerpt'];
		
		
		/*
		 * Set Taxonomy
		 */
		$category_ids = array();
		$tags = array();
		
		$terms = array();
		
		if( !empty( $taxonomy['category']) ){
			$terms = array_merge( $terms,  $taxonomy['category'] );
		}
		
		if( !empty( $taxonomy['post_tag'] ) ){
			$terms = array_merge( $terms,  $taxonomy['category'] );
		}
		
		if( !empty( $terms ) ){
			
			$terms = array_unique( $terms );
			
			foreach( $terms as $t ){
				
				$topic = $this->db->findOne( 'csv_taxonomy', array( 't' => new MongoDB\BSON\Regex( $t, 'i') ) );
				
				if( !empty( $topic ) ){
				
					$category_ids[] = $topic['category_id'];
			
				}else{
					
					$tags[] = ucwords($t);
				}
			}
		}
		
	
		// category
		if( !empty( $category_ids ) )
			$post['post_category'] = $category_ids;
		
		// set tags
		if( !empty( $tags ) )
			$post['tags_input'] = $tags;
		
			
		$wp_error = '';
		
		
								

		if( !empty( $post_id ) ){
				
			print "Update Post=$post_id--->".$post['post_title']."\n";
			
			// update post
			$post['ID'] = $post_id;
			
			$wp_error = wp_update_post( $post, true );
				
		}else{
				
			// create post
			$post_id = wp_insert_post( $post, $wp_error );
			print "Insert Post=$post_id--->".$post['post_title']."\n";
		}
	
		
		/*
		 * Set Author / Member
		 */
		//if( !empty( $user['wp_user_id'] ) ){
			
			if( $post_type == 'readerproject' ){
				
				$wp_user_id= $this->find_wp_author_history_link( $user );
				
				if( !empty( $wp_user_id ) ){
				
					update_field( 'field_57354de064872', $wp_user_id, $post_id );
				
				}else{
				
					$wp_user_id = find_wp_member( $user );
				
					if( !empty( $wp_user_id ) ){
						update_field( 'field_56ce013c265a0', $wp_user_id, $post_id );
					}
				}
				
			}else{
				
				$wp_user_id= $this->find_wp_author( $user );
				
				if( !empty( $wp_user_id ) ){
					$this->set_fhb_author( array( $wp_user_id ), $post_id );
				}
				
			}		
		//}
		
		//insert images
		$attachment_ids = array();
		$main_image = array();
		
		
		if( !empty( $inline_images ) ){
			foreach( $inline_images as $inline_image ){
				$this->attachment->add_attachment(  $inline_image, $post_id  );
			}
		}
		
		//print_R( $data['attachments']  );
		
		foreach( $data['attachments'] as $attachment ){
			
			// Images
			if( $attachment['post_type'] == "attachment" ){
				
				if( empty( $main_image ) ){
					$main_image = array(
						'image_src' => $attachment['post_title'],
						'image_caption' => strip_tags($attachment['post_content']),
					);
				}
				$image = array(
					'image_src' => $attachment['post_title'],
					'image_caption' => strip_tags($attachment['post_content']),
				);
				
				$attachment_id =  $this->attachment->add_attachment( $image, $post_id );
				$attachment_ids[] = $attachment_id;
			}
			
			//Videos
			if( $attachment['post_type'] == "attachment-video" ){
				
				$this->wp_video->set_codeigniter_video( $attachment, $post_id );
			}
			
		}
		
		// set gallery
		if( $section != 'Video' ){
			
			if( !empty( $attachment_ids ) ){
				update_field( 'field_567b0ddffed1c', $attachment_ids, $post_id );
			}
			
		}
		
		/*
		 set channel
		 */
		if( !empty( $post_tags ) ){

			foreach( $post_tags as $t ){
				$post_tag_slugs[] = sanitize_title( $t );
			}
			if( in_array( 'howto', $post_tag_slugs ) ){
				$this->wp_term->set_channel( $post_id, 'HowTo' );
			}elseif( in_array( 'design', $post_tag_slugs ) ){
				$this->wp_term->set_channel( $post_id, 'Design' );
			}
		
		}
		
		// set section
		if( $post_type == 'post' ){
			$this->wp_term->set_section( $post_id , $section );
		}
	
		// set history link
		update_field( $history_field, $history_link, $post_id );
			
		/* 
		 * set main image /thumbnail 
		 */
		if( !empty( $main_image ) ){
			$attachment_ids =  $this->attachment->add_main_image_attachment( $main_image, $post_id );
		}
		if( !empty( $attachment_ids ) ){
			$set_main_image = true; 	
			$this->set_fhb_main_image( $attachment_ids, $post_id, $set_main_image );
			
		}else{
			$this->remove_fhb_main_image( $post_id );
		}

		/*
		 set pool
		 */
		
		$pool_relation = $this->ci_db->findOne( 'pool_post_relationships', array( 'post_id' => $a['ID'] ) );
		
		if( !empty( $pool_relation ) ){
			
			$pool = $this->ci_db->findOne( 'pools', array( 'pool_id' => $pool_relation['pool_id'] ) );
			
			$pool_history_link = "http://www.finehomebuilding.com/".$pool['pool_guid'];
			
			$wp_pool = get_posts(array(
					'numberposts'	=> -1,
					'post_type'		=> 'pool',
					'meta_key'		=> 'history_link',
					'meta_value'	=> $pool_history_link
			));
			
			$wp_pool_id = 0;
			
			if( !empty( $wp_pool ) ){
				$wp_pool_id = $wp_pool[0]->ID;
			}
			
			if( !empty( $wp_pool_id ) ){

				update_field( 'field_56a6905f2d756', $wp_pool_id, $post_id );
			
			}
	
		}
		
		
		if( $post_type == 'readerproject' ){
			
			$post_meta = $data['post_meta'];
			
			$post_extended = $data['post_extended'];
			
			if( !empty( $post_meta ) ){
			
				$this->post_meta( $post_meta, $post_id );
			}
			
			if( !empty( $post_extended ) ){
				
				$this->post_extended( $post_extended,  $post_id, $a['post_type'] );
			
			}
		}
		
		/*
		 * set forpay flags
		 * "forpay_teaser" 
		 * "is_forpay"
		 * forpay_download
		 */	
		if( !empty( $a['is_forpay'] ) ){
			$this->set_content_access( 'Subscription Required', $post_id );
		}else{
			$this->set_content_access( 'Free Content', $post_id );
		}
		
		/*
		 * PDF
		 */
		
		if( !empty( $a['forpay_download'] ) ){
			$pdf_attachement_id = $this->attachment->add_pdf_attachment( $a['forpay_download'], $post_id );
			if( !empty( $pdf_attachement_id ) ){
				$this->set_pdf_attachment( $pdf_attachement_id, $post_id );
			}
		}
		
		$this->add_redirects( $a['guid'], get_permalink( $post_id ) );
		
		print "--------------------------------------------WP: $post_type: $post_id --------------------------------------------------\n";
		
		return $post_id;
		
	}
	
	/*
	  "vote_count",
        "pattern_type",
        "pattern_own_title",
        "pattern_own_url",
        "pattern_external_title",
        "pattern_external_url"
	 */
	
	function post_meta( $post_meta, $post_id ){
		
		/* Vote Count*/
		if( !empty( $post_meta['vote_count'] ) ){
			
			update_field( 'field_56ce0b9c9160b', $post_meta['vote_count'], $post_id );
			
		}
		
		/* Pattern Type / Design */
		if( !empty( $post_meta['pattern_type'] ) ){
			
			update_field( 'field_56ce0cee9160e', $post_meta['pattern_type'], $post_id );
			
		}
		
		/* Design Title */
		if( !empty( $post_meta['pattern_own_title'] )  ){
			
			update_field( 'field_56ce0cdf9160d',$post_meta['pattern_own_title'], $post_id );
			
		}elseif( !empty($post_meta['pattern_external_title']  )  ){
			
			update_field( 'field_56ce0cdf9160d',$post_meta['pattern_external_title'], $post_id );
			
		}
		
		/* Design URL */
		if( !empty( $post_meta['pattern_own_url'] )  ){
				
			update_field( 'field_56ce0bb89160c',$post_meta['pattern_own_url'], $post_id );
				
		}elseif( !empty($post_meta['pattern_external_url']  )  ){
				
			update_field( 'field_56ce0bb89160c',$post_meta['pattern_external_url'], $post_id );
				
		}
		
		
	}
	
	
	function post_extended( $post_extended,  $post_id, $post_type ){
		
		$repeater = array();
		
		foreach( $post_extended as $k => $v ){
			
			if( preg_match( "/field_/", $k ) ){
				
				if( !empty( $v ) ){
					
					$field_config = $this->get_flex_field( $k, $post_type );
					
					if( $field_config['type'] == 'text' ){
						$repeater_text[] = array(
								'field_56f6dd08fdf36' => $field_config['label'],
								'field_56f6dd08fdf37' => $v,
						);
					}
					
					if( $field_config['type'] == 'textarea' ){
						$repeater_textarea[] = array(
								'field_56f6db89da166' => $field_config['label'],
								'field_56f61c8e0261f' => $v,
						);
					}
					
					if( $field_config['type'] == 'fileupload' ){
						
						$id = $this->attachment->add_pdf_attachment( $v, $post_id, true );
						
						$repeater[ $field_config['key'] ] [] = array( $field_config['file_key'] => $id );

					}else{
						
						update_field( $field_config['key'], $v, $post_id );
						
					}
				}
			}
			
		}
		
		if( !empty( $repeater ) ){
			
			foreach( $repeater as $k => $v ){
			
				update_field( $k, $v, $post_id );
			
			}
		}
		
		/* delete serilize data 
		 *  meta_key: readerproject_post_extended
		 *  meta_key: _readerproject_post_extended
		 * 
		 */
		
		delete_post_meta($post_id, 'readerproject_post_extended');
		delete_post_meta($post_id, '_readerproject_post_extended');
	}
	
	function get_flex_field( $id, $type ) {
		
		$num = preg_replace( "/field_/", '', $id );
		
		$config = $this->get_flex_config( $type );
		
		return $config[$num-1];
	}

	function get_flex_config( $type ) {
		$config['field_definition_house-gallery'] = array(	
			array(	
				'key' => 'field_5702cc8e49575',
				'file_key' => 'field_5702ccc149576',	
				'label' => 'Floor Plan(s)',
				'help' 	=> '',
				'type' 	=> 'fileupload',
				'name'  => 'floor_plans',
				'rules'	=> 'required',
				'visible' => true
			),
			array(
				'key' => 'field_5702cd5fca475',	
				'label' => 'Location',
				'help' 	=> '',
				'type' 	=> 'text',
				'name'  => 'location',
				'rules'	=> 'required',
				'visible' => true
			),
			array(	
				'key' => 'field_5702cd86ca476',
				'label' => 'Number of bedrooms',
				'help' 	=> '',
				'type' 	=> 'text',
				'name'  => 'bedrooms',
				'rules'	=> 'required',
				'visible' => true
			),
			array(	
				'key' => 'field_5702cdafca477',
				'label' => 'Number of baths',
				'help' 	=> '',
				'type' 	=> 'text',
				'name'  => 'baths',
				'rules'	=> 'required',
				'visible' => true
			),
			array(
				'key' => 'field_5702cdc5ca478',
				'label' => 'Size (in square feet)',
				'help' 	=> '',
				'type' 	=> 'text',
				'name'  => 'sq_ft',
				'rules'	=> 'required',
				'visible' => true
			),
			array(	
				'key' => 'field_5702cde4ca479',
				'label' => 'Year completed',
				'help' 	=> '',
				'type' 	=> 'text',
				'name'  => 'completed',
				'rules'	=> 'required',
				'visible' => true
			),
			array(	
				'key' => 'field_5702cdfaca47a',
				'label' => 'Architect or designer',
				'help' 	=> '',
				'type' 	=> 'text',
				'name'  => 'architect',
				'rules'	=> 'required',
				'visible' => true
			),
			array(
				'key' => 'field_5702ce41df092',
				'label' => 'Builder',
				'help' 	=> '',
				'type' 	=> 'text',
				'name'  => 'builder',
				'rules'	=> 'required',
				'visible' => true
			),
			array(	
				'key' => 'field_5702ce6542738',
				'label' => 'Construction Costs',
				'help' 	=> '',
				'type' 	=> 'text',
				'name'  => 'costs',
				'rules'	=> 'required',
				'visible'	=> true
			),
			array(
				'key' => 'field_5702cfaa42739',
				'label' => 'Energy-efficiency, cool details, great ideas and more',
				'help' 	=> '',
				'type' 	=> 'textarea',
				'name'  => 'details',
				'rules'	=> '',
				'visible' => true
			),
			array(	
				'key' => 'field_5702d01d62097',
				'label' => 'name',
				'type' 	=> 'text',
				'name'  => 'name',
				'rules'	=> 'required',
				'visible' => true
			),
			array(	
				'key' => 'field_5702d02b62098',
				'label' => 'phone',
				'type' 	=> 'text',
				'name'  => 'phone',
				'rules'	=> 'required',
				'visible' => true
			),														
			array(	
				'key' => 'field_5702d03c62099',
				'label' => 'email',
				'type' 	=> 'text',
				'name'  => 'email',
				'rules'	=> 'required|valid_email',
				'visible' => true
			),
			array(	
				'key' => 'field_5702d04e6209a',		
				'label' => 'mailing address',
				'type' 	=> 'text',
				'name'  => 'address',
				'rules'	=> 'required',
				'visible' => true
			),
			array(	
				'key' => 'field_5702d0596209b',
				'label' => 'city',
				'type' 	=> 'text',
				'name'  => 'city',
				'rules'	=> 'required',
				'visible' => true
			),
			array(	
				'key' => 'field_5702d0666209c',
				'label' => 'state',
				'type' 	=> 'text',
				'name'  => 'state',
				'rules'	=> 'required',
				'visible' => true
			),
			array(	
				'key' => 'field_5702d0736209d',
				'label' => 'zipcode',
				'type' 	=> 'text',
				'name'  => 'zip',
				'rules'	=> 'required',
				'visible' => true
			),
			array(
				'key' => 'field_5702d0866209e',
				'label' => 'country',
				'type' 	=> 'text',
				'name'  => 'country',
				'rules'	=> 'required',
				'visible' => true
			),
			array(
				'key' => 'field_5702cc8e49575',
				'file_key' => 'field_5702ccc149576',	
				'label' => 'Floor Plan(s) #2',
				'help' 	=> '',
				'type' 	=> 'fileupload',
				'name'  => 'floor_plans1',
				'rules'	=> '',
				'visible' => true
			),
			array(	
				'key' => 'field_5702cc8e49575',
				'file_key' => 'field_5702ccc149576',	
				'label' => 'Floor Plan(s) #3',
				'help' 	=> '',
				'type' 	=> 'fileupload',
				'name'  => 'floor_plans2',
				'rules'	=> '',
				'visible' => true
			)
		);
		
		$config['field_definition_kb-contest'] = array(	
			
			array(	
				'key' => 'field_571a851437264',	
				'label' => 'Kitchen or Bath?',
				'help' 	=> '',
				'type' 	=> 'text',
				'name'  => 'kitchen_bath',
				'rules'	=> 'required',
				'visible' => true
			),
			array(	
				'key' => 'field_571a855337265',
				'label' => 'New, Remodel, or Remodel with Addition?',
				'help' 	=> '',
				'type' 	=> 'text',
				'name'  => 'new_remodel',
				'rules'	=> 'required',
				'visible' => true
			),
			array(	
				'key' => 'field_571a83eb37e25',
				'file_key' => 'field_571a83ec37e26',	
				'label' => 'Floor Plan',
				'help' 	=> '',
				'type' 	=> 'fileupload',
				'name'  => 'floor_plan',
				'rules'	=> '',
				'visible' => true
			),
			array(	
				'key' => 'field_571a859337266',
				'label' => 'If new, how does the project relate to adjacent spaces and rooms?',
				'help' 	=> '',
				'type' 	=> 'textarea',
				'name'  => 'adjacent_spaces',
				'rules'	=> 'required',
				'visible' => true
			),
			array(	
				'key' => 'field_571a85e637267',
				'label' => 'If addition, how many square feet did you add?',
				'help' 	=> '',
				'type' 	=> 'text',
				'name'  => 'sqft_added',
				'rules'	=> 'required',
				'visible' => true
			),
			array(
				'key' => 'field_571a861d37268',
				'label' => 'If remodel or addition, how much did the project cost?',
				'help' 	=> '',
				'type' 	=> 'textarea',
				'name'  => 'project_cost',
				'rules'	=> 'required',
				'visible' => true
			),
			array(
				'key' => 'field_571a865637269',
				'label' => 'Please describe your approach to choosing fixtures, materials and appliances.',
				'help' 	=> '',
				'type' 	=> 'textarea',
				'name'  => 'choices_methodology',
				'rules'	=> 'required',
				'visible' => true
			),
			array(
				'key' => 'field_571a86903726a',
				'label' => 'What did you do to save money?',
				'help' 	=> '',
				'type' 	=> 'textarea',
				'name'  => 'save_money',
				'rules'	=> 'required',
				'visible' => true
			),
			array(
				'key' => 'field_571a86b03726b',
				'label' => 'What items did you decide to splurge on?',
				'help' 	=> '',
				'type' 	=> 'textarea',
				'name'  => 'splurge',
				'rules'	=> 'required',
				'visible' => true
			),
			array(
				'key' => 'field_571a870f3726c',
				'label' => 'Please tell us about any innovative construction or design details as well as any storage solutions, energy or water conserving efforts, or aging in place strategies that you included in your project.',
				'help' 	=> '',
				'type' 	=> 'textarea',
				'name'  => 'innovative',
				'rules'	=> 'required',
				'visible' => true
			),
			array(
				'key' => 'field_5702d01d62097',
				'label' => 'name',
				'type' 	=> 'text',
				'name'  => 'name',
				'rules'	=> 'required',
				'visible' => true
			),
			array(
				'key' => 'field_5702d02b62098',
				'label' => 'phone',
				'type' 	=> 'text',
				'name'  => 'phone',
				'rules'	=> 'required',
				'visible' => true
			),														
			array(
				'key' => 'field_5702d03c62099',
				'label' => 'email',
				'type' 	=> 'text',
				'name'  => 'email',
				'rules'	=> 'required|valid_email',
				'visible' => true
			),
			array(
				'key' => 'field_5702d04e6209a',
				'label' => 'mailing address',
				'type' 	=> 'text',
				'name'  => 'address',
				'rules'	=> 'required',
				'visible' => true
			),
			array(
				'key' => 'field_5702d0596209b',
				'label' => 'city',
				'type' 	=> 'text',
				'name'  => 'city',
				'rules'	=> 'required',
				'visible' => true
			),
			array(
				'key' => 'field_5702d0666209c',
				'label' => 'state',
				'type' 	=> 'text',
				'name'  => 'state',
				'rules'	=> 'required',
				'visible' => true
			),
			array(
				'key' => 'field_5702d0736209d',
				'label' => 'zipcode',
				'type' 	=> 'text',
				'name'  => 'zip',
				'rules'	=> 'required',
				'visible' => true
			),
			array(
				'key' => 'field_5702d0866209e',
				'label' => 'country',
				'type' 	=> 'text',
				'name'  => 'country',
				'rules'	=> 'required',
				'visible' => true
			)
		);
		
		$config['field_definition_digitalissue'] = array(
			array(
				'label' => 'Subscriber Embed:',
				'type' 	=> 'textarea',
				'name'  => 'subscriber_embed',
				'rules'	=> 'required',
				'visible' => true,
				'help' => 'The player embed code subscribers will see.'
			),
			array(
				'label' => 'Non-subscriber Embed:',
				'type' 	=> 'textarea',
				'name'  => 'nonsubscriber_embed',
				'rules'	=> 'required',
				'visible' => true,
				'help' => 'The player embed code non-subscribers will see (typically a preview, etc).'
			),
			array(
				'label' => 'Issue Number:',
				'type' 	=> 'text',
				'name'  => 'issue_number',
				'rules'	=> '',
				'visible' => false,
				'help' => 'Issue Number for this issue'
			)
		);
		
		return $config['field_definition_'.$type];
	}
	
	function find_wp_author( $user ){
	
		$author_history_link = "http://www.finehomebuilding.com/profile/".$user['display_name'];
			
		$authors = get_posts(array(
				'numberposts'	=> -1,
				'post_type'		=> 'author',
				'meta_key'		=> 'history_link',
				'meta_value'	=> $author_history_link
		));
			
		$wp_author_id = 0;
	
		if( !empty( $authors ) ){
	
			$wp_author_id = $authors[0]->ID;
	
		}else{
			$authors = get_posts(array(
					'numberposts'	=> -1,
					'post_type'		=> 'author',
					'name'		=> sanitize_title($user['user_first_name']." ".$user['user_last_name'] ),
			));
			if( !empty( $authors ) ){
				$wp_author_id = $authors[0]->ID;
			}
	
		}
			
		return $wp_author_id;
	
	}
	
	function find_wp_author_history_link( $user ){
	
		$author_history_link = "http://www.finehomebuilding.com/profile/".$user['display_name'];
			
		$authors = get_posts(array(
				'numberposts'	=> -1,
				'post_type'		=> 'author',
				'meta_key'		=> 'history_link',
				'meta_value'	=> $author_history_link
		));
			
		$wp_author_id = 0;
	
		if( !empty( $authors ) ){
	
			$wp_author_id = $authors[0]->ID;
	
		}
		error_log( $author_history_link."----->". $wp_author_id );	
		return $wp_author_id;
	
	}
	
}




function find_wp_member($user){
	
	global $wpdb;
	$tid = $user['TID'];
	$user_id = '';
	$table_name = $wpdb->prefix . 'fhb_user';
	$m = $wpdb->get_row($wpdb->prepare("SELECT ID FROM $table_name WHERE TID= %d", intval($tid) ), ARRAY_A);
	if( !empty( $m ) ){
		$user_id = $m['ID'];
	}
	error_log( "------------------------find_wp_member : TID".$user['TID']."    WP_ID:".$user_id );
	return $user_id;
	
}


?>
