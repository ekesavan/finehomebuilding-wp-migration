<?php 

class WPReview extends WPContent{

	
	function __construct(){
				
		parent::__construct();
	}
	
	public function insert_post( &$a ){
		
		$post = array();
		
		$post['ping_status'] = 'closed';
		$post['comment_status'] = 'open';
		
		$meta_data = array();
		$metas = $a['META_DATA']['GetContentMetadataListResult']['CustomAttribute'];
		
		foreach( $metas as $meta ){
			$meta_data[$meta['Name']] = $meta['Value'];
		}
		
		if( !empty( $meta_data['Primary Channel - Fine Woodworking'] ) ){
			print "Skip...............\n";
			return;
		}
		
				
		$product_id = $meta_data['Associated Products'];
		
		/*
		 * Product
		 */
		
		$obj = $this->db->findOne( 'tool_guide', array( 'Id' => $product_id ) );
		
		$product = json_decode(json_encode($obj), true);
		
		error_log( "product link:". "http://www.finehomebuilding.com".$product['Quicklink'] );
		
		if( !empty( $product ) ){
			
			$post_id = $this->insert_product_post( $product, $a );
			
		}
		
		if( empty( $post_id ) ) {
		
			$post['post_title'] = $a['Title'];
			$post['post_name'] = sanitize_title( $a['Title'] );
			
			error_log( "---------------------insert_review_post: ". $post['post_title']  );
			
			// set post status 
			if( $a["IsPublished"] == "true" ){
				$post['post_status'] = 'publish';
			}else{
				$post['post_status'] = 'draft';
			}
			
			//$post['post_type'] = 'productreview';
			
			$post['post_type'] = 'post';
			
			// published date
			$published_date = '';
			
			if( !empty(  $meta_data['First Published Date'] ) ){
				$published_date = $meta_data['First Published Date'];
			}else{
				$published_date = $a['DisplayDateCreated'];
			}
			
			$post['post_date'] = $this->post_date( $published_date );
			
			$post['post_date_gmt'] = $this->post_date_gmt( $published_date );
		
		
			// set content
			list( $content, $inline_images ) = $this->review_clean_content( $a['Html'] );
		
						
			$post['post_content'] = $content;
				
			
			//Set Excerpt / Abstract
			$post['post_excerpt'] = $this->post_excerpt( $a['Html'] );
			
			
			$tags = array();
			$category_ids = array();
			
			// category
			if( !empty( $meta_data['Taxonomy Terms'] ) ){
				list( $category_ids, $tags ) =  $this->post_category( $meta_data['Taxonomy Terms'] );
		
				if( !empty( $category_ids ) ){
					$post['post_category'] = $category_ids;
				}
			}
			
			// set tags		
			$post['tags_input'] = $this->tags_input( $tags, @$a['Keywords'] );	
			$post['tags_input'][] = 'Product Review';
			
			$channel = 'ToolGuide';
			
			$wp_error = '';
					
			$history_link = "http://www.finehomebuilding.com".$a['Quicklink'];
			
			print $a['Id'].":".$history_link."\n";
			
			$post_id = 0;
			
			$posts = get_posts(array(
						'numberposts'	=> -1,
						'post_type'		=> 'post',
						'meta_key'		=> 'fhb_history_link',
						'meta_value'	=> $history_link
			));
				
			
			if( !empty( $posts ) ){
				$post_id = $posts[0]->ID;
					
			}
						
			if( !empty( $post_id ) ){
				
				print "Update Post=$post_id--->".$post['post_title']."\n";
				// update post
				$post['ID'] = $post_id;
				wp_update_post( $post );
				
			}else{
				
				// create post
				$post_id = wp_insert_post( $post, $wp_error );
				print "Insert Post=$post_id--->".$post['post_title']."\n";
			}
			error_log("----------------------01");
			/*
			 * Add Image Attachments
			 */
			if( !empty( $inline_images ) ){
				foreach( $inline_images as $inline_image ){
					$this->attachment->add_attachment(  $inline_image, $post_id  );
				}
			}
			error_log("----------------------02");
		
			/*
			 * Set History Link
			 */
			$this->set_fhb_history_link( $history_link, $post_id );
			
			/*
			 * Set Main and Thumbnail Image
			 */
			$attachment_ids = $this->main_image( $a['Html'],$post_id );
			if( !empty( $main_image_id ) ){
			
				$this->set_fhb_main_image( $attachment_ids, $post_id );
					
			}
			
			error_log("----------------------03");
			
			/*
			 * Set Deck / SubHead
			 */
			$deck = $this->post_deck( $a['Html'] );
			if( !empty( $deck ) ){
			
				$this->set_fhb_deck( $deck, $post_id );
			
			}
			
			// set channel
			$this->wp_term->set_channel( $post_id, $channel );
			
			// set section
			$this->wp_term->set_section( $post_id , 'Default' );
			
		}
		
		$review_specification = $this->review_specification( $a['Html'] );
		
		$specification_data = array();
		
		foreach( $review_specification as $k => $v ){
			
			if( is_array( $v ) && !empty( $v ) ){
				
				$spec_row = array_values($v);
				
				
				if( !empty( $spec_row[1] ) && !is_array($spec_row[0])){

					$label = $spec_row[0];
					$label = preg_replace( '/^(.*?)\d+\*(.*?)$/', '\\2', $label );
					
					$specification_data[] = array(
					
							'label' => trim($label),
							
							'value' => $spec_row[1],
						
							
					);
					
					
				}else{
					foreach( $v as $k1 => $v1 ){
						$spec_row = array_values($v1);
						if( !empty( $spec_row[1] ) && !is_array($spec_row[0])){
						
							$label = $spec_row[0];
							$label = preg_replace( '/^(.*?)\d+\*(.*?)$/', '\\2', $label );
								
							$specification_data[] = array(
										
									'label' => trim($label),
										
									'value' => $spec_row[1],
						
										
							);
								
						}
					}
				}
			}
			
		}
		
		// set specification
		update_field( 'field_56847a984074b', $specification_data, $post_id );
		
		/*
		 * Set Author
		 */
		if( !empty( $meta_data['Author'] ) ){
		
			$post_authors = $this->post_author( $meta_data['Author'] );
		
			$this->set_fhb_author( $post_authors, $post_id );
		}
		
		
		// set pdf link
		$pdf_attachement_id = $this->alternate_formats( $a['Html'],$post_id );
		if( !empty( $pdf_attachement_id ) ){
			$this->set_pdf_attachment( $pdf_attachement_id, $post_id );
		}
		
		/*
		 "Name" : "Content Access",
		 [Free Content] => 6154
		 [Subscription Required] => 1992
		 
		*/
		$this->set_content_access( $meta_data['Content Access'], $post_id );
		
		
			/*
		 "Name" : "Title/Issue",
		 "Value" : "123",
		*/
		/*
		if( !empty( "Title/Issue" ) ){
			$this->set_title_issue( $meta_data["Title/Issue"], $post_id );
		}
		*/
		
		/*
		 "Name" : "Page Number",
		 "Value" : "82-87",
		*/
		if( !empty( $meta_data["Page Number"] ) ){
			$this->set_page_number( $meta_data["Page Number"], $post_id );
		}
		
			
		/* [Best Overall] */
		update_field( 'field_567b245d48a39', $meta_data["Best Overall"], $post_id );
		
		/* [Best Value] */
		update_field( 'field_567b248d8582d', $meta_data["Best Value"], $post_id );
		
		/* [Review Format] => Summary */
		update_field( 'field_567b24b43e3fc', $meta_data["Review Format"], $post_id );
		
		
		/*
		 * Update Related Items
		 */
		if( __WP_UPDATE__ ){
				
			/* [Comparison Review] => 85696 */
			if( !empty( $meta_data["Comparison Review"] ) ){
				
				$this->set_companion_content( $meta_data["Comparison Review"], $post_id );
				
			}
				
			if( !empty( $meta_data["Associated Products"] ) ){
				
				$this->set_associated_products( $meta_data["Associated Products"], $post_id );
				
			}
				
		}
		
		print $post['post_title']."-------------->". $post_id."\n";	
		if( !empty( $product['Quicklink'] ) ){
			$this->add_redirects( $product['Quicklink'], get_permalink( $post_id ) );
		}
		$this->add_redirects( $a['Quicklink'], get_permalink( $post_id ) );
		
		unset($meta_data);
		unset($metas);
		
		unset( $product );
		unset( $post );
		unset( $specification_data );
		
		return $post_id;
	}


	public function review_specification( $html ){
	
		$content = '';
	
		$dom = str_get_html( $html  );
	
		$xml = simplexml_load_string($dom->find( "Editorial_Review_Specs",0)->outertext);
		$json = json_encode( $xml );
		$data = json_decode( $json, TRUE );
	
		//print_R( $data );
			
		$dom->clear();
		unset( $dom );
		unset( $xml );
		
		return $data;
	
	}
	
	public function review_clean_content( $html ){
	
	
		$content = '';
	
		$inline_images = array();
		
		$dom = str_get_html( $html  );
		
		if( $dom->find( 'Abstract', 0 ) ){
			
			foreach( $dom->find( 'Abstract', 0 )->find('img') as $image ){

				$image_src = $image->src;
				
				 //$section_image = __S3_URL__.CONTENT_DIR."/uploads". $this->attachment->lowercase_path( $image_src );
				 $attachment_id = $this->attachment->add_attachment(  $image_src , 0 );
				 $section_image = wp_get_attachment_url( $attachment_id );
				 

					$inline_images[] = array(
					
							'image_src' => $image_src,
							
							'image_caption' => '',
					
							
					);
			         
					$image->outertext = "<figure><img  src=\"".$section_image."\" style=\"".$image->style."\"/></figure>";
			        
			}	
			
					
			$content =  $dom->find( 'Abstract', 0 )->innertext;
			$content = preg_replace( "|<strong>(\s+)<\/strong>|si", '', $content );
			$content = preg_replace( "|<p>(\s+)</p>|si", '', $content );
						
		}
					
		$dom->clear();
		unset( $dom );
	
		//print "====================\n$content\n==================\n";
		
		//print_R( $inline_images );
		
		return array( $content, $inline_images );
	
	}
	
	
	public function insert_product_post( $a, $review ){
	
	
		$post = array();
	
		$metas = $a['META_DATA']['GetContentMetadataListResult']['CustomAttribute'];
	
		$meta_data = array();
	
		foreach( $metas as $meta ){
			$meta_data[$meta['Name']] = $meta['Value'];
		}
	
		if( !empty( $meta_data['Primary Channel - Fine Woodworking'] ) ){
			print "Skip...............\n";
			return;
		}
	
		if( !empty( $meta_data['title'] ) ){
			$post['post_title'] = $meta_data['title'];
		}else{
			$post['post_title'] = $review['Title'];
		}
		error_log( "---------------------insert_product_post: ". $post['post_title']  );
	
		$post['post_name'] = sanitize_title( $review['Title'] );
	
		// set post status
		if( $a["IsPublished"] == "true" ){
			$post['post_status'] = 'publish';
		}else{
			$post['post_status'] = 'draft';
		}
	
		$post['post_type'] = 'post';
	
		$post['post_date'] = $this->post_date( $a['DisplayDateCreated'] );
	
		$post['post_date_gmt'] = $this->post_date_gmt( $a['DisplayDateCreated'] );
	
		// set content
		list( $content, $inline_images ) = $this->review_clean_content( $review['Html'] );
	
		//print $content."\n\n";
	
		$product_specification = $this->product_specification( $a['Html'] );
			
		$specification_data = array();
		
		foreach( $product_specification as $k => $v ){
			if( is_array( $v ) ){
				foreach( $v as $s ){
						
					if( is_array( $s ) ){
						$spec_row = array_values($s);
	
						if( !empty( $spec_row[1] ) ){
							$label = $spec_row[0];
							$label = preg_replace( '/^(.*?)\d+\*(.*?)$/', '\\2', $label );
							$specification_data[] = array(
									'label' => trim($label),
									'value' => $spec_row[1],
							);
						}
						
					}else{
						$spec_row = array_values($v);
	
						if( !empty(  $spec_row[1] ) ){
							$label = $spec_row[0];
							$label = preg_replace( '/^(.*?)\d+\*(.*?)$/', '\\2', $label );
							$specification_data[] = array(
									'label' => trim($label),
									'value' => $spec_row[1],
							);
						}
					}
				}
	
			}
		}
		
		$post['post_content'] = $content;
	
		//Set Excerpt / Abstract
		//$post['post_excerpt'] = $this->post_excerpt( $a['Html'] );
		$post['post_excerpt'] = $a['Title'];

		
		$tags = array();
		$category_ids = array();
	
		// category
		if( !empty( $meta_data['Taxonomy Terms'] ) ){
			list( $category_ids, $tags ) =  $this->post_category( $meta_data['Taxonomy Terms'] );
	
			if( !empty( $category_ids ) ){
				$post['post_category'] = $category_ids;
			}
		}
	
		// set tags
		$post['tags_input'] = $this->tags_input( $tags, @$a['Keywords'] );
		
		$post['tags_input'][] = 'Product Review';
	
		$channel = 'ToolGuide';
	
		$wp_error = '';
	
		$history_link = "http://www.finehomebuilding.com".$a['Quicklink'];
	
		print $a['Id'].":".$history_link."\n";
			
		$posts = get_posts(array(
				'numberposts'	=> -1,
				'post_type'		=> 'post',
				'meta_key'		=> 'fhb_history_link',
				'meta_value'	=> $history_link
		));
		
		$post_id = 0;
		if( !empty( $posts ) ){
			$post_id = $posts[0]->ID;				
		}
		
		if( !empty( $post_id ) ){
				
			print "Update Post=$post_id--->".$post['post_title']."\n";
			// update post
			$post['ID'] = $post_id;
			wp_update_post( $post );
				
		}else{
				
			// create post
			$post_id = wp_insert_post( $post, $wp_error );
			print "Insert Post=$post_id--->".$post['post_title']."\n";
		}
	
		
		/*
		 * Add Image Attachments
		 */
		if( !empty( $inline_images ) ){
			foreach( $inline_images as $inline_image ){
				$this->attachment->add_attachment(  $inline_image, $post_id  );
			}
		}
		
	
		/*
		 * Set Mmain and Thumbnail Image
		 */
		$attachment_ids = $this->main_image( $a['Html'], $post_id );
		if( !empty( $attachment_ids ) ){
			$set_main_image = true;
			$this->set_fhb_main_image( $attachment_ids, $post_id, $set_main_image );
				
		}
	
	
		/*
		 * Set Deck / SubHead
		 */
		$deck = $this->post_deck( $a['Html'] );
		if( !empty( $deck ) ){
	
			$this->set_fhb_deck( $deck, $post_id );
	
		}
	
	
		// set specification
		update_field( 'field_56840e65c742d', $specification_data, $post_id );
	
		// set anufacturer
		$manufacturer = $meta_data['Manufacturer'];
		if( !empty ( $manufacturer ) ){
			$m = $this->db->findOne( 'manufacturers', array( 'Id' => $manufacturer ));
			if( !empty( $m ) ){
				update_field( 'field_567b11cacd436', $m['wp_post_id'], $post_id );
			}
		}
	
	
		// set channel
		$this->wp_term->set_channel( $post_id, $channel );
	
		// set section
		$this->wp_term->set_section( $post_id ,'Default' );
	
		/*
		 * Set Author
		 */
		if( !empty( $meta_data['Author'] ) ){
	
			$post_authors = $this->post_author( $meta_data['Author'] );
	
			$this->set_fhb_author( $post_authors, $post_id );
		}
	
	
		/*
		 * Set History Link
		 */
		$this->set_fhb_history_link( $history_link, $post_id );
	
	
		/* [Average Price] => 1300 */
		update_field( 'field_567b119400e47', $meta_data['Average Price'], $post_id );
	
		/* [Product Availability] => Available */
		update_field( 'field_567b11ecc99c5', $meta_data['Product Availability'], $post_id );
	
		/* [Model Number] => JSS-MCA */
		update_field( 'field_567b1203b1826', $meta_data['Model Number'], $post_id );
	
		/* [Amazon ID - ASIN] => B00SAHUO38 */
		update_field( 'field_567b121c277fe', $meta_data['Amazon ID - ASIN'], $post_id );
	
		/* [CMS5 Content ID] => */
		update_field( 'field_567b1235596b7', $meta_data['CMS5 Content ID'], $post_id );
	
		/* [SKU] => */
		update_field( 'field_567b1248f6df4', $meta_data['SKU'], $post_id );
	
	
		/*
		 * Update Related Items
		 */
		if( __WP_UPDATE__ ){
				
				
			if( !empty( $meta_data["Companion Content"] ) ){
				$this->set_companion_content( $meta_data["Companion Content"], $post_id );
			}
				
			if( !empty( $meta_data["Associated Products"] ) ){
				$this->set_associated_products( $meta_data["Associated Products"], $post_id );
			}
				
				
			if( !empty( $meta_data["Taunton Product Attribution"] ) ){
				$this->taunton_product_attribution( $meta_data["Taunton Product Attribution"], $post_id );
			}
				
				
		}
		
		unset( $post );
	
		return $post_id;
	}
	
	/*
	 <Specifications>
	 <Specs>
	 <Generic_Manufacturer_Specs>Blade Style</Generic_Manufacturer_Specs>
	 <Generic_Manufacturer_Specs_Value>Pin</Generic_Manufacturer_Specs_Value>
	 </Specs>
	 <Specs>
	 <Generic_Manufacturer_Specs>Maximum Marking Reach</Generic_Manufacturer_Specs>
	 <Generic_Manufacturer_Specs_Value>N/A</Generic_Manufacturer_Specs_Value>
	 </Specs>
	 </Specifications>
	
	 <Specifications>
	 <Materials>
	 <Manufacturer_Specs_Materials>All 1*Weight</Manufacturer_Specs_Materials>
	 <Manufacturer_Specs_Materials_Value>N/A</Manufacturer_Specs_Materials_Value>
	 </Materials>
	 <Materials>
	 <Manufacturer_Specs_Materials>All 2*Dimensions</Manufacturer_Specs_Materials>
	 <Manufacturer_Specs_Materials_Value>1/2 in., 5/8 in., 3/4 in., and 1-in. thick</Manufacturer_Specs_Materials_Value>
	 </Materials>
	 </Specifications>
	
	 */
	
	public function product_specification( $html ){
		error_log( "-------------------------product_specification---------------------");
		$content = '';
	
		$dom = str_get_html( $html  );
	
		$xml = simplexml_load_string( $dom->find( "Specifications",0)->outertext );

		$json = json_encode( $xml );
		$data = json_decode( $json, TRUE );
	
		$dom->clear();
		unset( $dom );
		unset( $xml );
	
		return $data;
	
	}
}

?>

