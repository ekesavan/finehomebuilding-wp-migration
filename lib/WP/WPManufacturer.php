<?php 

class WPManufacturer extends WPContent{

	
	function __construct(){

		parent::__construct();
		
	}
	
	public function insert_post( $a ){
		
		$post = array();
		$post['ping_status'] = 'closed';
		$post['comment_status'] = 'closed';
		
		$post['post_title'] = $a['Title'];
		$post['post_name'] = sanitize_title( $a['Title'] );
		
		// set post status 
		if( $a["IsPublished"] == "true" ){
			$post['post_status'] = 'publish';
		}else{
			$post['post_status'] = 'draft';
		}
		
		$post['post_type'] = 'manufacturer';
		
		$post['post_date'] = $this->post_date( $a['DisplayDateCreated'] );
		
		$post['post_date_gmt'] = $this->post_date_gmt( $a['DisplayDateCreated'] );
		
				
		$wp_error = '';
			
		$history_link = "http://www.finehomebuilding.com".$a['Quicklink'];
		
		print $a['Id'].":".$history_link."\n";
		
		$data = $this->clean_content( $a['Html'] );
					
		$posts = get_posts(array(
				'numberposts'	=> -1,
				'post_type'		=> 'manufacturer',
				'meta_key'		=> 'history_link',
				'meta_value'	=> $history_link
		));
		
		$post_id = 0;
		if( !empty( $posts ) ){
			$post_id = $posts[0]->ID;
		}

		if( !empty( $post_id ) ){
			
			print "Update Post=$post_id--->".$post['post_title']."\n";
			// update post
			$post['ID'] = $post_id;
			wp_update_post( $post );
			
		}else{
			
			// create post
			$post_id = wp_insert_post( $post, $wp_error );
			print "Insert Post=$post_id--->".$post['post_title']."\n";
		}
		
		if( !empty( $data['Logo'] ) ){
		
			$attachment_id =  $this->attachment->add_attachment( $data['Logo'] );
			
			update_field( 'field_567b53485e212', $attachment_id, $post_id );
		
		}
		
		//Phone_Number
		if( !empty( $data['Phone_Number'] ) ){
			update_field( 'field_567b50dbe8363', $data['Phone_Number'], $post_id );
		}
		
		//Web_Address
		if( !empty( $data['Web_Address'] ) ){
			update_field( 'field_567b512d6e223', $data['Web_Address'], $post_id );
		}
		
		//Web_Address
		if( !empty( $data['Web_Address'] ) ){
			update_field( 'field_567b512d6e223', $data['Web_Address'], $post_id );
		}
		//Street_Address
		if( !empty( $data['Street_Address'] ) ){
			update_field( 'Street_Address', $data['Street_Address'], $post_id );
		}
		//City
		if( !empty( $data['City'] ) ){
			update_field( 'field_567b517ce06ee', $data['City'], $post_id );
		}
		//State
		if( !empty( $data['State'] ) ){
			update_field( 'field_567b519277c38', $data['State'], $post_id );
		}
		//Country
		if( !empty( $data['Country'] ) ){
			update_field( 'field_567b51a6e7446', $data['Country'], $post_id );
		}
		
		//Zip
		if( !empty( $data['Zip'] ) ){
			update_field( 'field_567b51b4d34fd', $data['Zip'], $post_id );
		}
		//Advertiser_ID
		if( !empty( $data['Advertiser_ID'] ) ){
			update_field( 'field_567b533dbf800', $data['Advertiser_ID'], $post_id );
		}
		
		return $post_id;
		
	}
		
/*
 <root>\n
 <Phone_Number>877-267-2499</Phone_Number>\n
 <Web_Address>www.boschtools.com</Web_Address>\n
 <Street_Address />\n<City />\n<State />\n<Country>us</Country>\n<Zip />\n
 <Advertiser_ID>112302163</Advertiser_ID>\n
 <Logo></Logo>\n</root>
 
 <img src="/CMS/uploadedimages/Images/Gardening/Web_Only/AMIShieldLogo.jpg" alt="American Meadows" title="American Meadows" />
 
 */
	
	public function clean_content( $html ){
		
		$data = array();
		
		$dom = str_get_html( $html  );
		
		$data['Phone_Number'] = $dom->find( 'Phone_Number', 0 )->innertext;
		$data['Web_Address'] = $dom->find( 'Web_Address', 0 )->innertext;
		$data['Street_Address'] = $dom->find( 'Street_Address', 0 )->innertext;
		$data['City'] = $dom->find( 'City', 0 )->innertext;
		$data['State'] = $dom->find( 'State', 0 )->innertext;
		$data['Country'] = $dom->find( 'Country', 0 )->innertext;
		$data['Advertiser_ID'] = $dom->find( 'Advertiser_ID', 0 )->innertext;
		if( $dom->find( 'Logo', 0 ) && $dom->find( 'Logo', 0 )->find('img',0 ) ){
			$data['Logo'] = $dom->find( 'Logo', 0 )->find('img',0 )->src;
		}
		$dom->clear();
		unset( $dom );
		return $data;
		
	}
}

?>

