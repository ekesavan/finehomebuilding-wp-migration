<?php 

class WPAttachment{
	
	/*
	wp_insert_attachment( $attachment, $filename, $parent_post_id );
	attachment : (array) (required) Array of data about the attachment that will be written into the wp_posts table of the database. Must contain at a minimum the keys post_title, post_content (the value for this key should be the empty string), post_status and post_mime_type.
 	filename:(string) (optional) Location of the file on the server. Use absolute path and not the URI of the file. The file MUST be on the uploads directory. See wp_upload_dir()
	parent_post_id : (int) (optional) Attachments may be associated with a parent post or page. Specify the parent's post ID, or 0 if unattached.
	Return Values : Returns the resulting post ID (int) on success or 0 (int) on failure.
	*/
	
	/*
	 * Ektron Images
	 _xlg
	 _lg
	 _med
	 _lgsq
	 _ld
	 _medsq
	 _mth
	 _sprd
	 _sq
	 _th
	 */
	
	
	
	private $ektron_image_suffix = array(
			 '_xlg',
			 '_lg',
			 '_med',
			 '_lgsq',
			 '_ld',
			 '_medsq',
			 '_mth',
			 '_sprd',
			 '_sq',
			 '_th',
	);
	
	private $ektron_large_image_suffix = array(
			'_ld',
			'_xlg',
			'_lg',
			'_med',
			'_medsq',
	);
	
	private $imgge_api;
	
	function __construct(){
		
		$this->image_api = new ImageCrop();
		
	}
	
	function add_author_attachment( $image_data, $post_id = NULL ){

		if( is_array( $image_data ) ){
				
			$image_src = $image_data['image_src'];
			$caption = @$image_data['image_caption'];
			$check_large_image = @$image_data['check_large_image'];
				
		}else{
				
			$image_src = $image_data;
		}
		
		if( empty( $image_src ) ) return;
		
		$upload_dir = wp_upload_dir();
		
		$image_src = preg_replace( '|http://www.finehomebuilding.com|', '', $image_src );
		
		$pathinfo = pathinfo( $image_src );
		$original =  $pathinfo['filename'];
		
		$file_name = $pathinfo['filename'];
		
		$author_image_dst = strtolower( $pathinfo['dirname'] )."/".  $file_name .".".$pathinfo['extension'];
		
		$author_image = strtolower( $pathinfo['dirname'] )."/".  $file_name ."-thumb1.".$pathinfo['extension'];
		
		$this->copy_image( $image_src, $author_image_dst );
		
		$this->image_api->crop_image( $author_image_dst, $author_image, 300, 300 );
		
		$attachment_id = $this->get_attachment_id_by_url( $upload_dir['baseurl'].$author_image, $post_id );
								
		if( !empty( $attachment_id ) ){
			wp_delete_attachment( $attachment_id, true );
			$attachment_id = 0;
		}
		
		if( empty( $attachment_id ) ){
				
			$filetype = wp_check_filetype( basename( $author_image ), null );
				
			$postinfo = array(
					'guid' => $upload_dir['baseurl'].$author_image,
					'post_mime_type'	=> $filetype['type'],
					'post_title'		=> $file_name,
					'post_excerpt' => $caption,
					'post_content'	=> '',
					'post_status'	=> 'inherit',
			);

			$attachment_id = wp_insert_attachment( $postinfo,  $upload_dir['basedir'].$author_image, $post_id );
			
			$attach_data = wp_generate_attachment_metadata( $attachment_id, $upload_dir['basedir'].$author_image );
				
			$res = wp_update_attachment_metadata( $attachment_id,  $attach_data );
			
		}
		
		return $attachment_id;
		
	}
	
	function add_issue_attachment( $image_data, $post_id = NULL ){
	
		if( is_array( $image_data ) ){
	
			$image_src = $image_data['image_src'];
			$caption = @$image_data['image_caption'];
			$check_large_image = @$image_data['check_large_image'];
	
		}else{
	
			$image_src = $image_data;
		}
	
		if( empty( $image_src ) ) return;
	
		$upload_dir = wp_upload_dir();
	
		$image_src = preg_replace( '|http://www.finehomebuilding.com|', '', $image_src );
	
		$pathinfo = pathinfo( $image_src );
		$original =  $pathinfo['filename'];
	
		$file_name = $pathinfo['filename'];
	
		$issue_image_dst = strtolower( $pathinfo['dirname'] )."/".  $file_name .".".$pathinfo['extension'];
	
		$issue_image = strtolower( $pathinfo['dirname'] )."/".  $file_name ."-issue.".$pathinfo['extension'];
	
		$this->copy_image( $image_src, $issue_image_dst );
	
		$this->image_api->crop_image( $issue_image_dst, $issue_image, 360, 450 );
	
		$attachment_id = $this->get_attachment_id_by_url( $upload_dir['baseurl'].$issue_image, $post_id );
	
		/*
		if( !empty( $attachment_id ) ){
			wp_delete_attachment( $attachment_id, true );
			$attachment_id = 0;
		}
		*/
		
		if( empty( $attachment_id ) ){
	
			$filetype = wp_check_filetype( basename( $issue_image ), null );
	
			$postinfo = array(
					'guid' => $upload_dir['baseurl'].$issue_image,
					'post_mime_type'	=> $filetype['type'],
					'post_title'		=> $file_name,
					'post_excerpt' => $caption,
					'post_content'	=> '',
					'post_status'	=> 'inherit',
			);
	
			$attachment_id = wp_insert_attachment( $postinfo,  $upload_dir['basedir'].$issue_image, $post_id );
			
			$attach_data = wp_generate_attachment_metadata( $attachment_id, $upload_dir['basedir'].$issue_image );
				
			$res = wp_update_attachment_metadata( $attachment_id,  $attach_data );
			
		}
	
		return $attachment_id;
	
	}
	
	function add_attachment( $image_data, $post_id = NULL,  $meta_data = false ){
				
		$check_large_image = false;
				
		if( is_array( $image_data ) ){
			
			$image_src = $image_data['image_src'];
			$caption = @$image_data['image_caption'];
			$check_large_image = @$image_data['check_large_image'];
			
		}else{
			
			$image_src = $image_data;
		}
				
		if( empty( $image_src ) ) return;
		
		$image_src = preg_replace( '|http://www.finehomebuilding.com|', '', $image_src );
		
		$pathinfo = pathinfo( $image_src );
		
		/*
		 * check large image exists;
		 * 
		 */
		if( $check_large_image ){
			
			$large_image_src =  $pathinfo['dirname']."/".$pathinfo['filename']."_lgsq.".$pathinfo['extension'];
			
		
			if( $this->remote_file_exists( $large_image_src ) ){
				
				$image_src = $large_image_src;
				$image_dst = strtolower( $pathinfo['dirname'] )."/". $pathinfo['filename']."_lgsq.".$pathinfo['extension'];
				
			}
		
		}
		
		if( empty( $image_dst ) ) {
			
			$image_dst = strtolower( $pathinfo['dirname'] )."/". $pathinfo['basename'] ;
			
		}
		
		
		if( __LOG__ ){
			print "SOURCE: $image_src\n";
			print "DST: $image_dst\n";
		}
		
		$filename = $pathinfo['filename'];
							
		$upload_dir = wp_upload_dir();
		
		if( __LOG__ ) 
			print_R( $upload_dir );
		
			
		$attachment_id = $this->get_attachment_id_by_url( $upload_dir['baseurl'].$image_dst, $post_id );
		
		//$attachment = $this->get_attachment_by_url( $upload_dir['baseurl'].$image_dst, $post_id );
		
		if( __LOG__ )
			print "add_attachment: $attachment_id --->".$upload_dir['basedir'].$image_dst."\n";
		
	    /*
	    * Remove Leading 
	    */		
		$this->copy_image( $image_src, $image_dst );
		
		if( preg_match( "/_xlg/i",$filename ) ){
			$this->image_api->crop_xlg_image( $image_dst, $image_dst );
		}
		
		/*
		if( !empty( $attachment_id ) ){
			wp_delete_attachment( $attachment_id, true );
		 	$attachment_id = 0;
		}
		*/ 
		
		if( empty( $attachment_id ) ){
			
			$filetype = wp_check_filetype( basename( $image_dst ), null );
			
			$postinfo = array(
					'guid' => $upload_dir['baseurl'].$image_dst,
					'post_mime_type'	=> $filetype['type'],
					'post_title'		=> $filename,
					'post_excerpt' => $caption,
					'post_content'	=> '',
					'post_status'	=> 'inherit',
			);
				
			$attachment_id = wp_insert_attachment( $postinfo,   $upload_dir['basedir'].$image_dst, $post_id );
				
			$attach_data = wp_generate_attachment_metadata( $attachment_id, $upload_dir['basedir'].$image_dst );
				
			$res = wp_update_attachment_metadata( $attachment_id,  $attach_data );
				
			//print_R( wp_get_attachment_url( $attachment_id ) );		
			
		}else{
			
							
			$attach_data = wp_generate_attachment_metadata( $attachment_id,  $upload_dir['basedir'].$image_dst );
												
			$res = wp_update_attachment_metadata( $attachment_id,  $attach_data );
			
			
		}
		return $attachment_id;
	}
	
	
	function add_main_image_attachment( $image_data, $post_id = NULL, $ektron =  false  ){
		
		if( __LOG__ )
			print_R( $image_data );
			
		if( is_array( $image_data ) ){
			$image_src = $image_data['image_src'];
			$caption = $image_data['image_caption'];
			$layout = @$image_data['layout'];
				
			if( $layout == "Lead Image"){
				array_unshift ( $this->ektron_large_image_suffix, '_ld' );
			}
			
		}else{
			$image_src = $image_data;
		}
	
		if( empty( $image_src ) ) return;
		
		$upload_dir = wp_upload_dir();
		
		$image_src = preg_replace( '|http://www.finehomebuilding.com|', '', $image_src );
		
		$pathinfo = pathinfo( $image_src );
	
		/*
		 * check large image exists;
		 *
		 */
		$original =  $pathinfo['filename'];
		$file_name = $pathinfo['filename'];
		
		if( __LOG__ )
			print "add_main_image_attachment : $image_src \n";
		$found_images = array();
		
		if( $ektron ){
		
			foreach( $this->ektron_image_suffix as $suffix ){
				
				if( preg_match( "|".$suffix."$|i", $file_name ) ){
					
					$file_name = preg_replace( "|".$suffix."$|i", '', $file_name );
					break;
				}
				
			}
			
			
			
			foreach( $this->ektron_large_image_suffix as $suffix ){
					
				$ektron_image_src = $pathinfo['dirname'] ."/".  $file_name .$suffix.".".$pathinfo['extension']; 		
			
				if( $this->remote_file_exists( $ektron_image_src ) ) {
					$found_images[$suffix] = $ektron_image_src;
				}
				
			}
		}	
		
		$image_src_original =  $pathinfo['dirname'] ."/".  $original.".".$pathinfo['extension'];
		
		if( empty( $found_images ) && !$this->remote_file_exists( $image_src_original )  ){
			return;
		}
		
		/*
		Crop images 16 x 9
		700x394 - Article Main
		460x260 - Top Stories Module
		250x140
		*/
			
		$wp_image1 = strtolower( $pathinfo['dirname'] )."/".  $file_name ."-main.".$pathinfo['extension'];
		$wp_image2 = strtolower( $pathinfo['dirname'] )."/".  $file_name ."-thumb1.".$pathinfo['extension'];
		$wp_image3 = strtolower( $pathinfo['dirname'] )."/".  $file_name ."-thumb2.".$pathinfo['extension'];
		
		$article_images = array( $wp_image1, $wp_image2, $wp_image3 );
		
		
		foreach( $article_images as $dst ){
			
			$attachment_id = $this->get_attachment_id_by_url( $upload_dir['baseurl'].$dst, $post_id );
			
			if( !empty( $attachment_id ) ){
				wp_delete_attachment( $attachment_id, true );
				$attachment_id = 0;
			}
		}
		
		if( !empty ($found_images['_xlg'] ) ){
			$wp_image1_src = $found_images['_xlg'];
		}elseif( !empty ($found_images['_lg'] ) ){
			$wp_image1_src = $found_images['_lg'];
		}elseif( !empty ($found_images['_ld'] ) ){
			$wp_image1_src = $found_images['_ld'];
		}elseif( !empty ($found_images['_sprd'] ) ){
			$wp_image1_src = $found_images['_sprd'];
		}else{
			$wp_image1_src = $image_src_original;
		}
		
		$image1_pathinfo = pathinfo( $wp_image1_src );
		
		$wp_image1_dst = strtolower( $image1_pathinfo['dirname'] )."/".  $image1_pathinfo['filename'] .".".$image1_pathinfo['extension'];	
		
		if( !empty ($found_images['_xlg'] )  ){
			
			$this->copy_image( $wp_image1_src, $wp_image1_dst );
			
			$this->image_api->crop_xlg_image( $wp_image1_dst, $wp_image1_dst );
			
		}else{
			$this->copy_image( $wp_image1_src, $wp_image1_dst );
		}
		
		$this->image_api->resize_main_image( $wp_image1_dst, $wp_image1);
					
		$this->image_api->crop_image( $wp_image1_dst, $wp_image2, 300, 300 );

		$this->image_api->crop_image( $wp_image1_dst, $wp_image3, 460, 260 );
		
		
		$article_images = array( $wp_image1, $wp_image2, $wp_image3 );
		
		if( __LOG__ )
			print_R( $article_images );
				
		$attachment_ids = array();
		
		if( __LOG__ )
			print_R( $upload_dir );
		
		foreach( $article_images as $dst ){
				
		
			$attachment_id = $this->get_attachment_id_by_url( $upload_dir['baseurl'].$dst, $post_id );
			
			
			if( !empty( $attachment_id ) ){
				
				wp_delete_attachment( $attachment_id, true );
				$attachment_id = 0;
			
			}
			
		
			if( __LOG__ )
				print "add_attachment: $attachment_id --->".$upload_dir['baseurl'].$dst."\n";
		
			if( empty( $attachment_id ) ){
					
				$filetype = wp_check_filetype( basename( $dst ), null );
					
				$postinfo = array(
						'guid' => $upload_dir['baseurl'].$dst,
						'post_mime_type'	=> $filetype['type'],
						'post_title'		=> $file_name,
						'post_excerpt' => $caption,
						'post_content'	=> '',
						'post_status'	=> 'inherit',
				);
		
				
				$attachment_id = wp_insert_attachment( $postinfo,  $upload_dir['basedir'].$dst, $post_id );
		
				if( $attachment_id ){
					
					$attach_data = wp_generate_attachment_metadata( $attachment_id, $upload_dir['basedir'].$dst );
					
					$res = wp_update_attachment_metadata( $attachment_id,  $attach_data );
					
				}
				
			}else{
					
				/*
				$post = (array) get_post( $attachment_id );
				
				$post['post_excerpt'] = $caption;
				
				wp_update_post( $post );				

				$attach_data = wp_generate_attachment_metadata( $attachment_id, $upload_dir['basedir'].$dst );
				
				$res = wp_update_attachment_metadata( $attachment_id,  $attach_data );
				*/	
			}
			
			
			
			$attachment_ids[] = $attachment_id;
			
		}
		
		return $attachment_ids;
	
	}
	
	
	function add_pdf_attachment( $pdf, $post_id = NULL, $meta = false ){
		
		if( !preg_match( '/^\//', $pdf ) ){
			$pdf = "/".$pdf;
		}
		
		$pathinfo = pathinfo( $pdf );
	
		$filename = $pathinfo['filename'];
		
		if( empty( $filename ) ) return;
		
		if( $meta ){
			$this->copy_image( $pdf, $pdf );
		}
			
		$upload_dir = wp_upload_dir();
	
		if( __LOG__ )
			print_R( $upload_dir );
	
		$attachment_id = $this->get_attachment_id_by_url( $upload_dir['baseurl'].$pdf, $post_id );
		
	
		if( __LOG__ )
			print "add_attachment: $attachment_id --->".$upload_dir['basedir'].$pdf."\n";
	
		if( empty( $attachment_id ) ){
				
			//$this->copy_image( $pdf );
				
			$filetype = wp_check_filetype( basename( $pdf ), null );
				
			$postinfo = array(
					'guid' => $upload_dir['baseurl'].$pdf,
					'post_mime_type'	=> $filetype['type'],
					'post_title'		=> $filename,
					'post_content'	=> '',
					'post_status'	=> 'inherit',
			);
	
			$attachment_id = wp_insert_attachment( $postinfo,   $upload_dir['basedir'].$pdf, $post_id );
	
			if( $meta ){
				$attach_data = wp_generate_attachment_metadata( $attachment_id,  $upload_dir['basedir'].$pdf );
	
				wp_update_attachment_metadata( $attachment_id,  $attach_data );
			}
		}else{
				
			if( $meta ){
				$attach_data = wp_generate_attachment_metadata( $attachment_id,  $upload_dir['basedir'].$pdf );
				
				wp_update_attachment_metadata( $attachment_id,  $attach_data );
			}
		}
	
		return $attachment_id;
	
	}
	
	function get_attachment_id_by_url( $wp_url,$post_id ) {

		global $wpdb;
		$attachment = $wpdb->get_col( $wpdb->prepare( "SELECT ID FROM {$wpdb->prefix}posts WHERE ID=%d AND guid='%s';", $post_id, $wp_url ) );
		return @$attachment[0];
		
	}
	
	function get_attachment_by_url( $wp_url,$post_id ) {
	
		global $wpdb;
		$attachment = $wpdb->get_col( $wpdb->prepare( "SELECT * FROM {$wpdb->prefix}posts WHERE ID=%d AND guid='%s';", $post_id, $wp_url ) );
		
		print_R( $attachment );
		
		return @$attachment[0];
	
	}
	
	function remote_file_exists($image_src){
		
		if( preg_match( '|^http://www.finehomebuilding.com|', $image_src ) ){
			$url = $image_src;
		}else{
			$url = __FHB_HOST__.$image_src;
		}
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
		curl_setopt($ch, CURLOPT_HEADER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		$data = curl_exec($ch);		
		
		$result = curl_getinfo($ch);
		
		if( __LOG__ ){
			error_log( print_R( $result, true ) );
		}
		
		if(  $result['content_type']  == "text/html" ) return false;
		
		if( __LOG__ )
			error_log( "remote_file_exists : $url");
		
		return true;
	}
	
	function copy_image(  $image_src, $image_dst ){
		
		$upload_dir = wp_upload_dir();
		
		
		if( preg_match( '/^\//', $image_dst ) ){
			$file = $upload_dir['basedir'].$image_dst;
		}else{
			$file = $upload_dir['basedir']."/".$image_dst;
		}
		
		if( __LOG__ )
			print "copy_image :$image_src --> $file\n";
		
		
		if( file_exists( $file ) ){
			if( filesize( $file ) > 0 )
				return;
		}
		
		$pathinfo = pathinfo( $image_src );
		$src_file = $upload_dir['basedir'].strtolower( $pathinfo['dirname'] )."/".  $pathinfo['filename'] .".".$pathinfo['extension'];
		
		if( file_exists( $src_file ) && ( filesize( $src_file ) > 0 )){
			if (!copy($src_file, $file)) {
				echo "failed to copy $file...\n";
			}else{
				echo "success to copy $file...\n";
				return;
			}
		}
		
		if( preg_match( '/^\//', $image_src ) ){
			$url = __FHB_HOST__.$image_src;
		}else{
			$url = __FHB_HOST__."/".$image_src;
		}
		
			
		if( __LOG__ )
			print "File Not found copy from remote : $url \n";
		
		$dir = dirname( $file );
		
		if( __LOG__ )
			print $dir."\n";
			
		if( ! file_exists( $dir ) ){
			print mkdir( $dir, 0755, true );
			print "\n";
		}
		
		if( __LOG__ )
			print "start copy image $url \n";
		
			$ch = curl_init( $url  );
		$fp = fopen( $file, 'w');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
		curl_setopt($ch, CURLOPT_FILE, $fp);
		//curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_exec($ch);
		curl_close($ch);
		fclose($fp);
		
		if( __LOG__ )
			print "end copy image \n";
	
	}
	
	function copy_pdf( $pdf ){
	
		$file = WP_UPLOAD_DIR.$pdf;
	
		if( !file_exists( $file ) ){
			$dir = dirname( $file );
			//print $dir."\n";
	
			if( ! file_exists( $dir ) ){
				print mkdir( $dir, 0775, true );
				print "\n";
			}
			$url = __FHB_HOST__.$image;
			//print "$url\n";
			//print "start copy image $file \n";
			$ch = curl_init( $url  );
			$fp = fopen( $file, 'w');
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
			curl_setopt($ch, CURLOPT_FILE, $fp);
			//curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_exec($ch);
			curl_close($ch);
			fclose($fp);
			//print "end copy image \n";
		}
	
	}
	
	
	/* 
	 * EKTRON  IMAGE HTML
	 * 
	 <Image>
	 <Article_Image>
	 <img alt="" src="/CMS/uploadedImages/Images/Homebuilding/Articles/h00002_05.jpg" />
	 </Article_Image>
	 <Click_Action>None</Click_Action>
	 <Destination_URL></Destination_URL>
	 <Image_Credit></Image_Credit>
	 <Image_Caption>Mortar joints must be exact. A special trowel helps to ensure precise mortar thickness for block walls. Joints are flush with the wall.</Image_Caption>
	 </Image>
	 */
	
	public function parse_ektron_image( $image, &$inline_images ){
		
		$wp_image_html = '';
		
		if( $image->find("img",0 ) ){
		
			$image_src = $image->find("img",0 )->src;
		
			$image_caption = $image->find('image_caption', 0) ? $image->find('image_caption', 0)->innertext : '';
		
			$image_credit =  $image->find('image_credit', 0) ? $image->find('image_credit', 0)->innertext : '';
			
			$click_action = '';
			if(  $image->find('Click_Action', 0) )
				$click_action = $image->find('Click_Action', 0)->innertext;
			
			$href = $image->find("img",0 )->src; 
			
			if( $click_action == 'Enlarge' ){
				$inline_images[] = array(
						'image_src' => $this->enlarge_image_path( $image_src ),
						'image_caption' => implode( '<br/>', array($image_caption, $image_credit) )
				);
				$href= __S3_URL__.CONTENT_DIR."/uploads". $this->lowercase_path($this->enlarge_image_path( $image_src ));
			}
				
		
			$inline_images[] = array(
					'image_src' => $image_src,
					'image_caption' => implode( '<br/>', array($image_caption, $image_credit) )
			);
		
			$image->find("img",0 )->src = __S3_URL__.CONTENT_DIR."/uploads". $this->lowercase_path( $image_src );
			
			if( $image->prev_sibling() ){
				$image->find("img",0 )->class = $this->find_image_class( $image->prev_sibling()->innertext );
			}
			
			
			
			$wp_image_html = '[caption id="attachment" align="alignright"]';
			$wp_image_html .= '<a href="'.$href.'" rel="attachment">';
			$wp_image_html .= '<img class="alignright" src="'.$image->find("img",0 )->src.'" alt="image alt text" /></a>';
			$wp_image_html .= implode( '<br/>', array($image_caption, $image_credit) ). '[/caption]';
			
			if( $image->prev_sibling() ){
				$image->find("img",0 )->class = $this->find_image_class( $image->prev_sibling()->innertext );
				//$wp_image_html .= $this->find_image_class( $image->prev_sibling()->innertext );
			}
		}
			
		
		return $wp_image_html;
	}
	
	
	public function lowercase_path( $file ){
		$pathinfo = pathinfo( $file );
		$new = strtolower( $pathinfo['dirname'] )."/". $pathinfo['basename'] ;
		return $new;
	}
	
	
	public function  enlarge_image_path( $image_src ){
		$pathinfo = pathinfo( $image_src );
		
		/*
		 * check large image exists;
		 *
		 */
		$large_image_src =  $pathinfo['dirname']."/".$pathinfo['filename']."_xlg.".$pathinfo['extension'];
		
		
		return $large_image_src;
		
	}
	
	/*
	 * Large Square Image
	 *
	 */
	
	public function lgsq_image_path( $image_src ){
		$pathinfo = pathinfo( $image_src );
	
		$lgsq_image_path =  $pathinfo['dirname']."/".$pathinfo['filename']."_lgsq.".$pathinfo['extension'];
	
		return $lgsq_image_path;
	}
	
	public function find_image_class(){
		return "alignright";
	}
	
	
	public function check_remote_image_exists( $image_src, $type ){
		$pathinfo = pathinfo( $image_src );
		$type_image_path =  $pathinfo['dirname']."/".$pathinfo['filename']."_$type.".$pathinfo['extension'];
		if( $this->remote_file_exists($type_image_path) ){
			return $type_image_path;
		}
		return false;
	}
	
	/*
	
	* meta_key: media_credit
	* meta_value: image credit
	
	*/
}

?>