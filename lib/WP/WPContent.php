<?php

class WPContent{
	
	public  $db;
	
	public $wp_term;
	
	public $attachment;
	
	public $attachment_ids = array();
	
	function __construct(){
		
		$this->db =  new FHBMongo('Ektron');
		
		$this->ci_db =  new FHBMongo('CodeIgniter');
		
		$this->wp_map_db =   new FHBMongo('WPMap');
		
		$this->wp_term = new WPTerm();
		
		$this->wp_video = new WPVideo();
		
		$this->attachment = new WPAttachment();
	}
	
	
	public function post_deck( $html ){
	
		//<SubHead>AÂ great wall system that masons love to hate</SubHead>
	
		$sub_head = "";
	
		$dom = str_get_html( $html  );
	
		if( $dom->find( "SubHead", 0 ) ){
			$sub_head = $dom->find( "SubHead", 0 )->innertext;
			$sub_head = strip_tags( $sub_head );
		}
	
		$dom->clear();
		unset( $dom );
	
		return $sub_head;
	
	}
	
	public function post_excerpt( $html ){
				
		$abstract = '';
		
		$dom = str_get_html( $html  );
		
		if( $dom->find( "Abstract", 0 ) ){
			$abstract = $dom->find( "Abstract", 0 )->innertext;
		}
		
		$dom->clear();
		unset( $dom );
		
		return $abstract;

	}
	
	public function clean_content( $html ){
		
		if( __LOG__ )
			print $html."\n\n";		
		
		$content = '';
		
		$inline_images = array();
		
		$slides = array();
		
		/*
		 <SubHead>Greek and Roman orders of architecture can help establish molding size and placement</SubHead>
			<Abstract></Abstract>
			<Alternate_Formats>
			<Type>None</Type>
			<Filename></Filename>
			<Filesize></Filesize>
			<Description></Description>
			</Alternate_Formats>
			<Primary_Image_Section>
			<PrimaryImage></PrimaryImage>
			<Click_Action>None</Click_Action>
			<Destination_URL></Destination_URL>
			<Image_Credit></Image_Credit>
			<Image_Caption></Image_Caption>
			</Primary_Image_Section>
		*/
		
		$dom = str_get_html( $html  );
		
		/* Fine href */
		foreach( $dom->find( 'Content_Section') as $Content_Section ){
		
			foreach( $Content_Section->find('a') as $a ){
				error_log( $a->href );
				
				if( !empty( $a->href ) ){
					if( preg_match( "|^http:\/\/(www\.)?finehomebuilding\.com|i", $a->href  ) ){
						$a->href = preg_replace( "|^(http:\/\/www\.finehomebuilding\.com)(.*?)$|", '\\2', $a->href );
						$a->href = preg_replace( "|^(http:\/\/finehomebuilding\.com)(.*?)$|", '\\2', $a->href );
						
					}
				}
			}
		}
		
		
		/* Find Total Images */
		$total_images = 0;
		if( $dom->find( 'Content_Section') ){
			foreach( $dom->find( 'Content_Section') as $Content_Section ){
				
				foreach( $Content_Section->find('img') as $img ){
					
					if( preg_match( "/icon_zoom/", $img->src ) ){
						continue;
					}
					
					++$total_images;
				}
			}
		}
		
		$main_image_url = '';
		
		if( $total_images <= 1 ){
			if( $dom->find('Primary_Image_Section', 0 ) && $dom->find( 'Primary_Image_Section', 0 )->find( 'PrimaryImage', 0 )->find('img',0) ){
			
				$main_image_url = $dom->find( 'Primary_Image_Section', 0 )->find( 'PrimaryImage', 0 )->find('img',0)->src;
			
			}
		}
		
		/*
		if( $dom->find( 'SubHead', 0 ) ){
			$content .=  "<h3>".$dom->find( 'SubHead', 0 )->innertext."</h3>";
		}
		*/
		
		/*
		if( $dom->find( 'Abstract', 0 ) ){
			$content .=  "<p>".$dom->find( 'Abstract', 0 )->innertext."</p>";
		}
		*/
			
		if( $dom->find( 'Content_Section', 0 ) ){
			
			
			$i = 1;
			
			foreach( $dom->find( 'Content_Section') as $Content_Section ){
				
				$layout = $Content_Section->find( 'Layout', 0 )->innertext;
				
				
				
				/*
				 * Remove the primary image from content section
				 * 
				 */
				if( ($i == 1) && !empty( $main_image_url ) ){
					if(  $Content_Section->find('Image', 0 )->find('img',0) ){
						if( $Content_Section->find('Image', 0 )->find('img',0)->src == $main_image_url ) {
							$Content_Section->find('Image', 0 )->find('img',0)->src = '';
						}
					}
				}
				
				if( $layout == 'Lead Image'){
					
					ob_start();
					require( dirname(__FILE__).'/../../templates/content-section-lead-image.php');
					$content .= ob_get_clean();
				
				}elseif(  $layout == '3 Up' ){
				
					ob_start();
					require( dirname(__FILE__).'/../../templates/content-section-3up.php');
					$content .= ob_get_clean();
						
				}elseif(  $layout == '2 Up' ){

					ob_start();
					require( dirname(__FILE__).'/../../templates/content-section-2up.php');
					$content .= ob_get_clean();
					
				}elseif(  $layout == '1 Up' ){

					ob_start();
					require( dirname(__FILE__).'/../../templates/content-section-1up.php');
					$content .= ob_get_clean();
					
				}elseif(  $layout == 'None' ){
					
					ob_start();
					require( dirname(__FILE__).'/../../templates/content-section-none.php');
					$content .= ob_get_clean();
					
				}elseif(  $layout == 'Vertical' ){
						
					ob_start();
					require( dirname(__FILE__).'/../../templates/content-section-vertical.php');
					$content .= ob_get_clean();
						
				}elseif(  $layout == 'Slideshow' ){
							
					ob_start();
					require( dirname(__FILE__).'/../../templates/content-section-slideshow.php');
					$content .= ob_get_clean();
					
				}else{

					print "!!!!!!!!!!!!!!!!!!!!!!MISSING LAYOUT : $layout \n\n";
					print $Content_Section."\n";
					die;
					
				}
				++$i;				
				
			}
			
		}else if(  $dom->find( 'Question', 0 ) ) {
			error_log( dirname(__FILE__).'/../../templates/question-answer.php' );
			
			ob_start();
			require( dirname(__FILE__).'/../../templates/question-answer.php');
			$qa = ob_get_clean();
			$content .= $qa;
			
			
		}else{
			error_log( dirname(__FILE__).'/../../templates/paragraph-section.php' );
			ob_start();
			require( dirname(__FILE__).'/../../templates/paragraph-section.php');
			$content .= ob_get_clean();
		}
		
		
		/*
		<Article_Credits>Brent Hull, author of Historic Millwork (John Wiley &amp; Sons, 2003), is the owner of Hull Historical Millwork (<a href="http://www.hullhistorical.com/">www.hullhistorical.com</a>) in Fort Worth, Texas.</Article_Credits>
		*/
		if( preg_match( '/^\s+$/si',$content ) ){
			$content = '';
		}
		if( empty( $content ) ){
			list( $content, $abstract_inline_images ) = $this->abstract_clean_content( $html  );
			$inline_images = array_merge( $inline_images,  $abstract_inline_images );
		}
		
		$content = preg_replace( '|<paragraph>|i', '', $content );
		$content = preg_replace( '|</paragraph>|i', '', $content );
		if( preg_match( '/^\s+$/si',$content ) ){
			$content = '';
		}

		if( $dom->find( 'Article_Credits', 0 ) && !empty( $content ) ){
			if( !empty( $dom->find( 'Article_Credits', 0 )->innertext ) ){
				$content .=  "<p>".$dom->find( 'Article_Credits', 0 )->innertext."</p>";
			}
		}
		
		$dom->clear();
		unset( $dom );
		
		return array( $content, $inline_images, $slides );
		
	}
	
	/*
	 <Content_Section>
	 <Paragraph_Heading>A New Roof Over the Old One</Paragraph_Heading>
	 <Paragraph><p title=\"temporary paragraph, cl ick here to add a new paragraph\">Cap a swaybacked roof with a layer of new rafters and foam</p>\n</Paragraph>
	 <Destination_URL></Destination_URL>
	 <Destination_URL_Text>< /Destination_URL_Text>
	 <Image>
	 <Article_Image><img border=\"0\" src=\"/CMS/uploadedimages/Images/Homebuilding/slideshows/H190BE-13500023.jpg\" alt=\"c00236_01.jpg\" style=\ "border: 0px solid;\" title=\"c00236_01.jpg\" />
	 </Article_Image>\n<Click_Action>None</Click_Action>\n<Destination_URL></Destination_URL>\n<Image_Credit></Image_Credit>\n<Im age_Caption></Image_Caption>\n</Image>\n
	 <Embedded_Content>
	 <Content><a target=\"_blank\" href=\"/how-to/articles/a-new-metal-roof-over-the-old-one-with-new-rafters-and-polyu rethane-foam-insulation.aspx\" title=\"A New Roof Over the Old One\">A New Roof Over the Old One</a></Content>\n
	 <Style>Embedded</Style>\n</Embedded_Content>\n
	 </Content_Section>
	 */
	public function slideshow_content( $html ){
				
		$dom = str_get_html( $html  );
		
		$content = '';
		
		$content .= $dom->find( 'Description', 0 )->innertext;
		
		
		$slides =  array();
		
		foreach( $dom->find( 'Content_Section' ) as $Content_Section ){
			
			error_log( $Content_Section->innertext );
			
			$slide = array();
			
			$title = $Content_Section->find( 'Paragraph_Heading', 0 )->innertext;
			if( empty( $title ) ){
				if( $Content_Section->find( "Embedded_Content", 0 ) ){
					$title = $Content_Section->find( "Embedded_Content", 0 )->find( "Content", 0 )->find( "a", 0 )->innertext;
				}
			}
			
			$link = '';
			$Destination_URL = '';
			if( $Content_Section->find( 'destination_url', 0 ) && !empty( $Content_Section->find( 'destination_url', 0 )->innertext )){
				$href = $Content_Section->find( 'destination_url', 0 )->innertext;
				$link = $this->wp_post_link( $href );
				$Destination_URL = $link;
			}
			
			$Destination_URL_Text = '';
			if( $Content_Section->find( 'Destination_URL_Text', 0 ) && !empty( $Content_Section->find( 'Destination_URL_Text', 0 )->innertext )){
				$Destination_URL_Text = $Content_Section->find( 'Destination_URL_Text', 0 )->innertext;
			}
			
			$description = '';
			if( !empty( $Content_Section->find( 'Paragraph', 0 )->innertext ) ){
				
				$description = $Content_Section->find( 'Paragraph', 0 )->innertext;
			}
			
						
			$images = array();
			
			if( $Content_Section->find( 'Image', 0 )->find('Article_Image', 0 )->find('img',0) ){
				
				$image_src = $Content_Section->find( 'Image', 0 )->find('Article_Image', 0 )->find('img',0)->src;
				
				$slide['image'] =  $this->attachment->lgsq_image_path( $image_src );
				
			}
			
			if( $Content_Section->find( "Embedded_Content", 0 ) ){
				$a = $Content_Section->find( "Embedded_Content", 0 )->find( "Content", 0 )->find( "a", 0 ); 
				$ref_link = $a->href;
				/*
				 * /articles/article.aspx?id=111908
				 * /how-to/departments/how-it-works/ice-dams.aspx
				 */
				$ref_article = '';
				if( preg_match( "|/articles/|", $ref_link, $m ) ){
					if( preg_match( "|\?id=(\d+)|", $ref_link, $m1 ) ){
						$content_id = $m1[1];
						
						$ref_article = $this->db->findOne( 'article', array( 'Id' => $content_id ) );
						
						if( empty($ref_article) ){
							
							$ref_article = $this->db->findOne( 'department_article', array( 'Id' => $content_id ) );
							
						}
							
					}else{
						$ref_article = $this->db->findOne( 'article', array( 'Quicklink' => $ref_link ) );
					}
				}elseif( preg_match( "|/departments/|", $ref_link, $m )  ) {
					if( preg_match( "|\?id=(\d+)|", $ref_link, $m1 ) ){
						$content_id = $m1[1];
						$ref_article = $this->db->findOne( 'department_article', array( 'Id' => $content_id ) );
						
						if( empty($ref_article) ){
							$ref_article = $this->db->findOne( 'article', array( 'Id' => $content_id ) );
						}
					}else{
						$ref_article = $this->db->findOne( 'department_article', array( 'Quicklink' => $ref_link ) );
					}
				}
				
				if( !empty( $ref_article ) ){
					$html = $ref_article['Html'];
					$dom2 = str_get_html( $html  );
					
					$main_image = $dom2->find( "Primary_Image_Section", 0 )->find( "PrimaryImage", 0 )->find( "img", 0 )->src;
					
					if( empty( $description ) ){
						$description = $dom2->find( "SubHead", 0 )->innertext;
					}
				
					if( empty(  $slide['image'] ) ){
						$slide['image'] =  $this->attachment->lowercase_path( $main_image );
					}
					
					$link = $this->wp_post_link( $ref_article['Quicklink'] );
					
					$dom2->clear();
					unset( $dom2 );
				}
				
				
				
			}
			$slide['description'] = '';
			
			if( !empty( $title ) ){
				$slide['description'] =  "<a href=\"$link\" >".$title."</a><br/><br/>";
			}
			
			$slide['description'] .= $description;
			
			if( !empty($Destination_URL_Text) ){
				$slide['description'] .=  "<br/><a href=\"$Destination_URL\" >".$Destination_URL_Text."</a><br/><br/>";
			}
			error_log( print_R( $slide, true  ) );
			
			$slides[] = $slide;
			
		}
		
		$dom->clear();
		unset( $dom );
		
		return array( $content, $slides );
	}

	
		/*
		 * Main Image
		 <Primary_Image_Section>
			<PrimaryImage>
			<img alt="h00002_04.jpg" src="/CMS/uploadedImages/Images/Homebuilding/Articles/h00002_04.jpg" />
			</PrimaryImage>
			<Click_Action>None</Click_Action>
			<Destination_URL></Destination_URL>
			<Image_Credit></Image_Credit>
			<Image_Caption></Image_Caption>
			</Primary_Image_Section>
		 */
	public function main_image( $html, $post_id ){
		

		$attachment_id = '';
		
		$attachment_ids = array();
		
		$dom = str_get_html( $html  );
		
		$Primary_Image_Section = $dom->find( "Primary_Image_Section", 0 );
		
		if( $Primary_Image_Section ){
			
			$main_image = $Primary_Image_Section->find( "PrimaryImage", 0 )->find( "img", 0 );
			
			if( $main_image ){
				
				$image_src = $main_image->src;
				
				foreach( $dom->find('Image') as $ci ){
					if( $ci->find( 'Article_Image',0 )->find('img',0) ){
						if( $ci->find( 'Article_Image',0 )->find('img',0)->src == $image_src ){
							
							$image_caption = $ci->find('image_caption', 0)->innertext;
				
							$image_credit =  $ci->find('image_credit', 0)->innertext;
				
						} 
					}
				}
				
				/*
				$image_caption = $Primary_Image_Section->find('image_caption', 0)->innertext;
				
				$image_credit =  $Primary_Image_Section->find('image_credit', 0)->innertext;
				*/
				
				if( !empty( $image_credit ) &&  !empty( $image_caption ) ){
					$image_caption = implode( '<br/>', array($image_caption, $image_credit) );
				}else{
					$image_caption = @$image_caption ? $image_caption : @$image_credit;
				}
					
				$image = array(
						'image_src' => $image_src,
						'image_caption' =>  $image_caption,
				);
				
				$attachment_ids =  $this->attachment->add_main_image_attachment( $image, $post_id, true );
				
			}
		}
		
		$dom->clear();
		unset( $dom );
		
		return $attachment_ids;
		
	}
	
	public function abstarct_main_image( $html, $post_id ){
		
		$dom = str_get_html( $html  );
		
		$attachment_ids = array();
	
		if( $dom->find( 'Abstract', 0 ) && $dom->find( 'Abstract', 0 )->find('img',0 )){
			
			
			$main_image = $dom->find( 'Abstract', 0 )->find('img',0);
				
			if( $main_image ){
	
				$image_src = $main_image->src;
				$image_caption = '';
				$image = array(
						'image_src' => $image_src,
						'image_caption' =>  $image_caption,
				);
	
				$attachment_ids =  $this->attachment->add_main_image_attachment( $image, $post_id, true );
			}
		}
	
		$dom->clear();
		unset( $dom );
	
		return $attachment_ids;
	
	}
	
	
	
	public function post_date( $date ){
		
		$time =  strtotime( $date );
		return date( 'Y-m-d H:i:s', $time );
	}
	
	public function post_date_gmt( $date ){
		
		$time =  strtotime( $date );

		return gmdate ( 'Y-m-d H:i:s', $time );
	
	}
	
	public function post_category( $ids ){
	    print "post_category : $ids \n";
		$taxonomy_ids = preg_split( "/;/", $ids );
		
		$topics = array();
		
		foreach(  $taxonomy_ids as $id ){
			$topics[] = $this->db->findOne( 'taxonomy', array( 'id' => $id ) );
		}
	
		$wp_topics = array();
		$category_ids = array();
	
		$tags = array();
	
		foreach( $topics as $t ){
				
				
			$c = $this->db->findOne( 'csv_taxonomy', array( 't' => $t['title'] ) );
				
			if( empty( $c ) ){
	
	
				$taxonomy = $this->db->findOne( 'taxonomy', array( 'id' => $t['id'] ) );
				$tags[] = $taxonomy['title'];
	
				$parent = $this->db->findOne( 'taxonomy', array( 'id' => $t['parent']) );
	
				if( !empty( $parent ) ) {
						
					$c = $this->db->findOne( 'csv_taxonomy', array( 't' => $parent['title'] ) );
					$wp_topics[$t['title']] = $c['category_id'];
						
				}
	
			}else {
	
				$wp_topics[$t['title']] = $c['category_id'];
	
			}
	
		}
	
		foreach( $wp_topics as $t => $id ){
			$category_ids[] = $id;
		}
	
		$category_ids = array_unique( $category_ids );
		$category_ids = array_filter( $category_ids );
		
		if( __LOG__ )
			print_R( $category_ids );
	
		return array( $category_ids, $tags );
	
	}
	
	public function post_category_old( $ids ){
		
		$taxonomy_ids = preg_split( "/;/", $ids );
		$topics = array();
		foreach(  $taxonomy_ids as $id ){
			$topics[] = $this->db->findOne( 'taxonomy', array( 'id' => $id ) );
		}
		
		$wp_topics = array();
		$category_ids = array();
		
		$tags = array();
		
		foreach( $topics as $t ){
			
			
			$c = $this->db->findOne( 'menu_taxonomy', array( 'title' => $t['title'] ) );
			
			if( empty( $c ) ){
				
				
				$taxonomy = $this->db->findOne( 'taxonomy', array( 'id' => $t['id'] ) );
				$tags[] = $taxonomy['title'];
				
				$parent = $this->db->findOne('taxonomy',  array( 'id' => $t['parent']) );
				
				if( !empty( $parent ) ) {
					
					$c = $this->db->findOne( 'menu_taxonomy', array( 'title' => $parent['title'] ) );
					$wp_topics[$t['title']] = $c['category_id'];
					
				}
				
			}else {
				
				$wp_topics[$t['title']] = $c['category_id'];
				
			}
		
		}
		
		foreach( $wp_topics as $t => $id ){
			$category_ids[] = $id;
		}
		
		$category_ids = array_unique( $category_ids );
		$category_ids = array_filter( $category_ids );
		
		return array( $category_ids, $tags );
		
	}
	
	public function tags_input( $tags, $keywords_str  ){
		
		$keywords = preg_split( "/;/", $keywords_str );
		$tags = array_merge( $tags, $keywords);
		$tags = array_unique( $tags );
		$tags = array_filter( $tags );
		
		return $tags;
		
	}
	
	
	public function post_author(  $ids ){
		
		$authors_ids = preg_split( "/;/", $ids );
		$authors = array();
		$wp_authors_ids = array();
		
		foreach(  $authors_ids as $id ){
			$author = $this->db->findOne( 'fhb_author', array( 'Id' => $id ) );
			$wp_authors_ids[] = $author['wp_user_id'];
		}
		
		return $wp_authors_ids;
	}
	
	/*
	 <Alternate_Formats>
	 \n<Type>PDF</Type>\n
	 <Filename>Service_Equipment</Filename>\n
	 <Filesize></Filesize>\n
	 <Description></Description>
	 \n</Alternate_Formats>\
	
	 */
	
	public function is_pdf_content( $html ){

		$dom = str_get_html( $html  );
		$Alternate_Formats = $dom->find( "Alternate_Formats", 0 );
		$pdf = false;
		if( !empty( $Alternate_Formats ) ){
			$Filename = $Alternate_Formats->find( "Filename", 0 )->innertext;
			if( !empty( $Filename )  ){
				$pdf = true;
			}
		}
		
		$dom->clear();
		unset( $dom );
		return $pdf;	
	}
	
	public function alternate_formats( $html,$post_id ){
		$id = "";
		
		$dom = str_get_html( $html  );
	
		$Alternate_Formats = $dom->find( "Alternate_Formats", 0 );
	
		if( !empty( $Alternate_Formats ) ){
		
			$Filename = $Alternate_Formats->find( "Filename", 0 )->innertext;
			if( !empty( $Filename )  ){
				$Filename = "/pdf/".$Filename.".pdf";
				$id = $this->attachment->add_pdf_attachment( $Filename, $post_id );
			}
		}
	
		$dom->clear();
		unset( $dom );
		
		return $id;
	}
	
	
	
	
	public function wp_post_link(  $link ){

		if( !preg_match( "/^http/", $link ) ){
			$fhb_history_link = "http://www.finehomebuilding.com".$link;
		}else{
			$fhb_history_link = $link;
		}
		$posts = get_posts(array(
				'numberposts'	=> 1,
				'post_type'		=> 'post',
				'meta_key'		=> 'fhb_history_link',
				'meta_value'	=> $fhb_history_link
		));
		
		$post_id = 0;
		if( !empty( $posts ) ){
			$link = $posts[0]->guid;
		}
		
		return $link;
		
	}
	
	public function find_image_class(){
		return "alignright";
	}
	
	public function lowercase_path( $file ){
		$pathinfo = pathinfo( $file );
		$new = strtolower( $pathinfo['dirname'] )."/". $pathinfo['basename'] ;
		return $new;
	}
	
	public function set_fhb_author( $post_authors, $post_id ){
		
		update_field( 'field_56a3aac2ed181', $post_authors, $post_id );
		
	}
	
	public function set_fhb_deck( $deck, $post_id ){
		
		update_field( 'field_56a3ab57bc4eb', $deck, $post_id );
		
	}
	
	public function set_fhb_main_image( $attachment_ids, $post_id, $set_main_image = false  ){
		
		if( !empty( $attachment_ids[2]  ) ){
			update_post_meta( $post_id, '_thumbnail_id', $attachment_ids[2] );
		}
		
		/* Set Main Image */
		//error_log( print_R( $attachment_ids, true ) );
		
		if( $set_main_image ){
			update_field( 'field_56a3ab823d94f', $attachment_ids[0], $post_id );
		}else{
			update_field( 'field_56a3ab823d94f', '', $post_id );
		}
		
		if( !empty( $attachment_ids[1] ) ){
			update_field( 'field_56afdb0ba03cb', $attachment_ids[1], $post_id );
		}else{
			update_field( 'field_56afdb0ba03cb', '', $post_id );
		}
		
		//set_post_thumbnail( $post_id, $attachment_ids[2] );
		
		
	}

	public function remove_fhb_main_image( $post_id ){
	
		update_field( 'field_56a3ab823d94f', '', $post_id );
	
		update_field( 'field_56afdb0ba03cb', '', $post_id );
	
		set_post_thumbnail( $post_id, '' );
	
	}
	
	
	public function set_fhb_history_link( $history_link, $post_id ){
		
		update_field( 'field_56a3abe1da42c', $history_link, $post_id );
		
	}
	
	public function set_content_access( $access, $post_id ){
		
		$access_type = '';
		if( $access == 'Subscription Required' ){
			$access_type = "Premium";
		}else{
			$access_type = "Free";
		}
		
		update_field( 'field_5679b1d3b70e4', $access_type, $post_id );
	}
	
	/*
	 "Name" : "Companion Content",
	 "Value" : "",
	 */
	public function set_companion_content ( $ids,  $post_id ){
		$this->set_related_posts( 'field_56a3ba9d1fb6a',  $ids,  $post_id  );
	}
	
	/*
	 "Name" : "Associated Products",
	 "Value" : ""
	*/
	public function set_associated_products ( $ids,  $post_id ){
	
		$this->set_related_posts( 'field_56a3ba9d200ae',  $ids,  $post_id  );
	}
	
	/*
	"Name" : "Taunton Product Attribution",
	"Value" : "",
	*/
	public function taunton_product_attribution ( $ids,  $post_id ){
	
		$this->set_related_posts( 'field_56a3ba9d2034a',  $ids,  $post_id  );
	}
	
	
	public function set_related_posts( $field_id, $ids,  $post_id ){
		
		$content_ids = preg_split( "/;/", $ids );
		$wp_post_ids = array();
		foreach( $content_ids as $content_id ){
			$m = $this->wp_map_db->content->findOne( array( 'cms_id' => $content_id ) );
			$wp_post_ids[] = $m['wp_id'];
		}
		
		if( !empty( $wp_post_ids ) ){
			update_field( $field_id, $wp_post_ids, $post_id );
		}
	}
	
	
	public function set_title_issue( $title_issue, $post_id ){
		
		update_field( 'field_5679b237edf85',$title_issue, $post_id );
		
	}
	
	
	public function set_page_number( $page_number, $post_id ){
		
		update_field( 'field_5679da2de9468',$page_number, $post_id );
		
	}
	
	public function set_pdf_attachment( $pdf_attachement_id, $post_id ){
		
		update_field( 'field_5679bc26646bb',$pdf_attachement_id, $post_id );
		
	}
	
	
	public function add_redirects( $source, $target ){
		
		
		$item = Red_Item::get_for_url( $source, 'url' );
		
		$target = preg_replace( "|".home_url()."|", '', $target );
		
		if( empty( $item  ) ){
		
			$data = array(
					'source' => $source,
					'match' => 'url',
					'red_action' => 'url',
					'target' => $target,
					'group_id' => 1,
					'add' => 'Add Redirection',
					'group' => 0,
					'action' => 'red_redirect_add',
					'_wpnonce' => '541664d5f2',
					'_wp_http_referer' => '/wp-admin/tools.php?page=redirection.php',
			);
			print "Add redirect $source --> $target\n";
			$item = Red_Item::create( $data );
			
		}
		//print_R( $item );
	}
	
	
	/*
		<Contributor>
		<Full_Name>Corry M. Dodson</Full_Name>
		<City>Careywood</City>
		<State>ID</State>
		<Province></Province>
		<Country>us</Country>
		</Contributor>
	*/		

	public function Contributor( $Contributor ){
		$tags = array( 'Full_Name', 'City', 'State' );
		$values;
		foreach( $tags as $t ){
			if( !empty( $Contributor->find( $t, 0 )->innertext ) )
				$values[] = $Contributor->find( $t, 0 )->innertext;
		}
		
		return !empty($values)? implode( ", ", $values ) : '';
	}
	
	
	public function abstract_clean_content( $html ){
	
		$content = '';
	
		$inline_images = array();
		
		$dom = str_get_html( $html  );
		
	
		if( $dom->find( 'Abstract', 0 ) ){
			
			foreach( $dom->find( 'Abstract', 0 )->find('img') as $image ){
				/* <img alt="" title="subscription required" src="http://www.finehomebuilding.com/images/forpay_icon.gif" /> */
				if( preg_match( "/forpay_icon|member-only-caption/", $image->src ) ){
					$image->outertext = '';
					continue;
				}
				$image_src = $image->src;
				
				//$section_image = __S3_URL__.CONTENT_DIR."/uploads". $this->attachment->lowercase_path( $image_src );
				$attachment_id = $this->attachment->add_attachment(  $image_src , 0 );
				$section_image = wp_get_attachment_url( $attachment_id );
				
				$inline_images[] = array(
						'image_src' => $image_src,
						'image_caption' => '',
				);
				$image->src = $section_image;
			}
				
			foreach(  $dom->find( 'Abstract', 0 )->find('a') as $a ){
				
				if( !empty( $a->href ) ){
					if( preg_match( "|^http:\/\/(www\.)?finehomebuilding\.com|i", $a->href  ) ){
						$a->href = preg_replace( "|^(http:\/\/www\.finehomebuilding\.com)(.*?)$|", '\\2', $a->href );
						$a->href = preg_replace( "|^(http:\/\/finehomebuilding\.com)(.*?)$|", '\\2', $a->href );
					}
				}
				
				print $a->outertext."\n";
			}
			
			foreach(  $dom->find( 'Abstract', 0 )->find('p') as $p ){
				if( $p->innertext == '') $p->outertext = '';
			}
			
			foreach(  $dom->find( 'Abstract', 0 )->find('strong') as $strong ){
				if( $strong->innertext == '') $strong->outertext = '';
			}
			
			$content =  $dom->find( 'Abstract', 0 )->innertext;
			$content = preg_replace( "|\n|si", '', $content );
			$content = preg_replace( "|\r|si", '', $content );
			
		}
			
		$dom->clear();
		unset( $dom );
	
		return array( $content, $inline_images );
	
	}
	
	function ci_clean_content( $content, $post_type ){
		
		$inline_images = array();
		
		$dom = str_get_html( $content  );
		
		foreach( $dom->find( 'img') as $img ){
			$inline_images[] = array( 'image_src' => $img->src );
			error_log( $img->src );
			//$img->src = __S3_URL__.CONTENT_DIR."/uploads". $this->attachment->lowercase_path(  $img->src );
			$attachment_id = $this->attachment->add_attachment(  $img->src , 0 );
			$img->src = wp_get_attachment_url( $attachment_id );
		}
		
		foreach( $dom->find( 'style') as $s ){
			$s->outertext = '';
		}
		
		foreach( $dom->find( 'head') as $s ){
			$s->outertext = '';
		}
		
		foreach( $dom->find( 'body') as $s ){
			$s->outertext = $s->innertext;
		}
				
		
		/*
		 * Remove Page Breaks
		 */
		
		foreach( $dom->find('p') as $p ){
			
			if( $p->innertext == "[[[PAGE]]]" ){
				$p->outertext = '';
			}
			
			$p->style = null;
			$p->class = null;
		
			foreach( $p->find('span') as $span ){
				$span->style = null;
				$span_innertext = $span->innertext;
				$span->outertext = $span_innertext;
			}
			if( preg_match( "/^\s*$/i", $p->innertext ) || ( urlencode($p->innertext) == "%C2%A0" ) ){
				$p->outertext = '';
			}
		}
		
		foreach( $dom->find('span') as $span ){
			$span->style = null;
			$span_innertext = $span->innertext;
			$span->outertext = $span_innertext;
		}
		
		foreach( $dom->find('div') as $div ){
			$div->style = null;
		}
		
		if( $post_type == 'readerproject' ){
			foreach( $dom->find('a') as $a ){
				$a->rel = 'nofollow';
			}
		}
		
		foreach( $dom->find('a') as $a ){
			if( !empty( $a->href ) ){
				if( preg_match( "|^http:\/\/(www\.)?finehomebuilding\.com|i", $a->href  ) ){
					$a->href = preg_replace( "|^(http:\/\/www\.finehomebuilding\.com)(.*?)$|", '\\2', $a->href );
					$a->href = preg_replace( "|^(http:\/\/finehomebuilding\.com)(.*?)$|", '\\2', $a->href );
					error_log( $a->href );
				}
			}
		}
		
		/*
		 * iFrame Videos
		 * <iframe src=\"http://www.youtube.com/embed/8Uk86fsyCPc\" height=\"454\" width=\"551\" frameborder=\"0\"></iframe>
		 */
		foreach( $dom->find('iframe') as $iframe  ){
			$iframe->outertext = '[embed width="'.$iframe->width.'" height="'.$iframe->height.'"]'.$iframe->src.'[/embed]';
		}
		
		
		$content = (string ) $dom;
		
		$dom->clear();
		unset( $dom );
		
		return array( $content, $inline_images );
		
	}
	
	function spam_content( $title ){

		$spam = false;
		$pattern = array(
				"/Dr\s+Oz/i",
				"/N,?FL/i",
				"/Jewelry/i",
				"/ghfgh/",
				"/fdsfcgb/i",
				"/fdgertre/i",
				"/xcvxvx/i",
				"/sdfsdfsd/i",
				"/w\@atch/i",
				"/l\!ive/i",
				"/wa\@tch/i",
				"/w\@,tch/i",
				"/w\.\@\.tch/i",
				"/wa\.tch/i",
				"/wat\.ch/i",
				"/watc\.h/i",
				"/w\.atch/i",
				"/wa\[t\]ch/i",
				"/strÄ“Ä�ming/i",
				"/S\$tream/i",
				"/S-tream/i",
				"/Stre\@m/i",
				"/Stre,am/i",
				"/L;ive/i",
				"/L,ive/i",
				"/Li,ve/i",
				"/Li-ve/i",
				"/L\!ve/i",
				"/liv\*e/i",
				"/li\"ve/i",
				"/liv(\d+)e/i",
				"/F\.o\.o\.t\.b\.a\.ll/i",
				"/Fox-Tv/i",
				"/grand\s+prix/i",
				"/Houston\s+Texans/i",
				"/Ball\s+State/",
				"/foot\s+ball/",
				"/emmy.*?awards/i",
				"/fox\s+tv/i",
				"/Okla-homa/i",
				"/ga\|me/i",
				"/li\(nn\)ve/i",
				"/foot\s+ga\s+ball/i",
				"/Minnesota\s*Vikings/i",
				"/citing\s*the\s*photog/i",
				"/\>HD\</i",
				"/Dental\s*Care/i",
				"/mega\s*tv\!\!/i",
				"/Mayweather/i",
				"/bangalore|Sri\s*Lanka/i",
				"/sdsffidfjkdjf/i",
				"/Joseph\s*Parker/i",
				"/Daniel\s*Cormier/i",
				"/France\s*Romania/i",
				"/Signs\s*of\s*Aging/",
				"/Age?ing\s*skin/i",
				"/skin\s*care/i",
				"/Growing\s*older/i",
				"/vitamin/i",
				"/Age?ing\s*Can/i",
				"/Age?ing\s*should/i",
				"/age?ing\s*process/i",
				"/age?ing\s*sign/i",
				"/Quick\s*Ageing/i",
				"/Indications\s*of\s*aging/i",
				"/Anti-?Ageing/i",
				"/Anti\s*Ageing/i",
				"/Getting\s*old/i",
				"/Growing\s*old/i",
				"/Rapid\s*Ageing/i",
				"/luckyvogue/i",
				"/acne/i",
				"/Dentist/i",
				"/Central87|oscars/i",
				"/female\s*fairness/i",
				"/WWE\s*Hell|BNCD|falcons|Sakhil\s*Kio/i",
				"/sdgdsg|eiouoiru|sdkljfk|fghf|sdfg|sadfsaf|dgbdrf|dsdd|fdgd|asdasd|yutyut|fgh/i",
				"/losing\s*weight/i",
				"/porn|fuck|breast\s*milk/i",
				"/reddit\.com/i",
				"/Hindi\s*news/i",
				"/Hindi\s*news/i",
				"/Morgan\s*Freeman/i"
		);
		foreach( $pattern as $p ){
			if( preg_match( $p, $title ) ){
				$spam = true;
				break;
			}
		}
		if( empty( $spam ) ){
			if( preg_match( "/\s+(vs|v-s|v~s|vs\.|v\<\>s)\s+/i", $title ) && preg_match( "/(Str\<\>eam|stream)/i", $title ) ){
				$spam = true;
				
			}
		}
		
		/* word match */
		if( empty( $spam ) ){
			
			$title = preg_replace( "/\./", '', $title );
			
			if( preg_match( "/foot-?ball/i", $title )){
				$spam = true;
			}
			
			
			$title = urlencode($title);
			
			//print $title."\n";
			
			if( preg_match( "/(Liv%D0%B5|S%24tream|li%3A%3Ave|li%7Cve|S%D1%82reaming|S%D1%82ream|stream|%C3%99%EF%BF%BD)/i", $title ) ){
				$spam = true;
			}

		}
		
		return $spam;
	}
}