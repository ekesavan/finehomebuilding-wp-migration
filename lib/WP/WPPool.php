<?php 
/*
 pools
 		"pool_id" : "41",
        "pool_type" : "blog",
        "pool_status" : "closed",
        "pool_owner" : "1704",
        "pool_title" : "Chalk the Line",
        "pool_guid" : "chalk-the-line",
        "pool_header" : "/assets/uploads/pools/headers/41/editors_notepad_banner.jpg",
        "pool_banner" : "",
        "pool_theme" : "Blue_Rust.css",
        "pool_display" : "list",
        "pool_access" : "list",
        "pool_postsperpage" : "10",
        "pool_postsperuser" : "0",
        "pool_start" : "0000-00-00 00:00:00",
        "pool_end" : "0000-00-00 00:00:00",
        "created" : "2008-11-13 09:45:12",
        "modified" : "2010-09-15 13:05:21",
        "is_public" : "0",
        "approved" : "0",
        "hierarchy" : "0"

		WP Status
		
		'publish' - A published post or page
		'pending' - post is pending review
		'draft' - a post in draft status
		'auto-draft' - a newly created post, with no content
		'future' - a post to publish in the future
		'private' - not visible to users who are not logged in
		'inherit' - a revision. see get_children.
		'trash' - post is in trashbin. added with Version 2.9.

 */

class WPPool extends WPContent{ 
	
	function __construct(){
	
		parent::__construct();
	}
	
	public function insert( $data ){
	
		$post = array();
		$post['ping_status'] = 'closed';
		$post['comment_status'] = 'closed';
		
		$a = $data['post'];
		
		$taxonomy = $data['taxonomy'];
		
		
		if( $a['pool_type'] == 'blog' ){
			$post['post_type'] = 'pool';
		}elseif( $a['pool_type'] == 'contest'  )  {
			$post['post_type'] = 'pool';
		}else{
			return;
		}
		
		$post['post_title'] = $a['pool_title'];
		
		$post['post_name'] = sanitize_title( $a['pool_title'] );
		
		if( $a['pool_status'] == 'closed' ){
			$post['post_status'] = 'trash';
		}else{
			$post['post_status'] = $a['pool_status'];
		}
		
		$post['post_date'] = $a['created'];
		$post['post_date_gmt'] = $a['created'];
		
		
		// set taxonomy
		$category_ids = array();
		$tags = array();
		
		if( !empty( $taxonomy['post_tag'] ) ){
			
			$post_tags = $taxonomy['post_tag'];
			foreach( $post_tags as $t ){
				$topic = $this->db->findOne( 'csv_taxonomy', array( 't' => new MongoDB\BSON\Regex( $t, 'i') ) );
				if( !empty( $topic ) ){
					$category_ids[] = $topic['category_id'];
				}else{
					$tags[] = ucwords($t);
				}
			}
		}
		
		
		
	
		// category
		if( !empty( $category_ids ) )
			$post['post_category'] = $category_ids;
		
		// set tags
		if( !empty( $tags ) )
			$post['tags_input'] = $tags;
		
		$wp_error = '';
		
		$history_link = "http://www.finehomebuilding.com/".$a['pool_guid'];
		
		$posts = get_posts(array(
				'numberposts'	=> -1,
				'post_type'		=> $post['post_type'],
				'meta_key'		=> 'history_link',
				'meta_value'	=> $history_link
		));
		
		$post_id = 0;
		if( !empty( $posts ) ){
			$post_id = $posts[0]->ID;
		}
		
		if( !empty( $post_id ) ){
			$post['ID'] = $post_id;
			wp_update_post( $post );
		}else{
			$post_id = wp_insert_post( $post, $wp_error );
		}
		
		
		// set channel How-to, Design
		if( !empty( $taxonomy['category'] ) ){
			if( in_array( 'Design', $taxonomy['category'] ) )
				$this->wp_term->set_channel( $post_id, 'design' );
				elseif( in_array( 'How-to', $taxonomy['category'] ) )
				$this->wp_term->set_channel( $post_id, 'howto' );
		}
		
		update_field( 'field_56a26810e2bdc', $history_link, $post_id );
		
		// set design content
		
		// header/Banner
		if( !empty( $a['pool_header'] ) ){
			$attachment_id =  $this->attachment->add_attachment( $a['pool_header'],$post_id );
			update_field( 'field_56a13bce59cbc', true, $post_id );
			update_field( 'field_56a13bce59e21', $attachment_id, $post_id );
		}
		
		//Enter Custom HTML 
		if( !empty( $a['pool_banner'] ) ){
			update_field( 'field_56a13bce59f72', $a['pool_banner'] , $post_id );
		}
		
		// Styles
		if( !empty( $a['pool_theme'] ) ){
			update_field( 'field_56a13bce5a0c5', $a['pool_theme'] , $post_id );
		}
		
		// list or grid
		if( !empty( $a['pool_display'] ) ){
			update_field( 'field_56a13bce5a22c', $a['pool_display'] , $post_id );
		}
		
		// count
		if( !empty( $a['pool_postsperpage'] ) ){
			update_field( 'field_56a13bce5a378', $a['pool_postsperpage'] , $post_id );
		}
		
		/* Pool Start Date */
		update_field( 'field_56f58ab5820ae', $a['pool_start'] , $post_id );
		
		/* Pool End Date*/
		update_field( 'field_56f58b020ba54', $a['pool_end'] , $post_id );

		
		$this->set_modules( $post_id, $a['pool_type'], $data['modules'] );

				
		return $post_id;
		
	}
	
	private function set_modules( $post_id, $type, $modules ){
		
		print_R( $modules );
		
		// set pool type
		update_field( 'field_56c68a349dff1', $type, $post_id );
		
			
		//RSS Module
		$rss_module = $this->find_module( 'rss', $modules );
		print_R( $rss_module );
		if( !empty( $rss_module ) ){
			$sites = array();
			foreach( $rss_module['module_content']['sites'] as $k => $v ){
				if( $v ) $sites[] = $k;
			}
			
			update_field( 'field_56a12c7d0654b', $sites, $post_id );
			$selected_modules[] = 'rss';
		}
		
		// Recent Posts
		$recent_module = $this->find_module( 'recently', $modules );
		if( !empty( $recent_module ) ){
			//title
			update_field( 'field_56a134402e214', $recent_module['module_content']['title'], $post_id );
			//count
			update_field( 'field_56a134632e215', $recent_module['module_content']['tot_posts'], $post_id );
			
			$selected_modules[] = 'recently';
		}
		
		// comment module
		$comment_module = $this->find_module( 'comments', $modules );
		if( !empty( $comment_module ) ){
			
			//title
			update_field( 'field_56a135cb5ab5e', $comment_module['module_content']['module_title'], $post_id );
			//count
			update_field( 'field_56a135e45ab5f', $comment_module['module_content']['tot_comments'], $post_id );
			
			$selected_modules[] = 'comments';
		}
		
		//Blog Description
		if( $type == 'blog' ){
			$about_module = $this->find_module( 'about', $modules );
		}else{
			$about_module = $this->find_module( 'contest', $modules );
		}
		
		if( !empty( $about_module ) ){
				
			//title
			update_field( 'field_56a136f80889c', $about_module['module_content']['title'], $post_id );
			//content
			update_field( 'field_56a137180889d', $about_module['module_content']['content'], $post_id );
				
			$selected_modules[] = 'about';
		}
		
		//About Author
		$author_module = $this->find_module( 'authors', $modules );
		print_R( $author_module );
		if( !empty( $author_module ) ){
		
			//title
			update_field( 'field_56a1382aa44b2', $author_module['module_content']['module_title'], $post_id );
			//content
			update_field( 'field_56a13848a44b3', $author_module['module_content']['tot_authors'], $post_id );
		
			$selected_modules[] = 'authors';
		}
		
		
		
		//Blogroll
		$blogroll_module = $this->find_module( 'blogroll', $modules );
		if( !empty( $blogroll_module ) ){
		
			//title
			update_field( 'field_56a1389ec318a', $blogroll_module['module_content']['module_title'], $post_id );
			//blogroll
			
			$urls = array();
			foreach( $blogroll_module['module_content']['blogroll'] as $b ){
				$urls[] = array( 'field_56a138dc58161' => $b['title'], 'field_56a138f158162' => $b['url'] );
			}

			update_field( 'field_56a138c758160', $urls, $post_id );
		
			$selected_modules[] = 'blogroll';
		}
		
		//Taunton Product Teaser
		$product_module = $this->find_module( 'product', $modules );
		if( !empty( $product_module ) ){
		
			//title
			update_field( 'field_56a139f86bdf6', $product_module['module_content']['module_title'], $post_id );
			//blogroll
			update_field( 'field_56a13a2133296', $product_module['module_content']['product_itemid'], $post_id );
		
			$selected_modules[] = 'product';
		}
	
		//HTML Block 1
		$html1_module = $this->find_module( 'html1', $modules );
		if( !empty( $html1_module ) ){
		
			//content
			update_field( 'field_56a13a68900e2', $html1_module['module_content']['content'], $post_id );
		
			$selected_modules[] = 'html1';
		}
		
		
		//HTML Block 2
		$html2_module = $this->find_module( 'html2', $modules );
		if( !empty( $html2_module ) ){
		
			//content
			update_field( 'field_56a13aa5900e4', $html2_module['module_content']['content'], $post_id );
		
			$selected_modules[] = 'html2';
		}
		
		
		// selected modules
		print_R( $selected_modules );
		update_field( 'field_56a12be1508cf', $selected_modules , $post_id );
	}
	
	private function set_contest_modules( $post_id, $module ){
		
		//RSS Module
		$rss_module = $this->find_module( 'rss', $modules );
		if( !empty( $rss_module ) ){
			$sites = array();
			foreach( $rss_module['module_content']['sites'] as $k => $v ){
				if( $v ) $sites[] = $k;
			}
			update_field( 'field_56a13cfce84f0', $sites , $post_id );
			$selected_modules[] = 'rss';
		}
		
		//Contest Description
		$about_module = $this->find_module( 'contest', $modules );
		if( !empty( $comment_module ) ){
		
			//title
			update_field( 'field_56a13cfce8f57', $about_module['module_content']['title'], $post_id );
			//content
			update_field( 'field_56a13cfce90a5', $about_module['module_content']['content'], $post_id );
		
			$selected_modules[] = 'about';
		}
		
		// Recent Entries
		$recent_module = $this->find_module( 'recently', $modules );
		if( !empty( $recent_module ) ){
			//title
			update_field( 'field_56a13cfce8b72', $recent_module['module_content']['title'], $post_id );
			//count
			update_field( 'field_56a13cfce9489', $recent_module['module_content']['tot_posts'], $post_id );
				
			$selected_modules[] = 'recently';
		}
		
		// Recent Comments
		$comment_module = $this->find_module( 'comments', $modules );
		if( !empty( $comment_module ) ){
				
			//title
			update_field( 'field_56a14076e7578', $comment_module['module_content']['module_title'], $post_id );
			//count
			update_field( 'field_56a14090e7579', $comment_module['module_content']['tot_comments'], $post_id );
				
			$selected_modules[] = 'comments';
		}
		
		//HTML Block 1
		$html1_module = $this->find_module( 'html1', $modules );
		if( !empty( $html1_module ) ){
		
			//content
			update_field( 'field_56a13cfce9f28', $html1_module['module_content']['content'], $post_id );
		
			$selected_modules[] = 'html1';
		}
		
		
		//HTML Block 2
		$html2_module = $this->find_module( 'html2', $modules );
		if( !empty( $html2_module ) ){
		
			//content
			update_field( 'field_56a13cfcea1d2', $html2_module['module_content']['content'], $post_id );
		
			$selected_modules[] = 'html2';
		}
		
		
		// selected modules
		update_field( 'field_56a13cfce8252', $selected_modules , $post_id );
	
	}
	
	private function find_module( $code, $modules ){
		$module = '';
		foreach( $modules as $m ){
			if( $m['module_code'] == $code ){
				$module = $m;
				break;
			}
		}
		return $module;
		
	}
	
	
	/*
		 			[module_code] => rss
                    [module_content] => Array
                        (
                            [sites] => Array
                                (
                                    [yahoo] => 1
                                    [google] => 1
                                    [aol] => 1
                                )

                        )
                        

					[module_code] => recently
                    [module_content] => Array
                        (
                            [title] => Recent Posts
                            [tot_posts] => 3
                        )

                    [sort_order] => 5


					[module_code] => comments
                    [module_content] => Array
                        (
                            [module_title] => Recent Comments
                            [tot_comments] => 3
                     )
                     
                    [module_code] => about
                    [module_content] => Array
                        (
                            [title] => About this Blog
                            [content] => <p>Have your e...

					
					[module_code] => blogroll
                    [module_content] => Array
                        (
                            [module_title] => Other Fine Homebuilding Blogs
                            [blogroll] => Array
                                (
                                    [0] => Array
                                        (
                                            [title] => The Deans of Green
                                            [url] => http://finehomebuilding.taunton.com/blog/de ans-of-green
                                        )

                                    [1] => Array
                                        (
                                            [title] => Inside the Magazine
                                            [url] => http://finehomebuilding.taunton.com/blog/in side-the-magazine
                                        )


					[module_code] => product
                    [module_content] => Array
                        (
                            [module_title] => From the Store
                            [product_itemid] => 17215
                        )

					[module_code] => html1
                    [module_content] => Array
                        (
                            [content] => ....
                            
                            
                    [module_code] => html2
                    [module_content] => Array
                        (
                            [content] => ....        
		 
	 */
}

?>