<?php 
/* WP Author Roles
 *
 * subscriber
 * contributor
 * author
 * editor
 * administrator
 *
 * wp_insert_user
 *
 */

/*
 * Field Name	Description	Associated Filter
 ID	An integer that will be used for updating an existing user.	(none)
 user_pass	A string that contains the plain text password for the user.	pre_user_pass
 user_login	A string that contains the user's username for logging in.	pre_user_login
 user_nicename	A string that contains a URL-friendly name for the user. The default is the user's username.	pre_user_nicename
 user_url	A string containing the user's URL for the user's web site.	pre_user_url
 user_email	A string containing the user's email address.	pre_user_email
 display_name	A string that will be shown on the site. Defaults to user's username. It is likely that you will want to change this, for both appearance and security through obscurity (that is if you dont use and delete the default admin user).	pre_user_display_name
 nickname	The user's nickname, defaults to the user's username.	pre_user_nickname
 first_name	The user's first name.	pre_user_first_name
 last_name	The user's last name.	pre_user_last_name
 description	A string containing content about the user.	pre_user_description
 rich_editing	A string for whether to enable the rich editor or not. False if not empty.	(none)
 user_registered	The date the user registered. Format is Y-m-d H:i:s.	(none)
 role	A string used to set the user's role.	(none)
 jabber	User's Jabber account.	(none)
 aim	User's AOL IM account.	(none)
 yim	User's Yahoo IM account.
 */

/*
 * Custom Field
 _author_image

 */

class WPUser{
	
	public $attachment_ids = array();
	
	function __construct(){
		
		$this->attachment = new WPAttachment();
		
	}
	
	public function insert_user( $u ){
		
				
		$wp_user = $this->get_user_by_tid( $u['TID'] );
		
		/*
		if( !empty(  $wp_user ) ){
			return $wp_user['ID'];
		}
		*/
		
		if( !empty( $u['users_profile']['profile_image'] ) ){
			$profile_image = $u['users_profile']['profile_image'];
			
			$pathinfo = pathinfo( $profile_image );
			$image_src = "/assets/avatars/".$pathinfo['filename']."_sqm.".$pathinfo['extension'];
			$image_dst = "/assets/avatars/".$pathinfo['filename']."_sqm.".$pathinfo['extension'];
			
			//$this->attachment->copy_image( $image_src, $image_dst );
			
			if( !isset( $this->attachment_ids[$image_src] ) ){
				
				$attachment_id = $this->attachment->add_attachment(  $image_src, 0  );
			
				$this->attachment_ids[$image_src] = $attachment_id;
				
			}else{
				
				$attachment_id = $this->attachment_ids[$image_src];
				
			}
			
			$u['users_profile']['profile_image']  = $attachment_id;
		}
					
		
		$user_id = $wp_user['ID'];
		
		if( empty( $wp_user ) ){
		
			$user_id = $this->insert_wp_user( $u );
			
		}else{

			$this->update_wp_user( $user_id, $u );
			
		}
		
		
		if( empty( $wp_user ) ){
		
			
			$user_id = $this->insert_wp_user_profile( $user_id, $u['users_profile'] );
				
		}else{
			
			$this->update_wp_user_profile( $user_id, $u['users_profile'] );
			
		}
		
		
		
		return $user_id;
		
	}
	
	
	
	
	private function insert_wp_user( $u ){
		
		global $wpdb;
		 
		$taunton_user_table = $wpdb->prefix.'fhb_user';
		print "insert_wp_user TID:". intval($u['TID'])."\n";
		
		$wpdb->insert(
				$taunton_user_table,
				array(
						'TID' => intval($u['TID']),
						'user_first_name' => $u['user_first_name'],
						'user_last_name' =>  $u['user_last_name'],
						'user_email' => $u['user_email'],
						'user_url' => $u['user_url'],
						'user_registered' => $u['user_registered'],
						'user_status' => $u['user_status'],
						'display_name' => $u['display_name'],
				),
				array(
						'%d',
						'%s',
						'%s',
						'%s',
						'%s',
						'%s',
						'%d',
						'%s',
				)
				);
		print_R( $wpdb->last_error );
		 return $wpdb->insert_id;
	}
	
	
	private function insert_wp_user_profile( $user_id, $u){
		
		global $wpdb;
	
		$taunton_user_table = $wpdb->prefix.'fhb_user_profile';

		$wpdb->insert(
				$taunton_user_table,
				array(
						'user_id' => intval($user_id),
						'profile_image' => 	$u['profile_image'],
						'profile_city' => $u['profile_city'],
						'profile_state' => $u['profile_state'],
						'profile_country' =>  $u['profile_country'],
						'profile_bio' => $u['profile_bio'],
						'profile_data' => $u['profile_data'],
				),
				array(
						'%d',
						'%s',
						'%s',
						'%s',
						'%s',
						'%s',
						'%s',
				)
				);
		
		print_R( $wpdb->last_error );
		
	}
	
	
	public function update_wp_user( $fhb_user_id, $data ){
		
		global $wpdb;
		
		$taunton_user_table = $wpdb->prefix.'fhb_user';
		
		$updated = $wpdb->update(
				$taunton_user_table,
				array(
						'user_first_name' => $data['user_first_name'],
						'user_last_name' =>  $data['user_last_name'],
						'user_email' => $data['user_email'],
						'user_url' => $data['user_url'],
						'display_name' => $data['display_name'],
				),
				array(
						'ID' => intval($fhb_user_id),
				),
				array(
						'%s',
						'%s',
						'%s',
						'%s',
						'%s',
				),
				array(
						'%d'
				)
				);
		
		if ( false === $updated ) {
	    	print "Error update_wp_user : $fhb_user_id  \n";
		} else {
	    	print "Success update_wp_user : $fhb_user_id  \n";
		}
	
	}
	
	public function update_wp_user_profile($fhb_user_id, $data ){
		print_R( $data );
		
		global $wpdb;
		$taunton_user_table = $wpdb->prefix.'fhb_user_profile';
		$updated = $wpdb->update(
				$taunton_user_table,
				array(
						'profile_image' => 	$data['profile_image'],
						'profile_city' => $data['profile_city'],
						'profile_state' => $data['profile_state'],
						'profile_country' =>  $data['profile_country'],
						'profile_bio' => $data['profile_bio'],
						'profile_data' => $data['profile_data'],
				),
				array(
						'user_id' => intval($fhb_user_id),
				),
				array(
						'%s',
						'%s',
						'%s',
						'%s',
						'%s',
						'%s',
				),
				array(
						'%d'
				)
				);
		
		if ( false === $updated ) {
			print "Error update_wp_user_profile : $fhb_user_id  \n";
		} else {
			print "Success update_wp_user_profile : $fhb_user_id  \n";
		}
		
	}
	
	private function get_user_by_tid( $tid ){
		print "-----------------get_user_by_tid:$tid--------------------\n";
		global $wpdb;
		$table_name = $wpdb->prefix . 'fhb_user';
		$m = $wpdb->get_row($wpdb->prepare("SELECT ID FROM $table_name WHERE TID= %d", intval($tid) ), ARRAY_A);
		return $m;
	}
	
}

?>