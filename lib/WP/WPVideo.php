<?php 

class WPVideo{

	/*
	 Video Content
	  
	 <brightcove_playlist_id>64493844001</brightcove_playlist_id>
	 <brightcove_featured_video_id>671163897001</brightcove_featured_video_id>
	
	 <Video_Clip>
	 <Title></Title>
	 <Description></Description>
	 <File>
	 <Type>Quicktime</Type>
	 <Filename>HVT524_Belz_ep_4_Brightcove_H264_HD.mp4</Filename>
	 </File>
	 <Credit>Chuck Bickford and John Ross</Credit>
	 <Length>5:26</Length>
	 </Video_Clip>
	
	
	 */

	public function set_ektron_video( $html, $post_id, $ektron_id ){
		print "-----------------------set_ektron_video--------------------------\n";
		if( __LOG__ )
			print $html."\n\n";
		
		
		$dom = str_get_html( $html  );
		
		$video = array();
	
		if( $dom->find('brightcove_playlist_id',0 ) ){
		
			$video['brightcove_playlist_id'] = $dom->find('brightcove_playlist_id',0 )->innertext;
		
		}
	
		if( $dom->find('brightcove_featured_video_id',0 ) ){
		
			$video['brightcove_featured_video_id'] = $dom->find('brightcove_featured_video_id',0 )->innertext;
		
		}
		
		if( empty( $video['brightcove_featured_video_id'] ) ){
			$video['brightcove_featured_video_id'] = "ref:".$ektron_id;
		}
		
		if( $dom->find('Video_Clip',0 ) ){
		
			$video_clip = $dom->find('Video_Clip',0 );
			
			$video['video_title'] = $video_clip->find('Title',0 )->innertext;
			
			$video['video_description'] = $video_clip->find('Description',0 )->innertext;
			
			$file = $video_clip->find('File',0 );
			
			if( $file ){
				
				$video['file_type'] =  $file->find('Type',0 )->innertext;
				$video['file_name'] =  $file->find('Filename',0 )->innertext;
				
			}
		
			$video['video_credit'] = $video_clip->find('Credit',0 )->innertext;
			
			$video['video_length'] = $video_clip->find('Length',0 )->innertext;
			
		}
		
		if( __LOG__ )
			print_R( $video );
		
		$dom->clear();

		unset( $dom );
		
		if( !empty( $video ) ){
			$this->set_wp_video( $video, $post_id );
		}
	}
	
	/*
		"post_title" : "Payback Estimator: Insulation Upgrade Video",
        "post_category" : "0",
        "post_excerpt" : "
        <xml>
        <video_length_minutes></video_length_minutes>
        <video_length_seconds></video_length_seconds>
        <video_credit>Justin Fink and Steve Lombardi *requires latest version of Flash</video_credit>
        <video_hosted_at>YouTube</video_hosted_at>
        </xml>",

	 * 
	 */
	public function set_codeigniter_video( $attachment, $post_id ){
		
		if( __LOG__ )
			print_R( $attachment );
		
		$video = array();
		
		$post_content = $attachment['post_content'];
		
		if( preg_match( '|name="@videoPlayer"\s+value="(\d+)"|si', $post_content, $m ) ){
			$playerID = $m[1];
		}elseif(preg_match( '|name="@videoList\.featured"\s+value="(\d+)"|si', $post_content, $m ) ) {
			$playerID = $m[1];
		}elseif(preg_match( '|name="@videoList"\s+value="(\d+)"|si', $post_content, $m ) ) {
			$playerID = "list:".$playerID = $m[1];
		}
		print "--------------------------$playerID---------------------\n";

		if( !empty( $playerID ) ){
			$video['brightcove_featured_video_id'] = $playerID;
		}else{
			$video['code'] = $attachment['post_content'];
		}
		
		$video['video_title'] = $attachment['post_title'];
		
		if( !empty( $attachment['post_excerpt'] ) ){
			
			$dom = str_get_html( $attachment['post_excerpt']  );
			
			$video_length_minutes = $dom->find('video_length_minutes',0 )->innertext;
			$video_length_seconds = $dom->find('video_length_seconds',0 )->innertext;
			
			$length = '';
			if( !empty( $video_length_minutes ) ){
				$length = $video_length_minutes. "m ";
			}
			
			if( !empty( $video_length_seconds ) ){
				$length .= $video_length_seconds. "s ";
			}
			
			$video['video_length'] = $length;
			
			$video['video_credit'] = $dom->find('video_credit',0 )->innertext;
			$video['file_type'] = $dom->find('video_hosted_at',0 )->innertext;
			
			
			$dom->clear();
			unset( $dom );
			
		}
		
		if( !empty( $video ) ){
			$this->set_wp_video( $video, $post_id );
		}
	}
	
	
	public function set_wp_video( $video, $post_id  ){
		
		print_R( $video );
		
		if( !empty( $video['code'] ) ) {
			
			update_field( 'field_56a2b854176c9', $video['code'], $post_id );
			
		}else{
			update_field( 'field_56a2b854176c9', '', $post_id );
		}
		
		if( !empty( $video['brightcove_playlist_id'] ) ) {
				
			update_field( 'field_56a3e37131819', $video['brightcove_playlist_id'], $post_id );
				
		}
		
		if( !empty( $video['brightcove_featured_video_id'] ) ) {
		
			update_field( 'field_56a3e3b949ae5', $video['brightcove_featured_video_id'], $post_id );
		
		}
		
		if( !empty( $video['video_title'] ) ) {
		
			update_field( 'field_56a3e3eff7e85', $video['video_title'], $post_id );
		
		}
		
		if( !empty( $video['video_description'] ) ) {
		
			update_field( 'field_56a3e3fbf7e86', $video['video_description'], $post_id );
		
		}
		if( !empty( $video['file_type'] ) ) {
		
			update_field( 'field_56a3e40df7e87', $video['file_type'], $post_id );
		
		}
		
		
		if( !empty( $video['file_name'] ) ) {
		
			update_field( 'field_56a3e41ef7e88', $video['file_name'], $post_id );
		
		}
		
		if( !empty( $video['video_credit'] ) ) {
		
			update_field( 'field_56a3e427f7e89', $video['video_credit'], $post_id );
		
		}
		
		if( !empty( $video['video_length'] ) ) {
		
			update_field( 'field_56a3e432f7e8a', $video['video_length'], $post_id );
		
		}
		
		
	}
		
}


/*
 
 
 $str = <<<EOF
<!-- Start of Brightcove Player -->
<code>
<script language="JavaScript" type="text/javascript" src="http://admin.brightcove.com/js/BrightcoveExperiences.js"></script>
 <object id="singlePlayer$brightcove_featured_video_id" class="BrightcoveExperience"><param name="bgcolor" value="#FFFFFF" />
 <param name="width" value="595" />
 <param name="height" value="334" />
 <param name="playerID" value="3631488215001" />
 <param name="playerKey" value="AQ~~,AAAADAxIJDE~,N1RvxNDWjA2DBToCurl7IPCc6hVX3L9S" />
 <param name="isVid" value="true" />
 <param name="isUI" value ="true"/>
 <param name="dynamicStreaming" value="true" />
 <param name="wmode" value="transparent"/>
 <param name="quality" value="high"/>
 <param name="@videoPlayer" value="$brightcove_featured_video_id" />
 <!-- smart player api params -->
 <param name="includeAPI" value="true" />
 <param name="templateLoadHandler" value="onTemplateLoaded" />
 <param name="templateReadyHandler" value="onTemplateReady" />
 </object>
 <script type="text/javascript">brightcove.createExperiences();</script>
 <!-- End of Brightcove Player -->
</code>
EOF;


 */

?>

