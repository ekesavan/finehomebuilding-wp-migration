<?php 

class WPDigitalIssue extends WPContent{ 
	
	function __construct(){
	
		parent::__construct();
		
	}
	
	public function insert_digital_issue( $data ){
	
		$post = array();
		$post['ping_status'] = 'closed';
		$post['comment_status'] = 'closed';
		
		$a = $data['post'];
		
		$extended = $data['extended'][0];
		
		$post['post_title'] = $a['post_title'];
		
		$post['post_name'] = sanitize_title( $a['post_title'] );
		
		$post['post_status'] = $a['post_status'];
		
		$post['post_type'] = 'issue';
		
		$post['post_date'] = $a['post_date'];
		
		$post['post_date_gmt'] = $a['post_date_gmt'];

		$post['post_content'] = $a['post_content'];

		$post['post_excerpt'] = $a['post_excerpt'];
		
		if( !empty(   $data['user']['wp_user_id'] ) ){
			$post_authors = $data['user']['wp_user_id'];
		}
		
		$wp_error = '';
		
		$history_link = "http://www.finehomebuilding.com".$a['guid'];
		
		$posts = get_posts(array(
				'numberposts'	=> -1,
				'post_type'		=> 'issue',
				'meta_key'		=> 'history_link',
				'meta_value'	=> $history_link
		));
		
		$post_id = 0;
		if( !empty( $posts ) ){
			$post_id = $posts[0]->ID;
		}
		
		if( !empty( $post_id ) ){
		
			print "Update Post=$post_id--->".$post['post_title']."\n";

			$post['ID'] = $post_id;
			wp_update_post( $post );
		
		}else{
		
			// create post
			$post_id = wp_insert_post( $post, $wp_error );
			print "Insert Post=$post_id--->".$post['post_title']."\n";
		}
		
		//insert images
		$attachment_ids = array();
		if( !empty( $data['attachments'] ) ){
			foreach( $data['attachments'] as $image ){
				/* 360 x 450 */
				$attachment_id =  $this->attachment->add_issue_attachment( $image['post_title'],$post_id );
				$attachment_ids[] = $attachment_id;
			}
		}
		// set history link
		update_field( 'field_56a118b4dd24a', $history_link, $post_id );
		
		print_R( $attachment_ids );
		// set main image
		if( !empty( $attachment_ids ) ){
			//set_post_thumbnail( $post_id, $attachment_ids[0] );
			update_post_meta( $post_id, '_thumbnail_id', $attachment_ids[0] );
			update_field( 'field_56f16828ec4ce', $attachment_ids[0], $post_id );
			
		}
		
		// Full : set calameo player content
		if( !empty( $extended['field_1'] ) ){
			update_field( 'field_56a10d1babef8',$extended['field_1'], $post_id );
		}
		
		// Part : set calameo player content
		if( !empty( $extended['field_2'] ) ){
			update_field( 'field_56a10e9fabef9',$extended['field_2'], $post_id );
		}
		
		// issue number
		if( !empty( $extended['field_3'] ) ){
			
			$issue_number = intval(trim( $extended['field_3'] ));
			update_field( 'field_56a112de93f07',$issue_number, $post_id );
		}
		return $post_id;
	}
	
	
	public function update_digital_issue( $issue, $ids ){
		
			
		$article_count = count( $ids );
		$issue_number = $issue;
		if( !preg_match( '/^\d+$/', $issue_number ) ){
			$issue_number = sanitize_title( $issue );
		}
		
		$lookup_issue_number = preg_replace( '/\D+/', '', $issue_number );
		$lookup_issue_number = preg_replace( '/(\d{3})(\d{4})/', '\\1', $lookup_issue_number );
		
		
		$date = date( 'Y', strtotime($ids[0]['date'] ) );
		
		if( !preg_match( '/\d+/', $issue_number )){
			$issue_number= $issue_number."-$date";
			$issue= $issue." $date";
		}
		print $lookup_issue_number."###\n\n";
		
		if( !empty( $lookup_issue_number ) ){
			$posts = get_posts(array(
						'numberposts'	=> -1,
						'post_type'		=> 'issue',
						'meta_key'		=> 'issue_number',
						'meta_value'	=> $lookup_issue_number
			));
		}
		
		if( empty( $posts ) ){
			$posts = get_posts(array(
					'numberposts'	=> -1,
					'post_type'		=> 'issue',
					'meta_key'		=> 'issue_number',
					'meta_value'	=> $issue_number
			));
		}
		
		$post_id = 0;
		
		/* sort ids */
		$issue_article_ids = array();
		
		foreach( $ids as $id ){
			$issue_article_ids[] = intval( $id['Id'] );
		}
		
		if( empty( $posts ) ){
			
			// get an issue article;
			$aobj = $this->db->findOne( $ids[0]['collection'], array( 'Id' => (string) $issue_article_ids[0] ) );
			
			$a  = json_decode(json_encode($aobj), true);
			
			// set issue title and publish date
			$title_issue = $a['META_DATA']['GetContentMetadataListResult']['CustomAttribute'][10]['Value'];
			$published_date =  $a['META_DATA']['GetContentMetadataListResult']['CustomAttribute'][16]['Value'];
			
			if( empty( $issue_number ) ){
				$issue_number = $title_issue;
			}
			
			if( empty( $published_date ) ){
				$published_date = $a['DisplayDateCreated'];
			}

			// create magazine post
			$data = array();

			$data['post'] = array();
			
			$data['post']['post_title'] = "Issue $issue";
	               
			$data['post']['post_status'] = 'publish';

			$data['post']['post_date'] = $this->post_date( $published_date );

			$data['post']['post_date_gmt'] = $this->post_date_gmt( $published_date );
	        
			$data['post']['post_content'] = '';

			$data['post']['post_excerpt'] = '';

			$data['post']['guid'] = "/item/issue/$issue_number";
			
			$data['extended'] = array();
			$data['extended'][] = array( 'field_3' => $issue_number );
			
			$post_id = $this->insert_digital_issue( $data );
			
			print "=========================== $issue --> $lookup_issue_number \n";
			
		}else{

			$post_id = $posts[0]->ID;
		}

		print "update_digital_issue: Issue=$issue_number -->$lookup_issue_number, Article Count=$article_count, Post ID= $post_id \n";
		
		if ( $post_id ){
			
			$issue_articles =  array();
			
			foreach( $ids as $id ){
				
				print_R( $id );
				
				$aobj = $this->db->findOne( $id['collection'] , array( 'Id' => $id['Id'] ) );
				
				$a  = json_decode(json_encode($aobj), true);
					
				
				$section = '';
				if( !empty(  $a['folders'] ) ){
					$section = end(  $a['folders'] );
				}else{
					$section = ucfirst( $id['collection'] );
				}
				
				if( $section == "Qa" ){
					$section = "Question and Answer";
				}
				if( $section == 'Readers_tips' ){
					$section = 'Readers Tips';
				}
				if( preg_match( '/\d+/', $section ) ){
					$section = 'Article';
				}
					
				$issue_articles[$section][] = $a['wp_post_id'];
				
				
				/*
				 * set article issue id
				 */
				update_field( 'field_5679b237edf85', $post_id, $a['wp_post_id'] );
			}
			
			print_R( $issue_articles );
			
			$field_key = "field_56a63ab6b736e";

			$layout_articles = array();
			
			foreach( $issue_articles as $k => $wp_post_ids ){
				
				$term = get_term_by('name', $k, 'department');
				
				if( empty( $term ) ){
					wp_set_object_terms( 0, $k, 'department', false );
					
					$term = get_term_by('name', $k, 'department');
				}
				print "------------$k---------------\n";
				print_R( $term );
				print "###\n";
				$layout_articles[] = array(
						'issue_section' => $term->term_id,
						'issue_section_articles' => $wp_post_ids,
						'acf_fc_layout' => 'section_layout'
			
				);
				
			}
			print_R( $layout_articles );
			
			update_field( $field_key , $layout_articles,  $post_id );
			
		}
		
	}

}

?>
