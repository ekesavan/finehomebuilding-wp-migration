<?php 

class ImageCrop{
	
	private $api_key = "acc_41399aae11274c6";
	
	private $secret = "c28f918a03474dc4a4890f42319085d1";
	
	private $api_url = "https://api.imagga.com/v1/croppings";
	
	public $upload_path = "/srv/sites/finehomebuilding/finehomebuilding-wordpress/web/app/uploads";
	
	public function get_crop_analysis_data( $image_url, $sizes ){
		$resolutions_params = array();
		foreach ( $sizes as $size ) {
			$resolutions_params[] = 'resolution='.$size;
		}
		
		
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_URL, $this->api_url.'?url=' . $image_url . '&' . implode('&', $resolutions_params));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_USERPWD, $this->api_key.':'.$this->secret );
		
		$response = curl_exec($ch);
		curl_close($ch);
		
		$json_response = json_decode($response, true );
		
		return $json_response;
	}
	
	
	public function crop_xlg_image($image_src, $image_dst){
		$image_src = $this->upload_path . $image_src;
		$image_dst = $this->upload_path . $image_dst;
		
		//error_log( "crop_xlg_image: $image_src -> $image_dst");
		
		try{
			$thumb = new Imagick( $image_src );
		}catch( Exception $e ){
			return;
		}
		$src_height  = $thumb->getImageHeight();
		
		$src_width  = $thumb->getImageWidth();
		
		$thumb->cropImage ( $src_width, ($src_height - 12), 0, 0 );
				
		$thumb->writeImage($image_dst);
		
		$thumb->destroy();
	}
	
	public function resize_main_image($image_src, $image_dst){
		
		$image_src = $this->upload_path . $image_src;
		$image_dst = $this->upload_path . $image_dst;
		
		/*
		if( file_exists( $image_dst ) ){
			if( filesize( $image_dst ) > 0 )
				return;
		}
		
		//error_log( "resize_main_image start: $image_src -> $image_dst");
		*/
		
		try{
			$thumb = new Imagick( $image_src );
		}catch( Exception $e ){
			if (!copy($image_src, $image_dst)) {
				echo "failed to copy $file...\n";
			}else{
				echo "success to copy $file...\n";
			}
			return;
		}
		$src_height  = $thumb->getImageHeight();
	
		$src_width  = $thumb->getImageWidth();
	
		if( $src_width >= 700 ){
			$dst_width = 700;
			$dst_height = ceil( ($src_height/$src_width) * $dst_width );
		}else{
			$dst_width = $src_width;
			$dst_height = ceil( ($src_height/$src_width) * $dst_width );
			//error_log( "resize_main_image end: $src_width X $src_height -> $dst_width -> $dst_height =====> $result");
			if (!copy($image_src, $image_dst)) {
				echo "failed to copy $file...\n";
			}else{
				echo "success to copy $file...\n";
			}
			$thumb->destroy();
			return;
		}
		$thumb->resizeImage($dst_width,$dst_height,Imagick::FILTER_CATROM,0.9, false);
	
		$result = $thumb->writeImage( $image_dst );
		
		$thumb->destroy();
		
		//error_log( "resize_main_image end: $src_width X $src_height -> $dst_width -> $dst_height =====> $result");
				
	}
	
	public function crop_image( $image_src, $image_dst, $w, $h ){
		
		$image_src = $this->upload_path . $image_src;
		$image_dst = $this->upload_path . $image_dst;
		
		if( file_exists( $image_dst ) ){
			if( filesize( $image_dst ) > 0 )
				return;
		}
			
		try{
			$thumb = new Imagick( $image_src );
		}catch( Exception $e ){
			if (!copy($image_src, $image_dst)) {
				echo "failed to copy $file...\n";
			}else{
				echo "success to copy $file...\n";
			}
			return;
		}
		
		$thumb->cropThumbnailImage ( $w , $h );
		
		$thumb->writeImage($image_dst);
		
		$thumb->destroy();
	}
	
	
	public function imgaa_crop( $image_url ){
		$sizes = array(
				'700x394',
				'460x260',
				'300x300'
		); 
		
		$analysis = $this->get_crop_analysis_data( $image_url, $sizes );
		
		/*
		Array
		(
				[results] => Array
				(
						[0] => Array
						(
								[image] => http://www.finehomebuilding.com/CMS/uploadedimages/Fine_Homebuilding/Articles/237/021237096-susan-marvin_xlg.jpg
								[croppings] => Array
								(
										[0] => Array
										(
												[y2] => 336
												[target_width] => 700
												[x2] => 599
												[target_height] => 394
												[y1] => 0
												[x1] => 0
												)
		
										[1] => Array
										(
												[y2] => 659
												[target_width] => 300
												[x2] => 599
												[target_height] => 300
												[y1] => 59
												[x1] => 0
												)
		
										)
		
								)
		
						)
		
				[unsuccessful] => Array
				(
						)
		
				)
		
				*/
				
	}
	
	
	
}