<?php 

class EktronMenuTaxonomy {

	private $db;
	private $menu;	

	function __construct(){
		
		$this->menu = new EktronMenu();
		$mongo = new MongoClient();
		$this->db = $mongo->Ektron;
		
	}
	

	function collection(){
		return $this->db->menu_taxonomy;	
	}
	
	function load_taxonomy( $menu_id ){
		
		$this->load_menus( $menu_id );
		
		$this->wp_save();
	}
	
	function load_menus($menu_id){
						
		$topics = $this->menu->get_menus( $menu_id );
		


		$this->collection()->drop();
		
		foreach( $topics['Menu5820'] as $k => $sub_topics ){
		
		
			$t = array(
					'title' => $k,
					'level' => 1,
					'parent' => 0,
			);
		
		
			$this->collection()->insert( $t );
		
			foreach( $sub_topics as $v ){
				
				$st =  array(
						'title' => $v,
						'level' => 2,
						'parent' => $t['_id'],
				);
		
				$this->collection()->insert( $st );
			}
		
		}
	}
	
	function wp_save(){
	
		$query = array( 'level' => 1 );
		
		$cursor = $this->collection()->find( $query );
		
		foreach( $cursor  as $t ){
			
			
			$wp_category = array(
					'cat_name' => $t['title'],
					'category_parent' => 0,
					'taxonomy' => 'category',
			);
			
			$category_id = get_cat_ID( $t['title'] );
			
			if( empty( $category_id ) ){
				
				$category_id =  wp_insert_category( $wp_category );
				
			}
			
			$this->collection()->update( array( '_id' => $t['_id'] ), 
										 array( '$set' => array( 'category_id' => $category_id ) ) 
								);
			
			print "Category ID : ".$category_id."\n";
			
			
			$this->wp_save_sub( $t['_id'], $category_id );
			
			
		}
		
	}
	
	function wp_save_sub( $parent, $category_id ){
		
		$query = array( 'parent' => $parent );
		
		$cursor = $this->collection()->find( $query );
		
		foreach( $cursor  as $t ){
			
			$sub_category_id = get_cat_ID( $t['title'] );
				
			if( empty($sub_category_id) ){
				$sub_category_id =  wp_insert_category(
						array(
								'cat_name' => $t['title'],
								'category_parent' => $category_id,
								'taxonomy' => 'category',
						)
						);
			}
				
			$this->collection()->update( array( '_id' => $t['_id'] ),
					array( '$set' => array( 'category_id' => $sub_category_id ) )
					);
				
			
			$this->wp_save_sub( $t['_id'], $sub_category_id );
		}
		
	}

}


?>