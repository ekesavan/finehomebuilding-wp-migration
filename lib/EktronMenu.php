<?php 

class EktronMenu{
	
	private $url;
	
	function __construct(){
		$this->url = "http://".__EKTRON__CMS_DOMAIN__."/Workarea/webservices/ContentWS.asmx/GenerateHTMLMenu";
	}

	function get_menus( $menu_id ){
		
		$xml = $this->curl_post( $menu_id );
		
		
		$rows = explode( "\n", $xml );
		
		$topics = array ();
		
		$menus = array();
		
		foreach( $rows as $row ){
			if( preg_match( '/(Menu\d+)\s+/', $row, $m1 ) ){
				preg_match( '/\"(.*?)\"/', $row, $m2 );
				$parent = $m1[1];
				$menus[$m1[1]] = $m2[1];
			}
		}
		
		foreach( $rows as $row ){
			if( preg_match( '/((Menu\d+)_\d+)\s+/', $row, $m1 ) ){
				preg_match( '/\"(.*?)\"/', $row, $m2 );
				$topics[$m1[2]][$m1[1]] = array();
				$menus[$m1[1]] = $m2[1];
			}
		}
		
		ksort( $topics["Menu".$menu_id] );
		
		foreach( $rows as $row ){
			if( preg_match( '/((Menu\d+)_\d+)\.addMenuItem/', $row, $m1 ) ){
				preg_match( '/\"(.*?)\"/', $row, $m2 );
				$topics[$m1[2]][$m1[1]][] = $m2[1];
			}
		}
		
		foreach( $topics["Menu".$menu_id] as $k => $v ){
			$topics["Menu".$menu_id][$menus[$k]] = $v;
			unset( $topics["Menu".$menu_id][$k]);
		}
		
		return $topics;
		
	}
	
	function curl_post( $menu_id ){
		
		$fields = array(
				'MenuIds' => urlencode($menu_id),
		);
		
		$fields_string = '';
		
		foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
		
		rtrim($fields_string, '&');
		
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL, $this->url);
		curl_setopt($ch,CURLOPT_POST, count($fields));
		curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		curl_close($ch);
		
		return $result;
	}
}

?>