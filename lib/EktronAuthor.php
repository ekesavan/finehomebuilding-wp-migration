<?php 
/*
  "Html" : 
  <root>
  <AuthorName>A. R. Chase</AuthorName>
  <Author_Name>
  	<First_Name>A.</First_Name>
  	<Middle_Name>R.</Middle_Name>
  	<Last_Name>Chase</Last_Name>
  	<Suffix_Name></Suffix_Name>
  </Author_Name>
  <Contact>
  	<Address1></Address1>
  	<Address2></Address2>
  	<City></City>
  	<State>(Select)</State>
  	<Zip></Zip>
  	<Province></Province>
  	<Country>us</Country>
  	<WebSiteURL></WebSiteURL>
  	<EmailAddress></EmailAddress>
  </Contact>
  <Bios>
  	<Full_Bio></Full_Bio>
  	<Short_Bio></Short_Bio>
  	<Specialties></Specialties>
  </Bios>
  <Image_Section>
  	<Layout>None</Layout>
  	<Image>
  		<Article_Image></Article_Image>
  		<Click_Action>None</Click_Action>
  		<Destination_URL></Destination_URL>
  		<Image_Credit></Image_Credit>
  		<Image_Caption></Image_Caption>
  	 </Image>
  </Image_Section>
  <Ask_the_Experpts>
  	<ExpertForumTeaser></ExpertForumTeaser>
  	<ForumID></ForumID>
  	<FolderID></FolderID>
  	</Ask_the_Experpts>
  	</root>",

 */


class EktronAuthor extends EktronContent{
	
	function __construct(){
		parent::__construct();
	}
	
	function collection(){
		return 'author';
	}
	
	
	function insert_clean_author( $id ){
		
		$a = $this->db->findOne( 'fhb_author',  array( 'Id' => $id ) );
		
		if( empty( $a ) ){
			
			$a = $this->db->findOne( $this->collection(), array( 'Id' => $id ) );
			
			$clean_author = $this->clean_author( $a );
			
			print $clean_author['authorname']."\n";
			
			$this->db->insert( 'fhb_author', $clean_author );
		}
		
	}
	
	
	function clean_author( $a ){
			
		
		$author = array();

		if( empty( $a['Html'] ) ) return $author;
		
		$dom = str_get_html( $a['Html'] );
		
		$author['Id'] = $a['Id'];
		$author['Title'] = $a['Title'];
		$author['Quicklink'] = $a['Quicklink'];
		$author['DisplayDateCreated'] = $a['DisplayDateCreated'];
		
		$tags = array(
			'AuthorName',
			'First_Name',
			'Middle_Name',
			'Last_Name',
			'Suffix_Name',
			'Address1',
			'Address2',
			'City',
			'State',
			'Zip',
			'Province',
			'Country',
			'WebSiteURL',
			'EmailAddress',
			'Full_Bio',
			'Short_Bio',
			'Specialties',
			'Article_Image',
			'Click_Action',
			'Destination_URL',
			'Image_Credit',
			'Image_Caption',
			'ExpertForumTeaser',
			'ForumID',
			'FolderID',
		);
		
				
		foreach( $tags as $tag ){
			$author[strtolower( $tag ) ] = $dom->find( $tag, 0)->innertext;
		}
		$dom->clear();
		unset( $dom );
		
		if( !empty($author['article_image']) ){
		
			$dom = str_get_html( $author['article_image'] );
			$author['article_image'] = $dom->find( "img", 0)->src;
			$dom->clear();
			unset( $dom );
			
		}
		
		return $author;
		
	}
	
}

?>