<?php

class GetAllUnassignedItemApprovalsResponse
{

    /**
     * @var ArrayOfApprovalData $GetAllUnassignedItemApprovalsResult
     */
    protected $GetAllUnassignedItemApprovalsResult = null;

    /**
     * @param ArrayOfApprovalData $GetAllUnassignedItemApprovalsResult
     */
    public function __construct($GetAllUnassignedItemApprovalsResult)
    {
      $this->GetAllUnassignedItemApprovalsResult = $GetAllUnassignedItemApprovalsResult;
    }

    /**
     * @return ArrayOfApprovalData
     */
    public function getGetAllUnassignedItemApprovalsResult()
    {
      return $this->GetAllUnassignedItemApprovalsResult;
    }

    /**
     * @param ArrayOfApprovalData $GetAllUnassignedItemApprovalsResult
     * @return GetAllUnassignedItemApprovalsResponse
     */
    public function setGetAllUnassignedItemApprovalsResult($GetAllUnassignedItemApprovalsResult)
    {
      $this->GetAllUnassignedItemApprovalsResult = $GetAllUnassignedItemApprovalsResult;
      return $this;
    }

}
