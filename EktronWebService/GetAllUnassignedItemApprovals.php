<?php

class GetAllUnassignedItemApprovals
{

    /**
     * @var int $ContentID
     */
    protected $ContentID = null;

    /**
     * @var string $ItemType
     */
    protected $ItemType = null;

    /**
     * @param int $ContentID
     * @param string $ItemType
     */
    public function __construct($ContentID, $ItemType)
    {
      $this->ContentID = $ContentID;
      $this->ItemType = $ItemType;
    }

    /**
     * @return int
     */
    public function getContentID()
    {
      return $this->ContentID;
    }

    /**
     * @param int $ContentID
     * @return GetAllUnassignedItemApprovals
     */
    public function setContentID($ContentID)
    {
      $this->ContentID = $ContentID;
      return $this;
    }

    /**
     * @return string
     */
    public function getItemType()
    {
      return $this->ItemType;
    }

    /**
     * @param string $ItemType
     * @return GetAllUnassignedItemApprovals
     */
    public function setItemType($ItemType)
    {
      $this->ItemType = $ItemType;
      return $this;
    }

}
