<?php

class GetContentResponse
{

    /**
     * @var ContentData $GetContentResult
     */
    protected $GetContentResult = null;

    /**
     * @param ContentData $GetContentResult
     */
    public function __construct($GetContentResult)
    {
      $this->GetContentResult = $GetContentResult;
    }

    /**
     * @return ContentData
     */
    public function getGetContentResult()
    {
      return $this->GetContentResult;
    }

    /**
     * @param ContentData $GetContentResult
     * @return GetContentResponse
     */
    public function setGetContentResult($GetContentResult)
    {
      $this->GetContentResult = $GetContentResult;
      return $this;
    }

}
