<?php

class GetContentByHistoryResponse
{

    /**
     * @var ContentData $GetContentByHistoryResult
     */
    protected $GetContentByHistoryResult = null;

    /**
     * @param ContentData $GetContentByHistoryResult
     */
    public function __construct($GetContentByHistoryResult)
    {
      $this->GetContentByHistoryResult = $GetContentByHistoryResult;
    }

    /**
     * @return ContentData
     */
    public function getGetContentByHistoryResult()
    {
      return $this->GetContentByHistoryResult;
    }

    /**
     * @param ContentData $GetContentByHistoryResult
     * @return GetContentByHistoryResponse
     */
    public function setGetContentByHistoryResult($GetContentByHistoryResult)
    {
      $this->GetContentByHistoryResult = $GetContentByHistoryResult;
      return $this;
    }

}
