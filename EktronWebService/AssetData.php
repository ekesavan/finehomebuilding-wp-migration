<?php

class AssetData
{

    /**
     * @var string $Id
     */
    protected $Id = null;

    /**
     * @var string $Version
     */
    protected $Version = null;

    /**
     * @var string $MimeType
     */
    protected $MimeType = null;

    /**
     * @var string $MimeName
     */
    protected $MimeName = null;

    /**
     * @var string $FileName
     */
    protected $FileName = null;

    /**
     * @var string $FileExtension
     */
    protected $FileExtension = null;

    /**
     * @var string $ImageUrl
     */
    protected $ImageUrl = null;

    /**
     * @var string $Icon
     */
    protected $Icon = null;

    /**
     * @var string $Status
     */
    protected $Status = null;

    /**
     * @var int $Language
     */
    protected $Language = null;

    /**
     * @var int $Type
     */
    protected $Type = null;

    /**
     * @var string $PluginType
     */
    protected $PluginType = null;

    /**
     * @var boolean $PublishPdfActive
     */
    protected $PublishPdfActive = null;

    /**
     * @param int $Language
     * @param int $Type
     * @param boolean $PublishPdfActive
     */
    public function __construct($Language, $Type, $PublishPdfActive)
    {
      $this->Language = $Language;
      $this->Type = $Type;
      $this->PublishPdfActive = $PublishPdfActive;
    }

    /**
     * @return string
     */
    public function getId()
    {
      return $this->Id;
    }

    /**
     * @param string $Id
     * @return AssetData
     */
    public function setId($Id)
    {
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return string
     */
    public function getVersion()
    {
      return $this->Version;
    }

    /**
     * @param string $Version
     * @return AssetData
     */
    public function setVersion($Version)
    {
      $this->Version = $Version;
      return $this;
    }

    /**
     * @return string
     */
    public function getMimeType()
    {
      return $this->MimeType;
    }

    /**
     * @param string $MimeType
     * @return AssetData
     */
    public function setMimeType($MimeType)
    {
      $this->MimeType = $MimeType;
      return $this;
    }

    /**
     * @return string
     */
    public function getMimeName()
    {
      return $this->MimeName;
    }

    /**
     * @param string $MimeName
     * @return AssetData
     */
    public function setMimeName($MimeName)
    {
      $this->MimeName = $MimeName;
      return $this;
    }

    /**
     * @return string
     */
    public function getFileName()
    {
      return $this->FileName;
    }

    /**
     * @param string $FileName
     * @return AssetData
     */
    public function setFileName($FileName)
    {
      $this->FileName = $FileName;
      return $this;
    }

    /**
     * @return string
     */
    public function getFileExtension()
    {
      return $this->FileExtension;
    }

    /**
     * @param string $FileExtension
     * @return AssetData
     */
    public function setFileExtension($FileExtension)
    {
      $this->FileExtension = $FileExtension;
      return $this;
    }

    /**
     * @return string
     */
    public function getImageUrl()
    {
      return $this->ImageUrl;
    }

    /**
     * @param string $ImageUrl
     * @return AssetData
     */
    public function setImageUrl($ImageUrl)
    {
      $this->ImageUrl = $ImageUrl;
      return $this;
    }

    /**
     * @return string
     */
    public function getIcon()
    {
      return $this->Icon;
    }

    /**
     * @param string $Icon
     * @return AssetData
     */
    public function setIcon($Icon)
    {
      $this->Icon = $Icon;
      return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
      return $this->Status;
    }

    /**
     * @param string $Status
     * @return AssetData
     */
    public function setStatus($Status)
    {
      $this->Status = $Status;
      return $this;
    }

    /**
     * @return int
     */
    public function getLanguage()
    {
      return $this->Language;
    }

    /**
     * @param int $Language
     * @return AssetData
     */
    public function setLanguage($Language)
    {
      $this->Language = $Language;
      return $this;
    }

    /**
     * @return int
     */
    public function getType()
    {
      return $this->Type;
    }

    /**
     * @param int $Type
     * @return AssetData
     */
    public function setType($Type)
    {
      $this->Type = $Type;
      return $this;
    }

    /**
     * @return string
     */
    public function getPluginType()
    {
      return $this->PluginType;
    }

    /**
     * @param string $PluginType
     * @return AssetData
     */
    public function setPluginType($PluginType)
    {
      $this->PluginType = $PluginType;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getPublishPdfActive()
    {
      return $this->PublishPdfActive;
    }

    /**
     * @param boolean $PublishPdfActive
     * @return AssetData
     */
    public function setPublishPdfActive($PublishPdfActive)
    {
      $this->PublishPdfActive = $PublishPdfActive;
      return $this;
    }

}
