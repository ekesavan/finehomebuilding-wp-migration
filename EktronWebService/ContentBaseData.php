<?php

class ContentBaseData extends CmsLocalizedDataOfContentBaseData
{

    /**
     * @var string $Title
     */
    protected $Title = null;

    /**
     * @var string $Teaser
     */
    protected $Teaser = null;

    /**
     * @var string $Html
     */
    protected $Html = null;

    /**
     * @var string $Quicklink
     */
    protected $Quicklink = null;

    /**
     * @var string $Image
     */
    protected $Image = null;

    /**
     * @var boolean $IsPrivate
     */
    protected $IsPrivate = null;

    /**
     * @var int $Type
     */
    protected $Type = null;

    /**
     * @var CMSContentSubtype $SubType
     */
    protected $SubType = null;

    /**
     * @var int $ExternalTypeId
     */
    protected $ExternalTypeId = null;

    /**
     * @var AssetData $AssetData
     */
    protected $AssetData = null;

    /**
     * @param int $Id
     * @param int $LanguageId
     * @param boolean $IsPrivate
     * @param int $Type
     * @param CMSContentSubtype $SubType
     * @param int $ExternalTypeId
     */
    public function __construct($Id, $LanguageId, $IsPrivate, $Type, $SubType, $ExternalTypeId)
    {
      parent::__construct($Id, $LanguageId);
      $this->IsPrivate = $IsPrivate;
      $this->Type = $Type;
      $this->SubType = $SubType;
      $this->ExternalTypeId = $ExternalTypeId;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
      return $this->Title;
    }

    /**
     * @param string $Title
     * @return ContentBaseData
     */
    public function setTitle($Title)
    {
      $this->Title = $Title;
      return $this;
    }

    /**
     * @return string
     */
    public function getTeaser()
    {
      return $this->Teaser;
    }

    /**
     * @param string $Teaser
     * @return ContentBaseData
     */
    public function setTeaser($Teaser)
    {
      $this->Teaser = $Teaser;
      return $this;
    }

    /**
     * @return string
     */
    public function getHtml()
    {
      return $this->Html;
    }

    /**
     * @param string $Html
     * @return ContentBaseData
     */
    public function setHtml($Html)
    {
      $this->Html = $Html;
      return $this;
    }

    /**
     * @return string
     */
    public function getQuicklink()
    {
      return $this->Quicklink;
    }

    /**
     * @param string $Quicklink
     * @return ContentBaseData
     */
    public function setQuicklink($Quicklink)
    {
      $this->Quicklink = $Quicklink;
      return $this;
    }

    /**
     * @return string
     */
    public function getImage()
    {
      return $this->Image;
    }

    /**
     * @param string $Image
     * @return ContentBaseData
     */
    public function setImage($Image)
    {
      $this->Image = $Image;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIsPrivate()
    {
      return $this->IsPrivate;
    }

    /**
     * @param boolean $IsPrivate
     * @return ContentBaseData
     */
    public function setIsPrivate($IsPrivate)
    {
      $this->IsPrivate = $IsPrivate;
      return $this;
    }

    /**
     * @return int
     */
    public function getType()
    {
      return $this->Type;
    }

    /**
     * @param int $Type
     * @return ContentBaseData
     */
    public function setType($Type)
    {
      $this->Type = $Type;
      return $this;
    }

    /**
     * @return CMSContentSubtype
     */
    public function getSubType()
    {
      return $this->SubType;
    }

    /**
     * @param CMSContentSubtype $SubType
     * @return ContentBaseData
     */
    public function setSubType($SubType)
    {
      $this->SubType = $SubType;
      return $this;
    }

    /**
     * @return int
     */
    public function getExternalTypeId()
    {
      return $this->ExternalTypeId;
    }

    /**
     * @param int $ExternalTypeId
     * @return ContentBaseData
     */
    public function setExternalTypeId($ExternalTypeId)
    {
      $this->ExternalTypeId = $ExternalTypeId;
      return $this;
    }

    /**
     * @return AssetData
     */
    public function getAssetData()
    {
      return $this->AssetData;
    }

    /**
     * @param AssetData $AssetData
     * @return ContentBaseData
     */
    public function setAssetData($AssetData)
    {
      $this->AssetData = $AssetData;
      return $this;
    }

}
