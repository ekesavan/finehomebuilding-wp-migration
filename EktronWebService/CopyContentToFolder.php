<?php

class CopyContentToFolder
{

    /**
     * @var string $ContentID
     */
    protected $ContentID = null;

    /**
     * @var int $FolderID
     */
    protected $FolderID = null;

    /**
     * @var string $Language
     */
    protected $Language = null;

    /**
     * @var boolean $Publish
     */
    protected $Publish = null;

    /**
     * @param string $ContentID
     * @param int $FolderID
     * @param string $Language
     * @param boolean $Publish
     */
    public function __construct($ContentID, $FolderID, $Language, $Publish)
    {
      $this->ContentID = $ContentID;
      $this->FolderID = $FolderID;
      $this->Language = $Language;
      $this->Publish = $Publish;
    }

    /**
     * @return string
     */
    public function getContentID()
    {
      return $this->ContentID;
    }

    /**
     * @param string $ContentID
     * @return CopyContentToFolder
     */
    public function setContentID($ContentID)
    {
      $this->ContentID = $ContentID;
      return $this;
    }

    /**
     * @return int
     */
    public function getFolderID()
    {
      return $this->FolderID;
    }

    /**
     * @param int $FolderID
     * @return CopyContentToFolder
     */
    public function setFolderID($FolderID)
    {
      $this->FolderID = $FolderID;
      return $this;
    }

    /**
     * @return string
     */
    public function getLanguage()
    {
      return $this->Language;
    }

    /**
     * @param string $Language
     * @return CopyContentToFolder
     */
    public function setLanguage($Language)
    {
      $this->Language = $Language;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getPublish()
    {
      return $this->Publish;
    }

    /**
     * @param boolean $Publish
     * @return CopyContentToFolder
     */
    public function setPublish($Publish)
    {
      $this->Publish = $Publish;
      return $this;
    }

}
