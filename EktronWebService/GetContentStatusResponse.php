<?php

class GetContentStatusResponse
{

    /**
     * @var string $GetContentStatusResult
     */
    protected $GetContentStatusResult = null;

    /**
     * @param string $GetContentStatusResult
     */
    public function __construct($GetContentStatusResult)
    {
      $this->GetContentStatusResult = $GetContentStatusResult;
    }

    /**
     * @return string
     */
    public function getGetContentStatusResult()
    {
      return $this->GetContentStatusResult;
    }

    /**
     * @param string $GetContentStatusResult
     * @return GetContentStatusResponse
     */
    public function setGetContentStatusResult($GetContentStatusResult)
    {
      $this->GetContentStatusResult = $GetContentStatusResult;
      return $this;
    }

}
