<?php

class CommentData
{

    /**
     * @var int $CommentKeyId
     */
    protected $CommentKeyId = null;

    /**
     * @var int $CommentId
     */
    protected $CommentId = null;

    /**
     * @var int $RefId
     */
    protected $RefId = null;

    /**
     * @var int $Language
     */
    protected $Language = null;

    /**
     * @var string $CommentText
     */
    protected $CommentText = null;

    /**
     * @var int $UserId
     */
    protected $UserId = null;

    /**
     * @var string $FirstName
     */
    protected $FirstName = null;

    /**
     * @var string $LastName
     */
    protected $LastName = null;

    /**
     * @var string $DateModified
     */
    protected $DateModified = null;

    /**
     * @var string $DateCreated
     */
    protected $DateCreated = null;

    /**
     * @param int $CommentKeyId
     * @param int $CommentId
     * @param int $RefId
     * @param int $Language
     * @param int $UserId
     */
    public function __construct($CommentKeyId, $CommentId, $RefId, $Language, $UserId)
    {
      $this->CommentKeyId = $CommentKeyId;
      $this->CommentId = $CommentId;
      $this->RefId = $RefId;
      $this->Language = $Language;
      $this->UserId = $UserId;
    }

    /**
     * @return int
     */
    public function getCommentKeyId()
    {
      return $this->CommentKeyId;
    }

    /**
     * @param int $CommentKeyId
     * @return CommentData
     */
    public function setCommentKeyId($CommentKeyId)
    {
      $this->CommentKeyId = $CommentKeyId;
      return $this;
    }

    /**
     * @return int
     */
    public function getCommentId()
    {
      return $this->CommentId;
    }

    /**
     * @param int $CommentId
     * @return CommentData
     */
    public function setCommentId($CommentId)
    {
      $this->CommentId = $CommentId;
      return $this;
    }

    /**
     * @return int
     */
    public function getRefId()
    {
      return $this->RefId;
    }

    /**
     * @param int $RefId
     * @return CommentData
     */
    public function setRefId($RefId)
    {
      $this->RefId = $RefId;
      return $this;
    }

    /**
     * @return int
     */
    public function getLanguage()
    {
      return $this->Language;
    }

    /**
     * @param int $Language
     * @return CommentData
     */
    public function setLanguage($Language)
    {
      $this->Language = $Language;
      return $this;
    }

    /**
     * @return string
     */
    public function getCommentText()
    {
      return $this->CommentText;
    }

    /**
     * @param string $CommentText
     * @return CommentData
     */
    public function setCommentText($CommentText)
    {
      $this->CommentText = $CommentText;
      return $this;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
      return $this->UserId;
    }

    /**
     * @param int $UserId
     * @return CommentData
     */
    public function setUserId($UserId)
    {
      $this->UserId = $UserId;
      return $this;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
      return $this->FirstName;
    }

    /**
     * @param string $FirstName
     * @return CommentData
     */
    public function setFirstName($FirstName)
    {
      $this->FirstName = $FirstName;
      return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
      return $this->LastName;
    }

    /**
     * @param string $LastName
     * @return CommentData
     */
    public function setLastName($LastName)
    {
      $this->LastName = $LastName;
      return $this;
    }

    /**
     * @return string
     */
    public function getDateModified()
    {
      return $this->DateModified;
    }

    /**
     * @param string $DateModified
     * @return CommentData
     */
    public function setDateModified($DateModified)
    {
      $this->DateModified = $DateModified;
      return $this;
    }

    /**
     * @return string
     */
    public function getDateCreated()
    {
      return $this->DateCreated;
    }

    /**
     * @param string $DateCreated
     * @return CommentData
     */
    public function setDateCreated($DateCreated)
    {
      $this->DateCreated = $DateCreated;
      return $this;
    }

}
