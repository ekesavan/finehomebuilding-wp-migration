<?php

class ArrayOfContentData
{

    /**
     * @var ContentData[] $ContentData
     */
    protected $ContentData = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ContentData[]
     */
    public function getContentData()
    {
      return $this->ContentData;
    }

    /**
     * @param ContentData[] $ContentData
     * @return ArrayOfContentData
     */
    public function setContentData(array $ContentData = null)
    {
      $this->ContentData = $ContentData;
      return $this;
    }

}
