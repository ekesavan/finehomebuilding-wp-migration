<?php

class ContentMetaData extends CmsDataOfContentMetaData
{

    /**
     * @var string $Text
     */
    protected $Text = null;

    /**
     * @var string $Name
     */
    protected $Name = null;

    /**
     * @var ContentMetadataDataType $DataType
     */
    protected $DataType = null;

    /**
     * @var string $DefaultValue
     */
    protected $DefaultValue = null;

    /**
     * @var boolean $Required
     */
    protected $Required = null;

    /**
     * @var ContentMetadataType $Type
     */
    protected $Type = null;

    /**
     * @var boolean $IsEditable
     */
    protected $IsEditable = null;

    /**
     * @var string $Separator
     */
    protected $Separator = null;

    /**
     * @var boolean $CaseSensitive
     */
    protected $CaseSensitive = null;

    /**
     * @var boolean $RemoveDuplicate
     */
    protected $RemoveDuplicate = null;

    /**
     * @var boolean $IsSearchAllowed
     */
    protected $IsSearchAllowed = null;

    /**
     * @var boolean $IsDisplayable
     */
    protected $IsDisplayable = null;

    /**
     * @var int $Language
     */
    protected $Language = null;

    /**
     * @var string $SelectableText
     */
    protected $SelectableText = null;

    /**
     * @var boolean $AllowMultiple
     */
    protected $AllowMultiple = null;

    /**
     * @var boolean $IsSelectableOnly
     */
    protected $IsSelectableOnly = null;

    /**
     * @var boolean $MetaDisplayEE
     */
    protected $MetaDisplayEE = null;

    /**
     * @var int $BlogMetaType
     */
    protected $BlogMetaType = null;

    /**
     * @var int $ObjectType
     */
    protected $ObjectType = null;

    /**
     * @param int $Id
     * @param ContentMetadataDataType $DataType
     * @param boolean $Required
     * @param ContentMetadataType $Type
     * @param boolean $IsEditable
     * @param boolean $CaseSensitive
     * @param boolean $RemoveDuplicate
     * @param boolean $IsSearchAllowed
     * @param boolean $IsDisplayable
     * @param int $Language
     * @param boolean $AllowMultiple
     * @param boolean $IsSelectableOnly
     * @param boolean $MetaDisplayEE
     * @param int $BlogMetaType
     * @param int $ObjectType
     */
    public function __construct($Id, $DataType, $Required, $Type, $IsEditable, $CaseSensitive, $RemoveDuplicate, $IsSearchAllowed, $IsDisplayable, $Language, $AllowMultiple, $IsSelectableOnly, $MetaDisplayEE, $BlogMetaType, $ObjectType)
    {
      parent::__construct($Id);
      $this->DataType = $DataType;
      $this->Required = $Required;
      $this->Type = $Type;
      $this->IsEditable = $IsEditable;
      $this->CaseSensitive = $CaseSensitive;
      $this->RemoveDuplicate = $RemoveDuplicate;
      $this->IsSearchAllowed = $IsSearchAllowed;
      $this->IsDisplayable = $IsDisplayable;
      $this->Language = $Language;
      $this->AllowMultiple = $AllowMultiple;
      $this->IsSelectableOnly = $IsSelectableOnly;
      $this->MetaDisplayEE = $MetaDisplayEE;
      $this->BlogMetaType = $BlogMetaType;
      $this->ObjectType = $ObjectType;
    }

    /**
     * @return string
     */
    public function getText()
    {
      return $this->Text;
    }

    /**
     * @param string $Text
     * @return ContentMetaData
     */
    public function setText($Text)
    {
      $this->Text = $Text;
      return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
      return $this->Name;
    }

    /**
     * @param string $Name
     * @return ContentMetaData
     */
    public function setName($Name)
    {
      $this->Name = $Name;
      return $this;
    }

    /**
     * @return ContentMetadataDataType
     */
    public function getDataType()
    {
      return $this->DataType;
    }

    /**
     * @param ContentMetadataDataType $DataType
     * @return ContentMetaData
     */
    public function setDataType($DataType)
    {
      $this->DataType = $DataType;
      return $this;
    }

    /**
     * @return string
     */
    public function getDefaultValue()
    {
      return $this->DefaultValue;
    }

    /**
     * @param string $DefaultValue
     * @return ContentMetaData
     */
    public function setDefaultValue($DefaultValue)
    {
      $this->DefaultValue = $DefaultValue;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getRequired()
    {
      return $this->Required;
    }

    /**
     * @param boolean $Required
     * @return ContentMetaData
     */
    public function setRequired($Required)
    {
      $this->Required = $Required;
      return $this;
    }

    /**
     * @return ContentMetadataType
     */
    public function getType()
    {
      return $this->Type;
    }

    /**
     * @param ContentMetadataType $Type
     * @return ContentMetaData
     */
    public function setType($Type)
    {
      $this->Type = $Type;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIsEditable()
    {
      return $this->IsEditable;
    }

    /**
     * @param boolean $IsEditable
     * @return ContentMetaData
     */
    public function setIsEditable($IsEditable)
    {
      $this->IsEditable = $IsEditable;
      return $this;
    }

    /**
     * @return string
     */
    public function getSeparator()
    {
      return $this->Separator;
    }

    /**
     * @param string $Separator
     * @return ContentMetaData
     */
    public function setSeparator($Separator)
    {
      $this->Separator = $Separator;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getCaseSensitive()
    {
      return $this->CaseSensitive;
    }

    /**
     * @param boolean $CaseSensitive
     * @return ContentMetaData
     */
    public function setCaseSensitive($CaseSensitive)
    {
      $this->CaseSensitive = $CaseSensitive;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getRemoveDuplicate()
    {
      return $this->RemoveDuplicate;
    }

    /**
     * @param boolean $RemoveDuplicate
     * @return ContentMetaData
     */
    public function setRemoveDuplicate($RemoveDuplicate)
    {
      $this->RemoveDuplicate = $RemoveDuplicate;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIsSearchAllowed()
    {
      return $this->IsSearchAllowed;
    }

    /**
     * @param boolean $IsSearchAllowed
     * @return ContentMetaData
     */
    public function setIsSearchAllowed($IsSearchAllowed)
    {
      $this->IsSearchAllowed = $IsSearchAllowed;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIsDisplayable()
    {
      return $this->IsDisplayable;
    }

    /**
     * @param boolean $IsDisplayable
     * @return ContentMetaData
     */
    public function setIsDisplayable($IsDisplayable)
    {
      $this->IsDisplayable = $IsDisplayable;
      return $this;
    }

    /**
     * @return int
     */
    public function getLanguage()
    {
      return $this->Language;
    }

    /**
     * @param int $Language
     * @return ContentMetaData
     */
    public function setLanguage($Language)
    {
      $this->Language = $Language;
      return $this;
    }

    /**
     * @return string
     */
    public function getSelectableText()
    {
      return $this->SelectableText;
    }

    /**
     * @param string $SelectableText
     * @return ContentMetaData
     */
    public function setSelectableText($SelectableText)
    {
      $this->SelectableText = $SelectableText;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getAllowMultiple()
    {
      return $this->AllowMultiple;
    }

    /**
     * @param boolean $AllowMultiple
     * @return ContentMetaData
     */
    public function setAllowMultiple($AllowMultiple)
    {
      $this->AllowMultiple = $AllowMultiple;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIsSelectableOnly()
    {
      return $this->IsSelectableOnly;
    }

    /**
     * @param boolean $IsSelectableOnly
     * @return ContentMetaData
     */
    public function setIsSelectableOnly($IsSelectableOnly)
    {
      $this->IsSelectableOnly = $IsSelectableOnly;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getMetaDisplayEE()
    {
      return $this->MetaDisplayEE;
    }

    /**
     * @param boolean $MetaDisplayEE
     * @return ContentMetaData
     */
    public function setMetaDisplayEE($MetaDisplayEE)
    {
      $this->MetaDisplayEE = $MetaDisplayEE;
      return $this;
    }

    /**
     * @return int
     */
    public function getBlogMetaType()
    {
      return $this->BlogMetaType;
    }

    /**
     * @param int $BlogMetaType
     * @return ContentMetaData
     */
    public function setBlogMetaType($BlogMetaType)
    {
      $this->BlogMetaType = $BlogMetaType;
      return $this;
    }

    /**
     * @return int
     */
    public function getObjectType()
    {
      return $this->ObjectType;
    }

    /**
     * @param int $ObjectType
     * @return ContentMetaData
     */
    public function setObjectType($ObjectType)
    {
      $this->ObjectType = $ObjectType;
      return $this;
    }

}
