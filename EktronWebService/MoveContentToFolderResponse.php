<?php

class MoveContentToFolderResponse
{

    /**
     * @var ContentData $MoveContentToFolderResult
     */
    protected $MoveContentToFolderResult = null;

    /**
     * @param ContentData $MoveContentToFolderResult
     */
    public function __construct($MoveContentToFolderResult)
    {
      $this->MoveContentToFolderResult = $MoveContentToFolderResult;
    }

    /**
     * @return ContentData
     */
    public function getMoveContentToFolderResult()
    {
      return $this->MoveContentToFolderResult;
    }

    /**
     * @param ContentData $MoveContentToFolderResult
     * @return MoveContentToFolderResponse
     */
    public function setMoveContentToFolderResult($MoveContentToFolderResult)
    {
      $this->MoveContentToFolderResult = $MoveContentToFolderResult;
      return $this;
    }

}
