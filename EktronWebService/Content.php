<?php

class Content extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     */
    private static $classmap = array (
      'CheckOutContent' => '\\CheckOutContent',
      'CheckOutContentResponse' => '\\CheckOutContentResponse',
      'AuthenticationHeader' => '\\AuthenticationHeader',
      'RequestInfoParameters' => '\\RequestInfoParameters',
      'SaveContent' => '\\SaveContent',
      'ContentEditData' => '\\ContentEditData',
      'ContentData' => '\\ContentData',
      'ContentBaseData' => '\\ContentBaseData',
      'CmsLocalizedDataOfContentBaseData' => '\\CmsLocalizedDataOfContentBaseData',
      'CmsDataOfContentBaseData' => '\\CmsDataOfContentBaseData',
      'BaseDataOfContentBaseData' => '\\BaseDataOfContentBaseData',
      'AssetData' => '\\AssetData',
      'ArrayOfContentMetaData' => '\\ArrayOfContentMetaData',
      'ContentMetaData' => '\\ContentMetaData',
      'CmsDataOfContentMetaData' => '\\CmsDataOfContentMetaData',
      'BaseDataOfContentMetaData' => '\\BaseDataOfContentMetaData',
      'XmlConfigData' => '\\XmlConfigData',
      'TemplateData' => '\\TemplateData',
      'CmsDataOfTemplateData' => '\\CmsDataOfTemplateData',
      'BaseDataOfTemplateData' => '\\BaseDataOfTemplateData',
      'SaveContentResponse' => '\\SaveContentResponse',
      'CopyContentToFolder' => '\\CopyContentToFolder',
      'CopyContentToFolderResponse' => '\\CopyContentToFolderResponse',
      'AddContent' => '\\AddContent',
      'AddContentResponse' => '\\AddContentResponse',
      'AddContentWithUser' => '\\AddContentWithUser',
      'AddContentWithUserResponse' => '\\AddContentWithUserResponse',
      'AddContent_x0020_With_x0020_overload_x0020_Parameter_x0020_CleanHtml' => '\\AddContent_x0020_With_x0020_overload_x0020_Parameter_x0020_CleanHtml',
      'AddContent_x0020_With_x0020_overload_x0020_Parameter_x0020_CleanHtmlResponse' => '\\AddContent_x0020_With_x0020_overload_x0020_Parameter_x0020_CleanHtmlResponse',
      'AddContent_x0020_With_x0020_ContentID' => '\\AddContent_x0020_With_x0020_ContentID',
      'AddContent_x0020_With_x0020_ContentIDResponse' => '\\AddContent_x0020_With_x0020_ContentIDResponse',
      'AddContent_x0020_with_x0020_XmlID_x0020_and_x0020_TemplateID' => '\\AddContent_x0020_with_x0020_XmlID_x0020_and_x0020_TemplateID',
      'AddContent_x0020_with_x0020_XmlID_x0020_and_x0020_TemplateIDResponse' => '\\AddContent_x0020_with_x0020_XmlID_x0020_and_x0020_TemplateIDResponse',
      'GetContentForEditing' => '\\GetContentForEditing',
      'GetContentForEditingResponse' => '\\GetContentForEditingResponse',
      'DeleteContentItem' => '\\DeleteContentItem',
      'DeleteContentItemResponse' => '\\DeleteContentItemResponse',
      'GetAddViewLanguage' => '\\GetAddViewLanguage',
      'LanguageData' => '\\LanguageData',
      'ArrayOfLanguageData' => '\\ArrayOfLanguageData',
      'GetAddViewLanguageResponse' => '\\GetAddViewLanguageResponse',
      'GetAllComments' => '\\GetAllComments',
      'CommentData' => '\\CommentData',
      'ArrayOfCommentData' => '\\ArrayOfCommentData',
      'GetAllCommentsResponse' => '\\GetAllCommentsResponse',
      'GetAllTemplates' => '\\GetAllTemplates',
      'ArrayOfTemplateData' => '\\ArrayOfTemplateData',
      'GetAllTemplatesResponse' => '\\GetAllTemplatesResponse',
      'UpdateContentMetaData' => '\\UpdateContentMetaData',
      'UpdateContentMetaDataResponse' => '\\UpdateContentMetaDataResponse',
      'GetAllUnassignedItemApprovals' => '\\GetAllUnassignedItemApprovals',
      'ApprovalData' => '\\ApprovalData',
      'ArrayOfApprovalData' => '\\ArrayOfApprovalData',
      'GetAllUnassignedItemApprovalsResponse' => '\\GetAllUnassignedItemApprovalsResponse',
      'GetChildContent' => '\\GetChildContent',
      'ArrayOfContentData' => '\\ArrayOfContentData',
      'GetChildContentResponse' => '\\GetChildContentResponse',
      'GetChildContentWithPaging' => '\\GetChildContentWithPaging',
      'PagingInfo' => '\\PagingInfo',
      'GetChildContentWithPagingResponse' => '\\GetChildContentWithPagingResponse',
      'GetContent' => '\\GetContent',
      'GetContentResponse' => '\\GetContentResponse',
      'GetContentByHistory' => '\\GetContentByHistory',
      'GetContentByHistoryResponse' => '\\GetContentByHistoryResponse',
      'GetContentState' => '\\GetContentState',
      'GetContentStateResponse' => '\\GetContentStateResponse',
      'ContentStateData' => '\\ContentStateData',
      'GetContentStatus' => '\\GetContentStatus',
      'GetContentStatusResponse' => '\\GetContentStatusResponse',
      'GetDomain' => '\\GetDomain',
      'GetDomainResponse' => '\\GetDomainResponse',
      'MoveContentToFolder' => '\\MoveContentToFolder',
      'MoveContentToFolderResponse' => '\\MoveContentToFolderResponse',
      'PublishContent' => '\\PublishContent',
      'PublishContentResponse' => '\\PublishContentResponse',
      'RenameContent' => '\\RenameContent',
      'RenameContentResponse' => '\\RenameContentResponse',
      'SubmitForDelete' => '\\SubmitForDelete',
      'SubmitForDeleteResponse' => '\\SubmitForDeleteResponse',
      'UndoCheckout' => '\\UndoCheckout',
      'UndoCheckoutResponse' => '\\UndoCheckoutResponse',
      'NotifySubscriptionForSynchronizedContent' => '\\NotifySubscriptionForSynchronizedContent',
      'NotifySubscriptionForSynchronizedContentResponse' => '\\NotifySubscriptionForSynchronizedContentResponse',
      'NotifySubscriptionForSynchronizedContentByType' => '\\NotifySubscriptionForSynchronizedContentByType',
      'NotifySubscriptionForSynchronizedContentByTypeResponse' => '\\NotifySubscriptionForSynchronizedContentByTypeResponse',
    );

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     */
    public function __construct(array $options = array(), $wsdl = 'http://cmsedit.taunton.dev:8083/Workarea/webservices/Content.asmx?WSDL')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      $options = array_merge(array (
      'features' => 1,
    ), $options);
      parent::__construct($wsdl, $options);
    }

    /**
     * Checks the content out to the current user
     *
     * @param CheckOutContent $parameters
     * @return CheckOutContentResponse
     */
    public function CheckOutContent(CheckOutContent $parameters)
    {
      return $this->__soapCall('CheckOutContent', array($parameters));
    }

    /**
     * Updates the checked out content block.
     *
     * @param SaveContent $parameters
     * @return SaveContentResponse
     */
    public function SaveContent(SaveContent $parameters)
    {
      return $this->__soapCall('SaveContent', array($parameters));
    }

    /**
     * Copy the content into required folder.
     *
     * @param CopyContentToFolder $parameters
     * @return CopyContentToFolderResponse
     */
    public function CopyContentToFolder(CopyContentToFolder $parameters)
    {
      return $this->__soapCall('CopyContentToFolder', array($parameters));
    }

    /**
     * Adds a piece of content.
     *
     * @param AddContent $parameters
     * @return AddContentResponse
     */
    public function AddContent(AddContent $parameters)
    {
      return $this->__soapCall('AddContent', array($parameters));
    }

    /**
     * Loads the content details with checkout mode
     *
     * @param GetContentForEditing $parameters
     * @return GetContentForEditingResponse
     */
    public function GetContentForEditing(GetContentForEditing $parameters)
    {
      return $this->__soapCall('GetContentForEditing', array($parameters));
    }

    /**
     * Delete the content by the content's ID
     *
     * @param DeleteContentItem $parameters
     * @return DeleteContentItemResponse
     */
    public function DeleteContentItem(DeleteContentItem $parameters)
    {
      return $this->__soapCall('DeleteContentItem', array($parameters));
    }

    /**
     * Gets all the languages from the system for a given content ID
     *
     * @param GetAddViewLanguage $parameters
     * @return GetAddViewLanguageResponse
     */
    public function GetAddViewLanguage(GetAddViewLanguage $parameters)
    {
      return $this->__soapCall('GetAddViewLanguage', array($parameters));
    }

    /**
     * Gets all comments
     *
     * @param GetAllComments $parameters
     * @return GetAllCommentsResponse
     */
    public function GetAllComments(GetAllComments $parameters)
    {
      return $this->__soapCall('GetAllComments', array($parameters));
    }

    /**
     * Gets all templates
     *
     * @param GetAllTemplates $parameters
     * @return GetAllTemplatesResponse
     */
    public function GetAllTemplates(GetAllTemplates $parameters)
    {
      return $this->__soapCall('GetAllTemplates', array($parameters));
    }

    /**
     * Updates Content Metadata
     *
     * @param UpdateContentMetaData $parameters
     * @return UpdateContentMetaDataResponse
     */
    public function UpdateContentMetaData(UpdateContentMetaData $parameters)
    {
      return $this->__soapCall('UpdateContentMetaData', array($parameters));
    }

    /**
     * Returns all of the unassigned user/groups for a given content ID
     *
     * @param GetAllUnassignedItemApprovals $parameters
     * @return GetAllUnassignedItemApprovalsResponse
     */
    public function GetAllUnassignedItemApprovals(GetAllUnassignedItemApprovals $parameters)
    {
      return $this->__soapCall('GetAllUnassignedItemApprovals', array($parameters));
    }

    /**
     * Loads all of the contents for the given folder
     *
     * @param GetChildContent $parameters
     * @return GetChildContentResponse
     */
    public function GetChildContent(GetChildContent $parameters)
    {
      return $this->__soapCall('GetChildContent', array($parameters));
    }

    /**
     * Gets the content details by content id
     *
     * @param GetContent $parameters
     * @return GetContentResponse
     */
    public function GetContent(GetContent $parameters)
    {
      return $this->__soapCall('GetContent', array($parameters));
    }

    /**
     * Retrieves the corresponding content by the history ID
     *
     * @param GetContentByHistory $parameters
     * @return GetContentByHistoryResponse
     */
    public function GetContentByHistory(GetContentByHistory $parameters)
    {
      return $this->__soapCall('GetContentByHistory', array($parameters));
    }

    /**
     * Gets the content's details
     *
     * @param GetContentState $parameters
     * @return GetContentStateResponse
     */
    public function GetContentState(GetContentState $parameters)
    {
      return $this->__soapCall('GetContentState', array($parameters));
    }

    /**
     * Gets single letter string which is the status of the piece of content
     *
     * @param GetContentStatus $parameters
     * @return GetContentStatusResponse
     */
    public function GetContentStatus(GetContentStatus $parameters)
    {
      return $this->__soapCall('GetContentStatus', array($parameters));
    }

    /**
     * Gets web domain for a piece of content
     *
     * @param GetDomain $parameters
     * @return GetDomainResponse
     */
    public function GetDomain(GetDomain $parameters)
    {
      return $this->__soapCall('GetDomain', array($parameters));
    }

    /**
     * Moves content to a folder
     *
     * @param MoveContentToFolder $parameters
     * @return MoveContentToFolderResponse
     */
    public function MoveContentToFolder(MoveContentToFolder $parameters)
    {
      return $this->__soapCall('MoveContentToFolder', array($parameters));
    }

    /**
     * Function takes in information about the content to be checked in and published. This goes through the normal approval chain. The content must be in a checked out state to the current logged in user for this to succeed
     *
     * @param PublishContent $parameters
     * @return PublishContentResponse
     */
    public function PublishContent(PublishContent $parameters)
    {
      return $this->__soapCall('PublishContent', array($parameters));
    }

    /**
     * Renames the content by ID
     *
     * @param RenameContent $parameters
     * @return RenameContentResponse
     */
    public function RenameContent(RenameContent $parameters)
    {
      return $this->__soapCall('RenameContent', array($parameters));
    }

    /**
     * Deletes the content by the content's Id and folder Id
     *
     * @param SubmitForDelete $parameters
     * @return SubmitForDeleteResponse
     */
    public function SubmitForDelete(SubmitForDelete $parameters)
    {
      return $this->__soapCall('SubmitForDelete', array($parameters));
    }

    /**
     * Reverts the checkout for a piece of content which is checked out to the current user
     *
     * @param UndoCheckout $parameters
     * @return UndoCheckoutResponse
     */
    public function UndoCheckout(UndoCheckout $parameters)
    {
      return $this->__soapCall('UndoCheckout', array($parameters));
    }

    /**
     * Notify subscription for synchronized content.
     *
     * @param NotifySubscriptionForSynchronizedContent $parameters
     * @return NotifySubscriptionForSynchronizedContentResponse
     */
    public function NotifySubscriptionForSynchronizedContent(NotifySubscriptionForSynchronizedContent $parameters)
    {
      return $this->__soapCall('NotifySubscriptionForSynchronizedContent', array($parameters));
    }

    /**
     * Notify subscription for synchronized content.
     *
     * @param NotifySubscriptionForSynchronizedContentByType $parameters
     * @return NotifySubscriptionForSynchronizedContentByTypeResponse
     */
    public function NotifySubscriptionForSynchronizedContentByType(NotifySubscriptionForSynchronizedContentByType $parameters)
    {
      return $this->__soapCall('NotifySubscriptionForSynchronizedContentByType', array($parameters));
    }

}
