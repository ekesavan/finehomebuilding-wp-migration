<?php

class GetContent
{

    /**
     * @var int $ContentID
     */
    protected $ContentID = null;

    /**
     * @var ContentResultType $ResultType
     */
    protected $ResultType = null;

    /**
     * @param int $ContentID
     * @param ContentResultType $ResultType
     */
    public function __construct($ContentID, $ResultType)
    {
      $this->ContentID = $ContentID;
      $this->ResultType = $ResultType;
    }

    /**
     * @return int
     */
    public function getContentID()
    {
      return $this->ContentID;
    }

    /**
     * @param int $ContentID
     * @return GetContent
     */
    public function setContentID($ContentID)
    {
      $this->ContentID = $ContentID;
      return $this;
    }

    /**
     * @return ContentResultType
     */
    public function getResultType()
    {
      return $this->ResultType;
    }

    /**
     * @param ContentResultType $ResultType
     * @return GetContent
     */
    public function setResultType($ResultType)
    {
      $this->ResultType = $ResultType;
      return $this;
    }

}
