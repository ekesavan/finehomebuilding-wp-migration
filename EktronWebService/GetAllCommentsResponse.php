<?php

class GetAllCommentsResponse
{

    /**
     * @var ArrayOfCommentData $GetAllCommentsResult
     */
    protected $GetAllCommentsResult = null;

    /**
     * @param ArrayOfCommentData $GetAllCommentsResult
     */
    public function __construct($GetAllCommentsResult)
    {
      $this->GetAllCommentsResult = $GetAllCommentsResult;
    }

    /**
     * @return ArrayOfCommentData
     */
    public function getGetAllCommentsResult()
    {
      return $this->GetAllCommentsResult;
    }

    /**
     * @param ArrayOfCommentData $GetAllCommentsResult
     * @return GetAllCommentsResponse
     */
    public function setGetAllCommentsResult($GetAllCommentsResult)
    {
      $this->GetAllCommentsResult = $GetAllCommentsResult;
      return $this;
    }

}
