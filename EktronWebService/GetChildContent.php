<?php

class GetChildContent
{

    /**
     * @var int $FolderID
     */
    protected $FolderID = null;

    /**
     * @var boolean $Recursive
     */
    protected $Recursive = null;

    /**
     * @var string $OrderBy
     */
    protected $OrderBy = null;

    /**
     * @param int $FolderID
     * @param boolean $Recursive
     * @param string $OrderBy
     */
    public function __construct($FolderID, $Recursive, $OrderBy)
    {
      $this->FolderID = $FolderID;
      $this->Recursive = $Recursive;
      $this->OrderBy = $OrderBy;
    }

    /**
     * @return int
     */
    public function getFolderID()
    {
      return $this->FolderID;
    }

    /**
     * @param int $FolderID
     * @return GetChildContent
     */
    public function setFolderID($FolderID)
    {
      $this->FolderID = $FolderID;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getRecursive()
    {
      return $this->Recursive;
    }

    /**
     * @param boolean $Recursive
     * @return GetChildContent
     */
    public function setRecursive($Recursive)
    {
      $this->Recursive = $Recursive;
      return $this;
    }

    /**
     * @return string
     */
    public function getOrderBy()
    {
      return $this->OrderBy;
    }

    /**
     * @param string $OrderBy
     * @return GetChildContent
     */
    public function setOrderBy($OrderBy)
    {
      $this->OrderBy = $OrderBy;
      return $this;
    }

}
