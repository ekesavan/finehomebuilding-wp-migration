<?php

class GetDomainResponse
{

    /**
     * @var string $GetDomainResult
     */
    protected $GetDomainResult = null;

    /**
     * @param string $GetDomainResult
     */
    public function __construct($GetDomainResult)
    {
      $this->GetDomainResult = $GetDomainResult;
    }

    /**
     * @return string
     */
    public function getGetDomainResult()
    {
      return $this->GetDomainResult;
    }

    /**
     * @param string $GetDomainResult
     * @return GetDomainResponse
     */
    public function setGetDomainResult($GetDomainResult)
    {
      $this->GetDomainResult = $GetDomainResult;
      return $this;
    }

}
