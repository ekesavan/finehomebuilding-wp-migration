<?php

class LanguageData
{

    /**
     * @var int $Id
     */
    protected $Id = null;

    /**
     * @var int $Activated
     */
    protected $Activated = null;

    /**
     * @var string $Type
     */
    protected $Type = null;

    /**
     * @var string $Name
     */
    protected $Name = null;

    /**
     * @var string $ImagePath
     */
    protected $ImagePath = null;

    /**
     * @var string $CharSet
     */
    protected $CharSet = null;

    /**
     * @var string $BrowserCode
     */
    protected $BrowserCode = null;

    /**
     * @var string $Direction
     */
    protected $Direction = null;

    /**
     * @var string $FlagFile
     */
    protected $FlagFile = null;

    /**
     * @var string $XmlLang
     */
    protected $XmlLang = null;

    /**
     * @var boolean $SiteEnabled
     */
    protected $SiteEnabled = null;

    /**
     * @var string $LocalName
     */
    protected $LocalName = null;

    /**
     * @var anyType $LegacyData
     */
    protected $LegacyData = null;

    /**
     * @param int $Id
     * @param int $Activated
     * @param boolean $SiteEnabled
     */
    public function __construct($Id, $Activated, $SiteEnabled)
    {
      $this->Id = $Id;
      $this->Activated = $Activated;
      $this->SiteEnabled = $SiteEnabled;
    }

    /**
     * @return int
     */
    public function getId()
    {
      return $this->Id;
    }

    /**
     * @param int $Id
     * @return LanguageData
     */
    public function setId($Id)
    {
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return int
     */
    public function getActivated()
    {
      return $this->Activated;
    }

    /**
     * @param int $Activated
     * @return LanguageData
     */
    public function setActivated($Activated)
    {
      $this->Activated = $Activated;
      return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
      return $this->Type;
    }

    /**
     * @param string $Type
     * @return LanguageData
     */
    public function setType($Type)
    {
      $this->Type = $Type;
      return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
      return $this->Name;
    }

    /**
     * @param string $Name
     * @return LanguageData
     */
    public function setName($Name)
    {
      $this->Name = $Name;
      return $this;
    }

    /**
     * @return string
     */
    public function getImagePath()
    {
      return $this->ImagePath;
    }

    /**
     * @param string $ImagePath
     * @return LanguageData
     */
    public function setImagePath($ImagePath)
    {
      $this->ImagePath = $ImagePath;
      return $this;
    }

    /**
     * @return string
     */
    public function getCharSet()
    {
      return $this->CharSet;
    }

    /**
     * @param string $CharSet
     * @return LanguageData
     */
    public function setCharSet($CharSet)
    {
      $this->CharSet = $CharSet;
      return $this;
    }

    /**
     * @return string
     */
    public function getBrowserCode()
    {
      return $this->BrowserCode;
    }

    /**
     * @param string $BrowserCode
     * @return LanguageData
     */
    public function setBrowserCode($BrowserCode)
    {
      $this->BrowserCode = $BrowserCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getDirection()
    {
      return $this->Direction;
    }

    /**
     * @param string $Direction
     * @return LanguageData
     */
    public function setDirection($Direction)
    {
      $this->Direction = $Direction;
      return $this;
    }

    /**
     * @return string
     */
    public function getFlagFile()
    {
      return $this->FlagFile;
    }

    /**
     * @param string $FlagFile
     * @return LanguageData
     */
    public function setFlagFile($FlagFile)
    {
      $this->FlagFile = $FlagFile;
      return $this;
    }

    /**
     * @return string
     */
    public function getXmlLang()
    {
      return $this->XmlLang;
    }

    /**
     * @param string $XmlLang
     * @return LanguageData
     */
    public function setXmlLang($XmlLang)
    {
      $this->XmlLang = $XmlLang;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getSiteEnabled()
    {
      return $this->SiteEnabled;
    }

    /**
     * @param boolean $SiteEnabled
     * @return LanguageData
     */
    public function setSiteEnabled($SiteEnabled)
    {
      $this->SiteEnabled = $SiteEnabled;
      return $this;
    }

    /**
     * @return string
     */
    public function getLocalName()
    {
      return $this->LocalName;
    }

    /**
     * @param string $LocalName
     * @return LanguageData
     */
    public function setLocalName($LocalName)
    {
      $this->LocalName = $LocalName;
      return $this;
    }

    /**
     * @return anyType
     */
    public function getLegacyData()
    {
      return $this->LegacyData;
    }

    /**
     * @param anyType $LegacyData
     * @return LanguageData
     */
    public function setLegacyData($LegacyData)
    {
      $this->LegacyData = $LegacyData;
      return $this;
    }

}
