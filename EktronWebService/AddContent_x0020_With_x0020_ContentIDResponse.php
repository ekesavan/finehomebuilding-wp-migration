<?php

class AddContent_x0020_With_x0020_ContentIDResponse
{

    /**
     * @var int $AddContent_x0020_With_x0020_ContentIDResult
     */
    protected $AddContent_x0020_With_x0020_ContentIDResult = null;

    /**
     * @param int $AddContent_x0020_With_x0020_ContentIDResult
     */
    public function __construct($AddContent_x0020_With_x0020_ContentIDResult)
    {
      $this->AddContent_x0020_With_x0020_ContentIDResult = $AddContent_x0020_With_x0020_ContentIDResult;
    }

    /**
     * @return int
     */
    public function getAddContent_x0020_With_x0020_ContentIDResult()
    {
      return $this->AddContent_x0020_With_x0020_ContentIDResult;
    }

    /**
     * @param int $AddContent_x0020_With_x0020_ContentIDResult
     * @return AddContent_x0020_With_x0020_ContentIDResponse
     */
    public function setAddContent_x0020_With_x0020_ContentIDResult($AddContent_x0020_With_x0020_ContentIDResult)
    {
      $this->AddContent_x0020_With_x0020_ContentIDResult = $AddContent_x0020_With_x0020_ContentIDResult;
      return $this;
    }

}
