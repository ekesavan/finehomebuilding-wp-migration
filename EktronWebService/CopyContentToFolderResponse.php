<?php

class CopyContentToFolderResponse
{

    /**
     * @var ContentData $CopyContentToFolderResult
     */
    protected $CopyContentToFolderResult = null;

    /**
     * @param ContentData $CopyContentToFolderResult
     */
    public function __construct($CopyContentToFolderResult)
    {
      $this->CopyContentToFolderResult = $CopyContentToFolderResult;
    }

    /**
     * @return ContentData
     */
    public function getCopyContentToFolderResult()
    {
      return $this->CopyContentToFolderResult;
    }

    /**
     * @param ContentData $CopyContentToFolderResult
     * @return CopyContentToFolderResponse
     */
    public function setCopyContentToFolderResult($CopyContentToFolderResult)
    {
      $this->CopyContentToFolderResult = $CopyContentToFolderResult;
      return $this;
    }

}
