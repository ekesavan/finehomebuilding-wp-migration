<?php

class PagingInfo
{

    /**
     * @var int $RecordsPerPage
     */
    protected $RecordsPerPage = null;

    /**
     * @var int $CurrentPage
     */
    protected $CurrentPage = null;

    /**
     * @var int $TotalPages
     */
    protected $TotalPages = null;

    /**
     * @var int $TotalRecords
     */
    protected $TotalRecords = null;

    /**
     * @param int $RecordsPerPage
     * @param int $CurrentPage
     * @param int $TotalPages
     * @param int $TotalRecords
     */
    public function __construct($RecordsPerPage, $CurrentPage, $TotalPages, $TotalRecords)
    {
      $this->RecordsPerPage = $RecordsPerPage;
      $this->CurrentPage = $CurrentPage;
      $this->TotalPages = $TotalPages;
      $this->TotalRecords = $TotalRecords;
    }

    /**
     * @return int
     */
    public function getRecordsPerPage()
    {
      return $this->RecordsPerPage;
    }

    /**
     * @param int $RecordsPerPage
     * @return PagingInfo
     */
    public function setRecordsPerPage($RecordsPerPage)
    {
      $this->RecordsPerPage = $RecordsPerPage;
      return $this;
    }

    /**
     * @return int
     */
    public function getCurrentPage()
    {
      return $this->CurrentPage;
    }

    /**
     * @param int $CurrentPage
     * @return PagingInfo
     */
    public function setCurrentPage($CurrentPage)
    {
      $this->CurrentPage = $CurrentPage;
      return $this;
    }

    /**
     * @return int
     */
    public function getTotalPages()
    {
      return $this->TotalPages;
    }

    /**
     * @param int $TotalPages
     * @return PagingInfo
     */
    public function setTotalPages($TotalPages)
    {
      $this->TotalPages = $TotalPages;
      return $this;
    }

    /**
     * @return int
     */
    public function getTotalRecords()
    {
      return $this->TotalRecords;
    }

    /**
     * @param int $TotalRecords
     * @return PagingInfo
     */
    public function setTotalRecords($TotalRecords)
    {
      $this->TotalRecords = $TotalRecords;
      return $this;
    }

}
