<?php

class AddContentWithUser
{

    /**
     * @var string $Username
     */
    protected $Username = null;

    /**
     * @var string $Password
     */
    protected $Password = null;

    /**
     * @var string $Domain
     */
    protected $Domain = null;

    /**
     * @var string $ContentTitle
     */
    protected $ContentTitle = null;

    /**
     * @var string $ContentComment
     */
    protected $ContentComment = null;

    /**
     * @var string $ContentHtml
     */
    protected $ContentHtml = null;

    /**
     * @var string $SearchText
     */
    protected $SearchText = null;

    /**
     * @var string $SummaryHtml
     */
    protected $SummaryHtml = null;

    /**
     * @var string $ContentLanguage
     */
    protected $ContentLanguage = null;

    /**
     * @var int $FolderId
     */
    protected $FolderId = null;

    /**
     * @var anyType $GoLive
     */
    protected $GoLive = null;

    /**
     * @var anyType $EndDate
     */
    protected $EndDate = null;

    /**
     * @var string $MetaInfoXml
     */
    protected $MetaInfoXml = null;

    /**
     * @param string $Username
     * @param string $Password
     * @param string $Domain
     * @param string $ContentTitle
     * @param string $ContentComment
     * @param string $ContentHtml
     * @param string $SearchText
     * @param string $SummaryHtml
     * @param string $ContentLanguage
     * @param int $FolderId
     * @param anyType $GoLive
     * @param anyType $EndDate
     * @param string $MetaInfoXml
     */
    public function __construct($Username, $Password, $Domain, $ContentTitle, $ContentComment, $ContentHtml, $SearchText, $SummaryHtml, $ContentLanguage, $FolderId, $GoLive, $EndDate, $MetaInfoXml)
    {
      $this->Username = $Username;
      $this->Password = $Password;
      $this->Domain = $Domain;
      $this->ContentTitle = $ContentTitle;
      $this->ContentComment = $ContentComment;
      $this->ContentHtml = $ContentHtml;
      $this->SearchText = $SearchText;
      $this->SummaryHtml = $SummaryHtml;
      $this->ContentLanguage = $ContentLanguage;
      $this->FolderId = $FolderId;
      $this->GoLive = $GoLive;
      $this->EndDate = $EndDate;
      $this->MetaInfoXml = $MetaInfoXml;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
      return $this->Username;
    }

    /**
     * @param string $Username
     * @return AddContentWithUser
     */
    public function setUsername($Username)
    {
      $this->Username = $Username;
      return $this;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
      return $this->Password;
    }

    /**
     * @param string $Password
     * @return AddContentWithUser
     */
    public function setPassword($Password)
    {
      $this->Password = $Password;
      return $this;
    }

    /**
     * @return string
     */
    public function getDomain()
    {
      return $this->Domain;
    }

    /**
     * @param string $Domain
     * @return AddContentWithUser
     */
    public function setDomain($Domain)
    {
      $this->Domain = $Domain;
      return $this;
    }

    /**
     * @return string
     */
    public function getContentTitle()
    {
      return $this->ContentTitle;
    }

    /**
     * @param string $ContentTitle
     * @return AddContentWithUser
     */
    public function setContentTitle($ContentTitle)
    {
      $this->ContentTitle = $ContentTitle;
      return $this;
    }

    /**
     * @return string
     */
    public function getContentComment()
    {
      return $this->ContentComment;
    }

    /**
     * @param string $ContentComment
     * @return AddContentWithUser
     */
    public function setContentComment($ContentComment)
    {
      $this->ContentComment = $ContentComment;
      return $this;
    }

    /**
     * @return string
     */
    public function getContentHtml()
    {
      return $this->ContentHtml;
    }

    /**
     * @param string $ContentHtml
     * @return AddContentWithUser
     */
    public function setContentHtml($ContentHtml)
    {
      $this->ContentHtml = $ContentHtml;
      return $this;
    }

    /**
     * @return string
     */
    public function getSearchText()
    {
      return $this->SearchText;
    }

    /**
     * @param string $SearchText
     * @return AddContentWithUser
     */
    public function setSearchText($SearchText)
    {
      $this->SearchText = $SearchText;
      return $this;
    }

    /**
     * @return string
     */
    public function getSummaryHtml()
    {
      return $this->SummaryHtml;
    }

    /**
     * @param string $SummaryHtml
     * @return AddContentWithUser
     */
    public function setSummaryHtml($SummaryHtml)
    {
      $this->SummaryHtml = $SummaryHtml;
      return $this;
    }

    /**
     * @return string
     */
    public function getContentLanguage()
    {
      return $this->ContentLanguage;
    }

    /**
     * @param string $ContentLanguage
     * @return AddContentWithUser
     */
    public function setContentLanguage($ContentLanguage)
    {
      $this->ContentLanguage = $ContentLanguage;
      return $this;
    }

    /**
     * @return int
     */
    public function getFolderId()
    {
      return $this->FolderId;
    }

    /**
     * @param int $FolderId
     * @return AddContentWithUser
     */
    public function setFolderId($FolderId)
    {
      $this->FolderId = $FolderId;
      return $this;
    }

    /**
     * @return anyType
     */
    public function getGoLive()
    {
      return $this->GoLive;
    }

    /**
     * @param anyType $GoLive
     * @return AddContentWithUser
     */
    public function setGoLive($GoLive)
    {
      $this->GoLive = $GoLive;
      return $this;
    }

    /**
     * @return anyType
     */
    public function getEndDate()
    {
      return $this->EndDate;
    }

    /**
     * @param anyType $EndDate
     * @return AddContentWithUser
     */
    public function setEndDate($EndDate)
    {
      $this->EndDate = $EndDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getMetaInfoXml()
    {
      return $this->MetaInfoXml;
    }

    /**
     * @param string $MetaInfoXml
     * @return AddContentWithUser
     */
    public function setMetaInfoXml($MetaInfoXml)
    {
      $this->MetaInfoXml = $MetaInfoXml;
      return $this;
    }

}
