<?php

class TemplateSubType
{
    const __default = 'aDefault';
    const aDefault = 'Default';
    const Workspace = 'Workspace';
    const Photos = 'Photos';
    const Blog = 'Blog';
    const Wireframes = 'Wireframes';
    const Profile = 'Profile';
    const Calendar = 'Calendar';
    const Forum = 'Forum';
    const MasterLayout = 'MasterLayout';


}
