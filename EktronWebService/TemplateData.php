<?php

class TemplateData extends CmsDataOfTemplateData
{

    /**
     * @var string $TemplateName
     */
    protected $TemplateName = null;

    /**
     * @var string $FileName
     */
    protected $FileName = null;

    /**
     * @var TemplateType $Type
     */
    protected $Type = null;

    /**
     * @var TemplateSubType $SubType
     */
    protected $SubType = null;

    /**
     * @var string $Thumbnail
     */
    protected $Thumbnail = null;

    /**
     * @var string $Description
     */
    protected $Description = null;

    /**
     * @var int $MasterLayoutID
     */
    protected $MasterLayoutID = null;

    /**
     * @param int $Id
     * @param TemplateType $Type
     * @param TemplateSubType $SubType
     * @param int $MasterLayoutID
     */
    public function __construct($Id, $Type, $SubType, $MasterLayoutID)
    {
      parent::__construct($Id);
      $this->Type = $Type;
      $this->SubType = $SubType;
      $this->MasterLayoutID = $MasterLayoutID;
    }

    /**
     * @return string
     */
    public function getTemplateName()
    {
      return $this->TemplateName;
    }

    /**
     * @param string $TemplateName
     * @return TemplateData
     */
    public function setTemplateName($TemplateName)
    {
      $this->TemplateName = $TemplateName;
      return $this;
    }

    /**
     * @return string
     */
    public function getFileName()
    {
      return $this->FileName;
    }

    /**
     * @param string $FileName
     * @return TemplateData
     */
    public function setFileName($FileName)
    {
      $this->FileName = $FileName;
      return $this;
    }

    /**
     * @return TemplateType
     */
    public function getType()
    {
      return $this->Type;
    }

    /**
     * @param TemplateType $Type
     * @return TemplateData
     */
    public function setType($Type)
    {
      $this->Type = $Type;
      return $this;
    }

    /**
     * @return TemplateSubType
     */
    public function getSubType()
    {
      return $this->SubType;
    }

    /**
     * @param TemplateSubType $SubType
     * @return TemplateData
     */
    public function setSubType($SubType)
    {
      $this->SubType = $SubType;
      return $this;
    }

    /**
     * @return string
     */
    public function getThumbnail()
    {
      return $this->Thumbnail;
    }

    /**
     * @param string $Thumbnail
     * @return TemplateData
     */
    public function setThumbnail($Thumbnail)
    {
      $this->Thumbnail = $Thumbnail;
      return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
      return $this->Description;
    }

    /**
     * @param string $Description
     * @return TemplateData
     */
    public function setDescription($Description)
    {
      $this->Description = $Description;
      return $this;
    }

    /**
     * @return int
     */
    public function getMasterLayoutID()
    {
      return $this->MasterLayoutID;
    }

    /**
     * @param int $MasterLayoutID
     * @return TemplateData
     */
    public function setMasterLayoutID($MasterLayoutID)
    {
      $this->MasterLayoutID = $MasterLayoutID;
      return $this;
    }

}
