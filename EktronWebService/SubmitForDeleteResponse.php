<?php

class SubmitForDeleteResponse
{

    /**
     * @var boolean $SubmitForDeleteResult
     */
    protected $SubmitForDeleteResult = null;

    /**
     * @param boolean $SubmitForDeleteResult
     */
    public function __construct($SubmitForDeleteResult)
    {
      $this->SubmitForDeleteResult = $SubmitForDeleteResult;
    }

    /**
     * @return boolean
     */
    public function getSubmitForDeleteResult()
    {
      return $this->SubmitForDeleteResult;
    }

    /**
     * @param boolean $SubmitForDeleteResult
     * @return SubmitForDeleteResponse
     */
    public function setSubmitForDeleteResult($SubmitForDeleteResult)
    {
      $this->SubmitForDeleteResult = $SubmitForDeleteResult;
      return $this;
    }

}
