<?php

class GetChildContentWithPagingResponse
{

    /**
     * @var ArrayOfContentData $GetChildContentWithPagingResult
     */
    protected $GetChildContentWithPagingResult = null;

    /**
     * @param ArrayOfContentData $GetChildContentWithPagingResult
     */
    public function __construct($GetChildContentWithPagingResult)
    {
      $this->GetChildContentWithPagingResult = $GetChildContentWithPagingResult;
    }

    /**
     * @return ArrayOfContentData
     */
    public function getGetChildContentWithPagingResult()
    {
      return $this->GetChildContentWithPagingResult;
    }

    /**
     * @param ArrayOfContentData $GetChildContentWithPagingResult
     * @return GetChildContentWithPagingResponse
     */
    public function setGetChildContentWithPagingResult($GetChildContentWithPagingResult)
    {
      $this->GetChildContentWithPagingResult = $GetChildContentWithPagingResult;
      return $this;
    }

}
