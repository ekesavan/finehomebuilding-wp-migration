<?php

class MoveContentToFolder
{

    /**
     * @var string $ContentID
     */
    protected $ContentID = null;

    /**
     * @var string $ContentLanguage
     */
    protected $ContentLanguage = null;

    /**
     * @var int $FolderID
     */
    protected $FolderID = null;

    /**
     * @param string $ContentID
     * @param string $ContentLanguage
     * @param int $FolderID
     */
    public function __construct($ContentID, $ContentLanguage, $FolderID)
    {
      $this->ContentID = $ContentID;
      $this->ContentLanguage = $ContentLanguage;
      $this->FolderID = $FolderID;
    }

    /**
     * @return string
     */
    public function getContentID()
    {
      return $this->ContentID;
    }

    /**
     * @param string $ContentID
     * @return MoveContentToFolder
     */
    public function setContentID($ContentID)
    {
      $this->ContentID = $ContentID;
      return $this;
    }

    /**
     * @return string
     */
    public function getContentLanguage()
    {
      return $this->ContentLanguage;
    }

    /**
     * @param string $ContentLanguage
     * @return MoveContentToFolder
     */
    public function setContentLanguage($ContentLanguage)
    {
      $this->ContentLanguage = $ContentLanguage;
      return $this;
    }

    /**
     * @return int
     */
    public function getFolderID()
    {
      return $this->FolderID;
    }

    /**
     * @param int $FolderID
     * @return MoveContentToFolder
     */
    public function setFolderID($FolderID)
    {
      $this->FolderID = $FolderID;
      return $this;
    }

}
