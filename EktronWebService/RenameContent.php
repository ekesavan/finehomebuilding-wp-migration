<?php

class RenameContent
{

    /**
     * @var int $ContentID
     */
    protected $ContentID = null;

    /**
     * @var string $Title
     */
    protected $Title = null;

    /**
     * @param int $ContentID
     * @param string $Title
     */
    public function __construct($ContentID, $Title)
    {
      $this->ContentID = $ContentID;
      $this->Title = $Title;
    }

    /**
     * @return int
     */
    public function getContentID()
    {
      return $this->ContentID;
    }

    /**
     * @param int $ContentID
     * @return RenameContent
     */
    public function setContentID($ContentID)
    {
      $this->ContentID = $ContentID;
      return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
      return $this->Title;
    }

    /**
     * @param string $Title
     * @return RenameContent
     */
    public function setTitle($Title)
    {
      $this->Title = $Title;
      return $this;
    }

}
