<?php

class GetContentForEditing
{

    /**
     * @var int $ContentID
     */
    protected $ContentID = null;

    /**
     * @param int $ContentID
     */
    public function __construct($ContentID)
    {
      $this->ContentID = $ContentID;
    }

    /**
     * @return int
     */
    public function getContentID()
    {
      return $this->ContentID;
    }

    /**
     * @param int $ContentID
     * @return GetContentForEditing
     */
    public function setContentID($ContentID)
    {
      $this->ContentID = $ContentID;
      return $this;
    }

}
