<?php

class SubmitForDelete
{

    /**
     * @var int $ContentID
     */
    protected $ContentID = null;

    /**
     * @var int $FolderID
     */
    protected $FolderID = null;

    /**
     * @param int $ContentID
     * @param int $FolderID
     */
    public function __construct($ContentID, $FolderID)
    {
      $this->ContentID = $ContentID;
      $this->FolderID = $FolderID;
    }

    /**
     * @return int
     */
    public function getContentID()
    {
      return $this->ContentID;
    }

    /**
     * @param int $ContentID
     * @return SubmitForDelete
     */
    public function setContentID($ContentID)
    {
      $this->ContentID = $ContentID;
      return $this;
    }

    /**
     * @return int
     */
    public function getFolderID()
    {
      return $this->FolderID;
    }

    /**
     * @param int $FolderID
     * @return SubmitForDelete
     */
    public function setFolderID($FolderID)
    {
      $this->FolderID = $FolderID;
      return $this;
    }

}
