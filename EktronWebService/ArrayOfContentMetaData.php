<?php

class ArrayOfContentMetaData
{

    /**
     * @var ContentMetaData[] $ContentMetaData
     */
    protected $ContentMetaData = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ContentMetaData[]
     */
    public function getContentMetaData()
    {
      return $this->ContentMetaData;
    }

    /**
     * @param ContentMetaData[] $ContentMetaData
     * @return ArrayOfContentMetaData
     */
    public function setContentMetaData(array $ContentMetaData = null)
    {
      $this->ContentMetaData = $ContentMetaData;
      return $this;
    }

}
