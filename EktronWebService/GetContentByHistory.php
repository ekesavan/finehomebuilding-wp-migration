<?php

class GetContentByHistory
{

    /**
     * @var int $HistoryID
     */
    protected $HistoryID = null;

    /**
     * @param int $HistoryID
     */
    public function __construct($HistoryID)
    {
      $this->HistoryID = $HistoryID;
    }

    /**
     * @return int
     */
    public function getHistoryID()
    {
      return $this->HistoryID;
    }

    /**
     * @param int $HistoryID
     * @return GetContentByHistory
     */
    public function setHistoryID($HistoryID)
    {
      $this->HistoryID = $HistoryID;
      return $this;
    }

}
