<?php

class ArrayOfLanguageData
{

    /**
     * @var LanguageData[] $LanguageData
     */
    protected $LanguageData = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return LanguageData[]
     */
    public function getLanguageData()
    {
      return $this->LanguageData;
    }

    /**
     * @param LanguageData[] $LanguageData
     * @return ArrayOfLanguageData
     */
    public function setLanguageData(array $LanguageData = null)
    {
      $this->LanguageData = $LanguageData;
      return $this;
    }

}
