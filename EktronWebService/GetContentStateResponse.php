<?php

class GetContentStateResponse
{

    /**
     * @var ContentStateData $GetContentStateResult
     */
    protected $GetContentStateResult = null;

    /**
     * @param ContentStateData $GetContentStateResult
     */
    public function __construct($GetContentStateResult)
    {
      $this->GetContentStateResult = $GetContentStateResult;
    }

    /**
     * @return ContentStateData
     */
    public function getGetContentStateResult()
    {
      return $this->GetContentStateResult;
    }

    /**
     * @param ContentStateData $GetContentStateResult
     * @return GetContentStateResponse
     */
    public function setGetContentStateResult($GetContentStateResult)
    {
      $this->GetContentStateResult = $GetContentStateResult;
      return $this;
    }

}
