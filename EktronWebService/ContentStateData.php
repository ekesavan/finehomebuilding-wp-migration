<?php

class ContentStateData extends ContentData
{

    /**
     * @var string $CurrentTitle
     */
    protected $CurrentTitle = null;

    /**
     * @var string $CurrentDisplayEndDate
     */
    protected $CurrentDisplayEndDate = null;

    /**
     * @var string $CurrentEndDate
     */
    protected $CurrentEndDate = null;

    /**
     * @var string $CurrentEditorFirstName
     */
    protected $CurrentEditorFirstName = null;

    /**
     * @var string $CurrentEditorLastName
     */
    protected $CurrentEditorLastName = null;

    /**
     * @var string $PreviousStatus
     */
    protected $PreviousStatus = null;

    /**
     * @var int $CurrentFolderId
     */
    protected $CurrentFolderId = null;

    /**
     * @var string $CurrentGoLive
     */
    protected $CurrentGoLive = null;

    /**
     * @var string $CurrentDisplayGoLive
     */
    protected $CurrentDisplayGoLive = null;

    /**
     * @var string $CurrentLastEditDate
     */
    protected $CurrentLastEditDate = null;

    /**
     * @var string $CurrentDisplayLastEditDate
     */
    protected $CurrentDisplayLastEditDate = null;

    /**
     * @var boolean $CurrentMetaComplete
     */
    protected $CurrentMetaComplete = null;

    /**
     * @var int $CurrentUserId
     */
    protected $CurrentUserId = null;

    /**
     * @var string $CurrentComment
     */
    protected $CurrentComment = null;

    /**
     * @var boolean $IsFolderPrivate
     */
    protected $IsFolderPrivate = null;

    /**
     * @param int $Id
     * @param int $LanguageId
     * @param boolean $IsPrivate
     * @param int $Type
     * @param CMSContentSubtype $SubType
     * @param int $ExternalTypeId
     * @param \DateTime $DateModified
     * @param \DateTime $DateCreated
     * @param int $UserId
     * @param int $FolderId
     * @param boolean $IsPermissionsInherited
     * @param int $PermissionInheritedFrom
     * @param boolean $IsPublished
     * @param boolean $IsSearchable
     * @param int $XmlInheritedFrom
     * @param \DateTime $ExpireDate
     * @param CMSEndDateAction $EndDateActionType
     * @param int $ContType
     * @param int $Updates
     * @param int $HistoryId
     * @param int $FlagDefId
     * @param int $CurrentFolderId
     * @param boolean $CurrentMetaComplete
     * @param int $CurrentUserId
     * @param boolean $IsFolderPrivate
     */
    public function __construct($Id, $LanguageId, $IsPrivate, $Type, $SubType, $ExternalTypeId, \DateTime $DateModified, \DateTime $DateCreated, $UserId, $FolderId, $IsPermissionsInherited, $PermissionInheritedFrom, $IsPublished, $IsSearchable, $XmlInheritedFrom, \DateTime $ExpireDate, $EndDateActionType, $ContType, $Updates, $HistoryId, $FlagDefId, $CurrentFolderId, $CurrentMetaComplete, $CurrentUserId, $IsFolderPrivate)
    {
      parent::__construct($Id, $LanguageId, $IsPrivate, $Type, $SubType, $ExternalTypeId, $DateModified, $DateCreated, $UserId, $FolderId, $IsPermissionsInherited, $PermissionInheritedFrom, $IsPublished, $IsSearchable, $XmlInheritedFrom, $ExpireDate, $EndDateActionType, $ContType, $Updates, $HistoryId, $FlagDefId);
      $this->CurrentFolderId = $CurrentFolderId;
      $this->CurrentMetaComplete = $CurrentMetaComplete;
      $this->CurrentUserId = $CurrentUserId;
      $this->IsFolderPrivate = $IsFolderPrivate;
    }

    /**
     * @return string
     */
    public function getCurrentTitle()
    {
      return $this->CurrentTitle;
    }

    /**
     * @param string $CurrentTitle
     * @return ContentStateData
     */
    public function setCurrentTitle($CurrentTitle)
    {
      $this->CurrentTitle = $CurrentTitle;
      return $this;
    }

    /**
     * @return string
     */
    public function getCurrentDisplayEndDate()
    {
      return $this->CurrentDisplayEndDate;
    }

    /**
     * @param string $CurrentDisplayEndDate
     * @return ContentStateData
     */
    public function setCurrentDisplayEndDate($CurrentDisplayEndDate)
    {
      $this->CurrentDisplayEndDate = $CurrentDisplayEndDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getCurrentEndDate()
    {
      return $this->CurrentEndDate;
    }

    /**
     * @param string $CurrentEndDate
     * @return ContentStateData
     */
    public function setCurrentEndDate($CurrentEndDate)
    {
      $this->CurrentEndDate = $CurrentEndDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getCurrentEditorFirstName()
    {
      return $this->CurrentEditorFirstName;
    }

    /**
     * @param string $CurrentEditorFirstName
     * @return ContentStateData
     */
    public function setCurrentEditorFirstName($CurrentEditorFirstName)
    {
      $this->CurrentEditorFirstName = $CurrentEditorFirstName;
      return $this;
    }

    /**
     * @return string
     */
    public function getCurrentEditorLastName()
    {
      return $this->CurrentEditorLastName;
    }

    /**
     * @param string $CurrentEditorLastName
     * @return ContentStateData
     */
    public function setCurrentEditorLastName($CurrentEditorLastName)
    {
      $this->CurrentEditorLastName = $CurrentEditorLastName;
      return $this;
    }

    /**
     * @return string
     */
    public function getPreviousStatus()
    {
      return $this->PreviousStatus;
    }

    /**
     * @param string $PreviousStatus
     * @return ContentStateData
     */
    public function setPreviousStatus($PreviousStatus)
    {
      $this->PreviousStatus = $PreviousStatus;
      return $this;
    }

    /**
     * @return int
     */
    public function getCurrentFolderId()
    {
      return $this->CurrentFolderId;
    }

    /**
     * @param int $CurrentFolderId
     * @return ContentStateData
     */
    public function setCurrentFolderId($CurrentFolderId)
    {
      $this->CurrentFolderId = $CurrentFolderId;
      return $this;
    }

    /**
     * @return string
     */
    public function getCurrentGoLive()
    {
      return $this->CurrentGoLive;
    }

    /**
     * @param string $CurrentGoLive
     * @return ContentStateData
     */
    public function setCurrentGoLive($CurrentGoLive)
    {
      $this->CurrentGoLive = $CurrentGoLive;
      return $this;
    }

    /**
     * @return string
     */
    public function getCurrentDisplayGoLive()
    {
      return $this->CurrentDisplayGoLive;
    }

    /**
     * @param string $CurrentDisplayGoLive
     * @return ContentStateData
     */
    public function setCurrentDisplayGoLive($CurrentDisplayGoLive)
    {
      $this->CurrentDisplayGoLive = $CurrentDisplayGoLive;
      return $this;
    }

    /**
     * @return string
     */
    public function getCurrentLastEditDate()
    {
      return $this->CurrentLastEditDate;
    }

    /**
     * @param string $CurrentLastEditDate
     * @return ContentStateData
     */
    public function setCurrentLastEditDate($CurrentLastEditDate)
    {
      $this->CurrentLastEditDate = $CurrentLastEditDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getCurrentDisplayLastEditDate()
    {
      return $this->CurrentDisplayLastEditDate;
    }

    /**
     * @param string $CurrentDisplayLastEditDate
     * @return ContentStateData
     */
    public function setCurrentDisplayLastEditDate($CurrentDisplayLastEditDate)
    {
      $this->CurrentDisplayLastEditDate = $CurrentDisplayLastEditDate;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getCurrentMetaComplete()
    {
      return $this->CurrentMetaComplete;
    }

    /**
     * @param boolean $CurrentMetaComplete
     * @return ContentStateData
     */
    public function setCurrentMetaComplete($CurrentMetaComplete)
    {
      $this->CurrentMetaComplete = $CurrentMetaComplete;
      return $this;
    }

    /**
     * @return int
     */
    public function getCurrentUserId()
    {
      return $this->CurrentUserId;
    }

    /**
     * @param int $CurrentUserId
     * @return ContentStateData
     */
    public function setCurrentUserId($CurrentUserId)
    {
      $this->CurrentUserId = $CurrentUserId;
      return $this;
    }

    /**
     * @return string
     */
    public function getCurrentComment()
    {
      return $this->CurrentComment;
    }

    /**
     * @param string $CurrentComment
     * @return ContentStateData
     */
    public function setCurrentComment($CurrentComment)
    {
      $this->CurrentComment = $CurrentComment;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIsFolderPrivate()
    {
      return $this->IsFolderPrivate;
    }

    /**
     * @param boolean $IsFolderPrivate
     * @return ContentStateData
     */
    public function setIsFolderPrivate($IsFolderPrivate)
    {
      $this->IsFolderPrivate = $IsFolderPrivate;
      return $this;
    }

}
