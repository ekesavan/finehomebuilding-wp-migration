<?php

abstract class CmsLocalizedDataOfContentBaseData extends CmsDataOfContentBaseData
{

    /**
     * @var int $LanguageId
     */
    protected $LanguageId = null;

    /**
     * @param int $Id
     * @param int $LanguageId
     */
    public function __construct($Id, $LanguageId)
    {
      parent::__construct($Id);
      $this->LanguageId = $LanguageId;
    }

    /**
     * @return int
     */
    public function getLanguageId()
    {
      return $this->LanguageId;
    }

    /**
     * @param int $LanguageId
     * @return CmsLocalizedDataOfContentBaseData
     */
    public function setLanguageId($LanguageId)
    {
      $this->LanguageId = $LanguageId;
      return $this;
    }

}
