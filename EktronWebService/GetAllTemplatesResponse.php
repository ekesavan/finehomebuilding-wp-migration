<?php

class GetAllTemplatesResponse
{

    /**
     * @var ArrayOfTemplateData $GetAllTemplatesResult
     */
    protected $GetAllTemplatesResult = null;

    /**
     * @param ArrayOfTemplateData $GetAllTemplatesResult
     */
    public function __construct($GetAllTemplatesResult)
    {
      $this->GetAllTemplatesResult = $GetAllTemplatesResult;
    }

    /**
     * @return ArrayOfTemplateData
     */
    public function getGetAllTemplatesResult()
    {
      return $this->GetAllTemplatesResult;
    }

    /**
     * @param ArrayOfTemplateData $GetAllTemplatesResult
     * @return GetAllTemplatesResponse
     */
    public function setGetAllTemplatesResult($GetAllTemplatesResult)
    {
      $this->GetAllTemplatesResult = $GetAllTemplatesResult;
      return $this;
    }

}
