<?php

class ContentData extends ContentBaseData
{

    /**
     * @var string $EditorFirstName
     */
    protected $EditorFirstName = null;

    /**
     * @var string $EditorLastName
     */
    protected $EditorLastName = null;

    /**
     * @var string $Comment
     */
    protected $Comment = null;

    /**
     * @var \DateTime $DateModified
     */
    protected $DateModified = null;

    /**
     * @var string $DisplayLastEditDate
     */
    protected $DisplayLastEditDate = null;

    /**
     * @var \DateTime $DateCreated
     */
    protected $DateCreated = null;

    /**
     * @var string $DisplayDateCreated
     */
    protected $DisplayDateCreated = null;

    /**
     * @var int $UserId
     */
    protected $UserId = null;

    /**
     * @var int $FolderId
     */
    protected $FolderId = null;

    /**
     * @var boolean $IsPermissionsInherited
     */
    protected $IsPermissionsInherited = null;

    /**
     * @var int $PermissionInheritedFrom
     */
    protected $PermissionInheritedFrom = null;

    /**
     * @var string $Status
     */
    protected $Status = null;

    /**
     * @var \DateTime $GoLiveDate
     */
    protected $GoLiveDate = null;

    /**
     * @var string $DisplayGoLive
     */
    protected $DisplayGoLive = null;

    /**
     * @var boolean $IsPublished
     */
    protected $IsPublished = null;

    /**
     * @var boolean $IsSearchable
     */
    protected $IsSearchable = null;

    /**
     * @var int $XmlInheritedFrom
     */
    protected $XmlInheritedFrom = null;

    /**
     * @var ArrayOfContentMetaData $MetaData
     */
    protected $MetaData = null;

    /**
     * @var string $DisplayEndDate
     */
    protected $DisplayEndDate = null;

    /**
     * @var \DateTime $ExpireDate
     */
    protected $ExpireDate = null;

    /**
     * @var CMSEndDateAction $EndDateActionType
     */
    protected $EndDateActionType = null;

    /**
     * @var XmlConfigData $XmlConfiguration
     */
    protected $XmlConfiguration = null;

    /**
     * @var string $StyleSheet
     */
    protected $StyleSheet = null;

    /**
     * @var string $LanguageDescription
     */
    protected $LanguageDescription = null;

    /**
     * @var string $Text
     */
    protected $Text = null;

    /**
     * @var string $Path
     */
    protected $Path = null;

    /**
     * @var string $ContentPath
     */
    protected $ContentPath = null;

    /**
     * @var int $ContType
     */
    protected $ContType = null;

    /**
     * @var int $Updates
     */
    protected $Updates = null;

    /**
     * @var string $FolderName
     */
    protected $FolderName = null;

    /**
     * @var string $MediaText
     */
    protected $MediaText = null;

    /**
     * @var int $HistoryId
     */
    protected $HistoryId = null;

    /**
     * @var string $HyperLink
     */
    protected $HyperLink = null;

    /**
     * @var TemplateData $TemplateConfiguration
     */
    protected $TemplateConfiguration = null;

    /**
     * @var int $FlagDefId
     */
    protected $FlagDefId = null;

    /**
     * @param int $Id
     * @param int $LanguageId
     * @param boolean $IsPrivate
     * @param int $Type
     * @param CMSContentSubtype $SubType
     * @param int $ExternalTypeId
     * @param \DateTime $DateModified
     * @param \DateTime $DateCreated
     * @param int $UserId
     * @param int $FolderId
     * @param boolean $IsPermissionsInherited
     * @param int $PermissionInheritedFrom
     * @param boolean $IsPublished
     * @param boolean $IsSearchable
     * @param int $XmlInheritedFrom
     * @param \DateTime $ExpireDate
     * @param CMSEndDateAction $EndDateActionType
     * @param int $ContType
     * @param int $Updates
     * @param int $HistoryId
     * @param int $FlagDefId
     */
    public function __construct($Id, $LanguageId, $IsPrivate, $Type, $SubType, $ExternalTypeId, \DateTime $DateModified, \DateTime $DateCreated, $UserId, $FolderId, $IsPermissionsInherited, $PermissionInheritedFrom, $IsPublished, $IsSearchable, $XmlInheritedFrom, \DateTime $ExpireDate, $EndDateActionType, $ContType, $Updates, $HistoryId, $FlagDefId)
    {
      parent::__construct($Id, $LanguageId, $IsPrivate, $Type, $SubType, $ExternalTypeId);
      $this->DateModified = $DateModified->format(\DateTime::ATOM);
      $this->DateCreated = $DateCreated->format(\DateTime::ATOM);
      $this->UserId = $UserId;
      $this->FolderId = $FolderId;
      $this->IsPermissionsInherited = $IsPermissionsInherited;
      $this->PermissionInheritedFrom = $PermissionInheritedFrom;
      $this->IsPublished = $IsPublished;
      $this->IsSearchable = $IsSearchable;
      $this->XmlInheritedFrom = $XmlInheritedFrom;
      $this->ExpireDate = $ExpireDate->format(\DateTime::ATOM);
      $this->EndDateActionType = $EndDateActionType;
      $this->ContType = $ContType;
      $this->Updates = $Updates;
      $this->HistoryId = $HistoryId;
      $this->FlagDefId = $FlagDefId;
    }

    /**
     * @return string
     */
    public function getEditorFirstName()
    {
      return $this->EditorFirstName;
    }

    /**
     * @param string $EditorFirstName
     * @return ContentData
     */
    public function setEditorFirstName($EditorFirstName)
    {
      $this->EditorFirstName = $EditorFirstName;
      return $this;
    }

    /**
     * @return string
     */
    public function getEditorLastName()
    {
      return $this->EditorLastName;
    }

    /**
     * @param string $EditorLastName
     * @return ContentData
     */
    public function setEditorLastName($EditorLastName)
    {
      $this->EditorLastName = $EditorLastName;
      return $this;
    }

    /**
     * @return string
     */
    public function getComment()
    {
      return $this->Comment;
    }

    /**
     * @param string $Comment
     * @return ContentData
     */
    public function setComment($Comment)
    {
      $this->Comment = $Comment;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateModified()
    {
      if ($this->DateModified == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateModified);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateModified
     * @return ContentData
     */
    public function setDateModified(\DateTime $DateModified)
    {
      $this->DateModified = $DateModified->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return string
     */
    public function getDisplayLastEditDate()
    {
      return $this->DisplayLastEditDate;
    }

    /**
     * @param string $DisplayLastEditDate
     * @return ContentData
     */
    public function setDisplayLastEditDate($DisplayLastEditDate)
    {
      $this->DisplayLastEditDate = $DisplayLastEditDate;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateCreated()
    {
      if ($this->DateCreated == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DateCreated);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DateCreated
     * @return ContentData
     */
    public function setDateCreated(\DateTime $DateCreated)
    {
      $this->DateCreated = $DateCreated->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return string
     */
    public function getDisplayDateCreated()
    {
      return $this->DisplayDateCreated;
    }

    /**
     * @param string $DisplayDateCreated
     * @return ContentData
     */
    public function setDisplayDateCreated($DisplayDateCreated)
    {
      $this->DisplayDateCreated = $DisplayDateCreated;
      return $this;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
      return $this->UserId;
    }

    /**
     * @param int $UserId
     * @return ContentData
     */
    public function setUserId($UserId)
    {
      $this->UserId = $UserId;
      return $this;
    }

    /**
     * @return int
     */
    public function getFolderId()
    {
      return $this->FolderId;
    }

    /**
     * @param int $FolderId
     * @return ContentData
     */
    public function setFolderId($FolderId)
    {
      $this->FolderId = $FolderId;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIsPermissionsInherited()
    {
      return $this->IsPermissionsInherited;
    }

    /**
     * @param boolean $IsPermissionsInherited
     * @return ContentData
     */
    public function setIsPermissionsInherited($IsPermissionsInherited)
    {
      $this->IsPermissionsInherited = $IsPermissionsInherited;
      return $this;
    }

    /**
     * @return int
     */
    public function getPermissionInheritedFrom()
    {
      return $this->PermissionInheritedFrom;
    }

    /**
     * @param int $PermissionInheritedFrom
     * @return ContentData
     */
    public function setPermissionInheritedFrom($PermissionInheritedFrom)
    {
      $this->PermissionInheritedFrom = $PermissionInheritedFrom;
      return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
      return $this->Status;
    }

    /**
     * @param string $Status
     * @return ContentData
     */
    public function setStatus($Status)
    {
      $this->Status = $Status;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getGoLiveDate()
    {
      if ($this->GoLiveDate == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->GoLiveDate);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $GoLiveDate
     * @return ContentData
     */
    public function setGoLiveDate(\DateTime $GoLiveDate = null)
    {
      if ($GoLiveDate == null) {
       $this->GoLiveDate = null;
      } else {
        $this->GoLiveDate = $GoLiveDate->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return string
     */
    public function getDisplayGoLive()
    {
      return $this->DisplayGoLive;
    }

    /**
     * @param string $DisplayGoLive
     * @return ContentData
     */
    public function setDisplayGoLive($DisplayGoLive)
    {
      $this->DisplayGoLive = $DisplayGoLive;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIsPublished()
    {
      return $this->IsPublished;
    }

    /**
     * @param boolean $IsPublished
     * @return ContentData
     */
    public function setIsPublished($IsPublished)
    {
      $this->IsPublished = $IsPublished;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIsSearchable()
    {
      return $this->IsSearchable;
    }

    /**
     * @param boolean $IsSearchable
     * @return ContentData
     */
    public function setIsSearchable($IsSearchable)
    {
      $this->IsSearchable = $IsSearchable;
      return $this;
    }

    /**
     * @return int
     */
    public function getXmlInheritedFrom()
    {
      return $this->XmlInheritedFrom;
    }

    /**
     * @param int $XmlInheritedFrom
     * @return ContentData
     */
    public function setXmlInheritedFrom($XmlInheritedFrom)
    {
      $this->XmlInheritedFrom = $XmlInheritedFrom;
      return $this;
    }

    /**
     * @return ArrayOfContentMetaData
     */
    public function getMetaData()
    {
      return $this->MetaData;
    }

    /**
     * @param ArrayOfContentMetaData $MetaData
     * @return ContentData
     */
    public function setMetaData($MetaData)
    {
      $this->MetaData = $MetaData;
      return $this;
    }

    /**
     * @return string
     */
    public function getDisplayEndDate()
    {
      return $this->DisplayEndDate;
    }

    /**
     * @param string $DisplayEndDate
     * @return ContentData
     */
    public function setDisplayEndDate($DisplayEndDate)
    {
      $this->DisplayEndDate = $DisplayEndDate;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getExpireDate()
    {
      if ($this->ExpireDate == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->ExpireDate);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $ExpireDate
     * @return ContentData
     */
    public function setExpireDate(\DateTime $ExpireDate)
    {
      $this->ExpireDate = $ExpireDate->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return CMSEndDateAction
     */
    public function getEndDateActionType()
    {
      return $this->EndDateActionType;
    }

    /**
     * @param CMSEndDateAction $EndDateActionType
     * @return ContentData
     */
    public function setEndDateActionType($EndDateActionType)
    {
      $this->EndDateActionType = $EndDateActionType;
      return $this;
    }

    /**
     * @return XmlConfigData
     */
    public function getXmlConfiguration()
    {
      return $this->XmlConfiguration;
    }

    /**
     * @param XmlConfigData $XmlConfiguration
     * @return ContentData
     */
    public function setXmlConfiguration($XmlConfiguration)
    {
      $this->XmlConfiguration = $XmlConfiguration;
      return $this;
    }

    /**
     * @return string
     */
    public function getStyleSheet()
    {
      return $this->StyleSheet;
    }

    /**
     * @param string $StyleSheet
     * @return ContentData
     */
    public function setStyleSheet($StyleSheet)
    {
      $this->StyleSheet = $StyleSheet;
      return $this;
    }

    /**
     * @return string
     */
    public function getLanguageDescription()
    {
      return $this->LanguageDescription;
    }

    /**
     * @param string $LanguageDescription
     * @return ContentData
     */
    public function setLanguageDescription($LanguageDescription)
    {
      $this->LanguageDescription = $LanguageDescription;
      return $this;
    }

    /**
     * @return string
     */
    public function getText()
    {
      return $this->Text;
    }

    /**
     * @param string $Text
     * @return ContentData
     */
    public function setText($Text)
    {
      $this->Text = $Text;
      return $this;
    }

    /**
     * @return string
     */
    public function getPath()
    {
      return $this->Path;
    }

    /**
     * @param string $Path
     * @return ContentData
     */
    public function setPath($Path)
    {
      $this->Path = $Path;
      return $this;
    }

    /**
     * @return string
     */
    public function getContentPath()
    {
      return $this->ContentPath;
    }

    /**
     * @param string $ContentPath
     * @return ContentData
     */
    public function setContentPath($ContentPath)
    {
      $this->ContentPath = $ContentPath;
      return $this;
    }

    /**
     * @return int
     */
    public function getContType()
    {
      return $this->ContType;
    }

    /**
     * @param int $ContType
     * @return ContentData
     */
    public function setContType($ContType)
    {
      $this->ContType = $ContType;
      return $this;
    }

    /**
     * @return int
     */
    public function getUpdates()
    {
      return $this->Updates;
    }

    /**
     * @param int $Updates
     * @return ContentData
     */
    public function setUpdates($Updates)
    {
      $this->Updates = $Updates;
      return $this;
    }

    /**
     * @return string
     */
    public function getFolderName()
    {
      return $this->FolderName;
    }

    /**
     * @param string $FolderName
     * @return ContentData
     */
    public function setFolderName($FolderName)
    {
      $this->FolderName = $FolderName;
      return $this;
    }

    /**
     * @return string
     */
    public function getMediaText()
    {
      return $this->MediaText;
    }

    /**
     * @param string $MediaText
     * @return ContentData
     */
    public function setMediaText($MediaText)
    {
      $this->MediaText = $MediaText;
      return $this;
    }

    /**
     * @return int
     */
    public function getHistoryId()
    {
      return $this->HistoryId;
    }

    /**
     * @param int $HistoryId
     * @return ContentData
     */
    public function setHistoryId($HistoryId)
    {
      $this->HistoryId = $HistoryId;
      return $this;
    }

    /**
     * @return string
     */
    public function getHyperLink()
    {
      return $this->HyperLink;
    }

    /**
     * @param string $HyperLink
     * @return ContentData
     */
    public function setHyperLink($HyperLink)
    {
      $this->HyperLink = $HyperLink;
      return $this;
    }

    /**
     * @return TemplateData
     */
    public function getTemplateConfiguration()
    {
      return $this->TemplateConfiguration;
    }

    /**
     * @param TemplateData $TemplateConfiguration
     * @return ContentData
     */
    public function setTemplateConfiguration($TemplateConfiguration)
    {
      $this->TemplateConfiguration = $TemplateConfiguration;
      return $this;
    }

    /**
     * @return int
     */
    public function getFlagDefId()
    {
      return $this->FlagDefId;
    }

    /**
     * @param int $FlagDefId
     * @return ContentData
     */
    public function setFlagDefId($FlagDefId)
    {
      $this->FlagDefId = $FlagDefId;
      return $this;
    }

}
