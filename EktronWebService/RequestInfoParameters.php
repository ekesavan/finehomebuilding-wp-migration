<?php

class RequestInfoParameters
{

    /**
     * @var int $ContentLanguage
     */
    protected $ContentLanguage = null;

    /**
     * @param int $ContentLanguage
     */
    public function __construct($ContentLanguage)
    {
      $this->ContentLanguage = $ContentLanguage;
    }

    /**
     * @return int
     */
    public function getContentLanguage()
    {
      return $this->ContentLanguage;
    }

    /**
     * @param int $ContentLanguage
     * @return RequestInfoParameters
     */
    public function setContentLanguage($ContentLanguage)
    {
      $this->ContentLanguage = $ContentLanguage;
      return $this;
    }

}
