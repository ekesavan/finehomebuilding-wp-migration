<?php

class AddContentWithUserResponse
{

    /**
     * @var int $AddContentWithUserResult
     */
    protected $AddContentWithUserResult = null;

    /**
     * @param int $AddContentWithUserResult
     */
    public function __construct($AddContentWithUserResult)
    {
      $this->AddContentWithUserResult = $AddContentWithUserResult;
    }

    /**
     * @return int
     */
    public function getAddContentWithUserResult()
    {
      return $this->AddContentWithUserResult;
    }

    /**
     * @param int $AddContentWithUserResult
     * @return AddContentWithUserResponse
     */
    public function setAddContentWithUserResult($AddContentWithUserResult)
    {
      $this->AddContentWithUserResult = $AddContentWithUserResult;
      return $this;
    }

}
