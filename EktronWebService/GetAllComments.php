<?php

class GetAllComments
{

    /**
     * @var int $KeyID
     */
    protected $KeyID = null;

    /**
     * @var int $CommentID
     */
    protected $CommentID = null;

    /**
     * @var int $RefID
     */
    protected $RefID = null;

    /**
     * @var string $RefType
     */
    protected $RefType = null;

    /**
     * @var int $UserID
     */
    protected $UserID = null;

    /**
     * @var string $OrderBy
     */
    protected $OrderBy = null;

    /**
     * @param int $KeyID
     * @param int $CommentID
     * @param int $RefID
     * @param string $RefType
     * @param int $UserID
     * @param string $OrderBy
     */
    public function __construct($KeyID, $CommentID, $RefID, $RefType, $UserID, $OrderBy)
    {
      $this->KeyID = $KeyID;
      $this->CommentID = $CommentID;
      $this->RefID = $RefID;
      $this->RefType = $RefType;
      $this->UserID = $UserID;
      $this->OrderBy = $OrderBy;
    }

    /**
     * @return int
     */
    public function getKeyID()
    {
      return $this->KeyID;
    }

    /**
     * @param int $KeyID
     * @return GetAllComments
     */
    public function setKeyID($KeyID)
    {
      $this->KeyID = $KeyID;
      return $this;
    }

    /**
     * @return int
     */
    public function getCommentID()
    {
      return $this->CommentID;
    }

    /**
     * @param int $CommentID
     * @return GetAllComments
     */
    public function setCommentID($CommentID)
    {
      $this->CommentID = $CommentID;
      return $this;
    }

    /**
     * @return int
     */
    public function getRefID()
    {
      return $this->RefID;
    }

    /**
     * @param int $RefID
     * @return GetAllComments
     */
    public function setRefID($RefID)
    {
      $this->RefID = $RefID;
      return $this;
    }

    /**
     * @return string
     */
    public function getRefType()
    {
      return $this->RefType;
    }

    /**
     * @param string $RefType
     * @return GetAllComments
     */
    public function setRefType($RefType)
    {
      $this->RefType = $RefType;
      return $this;
    }

    /**
     * @return int
     */
    public function getUserID()
    {
      return $this->UserID;
    }

    /**
     * @param int $UserID
     * @return GetAllComments
     */
    public function setUserID($UserID)
    {
      $this->UserID = $UserID;
      return $this;
    }

    /**
     * @return string
     */
    public function getOrderBy()
    {
      return $this->OrderBy;
    }

    /**
     * @param string $OrderBy
     * @return GetAllComments
     */
    public function setOrderBy($OrderBy)
    {
      $this->OrderBy = $OrderBy;
      return $this;
    }

}
