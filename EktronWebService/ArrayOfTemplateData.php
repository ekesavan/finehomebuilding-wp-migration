<?php

class ArrayOfTemplateData
{

    /**
     * @var TemplateData[] $TemplateData
     */
    protected $TemplateData = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return TemplateData[]
     */
    public function getTemplateData()
    {
      return $this->TemplateData;
    }

    /**
     * @param TemplateData[] $TemplateData
     * @return ArrayOfTemplateData
     */
    public function setTemplateData(array $TemplateData = null)
    {
      $this->TemplateData = $TemplateData;
      return $this;
    }

}
