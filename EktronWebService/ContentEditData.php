<?php

class ContentEditData extends ContentData
{

    /**
     * @var int $NoOfSaves
     */
    protected $NoOfSaves = null;

    /**
     * @var int $MaxContentSize
     */
    protected $MaxContentSize = null;

    /**
     * @var int $MaxSummarySize
     */
    protected $MaxSummarySize = null;

    /**
     * @var string $CurrentStatus
     */
    protected $CurrentStatus = null;

    /**
     * @var boolean $LockedContentLink
     */
    protected $LockedContentLink = null;

    /**
     * @var boolean $FileChanged
     */
    protected $FileChanged = null;

    /**
     * @param int $Id
     * @param int $LanguageId
     * @param boolean $IsPrivate
     * @param int $Type
     * @param CMSContentSubtype $SubType
     * @param int $ExternalTypeId
     * @param \DateTime $DateModified
     * @param \DateTime $DateCreated
     * @param int $UserId
     * @param int $FolderId
     * @param boolean $IsPermissionsInherited
     * @param int $PermissionInheritedFrom
     * @param boolean $IsPublished
     * @param boolean $IsSearchable
     * @param int $XmlInheritedFrom
     * @param \DateTime $ExpireDate
     * @param CMSEndDateAction $EndDateActionType
     * @param int $ContType
     * @param int $Updates
     * @param int $HistoryId
     * @param int $FlagDefId
     * @param int $NoOfSaves
     * @param int $MaxContentSize
     * @param int $MaxSummarySize
     * @param boolean $LockedContentLink
     * @param boolean $FileChanged
     */
    public function __construct($Id, $LanguageId, $IsPrivate, $Type, $SubType, $ExternalTypeId, \DateTime $DateModified, \DateTime $DateCreated, $UserId, $FolderId, $IsPermissionsInherited, $PermissionInheritedFrom, $IsPublished, $IsSearchable, $XmlInheritedFrom, \DateTime $ExpireDate, $EndDateActionType, $ContType, $Updates, $HistoryId, $FlagDefId, $NoOfSaves, $MaxContentSize, $MaxSummarySize, $LockedContentLink, $FileChanged)
    {
      parent::__construct($Id, $LanguageId, $IsPrivate, $Type, $SubType, $ExternalTypeId, $DateModified, $DateCreated, $UserId, $FolderId, $IsPermissionsInherited, $PermissionInheritedFrom, $IsPublished, $IsSearchable, $XmlInheritedFrom, $ExpireDate, $EndDateActionType, $ContType, $Updates, $HistoryId, $FlagDefId);
      $this->NoOfSaves = $NoOfSaves;
      $this->MaxContentSize = $MaxContentSize;
      $this->MaxSummarySize = $MaxSummarySize;
      $this->LockedContentLink = $LockedContentLink;
      $this->FileChanged = $FileChanged;
    }

    /**
     * @return int
     */
    public function getNoOfSaves()
    {
      return $this->NoOfSaves;
    }

    /**
     * @param int $NoOfSaves
     * @return ContentEditData
     */
    public function setNoOfSaves($NoOfSaves)
    {
      $this->NoOfSaves = $NoOfSaves;
      return $this;
    }

    /**
     * @return int
     */
    public function getMaxContentSize()
    {
      return $this->MaxContentSize;
    }

    /**
     * @param int $MaxContentSize
     * @return ContentEditData
     */
    public function setMaxContentSize($MaxContentSize)
    {
      $this->MaxContentSize = $MaxContentSize;
      return $this;
    }

    /**
     * @return int
     */
    public function getMaxSummarySize()
    {
      return $this->MaxSummarySize;
    }

    /**
     * @param int $MaxSummarySize
     * @return ContentEditData
     */
    public function setMaxSummarySize($MaxSummarySize)
    {
      $this->MaxSummarySize = $MaxSummarySize;
      return $this;
    }

    /**
     * @return string
     */
    public function getCurrentStatus()
    {
      return $this->CurrentStatus;
    }

    /**
     * @param string $CurrentStatus
     * @return ContentEditData
     */
    public function setCurrentStatus($CurrentStatus)
    {
      $this->CurrentStatus = $CurrentStatus;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getLockedContentLink()
    {
      return $this->LockedContentLink;
    }

    /**
     * @param boolean $LockedContentLink
     * @return ContentEditData
     */
    public function setLockedContentLink($LockedContentLink)
    {
      $this->LockedContentLink = $LockedContentLink;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getFileChanged()
    {
      return $this->FileChanged;
    }

    /**
     * @param boolean $FileChanged
     * @return ContentEditData
     */
    public function setFileChanged($FileChanged)
    {
      $this->FileChanged = $FileChanged;
      return $this;
    }

}
