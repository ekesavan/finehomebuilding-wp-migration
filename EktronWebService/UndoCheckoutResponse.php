<?php

class UndoCheckoutResponse
{

    /**
     * @var boolean $UndoCheckoutResult
     */
    protected $UndoCheckoutResult = null;

    /**
     * @param boolean $UndoCheckoutResult
     */
    public function __construct($UndoCheckoutResult)
    {
      $this->UndoCheckoutResult = $UndoCheckoutResult;
    }

    /**
     * @return boolean
     */
    public function getUndoCheckoutResult()
    {
      return $this->UndoCheckoutResult;
    }

    /**
     * @param boolean $UndoCheckoutResult
     * @return UndoCheckoutResponse
     */
    public function setUndoCheckoutResult($UndoCheckoutResult)
    {
      $this->UndoCheckoutResult = $UndoCheckoutResult;
      return $this;
    }

}
