<?php

class ArrayOfCommentData
{

    /**
     * @var CommentData[] $CommentData
     */
    protected $CommentData = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return CommentData[]
     */
    public function getCommentData()
    {
      return $this->CommentData;
    }

    /**
     * @param CommentData[] $CommentData
     * @return ArrayOfCommentData
     */
    public function setCommentData(array $CommentData = null)
    {
      $this->CommentData = $CommentData;
      return $this;
    }

}
