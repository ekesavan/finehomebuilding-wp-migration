<?php

class UpdateContentMetaDataResponse
{

    /**
     * @var boolean $UpdateContentMetaDataResult
     */
    protected $UpdateContentMetaDataResult = null;

    /**
     * @param boolean $UpdateContentMetaDataResult
     */
    public function __construct($UpdateContentMetaDataResult)
    {
      $this->UpdateContentMetaDataResult = $UpdateContentMetaDataResult;
    }

    /**
     * @return boolean
     */
    public function getUpdateContentMetaDataResult()
    {
      return $this->UpdateContentMetaDataResult;
    }

    /**
     * @param boolean $UpdateContentMetaDataResult
     * @return UpdateContentMetaDataResponse
     */
    public function setUpdateContentMetaDataResult($UpdateContentMetaDataResult)
    {
      $this->UpdateContentMetaDataResult = $UpdateContentMetaDataResult;
      return $this;
    }

}
