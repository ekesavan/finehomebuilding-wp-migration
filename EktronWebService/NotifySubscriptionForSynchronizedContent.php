<?php

class NotifySubscriptionForSynchronizedContent
{

    /**
     * @var int $startVersion
     */
    protected $startVersion = null;

    /**
     * @param int $startVersion
     */
    public function __construct($startVersion)
    {
      $this->startVersion = $startVersion;
    }

    /**
     * @return int
     */
    public function getStartVersion()
    {
      return $this->startVersion;
    }

    /**
     * @param int $startVersion
     * @return NotifySubscriptionForSynchronizedContent
     */
    public function setStartVersion($startVersion)
    {
      $this->startVersion = $startVersion;
      return $this;
    }

}
