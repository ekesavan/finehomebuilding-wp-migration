<?php

class SaveContentResponse
{

    /**
     * @var boolean $SaveContentResult
     */
    protected $SaveContentResult = null;

    /**
     * @param boolean $SaveContentResult
     */
    public function __construct($SaveContentResult)
    {
      $this->SaveContentResult = $SaveContentResult;
    }

    /**
     * @return boolean
     */
    public function getSaveContentResult()
    {
      return $this->SaveContentResult;
    }

    /**
     * @param boolean $SaveContentResult
     * @return SaveContentResponse
     */
    public function setSaveContentResult($SaveContentResult)
    {
      $this->SaveContentResult = $SaveContentResult;
      return $this;
    }

}
