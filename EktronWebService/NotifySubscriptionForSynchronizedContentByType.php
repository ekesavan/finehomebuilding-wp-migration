<?php

class NotifySubscriptionForSynchronizedContentByType
{

    /**
     * @var int $startVersion
     */
    protected $startVersion = null;

    /**
     * @var int $syncType
     */
    protected $syncType = null;

    /**
     * @param int $startVersion
     * @param int $syncType
     */
    public function __construct($startVersion, $syncType)
    {
      $this->startVersion = $startVersion;
      $this->syncType = $syncType;
    }

    /**
     * @return int
     */
    public function getStartVersion()
    {
      return $this->startVersion;
    }

    /**
     * @param int $startVersion
     * @return NotifySubscriptionForSynchronizedContentByType
     */
    public function setStartVersion($startVersion)
    {
      $this->startVersion = $startVersion;
      return $this;
    }

    /**
     * @return int
     */
    public function getSyncType()
    {
      return $this->syncType;
    }

    /**
     * @param int $syncType
     * @return NotifySubscriptionForSynchronizedContentByType
     */
    public function setSyncType($syncType)
    {
      $this->syncType = $syncType;
      return $this;
    }

}
