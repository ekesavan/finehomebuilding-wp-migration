<?php

class UpdateContentMetaData
{

    /**
     * @var int $ContentID
     */
    protected $ContentID = null;

    /**
     * @var int $MetaDataTypeID
     */
    protected $MetaDataTypeID = null;

    /**
     * @var string $MetaDataText
     */
    protected $MetaDataText = null;

    /**
     * @param int $ContentID
     * @param int $MetaDataTypeID
     * @param string $MetaDataText
     */
    public function __construct($ContentID, $MetaDataTypeID, $MetaDataText)
    {
      $this->ContentID = $ContentID;
      $this->MetaDataTypeID = $MetaDataTypeID;
      $this->MetaDataText = $MetaDataText;
    }

    /**
     * @return int
     */
    public function getContentID()
    {
      return $this->ContentID;
    }

    /**
     * @param int $ContentID
     * @return UpdateContentMetaData
     */
    public function setContentID($ContentID)
    {
      $this->ContentID = $ContentID;
      return $this;
    }

    /**
     * @return int
     */
    public function getMetaDataTypeID()
    {
      return $this->MetaDataTypeID;
    }

    /**
     * @param int $MetaDataTypeID
     * @return UpdateContentMetaData
     */
    public function setMetaDataTypeID($MetaDataTypeID)
    {
      $this->MetaDataTypeID = $MetaDataTypeID;
      return $this;
    }

    /**
     * @return string
     */
    public function getMetaDataText()
    {
      return $this->MetaDataText;
    }

    /**
     * @param string $MetaDataText
     * @return UpdateContentMetaData
     */
    public function setMetaDataText($MetaDataText)
    {
      $this->MetaDataText = $MetaDataText;
      return $this;
    }

}
