<?php

class CheckOutContentResponse
{

    /**
     * @var boolean $CheckOutContentResult
     */
    protected $CheckOutContentResult = null;

    /**
     * @param boolean $CheckOutContentResult
     */
    public function __construct($CheckOutContentResult)
    {
      $this->CheckOutContentResult = $CheckOutContentResult;
    }

    /**
     * @return boolean
     */
    public function getCheckOutContentResult()
    {
      return $this->CheckOutContentResult;
    }

    /**
     * @param boolean $CheckOutContentResult
     * @return CheckOutContentResponse
     */
    public function setCheckOutContentResult($CheckOutContentResult)
    {
      $this->CheckOutContentResult = $CheckOutContentResult;
      return $this;
    }

}
