<?php

class ArrayOfApprovalData
{

    /**
     * @var ApprovalData[] $ApprovalData
     */
    protected $ApprovalData = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ApprovalData[]
     */
    public function getApprovalData()
    {
      return $this->ApprovalData;
    }

    /**
     * @param ApprovalData[] $ApprovalData
     * @return ArrayOfApprovalData
     */
    public function setApprovalData(array $ApprovalData = null)
    {
      $this->ApprovalData = $ApprovalData;
      return $this;
    }

}
