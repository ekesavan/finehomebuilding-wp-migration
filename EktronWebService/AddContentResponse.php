<?php

class AddContentResponse
{

    /**
     * @var int $AddContentResult
     */
    protected $AddContentResult = null;

    /**
     * @param int $AddContentResult
     */
    public function __construct($AddContentResult)
    {
      $this->AddContentResult = $AddContentResult;
    }

    /**
     * @return int
     */
    public function getAddContentResult()
    {
      return $this->AddContentResult;
    }

    /**
     * @param int $AddContentResult
     * @return AddContentResponse
     */
    public function setAddContentResult($AddContentResult)
    {
      $this->AddContentResult = $AddContentResult;
      return $this;
    }

}
