<?php

class ApprovalData
{

    /**
     * @var int $UserId
     */
    protected $UserId = null;

    /**
     * @var int $GroupId
     */
    protected $GroupId = null;

    /**
     * @var string $UserName
     */
    protected $UserName = null;

    /**
     * @var string $GroupName
     */
    protected $GroupName = null;

    /**
     * @var string $DisplayUserName
     */
    protected $DisplayUserName = null;

    /**
     * @var string $DisplayUserGroupName
     */
    protected $DisplayUserGroupName = null;

    /**
     * @var string $UserDomain
     */
    protected $UserDomain = null;

    /**
     * @var string $UserGroupDomain
     */
    protected $UserGroupDomain = null;

    /**
     * @var string $Type
     */
    protected $Type = null;

    /**
     * @var boolean $IsCurrentApprover
     */
    protected $IsCurrentApprover = null;

    /**
     * @var anyType $LegacyData
     */
    protected $LegacyData = null;

    /**
     * @param int $UserId
     * @param int $GroupId
     * @param boolean $IsCurrentApprover
     */
    public function __construct($UserId, $GroupId, $IsCurrentApprover)
    {
      $this->UserId = $UserId;
      $this->GroupId = $GroupId;
      $this->IsCurrentApprover = $IsCurrentApprover;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
      return $this->UserId;
    }

    /**
     * @param int $UserId
     * @return ApprovalData
     */
    public function setUserId($UserId)
    {
      $this->UserId = $UserId;
      return $this;
    }

    /**
     * @return int
     */
    public function getGroupId()
    {
      return $this->GroupId;
    }

    /**
     * @param int $GroupId
     * @return ApprovalData
     */
    public function setGroupId($GroupId)
    {
      $this->GroupId = $GroupId;
      return $this;
    }

    /**
     * @return string
     */
    public function getUserName()
    {
      return $this->UserName;
    }

    /**
     * @param string $UserName
     * @return ApprovalData
     */
    public function setUserName($UserName)
    {
      $this->UserName = $UserName;
      return $this;
    }

    /**
     * @return string
     */
    public function getGroupName()
    {
      return $this->GroupName;
    }

    /**
     * @param string $GroupName
     * @return ApprovalData
     */
    public function setGroupName($GroupName)
    {
      $this->GroupName = $GroupName;
      return $this;
    }

    /**
     * @return string
     */
    public function getDisplayUserName()
    {
      return $this->DisplayUserName;
    }

    /**
     * @param string $DisplayUserName
     * @return ApprovalData
     */
    public function setDisplayUserName($DisplayUserName)
    {
      $this->DisplayUserName = $DisplayUserName;
      return $this;
    }

    /**
     * @return string
     */
    public function getDisplayUserGroupName()
    {
      return $this->DisplayUserGroupName;
    }

    /**
     * @param string $DisplayUserGroupName
     * @return ApprovalData
     */
    public function setDisplayUserGroupName($DisplayUserGroupName)
    {
      $this->DisplayUserGroupName = $DisplayUserGroupName;
      return $this;
    }

    /**
     * @return string
     */
    public function getUserDomain()
    {
      return $this->UserDomain;
    }

    /**
     * @param string $UserDomain
     * @return ApprovalData
     */
    public function setUserDomain($UserDomain)
    {
      $this->UserDomain = $UserDomain;
      return $this;
    }

    /**
     * @return string
     */
    public function getUserGroupDomain()
    {
      return $this->UserGroupDomain;
    }

    /**
     * @param string $UserGroupDomain
     * @return ApprovalData
     */
    public function setUserGroupDomain($UserGroupDomain)
    {
      $this->UserGroupDomain = $UserGroupDomain;
      return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
      return $this->Type;
    }

    /**
     * @param string $Type
     * @return ApprovalData
     */
    public function setType($Type)
    {
      $this->Type = $Type;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIsCurrentApprover()
    {
      return $this->IsCurrentApprover;
    }

    /**
     * @param boolean $IsCurrentApprover
     * @return ApprovalData
     */
    public function setIsCurrentApprover($IsCurrentApprover)
    {
      $this->IsCurrentApprover = $IsCurrentApprover;
      return $this;
    }

    /**
     * @return anyType
     */
    public function getLegacyData()
    {
      return $this->LegacyData;
    }

    /**
     * @param anyType $LegacyData
     * @return ApprovalData
     */
    public function setLegacyData($LegacyData)
    {
      $this->LegacyData = $LegacyData;
      return $this;
    }

}
