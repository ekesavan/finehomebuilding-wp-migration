<?php

class XmlConfigData
{

    /**
     * @var int $Id
     */
    protected $Id = null;

    /**
     * @var string $Title
     */
    protected $Title = null;

    /**
     * @var string $Description
     */
    protected $Description = null;

    /**
     * @var string $EditXslt
     */
    protected $EditXslt = null;

    /**
     * @var string $SaveXslt
     */
    protected $SaveXslt = null;

    /**
     * @var string $Xslt1
     */
    protected $Xslt1 = null;

    /**
     * @var string $Xslt2
     */
    protected $Xslt2 = null;

    /**
     * @var string $Xslt3
     */
    protected $Xslt3 = null;

    /**
     * @var string $Xslt4
     */
    protected $Xslt4 = null;

    /**
     * @var string $Xslt5
     */
    protected $Xslt5 = null;

    /**
     * @var string $XmlSchema
     */
    protected $XmlSchema = null;

    /**
     * @var string $XmlNameSpace
     */
    protected $XmlNameSpace = null;

    /**
     * @var string $XmlAdvConfig
     */
    protected $XmlAdvConfig = null;

    /**
     * @var string $DateCreated
     */
    protected $DateCreated = null;

    /**
     * @var string $DisplayDateCreated
     */
    protected $DisplayDateCreated = null;

    /**
     * @var \DateTime $LastEditDate
     */
    protected $LastEditDate = null;

    /**
     * @var string $DisplayLastEditDate
     */
    protected $DisplayLastEditDate = null;

    /**
     * @var int $UserId
     */
    protected $UserId = null;

    /**
     * @var string $EditorFirstName
     */
    protected $EditorFirstName = null;

    /**
     * @var string $EditorLastName
     */
    protected $EditorLastName = null;

    /**
     * @var string $PhysicalPath
     */
    protected $PhysicalPath = null;

    /**
     * @var string $LogicalPath
     */
    protected $LogicalPath = null;

    /**
     * @var string $DefaultXslt
     */
    protected $DefaultXslt = null;

    /**
     * @var string $PackageXslt
     */
    protected $PackageXslt = null;

    /**
     * @var string $PackageDisplayXslt
     */
    protected $PackageDisplayXslt = null;

    /**
     * @var string $DesignStyleSheet
     */
    protected $DesignStyleSheet = null;

    /**
     * @var boolean $IsDefault
     */
    protected $IsDefault = null;

    /**
     * @var string $FieldList
     */
    protected $FieldList = null;

    /**
     * @param int $Id
     * @param \DateTime $LastEditDate
     * @param int $UserId
     * @param boolean $IsDefault
     */
    public function __construct($Id, \DateTime $LastEditDate, $UserId, $IsDefault)
    {
      $this->Id = $Id;
      $this->LastEditDate = $LastEditDate->format(\DateTime::ATOM);
      $this->UserId = $UserId;
      $this->IsDefault = $IsDefault;
    }

    /**
     * @return int
     */
    public function getId()
    {
      return $this->Id;
    }

    /**
     * @param int $Id
     * @return XmlConfigData
     */
    public function setId($Id)
    {
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
      return $this->Title;
    }

    /**
     * @param string $Title
     * @return XmlConfigData
     */
    public function setTitle($Title)
    {
      $this->Title = $Title;
      return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
      return $this->Description;
    }

    /**
     * @param string $Description
     * @return XmlConfigData
     */
    public function setDescription($Description)
    {
      $this->Description = $Description;
      return $this;
    }

    /**
     * @return string
     */
    public function getEditXslt()
    {
      return $this->EditXslt;
    }

    /**
     * @param string $EditXslt
     * @return XmlConfigData
     */
    public function setEditXslt($EditXslt)
    {
      $this->EditXslt = $EditXslt;
      return $this;
    }

    /**
     * @return string
     */
    public function getSaveXslt()
    {
      return $this->SaveXslt;
    }

    /**
     * @param string $SaveXslt
     * @return XmlConfigData
     */
    public function setSaveXslt($SaveXslt)
    {
      $this->SaveXslt = $SaveXslt;
      return $this;
    }

    /**
     * @return string
     */
    public function getXslt1()
    {
      return $this->Xslt1;
    }

    /**
     * @param string $Xslt1
     * @return XmlConfigData
     */
    public function setXslt1($Xslt1)
    {
      $this->Xslt1 = $Xslt1;
      return $this;
    }

    /**
     * @return string
     */
    public function getXslt2()
    {
      return $this->Xslt2;
    }

    /**
     * @param string $Xslt2
     * @return XmlConfigData
     */
    public function setXslt2($Xslt2)
    {
      $this->Xslt2 = $Xslt2;
      return $this;
    }

    /**
     * @return string
     */
    public function getXslt3()
    {
      return $this->Xslt3;
    }

    /**
     * @param string $Xslt3
     * @return XmlConfigData
     */
    public function setXslt3($Xslt3)
    {
      $this->Xslt3 = $Xslt3;
      return $this;
    }

    /**
     * @return string
     */
    public function getXslt4()
    {
      return $this->Xslt4;
    }

    /**
     * @param string $Xslt4
     * @return XmlConfigData
     */
    public function setXslt4($Xslt4)
    {
      $this->Xslt4 = $Xslt4;
      return $this;
    }

    /**
     * @return string
     */
    public function getXslt5()
    {
      return $this->Xslt5;
    }

    /**
     * @param string $Xslt5
     * @return XmlConfigData
     */
    public function setXslt5($Xslt5)
    {
      $this->Xslt5 = $Xslt5;
      return $this;
    }

    /**
     * @return string
     */
    public function getXmlSchema()
    {
      return $this->XmlSchema;
    }

    /**
     * @param string $XmlSchema
     * @return XmlConfigData
     */
    public function setXmlSchema($XmlSchema)
    {
      $this->XmlSchema = $XmlSchema;
      return $this;
    }

    /**
     * @return string
     */
    public function getXmlNameSpace()
    {
      return $this->XmlNameSpace;
    }

    /**
     * @param string $XmlNameSpace
     * @return XmlConfigData
     */
    public function setXmlNameSpace($XmlNameSpace)
    {
      $this->XmlNameSpace = $XmlNameSpace;
      return $this;
    }

    /**
     * @return string
     */
    public function getXmlAdvConfig()
    {
      return $this->XmlAdvConfig;
    }

    /**
     * @param string $XmlAdvConfig
     * @return XmlConfigData
     */
    public function setXmlAdvConfig($XmlAdvConfig)
    {
      $this->XmlAdvConfig = $XmlAdvConfig;
      return $this;
    }

    /**
     * @return string
     */
    public function getDateCreated()
    {
      return $this->DateCreated;
    }

    /**
     * @param string $DateCreated
     * @return XmlConfigData
     */
    public function setDateCreated($DateCreated)
    {
      $this->DateCreated = $DateCreated;
      return $this;
    }

    /**
     * @return string
     */
    public function getDisplayDateCreated()
    {
      return $this->DisplayDateCreated;
    }

    /**
     * @param string $DisplayDateCreated
     * @return XmlConfigData
     */
    public function setDisplayDateCreated($DisplayDateCreated)
    {
      $this->DisplayDateCreated = $DisplayDateCreated;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getLastEditDate()
    {
      if ($this->LastEditDate == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->LastEditDate);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $LastEditDate
     * @return XmlConfigData
     */
    public function setLastEditDate(\DateTime $LastEditDate)
    {
      $this->LastEditDate = $LastEditDate->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return string
     */
    public function getDisplayLastEditDate()
    {
      return $this->DisplayLastEditDate;
    }

    /**
     * @param string $DisplayLastEditDate
     * @return XmlConfigData
     */
    public function setDisplayLastEditDate($DisplayLastEditDate)
    {
      $this->DisplayLastEditDate = $DisplayLastEditDate;
      return $this;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
      return $this->UserId;
    }

    /**
     * @param int $UserId
     * @return XmlConfigData
     */
    public function setUserId($UserId)
    {
      $this->UserId = $UserId;
      return $this;
    }

    /**
     * @return string
     */
    public function getEditorFirstName()
    {
      return $this->EditorFirstName;
    }

    /**
     * @param string $EditorFirstName
     * @return XmlConfigData
     */
    public function setEditorFirstName($EditorFirstName)
    {
      $this->EditorFirstName = $EditorFirstName;
      return $this;
    }

    /**
     * @return string
     */
    public function getEditorLastName()
    {
      return $this->EditorLastName;
    }

    /**
     * @param string $EditorLastName
     * @return XmlConfigData
     */
    public function setEditorLastName($EditorLastName)
    {
      $this->EditorLastName = $EditorLastName;
      return $this;
    }

    /**
     * @return string
     */
    public function getPhysicalPath()
    {
      return $this->PhysicalPath;
    }

    /**
     * @param string $PhysicalPath
     * @return XmlConfigData
     */
    public function setPhysicalPath($PhysicalPath)
    {
      $this->PhysicalPath = $PhysicalPath;
      return $this;
    }

    /**
     * @return string
     */
    public function getLogicalPath()
    {
      return $this->LogicalPath;
    }

    /**
     * @param string $LogicalPath
     * @return XmlConfigData
     */
    public function setLogicalPath($LogicalPath)
    {
      $this->LogicalPath = $LogicalPath;
      return $this;
    }

    /**
     * @return string
     */
    public function getDefaultXslt()
    {
      return $this->DefaultXslt;
    }

    /**
     * @param string $DefaultXslt
     * @return XmlConfigData
     */
    public function setDefaultXslt($DefaultXslt)
    {
      $this->DefaultXslt = $DefaultXslt;
      return $this;
    }

    /**
     * @return string
     */
    public function getPackageXslt()
    {
      return $this->PackageXslt;
    }

    /**
     * @param string $PackageXslt
     * @return XmlConfigData
     */
    public function setPackageXslt($PackageXslt)
    {
      $this->PackageXslt = $PackageXslt;
      return $this;
    }

    /**
     * @return string
     */
    public function getPackageDisplayXslt()
    {
      return $this->PackageDisplayXslt;
    }

    /**
     * @param string $PackageDisplayXslt
     * @return XmlConfigData
     */
    public function setPackageDisplayXslt($PackageDisplayXslt)
    {
      $this->PackageDisplayXslt = $PackageDisplayXslt;
      return $this;
    }

    /**
     * @return string
     */
    public function getDesignStyleSheet()
    {
      return $this->DesignStyleSheet;
    }

    /**
     * @param string $DesignStyleSheet
     * @return XmlConfigData
     */
    public function setDesignStyleSheet($DesignStyleSheet)
    {
      $this->DesignStyleSheet = $DesignStyleSheet;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIsDefault()
    {
      return $this->IsDefault;
    }

    /**
     * @param boolean $IsDefault
     * @return XmlConfigData
     */
    public function setIsDefault($IsDefault)
    {
      $this->IsDefault = $IsDefault;
      return $this;
    }

    /**
     * @return string
     */
    public function getFieldList()
    {
      return $this->FieldList;
    }

    /**
     * @param string $FieldList
     * @return XmlConfigData
     */
    public function setFieldList($FieldList)
    {
      $this->FieldList = $FieldList;
      return $this;
    }

}
