<?php

class AddContent_x0020_With_x0020_overload_x0020_Parameter_x0020_CleanHtmlResponse
{

    /**
     * @var int $AddContent_x0020_With_x0020_overload_x0020_Parameter_x0020_CleanHtmlResult
     */
    protected $AddContent_x0020_With_x0020_overload_x0020_Parameter_x0020_CleanHtmlResult = null;

    /**
     * @param int $AddContent_x0020_With_x0020_overload_x0020_Parameter_x0020_CleanHtmlResult
     */
    public function __construct($AddContent_x0020_With_x0020_overload_x0020_Parameter_x0020_CleanHtmlResult)
    {
      $this->AddContent_x0020_With_x0020_overload_x0020_Parameter_x0020_CleanHtmlResult = $AddContent_x0020_With_x0020_overload_x0020_Parameter_x0020_CleanHtmlResult;
    }

    /**
     * @return int
     */
    public function getAddContent_x0020_With_x0020_overload_x0020_Parameter_x0020_CleanHtmlResult()
    {
      return $this->AddContent_x0020_With_x0020_overload_x0020_Parameter_x0020_CleanHtmlResult;
    }

    /**
     * @param int $AddContent_x0020_With_x0020_overload_x0020_Parameter_x0020_CleanHtmlResult
     * @return AddContent_x0020_With_x0020_overload_x0020_Parameter_x0020_CleanHtmlResponse
     */
    public function setAddContent_x0020_With_x0020_overload_x0020_Parameter_x0020_CleanHtmlResult($AddContent_x0020_With_x0020_overload_x0020_Parameter_x0020_CleanHtmlResult)
    {
      $this->AddContent_x0020_With_x0020_overload_x0020_Parameter_x0020_CleanHtmlResult = $AddContent_x0020_With_x0020_overload_x0020_Parameter_x0020_CleanHtmlResult;
      return $this;
    }

}
