<?php

class SaveContent
{

    /**
     * @var ContentEditData $contentEditData
     */
    protected $contentEditData = null;

    /**
     * @param ContentEditData $contentEditData
     */
    public function __construct($contentEditData)
    {
      $this->contentEditData = $contentEditData;
    }

    /**
     * @return ContentEditData
     */
    public function getContentEditData()
    {
      return $this->contentEditData;
    }

    /**
     * @param ContentEditData $contentEditData
     * @return SaveContent
     */
    public function setContentEditData($contentEditData)
    {
      $this->contentEditData = $contentEditData;
      return $this;
    }

}
