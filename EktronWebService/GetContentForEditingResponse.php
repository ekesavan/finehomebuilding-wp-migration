<?php

class GetContentForEditingResponse
{

    /**
     * @var ContentEditData $GetContentForEditingResult
     */
    protected $GetContentForEditingResult = null;

    /**
     * @param ContentEditData $GetContentForEditingResult
     */
    public function __construct($GetContentForEditingResult)
    {
      $this->GetContentForEditingResult = $GetContentForEditingResult;
    }

    /**
     * @return ContentEditData
     */
    public function getGetContentForEditingResult()
    {
      return $this->GetContentForEditingResult;
    }

    /**
     * @param ContentEditData $GetContentForEditingResult
     * @return GetContentForEditingResponse
     */
    public function setGetContentForEditingResult($GetContentForEditingResult)
    {
      $this->GetContentForEditingResult = $GetContentForEditingResult;
      return $this;
    }

}
