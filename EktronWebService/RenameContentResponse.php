<?php

class RenameContentResponse
{

    /**
     * @var ContentData $RenameContentResult
     */
    protected $RenameContentResult = null;

    /**
     * @param ContentData $RenameContentResult
     */
    public function __construct($RenameContentResult)
    {
      $this->RenameContentResult = $RenameContentResult;
    }

    /**
     * @return ContentData
     */
    public function getRenameContentResult()
    {
      return $this->RenameContentResult;
    }

    /**
     * @param ContentData $RenameContentResult
     * @return RenameContentResponse
     */
    public function setRenameContentResult($RenameContentResult)
    {
      $this->RenameContentResult = $RenameContentResult;
      return $this;
    }

}
