<?php

class AddContent_x0020_with_x0020_XmlID_x0020_and_x0020_TemplateIDResponse
{

    /**
     * @var int $AddContent_x0020_with_x0020_XmlID_x0020_and_x0020_TemplateIDResult
     */
    protected $AddContent_x0020_with_x0020_XmlID_x0020_and_x0020_TemplateIDResult = null;

    /**
     * @param int $AddContent_x0020_with_x0020_XmlID_x0020_and_x0020_TemplateIDResult
     */
    public function __construct($AddContent_x0020_with_x0020_XmlID_x0020_and_x0020_TemplateIDResult)
    {
      $this->AddContent_x0020_with_x0020_XmlID_x0020_and_x0020_TemplateIDResult = $AddContent_x0020_with_x0020_XmlID_x0020_and_x0020_TemplateIDResult;
    }

    /**
     * @return int
     */
    public function getAddContent_x0020_with_x0020_XmlID_x0020_and_x0020_TemplateIDResult()
    {
      return $this->AddContent_x0020_with_x0020_XmlID_x0020_and_x0020_TemplateIDResult;
    }

    /**
     * @param int $AddContent_x0020_with_x0020_XmlID_x0020_and_x0020_TemplateIDResult
     * @return AddContent_x0020_with_x0020_XmlID_x0020_and_x0020_TemplateIDResponse
     */
    public function setAddContent_x0020_with_x0020_XmlID_x0020_and_x0020_TemplateIDResult($AddContent_x0020_with_x0020_XmlID_x0020_and_x0020_TemplateIDResult)
    {
      $this->AddContent_x0020_with_x0020_XmlID_x0020_and_x0020_TemplateIDResult = $AddContent_x0020_with_x0020_XmlID_x0020_and_x0020_TemplateIDResult;
      return $this;
    }

}
