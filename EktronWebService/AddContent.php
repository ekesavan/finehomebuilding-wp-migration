<?php

class AddContent
{

    /**
     * @var string $ContentTitle
     */
    protected $ContentTitle = null;

    /**
     * @var string $ContentComment
     */
    protected $ContentComment = null;

    /**
     * @var string $ContentHtml
     */
    protected $ContentHtml = null;

    /**
     * @var string $SearchText
     */
    protected $SearchText = null;

    /**
     * @var string $SummaryHtml
     */
    protected $SummaryHtml = null;

    /**
     * @var string $ContentLanguage
     */
    protected $ContentLanguage = null;

    /**
     * @var int $FolderId
     */
    protected $FolderId = null;

    /**
     * @var anyType $GoLive
     */
    protected $GoLive = null;

    /**
     * @var anyType $EndDate
     */
    protected $EndDate = null;

    /**
     * @var string $MetaInfoXml
     */
    protected $MetaInfoXml = null;

    /**
     * @param string $ContentTitle
     * @param string $ContentComment
     * @param string $ContentHtml
     * @param string $SearchText
     * @param string $SummaryHtml
     * @param string $ContentLanguage
     * @param int $FolderId
     * @param anyType $GoLive
     * @param anyType $EndDate
     * @param string $MetaInfoXml
     */
    public function __construct($ContentTitle, $ContentComment, $ContentHtml, $SearchText, $SummaryHtml, $ContentLanguage, $FolderId, $GoLive, $EndDate, $MetaInfoXml)
    {
      $this->ContentTitle = $ContentTitle;
      $this->ContentComment = $ContentComment;
      $this->ContentHtml = $ContentHtml;
      $this->SearchText = $SearchText;
      $this->SummaryHtml = $SummaryHtml;
      $this->ContentLanguage = $ContentLanguage;
      $this->FolderId = $FolderId;
      $this->GoLive = $GoLive;
      $this->EndDate = $EndDate;
      $this->MetaInfoXml = $MetaInfoXml;
    }

    /**
     * @return string
     */
    public function getContentTitle()
    {
      return $this->ContentTitle;
    }

    /**
     * @param string $ContentTitle
     * @return AddContent
     */
    public function setContentTitle($ContentTitle)
    {
      $this->ContentTitle = $ContentTitle;
      return $this;
    }

    /**
     * @return string
     */
    public function getContentComment()
    {
      return $this->ContentComment;
    }

    /**
     * @param string $ContentComment
     * @return AddContent
     */
    public function setContentComment($ContentComment)
    {
      $this->ContentComment = $ContentComment;
      return $this;
    }

    /**
     * @return string
     */
    public function getContentHtml()
    {
      return $this->ContentHtml;
    }

    /**
     * @param string $ContentHtml
     * @return AddContent
     */
    public function setContentHtml($ContentHtml)
    {
      $this->ContentHtml = $ContentHtml;
      return $this;
    }

    /**
     * @return string
     */
    public function getSearchText()
    {
      return $this->SearchText;
    }

    /**
     * @param string $SearchText
     * @return AddContent
     */
    public function setSearchText($SearchText)
    {
      $this->SearchText = $SearchText;
      return $this;
    }

    /**
     * @return string
     */
    public function getSummaryHtml()
    {
      return $this->SummaryHtml;
    }

    /**
     * @param string $SummaryHtml
     * @return AddContent
     */
    public function setSummaryHtml($SummaryHtml)
    {
      $this->SummaryHtml = $SummaryHtml;
      return $this;
    }

    /**
     * @return string
     */
    public function getContentLanguage()
    {
      return $this->ContentLanguage;
    }

    /**
     * @param string $ContentLanguage
     * @return AddContent
     */
    public function setContentLanguage($ContentLanguage)
    {
      $this->ContentLanguage = $ContentLanguage;
      return $this;
    }

    /**
     * @return int
     */
    public function getFolderId()
    {
      return $this->FolderId;
    }

    /**
     * @param int $FolderId
     * @return AddContent
     */
    public function setFolderId($FolderId)
    {
      $this->FolderId = $FolderId;
      return $this;
    }

    /**
     * @return anyType
     */
    public function getGoLive()
    {
      return $this->GoLive;
    }

    /**
     * @param anyType $GoLive
     * @return AddContent
     */
    public function setGoLive($GoLive)
    {
      $this->GoLive = $GoLive;
      return $this;
    }

    /**
     * @return anyType
     */
    public function getEndDate()
    {
      return $this->EndDate;
    }

    /**
     * @param anyType $EndDate
     * @return AddContent
     */
    public function setEndDate($EndDate)
    {
      $this->EndDate = $EndDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getMetaInfoXml()
    {
      return $this->MetaInfoXml;
    }

    /**
     * @param string $MetaInfoXml
     * @return AddContent
     */
    public function setMetaInfoXml($MetaInfoXml)
    {
      $this->MetaInfoXml = $MetaInfoXml;
      return $this;
    }

}
