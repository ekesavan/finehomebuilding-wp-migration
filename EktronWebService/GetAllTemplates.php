<?php

class GetAllTemplates
{

    /**
     * @var string $OrderBy
     */
    protected $OrderBy = null;

    /**
     * @param string $OrderBy
     */
    public function __construct($OrderBy)
    {
      $this->OrderBy = $OrderBy;
    }

    /**
     * @return string
     */
    public function getOrderBy()
    {
      return $this->OrderBy;
    }

    /**
     * @param string $OrderBy
     * @return GetAllTemplates
     */
    public function setOrderBy($OrderBy)
    {
      $this->OrderBy = $OrderBy;
      return $this;
    }

}
