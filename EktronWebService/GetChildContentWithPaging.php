<?php

class GetChildContentWithPaging
{

    /**
     * @var int $FolderID
     */
    protected $FolderID = null;

    /**
     * @var boolean $Recursive
     */
    protected $Recursive = null;

    /**
     * @var string $OrderBy
     */
    protected $OrderBy = null;

    /**
     * @var PagingInfo $pagingInfo
     */
    protected $pagingInfo = null;

    /**
     * @param int $FolderID
     * @param boolean $Recursive
     * @param string $OrderBy
     * @param PagingInfo $pagingInfo
     */
    public function __construct($FolderID, $Recursive, $OrderBy, $pagingInfo)
    {
      $this->FolderID = $FolderID;
      $this->Recursive = $Recursive;
      $this->OrderBy = $OrderBy;
      $this->pagingInfo = $pagingInfo;
    }

    /**
     * @return int
     */
    public function getFolderID()
    {
      return $this->FolderID;
    }

    /**
     * @param int $FolderID
     * @return GetChildContentWithPaging
     */
    public function setFolderID($FolderID)
    {
      $this->FolderID = $FolderID;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getRecursive()
    {
      return $this->Recursive;
    }

    /**
     * @param boolean $Recursive
     * @return GetChildContentWithPaging
     */
    public function setRecursive($Recursive)
    {
      $this->Recursive = $Recursive;
      return $this;
    }

    /**
     * @return string
     */
    public function getOrderBy()
    {
      return $this->OrderBy;
    }

    /**
     * @param string $OrderBy
     * @return GetChildContentWithPaging
     */
    public function setOrderBy($OrderBy)
    {
      $this->OrderBy = $OrderBy;
      return $this;
    }

    /**
     * @return PagingInfo
     */
    public function getPagingInfo()
    {
      return $this->pagingInfo;
    }

    /**
     * @param PagingInfo $pagingInfo
     * @return GetChildContentWithPaging
     */
    public function setPagingInfo($pagingInfo)
    {
      $this->pagingInfo = $pagingInfo;
      return $this;
    }

}
