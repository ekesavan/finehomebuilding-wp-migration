<?php

class PublishContentResponse
{

    /**
     * @var boolean $PublishContentResult
     */
    protected $PublishContentResult = null;

    /**
     * @param boolean $PublishContentResult
     */
    public function __construct($PublishContentResult)
    {
      $this->PublishContentResult = $PublishContentResult;
    }

    /**
     * @return boolean
     */
    public function getPublishContentResult()
    {
      return $this->PublishContentResult;
    }

    /**
     * @param boolean $PublishContentResult
     * @return PublishContentResponse
     */
    public function setPublishContentResult($PublishContentResult)
    {
      $this->PublishContentResult = $PublishContentResult;
      return $this;
    }

}
