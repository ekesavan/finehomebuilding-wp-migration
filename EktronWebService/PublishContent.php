<?php

class PublishContent
{

    /**
     * @var int $ContentID
     */
    protected $ContentID = null;

    /**
     * @var int $FolderID
     */
    protected $FolderID = null;

    /**
     * @var int $ContentLanguage
     */
    protected $ContentLanguage = null;

    /**
     * @var string $DontCreateTask
     */
    protected $DontCreateTask = null;

    /**
     * @var int $UserID
     */
    protected $UserID = null;

    /**
     * @var string $TaskTitle
     */
    protected $TaskTitle = null;

    /**
     * @param int $ContentID
     * @param int $FolderID
     * @param int $ContentLanguage
     * @param string $DontCreateTask
     * @param int $UserID
     * @param string $TaskTitle
     */
    public function __construct($ContentID, $FolderID, $ContentLanguage, $DontCreateTask, $UserID, $TaskTitle)
    {
      $this->ContentID = $ContentID;
      $this->FolderID = $FolderID;
      $this->ContentLanguage = $ContentLanguage;
      $this->DontCreateTask = $DontCreateTask;
      $this->UserID = $UserID;
      $this->TaskTitle = $TaskTitle;
    }

    /**
     * @return int
     */
    public function getContentID()
    {
      return $this->ContentID;
    }

    /**
     * @param int $ContentID
     * @return PublishContent
     */
    public function setContentID($ContentID)
    {
      $this->ContentID = $ContentID;
      return $this;
    }

    /**
     * @return int
     */
    public function getFolderID()
    {
      return $this->FolderID;
    }

    /**
     * @param int $FolderID
     * @return PublishContent
     */
    public function setFolderID($FolderID)
    {
      $this->FolderID = $FolderID;
      return $this;
    }

    /**
     * @return int
     */
    public function getContentLanguage()
    {
      return $this->ContentLanguage;
    }

    /**
     * @param int $ContentLanguage
     * @return PublishContent
     */
    public function setContentLanguage($ContentLanguage)
    {
      $this->ContentLanguage = $ContentLanguage;
      return $this;
    }

    /**
     * @return string
     */
    public function getDontCreateTask()
    {
      return $this->DontCreateTask;
    }

    /**
     * @param string $DontCreateTask
     * @return PublishContent
     */
    public function setDontCreateTask($DontCreateTask)
    {
      $this->DontCreateTask = $DontCreateTask;
      return $this;
    }

    /**
     * @return int
     */
    public function getUserID()
    {
      return $this->UserID;
    }

    /**
     * @param int $UserID
     * @return PublishContent
     */
    public function setUserID($UserID)
    {
      $this->UserID = $UserID;
      return $this;
    }

    /**
     * @return string
     */
    public function getTaskTitle()
    {
      return $this->TaskTitle;
    }

    /**
     * @param string $TaskTitle
     * @return PublishContent
     */
    public function setTaskTitle($TaskTitle)
    {
      $this->TaskTitle = $TaskTitle;
      return $this;
    }

}
