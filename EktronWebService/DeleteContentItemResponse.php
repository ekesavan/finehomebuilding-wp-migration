<?php

class DeleteContentItemResponse
{

    /**
     * @var boolean $DeleteContentItemResult
     */
    protected $DeleteContentItemResult = null;

    /**
     * @param boolean $DeleteContentItemResult
     */
    public function __construct($DeleteContentItemResult)
    {
      $this->DeleteContentItemResult = $DeleteContentItemResult;
    }

    /**
     * @return boolean
     */
    public function getDeleteContentItemResult()
    {
      return $this->DeleteContentItemResult;
    }

    /**
     * @param boolean $DeleteContentItemResult
     * @return DeleteContentItemResponse
     */
    public function setDeleteContentItemResult($DeleteContentItemResult)
    {
      $this->DeleteContentItemResult = $DeleteContentItemResult;
      return $this;
    }

}
