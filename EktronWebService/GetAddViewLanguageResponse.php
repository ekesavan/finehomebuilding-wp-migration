<?php

class GetAddViewLanguageResponse
{

    /**
     * @var ArrayOfLanguageData $GetAddViewLanguageResult
     */
    protected $GetAddViewLanguageResult = null;

    /**
     * @param ArrayOfLanguageData $GetAddViewLanguageResult
     */
    public function __construct($GetAddViewLanguageResult)
    {
      $this->GetAddViewLanguageResult = $GetAddViewLanguageResult;
    }

    /**
     * @return ArrayOfLanguageData
     */
    public function getGetAddViewLanguageResult()
    {
      return $this->GetAddViewLanguageResult;
    }

    /**
     * @param ArrayOfLanguageData $GetAddViewLanguageResult
     * @return GetAddViewLanguageResponse
     */
    public function setGetAddViewLanguageResult($GetAddViewLanguageResult)
    {
      $this->GetAddViewLanguageResult = $GetAddViewLanguageResult;
      return $this;
    }

}
