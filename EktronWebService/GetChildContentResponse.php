<?php

class GetChildContentResponse
{

    /**
     * @var ArrayOfContentData $GetChildContentResult
     */
    protected $GetChildContentResult = null;

    /**
     * @param ArrayOfContentData $GetChildContentResult
     */
    public function __construct($GetChildContentResult)
    {
      $this->GetChildContentResult = $GetChildContentResult;
    }

    /**
     * @return ArrayOfContentData
     */
    public function getGetChildContentResult()
    {
      return $this->GetChildContentResult;
    }

    /**
     * @param ArrayOfContentData $GetChildContentResult
     * @return GetChildContentResponse
     */
    public function setGetChildContentResult($GetChildContentResult)
    {
      $this->GetChildContentResult = $GetChildContentResult;
      return $this;
    }

}
