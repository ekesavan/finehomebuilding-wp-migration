<?php 
set_time_limit( 5 * 60 * 60 );
ini_set('memory_limit','2056M');

require_once( dirname(__FILE__) . "/lib/Config.php");
require_once( dirname(__FILE__) . "/lib/FHBMongo.php");

if( ! defined('__FHB__MIGRATION__') )
	define( '__FHB__MIGRATION__', true );

// WP Load
require_once( dirname(__FILE__) . "/../finehomebuilding-wordpress/web/wp/wp-load.php");
require_once( dirname(__FILE__) . "/../finehomebuilding-wordpress/web/wp/wp-admin/includes/admin.php");
require_once( dirname(__FILE__) . "/../finehomebuilding-wordpress/web/app/themes/finehomebuilding/functions.php");

	
require_once( dirname(__FILE__) . "/vendor/simple_html_dom.php");

require_once( dirname(__FILE__) . "/lib/WordPressImport.php");
require_once( dirname(__FILE__) . "/lib/WP/WPUser.php");
require_once( dirname(__FILE__) . "/lib/WP/WPAttachment.php");
require_once( dirname(__FILE__) . "/lib/WP/WPContent.php");
require_once( dirname(__FILE__) . "/lib/WP/WPArticle.php");
require_once( dirname(__FILE__) . "/lib/WP/WPProduct.php");
require_once( dirname(__FILE__) . "/lib/WP/WPReview.php");
require_once( dirname(__FILE__) . "/lib/WP/WPVideo.php");
require_once( dirname(__FILE__) . "/lib/WP/WPTerm.php");
require_once( dirname(__FILE__) . "/lib/WP/WPManufacturer.php");
require_once( dirname(__FILE__) . "/lib/WP/WPAuthor.php");
require_once( dirname(__FILE__) . "/lib/WP/WPDigitalIssue.php");
require_once( dirname(__FILE__) . "/lib/WP/WPPool.php");
require_once( dirname(__FILE__) . "/lib/WP/ResizeImage.php");
require_once( dirname(__FILE__) . "/lib/ImageCrop.php");

define( '__WP_UPDATE__', false );

define( '__LOG__', false );

define('WP_DEBUG', true);

define( 'WP_DEBUG_LOG', true );

define( 'WP_DEBUG_DISPLAY', true );

$wp = new WordPressImport();

$query = array();

error_log( "WordPress Start Export:".WP_ENV );

//$wp->wp_map_db->drop('users');

/*
* Test : $query = array( "Id" => '60546' );
*/

//$wp->insert_authors( $query );

//$wp->insert_manufacturer();


//$query['Id'] = '114201';
//$wp->insert_articles(  $query );

//$query['Id'] = "131808";
//$wp->insert_video( $query );

//$query['Id'] = "69678";
//$wp->insert_department_articles($query);

//$wp->insert_book_excerpt();

//$wp->insert_book_contests();

//$query['Id'] ='131734';
//$wp->insert_question_answer($query);

//$wp->insert_readers_tips();

//$query['Id'] ='130358'; 
//$wp->insert_slide_show($query);

/* $wp->insert_special_collection(); */

/* $wp->insert_tool_guide(); */

//$query['Id'] = "131603";
//$wp->insert_review($query);

//$wp->insert_codeigniter_authors();

//$wp->wp_map_db->drop('users');
//$wp->insert_codeigniter_users();

//$wp->insert_codeigniter_pools();

$wp->insert_codeigniter_content();

//$wp->insert_codeigniter_digital_issues();

//$wp->insert_digital_issue_articles();

//$wp->insert_codeigniter_comments();

?>