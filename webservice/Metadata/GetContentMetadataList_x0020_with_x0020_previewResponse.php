<?php

class GetContentMetadataList_x0020_with_x0020_previewResponse
{

    /**
     * @var CustomAttribute[] $GetContentMetadataList_x0020_with_x0020_previewResult
     * @access public
     */
    public $GetContentMetadataList_x0020_with_x0020_previewResult = null;

    /**
     * @param CustomAttribute[] $GetContentMetadataList_x0020_with_x0020_previewResult
     * @access public
     */
    public function __construct($GetContentMetadataList_x0020_with_x0020_previewResult)
    {
      $this->GetContentMetadataList_x0020_with_x0020_previewResult = $GetContentMetadataList_x0020_with_x0020_previewResult;
    }

}
