<?php

class GetMetadataTypeResponse
{

    /**
     * @var ContentMetaData $GetMetadataTypeResult
     * @access public
     */
    public $GetMetadataTypeResult = null;

    /**
     * @param ContentMetaData $GetMetadataTypeResult
     * @access public
     */
    public function __construct($GetMetadataTypeResult)
    {
      $this->GetMetadataTypeResult = $GetMetadataTypeResult;
    }

}
