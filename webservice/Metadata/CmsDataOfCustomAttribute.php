<?php

include_once('BaseDataOfCustomAttribute.php');

class CmsDataOfCustomAttribute extends BaseDataOfCustomAttribute
{

    /**
     * @var int $Id
     * @access public
     */
    public $Id = null;

    /**
     * @param int $Id
     * @access public
     */
    public function __construct($Id)
    {
      $this->Id = $Id;
    }

}
