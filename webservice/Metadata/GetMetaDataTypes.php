<?php

class GetMetaDataTypes
{

    /**
     * @var string $OrderBy
     * @access public
     */
    public $OrderBy = null;

    /**
     * @param string $OrderBy
     * @access public
     */
    public function __construct($OrderBy)
    {
      $this->OrderBy = $OrderBy;
    }

}
