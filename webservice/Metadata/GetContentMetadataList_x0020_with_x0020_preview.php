<?php

class GetContentMetadataList_x0020_with_x0020_preview
{

    /**
     * @var int $ContentId
     * @access public
     */
    public $ContentId = null;

    /**
     * @var boolean $preview
     * @access public
     */
    public $preview = null;

    /**
     * @param int $ContentId
     * @param boolean $preview
     * @access public
     */
    public function __construct($ContentId, $preview)
    {
      $this->ContentId = $ContentId;
      $this->preview = $preview;
    }

}
