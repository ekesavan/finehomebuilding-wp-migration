<?php

class GetContent
{

    /**
     * @var int $ContentID
     * @access public
     */
    public $ContentID = null;

    /**
     * @var ContentResultType $ResultType
     * @access public
     */
    public $ResultType = null;

    /**
     * @param int $ContentID
     * @param ContentResultType $ResultType
     * @access public
     */
    public function __construct($ContentID, $ResultType)
    {
      $this->ContentID = $ContentID;
      $this->ResultType = $ResultType;
    }

}
