<?php

class GetChildContentWithPagingResponse
{

    /**
     * @var ContentData[] $GetChildContentWithPagingResult
     * @access public
     */
    public $GetChildContentWithPagingResult = null;

    /**
     * @param ContentData[] $GetChildContentWithPagingResult
     * @access public
     */
    public function __construct($GetChildContentWithPagingResult)
    {
      $this->GetChildContentWithPagingResult = $GetChildContentWithPagingResult;
    }

}
