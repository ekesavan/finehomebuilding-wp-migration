<?php

class PublishContent
{

    /**
     * @var int $ContentID
     * @access public
     */
    public $ContentID = null;

    /**
     * @var int $FolderID
     * @access public
     */
    public $FolderID = null;

    /**
     * @var int $ContentLanguage
     * @access public
     */
    public $ContentLanguage = null;

    /**
     * @var string $DontCreateTask
     * @access public
     */
    public $DontCreateTask = null;

    /**
     * @var int $UserID
     * @access public
     */
    public $UserID = null;

    /**
     * @var string $TaskTitle
     * @access public
     */
    public $TaskTitle = null;

    /**
     * @param int $ContentID
     * @param int $FolderID
     * @param int $ContentLanguage
     * @param string $DontCreateTask
     * @param int $UserID
     * @param string $TaskTitle
     * @access public
     */
    public function __construct($ContentID, $FolderID, $ContentLanguage, $DontCreateTask, $UserID, $TaskTitle)
    {
      $this->ContentID = $ContentID;
      $this->FolderID = $FolderID;
      $this->ContentLanguage = $ContentLanguage;
      $this->DontCreateTask = $DontCreateTask;
      $this->UserID = $UserID;
      $this->TaskTitle = $TaskTitle;
    }

}
