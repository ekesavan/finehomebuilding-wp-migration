<?php

class GetContentResponse
{

    /**
     * @var ContentData $GetContentResult
     * @access public
     */
    public $GetContentResult = null;

    /**
     * @param ContentData $GetContentResult
     * @access public
     */
    public function __construct($GetContentResult)
    {
      $this->GetContentResult = $GetContentResult;
    }

}
