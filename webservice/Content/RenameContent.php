<?php

class RenameContent
{

    /**
     * @var int $ContentID
     * @access public
     */
    public $ContentID = null;

    /**
     * @var string $Title
     * @access public
     */
    public $Title = null;

    /**
     * @param int $ContentID
     * @param string $Title
     * @access public
     */
    public function __construct($ContentID, $Title)
    {
      $this->ContentID = $ContentID;
      $this->Title = $Title;
    }

}
