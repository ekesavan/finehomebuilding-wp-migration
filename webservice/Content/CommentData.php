<?php

class CommentData
{

    /**
     * @var int $CommentKeyId
     * @access public
     */
    public $CommentKeyId = null;

    /**
     * @var int $CommentId
     * @access public
     */
    public $CommentId = null;

    /**
     * @var int $RefId
     * @access public
     */
    public $RefId = null;

    /**
     * @var int $Language
     * @access public
     */
    public $Language = null;

    /**
     * @var string $CommentText
     * @access public
     */
    public $CommentText = null;

    /**
     * @var int $UserId
     * @access public
     */
    public $UserId = null;

    /**
     * @var string $FirstName
     * @access public
     */
    public $FirstName = null;

    /**
     * @var string $LastName
     * @access public
     */
    public $LastName = null;

    /**
     * @var string $DateModified
     * @access public
     */
    public $DateModified = null;

    /**
     * @var string $DateCreated
     * @access public
     */
    public $DateCreated = null;

    /**
     * @param int $CommentKeyId
     * @param int $CommentId
     * @param int $RefId
     * @param int $Language
     * @param string $CommentText
     * @param int $UserId
     * @param string $FirstName
     * @param string $LastName
     * @param string $DateModified
     * @param string $DateCreated
     * @access public
     */
    public function __construct($CommentKeyId, $CommentId, $RefId, $Language, $CommentText, $UserId, $FirstName, $LastName, $DateModified, $DateCreated)
    {
      $this->CommentKeyId = $CommentKeyId;
      $this->CommentId = $CommentId;
      $this->RefId = $RefId;
      $this->Language = $Language;
      $this->CommentText = $CommentText;
      $this->UserId = $UserId;
      $this->FirstName = $FirstName;
      $this->LastName = $LastName;
      $this->DateModified = $DateModified;
      $this->DateCreated = $DateCreated;
    }

}
