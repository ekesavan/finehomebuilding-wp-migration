<?php

class GetAllUnassignedItemApprovalsResponse
{

    /**
     * @var ApprovalData[] $GetAllUnassignedItemApprovalsResult
     * @access public
     */
    public $GetAllUnassignedItemApprovalsResult = null;

    /**
     * @param ApprovalData[] $GetAllUnassignedItemApprovalsResult
     * @access public
     */
    public function __construct($GetAllUnassignedItemApprovalsResult)
    {
      $this->GetAllUnassignedItemApprovalsResult = $GetAllUnassignedItemApprovalsResult;
    }

}
