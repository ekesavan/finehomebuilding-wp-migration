<?php

class GetAllTemplatesResponse
{

    /**
     * @var TemplateData[] $GetAllTemplatesResult
     * @access public
     */
    public $GetAllTemplatesResult = null;

    /**
     * @param TemplateData[] $GetAllTemplatesResult
     * @access public
     */
    public function __construct($GetAllTemplatesResult)
    {
      $this->GetAllTemplatesResult = $GetAllTemplatesResult;
    }

}
