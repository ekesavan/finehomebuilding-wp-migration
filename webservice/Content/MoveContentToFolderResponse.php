<?php

class MoveContentToFolderResponse
{

    /**
     * @var ContentData $MoveContentToFolderResult
     * @access public
     */
    public $MoveContentToFolderResult = null;

    /**
     * @param ContentData $MoveContentToFolderResult
     * @access public
     */
    public function __construct($MoveContentToFolderResult)
    {
      $this->MoveContentToFolderResult = $MoveContentToFolderResult;
    }

}
