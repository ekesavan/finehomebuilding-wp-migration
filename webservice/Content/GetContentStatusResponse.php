<?php

class GetContentStatusResponse
{

    /**
     * @var string $GetContentStatusResult
     * @access public
     */
    public $GetContentStatusResult = null;

    /**
     * @param string $GetContentStatusResult
     * @access public
     */
    public function __construct($GetContentStatusResult)
    {
      $this->GetContentStatusResult = $GetContentStatusResult;
    }

}
