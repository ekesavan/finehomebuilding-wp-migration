<?php

class GetChildContentResponse
{

    /**
     * @var ContentData[] $GetChildContentResult
     * @access public
     */
    public $GetChildContentResult = null;

    /**
     * @param ContentData[] $GetChildContentResult
     * @access public
     */
    public function __construct($GetChildContentResult)
    {
      $this->GetChildContentResult = $GetChildContentResult;
    }

}
