<?php

class AddContent_x0020_With_x0020_ContentIDResponse
{

    /**
     * @var int $AddContent_x0020_With_x0020_ContentIDResult
     * @access public
     */
    public $AddContent_x0020_With_x0020_ContentIDResult = null;

    /**
     * @param int $AddContent_x0020_With_x0020_ContentIDResult
     * @access public
     */
    public function __construct($AddContent_x0020_With_x0020_ContentIDResult)
    {
      $this->AddContent_x0020_With_x0020_ContentIDResult = $AddContent_x0020_With_x0020_ContentIDResult;
    }

}
