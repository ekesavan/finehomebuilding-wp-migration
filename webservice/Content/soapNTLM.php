<?php
class NTLMStream {
	private $path;
	private $mode;
	private $options;
	private $opened_path;
	private $buffer;
	private $pos;	
	/**	 * Open the stream 	
	*	 * @param unknown_type $path	 
	* @param unknown_type $mode	 
	* @param unknown_type $options	 
	* @param unknown_type $opened_path	 
	* @return unknown	 
	*/	
	public function stream_open($path, $mode, $options, $opened_path) {
		echo "[NTLMStream::stream_open] $path , mode=$mode \n";		
		$this->path = $path;		
		$this->mode = $mode;		
		$this->options = $options;		
		$this->opened_path = $opened_path;		
		$this->createBuffer($path);		
		return true;	
	}	
	/**	 * Close the stream	 *	 */	
	public function stream_close() {
		echo "[NTLMStream::stream_close] \n";
		curl_close($this->ch);	

	}

	/**	 * Read the stream	 *	
	* @param int $count number of bytes to read	 
	* @return content from pos to count	 */	

	public function stream_read($count) {

		echo "[NTLMStream::stream_read] $count \n";

		if(strlen($this->buffer) == 0) {
			return false;		
		}		
		$read = substr($this->buffer,$this->pos, $count);
		$this->pos += $count;
		return $read;
	}	

	/**	 * write the stream	 *	 
	* @param int $count number of bytes to read	 
	* @return content from pos to count	 */	
	public function stream_write($data) {		
		echo "[NTLMStream::stream_write] \n";		
		
		if(strlen($this->buffer) == 0) {			
			return false;		
		}		
		
		return true;	
	}	
	
	/**	 *	 * @return true if eof else false	 */	
		
	public function stream_eof() {
		echo "[NTLMStream::stream_eof] ";
		if($this->pos > strlen($this->buffer)) {
			echo "true \n";
			return true;		
		}		
		echo "false \n";
		return false;	
	}	
	
	/**	 * @return int the position of the current read pointer	 */	
	public function stream_tell() {
		echo "[NTLMStream::stream_tell] \n";
		return $this->pos;
	}
	
	/**	 * Flush stream data	 */	
	public function stream_flush() {
		echo "[NTLMStream::stream_flush] \n";
		$this->buffer = null;
		$this->pos = null;	
	}	
	/**	 * Stat the file, return only the size of the buffer	 
	*	 * @return array stat information	 */	

	public function stream_stat() {
		echo "[NTLMStream::stream_stat] \n";
		$this->createBuffer($this->path);
		$stat = array(			
				'size' => strlen($this->buffer),		
			);		
		return $stat;	
	}	
	
	/**	 * Stat the url, return only the size of the buffer	 *	 
	* @return array stat information	 */	

	public function url_stat($path, $flags) {
		echo "[NTLMStream::url_stat] \n";
		$this->createBuffer($path);
		$stat = array(	
				'size' => strlen($this->buffer),
			);
		return $stat;	
	}
	/**	 * Create the buffer by requesting the url through cURL	 *
	 * @param unknown_type $path	 */	
	private function createBuffer($path) {
		if($this->buffer) {
			return;		
		}		
		echo "[NTLMStream::createBuffer] create buffer from : $path\n";
		$this->ch = curl_init($path);
		curl_setopt($this->ch, CURLOPT_CONNECTTIMEOUT, SOAP_TIMEOUT );
		curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($this->ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
		if( $this->user && $this->password ){
			curl_setopt($this->ch, CURLOPT_HTTPAUTH, CURLAUTH_NTLM);
			curl_setopt($this->ch, CURLOPT_USERPWD, $this->user.':'.$this->password);
		}
		echo $this->buffer = curl_exec($this->ch);
		if( SOAP_DEBUG ){
			$result = curl_getinfo($this->ch);
			print_r( $result );
		}
		echo "[NTLMStream::createBuffer] buffer size : ".strlen($this->buffer)."bytes\n";
		$this->pos = 0;	
	}
}


class MyServiceProviderNTLMStream extends NTLMStream {	
	protected $user = SHAREPOINT_USERNAME;	
	protected $password = SHAREPOINT_PASSWORD;
}


class NTLMSoapClient extends SoapClient {

	public $location;
	
	public $user;
	
	public $password;
	
	function __doRequest($request, $location, $action, $version, $one_way = NULL) {
		$headers = array(
		'Method: POST',
		'Connection: Keep-Alive',
		'User-Agent: PHP-SOAP-CURL',
		'Content-Type: text/xml; charset=utf-8',
		'SOAPAction: "'.$action.'"',
		);		
		$this->__last_request_headers = $headers;
		if( isset( $this->location ) && !empty( $this->location ))
			$location = $this->location;
			
		$ch = curl_init($location);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, SOAP_TIMEOUT );
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_POST, true );
		curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
		curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
		if( $this->user && $this->password ){
			curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_NTLM);
			curl_setopt($ch, CURLOPT_USERPWD, $this->user.':'.$this->password);
			print "======================>".$this->user.':'.$this->password."\n";
		}		
		$response = curl_exec($ch);
		if( empty( $response ) && SOAP_RETRY ){
			$response = curl_exec($ch);
		}  		

		if( SOAP_DEBUG ){
			$result = curl_getinfo($ch);
			print_r( $result );
		}
		$this->__last_request_response = $response;

		if( PRINT_SOAP_XML ) {
		   print_R( $headers );
			echo "----------------------------SOAP REQUEST-------------------------------------\n";
			echo "$request\n";
			echo "--------------------------- SOAP Response------------------------------------\n";
			echo "$response\n";
		}

		return $response;	
	}		

	function __getLastRequestHeaders() {		
		return implode("\n", $this->__last_request_headers)."\n";	
	}

	function __getLastResponse() {
		return $this->__last_request_response;
	}
}

// Authentification parameterclass 
class MyServiceNTLMSoapClient extends NTLMSoapClient {	
	//protected $user = SHAREPOINT_USERNAME;	
	//protected $password = SHAREPOINT_PASSWORD;
}

class SharePointDWS {

	private $client;
	public 	$log;
	public 	$createfolder_success_count = 0;
	public 	$createfolder_error_count = 0;
	public 	$createfolder_exception_count = 0;
   
	function __construct( $wsdl ) {

		$this->client =  new MyServiceNTLMSoapClient( $wsdl );
	}
		
	public function GetDwsData ( $document ) {
			
		try {
			$result = $this->client->GetDwsData(
								array(
									'document' => $document,
									)
								);

			return $result;

		}catch ( Exception $e ) {
			$error = "GetDwsData document: $document Exception: ".$e->faultstring."\n";
			if( isset( $e->detail ) ){
				if( isset( $e->detail->errorcode ) )
					$error .= "errorcode:".$e->detail->errorcode;
				if( isset( $e->detail->errorstring ))
					$error .= "  errorstring:".$e->detail->errorstring."\n";

			}
			echo $error;
		}
	}

         /*
        WSDL : https://portal.sbmedia.com/contentrepository/_vti_bin/DWS.asmx
        The CreateFolder method of the Document Workspace service
        creates a subfolder in a document library of the current Document Workspace site

        Parameters:
        url:  String. The proposed site-based URL of the folder to create;
        for example, "Shared Documents/folder_name."

        Return Value
        String Returns an empty <Result/> when successful.
        */

        public function createFolder( $folder ) {

                try {
                        $result = $this->client->CreateFolder(
                                                        array(
                                                                'url' => $folder,
                                                                )
                                                        );
						
                        if( isset( $result ) && isset( $result->CreateFolderResult ) ){

				 if ( preg_match( '|<Result/>|',$result->CreateFolderResult ) ){
					++$this->createfolder_success_count;				
				 }else{
			  	   	$error =  "CreateFolder url: $folder CreateFolderResult: ".$result->CreateFolderResult."\n";
					//if( SOAP_DEBUG )
					echo $error;

					if ( !preg_match( '|AlreadyExists|i', $result->CreateFolderResult ) ){
						$this->log .= $error;
						++$this->createfolder_error_count;
					}
				 }	
				
			}
			
			unset( $result );

                }catch ( Exception $e ) {
			++$this->createfolder_exception_count;
                        $error = "CreateFolder url: $folder Exception: ".$e->faultstring."\n";
			if( isset( $e->detail ) ){
                                if( isset( $e->detail->errorcode ) )
                                        $error .= "errorcode:".$e->detail->errorcode;
                                if( isset( $e->detail->errorstring ))
                                        $error .= "  errorstring:".$e->detail->errorstring."\n";

                        }
			$this->log .= $error;
			echo $error;
                }

        }
}

class SharePointCopy {

	public  $log;
        private $client;
        private $domain;
        private $SourceUrl;
        private $field_information = array();
        private $list_properties = array();
	public  $copyitem_success_count = 0;
        public  $copyitem_error_count = 0;
        public  $copyitem_exception_count = 0;

        function __construct( $wsdl ) {

                $this->client =  new MyServiceNTLMSoapClient( $wsdl );
        }

	public function setSourceUrl( $SourceUrl ) {

		$this->SourceUrl = $SourceUrl;
	}

	public function listProperties ( $list_properties ) {

		$this->list_properties = $list_properties;
	}

	public function GetItem( $url ) {
		$result = '';
		try {
                        $result = $this->client->GetItem(
                                                        array(
                                                                'Url' => $url,
                                                                )
                                                        );

			
                        if( SOAP_DEBUG ) print_r( $result );
                        if( isset($result->GetItemResult) && $result->GetItemResult == 0 )
                        	return false;

		}catch ( Exception $e ) {
                        $error = "GetItem Url: $url Exception: ".$e->faultstring."\n";
                        if( isset( $e->detail ) ){
                                if( isset( $e->detail->errorcode ) )
                                        $error .= "errorcode:".$e->detail->errorcode;
                                if( isset( $e->detail->errorstring ))
                                        $error .= "  errorstring:".$e->detail->errorstring."\n";

                        }
                        echo $error;
                }
		return false;
        }
	
        public function GetItemID( $url ) {
		$result = '';
		try {
                        $result = $this->client->GetItem(
                                                        array(
                                                                'Url' => $url,
                                                                )
                                                        );

			
                        if( SOAP_DEBUG ) print_r( $result );
                        if( isset($result->GetItemResult) && $result->GetItemResult == 0 ){
                        	if( isset( $result->Fields ) && isset( $result->Fields->FieldInformation )  ){
	                        	$fields = $result->Fields->FieldInformation;
	                        	foreach( $fields as $field ){
	                        		$f = (array) $field;
	                        		if( $f['InternalName'] == "ID" )
	                        			return $f['Value'];
	                        	}
                        	}
                        }
                        return false;

		}catch ( Exception $e ) {
                        $error = "GetItem Url: $url Exception: ".$e->faultstring."\n";
                        if( isset( $e->detail ) ){
                                if( isset( $e->detail->errorcode ) )
                                        $error .= "errorcode:".$e->detail->errorcode;
                                if( isset( $e->detail->errorstring ))
                                        $error .= "  errorstring:".$e->detail->errorstring."\n";

                        }
                        echo $error;
                }
		return false;
        }

        public function setFieldInformationNITFXML( $nitf_xml ){
                if( isset( $nitf_xml ) ){
                        $nitfobj = simplexml_load_string( $nitf_xml );

                        if( isset( $nitfobj ) ){
                                if( isset( $nitfobj->head->tobject ) ){
                                        $tobject = (array) $nitfobj->head->tobject;
                                        if( isset( $tobject['tobject.property'] ) ){
                                                $this->field_information['section'] = $tobject['tobject.property']->attributes();
                                        }else{
                                                $this->field_information['section'] = "News";
                                        }
                                }

                                $this->field_information['title']  = (string) $nitfobj->head->title;

                                $docdata = (array) $nitfobj->head->docdata;
                                $body = (array)$nitfobj->body;
                                $head = (array) $body['body.head'];

                                $date = $docdata['date.release']->attributes();
                                preg_match( '/^(\d{4})(\d{2})(\d{2})/', $date['norm'], $matches );
                                $y = $matches[1];$m =  $matches[2];$d = $matches[3];
                                $date = join( "/", array($m, $d, $y ) );
                                $this->field_information['year'] = $y;
                                $this->field_information['date'] = $date;
								$this->field_information['month'] = $m;
								// set key words
                                $keywords = '';
                                if( isset( $tobject['tobject.subject'] ) && is_array( $tobject['tobject.subject'] ) ){
                                        foreach( $tobject['tobject.subject'] as $type ){
                                                $obj = $type->attributes();
                                                $keywords = $keywords ." " . (string)$obj['tobject.subject.type'];
                                        }
                                }else{
                                        if( isset( $tobject['tobject.subject'] ) ){
                                                $obj = $tobject['tobject.subject']->attributes();
                                                $keywords = (string)$obj['tobject.subject.type'];
                                        }else{
                                                $keywords = "EMPTY";
                                        }
                                }
                                $this->field_information['keywords'] = $keywords;
                                if ( isset( $head['byline'] ) && !empty( $head['byline'] ) )
                                	$this->field_information['author'] = $head['byline'];
                                else
                               		$this->field_information['author'] = "Staff Writer";

                                if( isset( $head['abstract']->p ) ){
                                        $abstractobj = (array) $head['abstract']->p;
                                        $abstract = $abstractobj[0];
                                }else{
                                        $abstract = $head['abstract'];
                                }
                                $this->field_information['abstract'] = $abstract;
                        }
			unset( $nitfobj );
                }else{

                        echo "NITF XML is empty\n";
                }
        }


	public function setFieldInformationNITF( $article ){

		$this->field_information['section'] = $article['section'];
		$this->field_information['title']  = $article['title'];
		$date = nitf_dateformat($article['publish_date']);
		preg_match( '/^(\d{4})(\d{2})(\d{2})/', $date, $matches );
		$y = $matches[1];$m =  $matches[2];$d = $matches[3];
		$date = join( "/", array($m, $d, $y ) );
		$this->field_information['year'] = $y;
		$this->field_information['date'] = $date;
		$this->field_information['month'] = $m;
		// set key words
		$keywords = '';
		foreach( $article['nitfrefnums'] as $nitfrefnum => $nitftype ){
			$keywords = $keywords ." " .$nitftype;
		}
		$this->field_information['keywords'] = $keywords;
		if( !empty( $article['byline'] ) )
			$this->field_information['author'] = $article['byline'];
		else 	
			$this->field_information['author'] = "Staff Writer";
		$this->field_information['abstract'] = $article['abstract'];
        }

        /*

        WSDL : https://portal.sbmedia.com/contentrepository/_vti_bin/Copy.asmx

        Copies a document represented by a Byte array to one or more locations on a server.

        SourceUrl : A String that contains the absolute source URL of the document to be copied.
        DestinationUrls : An array of Strings that contain one or more absolute URLs specifying the destination location or locations of the copied document.
        Fields : An array of FieldInformation objects that define and optionally assign values to one or more fields associated with the copied document.
        Stream : An array of Bytes that contain the document to copy using base-64 encoding.

        Results : An array of CopyResult objects, passed as an out parameter.

        */

        public function copyNITF( $nitf_xml, $DestinationUrls, $site ) {
		$FieldInformation = array();
                $FieldInformation['Web_x0020_Domain_x0020_Name'] = $site;
                $FieldInformation['Article_x0020_Section'] = $this->field_information['section'];
                $FieldInformation['Month'] = $GLOBALS['MONTH_TO_TEXT'][$this->field_information['month']];
                $FieldInformation['Year'] = $this->field_information['year'];
                $FieldInformation['Article_x0020_Title'] = $this->field_information['title'];
                $FieldInformation['Author0'] = $this->field_information['author'];
                $FieldInformation['Article_x0020_Date'] = $this->field_information['date'];
                $FieldInformation['Article_x0020_Abstract'] = $this->field_information['abstract'];
                $FieldInformation['Article_x0020_Keywords'] = $this->field_information['keywords'];

                $Fields = array();
                foreach( $this->list_properties as $field ) {
                        array_push( $Fields, new SoapVar('<ns1:FieldInformation Type="'.$field['Type'].'"  DisplayName="'.$field['DisplayName'].'" InternalName="'.$field['InternalName'].'" Id="'.$field['ID'].'" Value="'.htmlentities( $FieldInformation[$field['InternalName']], ENT_QUOTES, 'UTF-8' ).'"/>',XSD_ANYXML) );
                }

                $result = '';
		$copyresult= 0;
                try{
                        $result = $this->client->CopyIntoItems(
                                                array(
                                                        'SourceUrl' => $this->SourceUrl,
                                                        'DestinationUrls' => array( $DestinationUrls ),
                                                        'Stream' => $nitf_xml,
                                                        'Fields' => $Fields,
                                                        )
                                                );

                        if( isset($result) && isset($result->Results) && $result->Results->CopyResult->ErrorCode == "Success" ) {
				if( SOAP_DEBUG )
					echo "CopyIntoItems".$DestinationUrls." = Success\n";
				$copyresult = 1;
				++$this->copyitem_success_count;
                        }else{
				$error =  "CopyIntoItems DestinationUrls: ".$DestinationUrls."\n";
				$error .= "CopyIntoItems ErrorCode: ". $result->Results->CopyResult->ErrorCode ." ErrorMessage: ".$result->Results->CopyResult->ErrorMessage. "\n";
				if( preg_match( '/is not checked out/', $result->Results->CopyResult->ErrorMessage ) ){
					$copyresult = -1;
				}else{
					 ++$this->copyitem_error_count;
					 $this->log .= $error;
				}

				if( SOAP_DEBUG )
					echo $error;
			}

                }catch ( Exception $e ) {
                        ++$this->copyitem_exception_count; 
			if( SOAP_DEBUG )
				print_r( $FieldInformation );
									
			 $error = "CopyIntoItems DestinationUrls: $DestinationUrls, Exception: ".$e->faultstring."\n";
			 if( isset( $e->detail ) ){
				if( isset( $e->detail->errorcode ) )
					$error .= "errorcode:".$e->detail->errorcode;
		
				if( isset( $e->detail->errorstring )){
					$error .= "  errorstring:".$e->detail->errorstring."\n";
					 if( SHAREPOINT_TCPERROR_SLEEP && preg_match( '/transport-level error|general network error|communication link failure/i', $e->detail->errorstring ) )
                                                sleep( SHAREPOINT_TCPERROR_SLEEP );

				}

			}
			
			if( SOAP_DEBUG ){
				 echo $error;
				 $this->log .= $error;
			}
                }
                unset( $FieldInformation );
                unset( $result );
		return $copyresult;
        }
		
}


class SharePointList {

	public 	$log;
	private $wsdl;
	public  $listName;
	public  $ViewFieldsXML;
	public  $listNames = array();
	private $client;
	public  $list_properties = array();
	public  $checkin_success_count = 0;
	public  $checkin_error_count = 0;
	public  $checkin_exception_count = 0;
	public  $checkout_success_count = 0;
        public  $checkout_error_count = 0;
        public  $checkout_exception_count = 0;

        function __construct( $wsdl ) {

                $this->client =  new MyServiceNTLMSoapClient( $wsdl );
        }
        
	function setlocation ( $location ){
		$this->client->location = $location; 
		
	}

        /*
        WSDL : https://portal.sbmedia.com/contentrepository/_vti_bin/Lists.asmx
        The GetListCollection method of the Lists service returns the names and GUIDs for all the lists in the site.
        Return Value
        A fragment in Collaborative Application Markup Language (CAML) in the following form that contains the names and GUIDs for the lists
        <Lists xmlns="http://schemas.microsoft.com/sharepoint/soap/">
                <List DocTemplateUrl="" DefaultViewUrl="/contentrepository/NITF Documents/Forms/AllItems.aspx" MobileDefaultViewUrl="" ID="{9DE8EFB5-993D-41D4-84F9-AF897A39C883}" Title="NITF Documents" Description="" ImageUrl="/_layouts/images/itdl.gif" Name="{9DE8EFB5-993D-41D4-84F9-AF897A39C883}" ..... />
        */
	public function SetListName( $str ) {
		if( isset($this->listNames) ){
			foreach( $this->listNames as  $guid => $viewurl ) {
				if( preg_match( '/'.$str.'/i', $viewurl ) ){
					$this->listName = $guid;
					break;
				}	
			}		
		}
	}
	
        public function GetListCollection(){
        	
        	if( !empty( $this->listNames ) )
			$this->listNames = array();

                try {
                        $result = $this->client->GetListCollection();
                }catch ( Exception $e ) {
                        $error = "GetListCollection, Exception: ".$e->faultstring."\n";
			if( isset( $e->detail ) ){
                                if( isset( $e->detail->errorcode ) )
                                        $error .= "errorcode:".$e->detail->errorcode;
                                if( isset( $e->detail->errorstring ))
                                        $error .= "  errorstring:".$e->detail->errorstring."\n";
			}
			$this->log .= $error;
			echo $error;
                }

                if( isset( $result ) ) {

                        if( $result && isset( $result->GetListCollectionResult->any ) ) {
                                libxml_use_internal_errors(true);
                                $listdata = simplexml_load_string( $result->GetListCollectionResult->any );
                                unset( $result );
                                if ( !$listdata ) {
                                    echo "Failed loading XML\n";
                                    foreach(libxml_get_errors() as $error) {
                                        echo "\t", $error->message;
                                    }
                                }

                                foreach( $listdata as $list ){
					$obj = (array) $list->attributes();
					$objarray = $obj ['@attributes'];
					$this->listNames[$objarray['Name'] ] = $objarray['DefaultViewUrl'];
                                }
								
				unset( $listdata );
                        }
                }
        }

        /*
                WSDL : https://portal.sbmedia.com/contentrepository/_vti_bin/Lists.asmx
                The Lists element contains the collection of lists for a configuration.
                Get Document Properties  InetrnalName, DisplayName, Type,
                Id it will be used for CopyItems FieldInformation
        */

        public function GetListAndView(){
                $result = "";
		if( !empty( $this->list_properties ) )
			$this->list_properties = array();
						
                try {

                        $result = $this->client->GetListAndView( 
								array( 
										'listName' => $this->listName 
									)																			
								);

                 }catch ( Exception $e ) {
			                         
                        $error = "GetListAndView Exception: ".$e->faultstring."\n";
			if( isset( $e->detail ) ){
				if( isset( $e->detail->errorcode ) )
					$error .= "errorcode:".$e->detail->errorcode;
				if( isset( $e->detail->errorstring ) )
					$error .= "  errorstring:".$e->detail->errorstring."\n";

			}
			$this->log .= $error;
			echo $error;
		}

                if( isset( $result ) && isset( $result->GetListAndViewResult->any ) ){
                        libxml_use_internal_errors(true);

                        $listdata = simplexml_load_string( $result->GetListAndViewResult->any);
						
                        unset( $result );
                        if( isset( $listdata ) ) {
                        
				foreach(  $listdata[0]->List->Fields->Field as $field){
					
              							
					$obj = $field->attributes();

					if( isset( $GLOBALS['DOCUMENT_PROPERTY_NAMES'] ) ){
										
						if( in_array( (string) $obj['Name'], $GLOBALS['DOCUMENT_PROPERTY_NAMES'] ) ){
							array_push( $this->list_properties, array(
																	'InternalName' => (string) $obj['Name'],
																	'DisplayName' => (string) $obj['DisplayName'],
																	'ID' => (string) $obj['ID'],
																	'Type' => (string) $obj['Type'][0],
																	)
							);
						}
										
						}else{
								array_push( $this->list_properties, array(
																		'InternalName' => (string) $obj['Name'],
																		'DisplayName' => (string) $obj['DisplayName'],
																		'ID' => (string) $obj['ID'],
																		'Type' => (string) $obj['Type'][0],
																		)
										);
						}
				}
                        }
                        unset( $listdata );
                }else{
                        echo "Failed loading XML\n";
                        foreach(libxml_get_errors() as $error) {
				echo "\t" . $error->message;
                        }
                }
			
        }

        /*
	CheckInFile
	Allows documents to be checked in to a SharePoint document library remotely. 
        pageUrl : A string that contains the full path to the document to check in.
        comment : A string containing optional check-in comments
        CheckinType : A string representation of the values 0, 1 or 2, where
        0 = MinorCheckIn,
        1 = MajorCheckIn,
        2 = OverwriteCheckIn.
        */

        public function CheckInFile( $DestinationUrls ){

                try {
                        $result = $this->client->CheckInFile( array(
                                                                'pageUrl' => $DestinationUrls,
                                                                'comment' => 'NITF',
                                                                'CheckinType' => '1',
                                                                )

                                                        );
			 if( isset( $result ) && isset( $result->CheckInFileResult ) && $result->CheckInFileResult == 1 ){
				++$this->checkin_success_count;
				return true;
			 }else{
				++$this->checkin_error_count;
				if( SOAP_DEBUG )
					print_r( $result );
			 }
				
                         unset( $result );

                }catch ( Exception $e ) {
			++$this->checkin_exception_count;
                        $error = "CheckInFile pageUrl: $DestinationUrls Exception: ".$e->faultstring."\n";
			if( isset( $e->detail ) ){	
				if( isset( $e->detail->errorcode ) )
					$error .= "errorcode:".$e->detail->errorcode;

				if( isset( $e->detail->errorstring )){
					$error .= "  errorstring:".$e->detail->errorstring."\n";
					if( SHAREPOINT_TCPERROR_SLEEP && preg_match( '/transport-level error|general network error|communication link failure/i', $e->detail->errorstring ) )
						sleep( SHAREPOINT_TCPERROR_SLEEP );
				}
			}
			if( SOAP_DEBUG )
				$this->log .= $error;
			echo $error;
                }

        }

	/*
	CheckOutFile:
	Allows documents in a SharePoint document library to be checked out remotely
	Parameters
	pageUrl : A string that contains the full path to the document to be checked out.
	checkoutToLocal : A string containing "true" or "false" that designates whether the file is to be flagged as checked out for offline editing.
	lastmodified : A string in RFC 1123 date format representing the date and time of the last modification to the file; for example, "20 Jun 1982 12:00:00 GMT".
	Return Value
	true if the operation succeeded; otherwise, false. 
	*/

	public function CheckOutFile( $DestinationUrls ){

                try {
                        $result = $this->client->CheckOutFile( array(
                                                                'pageUrl' => $DestinationUrls,
                                                                'checkoutToLocal' => 'true',
                                                                'lastmodified' => date('d M Y H:i:s e'),
                                                                )

                                                        );
			 if( isset( $result ) && isset( $result->CheckOutFileResult ) && $result->CheckOutFileResult == 1 ){
				++$this->checkout_success_count;
				return true;
                         }else{
				++$this->checkout_error_count;
				 if( SOAP_DEBUG )
					print_r( $result );
                         }

                         unset( $result );

                }catch ( Exception $e ) {
			++$this->checkout_exception_count;
                        $error = "CheckOutFile pageUrl: $DestinationUrls Exception: ".$e->faultstring."\n";
			if( isset( $e->detail ) ){
                                if( isset( $e->detail->errorcode ) )
                                        $error .= "errorcode:".$e->detail->errorcode;
                                if( isset( $e->detail->errorstring )) {
                                        $error .= "  errorstring:".$e->detail->errorstring."\n";
					 if( SHAREPOINT_TCPERROR_SLEEP && preg_match( '/transport-level error|general network error|communication link failure/i', $e->detail->errorstring ) )
                                                sleep( SHAREPOINT_TCPERROR_SLEEP );
				}

                        }
                        if( SOAP_DEBUG )
				$this->log .= $error;
			echo $error;
                }

        }
	
	public function GetListItems( $params ){

		$tags = array();
		$tags['listName'] = $this->listName;
			
		if ( isset( $params['rowLimit'] ) )
			$tags['rowLimit'] = $params['rowLimit'] ;
						
		if( isset( $params['queryOptions'] ) ){
				
			$queryOptions = '<ns1:queryOptions><QueryOptions>';
					
			if( isset( $params['queryOptions']['ViewAttributes'] ) ){
				$queryOptions .= '<ViewAttributes ';
				foreach ( $params['queryOptions']['ViewAttributes'] as $key => $value ){
					$queryOptions .= $key.'="'.$value.'" '; 						
				}
				$queryOptions .= '/>';
						
			}
					
			if( isset( $params['queryOptions']['Paging'] ) ){
				$queryOptions .= '<Paging ';
				foreach ( $params['queryOptions']['Paging'] as $key => $value ){
					$queryOptions .= $key.'="'.htmlentities( $value, ENT_QUOTES, 'UTF-8' ) .'" '; 						
				}
				$queryOptions .= ' />';						
			}
					
				
			$queryOptions .= '</QueryOptions></ns1:queryOptions>';
					
			$tags['queryOptions'] = new SoapVar( $queryOptions, XSD_ANYXML );
		}
		
		if( isset( $this->ViewFieldsXML ) ) {
			$viewFields = '<ns1:viewFields><ViewFields>'.$this->ViewFieldsXML.'</ViewFields></ns1:viewFields>';
			$tags['viewFields'] = new SoapVar( $viewFields, XSD_ANYXML );
		}
                try {
                        $result = $this->client->GetListItems( $tags ) ; 
			return $result;
             
		}catch ( Exception $e ) {
	                         
            		$error = "GetListItems Exception: ".$e->faultstring."\n";
			if( isset( $e->detail ) ){
				if( isset( $e->detail->errorcode ) )
					$error .= "errorcode:".$e->detail->errorcode;
				if( isset( $e->detail->errorstring ) )
					$error .= "  errorstring:".$e->detail->errorstring."\n";

			}
			$this->log .= $error;
			echo $error;
                }
               
        }

	public function setViewFields(){
		$fieldsxml = '';
		if( isset($this->list_properties) ){			
			foreach( $this->list_properties as $field ){
					
				$fieldsxml .= '<FieldRef Name="'.$field['InternalName'].'" />';
			}
		}
		$this->ViewFieldsXML = $fieldsxml;
	}
	
	public function deleteItem( $id, $ref ){
		$updates = '<ns1:updates><Batch OnError="Continue" ListVersion="1"><Method ID="1" Cmd="Delete"><Field Name="ID">'.$id.'</Field><Field Name="FileRef">'.$ref.'</Field></Method></Batch></ns1:updates>';			
		try {
	                      
			$result = $this->client->UpdateListItems ( 
								array(
	                                                               "listName" => $this->listName,
	                                                                "updates" => new SoapVar( $updates, XSD_ANYXML ),
	                                                                )
	                                                       );
	                if( preg_match( '|<ErrorCode>0x00000000</ErrorCode>|', $result->UpdateListItemsResult->any ) ){
	                	return true;
	                }else{
	                	return false;
	                }                                     
			
                }catch ( Exception $e ) {
			
			$error = "UpdateListItems Exception: ".$e->faultstring."\n";
			if( isset( $e->detail ) ){
				if( isset( $e->detail->errorcode ) )
					$error .= "errorcode:".$e->detail->errorcode;
				if( isset( $e->detail->errorstring ) )
					$error .= "  errorstring:".$e->detail->errorstring."\n";

			}
			if( SOAP_DEBUG )
				$this->log .= $error;
			echo $error;
			return false;
                }
		
	}
	
	public function updateItem( $id, $ref, $data ){
		$updates = '<ns1:updates><Batch OnError="Continue" ListVersion="1">';
		$updates .= '<Method ID="1" Cmd="Update">';
		$updates .= '<Field Name="ID">'.$id.'</Field>';
		$updates .= '<Field Name="FileRef">'.$ref.'</Field>';
		foreach( $data as $key => $value )
			$updates .= '<Field Name="'.$key.'">'.$value.'</Field>';
		$updates .= '</Method></Batch></ns1:updates>';			
		try {
	                      
			$result = $this->client->UpdateListItems ( 
								array(
	                                                               "listName" => $this->listName,
	                                                                "updates" => new SoapVar( $updates, XSD_ANYXML ),
	                                                                )
	                                                       );
	                print_r( $result );
	                if( preg_match( '|<ErrorCode>0x00000000</ErrorCode>|', $result->UpdateListItemsResult->any ) ){
	                	return true;
	                }else{
	                	return false;
	                }                                     
			
                }catch ( Exception $e ) {
			
			$error = "UpdateListItems Exception: ".$e->faultstring."\n";
			if( isset( $e->detail ) ){
				if( isset( $e->detail->errorcode ) )
					$error .= "errorcode:".$e->detail->errorcode;
				if( isset( $e->detail->errorstring ) )
					$error .= "  errorstring:".$e->detail->errorstring."\n";

			}
			if( SOAP_DEBUG )
				$this->log .= $error;
			echo $error;
			return false;
                }
		
	}
}

class SharepointWebs {

	private $client;
	
	function __construct( $wsdl ) {
		$this->client =  new MyServiceNTLMSoapClient( $wsdl );
	}

	function setlocation ( $location ){
		$this->client->location = $location; 
		
	}
			
	function GetWebCollection(){
			
		$siteUrls =  array();
			
		try {
			$result = $this->client->GetWebCollection();
			if( isset( $result) && isset( $result->GetWebCollectionResult ) ){
				$sitedata = simplexml_load_string( $result->GetWebCollectionResult->any );
				if( isset( $sitedata ) ) {
					foreach(  $sitedata->Web as $web ){
						$obj = $web->attributes();
						$siteUrls[ (string) $obj['Title'] ] = (string)$obj['Url'];
					}		
				}
			}
					
		}catch ( Exception $e ) {
		                        
			$error = "GetWebCollection Exception: ".$e->faultstring."\n";
			if( isset( $e->detail ) ){
				if( isset( $e->detail->errorcode ) )
					$error .= "errorcode:".$e->detail->errorcode;
				if( isset( $e->detail->errorstring ) )
					$error .= "  errorstring:".$e->detail->errorstring."\n";

			}
			$this->log .= $error;
			echo $error;
		}	
				
		return $siteUrls;
	}
		
	function GetAllSubWebCollection(){
		
		$siteUrls =  array();
			
		try {
				
			$result = $this->client->GetAllSubWebCollection();
			
			if( isset( $result) && isset( $result->GetWebCollectionResult ) ){
				$sitedata = simplexml_load_string( $result->GetWebCollectionResult->any );
				if( isset( $sitedata ) ) {
					foreach(  $sitedata->Web as $web ){
						$obj = $web->attributes();
						$siteUrls[ (string) $obj['Title'] ] = (string)$obj['Url'];
					}		
				}
			}	
				
	        }catch ( Exception $e ) {
	                        
			$error = "GetAllSubWebCollection Exception: ".$e->faultstring."\n";
			if( isset( $e->detail ) ){
				if( isset( $e->detail->errorcode ) )
					$error .= "errorcode:".$e->detail->errorcode;
				if( isset( $e->detail->errorstring ) )
					$error .= "  errorstring:".$e->detail->errorstring."\n";

			}
			$this->log .= $error;
			echo $error;
		}	
			
		return $siteUrls;
	}
} 

class SharepointWebParts {

	private $client;
	
	function __construct( $wsdl ) {
		$this->client =  new MyServiceNTLMSoapClient( $wsdl );
	}

	function setlocation ( $location ){
		$this->client->location = $location; 
		
	}
			
	function GetWebPartProperties( $pageUrl ){
		
		try {	
		$result = $this->client->GetWebPartProperties(
								array( 
									"pageUrl" => $pageUrl,							 
								)
								);									
			
			
			print_r( $result );
					
		}catch ( Exception $e ) {
		                        
			$error = "GetWebPartProperties Exception: ".$e->faultstring."\n";
			if( isset( $e->detail ) ){
				if( isset( $e->detail->errorcode ) )
					$error .= "errorcode:".$e->detail->errorcode;
				if( isset( $e->detail->errorstring ) )
					$error .= "  errorstring:".$e->detail->errorstring."\n";

			}
			$this->log .= $error;
			echo $error;
		}	
	}
	
}


class SharePointVersion {
	
	private $client;
	
	function __construct( $wsdl ) {
		$this->client =  new MyServiceNTLMSoapClient( $wsdl );
	}
	
	function GetVersions( $filename ){
		try {
	
			$result = $this->client->GetVersions(
								array( 
									"fileName" => $filename
								)
								);									
			
			 if( SOAP_DEBUG )
				print_r( $result );
			
			if( isset( $result) && isset( $result->GetVersionsResult ) ){
				$version = simplexml_load_string( $result->GetVersionsResult->any );
				$obj = (array) $version->result[0]->attributes();
				$objarray = $obj ['@attributes'];
				return preg_replace( '/@/','',$objarray['version']);
			}
					
		}catch ( Exception $e ) {
		                        
			$error = "GetVersions Exception: ".$e->faultstring."\n";
			if( isset( $e->detail ) ){
				if( isset( $e->detail->errorcode ) )
					$error .= "errorcode:".$e->detail->errorcode;
				if( isset( $e->detail->errorstring ) )
					$error .= "  errorstring:".$e->detail->errorstring."\n";

			}
			$this->log .= $error;
			echo $error;
		}	
	}
}
?>
