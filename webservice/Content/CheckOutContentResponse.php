<?php

class CheckOutContentResponse
{

    /**
     * @var boolean $CheckOutContentResult
     * @access public
     */
    public $CheckOutContentResult = null;

    /**
     * @param boolean $CheckOutContentResult
     * @access public
     */
    public function __construct($CheckOutContentResult)
    {
      $this->CheckOutContentResult = $CheckOutContentResult;
    }

}
