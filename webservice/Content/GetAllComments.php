<?php

class GetAllComments
{

    /**
     * @var int $KeyID
     * @access public
     */
    public $KeyID = null;

    /**
     * @var int $CommentID
     * @access public
     */
    public $CommentID = null;

    /**
     * @var int $RefID
     * @access public
     */
    public $RefID = null;

    /**
     * @var string $RefType
     * @access public
     */
    public $RefType = null;

    /**
     * @var int $UserID
     * @access public
     */
    public $UserID = null;

    /**
     * @var string $OrderBy
     * @access public
     */
    public $OrderBy = null;

    /**
     * @param int $KeyID
     * @param int $CommentID
     * @param int $RefID
     * @param string $RefType
     * @param int $UserID
     * @param string $OrderBy
     * @access public
     */
    public function __construct($KeyID, $CommentID, $RefID, $RefType, $UserID, $OrderBy)
    {
      $this->KeyID = $KeyID;
      $this->CommentID = $CommentID;
      $this->RefID = $RefID;
      $this->RefType = $RefType;
      $this->UserID = $UserID;
      $this->OrderBy = $OrderBy;
    }

}
