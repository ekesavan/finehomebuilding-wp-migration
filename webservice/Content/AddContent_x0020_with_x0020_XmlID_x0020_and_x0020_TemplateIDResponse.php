<?php

class AddContent_x0020_with_x0020_XmlID_x0020_and_x0020_TemplateIDResponse
{

    /**
     * @var int $AddContent_x0020_with_x0020_XmlID_x0020_and_x0020_TemplateIDResult
     * @access public
     */
    public $AddContent_x0020_with_x0020_XmlID_x0020_and_x0020_TemplateIDResult = null;

    /**
     * @param int $AddContent_x0020_with_x0020_XmlID_x0020_and_x0020_TemplateIDResult
     * @access public
     */
    public function __construct($AddContent_x0020_with_x0020_XmlID_x0020_and_x0020_TemplateIDResult)
    {
      $this->AddContent_x0020_with_x0020_XmlID_x0020_and_x0020_TemplateIDResult = $AddContent_x0020_with_x0020_XmlID_x0020_and_x0020_TemplateIDResult;
    }

}
