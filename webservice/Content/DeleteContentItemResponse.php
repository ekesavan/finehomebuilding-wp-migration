<?php

class DeleteContentItemResponse
{

    /**
     * @var boolean $DeleteContentItemResult
     * @access public
     */
    public $DeleteContentItemResult = null;

    /**
     * @param boolean $DeleteContentItemResult
     * @access public
     */
    public function __construct($DeleteContentItemResult)
    {
      $this->DeleteContentItemResult = $DeleteContentItemResult;
    }

}
