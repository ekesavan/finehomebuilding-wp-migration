<?php

class CopyContentToFolder
{

    /**
     * @var string $ContentID
     * @access public
     */
    public $ContentID = null;

    /**
     * @var int $FolderID
     * @access public
     */
    public $FolderID = null;

    /**
     * @var string $Language
     * @access public
     */
    public $Language = null;

    /**
     * @var boolean $Publish
     * @access public
     */
    public $Publish = null;

    /**
     * @param string $ContentID
     * @param int $FolderID
     * @param string $Language
     * @param boolean $Publish
     * @access public
     */
    public function __construct($ContentID, $FolderID, $Language, $Publish)
    {
      $this->ContentID = $ContentID;
      $this->FolderID = $FolderID;
      $this->Language = $Language;
      $this->Publish = $Publish;
    }

}
