<?php

class SubmitForDelete
{

    /**
     * @var int $ContentID
     * @access public
     */
    public $ContentID = null;

    /**
     * @var int $FolderID
     * @access public
     */
    public $FolderID = null;

    /**
     * @param int $ContentID
     * @param int $FolderID
     * @access public
     */
    public function __construct($ContentID, $FolderID)
    {
      $this->ContentID = $ContentID;
      $this->FolderID = $FolderID;
    }

}
