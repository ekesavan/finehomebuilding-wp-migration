<?php
require_once (dirname(__FILE__).'/../../lib/EktronSoapClient.php');

include_once('AddFolder.php');
include_once('FolderRequest.php');
include_once('SitemapPath.php');
include_once('SubscriptionPropertiesData.php');
include_once('SubscriptionPropertyNotificationTypes.php');
include_once('AddFolderResponse.php');
include_once('AuthenticationHeader.php');
include_once('RequestInfoParameters.php');
include_once('CreateFolder.php');
include_once('CreateFolderResponse.php');
include_once('DeleteFolder.php');
include_once('DeleteFolderResponse.php');
include_once('GetChildFolders_Obsolete.php');
include_once('GetChildFolders_ObsoleteResponse.php');
include_once('FolderData.php');
include_once('CmsDataOfFolderData.php');
include_once('BaseDataOfFolderData.php');
include_once('XmlConfigData.php');
include_once('FolderApprovalType.php');
include_once('PermissionData.php');
include_once('CmsDataOfPermissionData.php');
include_once('BaseDataOfPermissionData.php');
include_once('ContentMetaData.php');
include_once('CmsDataOfContentMetaData.php');
include_once('BaseDataOfContentMetaData.php');
include_once('ContentMetadataDataType.php');
include_once('ContentMetadataType.php');
include_once('TemplateData.php');
include_once('CmsDataOfTemplateData.php');
include_once('BaseDataOfTemplateData.php');
include_once('TemplateType.php');
include_once('TemplateSubType.php');
include_once('FolderType.php');
include_once('TaxonomyBaseData.php');
include_once('CmsDataOfTaxonomyBaseData.php');
include_once('BaseDataOfTaxonomyBaseData.php');
include_once('TaxonomyType.php');
include_once('FlagDefData.php');
include_once('CmsDataOfFlagDefData.php');
include_once('BaseDataOfFlagDefData.php');
include_once('FlagItemData.php');
include_once('GetChildFolders.php');
include_once('FolderOrderBy.php');
include_once('GetChildFoldersResponse.php');
include_once('GetFolderID.php');
include_once('GetFolderIDResponse.php');
include_once('GetFolderIDByName.php');
include_once('GetFolderIDByNameResponse.php');
include_once('GetPath.php');
include_once('GetPathResponse.php');
include_once('RenameFolder.php');
include_once('RenameFolderResponse.php');
include_once('GetFolder.php');
include_once('GetFolderResponse.php');
include_once('GetFolderByParam.php');
include_once('GetFolderByParamResponse.php');
include_once('UpdateFolder.php');
include_once('UpdateFolderResponse.php');
include_once('UpdateFolder_x005B_FolderData_x005D_.php');
include_once('UpdateFolder_x005B_FolderData_x005D_Response.php');

class Folder extends \EktronSoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'AddFolder' => '\AddFolder',
      'FolderRequest' => '\FolderRequest',
      'SitemapPath' => '\SitemapPath',
      'SubscriptionPropertiesData' => '\SubscriptionPropertiesData',
      'AddFolderResponse' => '\AddFolderResponse',
      'AuthenticationHeader' => '\AuthenticationHeader',
      'RequestInfoParameters' => '\RequestInfoParameters',
      'CreateFolder' => '\CreateFolder',
      'CreateFolderResponse' => '\CreateFolderResponse',
      'DeleteFolder' => '\DeleteFolder',
      'DeleteFolderResponse' => '\DeleteFolderResponse',
      'GetChildFolders_Obsolete' => '\GetChildFolders_Obsolete',
      'GetChildFolders_ObsoleteResponse' => '\GetChildFolders_ObsoleteResponse',
      'FolderData' => '\FolderData',
      'CmsDataOfFolderData' => '\CmsDataOfFolderData',
      'BaseDataOfFolderData' => '\BaseDataOfFolderData',
      'XmlConfigData' => '\XmlConfigData',
      'PermissionData' => '\PermissionData',
      'CmsDataOfPermissionData' => '\CmsDataOfPermissionData',
      'BaseDataOfPermissionData' => '\BaseDataOfPermissionData',
      'ContentMetaData' => '\ContentMetaData',
      'CmsDataOfContentMetaData' => '\CmsDataOfContentMetaData',
      'BaseDataOfContentMetaData' => '\BaseDataOfContentMetaData',
      'TemplateData' => '\TemplateData',
      'CmsDataOfTemplateData' => '\CmsDataOfTemplateData',
      'BaseDataOfTemplateData' => '\BaseDataOfTemplateData',
      'TaxonomyBaseData' => '\TaxonomyBaseData',
      'CmsDataOfTaxonomyBaseData' => '\CmsDataOfTaxonomyBaseData',
      'BaseDataOfTaxonomyBaseData' => '\BaseDataOfTaxonomyBaseData',
      'FlagDefData' => '\FlagDefData',
      'CmsDataOfFlagDefData' => '\CmsDataOfFlagDefData',
      'BaseDataOfFlagDefData' => '\BaseDataOfFlagDefData',
      'FlagItemData' => '\FlagItemData',
      'GetChildFolders' => '\GetChildFolders',
      'GetChildFoldersResponse' => '\GetChildFoldersResponse',
      'GetFolderID' => '\GetFolderID',
      'GetFolderIDResponse' => '\GetFolderIDResponse',
      'GetFolderIDByName' => '\GetFolderIDByName',
      'GetFolderIDByNameResponse' => '\GetFolderIDByNameResponse',
      'GetPath' => '\GetPath',
      'GetPathResponse' => '\GetPathResponse',
      'RenameFolder' => '\RenameFolder',
      'RenameFolderResponse' => '\RenameFolderResponse',
      'GetFolder' => '\GetFolder',
      'GetFolderResponse' => '\GetFolderResponse',
      'GetFolderByParam' => '\GetFolderByParam',
      'GetFolderByParamResponse' => '\GetFolderByParamResponse',
      'UpdateFolder' => '\UpdateFolder',
      'UpdateFolderResponse' => '\UpdateFolderResponse',
      'UpdateFolder_x005B_FolderData_x005D_' => '\UpdateFolder_x005B_FolderData_x005D_',
      'UpdateFolder_x005B_FolderData_x005D_Response' => '\UpdateFolder_x005B_FolderData_x005D_Response');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl='wsdl/Folder.wsdl')
    {
      foreach (self::$classmap as $key => $value) {
    if (!isset($options['classmap'][$key])) {
      $options['classmap'][$key] = $value;
    }
  }
  $options['trace'] = true;
  
  parent::__construct($wsdl, $options);
    }

    /**
     * Adds a folder
     *
     * @param AddFolder $parameters
     * @access public
     * @return AddFolderResponse
     */
    public function AddFolder(AddFolder $parameters)
    {
      return $this->__soapCall('AddFolder', array($parameters));
    }

    /**
     * Creates a folder with the given path
     *
     * @param CreateFolder $parameters
     * @access public
     * @return CreateFolderResponse
     */
    public function CreateFolder(CreateFolder $parameters)
    {
      return $this->__soapCall('CreateFolder', array($parameters));
    }

    /**
     * Deletes folder specified by folder id
     *
     * @param DeleteFolder $parameters
     * @access public
     * @return DeleteFolderResponse
     */
    public function DeleteFolder(DeleteFolder $parameters)
    {
      return $this->__soapCall('DeleteFolder', array($parameters));
    }

    /**
     * This method is obsolete. Use new GetChildFolders method with FolderOrderBy enum parameter
     *
     * @param GetChildFolders_Obsolete $parameters
     * @access public
     * @return GetChildFolders_ObsoleteResponse
     */
    public function GetChildFolders(GetChildFolders $parameters)
    {
      return $this->__soapCall('GetChildFolders', array($parameters));
    }

    /**
     * Finds the folder ID by using the content's ID
     *
     * @param GetFolderID $parameters
     * @access public
     * @return GetFolderIDResponse
     */
    public function GetFolderID(GetFolderID $parameters)
    {
      return $this->__soapCall('GetFolderID', array($parameters));
    }

    /**
     * Gets path to folder by folder ID
     *
     * @param GetPath $parameters
     * @access public
     * @return GetPathResponse
     */
    public function GetPath(GetPath $parameters)
    {
      return $this->__soapCall('GetPath', array($parameters));
    }

    /**
     * Renames the folder by ID.
     *
     * @param RenameFolder $parameters
     * @access public
     * @return RenameFolderResponse
     */
    public function RenameFolder(RenameFolder $parameters)
    {
      return $this->__soapCall('RenameFolder', array($parameters));
    }

    /**
     * Retrieves the folder's details
     *
     * @param GetFolder $parameters
     * @access public
     * @return GetFolderResponse
     */
    public function GetFolder(GetFolder $parameters)
    {
      return $this->__soapCall('GetFolder', array($parameters));
    }

    /**
     * Updates a folder
     *
     * @param UpdateFolder $parameters
     * @access public
     * @return UpdateFolderResponse
     */
    public function UpdateFolder(UpdateFolder $parameters)
    {
      return $this->__soapCall('UpdateFolder', array($parameters));
    }

}
