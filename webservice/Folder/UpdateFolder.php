<?php

class UpdateFolder
{

    /**
     * @var FolderRequest $FolderReq
     * @access public
     */
    public $FolderReq = null;

    /**
     * @param FolderRequest $FolderReq
     * @access public
     */
    public function __construct($FolderReq)
    {
      $this->FolderReq = $FolderReq;
    }

}
