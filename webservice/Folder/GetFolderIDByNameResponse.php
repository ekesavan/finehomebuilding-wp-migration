<?php

class GetFolderIDByNameResponse
{

    /**
     * @var int $GetFolderIDByNameResult
     * @access public
     */
    public $GetFolderIDByNameResult = null;

    /**
     * @param int $GetFolderIDByNameResult
     * @access public
     */
    public function __construct($GetFolderIDByNameResult)
    {
      $this->GetFolderIDByNameResult = $GetFolderIDByNameResult;
    }

}
