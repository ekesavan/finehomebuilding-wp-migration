<?php

class FolderType
{
    const __default = 'Content';
    const Content = 'Content';
    const Blog = 'Blog';
    const Domain = 'Domain';
    const DiscussionBoard = 'DiscussionBoard';
    const DiscussionForum = 'DiscussionForum';
    const Root = 'Root';
    const Community = 'Community';
    const Media = 'Media';
    const Calendar = 'Calendar';
    const Catalog = 'Catalog';
    const Program = 'Program';


}
