<?php

class RenameFolder
{

    /**
     * @var int $FolderID
     * @access public
     */
    public $FolderID = null;

    /**
     * @var string $Title
     * @access public
     */
    public $Title = null;

    /**
     * @param int $FolderID
     * @param string $Title
     * @access public
     */
    public function __construct($FolderID, $Title)
    {
      $this->FolderID = $FolderID;
      $this->Title = $Title;
    }

}
