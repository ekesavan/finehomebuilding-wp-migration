<?php

class GetFolderByParamResponse
{

    /**
     * @var FolderData $GetFolderByParamResult
     * @access public
     */
    public $GetFolderByParamResult = null;

    /**
     * @param FolderData $GetFolderByParamResult
     * @access public
     */
    public function __construct($GetFolderByParamResult)
    {
      $this->GetFolderByParamResult = $GetFolderByParamResult;
    }

}
