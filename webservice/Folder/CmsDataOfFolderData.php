<?php

include_once('BaseDataOfFolderData.php');

class CmsDataOfFolderData extends BaseDataOfFolderData
{

    /**
     * @var int $Id
     * @access public
     */
    public $Id = null;

    /**
     * @param int $Id
     * @access public
     */
    public function __construct($Id)
    {
      $this->Id = $Id;
    }

}
