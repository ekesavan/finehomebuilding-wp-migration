<?php

include_once('CmsDataOfFolderData.php');

class FolderData extends CmsDataOfFolderData
{

    /**
     * @var int $TemplateId
     * @access public
     */
    public $TemplateId = null;

    /**
     * @var int $ParentId
     * @access public
     */
    public $ParentId = null;

    /**
     * @var string $NameWithPath
     * @access public
     */
    public $NameWithPath = null;

    /**
     * @var string $FolderIdWithPath
     * @access public
     */
    public $FolderIdWithPath = null;

    /**
     * @var string $Name
     * @access public
     */
    public $Name = null;

    /**
     * @var string $Description
     * @access public
     */
    public $Description = null;

    /**
     * @var boolean $IsPermissionsInherited
     * @access public
     */
    public $IsPermissionsInherited = null;

    /**
     * @var boolean $IsHidden
     * @access public
     */
    public $IsHidden = null;

    /**
     * @var int $InheritedFrom
     * @access public
     */
    public $InheritedFrom = null;

    /**
     * @var boolean $PrivateContent
     * @access public
     */
    public $PrivateContent = null;

    /**
     * @var boolean $IsXmlInherited
     * @access public
     */
    public $IsXmlInherited = null;

    /**
     * @var int $XmlInheritedFrom
     * @access public
     */
    public $XmlInheritedFrom = null;

    /**
     * @var XmlConfigData[] $XmlConfiguration
     * @access public
     */
    public $XmlConfiguration = null;

    /**
     * @var string $StyleSheet
     * @access public
     */
    public $StyleSheet = null;

    /**
     * @var string $TemplateFileName
     * @access public
     */
    public $TemplateFileName = null;

    /**
     * @var boolean $IsStyleSheetInherited
     * @access public
     */
    public $IsStyleSheetInherited = null;

    /**
     * @var boolean $IsTemplateInherited
     * @access public
     */
    public $IsTemplateInherited = null;

    /**
     * @var int $TemplateInheritedFrom
     * @access public
     */
    public $TemplateInheritedFrom = null;

    /**
     * @var int $ApprovalMethod
     * @access public
     */
    public $ApprovalMethod = null;

    /**
     * @var FolderApprovalType $ApprovalType
     * @access public
     */
    public $ApprovalType = null;

    /**
     * @var FolderData[] $ChildFolders
     * @access public
     */
    public $ChildFolders = null;

    /**
     * @var int $MetaInheritedFrom
     * @access public
     */
    public $MetaInheritedFrom = null;

    /**
     * @var boolean $IsMetaInherited
     * @access public
     */
    public $IsMetaInherited = null;

    /**
     * @var boolean $HasChildren
     * @access public
     */
    public $HasChildren = null;

    /**
     * @var boolean $PublishPdfActive
     * @access public
     */
    public $PublishPdfActive = null;

    /**
     * @var PermissionData $Permissions
     * @access public
     */
    public $Permissions = null;

    /**
     * @var ContentMetaData[] $FolderMetadata
     * @access public
     */
    public $FolderMetadata = null;

    /**
     * @var TemplateData[] $FolderTemplates
     * @access public
     */
    public $FolderTemplates = null;

    /**
     * @var int $TotalContent
     * @access public
     */
    public $TotalContent = null;

    /**
     * @var boolean $IsDomainFolder
     * @access public
     */
    public $IsDomainFolder = null;

    /**
     * @var string $DomainStaging
     * @access public
     */
    public $DomainStaging = null;

    /**
     * @var string $DomainProduction
     * @access public
     */
    public $DomainProduction = null;

    /**
     * @var int $FolderType
     * @access public
     */
    public $FolderType = null;

    /**
     * @var FolderType $Type
     * @access public
     */
    public $Type = null;

    /**
     * @var boolean $IsSitemapInherited
     * @access public
     */
    public $IsSitemapInherited = null;

    /**
     * @var SitemapPath[] $BreadCrumbPath
     * @access public
     */
    public $BreadCrumbPath = null;

    /**
     * @var boolean $IsCommunityFolder
     * @access public
     */
    public $IsCommunityFolder = null;

    /**
     * @var boolean $IsTaxonomyInherited
     * @access public
     */
    public $IsTaxonomyInherited = null;

    /**
     * @var int $AliasInheritedFrom
     * @access public
     */
    public $AliasInheritedFrom = null;

    /**
     * @var boolean $IsAliasInherited
     * @access public
     */
    public $IsAliasInherited = null;

    /**
     * @var boolean $AliasRequired
     * @access public
     */
    public $AliasRequired = null;

    /**
     * @var int $FlagInheritedFrom
     * @access public
     */
    public $FlagInheritedFrom = null;

    /**
     * @var boolean $IsFlagInherited
     * @access public
     */
    public $IsFlagInherited = null;

    /**
     * @var boolean $IsCategoryRequired
     * @access public
     */
    public $IsCategoryRequired = null;

    /**
     * @var TaxonomyBaseData[] $FolderTaxonomy
     * @access public
     */
    public $FolderTaxonomy = null;

    /**
     * @var FlagDefData[] $FolderFlags
     * @access public
     */
    public $FolderFlags = null;

    /**
     * @var int $IsContentSearchableInheritedFrom
     * @access public
     */
    public $IsContentSearchableInheritedFrom = null;

    /**
     * @var boolean $IsContentSearchableInherited
     * @access public
     */
    public $IsContentSearchableInherited = null;

    /**
     * @var boolean $IscontentSearchable
     * @access public
     */
    public $IscontentSearchable = null;

    /**
     * @var int $DisplaySettings
     * @access public
     */
    public $DisplaySettings = null;

    /**
     * @var int $DisplaySettingsInheritedFrom
     * @access public
     */
    public $DisplaySettingsInheritedFrom = null;

    /**
     * @var boolean $IsDisplaySettingsInherited
     * @access public
     */
    public $IsDisplaySettingsInherited = null;

    /**
     * @param int $Id
     * @param int $TemplateId
     * @param int $ParentId
     * @param string $NameWithPath
     * @param string $FolderIdWithPath
     * @param string $Name
     * @param string $Description
     * @param boolean $IsPermissionsInherited
     * @param boolean $IsHidden
     * @param int $InheritedFrom
     * @param boolean $PrivateContent
     * @param boolean $IsXmlInherited
     * @param int $XmlInheritedFrom
     * @param XmlConfigData[] $XmlConfiguration
     * @param string $StyleSheet
     * @param string $TemplateFileName
     * @param boolean $IsStyleSheetInherited
     * @param boolean $IsTemplateInherited
     * @param int $TemplateInheritedFrom
     * @param int $ApprovalMethod
     * @param FolderApprovalType $ApprovalType
     * @param FolderData[] $ChildFolders
     * @param int $MetaInheritedFrom
     * @param boolean $IsMetaInherited
     * @param boolean $HasChildren
     * @param boolean $PublishPdfActive
     * @param PermissionData $Permissions
     * @param ContentMetaData[] $FolderMetadata
     * @param TemplateData[] $FolderTemplates
     * @param int $TotalContent
     * @param boolean $IsDomainFolder
     * @param string $DomainStaging
     * @param string $DomainProduction
     * @param int $FolderType
     * @param FolderType $Type
     * @param boolean $IsSitemapInherited
     * @param SitemapPath[] $BreadCrumbPath
     * @param boolean $IsCommunityFolder
     * @param boolean $IsTaxonomyInherited
     * @param int $AliasInheritedFrom
     * @param boolean $IsAliasInherited
     * @param boolean $AliasRequired
     * @param int $FlagInheritedFrom
     * @param boolean $IsFlagInherited
     * @param boolean $IsCategoryRequired
     * @param TaxonomyBaseData[] $FolderTaxonomy
     * @param FlagDefData[] $FolderFlags
     * @param int $IsContentSearchableInheritedFrom
     * @param boolean $IsContentSearchableInherited
     * @param boolean $IscontentSearchable
     * @param int $DisplaySettings
     * @param int $DisplaySettingsInheritedFrom
     * @param boolean $IsDisplaySettingsInherited
     * @access public
     */
    public function __construct($Id, $TemplateId, $ParentId, $NameWithPath, $FolderIdWithPath, $Name, $Description, $IsPermissionsInherited, $IsHidden, $InheritedFrom, $PrivateContent, $IsXmlInherited, $XmlInheritedFrom, $XmlConfiguration, $StyleSheet, $TemplateFileName, $IsStyleSheetInherited, $IsTemplateInherited, $TemplateInheritedFrom, $ApprovalMethod, $ApprovalType, $ChildFolders, $MetaInheritedFrom, $IsMetaInherited, $HasChildren, $PublishPdfActive, $Permissions, $FolderMetadata, $FolderTemplates, $TotalContent, $IsDomainFolder, $DomainStaging, $DomainProduction, $FolderType, $Type, $IsSitemapInherited, $BreadCrumbPath, $IsCommunityFolder, $IsTaxonomyInherited, $AliasInheritedFrom, $IsAliasInherited, $AliasRequired, $FlagInheritedFrom, $IsFlagInherited, $IsCategoryRequired, $FolderTaxonomy, $FolderFlags, $IsContentSearchableInheritedFrom, $IsContentSearchableInherited, $IscontentSearchable, $DisplaySettings, $DisplaySettingsInheritedFrom, $IsDisplaySettingsInherited)
    {
      parent::__construct($Id);
      $this->TemplateId = $TemplateId;
      $this->ParentId = $ParentId;
      $this->NameWithPath = $NameWithPath;
      $this->FolderIdWithPath = $FolderIdWithPath;
      $this->Name = $Name;
      $this->Description = $Description;
      $this->IsPermissionsInherited = $IsPermissionsInherited;
      $this->IsHidden = $IsHidden;
      $this->InheritedFrom = $InheritedFrom;
      $this->PrivateContent = $PrivateContent;
      $this->IsXmlInherited = $IsXmlInherited;
      $this->XmlInheritedFrom = $XmlInheritedFrom;
      $this->XmlConfiguration = $XmlConfiguration;
      $this->StyleSheet = $StyleSheet;
      $this->TemplateFileName = $TemplateFileName;
      $this->IsStyleSheetInherited = $IsStyleSheetInherited;
      $this->IsTemplateInherited = $IsTemplateInherited;
      $this->TemplateInheritedFrom = $TemplateInheritedFrom;
      $this->ApprovalMethod = $ApprovalMethod;
      $this->ApprovalType = $ApprovalType;
      $this->ChildFolders = $ChildFolders;
      $this->MetaInheritedFrom = $MetaInheritedFrom;
      $this->IsMetaInherited = $IsMetaInherited;
      $this->HasChildren = $HasChildren;
      $this->PublishPdfActive = $PublishPdfActive;
      $this->Permissions = $Permissions;
      $this->FolderMetadata = $FolderMetadata;
      $this->FolderTemplates = $FolderTemplates;
      $this->TotalContent = $TotalContent;
      $this->IsDomainFolder = $IsDomainFolder;
      $this->DomainStaging = $DomainStaging;
      $this->DomainProduction = $DomainProduction;
      $this->FolderType = $FolderType;
      $this->Type = $Type;
      $this->IsSitemapInherited = $IsSitemapInherited;
      $this->BreadCrumbPath = $BreadCrumbPath;
      $this->IsCommunityFolder = $IsCommunityFolder;
      $this->IsTaxonomyInherited = $IsTaxonomyInherited;
      $this->AliasInheritedFrom = $AliasInheritedFrom;
      $this->IsAliasInherited = $IsAliasInherited;
      $this->AliasRequired = $AliasRequired;
      $this->FlagInheritedFrom = $FlagInheritedFrom;
      $this->IsFlagInherited = $IsFlagInherited;
      $this->IsCategoryRequired = $IsCategoryRequired;
      $this->FolderTaxonomy = $FolderTaxonomy;
      $this->FolderFlags = $FolderFlags;
      $this->IsContentSearchableInheritedFrom = $IsContentSearchableInheritedFrom;
      $this->IsContentSearchableInherited = $IsContentSearchableInherited;
      $this->IscontentSearchable = $IscontentSearchable;
      $this->DisplaySettings = $DisplaySettings;
      $this->DisplaySettingsInheritedFrom = $DisplaySettingsInheritedFrom;
      $this->IsDisplaySettingsInherited = $IsDisplaySettingsInherited;
    }

}
