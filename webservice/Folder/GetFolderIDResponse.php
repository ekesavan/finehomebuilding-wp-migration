<?php

class GetFolderIDResponse
{

    /**
     * @var int $GetFolderIDResult
     * @access public
     */
    public $GetFolderIDResult = null;

    /**
     * @param int $GetFolderIDResult
     * @access public
     */
    public function __construct($GetFolderIDResult)
    {
      $this->GetFolderIDResult = $GetFolderIDResult;
    }

}
