<?php

class GetContentMetadataListResponse
{

    /**
     * @var CustomAttribute[] $GetContentMetadataListResult
     * @access public
     */
    public $GetContentMetadataListResult = null;

    /**
     * @param CustomAttribute[] $GetContentMetadataListResult
     * @access public
     */
    public function __construct($GetContentMetadataListResult)
    {
      $this->GetContentMetadataListResult = $GetContentMetadataListResult;
    }

}
