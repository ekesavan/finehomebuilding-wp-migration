<?php

include_once('CmsDataOfCustomAttribute.php');

class CustomAttribute extends CmsDataOfCustomAttribute
{

    /**
     * @var int $PropertyValueId
     * @access public
     */
    public $PropertyValueId = null;

    /**
     * @var string $Name
     * @access public
     */
    public $Name = null;

    /**
     * @var anyType $Value
     * @access public
     */
    public $Value = null;

    /**
     * @var CustomAttributeValueTypes $ValueType
     * @access public
     */
    public $ValueType = null;

    /**
     * @var boolean $IsRequired
     * @access public
     */
    public $IsRequired = null;

    /**
     * @var int $Language
     * @access public
     */
    public $Language = null;

    /**
     * @var int $TagType
     * @access public
     */
    public $TagType = null;

    /**
     * @var boolean $IsList
     * @access public
     */
    public $IsList = null;

    /**
     * @var boolean $DoesAtrributeExist
     * @access public
     */
    public $DoesAtrributeExist = null;

    /**
     * @param int $Id
     * @param int $PropertyValueId
     * @param string $Name
     * @param anyType $Value
     * @param CustomAttributeValueTypes $ValueType
     * @param boolean $IsRequired
     * @param int $Language
     * @param int $TagType
     * @param boolean $IsList
     * @param boolean $DoesAtrributeExist
     * @access public
     */
    public function __construct($Id, $PropertyValueId, $Name, $Value, $ValueType, $IsRequired, $Language, $TagType, $IsList, $DoesAtrributeExist)
    {
      parent::__construct($Id);
      $this->PropertyValueId = $PropertyValueId;
      $this->Name = $Name;
      $this->Value = $Value;
      $this->ValueType = $ValueType;
      $this->IsRequired = $IsRequired;
      $this->Language = $Language;
      $this->TagType = $TagType;
      $this->IsList = $IsList;
      $this->DoesAtrributeExist = $DoesAtrributeExist;
    }

}
