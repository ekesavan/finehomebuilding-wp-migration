<?php

require_once( dirname(__FILE__) . "/lib/Config.php");
require_once( dirname(__FILE__) . "/lib/FHBMongo.php");
require_once (dirname(__FILE__).'/lib/EktronSoapClient.php');

require_once( dirname(__FILE__) . "/wsdl/Folder/Folder.php");
require_once( dirname(__FILE__) . "/wsdl/Content/Content.php");
require_once( dirname(__FILE__) . "/wsdl/WebServiceAPI/Metadata/Metadata.php");

require_once( dirname(__FILE__) . "/lib/EktronMigration.php");
require_once( dirname(__FILE__) . "/lib/EktronMenu.php");

require_once( dirname(__FILE__) . "/lib/EktronContent.php");
require_once( dirname(__FILE__) . "/lib/EktronTaxonomy.php");
require_once( dirname(__FILE__) . "/lib/EktronMenuTaxonomy.php");
require_once( dirname(__FILE__) . "/lib/EktronAuthor.php");
require_once( dirname(__FILE__) . "/lib/EktronArticle.php");
require_once( dirname(__FILE__) . "/lib/EktronDepartmentArticle.php");
require_once( dirname(__FILE__) . "/lib/EktronReadersTips.php");
require_once( dirname(__FILE__) . "/lib/EktronReview.php");
require_once( dirname(__FILE__) . "/lib/EktronVideo.php");
require_once( dirname(__FILE__) . "/lib/EktronToolGuide.php");
require_once( dirname(__FILE__) . "/lib/EktronQA.php");


require_once( dirname(__FILE__) . "/lib/EktronWebService.php");

require_once( dirname(__FILE__) . "/vendor/simple_html_dom.php");

require_once( dirname(__FILE__) . "/../finehomebuilding-wordpress/web/wp/wp-load.php");
require_once( dirname(__FILE__) . "/../finehomebuilding-wordpress/web/wp/wp-admin/includes/admin.php");
require_once( dirname(__FILE__) . "/../finehomebuilding-wordpress/web/app/themes/finehomebuilding/functions.php");

define( '__FULL__', true );

$fhb_import = new FHBMongo('FHBMigration');

$start_time = new MongoDB\BSON\UTCDateTime( time() );

$fhb_import->insert( 'import', array( 'start_time' => $start_time ) );

$ektron = new EktronMigration();

//$ektron->get_content_by_id( '131734' );

$content_config = array(
		'EktronArticle' => '336',
		'EktronDepartmentArticle' => '1066',
		'EktronReadersTips' => '342',
		'EktronReview' => '1674',
		'EktronAudio' => '1348',
		'EktronBookExcerpt' => '1084',
		'EktronContests' => '1408',
		'EktronSlideShow' => '2659',
		'EktronSpecialCollection' => '340',
		'EktronVideo' => '344',
		'EktronToolGuide' => '1446',
		'EktronQA' => '338',
		//'EktronManufacturers' => 1676,
);


/* Load Taxonomy : 308 */
/*
print "Load Ektron Taxonomy.....\n";
$csv = dirname(__FILE__)."/FHB-TopicMapping-2016-01-14.csv";
$t = new EktronTaxonomy($csv);
$t->load_content( 308 );
$t->load_taxonomy( 308 );
*/

/* Load Ektron Authors */
//$ektron->load_content( 'EktronAuthor', 74 );

/* Load Content */
foreach( $content_config as $k => $v ){
	error_log(  "Load Content $k---->$v ...");
	$ektron->load_content( $k, $v );
	//$ektron->load_fhb_authors( $k );
}

?>