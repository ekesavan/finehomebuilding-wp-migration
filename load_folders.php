<?php

require_once("webservice/Folder/Folder.php");
try{
	require_once("webservice/Content/Content.php");
}catch(Exception $e ){
	
}
$options = array();		
		
$auth = new AuthenticationHeader( 'ekesavan', 'Taunton1', 'cmsedit.taunton.dev' );

$req = new Folder($options);

$headers[] = new SoapHeader("http://tempuri.org/", 
                            'AuthenticationHeader',
                            $auth);

$req->__setSoapHeaders($headers);


$GetFolder = new GetFolder( 308  );


$res = $req->GetFolder( $GetFolder );

print $req->__getLastResponse();

print "\n";

//print_R( $res );


// Get child folders
$GetChildFolders = new GetChildFolders( 308, true, 'Name' );

$result = $req->GetChildFolders( $GetChildFolders );

$folders = $result->GetChildFolders_ObsoleteResult->FolderData;

/*
ob_start();
print_R(  $folders );
$str = ob_get_clean();
file_put_contents( 'data.txt', $str  );
*/

foreach( $folders as $folder ){
	
	print 	$folder->Name ."\n";
	$t = "\t";
	print_folder_items( $folder->Id , $t  );
	if( $folder->ChildFolders )
		print_folder( $folder->ChildFolders, $t );
	
}

function print_folder(&$folder, $t ){
	
	if( is_array( $folder->FolderData ) ){
		foreach( $folder->FolderData as $f ){
			print $t.$f->Name."\n";
			
			// Folder ID KEY: Id
			/*
			GET Folder Items
			*/
			print_folder_items( $f->Id , $t."\t"  );
						
			if( $f->HasChildren ){
				print_folder( $f->ChildFolders, $t."\t" );
			}
		}
	}else{

		if( property_exists( $folder, "FolderData") ) {
			
			$FolderData = $folder->FolderData;
							
			if( $FolderData && property_exists( $FolderData, "Name" )  ){
				
				print $t.$FolderData->Name."\n";
				print_folder_items( $FolderData->Id , $t."\t"  );
			}else{
				print_R( $FolderData );
				die;
			
			}
			
		}else{
			print_R( $folder );
			die;
		}
	}
}


function print_folder_items( $folder_id, $t ) {
	
	global $headers;
	
	$req = new Content(array());
	
	$req->__setSoapHeaders($headers);

	$GetChildContent = new GetChildContent( $folder_id, true, 'Name'  );

	$res = $req->GetChildContent( $GetChildContent );
	
	if( $res->GetChildContentResult ){
		$items = $res->GetChildContentResult->ContentData;	
		
		foreach( $items as $item ){
			print $t.$item->Title."\n";
		}
	}
	/*
	ob_start();
	print_R( $res );
	$str = ob_get_clean();
	file_put_contents( 'C:\wamp\www\data.txt', $str, FILE_APPEND  );
	*/
	
}
$str = ob_get_clean();
file_put_contents( 'Taxonomy.txt', $str, FILE_APPEND  );
?>