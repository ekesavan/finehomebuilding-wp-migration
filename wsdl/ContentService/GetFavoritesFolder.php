<?php

class GetFavoritesFolder
{

    /**
     * @var string $folderList
     * @access public
     */
    public $folderList = null;

    /**
     * @param string $folderList
     * @access public
     */
    public function __construct($folderList)
    {
      $this->folderList = $folderList;
    }

}
