<?php

class AddContentResponse
{

    /**
     * @var boolean $AddContentResult
     * @access public
     */
    public $AddContentResult = null;

    /**
     * @param boolean $AddContentResult
     * @access public
     */
    public function __construct($AddContentResult)
    {
      $this->AddContentResult = $AddContentResult;
    }

}
