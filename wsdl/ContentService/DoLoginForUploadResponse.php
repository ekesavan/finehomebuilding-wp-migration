<?php

class DoLoginForUploadResponse
{

    /**
     * @var LoginUpload $DoLoginForUploadResult
     * @access public
     */
    public $DoLoginForUploadResult = null;

    /**
     * @param LoginUpload $DoLoginForUploadResult
     * @access public
     */
    public function __construct($DoLoginForUploadResult)
    {
      $this->DoLoginForUploadResult = $DoLoginForUploadResult;
    }

}
