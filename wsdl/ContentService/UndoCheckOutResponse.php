<?php

class UndoCheckOutResponse
{

    /**
     * @var boolean $UndoCheckOutResult
     * @access public
     */
    public $UndoCheckOutResult = null;

    /**
     * @param boolean $UndoCheckOutResult
     * @access public
     */
    public function __construct($UndoCheckOutResult)
    {
      $this->UndoCheckOutResult = $UndoCheckOutResult;
    }

}
