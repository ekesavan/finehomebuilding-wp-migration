<?php

class OFR
{

    /**
     * @var string $folderName
     * @access public
     */
    public $folderName = null;

    /**
     * @var string $id
     * @access public
     */
    public $id = null;

    /**
     * @var boolean $hc
     * @access public
     */
    public $hc = null;

    /**
     * @param string $folderName
     * @param string $id
     * @param boolean $hc
     * @access public
     */
    public function __construct($folderName, $id, $hc)
    {
      $this->folderName = $folderName;
      $this->id = $id;
      $this->hc = $hc;
    }

}
