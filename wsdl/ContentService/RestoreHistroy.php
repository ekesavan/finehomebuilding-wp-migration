<?php

class RestoreHistroy
{

    /**
     * @var string $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var string $HistoryId
     * @access public
     */
    public $HistoryId = null;

    /**
     * @param string $Id
     * @param string $HistoryId
     * @access public
     */
    public function __construct($Id, $HistoryId)
    {
      $this->Id = $Id;
      $this->HistoryId = $HistoryId;
    }

}
