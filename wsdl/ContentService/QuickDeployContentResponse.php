<?php

class QuickDeployContentResponse
{

    /**
     * @var boolean $QuickDeployContentResult
     * @access public
     */
    public $QuickDeployContentResult = null;

    /**
     * @param boolean $QuickDeployContentResult
     * @access public
     */
    public function __construct($QuickDeployContentResult)
    {
      $this->QuickDeployContentResult = $QuickDeployContentResult;
    }

}
