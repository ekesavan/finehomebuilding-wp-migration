<?php

class EktOfficeGetDocumentByAssetIDResponse
{

    /**
     * @var OFD $EktOfficeGetDocumentByAssetIDResult
     * @access public
     */
    public $EktOfficeGetDocumentByAssetIDResult = null;

    /**
     * @param OFD $EktOfficeGetDocumentByAssetIDResult
     * @access public
     */
    public function __construct($EktOfficeGetDocumentByAssetIDResult)
    {
      $this->EktOfficeGetDocumentByAssetIDResult = $EktOfficeGetDocumentByAssetIDResult;
    }

}
