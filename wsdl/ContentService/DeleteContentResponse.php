<?php

class DeleteContentResponse
{

    /**
     * @var boolean $DeleteContentResult
     * @access public
     */
    public $DeleteContentResult = null;

    /**
     * @param boolean $DeleteContentResult
     * @access public
     */
    public function __construct($DeleteContentResult)
    {
      $this->DeleteContentResult = $DeleteContentResult;
    }

}
