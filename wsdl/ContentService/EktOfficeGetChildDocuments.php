<?php

class EktOfficeGetChildDocuments
{

    /**
     * @var string $folderId
     * @access public
     */
    public $folderId = null;

    /**
     * @var string $extensions
     * @access public
     */
    public $extensions = null;

    /**
     * @param string $folderId
     * @param string $extensions
     * @access public
     */
    public function __construct($folderId, $extensions)
    {
      $this->folderId = $folderId;
      $this->extensions = $extensions;
    }

}
