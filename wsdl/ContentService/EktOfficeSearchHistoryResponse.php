<?php

class EktOfficeSearchHistoryResponse
{

    /**
     * @var HIS[] $EktOfficeSearchHistoryResult
     * @access public
     */
    public $EktOfficeSearchHistoryResult = null;

    /**
     * @param HIS[] $EktOfficeSearchHistoryResult
     * @access public
     */
    public function __construct($EktOfficeSearchHistoryResult)
    {
      $this->EktOfficeSearchHistoryResult = $EktOfficeSearchHistoryResult;
    }

}
