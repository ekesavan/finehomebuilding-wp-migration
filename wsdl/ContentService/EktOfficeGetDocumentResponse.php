<?php

class EktOfficeGetDocumentResponse
{

    /**
     * @var OFD $EktOfficeGetDocumentResult
     * @access public
     */
    public $EktOfficeGetDocumentResult = null;

    /**
     * @param OFD $EktOfficeGetDocumentResult
     * @access public
     */
    public function __construct($EktOfficeGetDocumentResult)
    {
      $this->EktOfficeGetDocumentResult = $EktOfficeGetDocumentResult;
    }

}
