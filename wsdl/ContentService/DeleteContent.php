<?php

class DeleteContent
{

    /**
     * @var int $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var int $fId
     * @access public
     */
    public $fId = null;

    /**
     * @param int $Id
     * @param int $fId
     * @access public
     */
    public function __construct($Id, $fId)
    {
      $this->Id = $Id;
      $this->fId = $fId;
    }

}
