<?php

class PublishContent
{

    /**
     * @var int $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var int $FolderId
     * @access public
     */
    public $FolderId = null;

    /**
     * @var int $language
     * @access public
     */
    public $language = null;

    /**
     * @param int $Id
     * @param int $FolderId
     * @param int $language
     * @access public
     */
    public function __construct($Id, $FolderId, $language)
    {
      $this->Id = $Id;
      $this->FolderId = $FolderId;
      $this->language = $language;
    }

}
