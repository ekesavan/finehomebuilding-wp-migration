<?php

class QuickDeployContent
{

    /**
     * @var string $contentIDs
     * @access public
     */
    public $contentIDs = null;

    /**
     * @param string $contentIDs
     * @access public
     */
    public function __construct($contentIDs)
    {
      $this->contentIDs = $contentIDs;
    }

}
