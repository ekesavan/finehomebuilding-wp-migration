<?php

class GetChildFoldersResponse
{

    /**
     * @var FolderReturn[] $GetChildFoldersResult
     * @access public
     */
    public $GetChildFoldersResult = null;

    /**
     * @param FolderReturn[] $GetChildFoldersResult
     * @access public
     */
    public function __construct($GetChildFoldersResult)
    {
      $this->GetChildFoldersResult = $GetChildFoldersResult;
    }

}
