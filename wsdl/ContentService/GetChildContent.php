<?php

class GetChildContent
{

    /**
     * @var int $folderId
     * @access public
     */
    public $folderId = null;

    /**
     * @var int $language
     * @access public
     */
    public $language = null;

    /**
     * @var boolean $summary
     * @access public
     */
    public $summary = null;

    /**
     * @var int $currentPage
     * @access public
     */
    public $currentPage = null;

    /**
     * @param int $folderId
     * @param int $language
     * @param boolean $summary
     * @param int $currentPage
     * @access public
     */
    public function __construct($folderId, $language, $summary, $currentPage)
    {
      $this->folderId = $folderId;
      $this->language = $language;
      $this->summary = $summary;
      $this->currentPage = $currentPage;
    }

}
