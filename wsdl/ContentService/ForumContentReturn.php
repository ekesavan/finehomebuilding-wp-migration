<?php

class ForumContentReturn
{

    /**
     * @var ContentData $ContentDataItem
     * @access public
     */
    public $ContentDataItem = null;

    /**
     * @var string $DomainProduction
     * @access public
     */
    public $DomainProduction = null;

    /**
     * @var ReplyItem[] $ReplyArray
     * @access public
     */
    public $ReplyArray = null;

    /**
     * @var string $FileName
     * @access public
     */
    public $FileName = null;

    /**
     * @var string $Quicklink
     * @access public
     */
    public $Quicklink = null;

    /**
     * @var string $BlogId
     * @access public
     */
    public $BlogId = null;

    /**
     * @var int $CurrentPage
     * @access public
     */
    public $CurrentPage = null;

    /**
     * @var int $TotalPages
     * @access public
     */
    public $TotalPages = null;

    /**
     * @param ContentData $ContentDataItem
     * @param string $DomainProduction
     * @param ReplyItem[] $ReplyArray
     * @param string $FileName
     * @param string $Quicklink
     * @param string $BlogId
     * @param int $CurrentPage
     * @param int $TotalPages
     * @access public
     */
    public function __construct($ContentDataItem, $DomainProduction, $ReplyArray, $FileName, $Quicklink, $BlogId, $CurrentPage, $TotalPages)
    {
      $this->ContentDataItem = $ContentDataItem;
      $this->DomainProduction = $DomainProduction;
      $this->ReplyArray = $ReplyArray;
      $this->FileName = $FileName;
      $this->Quicklink = $Quicklink;
      $this->BlogId = $BlogId;
      $this->CurrentPage = $CurrentPage;
      $this->TotalPages = $TotalPages;
    }

}
