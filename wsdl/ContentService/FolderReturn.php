<?php

class FolderReturn
{

    /**
     * @var FolderData $FolderDataItem
     * @access public
     */
    public $FolderDataItem = null;

    /**
     * @var string $FileTypes
     * @access public
     */
    public $FileTypes = null;

    /**
     * @var string $MachineName
     * @access public
     */
    public $MachineName = null;

    /**
     * @var string $XmlTitle
     * @access public
     */
    public $XmlTitle = null;

    /**
     * @var string $errorMsg
     * @access public
     */
    public $errorMsg = null;

    /**
     * @var boolean $isMembership
     * @access public
     */
    public $isMembership = null;

    /**
     * @var boolean $isXmlReq
     * @access public
     */
    public $isXmlReq = null;

    /**
     * @param FolderData $FolderDataItem
     * @param string $FileTypes
     * @param string $MachineName
     * @param string $XmlTitle
     * @param string $errorMsg
     * @param boolean $isMembership
     * @param boolean $isXmlReq
     * @access public
     */
    public function __construct($FolderDataItem, $FileTypes, $MachineName, $XmlTitle, $errorMsg, $isMembership, $isXmlReq)
    {
      $this->FolderDataItem = $FolderDataItem;
      $this->FileTypes = $FileTypes;
      $this->MachineName = $MachineName;
      $this->XmlTitle = $XmlTitle;
      $this->errorMsg = $errorMsg;
      $this->isMembership = $isMembership;
      $this->isXmlReq = $isXmlReq;
    }

}
