<?php

class EktOfficeNewFolder
{

    /**
     * @var string $id
     * @access public
     */
    public $id = null;

    /**
     * @var string $title
     * @access public
     */
    public $title = null;

    /**
     * @param string $id
     * @param string $title
     * @access public
     */
    public function __construct($id, $title)
    {
      $this->id = $id;
      $this->title = $title;
    }

}
