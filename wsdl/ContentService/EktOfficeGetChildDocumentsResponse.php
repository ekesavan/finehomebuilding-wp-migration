<?php

class EktOfficeGetChildDocumentsResponse
{

    /**
     * @var ODR[] $EktOfficeGetChildDocumentsResult
     * @access public
     */
    public $EktOfficeGetChildDocumentsResult = null;

    /**
     * @param ODR[] $EktOfficeGetChildDocumentsResult
     * @access public
     */
    public function __construct($EktOfficeGetChildDocumentsResult)
    {
      $this->EktOfficeGetChildDocumentsResult = $EktOfficeGetChildDocumentsResult;
    }

}
