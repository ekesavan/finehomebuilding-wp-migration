<?php

class ApproveContentResponse
{

    /**
     * @var boolean $ApproveContentResult
     * @access public
     */
    public $ApproveContentResult = null;

    /**
     * @param boolean $ApproveContentResult
     * @access public
     */
    public function __construct($ApproveContentResult)
    {
      $this->ApproveContentResult = $ApproveContentResult;
    }

}
