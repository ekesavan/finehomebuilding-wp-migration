<?php

class EktOfficeGetHistoryResponse
{

    /**
     * @var HIS[] $EktOfficeGetHistoryResult
     * @access public
     */
    public $EktOfficeGetHistoryResult = null;

    /**
     * @param HIS[] $EktOfficeGetHistoryResult
     * @access public
     */
    public function __construct($EktOfficeGetHistoryResult)
    {
      $this->EktOfficeGetHistoryResult = $EktOfficeGetHistoryResult;
    }

}
