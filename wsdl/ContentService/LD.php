<?php

class LD
{

    /**
     * @var string $n
     * @access public
     */
    public $n = null;

    /**
     * @var string $i
     * @access public
     */
    public $i = null;

    /**
     * @var boolean $d
     * @access public
     */
    public $d = null;

    /**
     * @param string $n
     * @param string $i
     * @param boolean $d
     * @access public
     */
    public function __construct($n, $i, $d)
    {
      $this->n = $n;
      $this->i = $i;
      $this->d = $d;
    }

}
