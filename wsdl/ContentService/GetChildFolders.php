<?php

class GetChildFolders
{

    /**
     * @var int $folderId
     * @access public
     */
    public $folderId = null;

    /**
     * @param int $folderId
     * @access public
     */
    public function __construct($folderId)
    {
      $this->folderId = $folderId;
    }

}
