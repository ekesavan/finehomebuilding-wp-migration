<?php

class GetAllLanguagesResponse
{

    /**
     * @var LanguageData[] $GetAllLanguagesResult
     * @access public
     */
    public $GetAllLanguagesResult = null;

    /**
     * @param LanguageData[] $GetAllLanguagesResult
     * @access public
     */
    public function __construct($GetAllLanguagesResult)
    {
      $this->GetAllLanguagesResult = $GetAllLanguagesResult;
    }

}
