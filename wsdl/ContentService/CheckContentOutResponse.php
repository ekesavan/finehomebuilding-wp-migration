<?php

class CheckContentOutResponse
{

    /**
     * @var boolean $CheckContentOutResult
     * @access public
     */
    public $CheckContentOutResult = null;

    /**
     * @param boolean $CheckContentOutResult
     * @access public
     */
    public function __construct($CheckContentOutResult)
    {
      $this->CheckContentOutResult = $CheckContentOutResult;
    }

}
