<?php

class MoveContentResponse
{

    /**
     * @var ContentData $MoveContentResult
     * @access public
     */
    public $MoveContentResult = null;

    /**
     * @param ContentData $MoveContentResult
     * @access public
     */
    public function __construct($MoveContentResult)
    {
      $this->MoveContentResult = $MoveContentResult;
    }

}
