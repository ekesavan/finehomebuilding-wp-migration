<?php

class EktOfficeGetFoldersAndDocuments
{

    /**
     * @var string $folderId
     * @access public
     */
    public $folderId = null;

    /**
     * @var string $extensions
     * @access public
     */
    public $extensions = null;

    /**
     * @var string $languageId
     * @access public
     */
    public $languageId = null;

    /**
     * @param string $folderId
     * @param string $extensions
     * @param string $languageId
     * @access public
     */
    public function __construct($folderId, $extensions, $languageId)
    {
      $this->folderId = $folderId;
      $this->extensions = $extensions;
      $this->languageId = $languageId;
    }

}
