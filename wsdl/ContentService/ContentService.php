<?php

include_once('DoLogin.php');
include_once('DoLoginResponse.php');
include_once('FirePreSyncStrategies.php');
include_once('FirePreSyncStrategiesResponse.php');
include_once('DoLogout.php');
include_once('DoLogoutResponse.php');
include_once('GetContent.php');
include_once('GetContentResponse.php');
include_once('ContentReturn.php');
include_once('ContentData.php');
include_once('ContentBaseData.php');
include_once('CmsLocalizedDataOfContentBaseData.php');
include_once('CmsDataOfContentBaseData.php');
include_once('BaseDataOfContentBaseData.php');
include_once('CMSContentSubtype.php');
include_once('AssetData.php');
//include_once('ContentMetaData.php');
//include_once('CmsDataOfContentMetaData.php');
//include_once('BaseDataOfContentMetaData.php');
//include_once('ContentMetadataDataType.php');
//include_once('ContentMetadataType.php');
include_once('CMSEndDateAction.php');
//include_once('XmlConfigData.php');
//include_once('TemplateData.php');
//include_once('CmsDataOfTemplateData.php');
//include_once('BaseDataOfTemplateData.php');
//include_once('TemplateType.php');
//include_once('TemplateSubType.php');
//include_once('GetFolder.php');
//include_once('GetFolderResponse.php');
include_once('FolderReturn.php');
//include_once('FolderData.php');
//include_once('CmsDataOfFolderData.php');
//include_once('BaseDataOfFolderData.php');
//include_once('FolderApprovalType.php');
//include_once('PermissionData.php');
//include_once('CmsDataOfPermissionData.php');
//include_once('BaseDataOfPermissionData.php');
//include_once('FolderType.php');
//include_once('SitemapPath.php');
//include_once('TaxonomyBaseData.php');
//include_once('CmsDataOfTaxonomyBaseData.php');
//include_once('BaseDataOfTaxonomyBaseData.php');
//include_once('TaxonomyType.php');
//include_once('FlagDefData.php');
//include_once('CmsDataOfFlagDefData.php');
//include_once('BaseDataOfFlagDefData.php');
//include_once('FlagItemData.php');
//include_once('GetChildFolders.php');
//include_once('GetChildFoldersResponse.php');
//include_once('GetFavoritesFolder.php');
//include_once('GetFavoritesFolderResponse.php');
include_once('AddContent.php');
include_once('AddContentResponse.php');
include_once('DeleteContent.php');
include_once('DeleteContentResponse.php');
//include_once('DeleteFolder.php');
//include_once('DeleteFolderResponse.php');
include_once('RenameContent.php');
include_once('RenameContentResponse.php');
include_once('ApproveContent.php');
include_once('ApproveContentResponse.php');
include_once('RestoreHistroy.php');
include_once('RestoreHistroyResponse.php');
include_once('DeclineContent.php');
include_once('DeclineContentResponse.php');
//include_once('RenameFolder.php');
//include_once('RenameFolderResponse.php');
include_once('MoveContent.php');
include_once('MoveContentResponse.php');
include_once('CopyContent.php');
include_once('CopyContentResponse.php');
include_once('CheckContentOut.php');
include_once('CheckContentOutResponse.php');
include_once('IsContentCheckedOut.php');
include_once('IsContentCheckedOutResponse.php');
include_once('CanAddToFolder.php');
include_once('CanAddToFolderResponse.php');
include_once('UndoCheckOut.php');
include_once('UndoCheckOutResponse.php');
include_once('PublishContent.php');
include_once('PublishContentResponse.php');
include_once('GetChildContent.php');
include_once('ChildContentReturn.php');
include_once('MetaDataPairs.php');
include_once('GetChildContentResponse.php');
include_once('DoLoginForUpload.php');
include_once('LoginUpload.php');
include_once('DoLoginForUploadResponse.php');
include_once('GetForumChildContent.php');
include_once('ForumContentReturn.php');
include_once('ReplyItem.php');
include_once('GetForumChildContentResponse.php');
include_once('RemoveTask.php');
include_once('RemoveTaskResponse.php');
include_once('GetFavoritesContent.php');
include_once('GetFavoritesContentResponse.php');
include_once('GetAllLanguages.php');
include_once('LanguageData.php');
include_once('GetAllLanguagesResponse.php');
include_once('EktOfficeGetAllLanguages.php');
include_once('LD.php');
include_once('EktOfficeGetAllLanguagesResponse.php');
include_once('QuickDeployContent.php');
include_once('QuickDeployContentResponse.php');
include_once('EktOfficeGetChildFolders.php');
include_once('OFR.php');
include_once('EktOfficeGetChildFoldersResponse.php');
include_once('EktOfficeGetChildDocuments.php');
include_once('ODR.php');
include_once('EktOfficeGetChildDocumentsResponse.php');
include_once('EktOfficeGetFoldersAndDocuments.php');
include_once('OFD.php');
include_once('EktOfficeGetFoldersAndDocumentsResponse.php');
include_once('EktOfficeGetDocument.php');
include_once('EktOfficeGetDocumentResponse.php');
include_once('EktOfficeGetDocumentByAssetID.php');
include_once('EktOfficeGetDocumentByAssetIDResponse.php');
include_once('EktOfficeGetDocumentQuicklink.php');
include_once('EktOfficeGetDocumentQuicklinkResponse.php');
include_once('EktOfficeGetFileExtensions.php');
include_once('EktOfficeGetFileExtensionsResponse.php');
include_once('EktOfficeGetSnippet.php');
include_once('EktOfficeGetSnippetResponse.php');
include_once('EktOfficeGetParentId.php');
include_once('EktOfficeGetParentIdResponse.php');
include_once('EktOfficeUserLogin.php');
include_once('EktOfficeUserLoginResponse.php');
include_once('EktOfficeGetHistory.php');
include_once('HIS.php');
include_once('EktOfficeGetHistoryResponse.php');
include_once('EktOfficeSearchHistory.php');
include_once('EktOfficeSearchHistoryResponse.php');
include_once('EktOfficeNewFolder.php');
include_once('EktOfficeNewFolderResponse.php');

class ContentService extends \EktronSoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'DoLogin' => '\DoLogin',
      'DoLoginResponse' => '\DoLoginResponse',
      'FirePreSyncStrategies' => '\FirePreSyncStrategies',
      'FirePreSyncStrategiesResponse' => '\FirePreSyncStrategiesResponse',
      'DoLogout' => '\DoLogout',
      'DoLogoutResponse' => '\DoLogoutResponse',
      'GetContent' => '\GetContent',
      'GetContentResponse' => '\GetContentResponse',
      'ContentReturn' => '\ContentReturn',
      'ContentData' => '\ContentData',
      'ContentBaseData' => '\ContentBaseData',
      'CmsLocalizedDataOfContentBaseData' => '\CmsLocalizedDataOfContentBaseData',
      'CmsDataOfContentBaseData' => '\CmsDataOfContentBaseData',
      'BaseDataOfContentBaseData' => '\BaseDataOfContentBaseData',
      'AssetData' => '\AssetData',
      'ContentMetaData' => '\ContentMetaData',
      'CmsDataOfContentMetaData' => '\CmsDataOfContentMetaData',
      'BaseDataOfContentMetaData' => '\BaseDataOfContentMetaData',
      'XmlConfigData' => '\XmlConfigData',
      'TemplateData' => '\TemplateData',
      'CmsDataOfTemplateData' => '\CmsDataOfTemplateData',
      'BaseDataOfTemplateData' => '\BaseDataOfTemplateData',
      'GetFolder' => '\GetFolder',
      'GetFolderResponse' => '\GetFolderResponse',
      'FolderReturn' => '\FolderReturn',
      'FolderData' => '\FolderData',
      'CmsDataOfFolderData' => '\CmsDataOfFolderData',
      'BaseDataOfFolderData' => '\BaseDataOfFolderData',
      'PermissionData' => '\PermissionData',
      'CmsDataOfPermissionData' => '\CmsDataOfPermissionData',
      'BaseDataOfPermissionData' => '\BaseDataOfPermissionData',
      'SitemapPath' => '\SitemapPath',
      'TaxonomyBaseData' => '\TaxonomyBaseData',
      'CmsDataOfTaxonomyBaseData' => '\CmsDataOfTaxonomyBaseData',
      'BaseDataOfTaxonomyBaseData' => '\BaseDataOfTaxonomyBaseData',
      'FlagDefData' => '\FlagDefData',
      'CmsDataOfFlagDefData' => '\CmsDataOfFlagDefData',
      'BaseDataOfFlagDefData' => '\BaseDataOfFlagDefData',
      'FlagItemData' => '\FlagItemData',
      'GetChildFolders' => '\GetChildFolders',
      'GetChildFoldersResponse' => '\GetChildFoldersResponse',
      'GetFavoritesFolder' => '\GetFavoritesFolder',
      'GetFavoritesFolderResponse' => '\GetFavoritesFolderResponse',
      'AddContent' => '\AddContent',
      'AddContentResponse' => '\AddContentResponse',
      'DeleteContent' => '\DeleteContent',
      'DeleteContentResponse' => '\DeleteContentResponse',
      'DeleteFolder' => '\DeleteFolder',
      'DeleteFolderResponse' => '\DeleteFolderResponse',
      'RenameContent' => '\RenameContent',
      'RenameContentResponse' => '\RenameContentResponse',
      'ApproveContent' => '\ApproveContent',
      'ApproveContentResponse' => '\ApproveContentResponse',
      'RestoreHistroy' => '\RestoreHistroy',
      'RestoreHistroyResponse' => '\RestoreHistroyResponse',
      'DeclineContent' => '\DeclineContent',
      'DeclineContentResponse' => '\DeclineContentResponse',
      'RenameFolder' => '\RenameFolder',
      'RenameFolderResponse' => '\RenameFolderResponse',
      'MoveContent' => '\MoveContent',
      'MoveContentResponse' => '\MoveContentResponse',
      'CopyContent' => '\CopyContent',
      'CopyContentResponse' => '\CopyContentResponse',
      'CheckContentOut' => '\CheckContentOut',
      'CheckContentOutResponse' => '\CheckContentOutResponse',
      'IsContentCheckedOut' => '\IsContentCheckedOut',
      'IsContentCheckedOutResponse' => '\IsContentCheckedOutResponse',
      'CanAddToFolder' => '\CanAddToFolder',
      'CanAddToFolderResponse' => '\CanAddToFolderResponse',
      'UndoCheckOut' => '\UndoCheckOut',
      'UndoCheckOutResponse' => '\UndoCheckOutResponse',
      'PublishContent' => '\PublishContent',
      'PublishContentResponse' => '\PublishContentResponse',
      'GetChildContent' => '\GetChildContent',
      'ChildContentReturn' => '\ChildContentReturn',
      'MetaDataPairs' => '\MetaDataPairs',
      'GetChildContentResponse' => '\GetChildContentResponse',
      'DoLoginForUpload' => '\DoLoginForUpload',
      'LoginUpload' => '\LoginUpload',
      'DoLoginForUploadResponse' => '\DoLoginForUploadResponse',
      'GetForumChildContent' => '\GetForumChildContent',
      'ForumContentReturn' => '\ForumContentReturn',
      'ReplyItem' => '\ReplyItem',
      'GetForumChildContentResponse' => '\GetForumChildContentResponse',
      'RemoveTask' => '\RemoveTask',
      'RemoveTaskResponse' => '\RemoveTaskResponse',
      'GetFavoritesContent' => '\GetFavoritesContent',
      'GetFavoritesContentResponse' => '\GetFavoritesContentResponse',
      'GetAllLanguages' => '\GetAllLanguages',
      'LanguageData' => '\LanguageData',
      'GetAllLanguagesResponse' => '\GetAllLanguagesResponse',
      'EktOfficeGetAllLanguages' => '\EktOfficeGetAllLanguages',
      'LD' => '\LD',
      'EktOfficeGetAllLanguagesResponse' => '\EktOfficeGetAllLanguagesResponse',
      'QuickDeployContent' => '\QuickDeployContent',
      'QuickDeployContentResponse' => '\QuickDeployContentResponse',
      'EktOfficeGetChildFolders' => '\EktOfficeGetChildFolders',
      'OFR' => '\OFR',
      'EktOfficeGetChildFoldersResponse' => '\EktOfficeGetChildFoldersResponse',
      'EktOfficeGetChildDocuments' => '\EktOfficeGetChildDocuments',
      'ODR' => '\ODR',
      'EktOfficeGetChildDocumentsResponse' => '\EktOfficeGetChildDocumentsResponse',
      'EktOfficeGetFoldersAndDocuments' => '\EktOfficeGetFoldersAndDocuments',
      'OFD' => '\OFD',
      'EktOfficeGetFoldersAndDocumentsResponse' => '\EktOfficeGetFoldersAndDocumentsResponse',
      'EktOfficeGetDocument' => '\EktOfficeGetDocument',
      'EktOfficeGetDocumentResponse' => '\EktOfficeGetDocumentResponse',
      'EktOfficeGetDocumentByAssetID' => '\EktOfficeGetDocumentByAssetID',
      'EktOfficeGetDocumentByAssetIDResponse' => '\EktOfficeGetDocumentByAssetIDResponse',
      'EktOfficeGetDocumentQuicklink' => '\EktOfficeGetDocumentQuicklink',
      'EktOfficeGetDocumentQuicklinkResponse' => '\EktOfficeGetDocumentQuicklinkResponse',
      'EktOfficeGetFileExtensions' => '\EktOfficeGetFileExtensions',
      'EktOfficeGetFileExtensionsResponse' => '\EktOfficeGetFileExtensionsResponse',
      'EktOfficeGetSnippet' => '\EktOfficeGetSnippet',
      'EktOfficeGetSnippetResponse' => '\EktOfficeGetSnippetResponse',
      'EktOfficeGetParentId' => '\EktOfficeGetParentId',
      'EktOfficeGetParentIdResponse' => '\EktOfficeGetParentIdResponse',
      'EktOfficeUserLogin' => '\EktOfficeUserLogin',
      'EktOfficeUserLoginResponse' => '\EktOfficeUserLoginResponse',
      'EktOfficeGetHistory' => '\EktOfficeGetHistory',
      'HIS' => '\HIS',
      'EktOfficeGetHistoryResponse' => '\EktOfficeGetHistoryResponse',
      'EktOfficeSearchHistory' => '\EktOfficeSearchHistory',
      'EktOfficeSearchHistoryResponse' => '\EktOfficeSearchHistoryResponse',
      'EktOfficeNewFolder' => '\EktOfficeNewFolder',
      'EktOfficeNewFolderResponse' => '\EktOfficeNewFolderResponse');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = 'wsdl/ContentService.wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      
      parent::__construct($wsdl, $options);
    }

    /**
     * @param DoLogin $parameters
     * @access public
     * @return DoLoginResponse
     */
    public function DoLogin(DoLogin $parameters)
    {
      return $this->__soapCall('DoLogin', array($parameters));
    }

    /**
     * @param FirePreSyncStrategies $parameters
     * @access public
     * @return FirePreSyncStrategiesResponse
     */
    public function FirePreSyncStrategies(FirePreSyncStrategies $parameters)
    {
      return $this->__soapCall('FirePreSyncStrategies', array($parameters));
    }

    /**
     * @param DoLogout $parameters
     * @access public
     * @return DoLogoutResponse
     */
    public function DoLogout(DoLogout $parameters)
    {
      return $this->__soapCall('DoLogout', array($parameters));
    }

    /**
     * @param GetContent $parameters
     * @access public
     * @return GetContentResponse
     */
    public function GetContent(GetContent $parameters)
    {
      return $this->__soapCall('GetContent', array($parameters));
    }

    /**
     * @param GetFolder $parameters
     * @access public
     * @return GetFolderResponse
     */
    public function GetFolder(GetFolder $parameters)
    {
      return $this->__soapCall('GetFolder', array($parameters));
    }

    /**
     * @param GetChildFolders $parameters
     * @access public
     * @return GetChildFoldersResponse
     */
    public function GetChildFolders(GetChildFolders $parameters)
    {
      return $this->__soapCall('GetChildFolders', array($parameters));
    }

    /**
     * @param GetFavoritesFolder $parameters
     * @access public
     * @return GetFavoritesFolderResponse
     */
    public function GetFavoritesFolder(GetFavoritesFolder $parameters)
    {
      return $this->__soapCall('GetFavoritesFolder', array($parameters));
    }

    /**
     * @param AddContent $parameters
     * @access public
     * @return AddContentResponse
     */
    public function AddContent(AddContent $parameters)
    {
      return $this->__soapCall('AddContent', array($parameters));
    }

    /**
     * @param DeleteContent $parameters
     * @access public
     * @return DeleteContentResponse
     */
    public function DeleteContent(DeleteContent $parameters)
    {
      return $this->__soapCall('DeleteContent', array($parameters));
    }

    /**
     * @param DeleteFolder $parameters
     * @access public
     * @return DeleteFolderResponse
     */
    public function DeleteFolder(DeleteFolder $parameters)
    {
      return $this->__soapCall('DeleteFolder', array($parameters));
    }

    /**
     * @param RenameContent $parameters
     * @access public
     * @return RenameContentResponse
     */
    public function RenameContent(RenameContent $parameters)
    {
      return $this->__soapCall('RenameContent', array($parameters));
    }

    /**
     * @param ApproveContent $parameters
     * @access public
     * @return ApproveContentResponse
     */
    public function ApproveContent(ApproveContent $parameters)
    {
      return $this->__soapCall('ApproveContent', array($parameters));
    }

    /**
     * @param RestoreHistroy $parameters
     * @access public
     * @return RestoreHistroyResponse
     */
    public function RestoreHistroy(RestoreHistroy $parameters)
    {
      return $this->__soapCall('RestoreHistroy', array($parameters));
    }

    /**
     * @param DeclineContent $parameters
     * @access public
     * @return DeclineContentResponse
     */
    public function DeclineContent(DeclineContent $parameters)
    {
      return $this->__soapCall('DeclineContent', array($parameters));
    }

    /**
     * @param RenameFolder $parameters
     * @access public
     * @return RenameFolderResponse
     */
    public function RenameFolder(RenameFolder $parameters)
    {
      return $this->__soapCall('RenameFolder', array($parameters));
    }

    /**
     * @param MoveContent $parameters
     * @access public
     * @return MoveContentResponse
     */
    public function MoveContent(MoveContent $parameters)
    {
      return $this->__soapCall('MoveContent', array($parameters));
    }

    /**
     * @param CopyContent $parameters
     * @access public
     * @return CopyContentResponse
     */
    public function CopyContent(CopyContent $parameters)
    {
      return $this->__soapCall('CopyContent', array($parameters));
    }

    /**
     * @param CheckContentOut $parameters
     * @access public
     * @return CheckContentOutResponse
     */
    public function CheckContentOut(CheckContentOut $parameters)
    {
      return $this->__soapCall('CheckContentOut', array($parameters));
    }

    /**
     * @param IsContentCheckedOut $parameters
     * @access public
     * @return IsContentCheckedOutResponse
     */
    public function IsContentCheckedOut(IsContentCheckedOut $parameters)
    {
      return $this->__soapCall('IsContentCheckedOut', array($parameters));
    }

    /**
     * @param CanAddToFolder $parameters
     * @access public
     * @return CanAddToFolderResponse
     */
    public function CanAddToFolder(CanAddToFolder $parameters)
    {
      return $this->__soapCall('CanAddToFolder', array($parameters));
    }

    /**
     * @param UndoCheckOut $parameters
     * @access public
     * @return UndoCheckOutResponse
     */
    public function UndoCheckOut(UndoCheckOut $parameters)
    {
      return $this->__soapCall('UndoCheckOut', array($parameters));
    }

    /**
     * @param PublishContent $parameters
     * @access public
     * @return PublishContentResponse
     */
    public function PublishContent(PublishContent $parameters)
    {
      return $this->__soapCall('PublishContent', array($parameters));
    }

    /**
     * @param GetChildContent $parameters
     * @access public
     * @return GetChildContentResponse
     */
    public function GetChildContent(GetChildContent $parameters)
    {
      return $this->__soapCall('GetChildContent', array($parameters));
    }

    /**
     * @param DoLoginForUpload $parameters
     * @access public
     * @return DoLoginForUploadResponse
     */
    public function DoLoginForUpload(DoLoginForUpload $parameters)
    {
      return $this->__soapCall('DoLoginForUpload', array($parameters));
    }

    /**
     * @param GetForumChildContent $parameters
     * @access public
     * @return GetForumChildContentResponse
     */
    public function GetForumChildContent(GetForumChildContent $parameters)
    {
      return $this->__soapCall('GetForumChildContent', array($parameters));
    }

    /**
     * @param RemoveTask $parameters
     * @access public
     * @return RemoveTaskResponse
     */
    public function RemoveTask(RemoveTask $parameters)
    {
      return $this->__soapCall('RemoveTask', array($parameters));
    }

    /**
     * @param GetFavoritesContent $parameters
     * @access public
     * @return GetFavoritesContentResponse
     */
    public function GetFavoritesContent(GetFavoritesContent $parameters)
    {
      return $this->__soapCall('GetFavoritesContent', array($parameters));
    }

    /**
     * @param GetAllLanguages $parameters
     * @access public
     * @return GetAllLanguagesResponse
     */
    public function GetAllLanguages(GetAllLanguages $parameters)
    {
      return $this->__soapCall('GetAllLanguages', array($parameters));
    }

    /**
     * @param EktOfficeGetAllLanguages $parameters
     * @access public
     * @return EktOfficeGetAllLanguagesResponse
     */
    public function EktOfficeGetAllLanguages(EktOfficeGetAllLanguages $parameters)
    {
      return $this->__soapCall('EktOfficeGetAllLanguages', array($parameters));
    }

    /**
     * @param QuickDeployContent $parameters
     * @access public
     * @return QuickDeployContentResponse
     */
    public function QuickDeployContent(QuickDeployContent $parameters)
    {
      return $this->__soapCall('QuickDeployContent', array($parameters));
    }

    /**
     * @param EktOfficeGetChildFolders $parameters
     * @access public
     * @return EktOfficeGetChildFoldersResponse
     */
    public function EktOfficeGetChildFolders(EktOfficeGetChildFolders $parameters)
    {
      return $this->__soapCall('EktOfficeGetChildFolders', array($parameters));
    }

    /**
     * @param EktOfficeGetChildDocuments $parameters
     * @access public
     * @return EktOfficeGetChildDocumentsResponse
     */
    public function EktOfficeGetChildDocuments(EktOfficeGetChildDocuments $parameters)
    {
      return $this->__soapCall('EktOfficeGetChildDocuments', array($parameters));
    }

    /**
     * @param EktOfficeGetFoldersAndDocuments $parameters
     * @access public
     * @return EktOfficeGetFoldersAndDocumentsResponse
     */
    public function EktOfficeGetFoldersAndDocuments(EktOfficeGetFoldersAndDocuments $parameters)
    {
      return $this->__soapCall('EktOfficeGetFoldersAndDocuments', array($parameters));
    }

    /**
     * @param EktOfficeGetDocument $parameters
     * @access public
     * @return EktOfficeGetDocumentResponse
     */
    public function EktOfficeGetDocument(EktOfficeGetDocument $parameters)
    {
      return $this->__soapCall('EktOfficeGetDocument', array($parameters));
    }

    /**
     * @param EktOfficeGetDocumentByAssetID $parameters
     * @access public
     * @return EktOfficeGetDocumentByAssetIDResponse
     */
    public function EktOfficeGetDocumentByAssetID(EktOfficeGetDocumentByAssetID $parameters)
    {
      return $this->__soapCall('EktOfficeGetDocumentByAssetID', array($parameters));
    }

    /**
     * @param EktOfficeGetDocumentQuicklink $parameters
     * @access public
     * @return EktOfficeGetDocumentQuicklinkResponse
     */
    public function EktOfficeGetDocumentQuicklink(EktOfficeGetDocumentQuicklink $parameters)
    {
      return $this->__soapCall('EktOfficeGetDocumentQuicklink', array($parameters));
    }

    /**
     * @param EktOfficeGetFileExtensions $parameters
     * @access public
     * @return EktOfficeGetFileExtensionsResponse
     */
    public function EktOfficeGetFileExtensions(EktOfficeGetFileExtensions $parameters)
    {
      return $this->__soapCall('EktOfficeGetFileExtensions', array($parameters));
    }

    /**
     * @param EktOfficeGetSnippet $parameters
     * @access public
     * @return EktOfficeGetSnippetResponse
     */
    public function EktOfficeGetSnippet(EktOfficeGetSnippet $parameters)
    {
      return $this->__soapCall('EktOfficeGetSnippet', array($parameters));
    }

    /**
     * @param EktOfficeGetParentId $parameters
     * @access public
     * @return EktOfficeGetParentIdResponse
     */
    public function EktOfficeGetParentId(EktOfficeGetParentId $parameters)
    {
      return $this->__soapCall('EktOfficeGetParentId', array($parameters));
    }

    /**
     * @param EktOfficeUserLogin $parameters
     * @access public
     * @return EktOfficeUserLoginResponse
     */
    public function EktOfficeUserLogin(EktOfficeUserLogin $parameters)
    {
      return $this->__soapCall('EktOfficeUserLogin', array($parameters));
    }

    /**
     * @param EktOfficeGetHistory $parameters
     * @access public
     * @return EktOfficeGetHistoryResponse
     */
    public function EktOfficeGetHistory(EktOfficeGetHistory $parameters)
    {
      return $this->__soapCall('EktOfficeGetHistory', array($parameters));
    }

    /**
     * @param EktOfficeSearchHistory $parameters
     * @access public
     * @return EktOfficeSearchHistoryResponse
     */
    public function EktOfficeSearchHistory(EktOfficeSearchHistory $parameters)
    {
      return $this->__soapCall('EktOfficeSearchHistory', array($parameters));
    }

    /**
     * @param EktOfficeNewFolder $parameters
     * @access public
     * @return EktOfficeNewFolderResponse
     */
    public function EktOfficeNewFolder(EktOfficeNewFolder $parameters)
    {
      return $this->__soapCall('EktOfficeNewFolder', array($parameters));
    }

}
