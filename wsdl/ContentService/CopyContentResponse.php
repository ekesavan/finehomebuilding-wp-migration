<?php

class CopyContentResponse
{

    /**
     * @var ContentData $CopyContentResult
     * @access public
     */
    public $CopyContentResult = null;

    /**
     * @param ContentData $CopyContentResult
     * @access public
     */
    public function __construct($CopyContentResult)
    {
      $this->CopyContentResult = $CopyContentResult;
    }

}
