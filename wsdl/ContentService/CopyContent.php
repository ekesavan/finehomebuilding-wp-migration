<?php

class CopyContent
{

    /**
     * @var int $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var int $Folder
     * @access public
     */
    public $Folder = null;

    /**
     * @param int $Id
     * @param int $Folder
     * @access public
     */
    public function __construct($Id, $Folder)
    {
      $this->Id = $Id;
      $this->Folder = $Folder;
    }

}
