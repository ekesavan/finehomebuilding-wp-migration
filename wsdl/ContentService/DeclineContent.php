<?php

class DeclineContent
{

    /**
     * @var int $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var int $language
     * @access public
     */
    public $language = null;

    /**
     * @param int $Id
     * @param int $language
     * @access public
     */
    public function __construct($Id, $language)
    {
      $this->Id = $Id;
      $this->language = $language;
    }

}
