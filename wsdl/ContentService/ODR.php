<?php

class ODR
{

    /**
     * @var string $n
     * @access public
     */
    public $n = null;

    /**
     * @var string $f
     * @access public
     */
    public $f = null;

    /**
     * @var string $a
     * @access public
     */
    public $a = null;

    /**
     * @var string $i
     * @access public
     */
    public $i = null;

    /**
     * @var boolean $e
     * @access public
     */
    public $e = null;

    /**
     * @param string $n
     * @param string $f
     * @param string $a
     * @param string $i
     * @param boolean $e
     * @access public
     */
    public function __construct($n, $f, $a, $i, $e)
    {
      $this->n = $n;
      $this->f = $f;
      $this->a = $a;
      $this->i = $i;
      $this->e = $e;
    }

}
