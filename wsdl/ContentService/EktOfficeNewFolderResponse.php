<?php

class EktOfficeNewFolderResponse
{

    /**
     * @var string $EktOfficeNewFolderResult
     * @access public
     */
    public $EktOfficeNewFolderResult = null;

    /**
     * @param string $EktOfficeNewFolderResult
     * @access public
     */
    public function __construct($EktOfficeNewFolderResult)
    {
      $this->EktOfficeNewFolderResult = $EktOfficeNewFolderResult;
    }

}
