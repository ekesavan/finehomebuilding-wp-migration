<?php

class CanAddToFolderResponse
{

    /**
     * @var boolean $CanAddToFolderResult
     * @access public
     */
    public $CanAddToFolderResult = null;

    /**
     * @param boolean $CanAddToFolderResult
     * @access public
     */
    public function __construct($CanAddToFolderResult)
    {
      $this->CanAddToFolderResult = $CanAddToFolderResult;
    }

}
