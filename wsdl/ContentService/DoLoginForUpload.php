<?php

class DoLoginForUpload
{

    /**
     * @var string $fileName
     * @access public
     */
    public $fileName = null;

    /**
     * @var int $folderId
     * @access public
     */
    public $folderId = null;

    /**
     * @var int $language
     * @access public
     */
    public $language = null;

    /**
     * @param string $fileName
     * @param int $folderId
     * @param int $language
     * @access public
     */
    public function __construct($fileName, $folderId, $language)
    {
      $this->fileName = $fileName;
      $this->folderId = $folderId;
      $this->language = $language;
    }

}
