<?php

class GetFolder
{

    /**
     * @var int $id
     * @access public
     */
    public $id = null;

    /**
     * @var boolean $root
     * @access public
     */
    public $root = null;

    /**
     * @param int $id
     * @param boolean $root
     * @access public
     */
    public function __construct($id, $root)
    {
      $this->id = $id;
      $this->root = $root;
    }

}
