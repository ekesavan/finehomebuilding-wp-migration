<?php

class EktOfficeGetDocumentQuicklinkResponse
{

    /**
     * @var string $EktOfficeGetDocumentQuicklinkResult
     * @access public
     */
    public $EktOfficeGetDocumentQuicklinkResult = null;

    /**
     * @param string $EktOfficeGetDocumentQuicklinkResult
     * @access public
     */
    public function __construct($EktOfficeGetDocumentQuicklinkResult)
    {
      $this->EktOfficeGetDocumentQuicklinkResult = $EktOfficeGetDocumentQuicklinkResult;
    }

}
