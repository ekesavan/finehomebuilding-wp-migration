<?php

class RestoreHistroyResponse
{

    /**
     * @var boolean $RestoreHistroyResult
     * @access public
     */
    public $RestoreHistroyResult = null;

    /**
     * @param boolean $RestoreHistroyResult
     * @access public
     */
    public function __construct($RestoreHistroyResult)
    {
      $this->RestoreHistroyResult = $RestoreHistroyResult;
    }

}
