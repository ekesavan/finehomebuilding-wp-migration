<?php

class EktOfficeGetParentId
{

    /**
     * @var string $folderId
     * @access public
     */
    public $folderId = null;

    /**
     * @param string $folderId
     * @access public
     */
    public function __construct($folderId)
    {
      $this->folderId = $folderId;
    }

}
