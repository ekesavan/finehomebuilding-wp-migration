<?php

class EktOfficeSearchHistory
{

    /**
     * @var string $id
     * @access public
     */
    public $id = null;

    /**
     * @var string $assetId
     * @access public
     */
    public $assetId = null;

    /**
     * @var string $searchText
     * @access public
     */
    public $searchText = null;

    /**
     * @var int $language
     * @access public
     */
    public $language = null;

    /**
     * @param string $id
     * @param string $assetId
     * @param string $searchText
     * @param int $language
     * @access public
     */
    public function __construct($id, $assetId, $searchText, $language)
    {
      $this->id = $id;
      $this->assetId = $assetId;
      $this->searchText = $searchText;
      $this->language = $language;
    }

}
