<?php

class SitemapPath
{

    /**
     * @var string $Title
     * @access public
     */
    public $Title = null;

    /**
     * @var string $Url
     * @access public
     */
    public $Url = null;

    /**
     * @var string $Description
     * @access public
     */
    public $Description = null;

    /**
     * @var int $Order
     * @access public
     */
    public $Order = null;

    /**
     * @var int $FolderId
     * @access public
     */
    public $FolderId = null;

    /**
     * @var int $ContentId
     * @access public
     */
    public $ContentId = null;

    /**
     * @var int $Language
     * @access public
     */
    public $Language = null;

    /**
     * @param string $Title
     * @param string $Url
     * @param string $Description
     * @param int $Order
     * @param int $FolderId
     * @param int $ContentId
     * @param int $Language
     * @access public
     */
    public function __construct($Title, $Url, $Description, $Order, $FolderId, $ContentId, $Language)
    {
      $this->Title = $Title;
      $this->Url = $Url;
      $this->Description = $Description;
      $this->Order = $Order;
      $this->FolderId = $FolderId;
      $this->ContentId = $ContentId;
      $this->Language = $Language;
    }

}
