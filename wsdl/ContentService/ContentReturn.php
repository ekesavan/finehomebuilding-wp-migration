<?php

class ContentReturn
{

    /**
     * @var ContentData $ContentDataItem
     * @access public
     */
    public $ContentDataItem = null;

    /**
     * @var string $DomainProduction
     * @access public
     */
    public $DomainProduction = null;

    /**
     * @var string $XmlTitle
     * @access public
     */
    public $XmlTitle = null;

    /**
     * @var string $CanApprove
     * @access public
     */
    public $CanApprove = null;

    /**
     * @param ContentData $ContentDataItem
     * @param string $DomainProduction
     * @param string $XmlTitle
     * @param string $CanApprove
     * @access public
     */
    public function __construct($ContentDataItem, $DomainProduction, $XmlTitle, $CanApprove)
    {
      $this->ContentDataItem = $ContentDataItem;
      $this->DomainProduction = $DomainProduction;
      $this->XmlTitle = $XmlTitle;
      $this->CanApprove = $CanApprove;
    }

}
