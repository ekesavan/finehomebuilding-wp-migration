<?php

class MetaDataPairs
{

    /**
     * @var string $name
     * @access public
     */
    public $name = null;

    /**
     * @var string $value
     * @access public
     */
    public $value = null;

    /**
     * @var string $type
     * @access public
     */
    public $type = null;

    /**
     * @param string $name
     * @param string $value
     * @param string $type
     * @access public
     */
    public function __construct($name, $value, $type)
    {
      $this->name = $name;
      $this->value = $value;
      $this->type = $type;
    }

}
