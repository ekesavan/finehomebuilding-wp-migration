<?php

class RemoveTaskResponse
{

    /**
     * @var boolean $RemoveTaskResult
     * @access public
     */
    public $RemoveTaskResult = null;

    /**
     * @param boolean $RemoveTaskResult
     * @access public
     */
    public function __construct($RemoveTaskResult)
    {
      $this->RemoveTaskResult = $RemoveTaskResult;
    }

}
