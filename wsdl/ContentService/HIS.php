<?php

class HIS
{

    /**
     * @var string $v
     * @access public
     */
    public $v = null;

    /**
     * @var string $p
     * @access public
     */
    public $p = null;

    /**
     * @var string $l
     * @access public
     */
    public $l = null;

    /**
     * @var string $t
     * @access public
     */
    public $t = null;

    /**
     * @var string $u
     * @access public
     */
    public $u = null;

    /**
     * @var string $c
     * @access public
     */
    public $c = null;

    /**
     * @var string $i
     * @access public
     */
    public $i = null;

    /**
     * @param string $v
     * @param string $p
     * @param string $l
     * @param string $t
     * @param string $u
     * @param string $c
     * @param string $i
     * @access public
     */
    public function __construct($v, $p, $l, $t, $u, $c, $i)
    {
      $this->v = $v;
      $this->p = $p;
      $this->l = $l;
      $this->t = $t;
      $this->u = $u;
      $this->c = $c;
      $this->i = $i;
    }

}
