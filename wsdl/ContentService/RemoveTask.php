<?php

class RemoveTask
{

    /**
     * @var string $taskId
     * @access public
     */
    public $taskId = null;

    /**
     * @param string $taskId
     * @access public
     */
    public function __construct($taskId)
    {
      $this->taskId = $taskId;
    }

}
