<?php

class EktOfficeUserLoginResponse
{

    /**
     * @var string $EktOfficeUserLoginResult
     * @access public
     */
    public $EktOfficeUserLoginResult = null;

    /**
     * @param string $EktOfficeUserLoginResult
     * @access public
     */
    public function __construct($EktOfficeUserLoginResult)
    {
      $this->EktOfficeUserLoginResult = $EktOfficeUserLoginResult;
    }

}
