<?php

class GetContent
{

    /**
     * @var int $id
     * @access public
     */
    public $id = null;

    /**
     * @var int $language
     * @access public
     */
    public $language = null;

    /**
     * @param int $id
     * @param int $language
     * @access public
     */
    public function __construct($id, $language)
    {
      $this->id = $id;
      $this->language = $language;
    }

}
