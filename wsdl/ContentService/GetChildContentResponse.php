<?php

class GetChildContentResponse
{

    /**
     * @var ChildContentReturn[] $GetChildContentResult
     * @access public
     */
    public $GetChildContentResult = null;

    /**
     * @param ChildContentReturn[] $GetChildContentResult
     * @access public
     */
    public function __construct($GetChildContentResult)
    {
      $this->GetChildContentResult = $GetChildContentResult;
    }

}
