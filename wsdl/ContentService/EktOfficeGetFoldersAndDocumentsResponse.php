<?php

class EktOfficeGetFoldersAndDocumentsResponse
{

    /**
     * @var OFD[] $EktOfficeGetFoldersAndDocumentsResult
     * @access public
     */
    public $EktOfficeGetFoldersAndDocumentsResult = null;

    /**
     * @param OFD[] $EktOfficeGetFoldersAndDocumentsResult
     * @access public
     */
    public function __construct($EktOfficeGetFoldersAndDocumentsResult)
    {
      $this->EktOfficeGetFoldersAndDocumentsResult = $EktOfficeGetFoldersAndDocumentsResult;
    }

}
