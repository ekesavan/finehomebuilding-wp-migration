<?php

include_once('CmsDataOfContentBaseData.php');

class CmsLocalizedDataOfContentBaseData extends CmsDataOfContentBaseData
{

    /**
     * @var int $LanguageId
     * @access public
     */
    public $LanguageId = null;

    /**
     * @param int $Id
     * @param int $LanguageId
     * @access public
     */
    public function __construct($Id, $LanguageId)
    {
      parent::__construct($Id);
      $this->LanguageId = $LanguageId;
    }

}
