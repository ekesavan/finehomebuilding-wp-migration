<?php

class GetForumChildContent
{

    /**
     * @var int $folderId
     * @access public
     */
    public $folderId = null;

    /**
     * @var int $language
     * @access public
     */
    public $language = null;

    /**
     * @var int $type
     * @access public
     */
    public $type = null;

    /**
     * @var int $currentPage
     * @access public
     */
    public $currentPage = null;

    /**
     * @param int $folderId
     * @param int $language
     * @param int $type
     * @param int $currentPage
     * @access public
     */
    public function __construct($folderId, $language, $type, $currentPage)
    {
      $this->folderId = $folderId;
      $this->language = $language;
      $this->type = $type;
      $this->currentPage = $currentPage;
    }

}
