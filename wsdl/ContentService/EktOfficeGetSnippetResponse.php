<?php

class EktOfficeGetSnippetResponse
{

    /**
     * @var string $EktOfficeGetSnippetResult
     * @access public
     */
    public $EktOfficeGetSnippetResult = null;

    /**
     * @param string $EktOfficeGetSnippetResult
     * @access public
     */
    public function __construct($EktOfficeGetSnippetResult)
    {
      $this->EktOfficeGetSnippetResult = $EktOfficeGetSnippetResult;
    }

}
