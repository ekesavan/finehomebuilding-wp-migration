<?php

class GetContentResponse
{

    /**
     * @var ContentReturn $GetContentResult
     * @access public
     */
    public $GetContentResult = null;

    /**
     * @param ContentReturn $GetContentResult
     * @access public
     */
    public function __construct($GetContentResult)
    {
      $this->GetContentResult = $GetContentResult;
    }

}
