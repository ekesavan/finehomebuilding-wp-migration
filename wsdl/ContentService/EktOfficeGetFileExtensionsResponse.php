<?php

class EktOfficeGetFileExtensionsResponse
{

    /**
     * @var string $EktOfficeGetFileExtensionsResult
     * @access public
     */
    public $EktOfficeGetFileExtensionsResult = null;

    /**
     * @param string $EktOfficeGetFileExtensionsResult
     * @access public
     */
    public function __construct($EktOfficeGetFileExtensionsResult)
    {
      $this->EktOfficeGetFileExtensionsResult = $EktOfficeGetFileExtensionsResult;
    }

}
