<?php

class IsContentCheckedOutResponse
{

    /**
     * @var boolean $IsContentCheckedOutResult
     * @access public
     */
    public $IsContentCheckedOutResult = null;

    /**
     * @param boolean $IsContentCheckedOutResult
     * @access public
     */
    public function __construct($IsContentCheckedOutResult)
    {
      $this->IsContentCheckedOutResult = $IsContentCheckedOutResult;
    }

}
