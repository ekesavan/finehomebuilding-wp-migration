<?php

class EktOfficeGetDocumentByAssetID
{

    /**
     * @var string $aId
     * @access public
     */
    public $aId = null;

    /**
     * @var int $language
     * @access public
     */
    public $language = null;

    /**
     * @param string $aId
     * @param int $language
     * @access public
     */
    public function __construct($aId, $language)
    {
      $this->aId = $aId;
      $this->language = $language;
    }

}
