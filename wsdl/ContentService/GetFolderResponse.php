<?php

class GetFolderResponse
{

    /**
     * @var FolderReturn $GetFolderResult
     * @access public
     */
    public $GetFolderResult = null;

    /**
     * @param FolderReturn $GetFolderResult
     * @access public
     */
    public function __construct($GetFolderResult)
    {
      $this->GetFolderResult = $GetFolderResult;
    }

}
