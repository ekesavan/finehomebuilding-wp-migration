<?php

class EktOfficeGetChildFoldersResponse
{

    /**
     * @var OFR[] $EktOfficeGetChildFoldersResult
     * @access public
     */
    public $EktOfficeGetChildFoldersResult = null;

    /**
     * @param OFR[] $EktOfficeGetChildFoldersResult
     * @access public
     */
    public function __construct($EktOfficeGetChildFoldersResult)
    {
      $this->EktOfficeGetChildFoldersResult = $EktOfficeGetChildFoldersResult;
    }

}
