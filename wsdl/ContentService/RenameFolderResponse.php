<?php

class RenameFolderResponse
{

    /**
     * @var boolean $RenameFolderResult
     * @access public
     */
    public $RenameFolderResult = null;

    /**
     * @param boolean $RenameFolderResult
     * @access public
     */
    public function __construct($RenameFolderResult)
    {
      $this->RenameFolderResult = $RenameFolderResult;
    }

}
