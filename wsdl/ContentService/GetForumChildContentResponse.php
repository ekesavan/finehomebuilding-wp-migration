<?php

class GetForumChildContentResponse
{

    /**
     * @var ForumContentReturn[] $GetForumChildContentResult
     * @access public
     */
    public $GetForumChildContentResult = null;

    /**
     * @param ForumContentReturn[] $GetForumChildContentResult
     * @access public
     */
    public function __construct($GetForumChildContentResult)
    {
      $this->GetForumChildContentResult = $GetForumChildContentResult;
    }

}
