<?php

class GetFavoritesFolderResponse
{

    /**
     * @var FolderData[] $GetFavoritesFolderResult
     * @access public
     */
    public $GetFavoritesFolderResult = null;

    /**
     * @param FolderData[] $GetFavoritesFolderResult
     * @access public
     */
    public function __construct($GetFavoritesFolderResult)
    {
      $this->GetFavoritesFolderResult = $GetFavoritesFolderResult;
    }

}
