<?php

class DoLoginResponse
{

    /**
     * @var boolean $DoLoginResult
     * @access public
     */
    public $DoLoginResult = null;

    /**
     * @param boolean $DoLoginResult
     * @access public
     */
    public function __construct($DoLoginResult)
    {
      $this->DoLoginResult = $DoLoginResult;
    }

}
