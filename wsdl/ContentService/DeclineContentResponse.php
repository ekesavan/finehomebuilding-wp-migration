<?php

class DeclineContentResponse
{

    /**
     * @var boolean $DeclineContentResult
     * @access public
     */
    public $DeclineContentResult = null;

    /**
     * @param boolean $DeclineContentResult
     * @access public
     */
    public function __construct($DeclineContentResult)
    {
      $this->DeclineContentResult = $DeclineContentResult;
    }

}
