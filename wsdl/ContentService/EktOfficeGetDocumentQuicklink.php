<?php

class EktOfficeGetDocumentQuicklink
{

    /**
     * @var string $cId
     * @access public
     */
    public $cId = null;

    /**
     * @var string $fId
     * @access public
     */
    public $fId = null;

    /**
     * @var string $language
     * @access public
     */
    public $language = null;

    /**
     * @param string $cId
     * @param string $fId
     * @param string $language
     * @access public
     */
    public function __construct($cId, $fId, $language)
    {
      $this->cId = $cId;
      $this->fId = $fId;
      $this->language = $language;
    }

}
