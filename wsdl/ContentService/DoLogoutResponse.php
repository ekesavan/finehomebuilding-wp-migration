<?php

class DoLogoutResponse
{

    /**
     * @var boolean $DoLogoutResult
     * @access public
     */
    public $DoLogoutResult = null;

    /**
     * @param boolean $DoLogoutResult
     * @access public
     */
    public function __construct($DoLogoutResult)
    {
      $this->DoLogoutResult = $DoLogoutResult;
    }

}
