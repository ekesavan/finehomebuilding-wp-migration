<?php

class RenameContent
{

    /**
     * @var int $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var string $Title
     * @access public
     */
    public $Title = null;

    /**
     * @param int $Id
     * @param string $Title
     * @access public
     */
    public function __construct($Id, $Title)
    {
      $this->Id = $Id;
      $this->Title = $Title;
    }

}
