<?php

class ReplyItem
{

    /**
     * @var string $id
     * @access public
     */
    public $id = null;

    /**
     * @var string $title
     * @access public
     */
    public $title = null;

    /**
     * @var string $state
     * @access public
     */
    public $state = null;

    /**
     * @var string $description
     * @access public
     */
    public $description = null;

    /**
     * @var string $dateCreated
     * @access public
     */
    public $dateCreated = null;

    /**
     * @var string $createdByUser
     * @access public
     */
    public $createdByUser = null;

    /**
     * @var string $quickLink
     * @access public
     */
    public $quickLink = null;

    /**
     * @var string $parentId
     * @access public
     */
    public $parentId = null;

    /**
     * @param string $id
     * @param string $title
     * @param string $state
     * @param string $description
     * @param string $dateCreated
     * @param string $createdByUser
     * @param string $quickLink
     * @param string $parentId
     * @access public
     */
    public function __construct($id, $title, $state, $description, $dateCreated, $createdByUser, $quickLink, $parentId)
    {
      $this->id = $id;
      $this->title = $title;
      $this->state = $state;
      $this->description = $description;
      $this->dateCreated = $dateCreated;
      $this->createdByUser = $createdByUser;
      $this->quickLink = $quickLink;
      $this->parentId = $parentId;
    }

}
