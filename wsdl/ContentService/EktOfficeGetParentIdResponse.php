<?php

class EktOfficeGetParentIdResponse
{

    /**
     * @var string $EktOfficeGetParentIdResult
     * @access public
     */
    public $EktOfficeGetParentIdResult = null;

    /**
     * @param string $EktOfficeGetParentIdResult
     * @access public
     */
    public function __construct($EktOfficeGetParentIdResult)
    {
      $this->EktOfficeGetParentIdResult = $EktOfficeGetParentIdResult;
    }

}
