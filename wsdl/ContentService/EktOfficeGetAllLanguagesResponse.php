<?php

class EktOfficeGetAllLanguagesResponse
{

    /**
     * @var LD[] $EktOfficeGetAllLanguagesResult
     * @access public
     */
    public $EktOfficeGetAllLanguagesResult = null;

    /**
     * @param LD[] $EktOfficeGetAllLanguagesResult
     * @access public
     */
    public function __construct($EktOfficeGetAllLanguagesResult)
    {
      $this->EktOfficeGetAllLanguagesResult = $EktOfficeGetAllLanguagesResult;
    }

}
