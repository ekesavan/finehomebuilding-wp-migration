<?php

class ContentMetadataType
{
    const __default = 'HtmlTag';
    const HtmlTag = 'HtmlTag';
    const MetaTag = 'MetaTag';
    const CollectionSelector = 'CollectionSelector';
    const ListSummarySelector = 'ListSummarySelector';
    const ContentSelector = 'ContentSelector';
    const ImageSelector = 'ImageSelector';
    const LinkSelector = 'LinkSelector';
    const FileSelector = 'FileSelector';
    const MenuSelector = 'MenuSelector';
    const UserSelector = 'UserSelector';
    const SearchableProperty = 'SearchableProperty';


}
