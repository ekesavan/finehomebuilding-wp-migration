<?php

class LoginUpload
{

    /**
     * @var int $status
     * @access public
     */
    public $status = null;

    /**
     * @var string $fileName
     * @access public
     */
    public $fileName = null;

    /**
     * @var string $assetId
     * @access public
     */
    public $assetId = null;

    /**
     * @var string $contentId
     * @access public
     */
    public $contentId = null;

    /**
     * @var string $contentStatus
     * @access public
     */
    public $contentStatus = null;

    /**
     * @param int $status
     * @param string $fileName
     * @param string $assetId
     * @param string $contentId
     * @param string $contentStatus
     * @access public
     */
    public function __construct($status, $fileName, $assetId, $contentId, $contentStatus)
    {
      $this->status = $status;
      $this->fileName = $fileName;
      $this->assetId = $assetId;
      $this->contentId = $contentId;
      $this->contentStatus = $contentStatus;
    }

}
