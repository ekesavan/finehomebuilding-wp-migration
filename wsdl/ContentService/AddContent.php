<?php

class AddContent
{

    /**
     * @var string $title
     * @access public
     */
    public $title = null;

    /**
     * @var string $comment
     * @access public
     */
    public $comment = null;

    /**
     * @var string $html
     * @access public
     */
    public $html = null;

    /**
     * @var string $summary
     * @access public
     */
    public $summary = null;

    /**
     * @var int $FolderId
     * @access public
     */
    public $FolderId = null;

    /**
     * @var int $ContentID
     * @access public
     */
    public $ContentID = null;

    /**
     * @param string $title
     * @param string $comment
     * @param string $html
     * @param string $summary
     * @param int $FolderId
     * @param int $ContentID
     * @access public
     */
    public function __construct($title, $comment, $html, $summary, $FolderId, $ContentID)
    {
      $this->title = $title;
      $this->comment = $comment;
      $this->html = $html;
      $this->summary = $summary;
      $this->FolderId = $FolderId;
      $this->ContentID = $ContentID;
    }

}
