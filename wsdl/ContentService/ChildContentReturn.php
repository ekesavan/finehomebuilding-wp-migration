<?php

class ChildContentReturn
{

    /**
     * @var ContentData $ContentDataItem
     * @access public
     */
    public $ContentDataItem = null;

    /**
     * @var string $DomainProduction
     * @access public
     */
    public $DomainProduction = null;

    /**
     * @var MetaDataPairs[] $MetaDataPairsArray
     * @access public
     */
    public $MetaDataPairsArray = null;

    /**
     * @var string $FileName
     * @access public
     */
    public $FileName = null;

    /**
     * @var string $Quicklink
     * @access public
     */
    public $Quicklink = null;

    /**
     * @var string $CanApprove
     * @access public
     */
    public $CanApprove = null;

    /**
     * @var int $TotalPages
     * @access public
     */
    public $TotalPages = null;

    /**
     * @var int $CurrentPage
     * @access public
     */
    public $CurrentPage = null;

    /**
     * @param ContentData $ContentDataItem
     * @param string $DomainProduction
     * @param MetaDataPairs[] $MetaDataPairsArray
     * @param string $FileName
     * @param string $Quicklink
     * @param string $CanApprove
     * @param int $TotalPages
     * @param int $CurrentPage
     * @access public
     */
    public function __construct($ContentDataItem, $DomainProduction, $MetaDataPairsArray, $FileName, $Quicklink, $CanApprove, $TotalPages, $CurrentPage)
    {
      $this->ContentDataItem = $ContentDataItem;
      $this->DomainProduction = $DomainProduction;
      $this->MetaDataPairsArray = $MetaDataPairsArray;
      $this->FileName = $FileName;
      $this->Quicklink = $Quicklink;
      $this->CanApprove = $CanApprove;
      $this->TotalPages = $TotalPages;
      $this->CurrentPage = $CurrentPage;
    }

}
