<?php

class GetFavoritesContent
{

    /**
     * @var string $contentList
     * @access public
     */
    public $contentList = null;

    /**
     * @var int $language
     * @access public
     */
    public $language = null;

    /**
     * @var boolean $summary
     * @access public
     */
    public $summary = null;

    /**
     * @param string $contentList
     * @param int $language
     * @param boolean $summary
     * @access public
     */
    public function __construct($contentList, $language, $summary)
    {
      $this->contentList = $contentList;
      $this->language = $language;
      $this->summary = $summary;
    }

}
