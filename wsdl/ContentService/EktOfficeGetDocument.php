<?php

class EktOfficeGetDocument
{

    /**
     * @var string $folderId
     * @access public
     */
    public $folderId = null;

    /**
     * @var string $filename
     * @access public
     */
    public $filename = null;

    /**
     * @var int $language
     * @access public
     */
    public $language = null;

    /**
     * @param string $folderId
     * @param string $filename
     * @param int $language
     * @access public
     */
    public function __construct($folderId, $filename, $language)
    {
      $this->folderId = $folderId;
      $this->filename = $filename;
      $this->language = $language;
    }

}
