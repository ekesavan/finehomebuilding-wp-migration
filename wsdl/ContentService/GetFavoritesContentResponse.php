<?php

class GetFavoritesContentResponse
{

    /**
     * @var ChildContentReturn[] $GetFavoritesContentResult
     * @access public
     */
    public $GetFavoritesContentResult = null;

    /**
     * @param ChildContentReturn[] $GetFavoritesContentResult
     * @access public
     */
    public function __construct($GetFavoritesContentResult)
    {
      $this->GetFavoritesContentResult = $GetFavoritesContentResult;
    }

}
