<?php

class UndoCheckoutResponse
{

    /**
     * @var boolean $UndoCheckoutResult
     * @access public
     */
    public $UndoCheckoutResult = null;

    /**
     * @param boolean $UndoCheckoutResult
     * @access public
     */
    public function __construct($UndoCheckoutResult)
    {
      $this->UndoCheckoutResult = $UndoCheckoutResult;
    }

}
