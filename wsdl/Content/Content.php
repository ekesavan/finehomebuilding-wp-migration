<?php

include_once('CheckOutContent.php');
include_once('CheckOutContentResponse.php');
//include_once('AuthenticationHeader.php');
//include_once('RequestInfoParameters.php');
include_once('SaveContent.php');
include_once('ContentEditData.php');
include_once('ContentData.php');
include_once('ContentBaseData.php');
include_once('CmsLocalizedDataOfContentBaseData.php');
include_once('CmsDataOfContentBaseData.php');
include_once('BaseDataOfContentBaseData.php');
include_once('CMSContentSubtype.php');
include_once('AssetData.php');
//include_once('ContentMetaData.php');
//include_once('CmsDataOfContentMetaData.php');
//include_once('BaseDataOfContentMetaData.php');
//include_once('ContentMetadataDataType.php');
//include_once('ContentMetadataType.php');
include_once('CMSEndDateAction.php');
//include_once('XmlConfigData.php');
//include_once('TemplateData.php');
//include_once('CmsDataOfTemplateData.php');
//include_once('BaseDataOfTemplateData.php');
//include_once('TemplateType.php');
//include_once('TemplateSubType.php');
include_once('SaveContentResponse.php');
include_once('CopyContentToFolder.php');
include_once('CopyContentToFolderResponse.php');
include_once('AddContent.php');
include_once('AddContentResponse.php');
include_once('AddContentWithUser.php');
include_once('AddContentWithUserResponse.php');
include_once('AddContent_x0020_With_x0020_overload_x0020_Parameter_x0020_CleanHtml.php');
include_once('AddContent_x0020_With_x0020_overload_x0020_Parameter_x0020_CleanHtmlResponse.php');
include_once('AddContent_x0020_With_x0020_ContentID.php');
include_once('AddContent_x0020_With_x0020_ContentIDResponse.php');
include_once('AddContent_x0020_with_x0020_XmlID_x0020_and_x0020_TemplateID.php');
include_once('AddContent_x0020_with_x0020_XmlID_x0020_and_x0020_TemplateIDResponse.php');
include_once('GetContentForEditing.php');
include_once('GetContentForEditingResponse.php');
include_once('DeleteContentItem.php');
include_once('DeleteContentItemResponse.php');
include_once('GetAddViewLanguage.php');
include_once('LanguageData.php');
include_once('GetAddViewLanguageResponse.php');
include_once('GetAllComments.php');
include_once('CommentData.php');
include_once('GetAllCommentsResponse.php');
include_once('GetAllTemplates.php');
include_once('GetAllTemplatesResponse.php');
include_once('UpdateContentMetaData.php');
include_once('UpdateContentMetaDataResponse.php');
include_once('GetAllUnassignedItemApprovals.php');
include_once('ApprovalData.php');
include_once('GetAllUnassignedItemApprovalsResponse.php');
include_once('GetChildContent.php');
include_once('GetChildContentResponse.php');
include_once('GetChildContentWithPaging.php');
include_once('PagingInfo.php');
include_once('GetChildContentWithPagingResponse.php');
include_once('GetContent.php');
include_once('ContentResultType.php');
include_once('GetContentResponse.php');
include_once('GetContentByHistory.php');
include_once('GetContentByHistoryResponse.php');
include_once('GetContentState.php');
include_once('GetContentStateResponse.php');
include_once('ContentStateData.php');
include_once('GetContentStatus.php');
include_once('GetContentStatusResponse.php');
include_once('GetDomain.php');
include_once('GetDomainResponse.php');
include_once('MoveContentToFolder.php');
include_once('MoveContentToFolderResponse.php');
include_once('PublishContent.php');
include_once('PublishContentResponse.php');
include_once('RenameContent.php');
include_once('RenameContentResponse.php');
include_once('SubmitForDelete.php');
include_once('SubmitForDeleteResponse.php');
include_once('UndoCheckout.php');
include_once('UndoCheckoutResponse.php');
include_once('NotifySubscriptionForSynchronizedContent.php');
include_once('NotifySubscriptionForSynchronizedContentResponse.php');
include_once('NotifySubscriptionForSynchronizedContentByType.php');
include_once('NotifySubscriptionForSynchronizedContentByTypeResponse.php');

class Content extends \EktronSoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'CheckOutContent' => '\CheckOutContent',
      'CheckOutContentResponse' => '\CheckOutContentResponse',
      'AuthenticationHeader' => '\AuthenticationHeader',
      'RequestInfoParameters' => '\RequestInfoParameters',
      'SaveContent' => '\SaveContent',
      'ContentEditData' => '\ContentEditData',
      'ContentData' => '\ContentData',
      'ContentBaseData' => '\ContentBaseData',
      'CmsLocalizedDataOfContentBaseData' => '\CmsLocalizedDataOfContentBaseData',
      'CmsDataOfContentBaseData' => '\CmsDataOfContentBaseData',
      'BaseDataOfContentBaseData' => '\BaseDataOfContentBaseData',
      'AssetData' => '\AssetData',
      'ContentMetaData' => '\ContentMetaData',
      'CmsDataOfContentMetaData' => '\CmsDataOfContentMetaData',
      'BaseDataOfContentMetaData' => '\BaseDataOfContentMetaData',
      'XmlConfigData' => '\XmlConfigData',
      'TemplateData' => '\TemplateData',
      'CmsDataOfTemplateData' => '\CmsDataOfTemplateData',
      'BaseDataOfTemplateData' => '\BaseDataOfTemplateData',
      'SaveContentResponse' => '\SaveContentResponse',
      'CopyContentToFolder' => '\CopyContentToFolder',
      'CopyContentToFolderResponse' => '\CopyContentToFolderResponse',
      'AddContent' => '\AddContent',
      'AddContentResponse' => '\AddContentResponse',
      'AddContentWithUser' => '\AddContentWithUser',
      'AddContentWithUserResponse' => '\AddContentWithUserResponse',
      'AddContent_x0020_With_x0020_overload_x0020_Parameter_x0020_CleanHtml' => '\AddContent_x0020_With_x0020_overload_x0020_Parameter_x0020_CleanHtml',
      'AddContent_x0020_With_x0020_overload_x0020_Parameter_x0020_CleanHtmlResponse' => '\AddContent_x0020_With_x0020_overload_x0020_Parameter_x0020_CleanHtmlResponse',
      'AddContent_x0020_With_x0020_ContentID' => '\AddContent_x0020_With_x0020_ContentID',
      'AddContent_x0020_With_x0020_ContentIDResponse' => '\AddContent_x0020_With_x0020_ContentIDResponse',
      'AddContent_x0020_with_x0020_XmlID_x0020_and_x0020_TemplateID' => '\AddContent_x0020_with_x0020_XmlID_x0020_and_x0020_TemplateID',
      'AddContent_x0020_with_x0020_XmlID_x0020_and_x0020_TemplateIDResponse' => '\AddContent_x0020_with_x0020_XmlID_x0020_and_x0020_TemplateIDResponse',
      'GetContentForEditing' => '\GetContentForEditing',
      'GetContentForEditingResponse' => '\GetContentForEditingResponse',
      'DeleteContentItem' => '\DeleteContentItem',
      'DeleteContentItemResponse' => '\DeleteContentItemResponse',
      'GetAddViewLanguage' => '\GetAddViewLanguage',
      'LanguageData' => '\LanguageData',
      'GetAddViewLanguageResponse' => '\GetAddViewLanguageResponse',
      'GetAllComments' => '\GetAllComments',
      'CommentData' => '\CommentData',
      'GetAllCommentsResponse' => '\GetAllCommentsResponse',
      'GetAllTemplates' => '\GetAllTemplates',
      'GetAllTemplatesResponse' => '\GetAllTemplatesResponse',
      'UpdateContentMetaData' => '\UpdateContentMetaData',
      'UpdateContentMetaDataResponse' => '\UpdateContentMetaDataResponse',
      'GetAllUnassignedItemApprovals' => '\GetAllUnassignedItemApprovals',
      'ApprovalData' => '\ApprovalData',
      'GetAllUnassignedItemApprovalsResponse' => '\GetAllUnassignedItemApprovalsResponse',
      'GetChildContent' => '\GetChildContent',
      'GetChildContentResponse' => '\GetChildContentResponse',
      'GetChildContentWithPaging' => '\GetChildContentWithPaging',
      'PagingInfo' => '\PagingInfo',
      'GetChildContentWithPagingResponse' => '\GetChildContentWithPagingResponse',
      'GetContent' => '\GetContent',
      'GetContentResponse' => '\GetContentResponse',
      'GetContentByHistory' => '\GetContentByHistory',
      'GetContentByHistoryResponse' => '\GetContentByHistoryResponse',
      'GetContentState' => '\GetContentState',
      'GetContentStateResponse' => '\GetContentStateResponse',
      'ContentStateData' => '\ContentStateData',
      'GetContentStatus' => '\GetContentStatus',
      'GetContentStatusResponse' => '\GetContentStatusResponse',
      'GetDomain' => '\GetDomain',
      'GetDomainResponse' => '\GetDomainResponse',
      'MoveContentToFolder' => '\MoveContentToFolder',
      'MoveContentToFolderResponse' => '\MoveContentToFolderResponse',
      'PublishContent' => '\PublishContent',
      'PublishContentResponse' => '\PublishContentResponse',
      'RenameContent' => '\RenameContent',
      'RenameContentResponse' => '\RenameContentResponse',
      'SubmitForDelete' => '\SubmitForDelete',
      'SubmitForDeleteResponse' => '\SubmitForDeleteResponse',
      'UndoCheckout' => '\UndoCheckout',
      'UndoCheckoutResponse' => '\UndoCheckoutResponse',
      'NotifySubscriptionForSynchronizedContent' => '\NotifySubscriptionForSynchronizedContent',
      'NotifySubscriptionForSynchronizedContentResponse' => '\NotifySubscriptionForSynchronizedContentResponse',
      'NotifySubscriptionForSynchronizedContentByType' => '\NotifySubscriptionForSynchronizedContentByType',
      'NotifySubscriptionForSynchronizedContentByTypeResponse' => '\NotifySubscriptionForSynchronizedContentByTypeResponse');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = 'wsdl/Content.wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      
      parent::__construct($wsdl, $options);
    }

    /**
     * Checks the content out to the current user
     *
     * @param CheckOutContent $parameters
     * @access public
     * @return CheckOutContentResponse
     */
    public function CheckOutContent(CheckOutContent $parameters)
    {
      return $this->__soapCall('CheckOutContent', array($parameters));
    }

    /**
     * Updates the checked out content block.
     *
     * @param SaveContent $parameters
     * @access public
     * @return SaveContentResponse
     */
    public function SaveContent(SaveContent $parameters)
    {
      return $this->__soapCall('SaveContent', array($parameters));
    }

    /**
     * Copy the content into required folder.
     *
     * @param CopyContentToFolder $parameters
     * @access public
     * @return CopyContentToFolderResponse
     */
    public function CopyContentToFolder(CopyContentToFolder $parameters)
    {
      return $this->__soapCall('CopyContentToFolder', array($parameters));
    }

    /**
     * Adds a piece of content.
     *
     * @param AddContent $parameters
     * @access public
     * @return AddContentResponse
     */
    public function AddContent(AddContent $parameters)
    {
      return $this->__soapCall('AddContent', array($parameters));
    }

    /**
     * Loads the content details with checkout mode
     *
     * @param GetContentForEditing $parameters
     * @access public
     * @return GetContentForEditingResponse
     */
    public function GetContentForEditing(GetContentForEditing $parameters)
    {
      return $this->__soapCall('GetContentForEditing', array($parameters));
    }

    /**
     * Delete the content by the content's ID
     *
     * @param DeleteContentItem $parameters
     * @access public
     * @return DeleteContentItemResponse
     */
    public function DeleteContentItem(DeleteContentItem $parameters)
    {
      return $this->__soapCall('DeleteContentItem', array($parameters));
    }

    /**
     * Gets all the languages from the system for a given content ID
     *
     * @param GetAddViewLanguage $parameters
     * @access public
     * @return GetAddViewLanguageResponse
     */
    public function GetAddViewLanguage(GetAddViewLanguage $parameters)
    {
      return $this->__soapCall('GetAddViewLanguage', array($parameters));
    }

    /**
     * Gets all comments
     *
     * @param GetAllComments $parameters
     * @access public
     * @return GetAllCommentsResponse
     */
    public function GetAllComments(GetAllComments $parameters)
    {
      return $this->__soapCall('GetAllComments', array($parameters));
    }

    /**
     * Gets all templates
     *
     * @param GetAllTemplates $parameters
     * @access public
     * @return GetAllTemplatesResponse
     */
    public function GetAllTemplates(GetAllTemplates $parameters)
    {
      return $this->__soapCall('GetAllTemplates', array($parameters));
    }

    /**
     * Updates Content Metadata
     *
     * @param UpdateContentMetaData $parameters
     * @access public
     * @return UpdateContentMetaDataResponse
     */
    public function UpdateContentMetaData(UpdateContentMetaData $parameters)
    {
      return $this->__soapCall('UpdateContentMetaData', array($parameters));
    }

    /**
     * Returns all of the unassigned user/groups for a given content ID
     *
     * @param GetAllUnassignedItemApprovals $parameters
     * @access public
     * @return GetAllUnassignedItemApprovalsResponse
     */
    public function GetAllUnassignedItemApprovals(GetAllUnassignedItemApprovals $parameters)
    {
      return $this->__soapCall('GetAllUnassignedItemApprovals', array($parameters));
    }

    /**
     * Loads all of the contents for the given folder
     *
     * @param GetChildContent $parameters
     * @access public
     * @return GetChildContentResponse
     */
    public function GetChildContent(GetChildContent $parameters)
    {
      return $this->__soapCall('GetChildContent', array($parameters));
    }

    /**
     * Gets the content details by content id
     *
     * @param GetContent $parameters
     * @access public
     * @return GetContentResponse
     */
    public function GetContent(GetContent $parameters)
    {
      return $this->__soapCall('GetContent', array($parameters));
    }

    /**
     * Retrieves the corresponding content by the history ID
     *
     * @param GetContentByHistory $parameters
     * @access public
     * @return GetContentByHistoryResponse
     */
    public function GetContentByHistory(GetContentByHistory $parameters)
    {
      return $this->__soapCall('GetContentByHistory', array($parameters));
    }

    /**
     * Gets the content's details
     *
     * @param GetContentState $parameters
     * @access public
     * @return GetContentStateResponse
     */
    public function GetContentState(GetContentState $parameters)
    {
      return $this->__soapCall('GetContentState', array($parameters));
    }

    /**
     * Gets single letter string which is the status of the piece of content
     *
     * @param GetContentStatus $parameters
     * @access public
     * @return GetContentStatusResponse
     */
    public function GetContentStatus(GetContentStatus $parameters)
    {
      return $this->__soapCall('GetContentStatus', array($parameters));
    }

    /**
     * Gets web domain for a piece of content
     *
     * @param GetDomain $parameters
     * @access public
     * @return GetDomainResponse
     */
    public function GetDomain(GetDomain $parameters)
    {
      return $this->__soapCall('GetDomain', array($parameters));
    }

    /**
     * Moves content to a folder
     *
     * @param MoveContentToFolder $parameters
     * @access public
     * @return MoveContentToFolderResponse
     */
    public function MoveContentToFolder(MoveContentToFolder $parameters)
    {
      return $this->__soapCall('MoveContentToFolder', array($parameters));
    }

    /**
     * Function takes in information about the content to be checked in and published. This goes through the normal approval chain. The content must be in a checked out state to the current logged in user for this to succeed
     *
     * @param PublishContent $parameters
     * @access public
     * @return PublishContentResponse
     */
    public function PublishContent(PublishContent $parameters)
    {
      return $this->__soapCall('PublishContent', array($parameters));
    }

    /**
     * Renames the content by ID
     *
     * @param RenameContent $parameters
     * @access public
     * @return RenameContentResponse
     */
    public function RenameContent(RenameContent $parameters)
    {
      return $this->__soapCall('RenameContent', array($parameters));
    }

    /**
     * Deletes the content by the content's Id and folder Id
     *
     * @param SubmitForDelete $parameters
     * @access public
     * @return SubmitForDeleteResponse
     */
    public function SubmitForDelete(SubmitForDelete $parameters)
    {
      return $this->__soapCall('SubmitForDelete', array($parameters));
    }

    /**
     * Reverts the checkout for a piece of content which is checked out to the current user
     *
     * @param UndoCheckout $parameters
     * @access public
     * @return UndoCheckoutResponse
     */
    public function UndoCheckout(UndoCheckout $parameters)
    {
      return $this->__soapCall('UndoCheckout', array($parameters));
    }

    /**
     * Notify subscription for synchronized content.
     *
     * @param NotifySubscriptionForSynchronizedContent $parameters
     * @access public
     * @return NotifySubscriptionForSynchronizedContentResponse
     */
    public function NotifySubscriptionForSynchronizedContent(NotifySubscriptionForSynchronizedContent $parameters)
    {
      return $this->__soapCall('NotifySubscriptionForSynchronizedContent', array($parameters));
    }

    /**
     * Notify subscription for synchronized content.
     *
     * @param NotifySubscriptionForSynchronizedContentByType $parameters
     * @access public
     * @return NotifySubscriptionForSynchronizedContentByTypeResponse
     */
    public function NotifySubscriptionForSynchronizedContentByType(NotifySubscriptionForSynchronizedContentByType $parameters)
    {
      return $this->__soapCall('NotifySubscriptionForSynchronizedContentByType', array($parameters));
    }

    
    //=======================Custom==================================================
    public function GetChildContentWithPaging( $parameters)
    {
    	return $this->GetChildContentWithPaging( $parameters );
    }
    
}
