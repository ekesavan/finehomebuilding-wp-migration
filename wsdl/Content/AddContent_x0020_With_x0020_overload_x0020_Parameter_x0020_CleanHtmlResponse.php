<?php

class AddContent_x0020_With_x0020_overload_x0020_Parameter_x0020_CleanHtmlResponse
{

    /**
     * @var int $AddContent_x0020_With_x0020_overload_x0020_Parameter_x0020_CleanHtmlResult
     * @access public
     */
    public $AddContent_x0020_With_x0020_overload_x0020_Parameter_x0020_CleanHtmlResult = null;

    /**
     * @param int $AddContent_x0020_With_x0020_overload_x0020_Parameter_x0020_CleanHtmlResult
     * @access public
     */
    public function __construct($AddContent_x0020_With_x0020_overload_x0020_Parameter_x0020_CleanHtmlResult)
    {
      $this->AddContent_x0020_With_x0020_overload_x0020_Parameter_x0020_CleanHtmlResult = $AddContent_x0020_With_x0020_overload_x0020_Parameter_x0020_CleanHtmlResult;
    }

}
