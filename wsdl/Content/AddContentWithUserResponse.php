<?php

class AddContentWithUserResponse
{

    /**
     * @var int $AddContentWithUserResult
     * @access public
     */
    public $AddContentWithUserResult = null;

    /**
     * @param int $AddContentWithUserResult
     * @access public
     */
    public function __construct($AddContentWithUserResult)
    {
      $this->AddContentWithUserResult = $AddContentWithUserResult;
    }

}
