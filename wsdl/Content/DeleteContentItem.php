<?php

class DeleteContentItem
{

    /**
     * @var int $ContentID
     * @access public
     */
    public $ContentID = null;

    /**
     * @param int $ContentID
     * @access public
     */
    public function __construct($ContentID)
    {
      $this->ContentID = $ContentID;
    }

}
