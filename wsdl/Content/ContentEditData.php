<?php

include_once('ContentData.php');

class ContentEditData extends ContentData
{

    /**
     * @var int $NoOfSaves
     * @access public
     */
    public $NoOfSaves = null;

    /**
     * @var int $MaxContentSize
     * @access public
     */
    public $MaxContentSize = null;

    /**
     * @var int $MaxSummarySize
     * @access public
     */
    public $MaxSummarySize = null;

    /**
     * @var string $CurrentStatus
     * @access public
     */
    public $CurrentStatus = null;

    /**
     * @var boolean $LockedContentLink
     * @access public
     */
    public $LockedContentLink = null;

    /**
     * @var boolean $FileChanged
     * @access public
     */
    public $FileChanged = null;

    /**
     * @param int $Id
     * @param int $LanguageId
     * @param string $Title
     * @param string $Teaser
     * @param string $Html
     * @param string $Quicklink
     * @param string $Image
     * @param boolean $IsPrivate
     * @param int $Type
     * @param CMSContentSubtype $SubType
     * @param int $ExternalTypeId
     * @param AssetData $AssetData
     * @param string $EditorFirstName
     * @param string $EditorLastName
     * @param string $Comment
     * @param dateTime $DateModified
     * @param string $DisplayLastEditDate
     * @param dateTime $DateCreated
     * @param string $DisplayDateCreated
     * @param int $UserId
     * @param int $FolderId
     * @param boolean $IsPermissionsInherited
     * @param int $PermissionInheritedFrom
     * @param string $Status
     * @param string $DisplayGoLive
     * @param boolean $IsPublished
     * @param boolean $IsSearchable
     * @param int $XmlInheritedFrom
     * @param ContentMetaData[] $MetaData
     * @param string $DisplayEndDate
     * @param dateTime $ExpireDate
     * @param CMSEndDateAction $EndDateActionType
     * @param XmlConfigData $XmlConfiguration
     * @param string $StyleSheet
     * @param string $LanguageDescription
     * @param string $Text
     * @param string $Path
     * @param string $ContentPath
     * @param int $ContType
     * @param int $Updates
     * @param string $FolderName
     * @param string $MediaText
     * @param int $HistoryId
     * @param string $HyperLink
     * @param TemplateData $TemplateConfiguration
     * @param int $FlagDefId
     * @param int $NoOfSaves
     * @param int $MaxContentSize
     * @param int $MaxSummarySize
     * @param string $CurrentStatus
     * @param boolean $LockedContentLink
     * @param boolean $FileChanged
     * @access public
     */
    public function __construct($Id, $LanguageId, $Title, $Teaser, $Html, $Quicklink, $Image, $IsPrivate, $Type, $SubType, $ExternalTypeId, $AssetData, $EditorFirstName, $EditorLastName, $Comment, $DateModified, $DisplayLastEditDate, $DateCreated, $DisplayDateCreated, $UserId, $FolderId, $IsPermissionsInherited, $PermissionInheritedFrom, $Status, $DisplayGoLive, $IsPublished, $IsSearchable, $XmlInheritedFrom, $MetaData, $DisplayEndDate, $ExpireDate, $EndDateActionType, $XmlConfiguration, $StyleSheet, $LanguageDescription, $Text, $Path, $ContentPath, $ContType, $Updates, $FolderName, $MediaText, $HistoryId, $HyperLink, $TemplateConfiguration, $FlagDefId, $NoOfSaves, $MaxContentSize, $MaxSummarySize, $CurrentStatus, $LockedContentLink, $FileChanged)
    {
      parent::__construct($Id, $LanguageId, $Title, $Teaser, $Html, $Quicklink, $Image, $IsPrivate, $Type, $SubType, $ExternalTypeId, $AssetData, $EditorFirstName, $EditorLastName, $Comment, $DateModified, $DisplayLastEditDate, $DateCreated, $DisplayDateCreated, $UserId, $FolderId, $IsPermissionsInherited, $PermissionInheritedFrom, $Status, $DisplayGoLive, $IsPublished, $IsSearchable, $XmlInheritedFrom, $MetaData, $DisplayEndDate, $ExpireDate, $EndDateActionType, $XmlConfiguration, $StyleSheet, $LanguageDescription, $Text, $Path, $ContentPath, $ContType, $Updates, $FolderName, $MediaText, $HistoryId, $HyperLink, $TemplateConfiguration, $FlagDefId);
      $this->NoOfSaves = $NoOfSaves;
      $this->MaxContentSize = $MaxContentSize;
      $this->MaxSummarySize = $MaxSummarySize;
      $this->CurrentStatus = $CurrentStatus;
      $this->LockedContentLink = $LockedContentLink;
      $this->FileChanged = $FileChanged;
    }

}
