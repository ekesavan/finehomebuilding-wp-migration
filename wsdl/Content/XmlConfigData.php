<?php

class XmlConfigData
{

    /**
     * @var int $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var string $Title
     * @access public
     */
    public $Title = null;

    /**
     * @var string $Description
     * @access public
     */
    public $Description = null;

    /**
     * @var string $EditXslt
     * @access public
     */
    public $EditXslt = null;

    /**
     * @var string $SaveXslt
     * @access public
     */
    public $SaveXslt = null;

    /**
     * @var string $Xslt1
     * @access public
     */
    public $Xslt1 = null;

    /**
     * @var string $Xslt2
     * @access public
     */
    public $Xslt2 = null;

    /**
     * @var string $Xslt3
     * @access public
     */
    public $Xslt3 = null;

    /**
     * @var string $Xslt4
     * @access public
     */
    public $Xslt4 = null;

    /**
     * @var string $Xslt5
     * @access public
     */
    public $Xslt5 = null;

    /**
     * @var string $XmlSchema
     * @access public
     */
    public $XmlSchema = null;

    /**
     * @var string $XmlNameSpace
     * @access public
     */
    public $XmlNameSpace = null;

    /**
     * @var string $XmlAdvConfig
     * @access public
     */
    public $XmlAdvConfig = null;

    /**
     * @var string $DateCreated
     * @access public
     */
    public $DateCreated = null;

    /**
     * @var string $DisplayDateCreated
     * @access public
     */
    public $DisplayDateCreated = null;

    /**
     * @var dateTime $LastEditDate
     * @access public
     */
    public $LastEditDate = null;

    /**
     * @var string $DisplayLastEditDate
     * @access public
     */
    public $DisplayLastEditDate = null;

    /**
     * @var int $UserId
     * @access public
     */
    public $UserId = null;

    /**
     * @var string $EditorFirstName
     * @access public
     */
    public $EditorFirstName = null;

    /**
     * @var string $EditorLastName
     * @access public
     */
    public $EditorLastName = null;

    /**
     * @var string $PhysicalPath
     * @access public
     */
    public $PhysicalPath = null;

    /**
     * @var string $LogicalPath
     * @access public
     */
    public $LogicalPath = null;

    /**
     * @var string $DefaultXslt
     * @access public
     */
    public $DefaultXslt = null;

    /**
     * @var string $PackageXslt
     * @access public
     */
    public $PackageXslt = null;

    /**
     * @var string $PackageDisplayXslt
     * @access public
     */
    public $PackageDisplayXslt = null;

    /**
     * @var string $DesignStyleSheet
     * @access public
     */
    public $DesignStyleSheet = null;

    /**
     * @var boolean $IsDefault
     * @access public
     */
    public $IsDefault = null;

    /**
     * @var string $FieldList
     * @access public
     */
    public $FieldList = null;

    /**
     * @param int $Id
     * @param string $Title
     * @param string $Description
     * @param string $EditXslt
     * @param string $SaveXslt
     * @param string $Xslt1
     * @param string $Xslt2
     * @param string $Xslt3
     * @param string $Xslt4
     * @param string $Xslt5
     * @param string $XmlSchema
     * @param string $XmlNameSpace
     * @param string $XmlAdvConfig
     * @param string $DateCreated
     * @param string $DisplayDateCreated
     * @param dateTime $LastEditDate
     * @param string $DisplayLastEditDate
     * @param int $UserId
     * @param string $EditorFirstName
     * @param string $EditorLastName
     * @param string $PhysicalPath
     * @param string $LogicalPath
     * @param string $DefaultXslt
     * @param string $PackageXslt
     * @param string $PackageDisplayXslt
     * @param string $DesignStyleSheet
     * @param boolean $IsDefault
     * @param string $FieldList
     * @access public
     */
    public function __construct($Id, $Title, $Description, $EditXslt, $SaveXslt, $Xslt1, $Xslt2, $Xslt3, $Xslt4, $Xslt5, $XmlSchema, $XmlNameSpace, $XmlAdvConfig, $DateCreated, $DisplayDateCreated, $LastEditDate, $DisplayLastEditDate, $UserId, $EditorFirstName, $EditorLastName, $PhysicalPath, $LogicalPath, $DefaultXslt, $PackageXslt, $PackageDisplayXslt, $DesignStyleSheet, $IsDefault, $FieldList)
    {
      $this->Id = $Id;
      $this->Title = $Title;
      $this->Description = $Description;
      $this->EditXslt = $EditXslt;
      $this->SaveXslt = $SaveXslt;
      $this->Xslt1 = $Xslt1;
      $this->Xslt2 = $Xslt2;
      $this->Xslt3 = $Xslt3;
      $this->Xslt4 = $Xslt4;
      $this->Xslt5 = $Xslt5;
      $this->XmlSchema = $XmlSchema;
      $this->XmlNameSpace = $XmlNameSpace;
      $this->XmlAdvConfig = $XmlAdvConfig;
      $this->DateCreated = $DateCreated;
      $this->DisplayDateCreated = $DisplayDateCreated;
      $this->LastEditDate = $LastEditDate;
      $this->DisplayLastEditDate = $DisplayLastEditDate;
      $this->UserId = $UserId;
      $this->EditorFirstName = $EditorFirstName;
      $this->EditorLastName = $EditorLastName;
      $this->PhysicalPath = $PhysicalPath;
      $this->LogicalPath = $LogicalPath;
      $this->DefaultXslt = $DefaultXslt;
      $this->PackageXslt = $PackageXslt;
      $this->PackageDisplayXslt = $PackageDisplayXslt;
      $this->DesignStyleSheet = $DesignStyleSheet;
      $this->IsDefault = $IsDefault;
      $this->FieldList = $FieldList;
    }

}
