<?php

class SaveContent
{

    /**
     * @var ContentEditData $contentEditData
     * @access public
     */
    public $contentEditData = null;

    /**
     * @param ContentEditData $contentEditData
     * @access public
     */
    public function __construct($contentEditData)
    {
      $this->contentEditData = $contentEditData;
    }

}
