<?php

class GetContentStateResponse
{

    /**
     * @var ContentStateData $GetContentStateResult
     * @access public
     */
    public $GetContentStateResult = null;

    /**
     * @param ContentStateData $GetContentStateResult
     * @access public
     */
    public function __construct($GetContentStateResult)
    {
      $this->GetContentStateResult = $GetContentStateResult;
    }

}
