<?php

class GetAllCommentsResponse
{

    /**
     * @var CommentData[] $GetAllCommentsResult
     * @access public
     */
    public $GetAllCommentsResult = null;

    /**
     * @param CommentData[] $GetAllCommentsResult
     * @access public
     */
    public function __construct($GetAllCommentsResult)
    {
      $this->GetAllCommentsResult = $GetAllCommentsResult;
    }

}
