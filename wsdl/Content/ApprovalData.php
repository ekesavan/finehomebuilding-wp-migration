<?php

class ApprovalData
{

    /**
     * @var int $UserId
     * @access public
     */
    public $UserId = null;

    /**
     * @var int $GroupId
     * @access public
     */
    public $GroupId = null;

    /**
     * @var string $UserName
     * @access public
     */
    public $UserName = null;

    /**
     * @var string $GroupName
     * @access public
     */
    public $GroupName = null;

    /**
     * @var string $DisplayUserName
     * @access public
     */
    public $DisplayUserName = null;

    /**
     * @var string $DisplayUserGroupName
     * @access public
     */
    public $DisplayUserGroupName = null;

    /**
     * @var string $UserDomain
     * @access public
     */
    public $UserDomain = null;

    /**
     * @var string $UserGroupDomain
     * @access public
     */
    public $UserGroupDomain = null;

    /**
     * @var string $Type
     * @access public
     */
    public $Type = null;

    /**
     * @var boolean $IsCurrentApprover
     * @access public
     */
    public $IsCurrentApprover = null;

    /**
     * @var anyType $LegacyData
     * @access public
     */
    public $LegacyData = null;

    /**
     * @param int $UserId
     * @param int $GroupId
     * @param string $UserName
     * @param string $GroupName
     * @param string $DisplayUserName
     * @param string $DisplayUserGroupName
     * @param string $UserDomain
     * @param string $UserGroupDomain
     * @param string $Type
     * @param boolean $IsCurrentApprover
     * @param anyType $LegacyData
     * @access public
     */
    public function __construct($UserId, $GroupId, $UserName, $GroupName, $DisplayUserName, $DisplayUserGroupName, $UserDomain, $UserGroupDomain, $Type, $IsCurrentApprover, $LegacyData)
    {
      $this->UserId = $UserId;
      $this->GroupId = $GroupId;
      $this->UserName = $UserName;
      $this->GroupName = $GroupName;
      $this->DisplayUserName = $DisplayUserName;
      $this->DisplayUserGroupName = $DisplayUserGroupName;
      $this->UserDomain = $UserDomain;
      $this->UserGroupDomain = $UserGroupDomain;
      $this->Type = $Type;
      $this->IsCurrentApprover = $IsCurrentApprover;
      $this->LegacyData = $LegacyData;
    }

}
