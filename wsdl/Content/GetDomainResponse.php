<?php

class GetDomainResponse
{

    /**
     * @var string $GetDomainResult
     * @access public
     */
    public $GetDomainResult = null;

    /**
     * @param string $GetDomainResult
     * @access public
     */
    public function __construct($GetDomainResult)
    {
      $this->GetDomainResult = $GetDomainResult;
    }

}
