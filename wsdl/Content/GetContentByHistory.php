<?php

class GetContentByHistory
{

    /**
     * @var int $HistoryID
     * @access public
     */
    public $HistoryID = null;

    /**
     * @param int $HistoryID
     * @access public
     */
    public function __construct($HistoryID)
    {
      $this->HistoryID = $HistoryID;
    }

}
