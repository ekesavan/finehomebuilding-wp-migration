<?php

class SaveContentResponse
{

    /**
     * @var boolean $SaveContentResult
     * @access public
     */
    public $SaveContentResult = null;

    /**
     * @param boolean $SaveContentResult
     * @access public
     */
    public function __construct($SaveContentResult)
    {
      $this->SaveContentResult = $SaveContentResult;
    }

}
