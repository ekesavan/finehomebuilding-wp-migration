<?php

class GetContentByHistoryResponse
{

    /**
     * @var ContentData $GetContentByHistoryResult
     * @access public
     */
    public $GetContentByHistoryResult = null;

    /**
     * @param ContentData $GetContentByHistoryResult
     * @access public
     */
    public function __construct($GetContentByHistoryResult)
    {
      $this->GetContentByHistoryResult = $GetContentByHistoryResult;
    }

}
