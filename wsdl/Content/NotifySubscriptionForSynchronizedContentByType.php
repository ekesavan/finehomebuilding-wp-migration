<?php

class NotifySubscriptionForSynchronizedContentByType
{

    /**
     * @var int $startVersion
     * @access public
     */
    public $startVersion = null;

    /**
     * @var int $syncType
     * @access public
     */
    public $syncType = null;

    /**
     * @param int $startVersion
     * @param int $syncType
     * @access public
     */
    public function __construct($startVersion, $syncType)
    {
      $this->startVersion = $startVersion;
      $this->syncType = $syncType;
    }

}
