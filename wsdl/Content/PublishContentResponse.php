<?php

class PublishContentResponse
{

    /**
     * @var boolean $PublishContentResult
     * @access public
     */
    public $PublishContentResult = null;

    /**
     * @param boolean $PublishContentResult
     * @access public
     */
    public function __construct($PublishContentResult)
    {
      $this->PublishContentResult = $PublishContentResult;
    }

}
