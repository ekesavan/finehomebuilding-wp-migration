<?php

class AddContentWithUser
{

    /**
     * @var string $Username
     * @access public
     */
    public $Username = null;

    /**
     * @var string $Password
     * @access public
     */
    public $Password = null;

    /**
     * @var string $Domain
     * @access public
     */
    public $Domain = null;

    /**
     * @var string $ContentTitle
     * @access public
     */
    public $ContentTitle = null;

    /**
     * @var string $ContentComment
     * @access public
     */
    public $ContentComment = null;

    /**
     * @var string $ContentHtml
     * @access public
     */
    public $ContentHtml = null;

    /**
     * @var string $SearchText
     * @access public
     */
    public $SearchText = null;

    /**
     * @var string $SummaryHtml
     * @access public
     */
    public $SummaryHtml = null;

    /**
     * @var string $ContentLanguage
     * @access public
     */
    public $ContentLanguage = null;

    /**
     * @var int $FolderId
     * @access public
     */
    public $FolderId = null;

    /**
     * @var anyType $GoLive
     * @access public
     */
    public $GoLive = null;

    /**
     * @var anyType $EndDate
     * @access public
     */
    public $EndDate = null;

    /**
     * @var string $MetaInfoXml
     * @access public
     */
    public $MetaInfoXml = null;

    /**
     * @param string $Username
     * @param string $Password
     * @param string $Domain
     * @param string $ContentTitle
     * @param string $ContentComment
     * @param string $ContentHtml
     * @param string $SearchText
     * @param string $SummaryHtml
     * @param string $ContentLanguage
     * @param int $FolderId
     * @param anyType $GoLive
     * @param anyType $EndDate
     * @param string $MetaInfoXml
     * @access public
     */
    public function __construct($Username, $Password, $Domain, $ContentTitle, $ContentComment, $ContentHtml, $SearchText, $SummaryHtml, $ContentLanguage, $FolderId, $GoLive, $EndDate, $MetaInfoXml)
    {
      $this->Username = $Username;
      $this->Password = $Password;
      $this->Domain = $Domain;
      $this->ContentTitle = $ContentTitle;
      $this->ContentComment = $ContentComment;
      $this->ContentHtml = $ContentHtml;
      $this->SearchText = $SearchText;
      $this->SummaryHtml = $SummaryHtml;
      $this->ContentLanguage = $ContentLanguage;
      $this->FolderId = $FolderId;
      $this->GoLive = $GoLive;
      $this->EndDate = $EndDate;
      $this->MetaInfoXml = $MetaInfoXml;
    }

}
