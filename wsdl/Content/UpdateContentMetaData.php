<?php

class UpdateContentMetaData
{

    /**
     * @var int $ContentID
     * @access public
     */
    public $ContentID = null;

    /**
     * @var int $MetaDataTypeID
     * @access public
     */
    public $MetaDataTypeID = null;

    /**
     * @var string $MetaDataText
     * @access public
     */
    public $MetaDataText = null;

    /**
     * @param int $ContentID
     * @param int $MetaDataTypeID
     * @param string $MetaDataText
     * @access public
     */
    public function __construct($ContentID, $MetaDataTypeID, $MetaDataText)
    {
      $this->ContentID = $ContentID;
      $this->MetaDataTypeID = $MetaDataTypeID;
      $this->MetaDataText = $MetaDataText;
    }

}
