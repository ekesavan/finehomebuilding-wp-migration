<?php

class MoveContentToFolder
{

    /**
     * @var string $ContentID
     * @access public
     */
    public $ContentID = null;

    /**
     * @var string $ContentLanguage
     * @access public
     */
    public $ContentLanguage = null;

    /**
     * @var int $FolderID
     * @access public
     */
    public $FolderID = null;

    /**
     * @param string $ContentID
     * @param string $ContentLanguage
     * @param int $FolderID
     * @access public
     */
    public function __construct($ContentID, $ContentLanguage, $FolderID)
    {
      $this->ContentID = $ContentID;
      $this->ContentLanguage = $ContentLanguage;
      $this->FolderID = $FolderID;
    }

}
