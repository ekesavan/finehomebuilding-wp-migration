<?php

include_once('CmsLocalizedDataOfContentBaseData.php');

class ContentBaseData extends CmsLocalizedDataOfContentBaseData
{

    /**
     * @var string $Title
     * @access public
     */
    public $Title = null;

    /**
     * @var string $Teaser
     * @access public
     */
    public $Teaser = null;

    /**
     * @var string $Html
     * @access public
     */
    public $Html = null;

    /**
     * @var string $Quicklink
     * @access public
     */
    public $Quicklink = null;

    /**
     * @var string $Image
     * @access public
     */
    public $Image = null;

    /**
     * @var boolean $IsPrivate
     * @access public
     */
    public $IsPrivate = null;

    /**
     * @var int $Type
     * @access public
     */
    public $Type = null;

    /**
     * @var CMSContentSubtype $SubType
     * @access public
     */
    public $SubType = null;

    /**
     * @var int $ExternalTypeId
     * @access public
     */
    public $ExternalTypeId = null;

    /**
     * @var AssetData $AssetData
     * @access public
     */
    public $AssetData = null;

    /**
     * @param int $Id
     * @param int $LanguageId
     * @param string $Title
     * @param string $Teaser
     * @param string $Html
     * @param string $Quicklink
     * @param string $Image
     * @param boolean $IsPrivate
     * @param int $Type
     * @param CMSContentSubtype $SubType
     * @param int $ExternalTypeId
     * @param AssetData $AssetData
     * @access public
     */
    public function __construct($Id, $LanguageId, $Title, $Teaser, $Html, $Quicklink, $Image, $IsPrivate, $Type, $SubType, $ExternalTypeId, $AssetData)
    {
      parent::__construct($Id, $LanguageId);
      $this->Title = $Title;
      $this->Teaser = $Teaser;
      $this->Html = $Html;
      $this->Quicklink = $Quicklink;
      $this->Image = $Image;
      $this->IsPrivate = $IsPrivate;
      $this->Type = $Type;
      $this->SubType = $SubType;
      $this->ExternalTypeId = $ExternalTypeId;
      $this->AssetData = $AssetData;
    }

}
