<?php

class UpdateContentMetaDataResponse
{

    /**
     * @var boolean $UpdateContentMetaDataResult
     * @access public
     */
    public $UpdateContentMetaDataResult = null;

    /**
     * @param boolean $UpdateContentMetaDataResult
     * @access public
     */
    public function __construct($UpdateContentMetaDataResult)
    {
      $this->UpdateContentMetaDataResult = $UpdateContentMetaDataResult;
    }

}
