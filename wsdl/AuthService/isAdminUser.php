<?php

class isAdminUser
{

    /**
     * @var string $username
     * @access public
     */
    public $username = null;

    /**
     * @var string $password
     * @access public
     */
    public $password = null;

    /**
     * @var string $hostname
     * @access public
     */
    public $hostname = null;

    /**
     * @var string $domain
     * @access public
     */
    public $domain = null;

    /**
     * @var string $protocol
     * @access public
     */
    public $protocol = null;

    /**
     * @param string $username
     * @param string $password
     * @param string $hostname
     * @param string $domain
     * @param string $protocol
     * @access public
     */
    public function __construct($username, $password, $hostname, $domain, $protocol)
    {
      $this->username = $username;
      $this->password = $password;
      $this->hostname = $hostname;
      $this->domain = $domain;
      $this->protocol = $protocol;
    }

}
