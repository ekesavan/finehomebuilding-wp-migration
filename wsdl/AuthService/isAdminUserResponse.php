<?php

class isAdminUserResponse
{

    /**
     * @var boolean $isAdminUserResult
     * @access public
     */
    public $isAdminUserResult = null;

    /**
     * @param boolean $isAdminUserResult
     * @access public
     */
    public function __construct($isAdminUserResult)
    {
      $this->isAdminUserResult = $isAdminUserResult;
    }

}
