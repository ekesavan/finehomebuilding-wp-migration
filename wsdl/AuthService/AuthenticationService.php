<?php

include_once('isValidUser.php');
include_once('isValidUserResponse.php');
include_once('isAdminUser.php');
include_once('isAdminUserResponse.php');

class AuthenticationService extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'isValidUser' => '\isValidUser',
      'isValidUserResponse' => '\isValidUserResponse',
      'isAdminUser' => '\isAdminUser',
      'isAdminUserResponse' => '\isAdminUserResponse');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = 'AuthService.wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      
      parent::__construct($wsdl, $options);
    }

    /**
     * @param isValidUser $parameters
     * @access public
     * @return isValidUserResponse
     */
    public function isValidUser(isValidUser $parameters)
    {
      return $this->__soapCall('isValidUser', array($parameters));
    }

    /**
     * @param isAdminUser $parameters
     * @access public
     * @return isAdminUserResponse
     */
    public function isAdminUser(isAdminUser $parameters)
    {
      return $this->__soapCall('isAdminUser', array($parameters));
    }

}
