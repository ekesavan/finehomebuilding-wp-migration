<?php

class isValidUserResponse
{

    /**
     * @var boolean $isValidUserResult
     * @access public
     */
    public $isValidUserResult = null;

    /**
     * @param boolean $isValidUserResult
     * @access public
     */
    public function __construct($isValidUserResult)
    {
      $this->isValidUserResult = $isValidUserResult;
    }

}
