<?php

class GetFolderIDByName
{

    /**
     * @var string $FolderName
     * @access public
     */
    public $FolderName = null;

    /**
     * @var int $ParentId
     * @access public
     */
    public $ParentId = null;

    /**
     * @param string $FolderName
     * @param int $ParentId
     * @access public
     */
    public function __construct($FolderName, $ParentId)
    {
      $this->FolderName = $FolderName;
      $this->ParentId = $ParentId;
    }

}
