<?php

class GetChildFolders
{

    /**
     * @var int $FolderID
     * @access public
     */
    public $FolderID = null;

    /**
     * @var boolean $Recursive
     * @access public
     */
    public $Recursive = null;

    /**
     * @var FolderOrderBy $OrderBy
     * @access public
     */
    public $OrderBy = null;

    /**
     * @param int $FolderID
     * @param boolean $Recursive
     * @param FolderOrderBy $OrderBy
     * @access public
     */
    public function __construct($FolderID, $Recursive, $OrderBy)
    {
      $this->FolderID = $FolderID;
      $this->Recursive = $Recursive;
      $this->OrderBy = $OrderBy;
    }

}
