<?php

class RequestInfoParameters
{

    /**
     * @var int $ContentLanguage
     * @access public
     */
    public $ContentLanguage = null;

    /**
     * @param int $ContentLanguage
     * @access public
     */
    public function __construct($ContentLanguage)
    {
      $this->ContentLanguage = $ContentLanguage;
    }

}
