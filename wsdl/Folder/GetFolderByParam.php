<?php

class GetFolderByParam
{

    /**
     * @var int $FolderID
     * @access public
     */
    public $FolderID = null;

    /**
     * @var boolean $getTaxonomy
     * @access public
     */
    public $getTaxonomy = null;

    /**
     * @var boolean $getFlags
     * @access public
     */
    public $getFlags = null;

    /**
     * @param int $FolderID
     * @param boolean $getTaxonomy
     * @param boolean $getFlags
     * @access public
     */
    public function __construct($FolderID, $getTaxonomy, $getFlags)
    {
      $this->FolderID = $FolderID;
      $this->getTaxonomy = $getTaxonomy;
      $this->getFlags = $getFlags;
    }

}
