<?php

class DeleteFolderResponse
{

    /**
     * @var boolean $DeleteFolderResult
     * @access public
     */
    public $DeleteFolderResult = null;

    /**
     * @param boolean $DeleteFolderResult
     * @access public
     */
    public function __construct($DeleteFolderResult)
    {
      $this->DeleteFolderResult = $DeleteFolderResult;
    }

}
