<?php

class UpdateFolder_x005B_FolderData_x005D_
{

    /**
     * @var FolderData $FolderReq
     * @access public
     */
    public $FolderReq = null;

    /**
     * @param FolderData $FolderReq
     * @access public
     */
    public function __construct($FolderReq)
    {
      $this->FolderReq = $FolderReq;
    }

}
