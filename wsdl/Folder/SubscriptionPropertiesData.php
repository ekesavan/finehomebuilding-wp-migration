<?php

class SubscriptionPropertiesData
{

    /**
     * @var boolean $BreakInheritance
     * @access public
     */
    public $BreakInheritance = null;

    /**
     * @var SubscriptionPropertyNotificationTypes $NotificationType
     * @access public
     */
    public $NotificationType = null;

    /**
     * @var boolean $SuspendNextNotification
     * @access public
     */
    public $SuspendNextNotification = null;

    /**
     * @var boolean $SendNextNotification
     * @access public
     */
    public $SendNextNotification = null;

    /**
     * @var string $Subject
     * @access public
     */
    public $Subject = null;

    /**
     * @var string $URL
     * @access public
     */
    public $URL = null;

    /**
     * @var string $EmailFrom
     * @access public
     */
    public $EmailFrom = null;

    /**
     * @var string $FileLocation
     * @access public
     */
    public $FileLocation = null;

    /**
     * @var string $WebLocation
     * @access public
     */
    public $WebLocation = null;

    /**
     * @var int $OptOutID
     * @access public
     */
    public $OptOutID = null;

    /**
     * @var int $DefaultMessageID
     * @access public
     */
    public $DefaultMessageID = null;

    /**
     * @var int $SummaryID
     * @access public
     */
    public $SummaryID = null;

    /**
     * @var int $ContentID
     * @access public
     */
    public $ContentID = null;

    /**
     * @var string $UseContentTitle
     * @access public
     */
    public $UseContentTitle = null;

    /**
     * @var int $UnsubscribeID
     * @access public
     */
    public $UnsubscribeID = null;

    /**
     * @var int $UseContentLink
     * @access public
     */
    public $UseContentLink = null;

    /**
     * @param boolean $BreakInheritance
     * @param SubscriptionPropertyNotificationTypes $NotificationType
     * @param boolean $SuspendNextNotification
     * @param boolean $SendNextNotification
     * @param string $Subject
     * @param string $URL
     * @param string $EmailFrom
     * @param string $FileLocation
     * @param string $WebLocation
     * @param int $OptOutID
     * @param int $DefaultMessageID
     * @param int $SummaryID
     * @param int $ContentID
     * @param string $UseContentTitle
     * @param int $UnsubscribeID
     * @param int $UseContentLink
     * @access public
     */
    public function __construct($BreakInheritance, $NotificationType, $SuspendNextNotification, $SendNextNotification, $Subject, $URL, $EmailFrom, $FileLocation, $WebLocation, $OptOutID, $DefaultMessageID, $SummaryID, $ContentID, $UseContentTitle, $UnsubscribeID, $UseContentLink)
    {
      $this->BreakInheritance = $BreakInheritance;
      $this->NotificationType = $NotificationType;
      $this->SuspendNextNotification = $SuspendNextNotification;
      $this->SendNextNotification = $SendNextNotification;
      $this->Subject = $Subject;
      $this->URL = $URL;
      $this->EmailFrom = $EmailFrom;
      $this->FileLocation = $FileLocation;
      $this->WebLocation = $WebLocation;
      $this->OptOutID = $OptOutID;
      $this->DefaultMessageID = $DefaultMessageID;
      $this->SummaryID = $SummaryID;
      $this->ContentID = $ContentID;
      $this->UseContentTitle = $UseContentTitle;
      $this->UnsubscribeID = $UnsubscribeID;
      $this->UseContentLink = $UseContentLink;
    }

}
