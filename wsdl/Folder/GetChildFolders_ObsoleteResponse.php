<?php

class GetChildFolders_ObsoleteResponse
{

    /**
     * @var FolderData[] $GetChildFolders_ObsoleteResult
     * @access public
     */
    public $GetChildFolders_ObsoleteResult = null;

    /**
     * @param FolderData[] $GetChildFolders_ObsoleteResult
     * @access public
     */
    public function __construct($GetChildFolders_ObsoleteResult)
    {
      $this->GetChildFolders_ObsoleteResult = $GetChildFolders_ObsoleteResult;
    }

}
