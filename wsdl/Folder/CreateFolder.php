<?php

class CreateFolder
{

    /**
     * @var string $FolderPath
     * @access public
     */
    public $FolderPath = null;

    /**
     * @param string $FolderPath
     * @access public
     */
    public function __construct($FolderPath)
    {
      $this->FolderPath = $FolderPath;
    }

}
