<?php

class GetChildFoldersResponse
{

    /**
     * @var FolderData[] $GetChildFoldersResult
     * @access public
     */
    public $GetChildFoldersResult = null;

    /**
     * @param FolderData[] $GetChildFoldersResult
     * @access public
     */
    public function __construct($GetChildFoldersResult)
    {
      $this->GetChildFoldersResult = $GetChildFoldersResult;
    }

}
