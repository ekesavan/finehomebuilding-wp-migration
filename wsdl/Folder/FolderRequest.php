<?php

class FolderRequest
{

    /**
     * @var int $FolderId
     * @access public
     */
    public $FolderId = null;

    /**
     * @var string $FolderName
     * @access public
     */
    public $FolderName = null;

    /**
     * @var string $FolderDescription
     * @access public
     */
    public $FolderDescription = null;

    /**
     * @var int $ParentId
     * @access public
     */
    public $ParentId = null;

    /**
     * @var string $TemplateFileName
     * @access public
     */
    public $TemplateFileName = null;

    /**
     * @var string $StyleSheet
     * @access public
     */
    public $StyleSheet = null;

    /**
     * @var boolean $SiteMapPathInherit
     * @access public
     */
    public $SiteMapPathInherit = null;

    /**
     * @var SitemapPath[] $SiteMapPath
     * @access public
     */
    public $SiteMapPath = null;

    /**
     * @var string $ImageDirectory
     * @access public
     */
    public $ImageDirectory = null;

    /**
     * @var string $FileDirectory
     * @access public
     */
    public $FileDirectory = null;

    /**
     * @var string $DomainStaging
     * @access public
     */
    public $DomainStaging = null;

    /**
     * @var string $DomainProduction
     * @access public
     */
    public $DomainProduction = null;

    /**
     * @var int $FolderType
     * @access public
     */
    public $FolderType = null;

    /**
     * @var boolean $IsDomainFolder
     * @access public
     */
    public $IsDomainFolder = null;

    /**
     * @var boolean $XmlInherited
     * @access public
     */
    public $XmlInherited = null;

    /**
     * @var boolean $EnableReplication
     * @access public
     */
    public $EnableReplication = null;

    /**
     * @var string $PublishActive
     * @access public
     */
    public $PublishActive = null;

    /**
     * @var string $XmlConfiguration
     * @access public
     */
    public $XmlConfiguration = null;

    /**
     * @var boolean $BreakInheritButton
     * @access public
     */
    public $BreakInheritButton = null;

    /**
     * @var string $FolderCfldAssignments
     * @access public
     */
    public $FolderCfldAssignments = null;

    /**
     * @var SubscriptionPropertiesData $SubscriptionProperties
     * @access public
     */
    public $SubscriptionProperties = null;

    /**
     * @var boolean $suppressNotification
     * @access public
     */
    public $suppressNotification = null;

    /**
     * @var string $ContentSubAssignments
     * @access public
     */
    public $ContentSubAssignments = null;

    /**
     * @var int $MetaInherited
     * @access public
     */
    public $MetaInherited = null;

    /**
     * @var int $MetaInheritedFrom
     * @access public
     */
    public $MetaInheritedFrom = null;

    /**
     * @var boolean $TaxonomyInherited
     * @access public
     */
    public $TaxonomyInherited = null;

    /**
     * @var int $TaxonomyInheritedFrom
     * @access public
     */
    public $TaxonomyInheritedFrom = null;

    /**
     * @var boolean $FlagInherited
     * @access public
     */
    public $FlagInherited = null;

    /**
     * @var int $FlagInheritedFrom
     * @access public
     */
    public $FlagInheritedFrom = null;

    /**
     * @var boolean $CategoryRequired
     * @access public
     */
    public $CategoryRequired = null;

    /**
     * @var string $TaxonomyIdList
     * @access public
     */
    public $TaxonomyIdList = null;

    /**
     * @var string $FlagDefIdList
     * @access public
     */
    public $FlagDefIdList = null;

    /**
     * @var boolean $ByPassMetadataUpateforFolder
     * @access public
     */
    public $ByPassMetadataUpateforFolder = null;

    /**
     * @var int $AliasInheritedFrom
     * @access public
     */
    public $AliasInheritedFrom = null;

    /**
     * @var boolean $AliasInherited
     * @access public
     */
    public $AliasInherited = null;

    /**
     * @var boolean $AliasRequired
     * @access public
     */
    public $AliasRequired = null;

    /**
     * @var int $IsContentSearchableInheritedFrom
     * @access public
     */
    public $IsContentSearchableInheritedFrom = null;

    /**
     * @var boolean $IsContentSearchableInherited
     * @access public
     */
    public $IsContentSearchableInherited = null;

    /**
     * @var boolean $IscontentSearchable
     * @access public
     */
    public $IscontentSearchable = null;

    /**
     * @var int $DisplaySettings
     * @access public
     */
    public $DisplaySettings = null;

    /**
     * @var int $DisplaySettingsInheritedFrom
     * @access public
     */
    public $DisplaySettingsInheritedFrom = null;

    /**
     * @var boolean $IsDisplaySettingsInherited
     * @access public
     */
    public $IsDisplaySettingsInherited = null;

    /**
     * @param int $FolderId
     * @param string $FolderName
     * @param string $FolderDescription
     * @param int $ParentId
     * @param string $TemplateFileName
     * @param string $StyleSheet
     * @param boolean $SiteMapPathInherit
     * @param SitemapPath[] $SiteMapPath
     * @param string $ImageDirectory
     * @param string $FileDirectory
     * @param string $DomainStaging
     * @param string $DomainProduction
     * @param int $FolderType
     * @param boolean $IsDomainFolder
     * @param boolean $XmlInherited
     * @param boolean $EnableReplication
     * @param string $PublishActive
     * @param string $XmlConfiguration
     * @param boolean $BreakInheritButton
     * @param string $FolderCfldAssignments
     * @param SubscriptionPropertiesData $SubscriptionProperties
     * @param boolean $suppressNotification
     * @param string $ContentSubAssignments
     * @param int $MetaInherited
     * @param int $MetaInheritedFrom
     * @param boolean $TaxonomyInherited
     * @param int $TaxonomyInheritedFrom
     * @param boolean $FlagInherited
     * @param int $FlagInheritedFrom
     * @param boolean $CategoryRequired
     * @param string $TaxonomyIdList
     * @param string $FlagDefIdList
     * @param boolean $ByPassMetadataUpateforFolder
     * @param int $AliasInheritedFrom
     * @param boolean $AliasInherited
     * @param boolean $AliasRequired
     * @param int $IsContentSearchableInheritedFrom
     * @param boolean $IsContentSearchableInherited
     * @param boolean $IscontentSearchable
     * @param int $DisplaySettings
     * @param int $DisplaySettingsInheritedFrom
     * @param boolean $IsDisplaySettingsInherited
     * @access public
     */
    public function __construct($FolderId, $FolderName, $FolderDescription, $ParentId, $TemplateFileName, $StyleSheet, $SiteMapPathInherit, $SiteMapPath, $ImageDirectory, $FileDirectory, $DomainStaging, $DomainProduction, $FolderType, $IsDomainFolder, $XmlInherited, $EnableReplication, $PublishActive, $XmlConfiguration, $BreakInheritButton, $FolderCfldAssignments, $SubscriptionProperties, $suppressNotification, $ContentSubAssignments, $MetaInherited, $MetaInheritedFrom, $TaxonomyInherited, $TaxonomyInheritedFrom, $FlagInherited, $FlagInheritedFrom, $CategoryRequired, $TaxonomyIdList, $FlagDefIdList, $ByPassMetadataUpateforFolder, $AliasInheritedFrom, $AliasInherited, $AliasRequired, $IsContentSearchableInheritedFrom, $IsContentSearchableInherited, $IscontentSearchable, $DisplaySettings, $DisplaySettingsInheritedFrom, $IsDisplaySettingsInherited)
    {
      $this->FolderId = $FolderId;
      $this->FolderName = $FolderName;
      $this->FolderDescription = $FolderDescription;
      $this->ParentId = $ParentId;
      $this->TemplateFileName = $TemplateFileName;
      $this->StyleSheet = $StyleSheet;
      $this->SiteMapPathInherit = $SiteMapPathInherit;
      $this->SiteMapPath = $SiteMapPath;
      $this->ImageDirectory = $ImageDirectory;
      $this->FileDirectory = $FileDirectory;
      $this->DomainStaging = $DomainStaging;
      $this->DomainProduction = $DomainProduction;
      $this->FolderType = $FolderType;
      $this->IsDomainFolder = $IsDomainFolder;
      $this->XmlInherited = $XmlInherited;
      $this->EnableReplication = $EnableReplication;
      $this->PublishActive = $PublishActive;
      $this->XmlConfiguration = $XmlConfiguration;
      $this->BreakInheritButton = $BreakInheritButton;
      $this->FolderCfldAssignments = $FolderCfldAssignments;
      $this->SubscriptionProperties = $SubscriptionProperties;
      $this->suppressNotification = $suppressNotification;
      $this->ContentSubAssignments = $ContentSubAssignments;
      $this->MetaInherited = $MetaInherited;
      $this->MetaInheritedFrom = $MetaInheritedFrom;
      $this->TaxonomyInherited = $TaxonomyInherited;
      $this->TaxonomyInheritedFrom = $TaxonomyInheritedFrom;
      $this->FlagInherited = $FlagInherited;
      $this->FlagInheritedFrom = $FlagInheritedFrom;
      $this->CategoryRequired = $CategoryRequired;
      $this->TaxonomyIdList = $TaxonomyIdList;
      $this->FlagDefIdList = $FlagDefIdList;
      $this->ByPassMetadataUpateforFolder = $ByPassMetadataUpateforFolder;
      $this->AliasInheritedFrom = $AliasInheritedFrom;
      $this->AliasInherited = $AliasInherited;
      $this->AliasRequired = $AliasRequired;
      $this->IsContentSearchableInheritedFrom = $IsContentSearchableInheritedFrom;
      $this->IsContentSearchableInherited = $IsContentSearchableInherited;
      $this->IscontentSearchable = $IscontentSearchable;
      $this->DisplaySettings = $DisplaySettings;
      $this->DisplaySettingsInheritedFrom = $DisplaySettingsInheritedFrom;
      $this->IsDisplaySettingsInherited = $IsDisplaySettingsInherited;
    }

}
