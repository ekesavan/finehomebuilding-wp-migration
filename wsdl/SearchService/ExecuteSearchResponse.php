<?php

class ExecuteSearchResponse
{

    /**
     * @var SearchContentItem[] $ExecuteSearchResult
     * @access public
     */
    public $ExecuteSearchResult = null;

    /**
     * @param SearchContentItem[] $ExecuteSearchResult
     * @access public
     */
    public function __construct($ExecuteSearchResult)
    {
      $this->ExecuteSearchResult = $ExecuteSearchResult;
    }

}
