<?php

class ExecuteOfficeSearch
{

    /**
     * @var string $query
     * @access public
     */
    public $query = null;

    /**
     * @param string $query
     * @access public
     */
    public function __construct($query)
    {
      $this->query = $query;
    }

}
