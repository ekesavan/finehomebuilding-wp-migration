<?php

class OFD
{

    /**
     * @var string $t
     * @access public
     */
    public $t = null;

    /**
     * @var string $n
     * @access public
     */
    public $n = null;

    /**
     * @var string $f
     * @access public
     */
    public $f = null;

    /**
     * @var string $a
     * @access public
     */
    public $a = null;

    /**
     * @var string $i
     * @access public
     */
    public $i = null;

    /**
     * @var boolean $e
     * @access public
     */
    public $e = null;

    /**
     * @var string $s
     * @access public
     */
    public $s = null;

    /**
     * @param string $t
     * @param string $n
     * @param string $f
     * @param string $a
     * @param string $i
     * @param boolean $e
     * @param string $s
     * @access public
     */
    public function __construct($t, $n, $f, $a, $i, $e, $s)
    {
      $this->t = $t;
      $this->n = $n;
      $this->f = $f;
      $this->a = $a;
      $this->i = $i;
      $this->e = $e;
      $this->s = $s;
    }

}
