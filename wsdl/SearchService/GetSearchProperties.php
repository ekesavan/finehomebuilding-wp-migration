<?php

class GetSearchProperties
{

    /**
     * @var string $fId
     * @access public
     */
    public $fId = null;

    /**
     * @param string $fId
     * @access public
     */
    public function __construct($fId)
    {
      $this->fId = $fId;
    }

}
