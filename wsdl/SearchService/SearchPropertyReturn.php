<?php

class SearchPropertyReturn
{

    /**
     * @var string $Caption
     * @access public
     */
    public $Caption = null;

    /**
     * @var string $DataType
     * @access public
     */
    public $DataType = null;

    /**
     * @var string $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var string $Options
     * @access public
     */
    public $Options = null;

    /**
     * @param string $Caption
     * @param string $DataType
     * @param string $Id
     * @param string $Options
     * @access public
     */
    public function __construct($Caption, $DataType, $Id, $Options)
    {
      $this->Caption = $Caption;
      $this->DataType = $DataType;
      $this->Id = $Id;
      $this->Options = $Options;
    }

}
