<?php

include_once('ExecuteSearch.php');
include_once('ExecuteSearchResponse.php');
include_once('SearchContentItem.php');
include_once('AssetData.php');
include_once('CMSContentType.php');
include_once('PermissionData.php');
include_once('CmsDataOfPermissionData.php');
include_once('BaseDataOfPermissionData.php');
include_once('ExecuteOfficeSearch.php');
include_once('OFD.php');
include_once('ExecuteOfficeSearchResponse.php');
include_once('GetSearchProperties.php');
include_once('SearchPropertyReturn.php');
include_once('GetSearchPropertiesResponse.php');

class SearchService extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'ExecuteSearch' => '\ExecuteSearch',
      'ExecuteSearchResponse' => '\ExecuteSearchResponse',
      'SearchContentItem' => '\SearchContentItem',
      'AssetData' => '\AssetData',
      'PermissionData' => '\PermissionData',
      'CmsDataOfPermissionData' => '\CmsDataOfPermissionData',
      'BaseDataOfPermissionData' => '\BaseDataOfPermissionData',
      'ExecuteOfficeSearch' => '\ExecuteOfficeSearch',
      'OFD' => '\OFD',
      'ExecuteOfficeSearchResponse' => '\ExecuteOfficeSearchResponse',
      'GetSearchProperties' => '\GetSearchProperties',
      'SearchPropertyReturn' => '\SearchPropertyReturn',
      'GetSearchPropertiesResponse' => '\GetSearchPropertiesResponse');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = 'SearchService.wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      
      parent::__construct($wsdl, $options);
    }

    /**
     * @param ExecuteSearch $parameters
     * @access public
     * @return ExecuteSearchResponse
     */
    public function ExecuteSearch(ExecuteSearch $parameters)
    {
      return $this->__soapCall('ExecuteSearch', array($parameters));
    }

    /**
     * @param ExecuteOfficeSearch $parameters
     * @access public
     * @return ExecuteOfficeSearchResponse
     */
    public function ExecuteOfficeSearch(ExecuteOfficeSearch $parameters)
    {
      return $this->__soapCall('ExecuteOfficeSearch', array($parameters));
    }

    /**
     * @param GetSearchProperties $parameters
     * @access public
     * @return GetSearchPropertiesResponse
     */
    public function GetSearchProperties(GetSearchProperties $parameters)
    {
      return $this->__soapCall('GetSearchProperties', array($parameters));
    }

}
