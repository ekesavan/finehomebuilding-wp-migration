<?php

class ExecuteOfficeSearchResponse
{

    /**
     * @var OFD[] $ExecuteOfficeSearchResult
     * @access public
     */
    public $ExecuteOfficeSearchResult = null;

    /**
     * @param OFD[] $ExecuteOfficeSearchResult
     * @access public
     */
    public function __construct($ExecuteOfficeSearchResult)
    {
      $this->ExecuteOfficeSearchResult = $ExecuteOfficeSearchResult;
    }

}
