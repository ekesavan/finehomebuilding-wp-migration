<?php

class GetSearchPropertiesResponse
{

    /**
     * @var SearchPropertyReturn[] $GetSearchPropertiesResult
     * @access public
     */
    public $GetSearchPropertiesResult = null;

    /**
     * @param SearchPropertyReturn[] $GetSearchPropertiesResult
     * @access public
     */
    public function __construct($GetSearchPropertiesResult)
    {
      $this->GetSearchPropertiesResult = $GetSearchPropertiesResult;
    }

}
