<?php

class Result
{

    /**
     * @var int $ErrorCode
     * @access public
     */
    public $ErrorCode = null;

    /**
     * @var string $ErrorMessage
     * @access public
     */
    public $ErrorMessage = null;

    /**
     * @var string $InfoMessage
     * @access public
     */
    public $InfoMessage = null;

    /**
     * @var int $Count
     * @access public
     */
    public $Count = null;

    /**
     * @param int $ErrorCode
     * @param string $ErrorMessage
     * @param string $InfoMessage
     * @param int $Count
     * @access public
     */
    public function __construct($ErrorCode, $ErrorMessage, $InfoMessage, $Count)
    {
      $this->ErrorCode = $ErrorCode;
      $this->ErrorMessage = $ErrorMessage;
      $this->InfoMessage = $InfoMessage;
      $this->Count = $Count;
    }

}
