<?php

class ecmCollection
{

    /**
     * @var int $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var string $DisplayFunction
     * @access public
     */
    public $DisplayFunction = null;

    /**
     * @var int $UserID
     * @access public
     */
    public $UserID = null;

    /**
     * @var string $SitePath
     * @access public
     */
    public $SitePath = null;

    /**
     * @var int $Preview
     * @access public
     */
    public $Preview = null;

    /**
     * @var int $SiteLanguage
     * @access public
     */
    public $SiteLanguage = null;

    /**
     * @param int $Id
     * @param string $DisplayFunction
     * @param int $UserID
     * @param string $SitePath
     * @param int $Preview
     * @param int $SiteLanguage
     * @access public
     */
    public function __construct($Id, $DisplayFunction, $UserID, $SitePath, $Preview, $SiteLanguage)
    {
      $this->Id = $Id;
      $this->DisplayFunction = $DisplayFunction;
      $this->UserID = $UserID;
      $this->SitePath = $SitePath;
      $this->Preview = $Preview;
      $this->SiteLanguage = $SiteLanguage;
    }

}
