<?php

class ecmMetadataResponse
{

    /**
     * @var string $ecmMetadataResult
     * @access public
     */
    public $ecmMetadataResult = null;

    /**
     * @param string $ecmMetadataResult
     * @access public
     */
    public function __construct($ecmMetadataResult)
    {
      $this->ecmMetadataResult = $ecmMetadataResult;
    }

}
