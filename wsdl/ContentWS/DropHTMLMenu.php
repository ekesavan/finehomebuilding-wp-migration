<?php

class DropHTMLMenu
{

    /**
     * @var string $RootMenuIds
     * @access public
     */
    public $RootMenuIds = null;

    /**
     * @var int $MenuId
     * @access public
     */
    public $MenuId = null;

    /**
     * @var string $MenuTitle
     * @access public
     */
    public $MenuTitle = null;

    /**
     * @param string $RootMenuIds
     * @param int $MenuId
     * @param string $MenuTitle
     * @access public
     */
    public function __construct($RootMenuIds, $MenuId, $MenuTitle)
    {
      $this->RootMenuIds = $RootMenuIds;
      $this->MenuId = $MenuId;
      $this->MenuTitle = $MenuTitle;
    }

}
