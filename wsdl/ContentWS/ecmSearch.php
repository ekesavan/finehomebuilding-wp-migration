<?php

class ecmSearch
{

    /**
     * @var string $StartingFolder
     * @access public
     */
    public $StartingFolder = null;

    /**
     * @var boolean $Recursive
     * @access public
     */
    public $Recursive = null;

    /**
     * @var string $TargetPage
     * @access public
     */
    public $TargetPage = null;

    /**
     * @var int $TextBoxSize
     * @access public
     */
    public $TextBoxSize = null;

    /**
     * @var int $MaxCharacters
     * @access public
     */
    public $MaxCharacters = null;

    /**
     * @var string $ButtonImageSrc
     * @access public
     */
    public $ButtonImageSrc = null;

    /**
     * @var string $ButtonText
     * @access public
     */
    public $ButtonText = null;

    /**
     * @var string $FontFace
     * @access public
     */
    public $FontFace = null;

    /**
     * @var string $FontColor
     * @access public
     */
    public $FontColor = null;

    /**
     * @var string $FontSize
     * @access public
     */
    public $FontSize = null;

    /**
     * @var boolean $Horizontal
     * @access public
     */
    public $Horizontal = null;

    /**
     * @var string $Spare1
     * @access public
     */
    public $Spare1 = null;

    /**
     * @var boolean $AddFormTag
     * @access public
     */
    public $AddFormTag = null;

    /**
     * @var int $SiteLanguage
     * @access public
     */
    public $SiteLanguage = null;

    /**
     * @param string $StartingFolder
     * @param boolean $Recursive
     * @param string $TargetPage
     * @param int $TextBoxSize
     * @param int $MaxCharacters
     * @param string $ButtonImageSrc
     * @param string $ButtonText
     * @param string $FontFace
     * @param string $FontColor
     * @param string $FontSize
     * @param boolean $Horizontal
     * @param string $Spare1
     * @param boolean $AddFormTag
     * @param int $SiteLanguage
     * @access public
     */
    public function __construct($StartingFolder, $Recursive, $TargetPage, $TextBoxSize, $MaxCharacters, $ButtonImageSrc, $ButtonText, $FontFace, $FontColor, $FontSize, $Horizontal, $Spare1, $AddFormTag, $SiteLanguage)
    {
      $this->StartingFolder = $StartingFolder;
      $this->Recursive = $Recursive;
      $this->TargetPage = $TargetPage;
      $this->TextBoxSize = $TextBoxSize;
      $this->MaxCharacters = $MaxCharacters;
      $this->ButtonImageSrc = $ButtonImageSrc;
      $this->ButtonText = $ButtonText;
      $this->FontFace = $FontFace;
      $this->FontColor = $FontColor;
      $this->FontSize = $FontSize;
      $this->Horizontal = $Horizontal;
      $this->Spare1 = $Spare1;
      $this->AddFormTag = $AddFormTag;
      $this->SiteLanguage = $SiteLanguage;
    }

}
