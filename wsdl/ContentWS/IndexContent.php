<?php

class IndexContent
{

    /**
     * @var Long[] $IDs
     * @access public
     */
    public $IDs = null;

    /**
     * @param Long[] $IDs
     * @access public
     */
    public function __construct($IDs)
    {
      $this->IDs = $IDs;
    }

}
