<?php

class ecmCalendar
{

    /**
     * @var int $CalendarId
     * @access public
     */
    public $CalendarId = null;

    /**
     * @var int $EventTypeId
     * @access public
     */
    public $EventTypeId = null;

    /**
     * @var int $Month
     * @access public
     */
    public $Month = null;

    /**
     * @var int $Year
     * @access public
     */
    public $Year = null;

    /**
     * @var string $URL
     * @access public
     */
    public $URL = null;

    /**
     * @var string $QueryString
     * @access public
     */
    public $QueryString = null;

    /**
     * @var int $UserID
     * @access public
     */
    public $UserID = null;

    /**
     * @var string $SitePath
     * @access public
     */
    public $SitePath = null;

    /**
     * @var int $Preview
     * @access public
     */
    public $Preview = null;

    /**
     * @var int $SiteLanguage
     * @access public
     */
    public $SiteLanguage = null;

    /**
     * @param int $CalendarId
     * @param int $EventTypeId
     * @param int $Month
     * @param int $Year
     * @param string $URL
     * @param string $QueryString
     * @param int $UserID
     * @param string $SitePath
     * @param int $Preview
     * @param int $SiteLanguage
     * @access public
     */
    public function __construct($CalendarId, $EventTypeId, $Month, $Year, $URL, $QueryString, $UserID, $SitePath, $Preview, $SiteLanguage)
    {
      $this->CalendarId = $CalendarId;
      $this->EventTypeId = $EventTypeId;
      $this->Month = $Month;
      $this->Year = $Year;
      $this->URL = $URL;
      $this->QueryString = $QueryString;
      $this->UserID = $UserID;
      $this->SitePath = $SitePath;
      $this->Preview = $Preview;
      $this->SiteLanguage = $SiteLanguage;
    }

}
