<?php

class GetListSummaryWithHTML
{

    /**
     * @var string $Folder
     * @access public
     */
    public $Folder = null;

    /**
     * @var int $Recursive
     * @access public
     */
    public $Recursive = null;

    /**
     * @var string $OrderBy
     * @access public
     */
    public $OrderBy = null;

    /**
     * @var int $MaxNumber
     * @access public
     */
    public $MaxNumber = null;

    /**
     * @var string $ObjType
     * @access public
     */
    public $ObjType = null;

    /**
     * @var string $OptionList
     * @access public
     */
    public $OptionList = null;

    /**
     * @param string $Folder
     * @param int $Recursive
     * @param string $OrderBy
     * @param int $MaxNumber
     * @param string $ObjType
     * @param string $OptionList
     * @access public
     */
    public function __construct($Folder, $Recursive, $OrderBy, $MaxNumber, $ObjType, $OptionList)
    {
      $this->Folder = $Folder;
      $this->Recursive = $Recursive;
      $this->OrderBy = $OrderBy;
      $this->MaxNumber = $MaxNumber;
      $this->ObjType = $ObjType;
      $this->OptionList = $OptionList;
    }

}
