<?php

class PdfStatusInfo
{

    /**
     * @var string $AssetId
     * @access public
     */
    public $AssetId = null;

    /**
     * @var int $StatusCode
     * @access public
     */
    public $StatusCode = null;

    /**
     * @var string $ErrorMessage
     * @access public
     */
    public $ErrorMessage = null;

    /**
     * @param string $AssetId
     * @param int $StatusCode
     * @param string $ErrorMessage
     * @access public
     */
    public function __construct($AssetId, $StatusCode, $ErrorMessage)
    {
      $this->AssetId = $AssetId;
      $this->StatusCode = $StatusCode;
      $this->ErrorMessage = $ErrorMessage;
    }

}
