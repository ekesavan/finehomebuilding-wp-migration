<?php

class ShowContentBlock
{

    /**
     * @var string $Title
     * @access public
     */
    public $Title = null;

    /**
     * @var int $ContentID
     * @access public
     */
    public $ContentID = null;

    /**
     * @var int $ContentLanguage
     * @access public
     */
    public $ContentLanguage = null;

    /**
     * @var string $Html
     * @access public
     */
    public $Html = null;

    /**
     * @var string $FolderID
     * @access public
     */
    public $FolderID = null;

    /**
     * @var boolean $PrivateContent
     * @access public
     */
    public $PrivateContent = null;

    /**
     * @var dateTime $GoLiveDate
     * @access public
     */
    public $GoLiveDate = null;

    /**
     * @var string $DisplayGoLiveDate
     * @access public
     */
    public $DisplayGoLiveDate = null;

    /**
     * @var dateTime $EndDate
     * @access public
     */
    public $EndDate = null;

    /**
     * @var string $DisplayEndDate
     * @access public
     */
    public $DisplayEndDate = null;

    /**
     * @var CMSEndDateAction $EndDateAction
     * @access public
     */
    public $EndDateAction = null;

    /**
     * @var string $ContentStatus
     * @access public
     */
    public $ContentStatus = null;

    /**
     * @var string $Xslt1
     * @access public
     */
    public $Xslt1 = null;

    /**
     * @var string $Xslt2
     * @access public
     */
    public $Xslt2 = null;

    /**
     * @var string $Xslt3
     * @access public
     */
    public $Xslt3 = null;

    /**
     * @var string $Xslt4
     * @access public
     */
    public $Xslt4 = null;

    /**
     * @var string $Xslt5
     * @access public
     */
    public $Xslt5 = null;

    /**
     * @var int $DefaultXslt
     * @access public
     */
    public $DefaultXslt = null;

    /**
     * @var string $PackageDisplayXslt
     * @access public
     */
    public $PackageDisplayXslt = null;

    /**
     * @var CMSContentType $ContentType
     * @access public
     */
    public $ContentType = null;

    /**
     * @var int $TemplateID
     * @access public
     */
    public $TemplateID = null;

    /**
     * @var string $FieldList
     * @access public
     */
    public $FieldList = null;

    /**
     * @var string $Image
     * @access public
     */
    public $Image = null;

    /**
     * @var string $ImageThumbnail
     * @access public
     */
    public $ImageThumbnail = null;

    /**
     * @var AssetData $AssetData
     * @access public
     */
    public $AssetData = null;

    /**
     * @param string $Title
     * @param int $ContentID
     * @param int $ContentLanguage
     * @param string $Html
     * @param string $FolderID
     * @param boolean $PrivateContent
     * @param dateTime $GoLiveDate
     * @param string $DisplayGoLiveDate
     * @param dateTime $EndDate
     * @param string $DisplayEndDate
     * @param CMSEndDateAction $EndDateAction
     * @param string $ContentStatus
     * @param string $Xslt1
     * @param string $Xslt2
     * @param string $Xslt3
     * @param string $Xslt4
     * @param string $Xslt5
     * @param int $DefaultXslt
     * @param string $PackageDisplayXslt
     * @param CMSContentType $ContentType
     * @param int $TemplateID
     * @param string $FieldList
     * @param string $Image
     * @param string $ImageThumbnail
     * @param AssetData $AssetData
     * @access public
     */
    public function __construct($Title, $ContentID, $ContentLanguage, $Html, $FolderID, $PrivateContent, $GoLiveDate, $DisplayGoLiveDate, $EndDate, $DisplayEndDate, $EndDateAction, $ContentStatus, $Xslt1, $Xslt2, $Xslt3, $Xslt4, $Xslt5, $DefaultXslt, $PackageDisplayXslt, $ContentType, $TemplateID, $FieldList, $Image, $ImageThumbnail, $AssetData)
    {
      $this->Title = $Title;
      $this->ContentID = $ContentID;
      $this->ContentLanguage = $ContentLanguage;
      $this->Html = $Html;
      $this->FolderID = $FolderID;
      $this->PrivateContent = $PrivateContent;
      $this->GoLiveDate = $GoLiveDate;
      $this->DisplayGoLiveDate = $DisplayGoLiveDate;
      $this->EndDate = $EndDate;
      $this->DisplayEndDate = $DisplayEndDate;
      $this->EndDateAction = $EndDateAction;
      $this->ContentStatus = $ContentStatus;
      $this->Xslt1 = $Xslt1;
      $this->Xslt2 = $Xslt2;
      $this->Xslt3 = $Xslt3;
      $this->Xslt4 = $Xslt4;
      $this->Xslt5 = $Xslt5;
      $this->DefaultXslt = $DefaultXslt;
      $this->PackageDisplayXslt = $PackageDisplayXslt;
      $this->ContentType = $ContentType;
      $this->TemplateID = $TemplateID;
      $this->FieldList = $FieldList;
      $this->Image = $Image;
      $this->ImageThumbnail = $ImageThumbnail;
      $this->AssetData = $AssetData;
    }

}
