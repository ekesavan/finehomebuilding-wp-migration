<?php

class ecmListSummaryXMLWithHtmlResponse
{

    /**
     * @var string $ecmListSummaryXMLWithHtmlResult
     * @access public
     */
    public $ecmListSummaryXMLWithHtmlResult = null;

    /**
     * @param string $ecmListSummaryXMLWithHtmlResult
     * @access public
     */
    public function __construct($ecmListSummaryXMLWithHtmlResult)
    {
      $this->ecmListSummaryXMLWithHtmlResult = $ecmListSummaryXMLWithHtmlResult;
    }

}
