<?php

class ecmSearchDisplayXMLResponse
{

    /**
     * @var string $ecmSearchDisplayXMLResult
     * @access public
     */
    public $ecmSearchDisplayXMLResult = null;

    /**
     * @param string $ecmSearchDisplayXMLResult
     * @access public
     */
    public function __construct($ecmSearchDisplayXMLResult)
    {
      $this->ecmSearchDisplayXMLResult = $ecmSearchDisplayXMLResult;
    }

}
