<?php

class DropHTMLMenuResponse
{

    /**
     * @var string $DropHTMLMenuResult
     * @access public
     */
    public $DropHTMLMenuResult = null;

    /**
     * @param string $DropHTMLMenuResult
     * @access public
     */
    public function __construct($DropHTMLMenuResult)
    {
      $this->DropHTMLMenuResult = $DropHTMLMenuResult;
    }

}
