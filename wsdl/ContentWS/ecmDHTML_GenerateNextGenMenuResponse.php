<?php

class ecmDHTML_GenerateNextGenMenuResponse
{

    /**
     * @var string $ecmDHTML_GenerateNextGenMenuResult
     * @access public
     */
    public $ecmDHTML_GenerateNextGenMenuResult = null;

    /**
     * @param string $ecmDHTML_GenerateNextGenMenuResult
     * @access public
     */
    public function __construct($ecmDHTML_GenerateNextGenMenuResult)
    {
      $this->ecmDHTML_GenerateNextGenMenuResult = $ecmDHTML_GenerateNextGenMenuResult;
    }

}
