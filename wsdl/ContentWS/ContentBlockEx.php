<?php

class ContentBlockEx
{

    /**
     * @var int $ContentId
     * @access public
     */
    public $ContentId = null;

    /**
     * @var string $Xslt
     * @access public
     */
    public $Xslt = null;

    /**
     * @var string $OverrideID
     * @access public
     */
    public $OverrideID = null;

    /**
     * @param int $ContentId
     * @param string $Xslt
     * @param string $OverrideID
     * @access public
     */
    public function __construct($ContentId, $Xslt, $OverrideID)
    {
      $this->ContentId = $ContentId;
      $this->Xslt = $Xslt;
      $this->OverrideID = $OverrideID;
    }

}
