<?php

class ecmListSummaryResponse
{

    /**
     * @var string $ecmListSummaryResult
     * @access public
     */
    public $ecmListSummaryResult = null;

    /**
     * @param string $ecmListSummaryResult
     * @access public
     */
    public function __construct($ecmListSummaryResult)
    {
      $this->ecmListSummaryResult = $ecmListSummaryResult;
    }

}
