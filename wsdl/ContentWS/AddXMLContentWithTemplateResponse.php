<?php

class AddXMLContentWithTemplateResponse
{

    /**
     * @var anyType $AddXMLContentWithTemplateResult
     * @access public
     */
    public $AddXMLContentWithTemplateResult = null;

    /**
     * @param anyType $AddXMLContentWithTemplateResult
     * @access public
     */
    public function __construct($AddXMLContentWithTemplateResult)
    {
      $this->AddXMLContentWithTemplateResult = $AddXMLContentWithTemplateResult;
    }

}
