<?php

class GetSearchDisplayResponse
{

    /**
     * @var SearchContentItem[] $GetSearchDisplayResult
     * @access public
     */
    public $GetSearchDisplayResult = null;

    /**
     * @param SearchContentItem[] $GetSearchDisplayResult
     * @access public
     */
    public function __construct($GetSearchDisplayResult)
    {
      $this->GetSearchDisplayResult = $GetSearchDisplayResult;
    }

}
