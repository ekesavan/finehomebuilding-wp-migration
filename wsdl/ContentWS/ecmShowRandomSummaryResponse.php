<?php

class ecmShowRandomSummaryResponse
{

    /**
     * @var string $ecmShowRandomSummaryResult
     * @access public
     */
    public $ecmShowRandomSummaryResult = null;

    /**
     * @param string $ecmShowRandomSummaryResult
     * @access public
     */
    public function __construct($ecmShowRandomSummaryResult)
    {
      $this->ecmShowRandomSummaryResult = $ecmShowRandomSummaryResult;
    }

}
