<?php

class MetaData
{

    /**
     * @var string $ItemList
     * @access public
     */
    public $ItemList = null;

    /**
     * @param string $ItemList
     * @access public
     */
    public function __construct($ItemList)
    {
      $this->ItemList = $ItemList;
    }

}
