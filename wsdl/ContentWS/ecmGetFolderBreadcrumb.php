<?php

class ecmGetFolderBreadcrumb
{

    /**
     * @var int $contentId
     * @access public
     */
    public $contentId = null;

    /**
     * @var int $folderId
     * @access public
     */
    public $folderId = null;

    /**
     * @var string $pathSeparator
     * @access public
     */
    public $pathSeparator = null;

    /**
     * @var int $linkNodes
     * @access public
     */
    public $linkNodes = null;

    /**
     * @var string $linkTarget
     * @access public
     */
    public $linkTarget = null;

    /**
     * @var int $displayVertical
     * @access public
     */
    public $displayVertical = null;

    /**
     * @var string $pageUrl
     * @access public
     */
    public $pageUrl = null;

    /**
     * @var string $dynamicParameter
     * @access public
     */
    public $dynamicParameter = null;

    /**
     * @var int $UserID
     * @access public
     */
    public $UserID = null;

    /**
     * @var string $SitePath
     * @access public
     */
    public $SitePath = null;

    /**
     * @var int $Preview
     * @access public
     */
    public $Preview = null;

    /**
     * @var int $SiteLanguage
     * @access public
     */
    public $SiteLanguage = null;

    /**
     * @param int $contentId
     * @param int $folderId
     * @param string $pathSeparator
     * @param int $linkNodes
     * @param string $linkTarget
     * @param int $displayVertical
     * @param string $pageUrl
     * @param string $dynamicParameter
     * @param int $UserID
     * @param string $SitePath
     * @param int $Preview
     * @param int $SiteLanguage
     * @access public
     */
    public function __construct($contentId, $folderId, $pathSeparator, $linkNodes, $linkTarget, $displayVertical, $pageUrl, $dynamicParameter, $UserID, $SitePath, $Preview, $SiteLanguage)
    {
      $this->contentId = $contentId;
      $this->folderId = $folderId;
      $this->pathSeparator = $pathSeparator;
      $this->linkNodes = $linkNodes;
      $this->linkTarget = $linkTarget;
      $this->displayVertical = $displayVertical;
      $this->pageUrl = $pageUrl;
      $this->dynamicParameter = $dynamicParameter;
      $this->UserID = $UserID;
      $this->SitePath = $SitePath;
      $this->Preview = $Preview;
      $this->SiteLanguage = $SiteLanguage;
    }

}
