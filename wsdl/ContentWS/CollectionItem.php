<?php

class CollectionItem
{

    /**
     * @var int $ID
     * @access public
     */
    public $ID = null;

    /**
     * @var string $Title
     * @access public
     */
    public $Title = null;

    /**
     * @var string $Teaser
     * @access public
     */
    public $Teaser = null;

    /**
     * @var dateTime $DateModified
     * @access public
     */
    public $DateModified = null;

    /**
     * @var string $DisplayDateModified
     * @access public
     */
    public $DisplayDateModified = null;

    /**
     * @var string $LastEditorLname
     * @access public
     */
    public $LastEditorLname = null;

    /**
     * @var string $LastEditorFname
     * @access public
     */
    public $LastEditorFname = null;

    /**
     * @var string $Comment
     * @access public
     */
    public $Comment = null;

    /**
     * @var dateTime $StartDate
     * @access public
     */
    public $StartDate = null;

    /**
     * @var string $DisplayStartDate
     * @access public
     */
    public $DisplayStartDate = null;

    /**
     * @var dateTime $EndDate
     * @access public
     */
    public $EndDate = null;

    /**
     * @var string $DisplayEndDate
     * @access public
     */
    public $DisplayEndDate = null;

    /**
     * @var string $Links
     * @access public
     */
    public $Links = null;

    /**
     * @var string $Html
     * @access public
     */
    public $Html = null;

    /**
     * @var dateTime $DateCreated
     * @access public
     */
    public $DateCreated = null;

    /**
     * @var string $DisplayDateCreated
     * @access public
     */
    public $DisplayDateCreated = null;

    /**
     * @param int $ID
     * @param string $Title
     * @param string $Teaser
     * @param dateTime $DateModified
     * @param string $DisplayDateModified
     * @param string $LastEditorLname
     * @param string $LastEditorFname
     * @param string $Comment
     * @param dateTime $StartDate
     * @param string $DisplayStartDate
     * @param dateTime $EndDate
     * @param string $DisplayEndDate
     * @param string $Links
     * @param string $Html
     * @param dateTime $DateCreated
     * @param string $DisplayDateCreated
     * @access public
     */
    public function __construct($ID, $Title, $Teaser, $DateModified, $DisplayDateModified, $LastEditorLname, $LastEditorFname, $Comment, $StartDate, $DisplayStartDate, $EndDate, $DisplayEndDate, $Links, $Html, $DateCreated, $DisplayDateCreated)
    {
      $this->ID = $ID;
      $this->Title = $Title;
      $this->Teaser = $Teaser;
      $this->DateModified = $DateModified;
      $this->DisplayDateModified = $DisplayDateModified;
      $this->LastEditorLname = $LastEditorLname;
      $this->LastEditorFname = $LastEditorFname;
      $this->Comment = $Comment;
      $this->StartDate = $StartDate;
      $this->DisplayStartDate = $DisplayStartDate;
      $this->EndDate = $EndDate;
      $this->DisplayEndDate = $DisplayEndDate;
      $this->Links = $Links;
      $this->Html = $Html;
      $this->DateCreated = $DateCreated;
      $this->DisplayDateCreated = $DisplayDateCreated;
    }

}
