<?php

class ecmGetFolderBreadcrumbResponse
{

    /**
     * @var string $ecmGetFolderBreadcrumbResult
     * @access public
     */
    public $ecmGetFolderBreadcrumbResult = null;

    /**
     * @param string $ecmGetFolderBreadcrumbResult
     * @access public
     */
    public function __construct($ecmGetFolderBreadcrumbResult)
    {
      $this->ecmGetFolderBreadcrumbResult = $ecmGetFolderBreadcrumbResult;
    }

}
