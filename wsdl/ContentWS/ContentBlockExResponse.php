<?php

class ContentBlockExResponse
{

    /**
     * @var string $ContentBlockExResult
     * @access public
     */
    public $ContentBlockExResult = null;

    /**
     * @param string $ContentBlockExResult
     * @access public
     */
    public function __construct($ContentBlockExResult)
    {
      $this->ContentBlockExResult = $ContentBlockExResult;
    }

}
