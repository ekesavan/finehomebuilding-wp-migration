<?php

class ecmSearchDisplay
{

    /**
     * @var int $MaxNumber
     * @access public
     */
    public $MaxNumber = null;

    /**
     * @var string $StyleInfo
     * @access public
     */
    public $StyleInfo = null;

    /**
     * @var boolean $ShowDate
     * @access public
     */
    public $ShowDate = null;

    /**
     * @var boolean $ReturnEmptyMsg
     * @access public
     */
    public $ReturnEmptyMsg = null;

    /**
     * @var string $FormFields
     * @access public
     */
    public $FormFields = null;

    /**
     * @var boolean $ShowSummary
     * @access public
     */
    public $ShowSummary = null;

    /**
     * @var int $UserID
     * @access public
     */
    public $UserID = null;

    /**
     * @var string $SitePath
     * @access public
     */
    public $SitePath = null;

    /**
     * @var int $Preview
     * @access public
     */
    public $Preview = null;

    /**
     * @var int $SiteLanguage
     * @access public
     */
    public $SiteLanguage = null;

    /**
     * @param int $MaxNumber
     * @param string $StyleInfo
     * @param boolean $ShowDate
     * @param boolean $ReturnEmptyMsg
     * @param string $FormFields
     * @param boolean $ShowSummary
     * @param int $UserID
     * @param string $SitePath
     * @param int $Preview
     * @param int $SiteLanguage
     * @access public
     */
    public function __construct($MaxNumber, $StyleInfo, $ShowDate, $ReturnEmptyMsg, $FormFields, $ShowSummary, $UserID, $SitePath, $Preview, $SiteLanguage)
    {
      $this->MaxNumber = $MaxNumber;
      $this->StyleInfo = $StyleInfo;
      $this->ShowDate = $ShowDate;
      $this->ReturnEmptyMsg = $ReturnEmptyMsg;
      $this->FormFields = $FormFields;
      $this->ShowSummary = $ShowSummary;
      $this->UserID = $UserID;
      $this->SitePath = $SitePath;
      $this->Preview = $Preview;
      $this->SiteLanguage = $SiteLanguage;
    }

}
