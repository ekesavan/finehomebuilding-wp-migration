<?php

class AddTranslatedContentResponse
{

    /**
     * @var anyType $AddTranslatedContentResult
     * @access public
     */
    public $AddTranslatedContentResult = null;

    /**
     * @param anyType $AddTranslatedContentResult
     * @access public
     */
    public function __construct($AddTranslatedContentResult)
    {
      $this->AddTranslatedContentResult = $AddTranslatedContentResult;
    }

}
