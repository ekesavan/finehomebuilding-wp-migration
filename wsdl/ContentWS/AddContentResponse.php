<?php

class AddContentResponse
{

    /**
     * @var anyType $AddContentResult
     * @access public
     */
    public $AddContentResult = null;

    /**
     * @param anyType $AddContentResult
     * @access public
     */
    public function __construct($AddContentResult)
    {
      $this->AddContentResult = $AddContentResult;
    }

}
