<?php

class ecmContentRatingDialogResponse
{

    /**
     * @var string $ecmContentRatingDialogResult
     * @access public
     */
    public $ecmContentRatingDialogResult = null;

    /**
     * @param string $ecmContentRatingDialogResult
     * @access public
     */
    public function __construct($ecmContentRatingDialogResult)
    {
      $this->ecmContentRatingDialogResult = $ecmContentRatingDialogResult;
    }

}
