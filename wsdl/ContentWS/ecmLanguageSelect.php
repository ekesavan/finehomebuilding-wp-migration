<?php

class ecmLanguageSelect
{

    /**
     * @var int $SiteLanguage
     * @access public
     */
    public $SiteLanguage = null;

    /**
     * @param int $SiteLanguage
     * @access public
     */
    public function __construct($SiteLanguage)
    {
      $this->SiteLanguage = $SiteLanguage;
    }

}
