<?php

class ecmContentBlockExResponse
{

    /**
     * @var string $ecmContentBlockExResult
     * @access public
     */
    public $ecmContentBlockExResult = null;

    /**
     * @param string $ecmContentBlockExResult
     * @access public
     */
    public function __construct($ecmContentBlockExResult)
    {
      $this->ecmContentBlockExResult = $ecmContentBlockExResult;
    }

}
