<?php

class ecmSearchDisplayResponse
{

    /**
     * @var string $ecmSearchDisplayResult
     * @access public
     */
    public $ecmSearchDisplayResult = null;

    /**
     * @param string $ecmSearchDisplayResult
     * @access public
     */
    public function __construct($ecmSearchDisplayResult)
    {
      $this->ecmSearchDisplayResult = $ecmSearchDisplayResult;
    }

}
