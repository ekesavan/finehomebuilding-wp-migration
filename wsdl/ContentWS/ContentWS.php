<?php

include_once('SendNotification.php');
include_once('PdfStatusInfo.php');
include_once('SendNotificationResponse.php');
include_once('Collection.php');
include_once('CollectionResponse.php');
include_once('GetCollection.php');
include_once('GetCollectionResponse.php');
include_once('CollectionItemsResult.php');
include_once('Result.php');
include_once('CollectionItem.php');
include_once('ContentBlock.php');
include_once('ContentBlockResponse.php');
include_once('ContentBlockEx.php');
include_once('ContentBlockExResponse.php');
include_once('GetContentBlock.php');
include_once('GetContentBlockResponse.php');
include_once('ShowContentResult.php');
include_once('ShowContentBlock.php');
include_once('CMSEndDateAction.php');
include_once('CMSContentType.php');
include_once('AssetData.php');
include_once('ListSummary.php');
include_once('ListSummaryResponse.php');
include_once('ShowRandomSummary.php');
include_once('ShowRandomSummaryResponse.php');
include_once('ShowRandomContent.php');
include_once('ShowRandomContentResponse.php');
include_once('SingleSummary.php');
include_once('SingleSummaryResponse.php');
include_once('MetaData.php');
include_once('MetaDataResponse.php');
include_once('AddTranslatedContent.php');
include_once('AddTranslatedContentResponse.php');
include_once('AddContent.php');
include_once('AddContentResponse.php');
include_once('AddXMLContentWithTemplate.php');
include_once('AddXMLContentWithTemplateResponse.php');
include_once('AddTranslatedXMLContent.php');
include_once('AddTranslatedXMLContentResponse.php');
include_once('GetListSummary.php');
include_once('ContentBase.php');
include_once('CMSContentSubtype.php');
include_once('ItemStatus.php');
include_once('AnalyticsData.php');
include_once('GetListSummaryResponse.php');
include_once('TeaserResult.php');
include_once('GetListSummaryWithHTML.php');
include_once('GetListSummaryWithHTMLResponse.php');
include_once('GetSingleSummary.php');
include_once('GetSingleSummaryResponse.php');
include_once('SearchDisplay.php');
include_once('SearchDisplayResponse.php');
include_once('GetSearchDisplay.php');
include_once('SearchContentItem.php');
include_once('PermissionData.php');
include_once('CmsDataOfPermissionData.php');
include_once('BaseDataOfPermissionData.php');
include_once('GetSearchDisplayResponse.php');
include_once('GetFormBlock.php');
include_once('GetFormBlockResponse.php');
include_once('GetEventsByCalendar.php');
include_once('GetEventsByCalendarResponse.php');
include_once('ecmCalendar.php');
include_once('ecmCalendarResponse.php');
include_once('GenerateHTMLMenu.php');
include_once('GenerateHTMLMenuResponse.php');
include_once('DropHTMLMenu.php');
include_once('DropHTMLMenuResponse.php');
include_once('DoScheduledContent.php');
include_once('DoScheduledContentResponse.php');
include_once('DoRunJob.php');
include_once('JobType.php');
include_once('DoRunJobResponse.php');
include_once('IndexContent.php');
include_once('IndexContentResponse.php');
include_once('DoEndDateAction.php');
include_once('DoEndDateActionResponse.php');
include_once('ecmGetContentBlock.php');
include_once('ecmGetContentBlockResponse.php');
include_once('ecmContentBlockEx.php');
include_once('ecmContentBlockExResponse.php');
include_once('ecmFormBlock.php');
include_once('ecmFormBlockResponse.php');
include_once('ecmListSummary.php');
include_once('ecmListSummaryResponse.php');
include_once('ecmSingleSummary.php');
include_once('ecmSingleSummaryResponse.php');
include_once('ecmListSummaryXML.php');
include_once('ecmListSummaryXMLResponse.php');
include_once('ecmListSummaryXMLWithHtml.php');
include_once('ecmListSummaryXMLWithHtmlResponse.php');
include_once('ecmMetadata.php');
include_once('ecmMetadataResponse.php');
include_once('ecmCollectionXML.php');
include_once('ecmCollectionXMLResponse.php');
include_once('ecmCollection.php');
include_once('ecmCollectionResponse.php');
include_once('ecmShowRandomContent.php');
include_once('ecmShowRandomContentResponse.php');
include_once('ecmShowRandomSummary.php');
include_once('ecmShowRandomSummaryResponse.php');
include_once('ecmRssCollection.php');
include_once('ecmRssCollectionResponse.php');
include_once('ecmRssSummary.php');
include_once('ecmRssSummaryResponse.php');
include_once('ecmEvtCalendar.php');
include_once('ecmEvtCalendarResponse.php');
include_once('ecmSearch.php');
include_once('ecmSearchResponse.php');
include_once('ecmSearchDisplay.php');
include_once('ecmSearchDisplayResponse.php');
include_once('ecmSearchDisplayXML.php');
include_once('ecmSearchDisplayXMLResponse.php');
include_once('ecmIndexSearchDisplay.php');
include_once('ecmIndexSearchDisplayResponse.php');
include_once('ecmIndexSearch.php');
include_once('ecmIndexSearchResponse.php');
include_once('ecmLogin.php');
include_once('ecmLoginResponse.php');
include_once('ecmLanguageSelect.php');
include_once('ecmLanguageSelectResponse.php');
include_once('ecmGetMenuXML.php');
include_once('ecmGetMenuXMLResponse.php');
include_once('ecmDHTML_GenerateNextGenMenu.php');
include_once('ecmDHTML_GenerateNextGenMenuResponse.php');
include_once('ecmDHTML_DropNextGenMenu.php');
include_once('ecmDHTML_DropNextGenMenuResponse.php');
include_once('ecmBusinessRulesByID.php');
include_once('ecmBusinessRulesByIDResponse.php');
include_once('ecmBusinessRulesByIdentifier.php');
include_once('ecmBusinessRulesByIdentifierResponse.php');
include_once('ecmTrackVisit.php');
include_once('ecmTrackVisitResponse.php');
include_once('ecmContentRatingDialog.php');
include_once('ecmContentRatingDialogResponse.php');
include_once('ecmContentRatingResult.php');
include_once('ecmContentRatingResultResponse.php');
include_once('ecmGetFolderBreadcrumb.php');
include_once('ecmGetFolderBreadcrumbResponse.php');
include_once('ecmMetadataList.php');
include_once('ecmMetadataListResponse.php');
include_once('ecmRssAggregator.php');
include_once('ecmRssAggregatorResponse.php');
include_once('GetGUID.php');
include_once('GetGUIDResponse.php');

class ContentWS extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'SendNotification' => '\SendNotification',
      'PdfStatusInfo' => '\PdfStatusInfo',
      'SendNotificationResponse' => '\SendNotificationResponse',
      'Collection' => '\Collection',
      'CollectionResponse' => '\CollectionResponse',
      'GetCollection' => '\GetCollection',
      'GetCollectionResponse' => '\GetCollectionResponse',
      'CollectionItemsResult' => '\CollectionItemsResult',
      'Result' => '\Result',
      'CollectionItem' => '\CollectionItem',
      'ContentBlock' => '\ContentBlock',
      'ContentBlockResponse' => '\ContentBlockResponse',
      'ContentBlockEx' => '\ContentBlockEx',
      'ContentBlockExResponse' => '\ContentBlockExResponse',
      'GetContentBlock' => '\GetContentBlock',
      'GetContentBlockResponse' => '\GetContentBlockResponse',
      'ShowContentResult' => '\ShowContentResult',
      'ShowContentBlock' => '\ShowContentBlock',
      'AssetData' => '\AssetData',
      'ListSummary' => '\ListSummary',
      'ListSummaryResponse' => '\ListSummaryResponse',
      'ShowRandomSummary' => '\ShowRandomSummary',
      'ShowRandomSummaryResponse' => '\ShowRandomSummaryResponse',
      'ShowRandomContent' => '\ShowRandomContent',
      'ShowRandomContentResponse' => '\ShowRandomContentResponse',
      'SingleSummary' => '\SingleSummary',
      'SingleSummaryResponse' => '\SingleSummaryResponse',
      'MetaData' => '\MetaData',
      'MetaDataResponse' => '\MetaDataResponse',
      'AddTranslatedContent' => '\AddTranslatedContent',
      'AddTranslatedContentResponse' => '\AddTranslatedContentResponse',
      'AddContent' => '\AddContent',
      'AddContentResponse' => '\AddContentResponse',
      'AddXMLContentWithTemplate' => '\AddXMLContentWithTemplate',
      'AddXMLContentWithTemplateResponse' => '\AddXMLContentWithTemplateResponse',
      'AddTranslatedXMLContent' => '\AddTranslatedXMLContent',
      'AddTranslatedXMLContentResponse' => '\AddTranslatedXMLContentResponse',
      'GetListSummary' => '\GetListSummary',
      'ContentBase' => '\ContentBase',
      'AnalyticsData' => '\AnalyticsData',
      'GetListSummaryResponse' => '\GetListSummaryResponse',
      'TeaserResult' => '\TeaserResult',
      'GetListSummaryWithHTML' => '\GetListSummaryWithHTML',
      'GetListSummaryWithHTMLResponse' => '\GetListSummaryWithHTMLResponse',
      'GetSingleSummary' => '\GetSingleSummary',
      'GetSingleSummaryResponse' => '\GetSingleSummaryResponse',
      'SearchDisplay' => '\SearchDisplay',
      'SearchDisplayResponse' => '\SearchDisplayResponse',
      'GetSearchDisplay' => '\GetSearchDisplay',
      'SearchContentItem' => '\SearchContentItem',
      'PermissionData' => '\PermissionData',
      'CmsDataOfPermissionData' => '\CmsDataOfPermissionData',
      'BaseDataOfPermissionData' => '\BaseDataOfPermissionData',
      'GetSearchDisplayResponse' => '\GetSearchDisplayResponse',
      'GetFormBlock' => '\GetFormBlock',
      'GetFormBlockResponse' => '\GetFormBlockResponse',
      'GetEventsByCalendar' => '\GetEventsByCalendar',
      'GetEventsByCalendarResponse' => '\GetEventsByCalendarResponse',
      'ecmCalendar' => '\ecmCalendar',
      'ecmCalendarResponse' => '\ecmCalendarResponse',
      'GenerateHTMLMenu' => '\GenerateHTMLMenu',
      'GenerateHTMLMenuResponse' => '\GenerateHTMLMenuResponse',
      'DropHTMLMenu' => '\DropHTMLMenu',
      'DropHTMLMenuResponse' => '\DropHTMLMenuResponse',
      'DoScheduledContent' => '\DoScheduledContent',
      'DoScheduledContentResponse' => '\DoScheduledContentResponse',
      'DoRunJob' => '\DoRunJob',
      'DoRunJobResponse' => '\DoRunJobResponse',
      'IndexContent' => '\IndexContent',
      'IndexContentResponse' => '\IndexContentResponse',
      'DoEndDateAction' => '\DoEndDateAction',
      'DoEndDateActionResponse' => '\DoEndDateActionResponse',
      'ecmGetContentBlock' => '\ecmGetContentBlock',
      'ecmGetContentBlockResponse' => '\ecmGetContentBlockResponse',
      'ecmContentBlockEx' => '\ecmContentBlockEx',
      'ecmContentBlockExResponse' => '\ecmContentBlockExResponse',
      'ecmFormBlock' => '\ecmFormBlock',
      'ecmFormBlockResponse' => '\ecmFormBlockResponse',
      'ecmListSummary' => '\ecmListSummary',
      'ecmListSummaryResponse' => '\ecmListSummaryResponse',
      'ecmSingleSummary' => '\ecmSingleSummary',
      'ecmSingleSummaryResponse' => '\ecmSingleSummaryResponse',
      'ecmListSummaryXML' => '\ecmListSummaryXML',
      'ecmListSummaryXMLResponse' => '\ecmListSummaryXMLResponse',
      'ecmListSummaryXMLWithHtml' => '\ecmListSummaryXMLWithHtml',
      'ecmListSummaryXMLWithHtmlResponse' => '\ecmListSummaryXMLWithHtmlResponse',
      'ecmMetadata' => '\ecmMetadata',
      'ecmMetadataResponse' => '\ecmMetadataResponse',
      'ecmCollectionXML' => '\ecmCollectionXML',
      'ecmCollectionXMLResponse' => '\ecmCollectionXMLResponse',
      'ecmCollection' => '\ecmCollection',
      'ecmCollectionResponse' => '\ecmCollectionResponse',
      'ecmShowRandomContent' => '\ecmShowRandomContent',
      'ecmShowRandomContentResponse' => '\ecmShowRandomContentResponse',
      'ecmShowRandomSummary' => '\ecmShowRandomSummary',
      'ecmShowRandomSummaryResponse' => '\ecmShowRandomSummaryResponse',
      'ecmRssCollection' => '\ecmRssCollection',
      'ecmRssCollectionResponse' => '\ecmRssCollectionResponse',
      'ecmRssSummary' => '\ecmRssSummary',
      'ecmRssSummaryResponse' => '\ecmRssSummaryResponse',
      'ecmEvtCalendar' => '\ecmEvtCalendar',
      'ecmEvtCalendarResponse' => '\ecmEvtCalendarResponse',
      'ecmSearch' => '\ecmSearch',
      'ecmSearchResponse' => '\ecmSearchResponse',
      'ecmSearchDisplay' => '\ecmSearchDisplay',
      'ecmSearchDisplayResponse' => '\ecmSearchDisplayResponse',
      'ecmSearchDisplayXML' => '\ecmSearchDisplayXML',
      'ecmSearchDisplayXMLResponse' => '\ecmSearchDisplayXMLResponse',
      'ecmIndexSearchDisplay' => '\ecmIndexSearchDisplay',
      'ecmIndexSearchDisplayResponse' => '\ecmIndexSearchDisplayResponse',
      'ecmIndexSearch' => '\ecmIndexSearch',
      'ecmIndexSearchResponse' => '\ecmIndexSearchResponse',
      'ecmLogin' => '\ecmLogin',
      'ecmLoginResponse' => '\ecmLoginResponse',
      'ecmLanguageSelect' => '\ecmLanguageSelect',
      'ecmLanguageSelectResponse' => '\ecmLanguageSelectResponse',
      'ecmGetMenuXML' => '\ecmGetMenuXML',
      'ecmGetMenuXMLResponse' => '\ecmGetMenuXMLResponse',
      'ecmDHTML_GenerateNextGenMenu' => '\ecmDHTML_GenerateNextGenMenu',
      'ecmDHTML_GenerateNextGenMenuResponse' => '\ecmDHTML_GenerateNextGenMenuResponse',
      'ecmDHTML_DropNextGenMenu' => '\ecmDHTML_DropNextGenMenu',
      'ecmDHTML_DropNextGenMenuResponse' => '\ecmDHTML_DropNextGenMenuResponse',
      'ecmBusinessRulesByID' => '\ecmBusinessRulesByID',
      'ecmBusinessRulesByIDResponse' => '\ecmBusinessRulesByIDResponse',
      'ecmBusinessRulesByIdentifier' => '\ecmBusinessRulesByIdentifier',
      'ecmBusinessRulesByIdentifierResponse' => '\ecmBusinessRulesByIdentifierResponse',
      'ecmTrackVisit' => '\ecmTrackVisit',
      'ecmTrackVisitResponse' => '\ecmTrackVisitResponse',
      'ecmContentRatingDialog' => '\ecmContentRatingDialog',
      'ecmContentRatingDialogResponse' => '\ecmContentRatingDialogResponse',
      'ecmContentRatingResult' => '\ecmContentRatingResult',
      'ecmContentRatingResultResponse' => '\ecmContentRatingResultResponse',
      'ecmGetFolderBreadcrumb' => '\ecmGetFolderBreadcrumb',
      'ecmGetFolderBreadcrumbResponse' => '\ecmGetFolderBreadcrumbResponse',
      'ecmMetadataList' => '\ecmMetadataList',
      'ecmMetadataListResponse' => '\ecmMetadataListResponse',
      'ecmRssAggregator' => '\ecmRssAggregator',
      'ecmRssAggregatorResponse' => '\ecmRssAggregatorResponse',
      'GetGUID' => '\GetGUID',
      'GetGUIDResponse' => '\GetGUIDResponse');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = 'ContentWS.wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      
      parent::__construct($wsdl, $options);
    }

    /**
     * @param SendNotification $parameters
     * @access public
     * @return SendNotificationResponse
     */
    public function SendNotification(SendNotification $parameters)
    {
      return $this->__soapCall('SendNotification', array($parameters));
    }

    /**
     * @param Collection $parameters
     * @access public
     * @return CollectionResponse
     */
    public function Collection(Collection $parameters)
    {
      return $this->__soapCall('Collection', array($parameters));
    }

    /**
     * @param GetCollection $parameters
     * @access public
     * @return GetCollectionResponse
     */
    public function GetCollection(GetCollection $parameters)
    {
      return $this->__soapCall('GetCollection', array($parameters));
    }

    /**
     * @param ContentBlock $parameters
     * @access public
     * @return ContentBlockResponse
     */
    public function ContentBlock(ContentBlock $parameters)
    {
      return $this->__soapCall('ContentBlock', array($parameters));
    }

    /**
     * @param ContentBlockEx $parameters
     * @access public
     * @return ContentBlockExResponse
     */
    public function ContentBlockEx(ContentBlockEx $parameters)
    {
      return $this->__soapCall('ContentBlockEx', array($parameters));
    }

    /**
     * @param GetContentBlock $parameters
     * @access public
     * @return GetContentBlockResponse
     */
    public function GetContentBlock(GetContentBlock $parameters)
    {
      return $this->__soapCall('GetContentBlock', array($parameters));
    }

    /**
     * @param ListSummary $parameters
     * @access public
     * @return ListSummaryResponse
     */
    public function ListSummary(ListSummary $parameters)
    {
      return $this->__soapCall('ListSummary', array($parameters));
    }

    /**
     * @param ShowRandomSummary $parameters
     * @access public
     * @return ShowRandomSummaryResponse
     */
    public function ShowRandomSummary(ShowRandomSummary $parameters)
    {
      return $this->__soapCall('ShowRandomSummary', array($parameters));
    }

    /**
     * @param ShowRandomContent $parameters
     * @access public
     * @return ShowRandomContentResponse
     */
    public function ShowRandomContent(ShowRandomContent $parameters)
    {
      return $this->__soapCall('ShowRandomContent', array($parameters));
    }

    /**
     * @param SingleSummary $parameters
     * @access public
     * @return SingleSummaryResponse
     */
    public function SingleSummary(SingleSummary $parameters)
    {
      return $this->__soapCall('SingleSummary', array($parameters));
    }

    /**
     * @param MetaData $parameters
     * @access public
     * @return MetaDataResponse
     */
    public function MetaData(MetaData $parameters)
    {
      return $this->__soapCall('MetaData', array($parameters));
    }

    /**
     * @param AddTranslatedContent $parameters
     * @access public
     * @return AddTranslatedContentResponse
     */
    public function AddTranslatedContent(AddTranslatedContent $parameters)
    {
      return $this->__soapCall('AddTranslatedContent', array($parameters));
    }

    /**
     * @param AddContent $parameters
     * @access public
     * @return AddContentResponse
     */
    public function AddContent(AddContent $parameters)
    {
      return $this->__soapCall('AddContent', array($parameters));
    }

    /**
     * @param AddXMLContentWithTemplate $parameters
     * @access public
     * @return AddXMLContentWithTemplateResponse
     */
    public function AddXMLContentWithTemplate(AddXMLContentWithTemplate $parameters)
    {
      return $this->__soapCall('AddXMLContentWithTemplate', array($parameters));
    }

    /**
     * @param AddTranslatedXMLContent $parameters
     * @access public
     * @return AddTranslatedXMLContentResponse
     */
    public function AddTranslatedXMLContent(AddTranslatedXMLContent $parameters)
    {
      return $this->__soapCall('AddTranslatedXMLContent', array($parameters));
    }

    /**
     * @param GetListSummary $parameters
     * @access public
     * @return GetListSummaryResponse
     */
    public function GetListSummary(GetListSummary $parameters)
    {
      return $this->__soapCall('GetListSummary', array($parameters));
    }

    /**
     * @param GetListSummaryWithHTML $parameters
     * @access public
     * @return GetListSummaryWithHTMLResponse
     */
    public function GetListSummaryWithHTML(GetListSummaryWithHTML $parameters)
    {
      return $this->__soapCall('GetListSummaryWithHTML', array($parameters));
    }

    /**
     * @param GetSingleSummary $parameters
     * @access public
     * @return GetSingleSummaryResponse
     */
    public function GetSingleSummary(GetSingleSummary $parameters)
    {
      return $this->__soapCall('GetSingleSummary', array($parameters));
    }

    /**
     * @param SearchDisplay $parameters
     * @access public
     * @return SearchDisplayResponse
     */
    public function SearchDisplay(SearchDisplay $parameters)
    {
      return $this->__soapCall('SearchDisplay', array($parameters));
    }

    /**
     * @param GetSearchDisplay $parameters
     * @access public
     * @return GetSearchDisplayResponse
     */
    public function GetSearchDisplay(GetSearchDisplay $parameters)
    {
      return $this->__soapCall('GetSearchDisplay', array($parameters));
    }

    /**
     * @param GetFormBlock $parameters
     * @access public
     * @return GetFormBlockResponse
     */
    public function GetFormBlock(GetFormBlock $parameters)
    {
      return $this->__soapCall('GetFormBlock', array($parameters));
    }

    /**
     * @param GetEventsByCalendar $parameters
     * @access public
     * @return GetEventsByCalendarResponse
     */
    public function GetEventsByCalendar(GetEventsByCalendar $parameters)
    {
      return $this->__soapCall('GetEventsByCalendar', array($parameters));
    }

    /**
     * @param ecmCalendar $parameters
     * @access public
     * @return ecmCalendarResponse
     */
    public function ecmCalendar(ecmCalendar $parameters)
    {
      return $this->__soapCall('ecmCalendar', array($parameters));
    }

    /**
     * @param GenerateHTMLMenu $parameters
     * @access public
     * @return GenerateHTMLMenuResponse
     */
    public function GenerateHTMLMenu(GenerateHTMLMenu $parameters)
    {
      return $this->__soapCall('GenerateHTMLMenu', array($parameters));
    }

    /**
     * @param DropHTMLMenu $parameters
     * @access public
     * @return DropHTMLMenuResponse
     */
    public function DropHTMLMenu(DropHTMLMenu $parameters)
    {
      return $this->__soapCall('DropHTMLMenu', array($parameters));
    }

    /**
     * @param DoScheduledContent $parameters
     * @access public
     * @return DoScheduledContentResponse
     */
    public function DoScheduledContent(DoScheduledContent $parameters)
    {
      return $this->__soapCall('DoScheduledContent', array($parameters));
    }

    /**
     * @param DoRunJob $parameters
     * @access public
     * @return DoRunJobResponse
     */
    public function DoRunJob(DoRunJob $parameters)
    {
      return $this->__soapCall('DoRunJob', array($parameters));
    }

    /**
     * @param IndexContent $parameters
     * @access public
     * @return IndexContentResponse
     */
    public function IndexContent(IndexContent $parameters)
    {
      return $this->__soapCall('IndexContent', array($parameters));
    }

    /**
     * @param DoEndDateAction $parameters
     * @access public
     * @return DoEndDateActionResponse
     */
    public function DoEndDateAction(DoEndDateAction $parameters)
    {
      return $this->__soapCall('DoEndDateAction', array($parameters));
    }

    /**
     * @param ecmGetContentBlock $parameters
     * @access public
     * @return ecmGetContentBlockResponse
     */
    public function ecmGetContentBlock(ecmGetContentBlock $parameters)
    {
      return $this->__soapCall('ecmGetContentBlock', array($parameters));
    }

    /**
     * @param ecmContentBlockEx $parameters
     * @access public
     * @return ecmContentBlockExResponse
     */
    public function ecmContentBlockEx(ecmContentBlockEx $parameters)
    {
      return $this->__soapCall('ecmContentBlockEx', array($parameters));
    }

    /**
     * @param ecmFormBlock $parameters
     * @access public
     * @return ecmFormBlockResponse
     */
    public function ecmFormBlock(ecmFormBlock $parameters)
    {
      return $this->__soapCall('ecmFormBlock', array($parameters));
    }

    /**
     * @param ecmListSummary $parameters
     * @access public
     * @return ecmListSummaryResponse
     */
    public function ecmListSummary(ecmListSummary $parameters)
    {
      return $this->__soapCall('ecmListSummary', array($parameters));
    }

    /**
     * @param ecmSingleSummary $parameters
     * @access public
     * @return ecmSingleSummaryResponse
     */
    public function ecmSingleSummary(ecmSingleSummary $parameters)
    {
      return $this->__soapCall('ecmSingleSummary', array($parameters));
    }

    /**
     * @param ecmListSummaryXML $parameters
     * @access public
     * @return ecmListSummaryXMLResponse
     */
    public function ecmListSummaryXML(ecmListSummaryXML $parameters)
    {
      return $this->__soapCall('ecmListSummaryXML', array($parameters));
    }

    /**
     * @param ecmListSummaryXMLWithHtml $parameters
     * @access public
     * @return ecmListSummaryXMLWithHtmlResponse
     */
    public function ecmListSummaryXMLWithHtml(ecmListSummaryXMLWithHtml $parameters)
    {
      return $this->__soapCall('ecmListSummaryXMLWithHtml', array($parameters));
    }

    /**
     * @param ecmMetadata $parameters
     * @access public
     * @return ecmMetadataResponse
     */
    public function ecmMetadata(ecmMetadata $parameters)
    {
      return $this->__soapCall('ecmMetadata', array($parameters));
    }

    /**
     * @param ecmCollectionXML $parameters
     * @access public
     * @return ecmCollectionXMLResponse
     */
    public function ecmCollectionXML(ecmCollectionXML $parameters)
    {
      return $this->__soapCall('ecmCollectionXML', array($parameters));
    }

    /**
     * @param ecmCollection $parameters
     * @access public
     * @return ecmCollectionResponse
     */
    public function ecmCollection(ecmCollection $parameters)
    {
      return $this->__soapCall('ecmCollection', array($parameters));
    }

    /**
     * @param ecmShowRandomContent $parameters
     * @access public
     * @return ecmShowRandomContentResponse
     */
    public function ecmShowRandomContent(ecmShowRandomContent $parameters)
    {
      return $this->__soapCall('ecmShowRandomContent', array($parameters));
    }

    /**
     * @param ecmShowRandomSummary $parameters
     * @access public
     * @return ecmShowRandomSummaryResponse
     */
    public function ecmShowRandomSummary(ecmShowRandomSummary $parameters)
    {
      return $this->__soapCall('ecmShowRandomSummary', array($parameters));
    }

    /**
     * @param ecmRssCollection $parameters
     * @access public
     * @return ecmRssCollectionResponse
     */
    public function ecmRssCollection(ecmRssCollection $parameters)
    {
      return $this->__soapCall('ecmRssCollection', array($parameters));
    }

    /**
     * @param ecmRssSummary $parameters
     * @access public
     * @return ecmRssSummaryResponse
     */
    public function ecmRssSummary(ecmRssSummary $parameters)
    {
      return $this->__soapCall('ecmRssSummary', array($parameters));
    }

    /**
     * @param ecmEvtCalendar $parameters
     * @access public
     * @return ecmEvtCalendarResponse
     */
    public function ecmEvtCalendar(ecmEvtCalendar $parameters)
    {
      return $this->__soapCall('ecmEvtCalendar', array($parameters));
    }

    /**
     * @param ecmSearch $parameters
     * @access public
     * @return ecmSearchResponse
     */
    public function ecmSearch(ecmSearch $parameters)
    {
      return $this->__soapCall('ecmSearch', array($parameters));
    }

    /**
     * @param ecmSearchDisplay $parameters
     * @access public
     * @return ecmSearchDisplayResponse
     */
    public function ecmSearchDisplay(ecmSearchDisplay $parameters)
    {
      return $this->__soapCall('ecmSearchDisplay', array($parameters));
    }

    /**
     * [SearchText -> Text you would like the CMS to search.]<br/>
     *  [SearchType -> "And", "Or", "ExactPhrase"] <br/>
     *  [StartingFolder -> CMS will search in the specified folder] <br/>
     *  [Recursive -> Allow search in sub folders] <br/>
     *  [AllowFragments -> Match partial word] <br/>
     *  [MaxNumber -> 0 = Unlimited]
     *
     * @param ecmSearchDisplayXML $parameters
     * @access public
     * @return ecmSearchDisplayXMLResponse
     */
    public function ecmSearchDisplayXML(ecmSearchDisplayXML $parameters)
    {
      return $this->__soapCall('ecmSearchDisplayXML', array($parameters));
    }

    /**
     * @param ecmIndexSearchDisplay $parameters
     * @access public
     * @return ecmIndexSearchDisplayResponse
     */
    public function ecmIndexSearchDisplay(ecmIndexSearchDisplay $parameters)
    {
      return $this->__soapCall('ecmIndexSearchDisplay', array($parameters));
    }

    /**
     * @param ecmIndexSearch $parameters
     * @access public
     * @return ecmIndexSearchResponse
     */
    public function ecmIndexSearch(ecmIndexSearch $parameters)
    {
      return $this->__soapCall('ecmIndexSearch', array($parameters));
    }

    /**
     * @param ecmLogin $parameters
     * @access public
     * @return ecmLoginResponse
     */
    public function ecmLogin(ecmLogin $parameters)
    {
      return $this->__soapCall('ecmLogin', array($parameters));
    }

    /**
     * @param ecmLanguageSelect $parameters
     * @access public
     * @return ecmLanguageSelectResponse
     */
    public function ecmLanguageSelect(ecmLanguageSelect $parameters)
    {
      return $this->__soapCall('ecmLanguageSelect', array($parameters));
    }

    /**
     * @param ecmGetMenuXML $parameters
     * @access public
     * @return ecmGetMenuXMLResponse
     */
    public function ecmGetMenuXML(ecmGetMenuXML $parameters)
    {
      return $this->__soapCall('ecmGetMenuXML', array($parameters));
    }

    /**
     * @param ecmDHTML_GenerateNextGenMenu $parameters
     * @access public
     * @return ecmDHTML_GenerateNextGenMenuResponse
     */
    public function ecmDHTML_GenerateNextGenMenu(ecmDHTML_GenerateNextGenMenu $parameters)
    {
      return $this->__soapCall('ecmDHTML_GenerateNextGenMenu', array($parameters));
    }

    /**
     * @param ecmDHTML_DropNextGenMenu $parameters
     * @access public
     * @return ecmDHTML_DropNextGenMenuResponse
     */
    public function ecmDHTML_DropNextGenMenu(ecmDHTML_DropNextGenMenu $parameters)
    {
      return $this->__soapCall('ecmDHTML_DropNextGenMenu', array($parameters));
    }

    /**
     * @param ecmBusinessRulesByID $parameters
     * @access public
     * @return ecmBusinessRulesByIDResponse
     */
    public function ecmBusinessRulesByID(ecmBusinessRulesByID $parameters)
    {
      return $this->__soapCall('ecmBusinessRulesByID', array($parameters));
    }

    /**
     * @param ecmBusinessRulesByIdentifier $parameters
     * @access public
     * @return ecmBusinessRulesByIdentifierResponse
     */
    public function ecmBusinessRulesByIdentifier(ecmBusinessRulesByIdentifier $parameters)
    {
      return $this->__soapCall('ecmBusinessRulesByIdentifier', array($parameters));
    }

    /**
     * @param ecmTrackVisit $parameters
     * @access public
     * @return ecmTrackVisitResponse
     */
    public function ecmTrackVisit(ecmTrackVisit $parameters)
    {
      return $this->__soapCall('ecmTrackVisit', array($parameters));
    }

    /**
     * @param ecmContentRatingDialog $parameters
     * @access public
     * @return ecmContentRatingDialogResponse
     */
    public function ecmContentRatingDialog(ecmContentRatingDialog $parameters)
    {
      return $this->__soapCall('ecmContentRatingDialog', array($parameters));
    }

    /**
     * @param ecmContentRatingResult $parameters
     * @access public
     * @return ecmContentRatingResultResponse
     */
    public function ecmContentRatingResult(ecmContentRatingResult $parameters)
    {
      return $this->__soapCall('ecmContentRatingResult', array($parameters));
    }

    /**
     * contentId := 0 [None] or any valid content id<br/>folderId := 0 [None] or any valid folder id<br/>pathSeparator := Charecter that separators nodes in the path<br/>linkNodes := Make node hyperlinked<br/>linkTarget := _self, _top, _blank, _parent<br/>displayVertical := display node vertically <br/>pageUrl := page url from client request<br/>dynamicParameter := querystring used for dynamic id
     *
     * @param ecmGetFolderBreadcrumb $parameters
     * @access public
     * @return ecmGetFolderBreadcrumbResponse
     */
    public function ecmGetFolderBreadcrumb(ecmGetFolderBreadcrumb $parameters)
    {
      return $this->__soapCall('ecmGetFolderBreadcrumb', array($parameters));
    }

    /**
     * @param ecmMetadataList $parameters
     * @access public
     * @return ecmMetadataListResponse
     */
    public function ecmMetadataList(ecmMetadataList $parameters)
    {
      return $this->__soapCall('ecmMetadataList', array($parameters));
    }

    /**
     * @param ecmRssAggregator $parameters
     * @access public
     * @return ecmRssAggregatorResponse
     */
    public function ecmRssAggregator(ecmRssAggregator $parameters)
    {
      return $this->__soapCall('ecmRssAggregator', array($parameters));
    }

    /**
     * @param GetGUID $parameters
     * @access public
     * @return GetGUIDResponse
     */
    public function GetGUID(GetGUID $parameters)
    {
      return $this->__soapCall('GetGUID', array($parameters));
    }

}
