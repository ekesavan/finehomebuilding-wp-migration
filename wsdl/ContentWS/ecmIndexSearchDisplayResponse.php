<?php

class ecmIndexSearchDisplayResponse
{

    /**
     * @var string $ecmIndexSearchDisplayResult
     * @access public
     */
    public $ecmIndexSearchDisplayResult = null;

    /**
     * @param string $ecmIndexSearchDisplayResult
     * @access public
     */
    public function __construct($ecmIndexSearchDisplayResult)
    {
      $this->ecmIndexSearchDisplayResult = $ecmIndexSearchDisplayResult;
    }

}
