<?php

class Collection
{

    /**
     * @var int $CollectionId
     * @access public
     */
    public $CollectionId = null;

    /**
     * @var string $DisplayFormatter
     * @access public
     */
    public $DisplayFormatter = null;

    /**
     * @param int $CollectionId
     * @param string $DisplayFormatter
     * @access public
     */
    public function __construct($CollectionId, $DisplayFormatter)
    {
      $this->CollectionId = $CollectionId;
      $this->DisplayFormatter = $DisplayFormatter;
    }

}
