<?php

class ecmDHTML_DropNextGenMenuResponse
{

    /**
     * @var string $ecmDHTML_DropNextGenMenuResult
     * @access public
     */
    public $ecmDHTML_DropNextGenMenuResult = null;

    /**
     * @param string $ecmDHTML_DropNextGenMenuResult
     * @access public
     */
    public function __construct($ecmDHTML_DropNextGenMenuResult)
    {
      $this->ecmDHTML_DropNextGenMenuResult = $ecmDHTML_DropNextGenMenuResult;
    }

}
