<?php

class ecmTrackVisit
{

    /**
     * @var int $content_id
     * @access public
     */
    public $content_id = null;

    /**
     * @var string $visitor_id
     * @access public
     */
    public $visitor_id = null;

    /**
     * @var int $session_rank
     * @access public
     */
    public $session_rank = null;

    /**
     * @var string $referring_url
     * @access public
     */
    public $referring_url = null;

    /**
     * @var string $url
     * @access public
     */
    public $url = null;

    /**
     * @var int $UserID
     * @access public
     */
    public $UserID = null;

    /**
     * @var string $SitePath
     * @access public
     */
    public $SitePath = null;

    /**
     * @var int $Preview
     * @access public
     */
    public $Preview = null;

    /**
     * @var int $SiteLanguage
     * @access public
     */
    public $SiteLanguage = null;

    /**
     * @param int $content_id
     * @param string $visitor_id
     * @param int $session_rank
     * @param string $referring_url
     * @param string $url
     * @param int $UserID
     * @param string $SitePath
     * @param int $Preview
     * @param int $SiteLanguage
     * @access public
     */
    public function __construct($content_id, $visitor_id, $session_rank, $referring_url, $url, $UserID, $SitePath, $Preview, $SiteLanguage)
    {
      $this->content_id = $content_id;
      $this->visitor_id = $visitor_id;
      $this->session_rank = $session_rank;
      $this->referring_url = $referring_url;
      $this->url = $url;
      $this->UserID = $UserID;
      $this->SitePath = $SitePath;
      $this->Preview = $Preview;
      $this->SiteLanguage = $SiteLanguage;
    }

}
