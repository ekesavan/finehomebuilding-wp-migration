<?php

class GetFormBlockResponse
{

    /**
     * @var string $GetFormBlockResult
     * @access public
     */
    public $GetFormBlockResult = null;

    /**
     * @param string $GetFormBlockResult
     * @access public
     */
    public function __construct($GetFormBlockResult)
    {
      $this->GetFormBlockResult = $GetFormBlockResult;
    }

}
