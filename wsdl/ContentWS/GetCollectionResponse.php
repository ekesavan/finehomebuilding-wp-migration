<?php

class GetCollectionResponse
{

    /**
     * @var CollectionItemsResult $GetCollectionResult
     * @access public
     */
    public $GetCollectionResult = null;

    /**
     * @param CollectionItemsResult $GetCollectionResult
     * @access public
     */
    public function __construct($GetCollectionResult)
    {
      $this->GetCollectionResult = $GetCollectionResult;
    }

}
