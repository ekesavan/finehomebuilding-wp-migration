<?php

class ListSummaryResponse
{

    /**
     * @var string $ListSummaryResult
     * @access public
     */
    public $ListSummaryResult = null;

    /**
     * @param string $ListSummaryResult
     * @access public
     */
    public function __construct($ListSummaryResult)
    {
      $this->ListSummaryResult = $ListSummaryResult;
    }

}
