<?php

class ecmCollectionResponse
{

    /**
     * @var string $ecmCollectionResult
     * @access public
     */
    public $ecmCollectionResult = null;

    /**
     * @param string $ecmCollectionResult
     * @access public
     */
    public function __construct($ecmCollectionResult)
    {
      $this->ecmCollectionResult = $ecmCollectionResult;
    }

}
