<?php

class SingleSummary
{

    /**
     * @var int $ContentID
     * @access public
     */
    public $ContentID = null;

    /**
     * @var int $ShowSummary
     * @access public
     */
    public $ShowSummary = null;

    /**
     * @var string $StyleInfo
     * @access public
     */
    public $StyleInfo = null;

    /**
     * @var string $ShowInfo
     * @access public
     */
    public $ShowInfo = null;

    /**
     * @param int $ContentID
     * @param int $ShowSummary
     * @param string $StyleInfo
     * @param string $ShowInfo
     * @access public
     */
    public function __construct($ContentID, $ShowSummary, $StyleInfo, $ShowInfo)
    {
      $this->ContentID = $ContentID;
      $this->ShowSummary = $ShowSummary;
      $this->StyleInfo = $StyleInfo;
      $this->ShowInfo = $ShowInfo;
    }

}
