<?php

class ecmListSummaryXMLResponse
{

    /**
     * @var string $ecmListSummaryXMLResult
     * @access public
     */
    public $ecmListSummaryXMLResult = null;

    /**
     * @param string $ecmListSummaryXMLResult
     * @access public
     */
    public function __construct($ecmListSummaryXMLResult)
    {
      $this->ecmListSummaryXMLResult = $ecmListSummaryXMLResult;
    }

}
