<?php

class ecmRssSummaryResponse
{

    /**
     * @var string $ecmRssSummaryResult
     * @access public
     */
    public $ecmRssSummaryResult = null;

    /**
     * @param string $ecmRssSummaryResult
     * @access public
     */
    public function __construct($ecmRssSummaryResult)
    {
      $this->ecmRssSummaryResult = $ecmRssSummaryResult;
    }

}
