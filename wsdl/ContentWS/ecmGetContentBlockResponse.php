<?php

class ecmGetContentBlockResponse
{

    /**
     * @var string $ecmGetContentBlockResult
     * @access public
     */
    public $ecmGetContentBlockResult = null;

    /**
     * @param string $ecmGetContentBlockResult
     * @access public
     */
    public function __construct($ecmGetContentBlockResult)
    {
      $this->ecmGetContentBlockResult = $ecmGetContentBlockResult;
    }

}
