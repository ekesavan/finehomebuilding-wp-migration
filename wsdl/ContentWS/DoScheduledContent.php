<?php

class DoScheduledContent
{

    /**
     * @var Long[] $IDs
     * @access public
     */
    public $IDs = null;

    /**
     * @var Int[] $Langs
     * @access public
     */
    public $Langs = null;

    /**
     * @param Long[] $IDs
     * @param Int[] $Langs
     * @access public
     */
    public function __construct($IDs, $Langs)
    {
      $this->IDs = $IDs;
      $this->Langs = $Langs;
    }

}
