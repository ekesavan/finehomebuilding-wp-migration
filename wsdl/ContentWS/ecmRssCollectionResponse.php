<?php

class ecmRssCollectionResponse
{

    /**
     * @var string $ecmRssCollectionResult
     * @access public
     */
    public $ecmRssCollectionResult = null;

    /**
     * @param string $ecmRssCollectionResult
     * @access public
     */
    public function __construct($ecmRssCollectionResult)
    {
      $this->ecmRssCollectionResult = $ecmRssCollectionResult;
    }

}
