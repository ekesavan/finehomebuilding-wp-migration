<?php

class ecmContentRatingResultResponse
{

    /**
     * @var string $ecmContentRatingResultResult
     * @access public
     */
    public $ecmContentRatingResultResult = null;

    /**
     * @param string $ecmContentRatingResultResult
     * @access public
     */
    public function __construct($ecmContentRatingResultResult)
    {
      $this->ecmContentRatingResultResult = $ecmContentRatingResultResult;
    }

}
