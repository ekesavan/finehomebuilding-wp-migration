<?php

include_once('Result.php');

class TeaserResult extends Result
{

    /**
     * @var ContentBase[] $Item
     * @access public
     */
    public $Item = null;

    /**
     * @param int $ErrorCode
     * @param string $ErrorMessage
     * @param string $InfoMessage
     * @param int $Count
     * @param ContentBase[] $Item
     * @access public
     */
    public function __construct($ErrorCode, $ErrorMessage, $InfoMessage, $Count, $Item)
    {
      parent::__construct($ErrorCode, $ErrorMessage, $InfoMessage, $Count);
      $this->Item = $Item;
    }

}
