<?php

class ShowRandomSummary
{

    /**
     * @var int $CollectionID
     * @access public
     */
    public $CollectionID = null;

    /**
     * @param int $CollectionID
     * @access public
     */
    public function __construct($CollectionID)
    {
      $this->CollectionID = $CollectionID;
    }

}
