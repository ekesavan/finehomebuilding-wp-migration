<?php

class ecmRssAggregator
{

    /**
     * @var string $Url
     * @access public
     */
    public $Url = null;

    /**
     * @var string $LinkTarget
     * @access public
     */
    public $LinkTarget = null;

    /**
     * @var int $MaxResults
     * @access public
     */
    public $MaxResults = null;

    /**
     * @var string $DisplayType
     * @access public
     */
    public $DisplayType = null;

    /**
     * @var int $UserID
     * @access public
     */
    public $UserID = null;

    /**
     * @var string $SitePath
     * @access public
     */
    public $SitePath = null;

    /**
     * @var int $Preview
     * @access public
     */
    public $Preview = null;

    /**
     * @var int $SiteLanguage
     * @access public
     */
    public $SiteLanguage = null;

    /**
     * @param string $Url
     * @param string $LinkTarget
     * @param int $MaxResults
     * @param string $DisplayType
     * @param int $UserID
     * @param string $SitePath
     * @param int $Preview
     * @param int $SiteLanguage
     * @access public
     */
    public function __construct($Url, $LinkTarget, $MaxResults, $DisplayType, $UserID, $SitePath, $Preview, $SiteLanguage)
    {
      $this->Url = $Url;
      $this->LinkTarget = $LinkTarget;
      $this->MaxResults = $MaxResults;
      $this->DisplayType = $DisplayType;
      $this->UserID = $UserID;
      $this->SitePath = $SitePath;
      $this->Preview = $Preview;
      $this->SiteLanguage = $SiteLanguage;
    }

}
