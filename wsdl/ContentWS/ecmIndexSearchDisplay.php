<?php

class ecmIndexSearchDisplay
{

    /**
     * @var int $XmlConfigID
     * @access public
     */
    public $XmlConfigID = null;

    /**
     * @var boolean $WeightedResults
     * @access public
     */
    public $WeightedResults = null;

    /**
     * @var int $folderID
     * @access public
     */
    public $folderID = null;

    /**
     * @var boolean $recursive
     * @access public
     */
    public $recursive = null;

    /**
     * @var boolean $ShowSummary
     * @access public
     */
    public $ShowSummary = null;

    /**
     * @var string $orderby
     * @access public
     */
    public $orderby = null;

    /**
     * @var string $order
     * @access public
     */
    public $order = null;

    /**
     * @var string $RequestXML
     * @access public
     */
    public $RequestXML = null;

    /**
     * @var string $label_NoResults
     * @access public
     */
    public $label_NoResults = null;

    /**
     * @var string $label_hit
     * @access public
     */
    public $label_hit = null;

    /**
     * @var int $UserID
     * @access public
     */
    public $UserID = null;

    /**
     * @var string $SitePath
     * @access public
     */
    public $SitePath = null;

    /**
     * @var int $Preview
     * @access public
     */
    public $Preview = null;

    /**
     * @var int $SiteLanguage
     * @access public
     */
    public $SiteLanguage = null;

    /**
     * @param int $XmlConfigID
     * @param boolean $WeightedResults
     * @param int $folderID
     * @param boolean $recursive
     * @param boolean $ShowSummary
     * @param string $orderby
     * @param string $order
     * @param string $RequestXML
     * @param string $label_NoResults
     * @param string $label_hit
     * @param int $UserID
     * @param string $SitePath
     * @param int $Preview
     * @param int $SiteLanguage
     * @access public
     */
    public function __construct($XmlConfigID, $WeightedResults, $folderID, $recursive, $ShowSummary, $orderby, $order, $RequestXML, $label_NoResults, $label_hit, $UserID, $SitePath, $Preview, $SiteLanguage)
    {
      $this->XmlConfigID = $XmlConfigID;
      $this->WeightedResults = $WeightedResults;
      $this->folderID = $folderID;
      $this->recursive = $recursive;
      $this->ShowSummary = $ShowSummary;
      $this->orderby = $orderby;
      $this->order = $order;
      $this->RequestXML = $RequestXML;
      $this->label_NoResults = $label_NoResults;
      $this->label_hit = $label_hit;
      $this->UserID = $UserID;
      $this->SitePath = $SitePath;
      $this->Preview = $Preview;
      $this->SiteLanguage = $SiteLanguage;
    }

}
