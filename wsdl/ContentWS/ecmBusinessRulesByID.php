<?php

class ecmBusinessRulesByID
{

    /**
     * @var string $httpHost
     * @access public
     */
    public $httpHost = null;

    /**
     * @var string $urlRest
     * @access public
     */
    public $urlRest = null;

    /**
     * @var string $cookies
     * @access public
     */
    public $cookies = null;

    /**
     * @var string $formValues
     * @access public
     */
    public $formValues = null;

    /**
     * @var string $queryValues
     * @access public
     */
    public $queryValues = null;

    /**
     * @var string $serverValues
     * @access public
     */
    public $serverValues = null;

    /**
     * @var string $paramValues
     * @access public
     */
    public $paramValues = null;

    /**
     * @var int $RuleID
     * @access public
     */
    public $RuleID = null;

    /**
     * @var int $UserID
     * @access public
     */
    public $UserID = null;

    /**
     * @var string $SitePath
     * @access public
     */
    public $SitePath = null;

    /**
     * @var int $Preview
     * @access public
     */
    public $Preview = null;

    /**
     * @var int $SiteLanguage
     * @access public
     */
    public $SiteLanguage = null;

    /**
     * @param string $httpHost
     * @param string $urlRest
     * @param string $cookies
     * @param string $formValues
     * @param string $queryValues
     * @param string $serverValues
     * @param string $paramValues
     * @param int $RuleID
     * @param int $UserID
     * @param string $SitePath
     * @param int $Preview
     * @param int $SiteLanguage
     * @access public
     */
    public function __construct($httpHost, $urlRest, $cookies, $formValues, $queryValues, $serverValues, $paramValues, $RuleID, $UserID, $SitePath, $Preview, $SiteLanguage)
    {
      $this->httpHost = $httpHost;
      $this->urlRest = $urlRest;
      $this->cookies = $cookies;
      $this->formValues = $formValues;
      $this->queryValues = $queryValues;
      $this->serverValues = $serverValues;
      $this->paramValues = $paramValues;
      $this->RuleID = $RuleID;
      $this->UserID = $UserID;
      $this->SitePath = $SitePath;
      $this->Preview = $Preview;
      $this->SiteLanguage = $SiteLanguage;
    }

}
