<?php

class ecmSingleSummaryResponse
{

    /**
     * @var string $ecmSingleSummaryResult
     * @access public
     */
    public $ecmSingleSummaryResult = null;

    /**
     * @param string $ecmSingleSummaryResult
     * @access public
     */
    public function __construct($ecmSingleSummaryResult)
    {
      $this->ecmSingleSummaryResult = $ecmSingleSummaryResult;
    }

}
