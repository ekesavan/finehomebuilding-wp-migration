<?php

class ecmContentRatingResult
{

    /**
     * @var int $contentID
     * @access public
     */
    public $contentID = null;

    /**
     * @var string $visitorID
     * @access public
     */
    public $visitorID = null;

    /**
     * @var int $padding
     * @access public
     */
    public $padding = null;

    /**
     * @var string $bgColorName
     * @access public
     */
    public $bgColorName = null;

    /**
     * @var string $barColorName
     * @access public
     */
    public $barColorName = null;

    /**
     * @var string $textColorName
     * @access public
     */
    public $textColorName = null;

    /**
     * @var string $ratingCompleteGraphName
     * @access public
     */
    public $ratingCompleteGraphName = null;

    /**
     * @var string $thankYouMessage
     * @access public
     */
    public $thankYouMessage = null;

    /**
     * @var string $alreadyRatedMessage
     * @access public
     */
    public $alreadyRatedMessage = null;

    /**
     * @var string $RatingLevelLabel
     * @access public
     */
    public $RatingLevelLabel = null;

    /**
     * @var string $TotalRatingsLabel
     * @access public
     */
    public $TotalRatingsLabel = null;

    /**
     * @var int $ratingValue
     * @access public
     */
    public $ratingValue = null;

    /**
     * @var string $ratingReason
     * @access public
     */
    public $ratingReason = null;

    /**
     * @var int $UserID
     * @access public
     */
    public $UserID = null;

    /**
     * @var string $SitePath
     * @access public
     */
    public $SitePath = null;

    /**
     * @var int $Preview
     * @access public
     */
    public $Preview = null;

    /**
     * @var int $SiteLanguage
     * @access public
     */
    public $SiteLanguage = null;

    /**
     * @param int $contentID
     * @param string $visitorID
     * @param int $padding
     * @param string $bgColorName
     * @param string $barColorName
     * @param string $textColorName
     * @param string $ratingCompleteGraphName
     * @param string $thankYouMessage
     * @param string $alreadyRatedMessage
     * @param string $RatingLevelLabel
     * @param string $TotalRatingsLabel
     * @param int $ratingValue
     * @param string $ratingReason
     * @param int $UserID
     * @param string $SitePath
     * @param int $Preview
     * @param int $SiteLanguage
     * @access public
     */
    public function __construct($contentID, $visitorID, $padding, $bgColorName, $barColorName, $textColorName, $ratingCompleteGraphName, $thankYouMessage, $alreadyRatedMessage, $RatingLevelLabel, $TotalRatingsLabel, $ratingValue, $ratingReason, $UserID, $SitePath, $Preview, $SiteLanguage)
    {
      $this->contentID = $contentID;
      $this->visitorID = $visitorID;
      $this->padding = $padding;
      $this->bgColorName = $bgColorName;
      $this->barColorName = $barColorName;
      $this->textColorName = $textColorName;
      $this->ratingCompleteGraphName = $ratingCompleteGraphName;
      $this->thankYouMessage = $thankYouMessage;
      $this->alreadyRatedMessage = $alreadyRatedMessage;
      $this->RatingLevelLabel = $RatingLevelLabel;
      $this->TotalRatingsLabel = $TotalRatingsLabel;
      $this->ratingValue = $ratingValue;
      $this->ratingReason = $ratingReason;
      $this->UserID = $UserID;
      $this->SitePath = $SitePath;
      $this->Preview = $Preview;
      $this->SiteLanguage = $SiteLanguage;
    }

}
