<?php

class ecmEvtCalendarResponse
{

    /**
     * @var string $ecmEvtCalendarResult
     * @access public
     */
    public $ecmEvtCalendarResult = null;

    /**
     * @param string $ecmEvtCalendarResult
     * @access public
     */
    public function __construct($ecmEvtCalendarResult)
    {
      $this->ecmEvtCalendarResult = $ecmEvtCalendarResult;
    }

}
