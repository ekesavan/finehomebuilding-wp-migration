<?php

class ecmContentRatingDialog
{

    /**
     * @var int $ContentID
     * @access public
     */
    public $ContentID = null;

    /**
     * @var string $VisitorID
     * @access public
     */
    public $VisitorID = null;

    /**
     * @var int $padding
     * @access public
     */
    public $padding = null;

    /**
     * @var string $InitialGraph
     * @access public
     */
    public $InitialGraph = null;

    /**
     * @var string $BackgroundColor
     * @access public
     */
    public $BackgroundColor = null;

    /**
     * @var string $BarColor
     * @access public
     */
    public $BarColor = null;

    /**
     * @var string $TextColor
     * @access public
     */
    public $TextColor = null;

    /**
     * @var string $FeedbackHeader
     * @access public
     */
    public $FeedbackHeader = null;

    /**
     * @var string $BadLabel
     * @access public
     */
    public $BadLabel = null;

    /**
     * @var string $GoodLabel
     * @access public
     */
    public $GoodLabel = null;

    /**
     * @var string $UserCommentsHeader
     * @access public
     */
    public $UserCommentsHeader = null;

    /**
     * @var string $CharsRemainingLabel
     * @access public
     */
    public $CharsRemainingLabel = null;

    /**
     * @var string $RatingLevelLabel
     * @access public
     */
    public $RatingLevelLabel = null;

    /**
     * @var string $TotalRatingsLabel
     * @access public
     */
    public $TotalRatingsLabel = null;

    /**
     * @var string $AlreadyRatedMessage
     * @access public
     */
    public $AlreadyRatedMessage = null;

    /**
     * @var string $ValidationMessage
     * @access public
     */
    public $ValidationMessage = null;

    /**
     * @var string $OptionalReasonLabel
     * @access public
     */
    public $OptionalReasonLabel = null;

    /**
     * @var string $SubmitLabel
     * @access public
     */
    public $SubmitLabel = null;

    /**
     * @var int $UserID
     * @access public
     */
    public $UserID = null;

    /**
     * @var string $SitePath
     * @access public
     */
    public $SitePath = null;

    /**
     * @var int $Preview
     * @access public
     */
    public $Preview = null;

    /**
     * @var int $SiteLanguage
     * @access public
     */
    public $SiteLanguage = null;

    /**
     * @param int $ContentID
     * @param string $VisitorID
     * @param int $padding
     * @param string $InitialGraph
     * @param string $BackgroundColor
     * @param string $BarColor
     * @param string $TextColor
     * @param string $FeedbackHeader
     * @param string $BadLabel
     * @param string $GoodLabel
     * @param string $UserCommentsHeader
     * @param string $CharsRemainingLabel
     * @param string $RatingLevelLabel
     * @param string $TotalRatingsLabel
     * @param string $AlreadyRatedMessage
     * @param string $ValidationMessage
     * @param string $OptionalReasonLabel
     * @param string $SubmitLabel
     * @param int $UserID
     * @param string $SitePath
     * @param int $Preview
     * @param int $SiteLanguage
     * @access public
     */
    public function __construct($ContentID, $VisitorID, $padding, $InitialGraph, $BackgroundColor, $BarColor, $TextColor, $FeedbackHeader, $BadLabel, $GoodLabel, $UserCommentsHeader, $CharsRemainingLabel, $RatingLevelLabel, $TotalRatingsLabel, $AlreadyRatedMessage, $ValidationMessage, $OptionalReasonLabel, $SubmitLabel, $UserID, $SitePath, $Preview, $SiteLanguage)
    {
      $this->ContentID = $ContentID;
      $this->VisitorID = $VisitorID;
      $this->padding = $padding;
      $this->InitialGraph = $InitialGraph;
      $this->BackgroundColor = $BackgroundColor;
      $this->BarColor = $BarColor;
      $this->TextColor = $TextColor;
      $this->FeedbackHeader = $FeedbackHeader;
      $this->BadLabel = $BadLabel;
      $this->GoodLabel = $GoodLabel;
      $this->UserCommentsHeader = $UserCommentsHeader;
      $this->CharsRemainingLabel = $CharsRemainingLabel;
      $this->RatingLevelLabel = $RatingLevelLabel;
      $this->TotalRatingsLabel = $TotalRatingsLabel;
      $this->AlreadyRatedMessage = $AlreadyRatedMessage;
      $this->ValidationMessage = $ValidationMessage;
      $this->OptionalReasonLabel = $OptionalReasonLabel;
      $this->SubmitLabel = $SubmitLabel;
      $this->UserID = $UserID;
      $this->SitePath = $SitePath;
      $this->Preview = $Preview;
      $this->SiteLanguage = $SiteLanguage;
    }

}
