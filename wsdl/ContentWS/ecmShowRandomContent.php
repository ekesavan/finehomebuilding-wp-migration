<?php

class ecmShowRandomContent
{

    /**
     * @var int $CollectionID
     * @access public
     */
    public $CollectionID = null;

    /**
     * @var int $XsltId
     * @access public
     */
    public $XsltId = null;

    /**
     * @var int $UserID
     * @access public
     */
    public $UserID = null;

    /**
     * @var string $SitePath
     * @access public
     */
    public $SitePath = null;

    /**
     * @var int $Preview
     * @access public
     */
    public $Preview = null;

    /**
     * @var int $SiteLanguage
     * @access public
     */
    public $SiteLanguage = null;

    /**
     * @param int $CollectionID
     * @param int $XsltId
     * @param int $UserID
     * @param string $SitePath
     * @param int $Preview
     * @param int $SiteLanguage
     * @access public
     */
    public function __construct($CollectionID, $XsltId, $UserID, $SitePath, $Preview, $SiteLanguage)
    {
      $this->CollectionID = $CollectionID;
      $this->XsltId = $XsltId;
      $this->UserID = $UserID;
      $this->SitePath = $SitePath;
      $this->Preview = $Preview;
      $this->SiteLanguage = $SiteLanguage;
    }

}
