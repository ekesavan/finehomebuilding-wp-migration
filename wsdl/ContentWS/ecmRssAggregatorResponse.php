<?php

class ecmRssAggregatorResponse
{

    /**
     * @var string $ecmRssAggregatorResult
     * @access public
     */
    public $ecmRssAggregatorResult = null;

    /**
     * @param string $ecmRssAggregatorResult
     * @access public
     */
    public function __construct($ecmRssAggregatorResult)
    {
      $this->ecmRssAggregatorResult = $ecmRssAggregatorResult;
    }

}
