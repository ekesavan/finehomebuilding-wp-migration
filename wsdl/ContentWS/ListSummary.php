<?php

class ListSummary
{

    /**
     * @var string $Folder
     * @access public
     */
    public $Folder = null;

    /**
     * @var int $Recursive
     * @access public
     */
    public $Recursive = null;

    /**
     * @var int $ShowSummary
     * @access public
     */
    public $ShowSummary = null;

    /**
     * @var string $StyleInfo
     * @access public
     */
    public $StyleInfo = null;

    /**
     * @var string $OrderBy
     * @access public
     */
    public $OrderBy = null;

    /**
     * @var string $ShowInfo
     * @access public
     */
    public $ShowInfo = null;

    /**
     * @var int $MaxNumber
     * @access public
     */
    public $MaxNumber = null;

    /**
     * @var string $ObjType
     * @access public
     */
    public $ObjType = null;

    /**
     * @var string $OptionList
     * @access public
     */
    public $OptionList = null;

    /**
     * @param string $Folder
     * @param int $Recursive
     * @param int $ShowSummary
     * @param string $StyleInfo
     * @param string $OrderBy
     * @param string $ShowInfo
     * @param int $MaxNumber
     * @param string $ObjType
     * @param string $OptionList
     * @access public
     */
    public function __construct($Folder, $Recursive, $ShowSummary, $StyleInfo, $OrderBy, $ShowInfo, $MaxNumber, $ObjType, $OptionList)
    {
      $this->Folder = $Folder;
      $this->Recursive = $Recursive;
      $this->ShowSummary = $ShowSummary;
      $this->StyleInfo = $StyleInfo;
      $this->OrderBy = $OrderBy;
      $this->ShowInfo = $ShowInfo;
      $this->MaxNumber = $MaxNumber;
      $this->ObjType = $ObjType;
      $this->OptionList = $OptionList;
    }

}
