<?php

class ecmIndexSearch
{

    /**
     * @var int $XmlConfigID
     * @access public
     */
    public $XmlConfigID = null;

    /**
     * @var int $folderID
     * @access public
     */
    public $folderID = null;

    /**
     * @var boolean $recursive
     * @access public
     */
    public $recursive = null;

    /**
     * @var string $formname
     * @access public
     */
    public $formname = null;

    /**
     * @var int $UserID
     * @access public
     */
    public $UserID = null;

    /**
     * @var string $SitePath
     * @access public
     */
    public $SitePath = null;

    /**
     * @var int $Preview
     * @access public
     */
    public $Preview = null;

    /**
     * @var int $SiteLanguage
     * @access public
     */
    public $SiteLanguage = null;

    /**
     * @param int $XmlConfigID
     * @param int $folderID
     * @param boolean $recursive
     * @param string $formname
     * @param int $UserID
     * @param string $SitePath
     * @param int $Preview
     * @param int $SiteLanguage
     * @access public
     */
    public function __construct($XmlConfigID, $folderID, $recursive, $formname, $UserID, $SitePath, $Preview, $SiteLanguage)
    {
      $this->XmlConfigID = $XmlConfigID;
      $this->folderID = $folderID;
      $this->recursive = $recursive;
      $this->formname = $formname;
      $this->UserID = $UserID;
      $this->SitePath = $SitePath;
      $this->Preview = $Preview;
      $this->SiteLanguage = $SiteLanguage;
    }

}
