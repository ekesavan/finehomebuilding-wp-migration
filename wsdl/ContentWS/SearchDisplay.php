<?php

class SearchDisplay
{

    /**
     * @var string $SearchText
     * @access public
     */
    public $SearchText = null;

    /**
     * @var string $SearchType
     * @access public
     */
    public $SearchType = null;

    /**
     * @var int $Recursive
     * @access public
     */
    public $Recursive = null;

    /**
     * @var string $StartingFolder
     * @access public
     */
    public $StartingFolder = null;

    /**
     * @var int $AllowFragments
     * @access public
     */
    public $AllowFragments = null;

    /**
     * @var int $MaxNumber
     * @access public
     */
    public $MaxNumber = null;

    /**
     * @var string $StyleInfo
     * @access public
     */
    public $StyleInfo = null;

    /**
     * @var int $ShowDate
     * @access public
     */
    public $ShowDate = null;

    /**
     * @param string $SearchText
     * @param string $SearchType
     * @param int $Recursive
     * @param string $StartingFolder
     * @param int $AllowFragments
     * @param int $MaxNumber
     * @param string $StyleInfo
     * @param int $ShowDate
     * @access public
     */
    public function __construct($SearchText, $SearchType, $Recursive, $StartingFolder, $AllowFragments, $MaxNumber, $StyleInfo, $ShowDate)
    {
      $this->SearchText = $SearchText;
      $this->SearchType = $SearchType;
      $this->Recursive = $Recursive;
      $this->StartingFolder = $StartingFolder;
      $this->AllowFragments = $AllowFragments;
      $this->MaxNumber = $MaxNumber;
      $this->StyleInfo = $StyleInfo;
      $this->ShowDate = $ShowDate;
    }

}
