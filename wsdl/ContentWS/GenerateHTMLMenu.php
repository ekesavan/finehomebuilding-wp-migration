<?php

class GenerateHTMLMenu
{

    /**
     * @var string $MenuIds
     * @access public
     */
    public $MenuIds = null;

    /**
     * @param string $MenuIds
     * @access public
     */
    public function __construct($MenuIds)
    {
      $this->MenuIds = $MenuIds;
    }

}
