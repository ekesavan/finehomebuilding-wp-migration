<?php

class SendNotification
{

    /**
     * @var PdfStatusInfo $data
     * @access public
     */
    public $data = null;

    /**
     * @param PdfStatusInfo $data
     * @access public
     */
    public function __construct($data)
    {
      $this->data = $data;
    }

}
