<?php

class ecmLanguageSelectResponse
{

    /**
     * @var string $ecmLanguageSelectResult
     * @access public
     */
    public $ecmLanguageSelectResult = null;

    /**
     * @param string $ecmLanguageSelectResult
     * @access public
     */
    public function __construct($ecmLanguageSelectResult)
    {
      $this->ecmLanguageSelectResult = $ecmLanguageSelectResult;
    }

}
