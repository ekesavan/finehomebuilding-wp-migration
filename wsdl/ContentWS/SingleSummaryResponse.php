<?php

class SingleSummaryResponse
{

    /**
     * @var string $SingleSummaryResult
     * @access public
     */
    public $SingleSummaryResult = null;

    /**
     * @param string $SingleSummaryResult
     * @access public
     */
    public function __construct($SingleSummaryResult)
    {
      $this->SingleSummaryResult = $SingleSummaryResult;
    }

}
