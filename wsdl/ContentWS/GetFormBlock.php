<?php

class GetFormBlock
{

    /**
     * @var int $FormId
     * @access public
     */
    public $FormId = null;

    /**
     * @var string $FormData
     * @access public
     */
    public $FormData = null;

    /**
     * @var boolean $FormTagReqd
     * @access public
     */
    public $FormTagReqd = null;

    /**
     * @param int $FormId
     * @param string $FormData
     * @param boolean $FormTagReqd
     * @access public
     */
    public function __construct($FormId, $FormData, $FormTagReqd)
    {
      $this->FormId = $FormId;
      $this->FormData = $FormData;
      $this->FormTagReqd = $FormTagReqd;
    }

}
