<?php

class ShowRandomSummaryResponse
{

    /**
     * @var string $ShowRandomSummaryResult
     * @access public
     */
    public $ShowRandomSummaryResult = null;

    /**
     * @param string $ShowRandomSummaryResult
     * @access public
     */
    public function __construct($ShowRandomSummaryResult)
    {
      $this->ShowRandomSummaryResult = $ShowRandomSummaryResult;
    }

}
