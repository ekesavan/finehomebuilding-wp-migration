<?php

include_once('Result.php');

class ShowContentResult extends Result
{

    /**
     * @var ShowContentBlock $Item
     * @access public
     */
    public $Item = null;

    /**
     * @param int $ErrorCode
     * @param string $ErrorMessage
     * @param string $InfoMessage
     * @param int $Count
     * @param ShowContentBlock $Item
     * @access public
     */
    public function __construct($ErrorCode, $ErrorMessage, $InfoMessage, $Count, $Item)
    {
      parent::__construct($ErrorCode, $ErrorMessage, $InfoMessage, $Count);
      $this->Item = $Item;
    }

}
