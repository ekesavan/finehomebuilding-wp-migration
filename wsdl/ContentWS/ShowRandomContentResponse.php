<?php

class ShowRandomContentResponse
{

    /**
     * @var string $ShowRandomContentResult
     * @access public
     */
    public $ShowRandomContentResult = null;

    /**
     * @param string $ShowRandomContentResult
     * @access public
     */
    public function __construct($ShowRandomContentResult)
    {
      $this->ShowRandomContentResult = $ShowRandomContentResult;
    }

}
