<?php

class ecmTrackVisitResponse
{

    /**
     * @var string $ecmTrackVisitResult
     * @access public
     */
    public $ecmTrackVisitResult = null;

    /**
     * @param string $ecmTrackVisitResult
     * @access public
     */
    public function __construct($ecmTrackVisitResult)
    {
      $this->ecmTrackVisitResult = $ecmTrackVisitResult;
    }

}
