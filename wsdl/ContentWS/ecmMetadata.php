<?php

class ecmMetadata
{

    /**
     * @var string $ItemList
     * @access public
     */
    public $ItemList = null;

    /**
     * @var anyType $Spare1
     * @access public
     */
    public $Spare1 = null;

    /**
     * @var int $Preview
     * @access public
     */
    public $Preview = null;

    /**
     * @var int $SiteLanguage
     * @access public
     */
    public $SiteLanguage = null;

    /**
     * @param string $ItemList
     * @param anyType $Spare1
     * @param int $Preview
     * @param int $SiteLanguage
     * @access public
     */
    public function __construct($ItemList, $Spare1, $Preview, $SiteLanguage)
    {
      $this->ItemList = $ItemList;
      $this->Spare1 = $Spare1;
      $this->Preview = $Preview;
      $this->SiteLanguage = $SiteLanguage;
    }

}
