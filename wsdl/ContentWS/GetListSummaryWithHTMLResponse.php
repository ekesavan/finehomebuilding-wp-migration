<?php

class GetListSummaryWithHTMLResponse
{

    /**
     * @var TeaserResult $GetListSummaryWithHTMLResult
     * @access public
     */
    public $GetListSummaryWithHTMLResult = null;

    /**
     * @param TeaserResult $GetListSummaryWithHTMLResult
     * @access public
     */
    public function __construct($GetListSummaryWithHTMLResult)
    {
      $this->GetListSummaryWithHTMLResult = $GetListSummaryWithHTMLResult;
    }

}
