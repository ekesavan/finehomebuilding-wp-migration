<?php

class GetEventsByCalendarResponse
{

    /**
     * @var string $GetEventsByCalendarResult
     * @access public
     */
    public $GetEventsByCalendarResult = null;

    /**
     * @param string $GetEventsByCalendarResult
     * @access public
     */
    public function __construct($GetEventsByCalendarResult)
    {
      $this->GetEventsByCalendarResult = $GetEventsByCalendarResult;
    }

}
