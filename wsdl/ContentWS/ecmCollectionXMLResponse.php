<?php

class ecmCollectionXMLResponse
{

    /**
     * @var string $ecmCollectionXMLResult
     * @access public
     */
    public $ecmCollectionXMLResult = null;

    /**
     * @param string $ecmCollectionXMLResult
     * @access public
     */
    public function __construct($ecmCollectionXMLResult)
    {
      $this->ecmCollectionXMLResult = $ecmCollectionXMLResult;
    }

}
