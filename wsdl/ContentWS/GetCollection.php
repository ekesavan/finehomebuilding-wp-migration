<?php

class GetCollection
{

    /**
     * @var int $CollectionId
     * @access public
     */
    public $CollectionId = null;

    /**
     * @var int $GetHTML
     * @access public
     */
    public $GetHTML = null;

    /**
     * @param int $CollectionId
     * @param int $GetHTML
     * @access public
     */
    public function __construct($CollectionId, $GetHTML)
    {
      $this->CollectionId = $CollectionId;
      $this->GetHTML = $GetHTML;
    }

}
