<?php

class ecmSingleSummary
{

    /**
     * @var int $ContentID
     * @access public
     */
    public $ContentID = null;

    /**
     * @var boolean $ShowSummary
     * @access public
     */
    public $ShowSummary = null;

    /**
     * @var string $StyleInfo
     * @access public
     */
    public $StyleInfo = null;

    /**
     * @var string $ShowInfo
     * @access public
     */
    public $ShowInfo = null;

    /**
     * @var string $Spare1
     * @access public
     */
    public $Spare1 = null;

    /**
     * @var int $UserID
     * @access public
     */
    public $UserID = null;

    /**
     * @var string $SitePath
     * @access public
     */
    public $SitePath = null;

    /**
     * @var int $Preview
     * @access public
     */
    public $Preview = null;

    /**
     * @var int $SiteLanguage
     * @access public
     */
    public $SiteLanguage = null;

    /**
     * @param int $ContentID
     * @param boolean $ShowSummary
     * @param string $StyleInfo
     * @param string $ShowInfo
     * @param string $Spare1
     * @param int $UserID
     * @param string $SitePath
     * @param int $Preview
     * @param int $SiteLanguage
     * @access public
     */
    public function __construct($ContentID, $ShowSummary, $StyleInfo, $ShowInfo, $Spare1, $UserID, $SitePath, $Preview, $SiteLanguage)
    {
      $this->ContentID = $ContentID;
      $this->ShowSummary = $ShowSummary;
      $this->StyleInfo = $StyleInfo;
      $this->ShowInfo = $ShowInfo;
      $this->Spare1 = $Spare1;
      $this->UserID = $UserID;
      $this->SitePath = $SitePath;
      $this->Preview = $Preview;
      $this->SiteLanguage = $SiteLanguage;
    }

}
