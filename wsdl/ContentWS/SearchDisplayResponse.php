<?php

class SearchDisplayResponse
{

    /**
     * @var string $SearchDisplayResult
     * @access public
     */
    public $SearchDisplayResult = null;

    /**
     * @param string $SearchDisplayResult
     * @access public
     */
    public function __construct($SearchDisplayResult)
    {
      $this->SearchDisplayResult = $SearchDisplayResult;
    }

}
