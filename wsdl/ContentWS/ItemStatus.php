<?php

class ItemStatus
{
    const __default = 'All';
    const All = 'All';
    const PendingStart = 'PendingStart';
    const PendingExpiration = 'PendingExpiration';
    const Approved = 'Approved';
    const Expired = 'Expired';
    const Submitted = 'Submitted';
    const CheckedOut = 'CheckedOut';
    const CheckedIn = 'CheckedIn';
    const PendingDelete = 'PendingDelete';


}
