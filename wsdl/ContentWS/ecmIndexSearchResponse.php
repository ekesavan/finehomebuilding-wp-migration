<?php

class ecmIndexSearchResponse
{

    /**
     * @var string $ecmIndexSearchResult
     * @access public
     */
    public $ecmIndexSearchResult = null;

    /**
     * @param string $ecmIndexSearchResult
     * @access public
     */
    public function __construct($ecmIndexSearchResult)
    {
      $this->ecmIndexSearchResult = $ecmIndexSearchResult;
    }

}
