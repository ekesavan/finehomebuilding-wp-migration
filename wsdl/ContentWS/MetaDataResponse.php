<?php

class MetaDataResponse
{

    /**
     * @var string $MetaDataResult
     * @access public
     */
    public $MetaDataResult = null;

    /**
     * @param string $MetaDataResult
     * @access public
     */
    public function __construct($MetaDataResult)
    {
      $this->MetaDataResult = $MetaDataResult;
    }

}
