<?php

class ContentBlockResponse
{

    /**
     * @var string $ContentBlockResult
     * @access public
     */
    public $ContentBlockResult = null;

    /**
     * @param string $ContentBlockResult
     * @access public
     */
    public function __construct($ContentBlockResult)
    {
      $this->ContentBlockResult = $ContentBlockResult;
    }

}
