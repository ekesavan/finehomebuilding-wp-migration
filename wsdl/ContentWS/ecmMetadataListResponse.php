<?php

class ecmMetadataListResponse
{

    /**
     * @var string $ecmMetadataListResult
     * @access public
     */
    public $ecmMetadataListResult = null;

    /**
     * @param string $ecmMetadataListResult
     * @access public
     */
    public function __construct($ecmMetadataListResult)
    {
      $this->ecmMetadataListResult = $ecmMetadataListResult;
    }

}
