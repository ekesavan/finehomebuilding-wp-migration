<?php

class GetListSummaryResponse
{

    /**
     * @var TeaserResult $GetListSummaryResult
     * @access public
     */
    public $GetListSummaryResult = null;

    /**
     * @param TeaserResult $GetListSummaryResult
     * @access public
     */
    public function __construct($GetListSummaryResult)
    {
      $this->GetListSummaryResult = $GetListSummaryResult;
    }

}
