<?php

class ecmRssSummary
{

    /**
     * @var string $Folder
     * @access public
     */
    public $Folder = null;

    /**
     * @var boolean $Recursive
     * @access public
     */
    public $Recursive = null;

    /**
     * @var string $OrderBy
     * @access public
     */
    public $OrderBy = null;

    /**
     * @var int $MaxNumber
     * @access public
     */
    public $MaxNumber = null;

    /**
     * @var string $SummaryType
     * @access public
     */
    public $SummaryType = null;

    /**
     * @var int $UserID
     * @access public
     */
    public $UserID = null;

    /**
     * @var string $SitePath
     * @access public
     */
    public $SitePath = null;

    /**
     * @var int $Preview
     * @access public
     */
    public $Preview = null;

    /**
     * @var int $SiteLanguage
     * @access public
     */
    public $SiteLanguage = null;

    /**
     * @param string $Folder
     * @param boolean $Recursive
     * @param string $OrderBy
     * @param int $MaxNumber
     * @param string $SummaryType
     * @param int $UserID
     * @param string $SitePath
     * @param int $Preview
     * @param int $SiteLanguage
     * @access public
     */
    public function __construct($Folder, $Recursive, $OrderBy, $MaxNumber, $SummaryType, $UserID, $SitePath, $Preview, $SiteLanguage)
    {
      $this->Folder = $Folder;
      $this->Recursive = $Recursive;
      $this->OrderBy = $OrderBy;
      $this->MaxNumber = $MaxNumber;
      $this->SummaryType = $SummaryType;
      $this->UserID = $UserID;
      $this->SitePath = $SitePath;
      $this->Preview = $Preview;
      $this->SiteLanguage = $SiteLanguage;
    }

}
