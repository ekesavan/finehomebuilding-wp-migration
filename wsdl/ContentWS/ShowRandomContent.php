<?php

class ShowRandomContent
{

    /**
     * @var int $CollectionId
     * @access public
     */
    public $CollectionId = null;

    /**
     * @var int $xsltid
     * @access public
     */
    public $xsltid = null;

    /**
     * @param int $CollectionId
     * @param int $xsltid
     * @access public
     */
    public function __construct($CollectionId, $xsltid)
    {
      $this->CollectionId = $CollectionId;
      $this->xsltid = $xsltid;
    }

}
