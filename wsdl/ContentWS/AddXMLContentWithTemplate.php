<?php

class AddXMLContentWithTemplate
{

    /**
     * @var string $UserName
     * @access public
     */
    public $UserName = null;

    /**
     * @var string $Password
     * @access public
     */
    public $Password = null;

    /**
     * @var string $Domain
     * @access public
     */
    public $Domain = null;

    /**
     * @var string $content_title
     * @access public
     */
    public $content_title = null;

    /**
     * @var string $content_comment
     * @access public
     */
    public $content_comment = null;

    /**
     * @var string $ContentHtml
     * @access public
     */
    public $ContentHtml = null;

    /**
     * @var string $SummaryHTML
     * @access public
     */
    public $SummaryHTML = null;

    /**
     * @var string $ContentLanguage
     * @access public
     */
    public $ContentLanguage = null;

    /**
     * @var string $FolderID
     * @access public
     */
    public $FolderID = null;

    /**
     * @var string $GoLive
     * @access public
     */
    public $GoLive = null;

    /**
     * @var string $EndDate
     * @access public
     */
    public $EndDate = null;

    /**
     * @var string $MetaInfo
     * @access public
     */
    public $MetaInfo = null;

    /**
     * @var string $ErrString
     * @access public
     */
    public $ErrString = null;

    /**
     * @var int $XmlID
     * @access public
     */
    public $XmlID = null;

    /**
     * @var int $TemplateID
     * @access public
     */
    public $TemplateID = null;

    /**
     * @param string $UserName
     * @param string $Password
     * @param string $Domain
     * @param string $content_title
     * @param string $content_comment
     * @param string $ContentHtml
     * @param string $SummaryHTML
     * @param string $ContentLanguage
     * @param string $FolderID
     * @param string $GoLive
     * @param string $EndDate
     * @param string $MetaInfo
     * @param string $ErrString
     * @param int $XmlID
     * @param int $TemplateID
     * @access public
     */
    public function __construct($UserName, $Password, $Domain, $content_title, $content_comment, $ContentHtml, $SummaryHTML, $ContentLanguage, $FolderID, $GoLive, $EndDate, $MetaInfo, $ErrString, $XmlID, $TemplateID)
    {
      $this->UserName = $UserName;
      $this->Password = $Password;
      $this->Domain = $Domain;
      $this->content_title = $content_title;
      $this->content_comment = $content_comment;
      $this->ContentHtml = $ContentHtml;
      $this->SummaryHTML = $SummaryHTML;
      $this->ContentLanguage = $ContentLanguage;
      $this->FolderID = $FolderID;
      $this->GoLive = $GoLive;
      $this->EndDate = $EndDate;
      $this->MetaInfo = $MetaInfo;
      $this->ErrString = $ErrString;
      $this->XmlID = $XmlID;
      $this->TemplateID = $TemplateID;
    }

}
