<?php

class GetContentBlockResponse
{

    /**
     * @var ShowContentResult $GetContentBlockResult
     * @access public
     */
    public $GetContentBlockResult = null;

    /**
     * @param ShowContentResult $GetContentBlockResult
     * @access public
     */
    public function __construct($GetContentBlockResult)
    {
      $this->GetContentBlockResult = $GetContentBlockResult;
    }

}
