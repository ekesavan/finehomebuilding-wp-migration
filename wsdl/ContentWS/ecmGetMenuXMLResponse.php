<?php

class ecmGetMenuXMLResponse
{

    /**
     * @var string $ecmGetMenuXMLResult
     * @access public
     */
    public $ecmGetMenuXMLResult = null;

    /**
     * @param string $ecmGetMenuXMLResult
     * @access public
     */
    public function __construct($ecmGetMenuXMLResult)
    {
      $this->ecmGetMenuXMLResult = $ecmGetMenuXMLResult;
    }

}
