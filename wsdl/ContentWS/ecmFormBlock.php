<?php

class ecmFormBlock
{

    /**
     * @var int $Formid
     * @access public
     */
    public $Formid = null;

    /**
     * @var boolean $FormTagReqd
     * @access public
     */
    public $FormTagReqd = null;

    /**
     * @var string $SearchTerms
     * @access public
     */
    public $SearchTerms = null;

    /**
     * @var string $SearchType
     * @access public
     */
    public $SearchType = null;

    /**
     * @var string $SearchFragment
     * @access public
     */
    public $SearchFragment = null;

    /**
     * @var int $UserID
     * @access public
     */
    public $UserID = null;

    /**
     * @var string $SitePath
     * @access public
     */
    public $SitePath = null;

    /**
     * @var int $Preview
     * @access public
     */
    public $Preview = null;

    /**
     * @var int $SiteLanguage
     * @access public
     */
    public $SiteLanguage = null;

    /**
     * @var string $FormFields
     * @access public
     */
    public $FormFields = null;

    /**
     * @param int $Formid
     * @param boolean $FormTagReqd
     * @param string $SearchTerms
     * @param string $SearchType
     * @param string $SearchFragment
     * @param int $UserID
     * @param string $SitePath
     * @param int $Preview
     * @param int $SiteLanguage
     * @param string $FormFields
     * @access public
     */
    public function __construct($Formid, $FormTagReqd, $SearchTerms, $SearchType, $SearchFragment, $UserID, $SitePath, $Preview, $SiteLanguage, $FormFields)
    {
      $this->Formid = $Formid;
      $this->FormTagReqd = $FormTagReqd;
      $this->SearchTerms = $SearchTerms;
      $this->SearchType = $SearchType;
      $this->SearchFragment = $SearchFragment;
      $this->UserID = $UserID;
      $this->SitePath = $SitePath;
      $this->Preview = $Preview;
      $this->SiteLanguage = $SiteLanguage;
      $this->FormFields = $FormFields;
    }

}
