<?php

class GetGUIDResponse
{

    /**
     * @var string $GetGUIDResult
     * @access public
     */
    public $GetGUIDResult = null;

    /**
     * @param string $GetGUIDResult
     * @access public
     */
    public function __construct($GetGUIDResult)
    {
      $this->GetGUIDResult = $GetGUIDResult;
    }

}
