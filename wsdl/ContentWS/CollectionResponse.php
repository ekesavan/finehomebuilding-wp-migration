<?php

class CollectionResponse
{

    /**
     * @var string $CollectionResult
     * @access public
     */
    public $CollectionResult = null;

    /**
     * @param string $CollectionResult
     * @access public
     */
    public function __construct($CollectionResult)
    {
      $this->CollectionResult = $CollectionResult;
    }

}
