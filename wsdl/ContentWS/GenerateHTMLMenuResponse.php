<?php

class GenerateHTMLMenuResponse
{

    /**
     * @var string $GenerateHTMLMenuResult
     * @access public
     */
    public $GenerateHTMLMenuResult = null;

    /**
     * @param string $GenerateHTMLMenuResult
     * @access public
     */
    public function __construct($GenerateHTMLMenuResult)
    {
      $this->GenerateHTMLMenuResult = $GenerateHTMLMenuResult;
    }

}
