<?php

class ecmEvtCalendar
{

    /**
     * @var int $CalendarID
     * @access public
     */
    public $CalendarID = null;

    /**
     * @var string $DisplayType
     * @access public
     */
    public $DisplayType = null;

    /**
     * @var string $StartDate
     * @access public
     */
    public $StartDate = null;

    /**
     * @var string $EndDate
     * @access public
     */
    public $EndDate = null;

    /**
     * @var int $UserID
     * @access public
     */
    public $UserID = null;

    /**
     * @var string $SitePath
     * @access public
     */
    public $SitePath = null;

    /**
     * @var int $Preview
     * @access public
     */
    public $Preview = null;

    /**
     * @var int $SiteLanguage
     * @access public
     */
    public $SiteLanguage = null;

    /**
     * @param int $CalendarID
     * @param string $DisplayType
     * @param string $StartDate
     * @param string $EndDate
     * @param int $UserID
     * @param string $SitePath
     * @param int $Preview
     * @param int $SiteLanguage
     * @access public
     */
    public function __construct($CalendarID, $DisplayType, $StartDate, $EndDate, $UserID, $SitePath, $Preview, $SiteLanguage)
    {
      $this->CalendarID = $CalendarID;
      $this->DisplayType = $DisplayType;
      $this->StartDate = $StartDate;
      $this->EndDate = $EndDate;
      $this->UserID = $UserID;
      $this->SitePath = $SitePath;
      $this->Preview = $Preview;
      $this->SiteLanguage = $SiteLanguage;
    }

}
