<?php

class ecmMetadataList
{

    /**
     * @var int $FolderId
     * @access public
     */
    public $FolderId = null;

    /**
     * @var boolean $Recursive
     * @access public
     */
    public $Recursive = null;

    /**
     * @var string $KeyWordName
     * @access public
     */
    public $KeyWordName = null;

    /**
     * @var string $KeyWordValue
     * @access public
     */
    public $KeyWordValue = null;

    /**
     * @var boolean $ExactPhrase
     * @access public
     */
    public $ExactPhrase = null;

    /**
     * @var boolean $GetHtml
     * @access public
     */
    public $GetHtml = null;

    /**
     * @var int $MaxNumber
     * @access public
     */
    public $MaxNumber = null;

    /**
     * @var string $OrderBy
     * @access public
     */
    public $OrderBy = null;

    /**
     * @var string $SortOrder
     * @access public
     */
    public $SortOrder = null;

    /**
     * @var string $ContentType
     * @access public
     */
    public $ContentType = null;

    /**
     * @var string $LinkTarget
     * @access public
     */
    public $LinkTarget = null;

    /**
     * @var boolean $ShowSummary
     * @access public
     */
    public $ShowSummary = null;

    /**
     * @var string $StyleInfo
     * @access public
     */
    public $StyleInfo = null;

    /**
     * @var string $ShowInfo
     * @access public
     */
    public $ShowInfo = null;

    /**
     * @var int $UserID
     * @access public
     */
    public $UserID = null;

    /**
     * @var string $SitePath
     * @access public
     */
    public $SitePath = null;

    /**
     * @var int $Preview
     * @access public
     */
    public $Preview = null;

    /**
     * @var int $SiteLanguage
     * @access public
     */
    public $SiteLanguage = null;

    /**
     * @param int $FolderId
     * @param boolean $Recursive
     * @param string $KeyWordName
     * @param string $KeyWordValue
     * @param boolean $ExactPhrase
     * @param boolean $GetHtml
     * @param int $MaxNumber
     * @param string $OrderBy
     * @param string $SortOrder
     * @param string $ContentType
     * @param string $LinkTarget
     * @param boolean $ShowSummary
     * @param string $StyleInfo
     * @param string $ShowInfo
     * @param int $UserID
     * @param string $SitePath
     * @param int $Preview
     * @param int $SiteLanguage
     * @access public
     */
    public function __construct($FolderId, $Recursive, $KeyWordName, $KeyWordValue, $ExactPhrase, $GetHtml, $MaxNumber, $OrderBy, $SortOrder, $ContentType, $LinkTarget, $ShowSummary, $StyleInfo, $ShowInfo, $UserID, $SitePath, $Preview, $SiteLanguage)
    {
      $this->FolderId = $FolderId;
      $this->Recursive = $Recursive;
      $this->KeyWordName = $KeyWordName;
      $this->KeyWordValue = $KeyWordValue;
      $this->ExactPhrase = $ExactPhrase;
      $this->GetHtml = $GetHtml;
      $this->MaxNumber = $MaxNumber;
      $this->OrderBy = $OrderBy;
      $this->SortOrder = $SortOrder;
      $this->ContentType = $ContentType;
      $this->LinkTarget = $LinkTarget;
      $this->ShowSummary = $ShowSummary;
      $this->StyleInfo = $StyleInfo;
      $this->ShowInfo = $ShowInfo;
      $this->UserID = $UserID;
      $this->SitePath = $SitePath;
      $this->Preview = $Preview;
      $this->SiteLanguage = $SiteLanguage;
    }

}
