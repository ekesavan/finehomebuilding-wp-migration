<?php

class SearchContentItem
{

    /**
     * @var AssetData $AssetInfo
     * @access public
     */
    public $AssetInfo = null;

    /**
     * @var int $ID
     * @access public
     */
    public $ID = null;

    /**
     * @var string $ContentText
     * @access public
     */
    public $ContentText = null;

    /**
     * @var string $ItemStatus
     * @access public
     */
    public $ItemStatus = null;

    /**
     * @var int $LanguageID
     * @access public
     */
    public $LanguageID = null;

    /**
     * @var string $Title
     * @access public
     */
    public $Title = null;

    /**
     * @var dateTime $DateCreated
     * @access public
     */
    public $DateCreated = null;

    /**
     * @var string $DisplayDateCreated
     * @access public
     */
    public $DisplayDateCreated = null;

    /**
     * @var string $LastEditorLname
     * @access public
     */
    public $LastEditorLname = null;

    /**
     * @var string $LastEditorFname
     * @access public
     */
    public $LastEditorFname = null;

    /**
     * @var dateTime $DateModified
     * @access public
     */
    public $DateModified = null;

    /**
     * @var string $DisplayDateModified
     * @access public
     */
    public $DisplayDateModified = null;

    /**
     * @var string $TemplateFileName
     * @access public
     */
    public $TemplateFileName = null;

    /**
     * @var string $QuickLink
     * @access public
     */
    public $QuickLink = null;

    /**
     * @var string $Teaser
     * @access public
     */
    public $Teaser = null;

    /**
     * @var int $FolderID
     * @access public
     */
    public $FolderID = null;

    /**
     * @var string $FolderPath
     * @access public
     */
    public $FolderPath = null;

    /**
     * @var string $GoLiveDate
     * @access public
     */
    public $GoLiveDate = null;

    /**
     * @var string $EndDate
     * @access public
     */
    public $EndDate = null;

    /**
     * @var string $Size
     * @access public
     */
    public $Size = null;

    /**
     * @var string $Rank
     * @access public
     */
    public $Rank = null;

    /**
     * @var boolean $PrivateContent
     * @access public
     */
    public $PrivateContent = null;

    /**
     * @var CMSContentType $ContentType
     * @access public
     */
    public $ContentType = null;

    /**
     * @var int $LibraryID
     * @access public
     */
    public $LibraryID = null;

    /**
     * @var int $LibraryParentID
     * @access public
     */
    public $LibraryParentID = null;

    /**
     * @var int $LibraryTypeID
     * @access public
     */
    public $LibraryTypeID = null;

    /**
     * @var string $LibraryFileName
     * @access public
     */
    public $LibraryFileName = null;

    /**
     * @var string $LibraryThumbnail
     * @access public
     */
    public $LibraryThumbnail = null;

    /**
     * @var string $LibraryTitle
     * @access public
     */
    public $LibraryTitle = null;

    /**
     * @var PermissionData $Permissions
     * @access public
     */
    public $Permissions = null;

    /**
     * @var string $AssetVersion
     * @access public
     */
    public $AssetVersion = null;

    /**
     * @var string $Image
     * @access public
     */
    public $Image = null;

    /**
     * @var string $ImageThumbnail
     * @access public
     */
    public $ImageThumbnail = null;

    /**
     * @param AssetData $AssetInfo
     * @param int $ID
     * @param string $ContentText
     * @param string $ItemStatus
     * @param int $LanguageID
     * @param string $Title
     * @param string $DisplayDateCreated
     * @param string $LastEditorLname
     * @param string $LastEditorFname
     * @param string $DisplayDateModified
     * @param string $TemplateFileName
     * @param string $QuickLink
     * @param string $Teaser
     * @param int $FolderID
     * @param string $FolderPath
     * @param string $GoLiveDate
     * @param string $EndDate
     * @param string $Size
     * @param string $Rank
     * @param boolean $PrivateContent
     * @param CMSContentType $ContentType
     * @param int $LibraryID
     * @param int $LibraryParentID
     * @param int $LibraryTypeID
     * @param string $LibraryFileName
     * @param string $LibraryThumbnail
     * @param string $LibraryTitle
     * @param PermissionData $Permissions
     * @param string $AssetVersion
     * @param string $Image
     * @param string $ImageThumbnail
     * @access public
     */
    public function __construct($AssetInfo, $ID, $ContentText, $ItemStatus, $LanguageID, $Title, $DisplayDateCreated, $LastEditorLname, $LastEditorFname, $DisplayDateModified, $TemplateFileName, $QuickLink, $Teaser, $FolderID, $FolderPath, $GoLiveDate, $EndDate, $Size, $Rank, $PrivateContent, $ContentType, $LibraryID, $LibraryParentID, $LibraryTypeID, $LibraryFileName, $LibraryThumbnail, $LibraryTitle, $Permissions, $AssetVersion, $Image, $ImageThumbnail)
    {
      $this->AssetInfo = $AssetInfo;
      $this->ID = $ID;
      $this->ContentText = $ContentText;
      $this->ItemStatus = $ItemStatus;
      $this->LanguageID = $LanguageID;
      $this->Title = $Title;
      $this->DisplayDateCreated = $DisplayDateCreated;
      $this->LastEditorLname = $LastEditorLname;
      $this->LastEditorFname = $LastEditorFname;
      $this->DisplayDateModified = $DisplayDateModified;
      $this->TemplateFileName = $TemplateFileName;
      $this->QuickLink = $QuickLink;
      $this->Teaser = $Teaser;
      $this->FolderID = $FolderID;
      $this->FolderPath = $FolderPath;
      $this->GoLiveDate = $GoLiveDate;
      $this->EndDate = $EndDate;
      $this->Size = $Size;
      $this->Rank = $Rank;
      $this->PrivateContent = $PrivateContent;
      $this->ContentType = $ContentType;
      $this->LibraryID = $LibraryID;
      $this->LibraryParentID = $LibraryParentID;
      $this->LibraryTypeID = $LibraryTypeID;
      $this->LibraryFileName = $LibraryFileName;
      $this->LibraryThumbnail = $LibraryThumbnail;
      $this->LibraryTitle = $LibraryTitle;
      $this->Permissions = $Permissions;
      $this->AssetVersion = $AssetVersion;
      $this->Image = $Image;
      $this->ImageThumbnail = $ImageThumbnail;
    }

}
