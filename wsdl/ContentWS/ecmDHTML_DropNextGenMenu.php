<?php

class ecmDHTML_DropNextGenMenu
{

    /**
     * @var int $MenuId
     * @access public
     */
    public $MenuId = null;

    /**
     * @var string $Title
     * @access public
     */
    public $Title = null;

    /**
     * @var int $UserID
     * @access public
     */
    public $UserID = null;

    /**
     * @var string $SitePath
     * @access public
     */
    public $SitePath = null;

    /**
     * @var int $Preview
     * @access public
     */
    public $Preview = null;

    /**
     * @var int $SiteLanguage
     * @access public
     */
    public $SiteLanguage = null;

    /**
     * @param int $MenuId
     * @param string $Title
     * @param int $UserID
     * @param string $SitePath
     * @param int $Preview
     * @param int $SiteLanguage
     * @access public
     */
    public function __construct($MenuId, $Title, $UserID, $SitePath, $Preview, $SiteLanguage)
    {
      $this->MenuId = $MenuId;
      $this->Title = $Title;
      $this->UserID = $UserID;
      $this->SitePath = $SitePath;
      $this->Preview = $Preview;
      $this->SiteLanguage = $SiteLanguage;
    }

}
