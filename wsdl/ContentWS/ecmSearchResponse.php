<?php

class ecmSearchResponse
{

    /**
     * @var string $ecmSearchResult
     * @access public
     */
    public $ecmSearchResult = null;

    /**
     * @param string $ecmSearchResult
     * @access public
     */
    public function __construct($ecmSearchResult)
    {
      $this->ecmSearchResult = $ecmSearchResult;
    }

}
