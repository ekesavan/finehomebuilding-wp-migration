<?php

class ecmLoginResponse
{

    /**
     * @var string $ecmLoginResult
     * @access public
     */
    public $ecmLoginResult = null;

    /**
     * @param string $ecmLoginResult
     * @access public
     */
    public function __construct($ecmLoginResult)
    {
      $this->ecmLoginResult = $ecmLoginResult;
    }

}
