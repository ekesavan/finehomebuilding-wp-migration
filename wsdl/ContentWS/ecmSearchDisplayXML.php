<?php

class ecmSearchDisplayXML
{

    /**
     * @var string $SearchText
     * @access public
     */
    public $SearchText = null;

    /**
     * @var string $SearchType
     * @access public
     */
    public $SearchType = null;

    /**
     * @var int $Recursive
     * @access public
     */
    public $Recursive = null;

    /**
     * @var string $StartingFolder
     * @access public
     */
    public $StartingFolder = null;

    /**
     * @var int $AllowFragments
     * @access public
     */
    public $AllowFragments = null;

    /**
     * @var int $MaxNumber
     * @access public
     */
    public $MaxNumber = null;

    /**
     * @var int $UserID
     * @access public
     */
    public $UserID = null;

    /**
     * @var string $SitePath
     * @access public
     */
    public $SitePath = null;

    /**
     * @var int $Preview
     * @access public
     */
    public $Preview = null;

    /**
     * @var int $SiteLanguage
     * @access public
     */
    public $SiteLanguage = null;

    /**
     * @param string $SearchText
     * @param string $SearchType
     * @param int $Recursive
     * @param string $StartingFolder
     * @param int $AllowFragments
     * @param int $MaxNumber
     * @param int $UserID
     * @param string $SitePath
     * @param int $Preview
     * @param int $SiteLanguage
     * @access public
     */
    public function __construct($SearchText, $SearchType, $Recursive, $StartingFolder, $AllowFragments, $MaxNumber, $UserID, $SitePath, $Preview, $SiteLanguage)
    {
      $this->SearchText = $SearchText;
      $this->SearchType = $SearchType;
      $this->Recursive = $Recursive;
      $this->StartingFolder = $StartingFolder;
      $this->AllowFragments = $AllowFragments;
      $this->MaxNumber = $MaxNumber;
      $this->UserID = $UserID;
      $this->SitePath = $SitePath;
      $this->Preview = $Preview;
      $this->SiteLanguage = $SiteLanguage;
    }

}
