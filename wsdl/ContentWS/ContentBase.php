<?php

class ContentBase
{

    /**
     * @var string $Title
     * @access public
     */
    public $Title = null;

    /**
     * @var int $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var int $Language
     * @access public
     */
    public $Language = null;

    /**
     * @var string $Html
     * @access public
     */
    public $Html = null;

    /**
     * @var int $FolderId
     * @access public
     */
    public $FolderId = null;

    /**
     * @var boolean $IsPrivate
     * @access public
     */
    public $IsPrivate = null;

    /**
     * @var dateTime $GoLiveDate
     * @access public
     */
    public $GoLiveDate = null;

    /**
     * @var dateTime $StartDate
     * @access public
     */
    public $StartDate = null;

    /**
     * @var string $DisplayStartDate
     * @access public
     */
    public $DisplayStartDate = null;

    /**
     * @var dateTime $EndDate
     * @access public
     */
    public $EndDate = null;

    /**
     * @var string $DisplayGoLiveDate
     * @access public
     */
    public $DisplayGoLiveDate = null;

    /**
     * @var string $DisplayEndDate
     * @access public
     */
    public $DisplayEndDate = null;

    /**
     * @var CMSEndDateAction $EndDateAction
     * @access public
     */
    public $EndDateAction = null;

    /**
     * @var string $ContentStatus
     * @access public
     */
    public $ContentStatus = null;

    /**
     * @var string $Xslt1
     * @access public
     */
    public $Xslt1 = null;

    /**
     * @var string $Xslt2
     * @access public
     */
    public $Xslt2 = null;

    /**
     * @var string $Xslt3
     * @access public
     */
    public $Xslt3 = null;

    /**
     * @var string $Xslt4
     * @access public
     */
    public $Xslt4 = null;

    /**
     * @var string $Xslt5
     * @access public
     */
    public $Xslt5 = null;

    /**
     * @var int $DefaultXslt
     * @access public
     */
    public $DefaultXslt = null;

    /**
     * @var string $PackageDisplayXslt
     * @access public
     */
    public $PackageDisplayXslt = null;

    /**
     * @var CMSContentType $ContentType
     * @access public
     */
    public $ContentType = null;

    /**
     * @var CMSContentSubtype $ContentSubType
     * @access public
     */
    public $ContentSubType = null;

    /**
     * @var int $ExternalTypeId
     * @access public
     */
    public $ExternalTypeId = null;

    /**
     * @var dateTime $DateCreated
     * @access public
     */
    public $DateCreated = null;

    /**
     * @var string $DisplayDateCreated
     * @access public
     */
    public $DisplayDateCreated = null;

    /**
     * @var dateTime $DateModified
     * @access public
     */
    public $DateModified = null;

    /**
     * @var string $DisplayDateModified
     * @access public
     */
    public $DisplayDateModified = null;

    /**
     * @var string $LastEditorFname
     * @access public
     */
    public $LastEditorFname = null;

    /**
     * @var string $LastEditorLname
     * @access public
     */
    public $LastEditorLname = null;

    /**
     * @var int $UserId
     * @access public
     */
    public $UserId = null;

    /**
     * @var boolean $IsInherited
     * @access public
     */
    public $IsInherited = null;

    /**
     * @var int $InheritedFrom
     * @access public
     */
    public $InheritedFrom = null;

    /**
     * @var string $TemplateLink
     * @access public
     */
    public $TemplateLink = null;

    /**
     * @var string $QuickLink
     * @access public
     */
    public $QuickLink = null;

    /**
     * @var string $HyperLink
     * @access public
     */
    public $HyperLink = null;

    /**
     * @var string $Teaser
     * @access public
     */
    public $Teaser = null;

    /**
     * @var ItemStatus $Status
     * @access public
     */
    public $Status = null;

    /**
     * @var string $Comment
     * @access public
     */
    public $Comment = null;

    /**
     * @var string $LanguageDescription
     * @access public
     */
    public $LanguageDescription = null;

    /**
     * @var AssetData $AssetInfo
     * @access public
     */
    public $AssetInfo = null;

    /**
     * @var string $StagingDomain
     * @access public
     */
    public $StagingDomain = null;

    /**
     * @var string $ProductionDomain
     * @access public
     */
    public $ProductionDomain = null;

    /**
     * @var int $XMLCollectionID
     * @access public
     */
    public $XMLCollectionID = null;

    /**
     * @var string $FieldList
     * @access public
     */
    public $FieldList = null;

    /**
     * @var string $Image
     * @access public
     */
    public $Image = null;

    /**
     * @var string $ImageThumbnail
     * @access public
     */
    public $ImageThumbnail = null;

    /**
     * @var boolean $IsPublished
     * @access public
     */
    public $IsPublished = null;

    /**
     * @var AnalyticsData $AnalyticsInfo
     * @access public
     */
    public $AnalyticsInfo = null;

    /**
     * @param string $Title
     * @param int $Id
     * @param int $Language
     * @param string $Html
     * @param int $FolderId
     * @param boolean $IsPrivate
     * @param dateTime $GoLiveDate
     * @param dateTime $StartDate
     * @param string $DisplayStartDate
     * @param dateTime $EndDate
     * @param string $DisplayGoLiveDate
     * @param string $DisplayEndDate
     * @param CMSEndDateAction $EndDateAction
     * @param string $ContentStatus
     * @param string $Xslt1
     * @param string $Xslt2
     * @param string $Xslt3
     * @param string $Xslt4
     * @param string $Xslt5
     * @param int $DefaultXslt
     * @param string $PackageDisplayXslt
     * @param CMSContentType $ContentType
     * @param CMSContentSubtype $ContentSubType
     * @param int $ExternalTypeId
     * @param dateTime $DateCreated
     * @param string $DisplayDateCreated
     * @param dateTime $DateModified
     * @param string $DisplayDateModified
     * @param string $LastEditorFname
     * @param string $LastEditorLname
     * @param int $UserId
     * @param boolean $IsInherited
     * @param int $InheritedFrom
     * @param string $TemplateLink
     * @param string $QuickLink
     * @param string $HyperLink
     * @param string $Teaser
     * @param ItemStatus $Status
     * @param string $Comment
     * @param string $LanguageDescription
     * @param AssetData $AssetInfo
     * @param string $StagingDomain
     * @param string $ProductionDomain
     * @param int $XMLCollectionID
     * @param string $FieldList
     * @param string $Image
     * @param string $ImageThumbnail
     * @param boolean $IsPublished
     * @param AnalyticsData $AnalyticsInfo
     * @access public
     */
    public function __construct($Title, $Id, $Language, $Html, $FolderId, $IsPrivate, $GoLiveDate, $StartDate, $DisplayStartDate, $EndDate, $DisplayGoLiveDate, $DisplayEndDate, $EndDateAction, $ContentStatus, $Xslt1, $Xslt2, $Xslt3, $Xslt4, $Xslt5, $DefaultXslt, $PackageDisplayXslt, $ContentType, $ContentSubType, $ExternalTypeId, $DateCreated, $DisplayDateCreated, $DateModified, $DisplayDateModified, $LastEditorFname, $LastEditorLname, $UserId, $IsInherited, $InheritedFrom, $TemplateLink, $QuickLink, $HyperLink, $Teaser, $Status, $Comment, $LanguageDescription, $AssetInfo, $StagingDomain, $ProductionDomain, $XMLCollectionID, $FieldList, $Image, $ImageThumbnail, $IsPublished, $AnalyticsInfo)
    {
      $this->Title = $Title;
      $this->Id = $Id;
      $this->Language = $Language;
      $this->Html = $Html;
      $this->FolderId = $FolderId;
      $this->IsPrivate = $IsPrivate;
      $this->GoLiveDate = $GoLiveDate;
      $this->StartDate = $StartDate;
      $this->DisplayStartDate = $DisplayStartDate;
      $this->EndDate = $EndDate;
      $this->DisplayGoLiveDate = $DisplayGoLiveDate;
      $this->DisplayEndDate = $DisplayEndDate;
      $this->EndDateAction = $EndDateAction;
      $this->ContentStatus = $ContentStatus;
      $this->Xslt1 = $Xslt1;
      $this->Xslt2 = $Xslt2;
      $this->Xslt3 = $Xslt3;
      $this->Xslt4 = $Xslt4;
      $this->Xslt5 = $Xslt5;
      $this->DefaultXslt = $DefaultXslt;
      $this->PackageDisplayXslt = $PackageDisplayXslt;
      $this->ContentType = $ContentType;
      $this->ContentSubType = $ContentSubType;
      $this->ExternalTypeId = $ExternalTypeId;
      $this->DateCreated = $DateCreated;
      $this->DisplayDateCreated = $DisplayDateCreated;
      $this->DateModified = $DateModified;
      $this->DisplayDateModified = $DisplayDateModified;
      $this->LastEditorFname = $LastEditorFname;
      $this->LastEditorLname = $LastEditorLname;
      $this->UserId = $UserId;
      $this->IsInherited = $IsInherited;
      $this->InheritedFrom = $InheritedFrom;
      $this->TemplateLink = $TemplateLink;
      $this->QuickLink = $QuickLink;
      $this->HyperLink = $HyperLink;
      $this->Teaser = $Teaser;
      $this->Status = $Status;
      $this->Comment = $Comment;
      $this->LanguageDescription = $LanguageDescription;
      $this->AssetInfo = $AssetInfo;
      $this->StagingDomain = $StagingDomain;
      $this->ProductionDomain = $ProductionDomain;
      $this->XMLCollectionID = $XMLCollectionID;
      $this->FieldList = $FieldList;
      $this->Image = $Image;
      $this->ImageThumbnail = $ImageThumbnail;
      $this->IsPublished = $IsPublished;
      $this->AnalyticsInfo = $AnalyticsInfo;
    }

}
