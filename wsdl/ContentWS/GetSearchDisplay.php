<?php

class GetSearchDisplay
{

    /**
     * @var string $SearchText
     * @access public
     */
    public $SearchText = null;

    /**
     * @var string $SearchType
     * @access public
     */
    public $SearchType = null;

    /**
     * @var int $Recursive
     * @access public
     */
    public $Recursive = null;

    /**
     * @var string $StartingFolder
     * @access public
     */
    public $StartingFolder = null;

    /**
     * @var int $AllowFragments
     * @access public
     */
    public $AllowFragments = null;

    /**
     * @var int $MaxNumber
     * @access public
     */
    public $MaxNumber = null;

    /**
     * @param string $SearchText
     * @param string $SearchType
     * @param int $Recursive
     * @param string $StartingFolder
     * @param int $AllowFragments
     * @param int $MaxNumber
     * @access public
     */
    public function __construct($SearchText, $SearchType, $Recursive, $StartingFolder, $AllowFragments, $MaxNumber)
    {
      $this->SearchText = $SearchText;
      $this->SearchType = $SearchType;
      $this->Recursive = $Recursive;
      $this->StartingFolder = $StartingFolder;
      $this->AllowFragments = $AllowFragments;
      $this->MaxNumber = $MaxNumber;
    }

}
