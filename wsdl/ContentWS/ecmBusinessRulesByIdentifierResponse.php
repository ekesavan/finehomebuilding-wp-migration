<?php

class ecmBusinessRulesByIdentifierResponse
{

    /**
     * @var string $ecmBusinessRulesByIdentifierResult
     * @access public
     */
    public $ecmBusinessRulesByIdentifierResult = null;

    /**
     * @param string $ecmBusinessRulesByIdentifierResult
     * @access public
     */
    public function __construct($ecmBusinessRulesByIdentifierResult)
    {
      $this->ecmBusinessRulesByIdentifierResult = $ecmBusinessRulesByIdentifierResult;
    }

}
