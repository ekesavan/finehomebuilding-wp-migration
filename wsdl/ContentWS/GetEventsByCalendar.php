<?php

class GetEventsByCalendar
{

    /**
     * @var int $CalendarId
     * @access public
     */
    public $CalendarId = null;

    /**
     * @var int $EventTypeId
     * @access public
     */
    public $EventTypeId = null;

    /**
     * @var int $Month
     * @access public
     */
    public $Month = null;

    /**
     * @var int $Year
     * @access public
     */
    public $Year = null;

    /**
     * @var string $URL
     * @access public
     */
    public $URL = null;

    /**
     * @var string $QueryString
     * @access public
     */
    public $QueryString = null;

    /**
     * @param int $CalendarId
     * @param int $EventTypeId
     * @param int $Month
     * @param int $Year
     * @param string $URL
     * @param string $QueryString
     * @access public
     */
    public function __construct($CalendarId, $EventTypeId, $Month, $Year, $URL, $QueryString)
    {
      $this->CalendarId = $CalendarId;
      $this->EventTypeId = $EventTypeId;
      $this->Month = $Month;
      $this->Year = $Year;
      $this->URL = $URL;
      $this->QueryString = $QueryString;
    }

}
