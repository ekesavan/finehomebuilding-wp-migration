<?php

class ecmFormBlockResponse
{

    /**
     * @var string $ecmFormBlockResult
     * @access public
     */
    public $ecmFormBlockResult = null;

    /**
     * @param string $ecmFormBlockResult
     * @access public
     */
    public function __construct($ecmFormBlockResult)
    {
      $this->ecmFormBlockResult = $ecmFormBlockResult;
    }

}
