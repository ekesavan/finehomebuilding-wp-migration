<?php

class ecmBusinessRulesByIDResponse
{

    /**
     * @var string $ecmBusinessRulesByIDResult
     * @access public
     */
    public $ecmBusinessRulesByIDResult = null;

    /**
     * @param string $ecmBusinessRulesByIDResult
     * @access public
     */
    public function __construct($ecmBusinessRulesByIDResult)
    {
      $this->ecmBusinessRulesByIDResult = $ecmBusinessRulesByIDResult;
    }

}
