<?php

class ecmShowRandomContentResponse
{

    /**
     * @var string $ecmShowRandomContentResult
     * @access public
     */
    public $ecmShowRandomContentResult = null;

    /**
     * @param string $ecmShowRandomContentResult
     * @access public
     */
    public function __construct($ecmShowRandomContentResult)
    {
      $this->ecmShowRandomContentResult = $ecmShowRandomContentResult;
    }

}
