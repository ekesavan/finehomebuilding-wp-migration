<?php

class ecmCalendarResponse
{

    /**
     * @var string $ecmCalendarResult
     * @access public
     */
    public $ecmCalendarResult = null;

    /**
     * @param string $ecmCalendarResult
     * @access public
     */
    public function __construct($ecmCalendarResult)
    {
      $this->ecmCalendarResult = $ecmCalendarResult;
    }

}
