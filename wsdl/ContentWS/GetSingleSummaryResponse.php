<?php

class GetSingleSummaryResponse
{

    /**
     * @var TeaserResult $GetSingleSummaryResult
     * @access public
     */
    public $GetSingleSummaryResult = null;

    /**
     * @param TeaserResult $GetSingleSummaryResult
     * @access public
     */
    public function __construct($GetSingleSummaryResult)
    {
      $this->GetSingleSummaryResult = $GetSingleSummaryResult;
    }

}
