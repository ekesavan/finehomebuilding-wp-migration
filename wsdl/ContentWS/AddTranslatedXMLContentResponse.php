<?php

class AddTranslatedXMLContentResponse
{

    /**
     * @var anyType $AddTranslatedXMLContentResult
     * @access public
     */
    public $AddTranslatedXMLContentResult = null;

    /**
     * @param anyType $AddTranslatedXMLContentResult
     * @access public
     */
    public function __construct($AddTranslatedXMLContentResult)
    {
      $this->AddTranslatedXMLContentResult = $AddTranslatedXMLContentResult;
    }

}
