<?php

class ecmListSummary
{

    /**
     * @var string $Folder
     * @access public
     */
    public $Folder = null;

    /**
     * @var boolean $Recursive
     * @access public
     */
    public $Recursive = null;

    /**
     * @var boolean $ShowSummary
     * @access public
     */
    public $ShowSummary = null;

    /**
     * @var string $StyleInfo
     * @access public
     */
    public $StyleInfo = null;

    /**
     * @var string $OrderBy
     * @access public
     */
    public $OrderBy = null;

    /**
     * @var string $ShowInfo
     * @access public
     */
    public $ShowInfo = null;

    /**
     * @var int $MaxNumber
     * @access public
     */
    public $MaxNumber = null;

    /**
     * @var string $ObjType
     * @access public
     */
    public $ObjType = null;

    /**
     * @var string $OptionList
     * @access public
     */
    public $OptionList = null;

    /**
     * @var string $SummaryType
     * @access public
     */
    public $SummaryType = null;

    /**
     * @var int $UserID
     * @access public
     */
    public $UserID = null;

    /**
     * @var string $SitePath
     * @access public
     */
    public $SitePath = null;

    /**
     * @var int $Preview
     * @access public
     */
    public $Preview = null;

    /**
     * @var int $SiteLanguage
     * @access public
     */
    public $SiteLanguage = null;

    /**
     * @param string $Folder
     * @param boolean $Recursive
     * @param boolean $ShowSummary
     * @param string $StyleInfo
     * @param string $OrderBy
     * @param string $ShowInfo
     * @param int $MaxNumber
     * @param string $ObjType
     * @param string $OptionList
     * @param string $SummaryType
     * @param int $UserID
     * @param string $SitePath
     * @param int $Preview
     * @param int $SiteLanguage
     * @access public
     */
    public function __construct($Folder, $Recursive, $ShowSummary, $StyleInfo, $OrderBy, $ShowInfo, $MaxNumber, $ObjType, $OptionList, $SummaryType, $UserID, $SitePath, $Preview, $SiteLanguage)
    {
      $this->Folder = $Folder;
      $this->Recursive = $Recursive;
      $this->ShowSummary = $ShowSummary;
      $this->StyleInfo = $StyleInfo;
      $this->OrderBy = $OrderBy;
      $this->ShowInfo = $ShowInfo;
      $this->MaxNumber = $MaxNumber;
      $this->ObjType = $ObjType;
      $this->OptionList = $OptionList;
      $this->SummaryType = $SummaryType;
      $this->UserID = $UserID;
      $this->SitePath = $SitePath;
      $this->Preview = $Preview;
      $this->SiteLanguage = $SiteLanguage;
    }

}
