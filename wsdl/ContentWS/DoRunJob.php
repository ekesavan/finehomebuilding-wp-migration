<?php

class DoRunJob
{

    /**
     * @var JobType $job
     * @access public
     */
    public $job = null;

    /**
     * @param JobType $job
     * @access public
     */
    public function __construct($job)
    {
      $this->job = $job;
    }

}
