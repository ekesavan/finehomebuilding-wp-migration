<?php

class ecmGetContentBlock
{

    /**
     * @var int $ContentId
     * @access public
     */
    public $ContentId = null;

    /**
     * @var string $SearchTerms
     * @access public
     */
    public $SearchTerms = null;

    /**
     * @var string $SearchType
     * @access public
     */
    public $SearchType = null;

    /**
     * @var string $SearchFragment
     * @access public
     */
    public $SearchFragment = null;

    /**
     * @var int $UserID
     * @access public
     */
    public $UserID = null;

    /**
     * @var string $SitePath
     * @access public
     */
    public $SitePath = null;

    /**
     * @var int $Preview
     * @access public
     */
    public $Preview = null;

    /**
     * @var int $SiteLanguage
     * @access public
     */
    public $SiteLanguage = null;

    /**
     * @param int $ContentId
     * @param string $SearchTerms
     * @param string $SearchType
     * @param string $SearchFragment
     * @param int $UserID
     * @param string $SitePath
     * @param int $Preview
     * @param int $SiteLanguage
     * @access public
     */
    public function __construct($ContentId, $SearchTerms, $SearchType, $SearchFragment, $UserID, $SitePath, $Preview, $SiteLanguage)
    {
      $this->ContentId = $ContentId;
      $this->SearchTerms = $SearchTerms;
      $this->SearchType = $SearchType;
      $this->SearchFragment = $SearchFragment;
      $this->UserID = $UserID;
      $this->SitePath = $SitePath;
      $this->Preview = $Preview;
      $this->SiteLanguage = $SiteLanguage;
    }

}
