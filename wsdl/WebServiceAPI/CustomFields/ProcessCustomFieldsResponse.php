<?php

class ProcessCustomFieldsResponse
{

    /**
     * @var anyType $ProcessCustomFieldsResult
     * @access public
     */
    public $ProcessCustomFieldsResult = null;

    /**
     * @param anyType $ProcessCustomFieldsResult
     * @access public
     */
    public function __construct($ProcessCustomFieldsResult)
    {
      $this->ProcessCustomFieldsResult = $ProcessCustomFieldsResult;
    }

}
