<?php

class WriteMetadataTypeForView
{

    /**
     * @var string $DataType
     * @access public
     */
    public $DataType = null;

    /**
     * @var string $Caption
     * @access public
     */
    public $Caption = null;

    /**
     * @var string $Value
     * @access public
     */
    public $Value = null;

    /**
     * @param string $DataType
     * @param string $Caption
     * @param string $Value
     * @access public
     */
    public function __construct($DataType, $Caption, $Value)
    {
      $this->DataType = $DataType;
      $this->Caption = $Caption;
      $this->Value = $Value;
    }

}
