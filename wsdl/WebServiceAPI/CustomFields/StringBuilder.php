<?php

class StringBuilder
{

    /**
     * @var int $Capacity
     * @access public
     */
    public $Capacity = null;

    /**
     * @var int $Length
     * @access public
     */
    public $Length = null;

    /**
     * @param int $Capacity
     * @param int $Length
     * @access public
     */
    public function __construct($Capacity, $Length)
    {
      $this->Capacity = $Capacity;
      $this->Length = $Length;
    }

}
