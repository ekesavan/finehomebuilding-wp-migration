<?php

class WriteLibrarySearchExtendedResponse
{

    /**
     * @var StringBuilder $WriteLibrarySearchExtendedResult
     * @access public
     */
    public $WriteLibrarySearchExtendedResult = null;

    /**
     * @param StringBuilder $WriteLibrarySearchExtendedResult
     * @access public
     */
    public function __construct($WriteLibrarySearchExtendedResult)
    {
      $this->WriteLibrarySearchExtendedResult = $WriteLibrarySearchExtendedResult;
    }

}
