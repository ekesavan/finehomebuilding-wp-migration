<?php

class SearchAssetDispayRequest
{

    /**
     * @var boolean $Recursive
     * @access public
     */
    public $Recursive = null;

    /**
     * @var string $StartingFolder
     * @access public
     */
    public $StartingFolder = null;

    /**
     * @var int $StartingFolderID
     * @access public
     */
    public $StartingFolderID = null;

    /**
     * @var int $TextBoxSize
     * @access public
     */
    public $TextBoxSize = null;

    /**
     * @var int $TextBoxMaxChars
     * @access public
     */
    public $TextBoxMaxChars = null;

    /**
     * @var string $ButtonImageSrc
     * @access public
     */
    public $ButtonImageSrc = null;

    /**
     * @var string $ButtonText
     * @access public
     */
    public $ButtonText = null;

    /**
     * @var boolean $Horizontal
     * @access public
     */
    public $Horizontal = null;

    /**
     * @var boolean $PublicSearch
     * @access public
     */
    public $PublicSearch = null;

    /**
     * @var string $styleText
     * @access public
     */
    public $styleText = null;

    /**
     * @var string $className
     * @access public
     */
    public $className = null;

    /**
     * @var int $LangugeID
     * @access public
     */
    public $LangugeID = null;

    /**
     * @var boolean $ShowSearchOptions
     * @access public
     */
    public $ShowSearchOptions = null;

    /**
     * @var string $BeginFontInfo
     * @access public
     */
    public $BeginFontInfo = null;

    /**
     * @var string $EndFontInfo
     * @access public
     */
    public $EndFontInfo = null;

    /**
     * @var string $FontFace
     * @access public
     */
    public $FontFace = null;

    /**
     * @var string $FontColor
     * @access public
     */
    public $FontColor = null;

    /**
     * @var string $FontSize
     * @access public
     */
    public $FontSize = null;

    /**
     * @var string $TargetPage
     * @access public
     */
    public $TargetPage = null;

    /**
     * @var boolean $DynamicIncludeCmsTypes
     * @access public
     */
    public $DynamicIncludeCmsTypes = null;

    /**
     * @var string $CmsTypes
     * @access public
     */
    public $CmsTypes = null;

    /**
     * @var boolean $DynamicIncludeAssetTypes
     * @access public
     */
    public $DynamicIncludeAssetTypes = null;

    /**
     * @var string $AssetTypes
     * @access public
     */
    public $AssetTypes = null;

    /**
     * @var boolean $AddFormTag
     * @access public
     */
    public $AddFormTag = null;

    /**
     * @var string $FormMode
     * @access public
     */
    public $FormMode = null;

    /**
     * @var int $CustomSearchId
     * @access public
     */
    public $CustomSearchId = null;

    /**
     * @var string $IncludeFile
     * @access public
     */
    public $IncludeFile = null;

    /**
     * @var string $SearchInstance
     * @access public
     */
    public $SearchInstance = null;

    /**
     * @var boolean $ShowSearchableProperties
     * @access public
     */
    public $ShowSearchableProperties = null;

    /**
     * @var string $SearchHeaderText
     * @access public
     */
    public $SearchHeaderText = null;

    /**
     * @var boolean $EnableShowLibrary
     * @access public
     */
    public $EnableShowLibrary = null;

    /**
     * @var boolean $EnableShowArchived
     * @access public
     */
    public $EnableShowArchived = null;

    /**
     * @var boolean $EnableAdvancedSearchLink
     * @access public
     */
    public $EnableAdvancedSearchLink = null;

    /**
     * @var boolean $EnableBasicSearchLink
     * @access public
     */
    public $EnableBasicSearchLink = null;

    /**
     * @param boolean $Recursive
     * @param string $StartingFolder
     * @param int $StartingFolderID
     * @param int $TextBoxSize
     * @param int $TextBoxMaxChars
     * @param string $ButtonImageSrc
     * @param string $ButtonText
     * @param boolean $Horizontal
     * @param boolean $PublicSearch
     * @param string $styleText
     * @param string $className
     * @param int $LangugeID
     * @param boolean $ShowSearchOptions
     * @param string $BeginFontInfo
     * @param string $EndFontInfo
     * @param string $FontFace
     * @param string $FontColor
     * @param string $FontSize
     * @param string $TargetPage
     * @param boolean $DynamicIncludeCmsTypes
     * @param string $CmsTypes
     * @param boolean $DynamicIncludeAssetTypes
     * @param string $AssetTypes
     * @param boolean $AddFormTag
     * @param string $FormMode
     * @param int $CustomSearchId
     * @param string $IncludeFile
     * @param string $SearchInstance
     * @param boolean $ShowSearchableProperties
     * @param string $SearchHeaderText
     * @param boolean $EnableShowLibrary
     * @param boolean $EnableShowArchived
     * @param boolean $EnableAdvancedSearchLink
     * @param boolean $EnableBasicSearchLink
     * @access public
     */
    public function __construct($Recursive, $StartingFolder, $StartingFolderID, $TextBoxSize, $TextBoxMaxChars, $ButtonImageSrc, $ButtonText, $Horizontal, $PublicSearch, $styleText, $className, $LangugeID, $ShowSearchOptions, $BeginFontInfo, $EndFontInfo, $FontFace, $FontColor, $FontSize, $TargetPage, $DynamicIncludeCmsTypes, $CmsTypes, $DynamicIncludeAssetTypes, $AssetTypes, $AddFormTag, $FormMode, $CustomSearchId, $IncludeFile, $SearchInstance, $ShowSearchableProperties, $SearchHeaderText, $EnableShowLibrary, $EnableShowArchived, $EnableAdvancedSearchLink, $EnableBasicSearchLink)
    {
      $this->Recursive = $Recursive;
      $this->StartingFolder = $StartingFolder;
      $this->StartingFolderID = $StartingFolderID;
      $this->TextBoxSize = $TextBoxSize;
      $this->TextBoxMaxChars = $TextBoxMaxChars;
      $this->ButtonImageSrc = $ButtonImageSrc;
      $this->ButtonText = $ButtonText;
      $this->Horizontal = $Horizontal;
      $this->PublicSearch = $PublicSearch;
      $this->styleText = $styleText;
      $this->className = $className;
      $this->LangugeID = $LangugeID;
      $this->ShowSearchOptions = $ShowSearchOptions;
      $this->BeginFontInfo = $BeginFontInfo;
      $this->EndFontInfo = $EndFontInfo;
      $this->FontFace = $FontFace;
      $this->FontColor = $FontColor;
      $this->FontSize = $FontSize;
      $this->TargetPage = $TargetPage;
      $this->DynamicIncludeCmsTypes = $DynamicIncludeCmsTypes;
      $this->CmsTypes = $CmsTypes;
      $this->DynamicIncludeAssetTypes = $DynamicIncludeAssetTypes;
      $this->AssetTypes = $AssetTypes;
      $this->AddFormTag = $AddFormTag;
      $this->FormMode = $FormMode;
      $this->CustomSearchId = $CustomSearchId;
      $this->IncludeFile = $IncludeFile;
      $this->SearchInstance = $SearchInstance;
      $this->ShowSearchableProperties = $ShowSearchableProperties;
      $this->SearchHeaderText = $SearchHeaderText;
      $this->EnableShowLibrary = $EnableShowLibrary;
      $this->EnableShowArchived = $EnableShowArchived;
      $this->EnableAdvancedSearchLink = $EnableAdvancedSearchLink;
      $this->EnableBasicSearchLink = $EnableBasicSearchLink;
    }

}
