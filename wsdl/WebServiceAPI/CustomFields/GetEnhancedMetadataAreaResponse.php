<?php

class GetEnhancedMetadataAreaResponse
{

    /**
     * @var string $GetEnhancedMetadataAreaResult
     * @access public
     */
    public $GetEnhancedMetadataAreaResult = null;

    /**
     * @param string $GetEnhancedMetadataAreaResult
     * @access public
     */
    public function __construct($GetEnhancedMetadataAreaResult)
    {
      $this->GetEnhancedMetadataAreaResult = $GetEnhancedMetadataAreaResult;
    }

}
