<?php

class WritePublicSearchResponse
{

    /**
     * @var StringBuilder $WritePublicSearchResult
     * @access public
     */
    public $WritePublicSearchResult = null;

    /**
     * @param StringBuilder $WritePublicSearchResult
     * @access public
     */
    public function __construct($WritePublicSearchResult)
    {
      $this->WritePublicSearchResult = $WritePublicSearchResult;
    }

}
