<?php

class SearchAllAssetsResponse
{

    /**
     * @var StringBuilder $SearchAllAssetsResult
     * @access public
     */
    public $SearchAllAssetsResult = null;

    /**
     * @param StringBuilder $SearchAllAssetsResult
     * @access public
     */
    public function __construct($SearchAllAssetsResult)
    {
      $this->SearchAllAssetsResult = $SearchAllAssetsResult;
    }

}
