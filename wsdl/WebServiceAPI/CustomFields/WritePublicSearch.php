<?php

class WritePublicSearch
{

    /**
     * @var SearchAssetDispayRequest $sadReq
     * @access public
     */
    public $sadReq = null;

    /**
     * @param SearchAssetDispayRequest $sadReq
     * @access public
     */
    public function __construct($sadReq)
    {
      $this->sadReq = $sadReq;
    }

}
