<?php

class GetSearchPropertiesResponse
{

    /**
     * @var StringBuilder $GetSearchPropertiesResult
     * @access public
     */
    public $GetSearchPropertiesResult = null;

    /**
     * @param StringBuilder $GetSearchPropertiesResult
     * @access public
     */
    public function __construct($GetSearchPropertiesResult)
    {
      $this->GetSearchPropertiesResult = $GetSearchPropertiesResult;
    }

}
