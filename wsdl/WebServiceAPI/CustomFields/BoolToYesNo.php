<?php

class BoolToYesNo
{

    /**
     * @var boolean $Value
     * @access public
     */
    public $Value = null;

    /**
     * @param boolean $Value
     * @access public
     */
    public function __construct($Value)
    {
      $this->Value = $Value;
    }

}
