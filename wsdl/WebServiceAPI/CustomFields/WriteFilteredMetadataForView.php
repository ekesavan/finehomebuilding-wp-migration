<?php

class WriteFilteredMetadataForView
{

    /**
     * @var ContentMetaData[] $MetadataTypes
     * @access public
     */
    public $MetadataTypes = null;

    /**
     * @var int $FolderId
     * @access public
     */
    public $FolderId = null;

    /**
     * @var boolean $SearchableOnly
     * @access public
     */
    public $SearchableOnly = null;

    /**
     * @param ContentMetaData[] $MetadataTypes
     * @param int $FolderId
     * @param boolean $SearchableOnly
     * @access public
     */
    public function __construct($MetadataTypes, $FolderId, $SearchableOnly)
    {
      $this->MetadataTypes = $MetadataTypes;
      $this->FolderId = $FolderId;
      $this->SearchableOnly = $SearchableOnly;
    }

}
