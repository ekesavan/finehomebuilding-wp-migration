<?php

class AddNewEditableCustomFieldAssignmentsResponse
{

    /**
     * @var string $AddNewEditableCustomFieldAssignmentsResult
     * @access public
     */
    public $AddNewEditableCustomFieldAssignmentsResult = null;

    /**
     * @param string $AddNewEditableCustomFieldAssignmentsResult
     * @access public
     */
    public function __construct($AddNewEditableCustomFieldAssignmentsResult)
    {
      $this->AddNewEditableCustomFieldAssignmentsResult = $AddNewEditableCustomFieldAssignmentsResult;
    }

}
