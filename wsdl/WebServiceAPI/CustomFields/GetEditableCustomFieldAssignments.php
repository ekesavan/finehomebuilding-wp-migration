<?php

class GetEditableCustomFieldAssignments
{

    /**
     * @var anyType $folderID
     * @access public
     */
    public $folderID = null;

    /**
     * @var anyType $editMode
     * @access public
     */
    public $editMode = null;

    /**
     * @param anyType $folderID
     * @param anyType $editMode
     * @access public
     */
    public function __construct($folderID, $editMode)
    {
      $this->folderID = $folderID;
      $this->editMode = $editMode;
    }

}
