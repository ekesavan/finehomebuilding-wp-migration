<?php

class AddNewEditableCustomFieldAssignments
{

    /**
     * @var int $parentfolderid
     * @access public
     */
    public $parentfolderid = null;

    /**
     * @param int $parentfolderid
     * @access public
     */
    public function __construct($parentfolderid)
    {
      $this->parentfolderid = $parentfolderid;
    }

}
