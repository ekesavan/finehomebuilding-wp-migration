<?php

class GetEditableCustomFieldAssignmentsResponse
{

    /**
     * @var string $GetEditableCustomFieldAssignmentsResult
     * @access public
     */
    public $GetEditableCustomFieldAssignmentsResult = null;

    /**
     * @param string $GetEditableCustomFieldAssignmentsResult
     * @access public
     */
    public function __construct($GetEditableCustomFieldAssignmentsResult)
    {
      $this->GetEditableCustomFieldAssignmentsResult = $GetEditableCustomFieldAssignmentsResult;
    }

}
