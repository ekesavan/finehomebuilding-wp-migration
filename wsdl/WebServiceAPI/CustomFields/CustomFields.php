<?php

include_once('AddNewEditableCustomFieldAssignments.php');
include_once('AddNewEditableCustomFieldAssignmentsResponse.php');
include_once('AuthenticationHeader.php');
include_once('RequestInfoParameters.php');
include_once('WriteLibrarySearchExtended.php');
include_once('WriteLibrarySearchExtendedResponse.php');
include_once('StringBuilder.php');
include_once('SearchAllAssets.php');
include_once('SearchAssetDispayRequest.php');
include_once('SearchAllAssetsResponse.php');
include_once('WritePublicSearch.php');
include_once('WritePublicSearchResponse.php');
include_once('WriteFilteredMetadataForView.php');
include_once('ContentMetaData.php');
include_once('CmsDataOfContentMetaData.php');
include_once('BaseDataOfContentMetaData.php');
include_once('ContentMetadataDataType.php');
include_once('ContentMetadataType.php');
include_once('WriteFilteredMetadataForViewResponse.php');
include_once('WriteMetadataTypeForView.php');
include_once('WriteMetadataTypeForViewResponse.php');
include_once('GetEditableCustomFieldAssignments.php');
include_once('GetEditableCustomFieldAssignmentsResponse.php');
include_once('ProcessCustomFields.php');
include_once('ProcessCustomFieldsResponse.php');
include_once('BoolToYesNo.php');
include_once('BoolToYesNoResponse.php');
include_once('GetEnhancedMetadataArea.php');
include_once('GetEnhancedMetadataAreaResponse.php');
include_once('GetEnhancedMetadataScript.php');
include_once('GetEnhancedMetadataScriptResponse.php');
include_once('GetSearchProperties.php');
include_once('GetSearchPropertiesResponse.php');

class CustomFields extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'AddNewEditableCustomFieldAssignments' => '\AddNewEditableCustomFieldAssignments',
      'AddNewEditableCustomFieldAssignmentsResponse' => '\AddNewEditableCustomFieldAssignmentsResponse',
      'AuthenticationHeader' => '\AuthenticationHeader',
      'RequestInfoParameters' => '\RequestInfoParameters',
      'WriteLibrarySearchExtended' => '\WriteLibrarySearchExtended',
      'WriteLibrarySearchExtendedResponse' => '\WriteLibrarySearchExtendedResponse',
      'StringBuilder' => '\StringBuilder',
      'SearchAllAssets' => '\SearchAllAssets',
      'SearchAssetDispayRequest' => '\SearchAssetDispayRequest',
      'SearchAllAssetsResponse' => '\SearchAllAssetsResponse',
      'WritePublicSearch' => '\WritePublicSearch',
      'WritePublicSearchResponse' => '\WritePublicSearchResponse',
      'WriteFilteredMetadataForView' => '\WriteFilteredMetadataForView',
      'ContentMetaData' => '\ContentMetaData',
      'CmsDataOfContentMetaData' => '\CmsDataOfContentMetaData',
      'BaseDataOfContentMetaData' => '\BaseDataOfContentMetaData',
      'WriteFilteredMetadataForViewResponse' => '\WriteFilteredMetadataForViewResponse',
      'WriteMetadataTypeForView' => '\WriteMetadataTypeForView',
      'WriteMetadataTypeForViewResponse' => '\WriteMetadataTypeForViewResponse',
      'GetEditableCustomFieldAssignments' => '\GetEditableCustomFieldAssignments',
      'GetEditableCustomFieldAssignmentsResponse' => '\GetEditableCustomFieldAssignmentsResponse',
      'ProcessCustomFields' => '\ProcessCustomFields',
      'ProcessCustomFieldsResponse' => '\ProcessCustomFieldsResponse',
      'BoolToYesNo' => '\BoolToYesNo',
      'BoolToYesNoResponse' => '\BoolToYesNoResponse',
      'GetEnhancedMetadataArea' => '\GetEnhancedMetadataArea',
      'GetEnhancedMetadataAreaResponse' => '\GetEnhancedMetadataAreaResponse',
      'GetEnhancedMetadataScript' => '\GetEnhancedMetadataScript',
      'GetEnhancedMetadataScriptResponse' => '\GetEnhancedMetadataScriptResponse',
      'GetSearchProperties' => '\GetSearchProperties',
      'GetSearchPropertiesResponse' => '\GetSearchPropertiesResponse');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = 'CustomFields.wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      
      parent::__construct($wsdl, $options);
    }

    /**
     * @param AddNewEditableCustomFieldAssignments $parameters
     * @access public
     * @return AddNewEditableCustomFieldAssignmentsResponse
     */
    public function AddNewEditableCustomFieldAssignments(AddNewEditableCustomFieldAssignments $parameters)
    {
      return $this->__soapCall('AddNewEditableCustomFieldAssignments', array($parameters));
    }

    /**
     * @param WriteLibrarySearchExtended $parameters
     * @access public
     * @return WriteLibrarySearchExtendedResponse
     */
    public function WriteLibrarySearchExtended(WriteLibrarySearchExtended $parameters)
    {
      return $this->__soapCall('WriteLibrarySearchExtended', array($parameters));
    }

    /**
     * @param SearchAllAssets $parameters
     * @access public
     * @return SearchAllAssetsResponse
     */
    public function SearchAllAssets(SearchAllAssets $parameters)
    {
      return $this->__soapCall('SearchAllAssets', array($parameters));
    }

    /**
     * @param WritePublicSearch $parameters
     * @access public
     * @return WritePublicSearchResponse
     */
    public function WritePublicSearch(WritePublicSearch $parameters)
    {
      return $this->__soapCall('WritePublicSearch', array($parameters));
    }

    /**
     * @param WriteFilteredMetadataForView $parameters
     * @access public
     * @return WriteFilteredMetadataForViewResponse
     */
    public function WriteFilteredMetadataForView(WriteFilteredMetadataForView $parameters)
    {
      return $this->__soapCall('WriteFilteredMetadataForView', array($parameters));
    }

    /**
     * @param WriteMetadataTypeForView $parameters
     * @access public
     * @return WriteMetadataTypeForViewResponse
     */
    public function WriteMetadataTypeForView(WriteMetadataTypeForView $parameters)
    {
      return $this->__soapCall('WriteMetadataTypeForView', array($parameters));
    }

    /**
     * @param GetEditableCustomFieldAssignments $parameters
     * @access public
     * @return GetEditableCustomFieldAssignmentsResponse
     */
    public function GetEditableCustomFieldAssignments(GetEditableCustomFieldAssignments $parameters)
    {
      return $this->__soapCall('GetEditableCustomFieldAssignments', array($parameters));
    }

    /**
     * @param ProcessCustomFields $parameters
     * @access public
     * @return ProcessCustomFieldsResponse
     */
    public function ProcessCustomFields(ProcessCustomFields $parameters)
    {
      return $this->__soapCall('ProcessCustomFields', array($parameters));
    }

    /**
     * @param BoolToYesNo $parameters
     * @access public
     * @return BoolToYesNoResponse
     */
    public function BoolToYesNo(BoolToYesNo $parameters)
    {
      return $this->__soapCall('BoolToYesNo', array($parameters));
    }

    /**
     * @param GetEnhancedMetadataArea $parameters
     * @access public
     * @return GetEnhancedMetadataAreaResponse
     */
    public function GetEnhancedMetadataArea(GetEnhancedMetadataArea $parameters)
    {
      return $this->__soapCall('GetEnhancedMetadataArea', array($parameters));
    }

    /**
     * @param GetEnhancedMetadataScript $parameters
     * @access public
     * @return GetEnhancedMetadataScriptResponse
     */
    public function GetEnhancedMetadataScript(GetEnhancedMetadataScript $parameters)
    {
      return $this->__soapCall('GetEnhancedMetadataScript', array($parameters));
    }

    /**
     * @param GetSearchProperties $parameters
     * @access public
     * @return GetSearchPropertiesResponse
     */
    public function GetSearchProperties(GetSearchProperties $parameters)
    {
      return $this->__soapCall('GetSearchProperties', array($parameters));
    }

}
