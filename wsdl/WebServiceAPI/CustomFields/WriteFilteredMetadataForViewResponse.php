<?php

class WriteFilteredMetadataForViewResponse
{

    /**
     * @var string $WriteFilteredMetadataForViewResult
     * @access public
     */
    public $WriteFilteredMetadataForViewResult = null;

    /**
     * @param string $WriteFilteredMetadataForViewResult
     * @access public
     */
    public function __construct($WriteFilteredMetadataForViewResult)
    {
      $this->WriteFilteredMetadataForViewResult = $WriteFilteredMetadataForViewResult;
    }

}
