<?php

class WriteMetadataTypeForViewResponse
{

    /**
     * @var string $WriteMetadataTypeForViewResult
     * @access public
     */
    public $WriteMetadataTypeForViewResult = null;

    /**
     * @param string $WriteMetadataTypeForViewResult
     * @access public
     */
    public function __construct($WriteMetadataTypeForViewResult)
    {
      $this->WriteMetadataTypeForViewResult = $WriteMetadataTypeForViewResult;
    }

}
