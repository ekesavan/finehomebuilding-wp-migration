<?php

class ProcessCustomFields
{

    /**
     * @var int $folderID
     * @access public
     */
    public $folderID = null;

    /**
     * @param int $folderID
     * @access public
     */
    public function __construct($folderID)
    {
      $this->folderID = $folderID;
    }

}
