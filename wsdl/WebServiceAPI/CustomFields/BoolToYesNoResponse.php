<?php

class BoolToYesNoResponse
{

    /**
     * @var string $BoolToYesNoResult
     * @access public
     */
    public $BoolToYesNoResult = null;

    /**
     * @param string $BoolToYesNoResult
     * @access public
     */
    public function __construct($BoolToYesNoResult)
    {
      $this->BoolToYesNoResult = $BoolToYesNoResult;
    }

}
