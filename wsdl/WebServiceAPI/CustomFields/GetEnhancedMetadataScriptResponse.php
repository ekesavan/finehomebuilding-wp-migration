<?php

class GetEnhancedMetadataScriptResponse
{

    /**
     * @var string $GetEnhancedMetadataScriptResult
     * @access public
     */
    public $GetEnhancedMetadataScriptResult = null;

    /**
     * @param string $GetEnhancedMetadataScriptResult
     * @access public
     */
    public function __construct($GetEnhancedMetadataScriptResult)
    {
      $this->GetEnhancedMetadataScriptResult = $GetEnhancedMetadataScriptResult;
    }

}
