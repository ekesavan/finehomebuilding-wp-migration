<?php

class CreateFolderResponse
{

    /**
     * @var boolean $CreateFolderResult
     * @access public
     */
    public $CreateFolderResult = null;

    /**
     * @param boolean $CreateFolderResult
     * @access public
     */
    public function __construct($CreateFolderResult)
    {
      $this->CreateFolderResult = $CreateFolderResult;
    }

}
