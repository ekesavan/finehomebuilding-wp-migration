<?php

class FlagItemData
{

    /**
     * @var int $ID
     * @access public
     */
    public $ID = null;

    /**
     * @var int $FlagDefinitionID
     * @access public
     */
    public $FlagDefinitionID = null;

    /**
     * @var int $FlagDefinitionLanguage
     * @access public
     */
    public $FlagDefinitionLanguage = null;

    /**
     * @var int $TotalFlags
     * @access public
     */
    public $TotalFlags = null;

    /**
     * @var int $SortOrder
     * @access public
     */
    public $SortOrder = null;

    /**
     * @var string $Name
     * @access public
     */
    public $Name = null;

    /**
     * @var string $Description
     * @access public
     */
    public $Description = null;

    /**
     * @var dateTime $DateModified
     * @access public
     */
    public $DateModified = null;

    /**
     * @param int $ID
     * @param int $FlagDefinitionID
     * @param int $FlagDefinitionLanguage
     * @param int $TotalFlags
     * @param int $SortOrder
     * @param string $Name
     * @param string $Description
     * @param dateTime $DateModified
     * @access public
     */
    public function __construct($ID, $FlagDefinitionID, $FlagDefinitionLanguage, $TotalFlags, $SortOrder, $Name, $Description, $DateModified)
    {
      $this->ID = $ID;
      $this->FlagDefinitionID = $FlagDefinitionID;
      $this->FlagDefinitionLanguage = $FlagDefinitionLanguage;
      $this->TotalFlags = $TotalFlags;
      $this->SortOrder = $SortOrder;
      $this->Name = $Name;
      $this->Description = $Description;
      $this->DateModified = $DateModified;
    }

}
