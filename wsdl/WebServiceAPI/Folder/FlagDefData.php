<?php

include_once('CmsDataOfFlagDefData.php');

class FlagDefData extends CmsDataOfFlagDefData
{

    /**
     * @var int $Language
     * @access public
     */
    public $Language = null;

    /**
     * @var string $Name
     * @access public
     */
    public $Name = null;

    /**
     * @var string $Description
     * @access public
     */
    public $Description = null;

    /**
     * @var boolean $Hidden
     * @access public
     */
    public $Hidden = null;

    /**
     * @var FlagItemData[] $Items
     * @access public
     */
    public $Items = null;

    /**
     * @param int $Id
     * @param int $Language
     * @param string $Name
     * @param string $Description
     * @param boolean $Hidden
     * @param FlagItemData[] $Items
     * @access public
     */
    public function __construct($Id, $Language, $Name, $Description, $Hidden, $Items)
    {
      parent::__construct($Id);
      $this->Language = $Language;
      $this->Name = $Name;
      $this->Description = $Description;
      $this->Hidden = $Hidden;
      $this->Items = $Items;
    }

}
