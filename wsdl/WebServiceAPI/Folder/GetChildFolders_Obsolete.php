<?php

class GetChildFolders_Obsolete
{

    /**
     * @var int $FolderID
     * @access public
     */
    public $FolderID = null;

    /**
     * @var boolean $Recursive
     * @access public
     */
    public $Recursive = null;

    /**
     * @var string $OrderBy
     * @access public
     */
    public $OrderBy = null;

    /**
     * @param int $FolderID
     * @param boolean $Recursive
     * @param string $OrderBy
     * @access public
     */
    public function __construct($FolderID, $Recursive, $OrderBy)
    {
      $this->FolderID = $FolderID;
      $this->Recursive = $Recursive;
      $this->OrderBy = $OrderBy;
    }

}
