<?php

class ContentMetadataDataType
{
    const __default = 'Text';
    const Text = 'Text';
    const Number = 'Number';
    const Byte = 'Byte';
    const Double = 'Double';
    const Float = 'Float';
    const Integer = 'Integer';
    const Long = 'Long';
    const Short = 'Short';
    const Date = 'Date';
    const Boolean = 'Boolean';
    const SingleSelect = 'SingleSelect';
    const MultiSelect = 'MultiSelect';


}
