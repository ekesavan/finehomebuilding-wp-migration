<?php

class GetPathResponse
{

    /**
     * @var string $GetPathResult
     * @access public
     */
    public $GetPathResult = null;

    /**
     * @param string $GetPathResult
     * @access public
     */
    public function __construct($GetPathResult)
    {
      $this->GetPathResult = $GetPathResult;
    }

}
