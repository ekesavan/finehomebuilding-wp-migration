<?php

include_once('CmsDataOfPermissionData.php');

class PermissionData extends CmsDataOfPermissionData
{

    /**
     * @var boolean $CanApprove
     * @access public
     */
    public $CanApprove = null;

    /**
     * @var boolean $CanAdd
     * @access public
     */
    public $CanAdd = null;

    /**
     * @var boolean $CanEdit
     * @access public
     */
    public $CanEdit = null;

    /**
     * @var boolean $CanDelete
     * @access public
     */
    public $CanDelete = null;

    /**
     * @var boolean $CanRestore
     * @access public
     */
    public $CanRestore = null;

    /**
     * @var boolean $CanAddToImageLib
     * @access public
     */
    public $CanAddToImageLib = null;

    /**
     * @var boolean $CanAddToFileLib
     * @access public
     */
    public $CanAddToFileLib = null;

    /**
     * @var boolean $CanOverwriteLib
     * @access public
     */
    public $CanOverwriteLib = null;

    /**
     * @var boolean $CanAddToHyperlinkLib
     * @access public
     */
    public $CanAddToHyperlinkLib = null;

    /**
     * @var boolean $CanAddToQuicklinkLib
     * @access public
     */
    public $CanAddToQuicklinkLib = null;

    /**
     * @var boolean $CanAddFolders
     * @access public
     */
    public $CanAddFolders = null;

    /**
     * @var boolean $CanEditFolders
     * @access public
     */
    public $CanEditFolders = null;

    /**
     * @var boolean $CanDeleteFolders
     * @access public
     */
    public $CanDeleteFolders = null;

    /**
     * @var boolean $CanTraverseFolders
     * @access public
     */
    public $CanTraverseFolders = null;

    /**
     * @var boolean $CanCreateTask
     * @access public
     */
    public $CanCreateTask = null;

    /**
     * @var boolean $CanRedirectTask
     * @access public
     */
    public $CanRedirectTask = null;

    /**
     * @var boolean $CanDestructTask
     * @access public
     */
    public $CanDestructTask = null;

    /**
     * @var boolean $IsReadOnly
     * @access public
     */
    public $IsReadOnly = null;

    /**
     * @var boolean $IsCollections
     * @access public
     */
    public $IsCollections = null;

    /**
     * @var boolean $IsAdmin
     * @access public
     */
    public $IsAdmin = null;

    /**
     * @var boolean $IsLoggedIn
     * @access public
     */
    public $IsLoggedIn = null;

    /**
     * @var boolean $IsInMemberShip
     * @access public
     */
    public $IsInMemberShip = null;

    /**
     * @var boolean $IsReadOnlyLib
     * @access public
     */
    public $IsReadOnlyLib = null;

    /**
     * @var anyType $LegacyData
     * @access public
     */
    public $LegacyData = null;

    /**
     * @var boolean $CanHistory
     * @access public
     */
    public $CanHistory = null;

    /**
     * @var boolean $CanPublish
     * @access public
     */
    public $CanPublish = null;

    /**
     * @var boolean $CanAddTask
     * @access public
     */
    public $CanAddTask = null;

    /**
     * @var boolean $CanBreakPending
     * @access public
     */
    public $CanBreakPending = null;

    /**
     * @var boolean $CanView
     * @access public
     */
    public $CanView = null;

    /**
     * @var boolean $CanEditSummary
     * @access public
     */
    public $CanEditSummary = null;

    /**
     * @var boolean $CanSeeProperty
     * @access public
     */
    public $CanSeeProperty = null;

    /**
     * @var boolean $CanPreview
     * @access public
     */
    public $CanPreview = null;

    /**
     * @var boolean $CanDecline
     * @access public
     */
    public $CanDecline = null;

    /**
     * @var boolean $CanEditSumit
     * @access public
     */
    public $CanEditSumit = null;

    /**
     * @var boolean $CanEditCollections
     * @access public
     */
    public $CanEditCollections = null;

    /**
     * @var boolean $CanDeleteTask
     * @access public
     */
    public $CanDeleteTask = null;

    /**
     * @var boolean $CanMetadataComplete
     * @access public
     */
    public $CanMetadataComplete = null;

    /**
     * @var boolean $CanEditProperties
     * @access public
     */
    public $CanEditProperties = null;

    /**
     * @var boolean $CanEditApprovals
     * @access public
     */
    public $CanEditApprovals = null;

    /**
     * @var boolean $CanEditQLinks
     * @access public
     */
    public $CanEditQLinks = null;

    /**
     * @param int $Id
     * @param boolean $CanApprove
     * @param boolean $CanAdd
     * @param boolean $CanEdit
     * @param boolean $CanDelete
     * @param boolean $CanRestore
     * @param boolean $CanAddToImageLib
     * @param boolean $CanAddToFileLib
     * @param boolean $CanOverwriteLib
     * @param boolean $CanAddToHyperlinkLib
     * @param boolean $CanAddToQuicklinkLib
     * @param boolean $CanAddFolders
     * @param boolean $CanEditFolders
     * @param boolean $CanDeleteFolders
     * @param boolean $CanTraverseFolders
     * @param boolean $CanCreateTask
     * @param boolean $CanRedirectTask
     * @param boolean $CanDestructTask
     * @param boolean $IsReadOnly
     * @param boolean $IsCollections
     * @param boolean $IsAdmin
     * @param boolean $IsLoggedIn
     * @param boolean $IsInMemberShip
     * @param boolean $IsReadOnlyLib
     * @param anyType $LegacyData
     * @param boolean $CanHistory
     * @param boolean $CanPublish
     * @param boolean $CanAddTask
     * @param boolean $CanBreakPending
     * @param boolean $CanView
     * @param boolean $CanEditSummary
     * @param boolean $CanSeeProperty
     * @param boolean $CanPreview
     * @param boolean $CanDecline
     * @param boolean $CanEditSumit
     * @param boolean $CanEditCollections
     * @param boolean $CanDeleteTask
     * @param boolean $CanMetadataComplete
     * @param boolean $CanEditProperties
     * @param boolean $CanEditApprovals
     * @param boolean $CanEditQLinks
     * @access public
     */
    public function __construct($Id, $CanApprove, $CanAdd, $CanEdit, $CanDelete, $CanRestore, $CanAddToImageLib, $CanAddToFileLib, $CanOverwriteLib, $CanAddToHyperlinkLib, $CanAddToQuicklinkLib, $CanAddFolders, $CanEditFolders, $CanDeleteFolders, $CanTraverseFolders, $CanCreateTask, $CanRedirectTask, $CanDestructTask, $IsReadOnly, $IsCollections, $IsAdmin, $IsLoggedIn, $IsInMemberShip, $IsReadOnlyLib, $LegacyData, $CanHistory, $CanPublish, $CanAddTask, $CanBreakPending, $CanView, $CanEditSummary, $CanSeeProperty, $CanPreview, $CanDecline, $CanEditSumit, $CanEditCollections, $CanDeleteTask, $CanMetadataComplete, $CanEditProperties, $CanEditApprovals, $CanEditQLinks)
    {
      parent::__construct($Id);
      $this->CanApprove = $CanApprove;
      $this->CanAdd = $CanAdd;
      $this->CanEdit = $CanEdit;
      $this->CanDelete = $CanDelete;
      $this->CanRestore = $CanRestore;
      $this->CanAddToImageLib = $CanAddToImageLib;
      $this->CanAddToFileLib = $CanAddToFileLib;
      $this->CanOverwriteLib = $CanOverwriteLib;
      $this->CanAddToHyperlinkLib = $CanAddToHyperlinkLib;
      $this->CanAddToQuicklinkLib = $CanAddToQuicklinkLib;
      $this->CanAddFolders = $CanAddFolders;
      $this->CanEditFolders = $CanEditFolders;
      $this->CanDeleteFolders = $CanDeleteFolders;
      $this->CanTraverseFolders = $CanTraverseFolders;
      $this->CanCreateTask = $CanCreateTask;
      $this->CanRedirectTask = $CanRedirectTask;
      $this->CanDestructTask = $CanDestructTask;
      $this->IsReadOnly = $IsReadOnly;
      $this->IsCollections = $IsCollections;
      $this->IsAdmin = $IsAdmin;
      $this->IsLoggedIn = $IsLoggedIn;
      $this->IsInMemberShip = $IsInMemberShip;
      $this->IsReadOnlyLib = $IsReadOnlyLib;
      $this->LegacyData = $LegacyData;
      $this->CanHistory = $CanHistory;
      $this->CanPublish = $CanPublish;
      $this->CanAddTask = $CanAddTask;
      $this->CanBreakPending = $CanBreakPending;
      $this->CanView = $CanView;
      $this->CanEditSummary = $CanEditSummary;
      $this->CanSeeProperty = $CanSeeProperty;
      $this->CanPreview = $CanPreview;
      $this->CanDecline = $CanDecline;
      $this->CanEditSumit = $CanEditSumit;
      $this->CanEditCollections = $CanEditCollections;
      $this->CanDeleteTask = $CanDeleteTask;
      $this->CanMetadataComplete = $CanMetadataComplete;
      $this->CanEditProperties = $CanEditProperties;
      $this->CanEditApprovals = $CanEditApprovals;
      $this->CanEditQLinks = $CanEditQLinks;
    }

}
