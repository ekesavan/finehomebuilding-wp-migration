<?php

class GetFolderResponse
{

    /**
     * @var FolderData $GetFolderResult
     * @access public
     */
    public $GetFolderResult = null;

    /**
     * @param FolderData $GetFolderResult
     * @access public
     */
    public function __construct($GetFolderResult)
    {
      $this->GetFolderResult = $GetFolderResult;
    }

}
