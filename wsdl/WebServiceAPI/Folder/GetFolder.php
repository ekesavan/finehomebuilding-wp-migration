<?php

class GetFolder
{

    /**
     * @var int $FolderID
     * @access public
     */
    public $FolderID = null;

    /**
     * @param int $FolderID
     * @access public
     */
    public function __construct($FolderID)
    {
      $this->FolderID = $FolderID;
    }

}
