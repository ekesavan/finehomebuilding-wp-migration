<?php

class GetBusinessRules_x0020_by_x0020_String_x0020_RuleIDResponse
{

    /**
     * @var string $GetBusinessRules_x0020_by_x0020_String_x0020_RuleIDResult
     * @access public
     */
    public $GetBusinessRules_x0020_by_x0020_String_x0020_RuleIDResult = null;

    /**
     * @param string $GetBusinessRules_x0020_by_x0020_String_x0020_RuleIDResult
     * @access public
     */
    public function __construct($GetBusinessRules_x0020_by_x0020_String_x0020_RuleIDResult)
    {
      $this->GetBusinessRules_x0020_by_x0020_String_x0020_RuleIDResult = $GetBusinessRules_x0020_by_x0020_String_x0020_RuleIDResult;
    }

}
