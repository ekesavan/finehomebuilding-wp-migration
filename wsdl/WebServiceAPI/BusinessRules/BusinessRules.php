<?php

include_once('GetBusinessRules_x0020_By_x0020_Int_x0020_RuleID.php');
include_once('GetBusinessRules_x0020_By_x0020_Int_x0020_RuleIDResponse.php');
include_once('AuthenticationHeader.php');
include_once('RequestInfoParameters.php');
include_once('GetBusinessRules_x0020_by_x0020_String_x0020_RuleID.php');
include_once('GetBusinessRules_x0020_by_x0020_String_x0020_RuleIDResponse.php');

class BusinessRules extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'GetBusinessRules_x0020_By_x0020_Int_x0020_RuleID' => '\GetBusinessRules_x0020_By_x0020_Int_x0020_RuleID',
      'GetBusinessRules_x0020_By_x0020_Int_x0020_RuleIDResponse' => '\GetBusinessRules_x0020_By_x0020_Int_x0020_RuleIDResponse',
      'AuthenticationHeader' => '\AuthenticationHeader',
      'RequestInfoParameters' => '\RequestInfoParameters',
      'GetBusinessRules_x0020_by_x0020_String_x0020_RuleID' => '\GetBusinessRules_x0020_by_x0020_String_x0020_RuleID',
      'GetBusinessRules_x0020_by_x0020_String_x0020_RuleIDResponse' => '\GetBusinessRules_x0020_by_x0020_String_x0020_RuleIDResponse');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = 'BusinessRules.wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      
      parent::__construct($wsdl, $options);
    }

    /**
     * @param GetBusinessRules_x0020_By_x0020_Int_x0020_RuleID $parameters
     * @access public
     * @return GetBusinessRules_x0020_By_x0020_Int_x0020_RuleIDResponse
     */
    public function GetBusinessRules(GetBusinessRules_x0020_By_x0020_Int_x0020_RuleID $parameters)
    {
      return $this->__soapCall('GetBusinessRules', array($parameters));
    }

}
