<?php

class GetBusinessRules_x0020_by_x0020_String_x0020_RuleID
{

    /**
     * @var string $httpHost
     * @access public
     */
    public $httpHost = null;

    /**
     * @var string $urlRest
     * @access public
     */
    public $urlRest = null;

    /**
     * @var string $cookies
     * @access public
     */
    public $cookies = null;

    /**
     * @var string $formValues
     * @access public
     */
    public $formValues = null;

    /**
     * @var string $queryValues
     * @access public
     */
    public $queryValues = null;

    /**
     * @var string $serverValues
     * @access public
     */
    public $serverValues = null;

    /**
     * @var string $paramValues
     * @access public
     */
    public $paramValues = null;

    /**
     * @var string $RuleID
     * @access public
     */
    public $RuleID = null;

    /**
     * @param string $httpHost
     * @param string $urlRest
     * @param string $cookies
     * @param string $formValues
     * @param string $queryValues
     * @param string $serverValues
     * @param string $paramValues
     * @param string $RuleID
     * @access public
     */
    public function __construct($httpHost, $urlRest, $cookies, $formValues, $queryValues, $serverValues, $paramValues, $RuleID)
    {
      $this->httpHost = $httpHost;
      $this->urlRest = $urlRest;
      $this->cookies = $cookies;
      $this->formValues = $formValues;
      $this->queryValues = $queryValues;
      $this->serverValues = $serverValues;
      $this->paramValues = $paramValues;
      $this->RuleID = $RuleID;
    }

}
