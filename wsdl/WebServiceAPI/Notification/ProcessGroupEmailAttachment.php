<?php

class ProcessGroupEmailAttachment
{

    /**
     * @var int $communityGroupId
     * @access public
     */
    public $communityGroupId = null;

    /**
     * @var string $message
     * @access public
     */
    public $message = null;

    /**
     * @var string $fromEmail
     * @access public
     */
    public $fromEmail = null;

    /**
     * @var base64Binary $document
     * @access public
     */
    public $document = null;

    /**
     * @var string $documentName
     * @access public
     */
    public $documentName = null;

    /**
     * @param int $communityGroupId
     * @param string $message
     * @param string $fromEmail
     * @param base64Binary $document
     * @param string $documentName
     * @access public
     */
    public function __construct($communityGroupId, $message, $fromEmail, $document, $documentName)
    {
      $this->communityGroupId = $communityGroupId;
      $this->message = $message;
      $this->fromEmail = $fromEmail;
      $this->document = $document;
      $this->documentName = $documentName;
    }

}
