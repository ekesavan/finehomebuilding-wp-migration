<?php

class ProcessActivityReply
{

    /**
     * @var guid $activityGuid
     * @access public
     */
    public $activityGuid = null;

    /**
     * @var string $reply
     * @access public
     */
    public $reply = null;

    /**
     * @var string $fromEmail
     * @access public
     */
    public $fromEmail = null;

    /**
     * @param guid $activityGuid
     * @param string $reply
     * @param string $fromEmail
     * @access public
     */
    public function __construct($activityGuid, $reply, $fromEmail)
    {
      $this->activityGuid = $activityGuid;
      $this->reply = $reply;
      $this->fromEmail = $fromEmail;
    }

}
