<?php

include_once('BaseDataOfActivityCommentData.php');

class CmsDataOfActivityCommentData extends BaseDataOfActivityCommentData
{

    /**
     * @var int $Id
     * @access public
     */
    public $Id = null;

    /**
     * @param int $Id
     * @access public
     */
    public function __construct($Id)
    {
      $this->Id = $Id;
    }

}
