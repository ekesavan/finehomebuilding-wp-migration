<?php

class Send
{

    /**
     * @var ActivityData $activity
     * @access public
     */
    public $activity = null;

    /**
     * @param ActivityData $activity
     * @access public
     */
    public function __construct($activity)
    {
      $this->activity = $activity;
    }

}
