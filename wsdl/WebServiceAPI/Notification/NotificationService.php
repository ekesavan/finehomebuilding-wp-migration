<?php

include_once('Send.php');
include_once('ActivityData.php');
include_once('CmsLocalizedDataOfActivityData.php');
include_once('CmsDataOfActivityData.php');
include_once('BaseDataOfActivityData.php');
include_once('ActivityUserInfo.php');
include_once('ActivityCommentData.php');
include_once('CmsDataOfActivityCommentData.php');
include_once('BaseDataOfActivityCommentData.php');
include_once('SendResponse.php');
include_once('ProcessActivityReply.php');
include_once('ProcessActivityReplyResponse.php');
include_once('ProcessActivityReplyAttachment.php');
include_once('ProcessActivityReplyAttachmentResponse.php');
include_once('ProcessGroupEmail.php');
include_once('ProcessGroupEmailResponse.php');
include_once('ProcessGroupEmailAttachment.php');
include_once('ProcessGroupEmailAttachmentResponse.php');

class NotificationService extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'Send' => '\Send',
      'ActivityData' => '\ActivityData',
      'CmsLocalizedDataOfActivityData' => '\CmsLocalizedDataOfActivityData',
      'CmsDataOfActivityData' => '\CmsDataOfActivityData',
      'BaseDataOfActivityData' => '\BaseDataOfActivityData',
      'ActivityUserInfo' => '\ActivityUserInfo',
      'ActivityCommentData' => '\ActivityCommentData',
      'CmsDataOfActivityCommentData' => '\CmsDataOfActivityCommentData',
      'BaseDataOfActivityCommentData' => '\BaseDataOfActivityCommentData',
      'SendResponse' => '\SendResponse',
      'ProcessActivityReply' => '\ProcessActivityReply',
      'ProcessActivityReplyResponse' => '\ProcessActivityReplyResponse',
      'ProcessActivityReplyAttachment' => '\ProcessActivityReplyAttachment',
      'ProcessActivityReplyAttachmentResponse' => '\ProcessActivityReplyAttachmentResponse',
      'ProcessGroupEmail' => '\ProcessGroupEmail',
      'ProcessGroupEmailResponse' => '\ProcessGroupEmailResponse',
      'ProcessGroupEmailAttachment' => '\ProcessGroupEmailAttachment',
      'ProcessGroupEmailAttachmentResponse' => '\ProcessGroupEmailAttachmentResponse');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = 'Notification.wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      
      parent::__construct($wsdl, $options);
    }

    /**
     * @param Send $parameters
     * @access public
     * @return SendResponse
     */
    public function Send(Send $parameters)
    {
      return $this->__soapCall('Send', array($parameters));
    }

    /**
     * @param ProcessActivityReply $parameters
     * @access public
     * @return ProcessActivityReplyResponse
     */
    public function ProcessActivityReply(ProcessActivityReply $parameters)
    {
      return $this->__soapCall('ProcessActivityReply', array($parameters));
    }

    /**
     * @param ProcessActivityReplyAttachment $parameters
     * @access public
     * @return ProcessActivityReplyAttachmentResponse
     */
    public function ProcessActivityReplyAttachment(ProcessActivityReplyAttachment $parameters)
    {
      return $this->__soapCall('ProcessActivityReplyAttachment', array($parameters));
    }

    /**
     * @param ProcessGroupEmail $parameters
     * @access public
     * @return ProcessGroupEmailResponse
     */
    public function ProcessGroupEmail(ProcessGroupEmail $parameters)
    {
      return $this->__soapCall('ProcessGroupEmail', array($parameters));
    }

    /**
     * @param ProcessGroupEmailAttachment $parameters
     * @access public
     * @return ProcessGroupEmailAttachmentResponse
     */
    public function ProcessGroupEmailAttachment(ProcessGroupEmailAttachment $parameters)
    {
      return $this->__soapCall('ProcessGroupEmailAttachment', array($parameters));
    }

}
