<?php

class ActivityUserInfo
{

    /**
     * @var int $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var string $DisplayName
     * @access public
     */
    public $DisplayName = null;

    /**
     * @var string $Avatar
     * @access public
     */
    public $Avatar = null;

    /**
     * @param int $Id
     * @param string $DisplayName
     * @param string $Avatar
     * @access public
     */
    public function __construct($Id, $DisplayName, $Avatar)
    {
      $this->Id = $Id;
      $this->DisplayName = $DisplayName;
      $this->Avatar = $Avatar;
    }

}
