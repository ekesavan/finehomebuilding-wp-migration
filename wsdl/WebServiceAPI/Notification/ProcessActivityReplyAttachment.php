<?php

class ProcessActivityReplyAttachment
{

    /**
     * @var guid $activityGuid
     * @access public
     */
    public $activityGuid = null;

    /**
     * @var string $reply
     * @access public
     */
    public $reply = null;

    /**
     * @var string $fromEmail
     * @access public
     */
    public $fromEmail = null;

    /**
     * @var base64Binary $document
     * @access public
     */
    public $document = null;

    /**
     * @var string $documentName
     * @access public
     */
    public $documentName = null;

    /**
     * @param guid $activityGuid
     * @param string $reply
     * @param string $fromEmail
     * @param base64Binary $document
     * @param string $documentName
     * @access public
     */
    public function __construct($activityGuid, $reply, $fromEmail, $document, $documentName)
    {
      $this->activityGuid = $activityGuid;
      $this->reply = $reply;
      $this->fromEmail = $fromEmail;
      $this->document = $document;
      $this->documentName = $documentName;
    }

}
