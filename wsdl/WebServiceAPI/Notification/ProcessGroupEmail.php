<?php

class ProcessGroupEmail
{

    /**
     * @var int $communityGroupId
     * @access public
     */
    public $communityGroupId = null;

    /**
     * @var string $message
     * @access public
     */
    public $message = null;

    /**
     * @var string $fromEmail
     * @access public
     */
    public $fromEmail = null;

    /**
     * @param int $communityGroupId
     * @param string $message
     * @param string $fromEmail
     * @access public
     */
    public function __construct($communityGroupId, $message, $fromEmail)
    {
      $this->communityGroupId = $communityGroupId;
      $this->message = $message;
      $this->fromEmail = $fromEmail;
    }

}
