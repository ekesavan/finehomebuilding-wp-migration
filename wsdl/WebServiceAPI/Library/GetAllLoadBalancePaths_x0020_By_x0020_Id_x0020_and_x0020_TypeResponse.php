<?php

class GetAllLoadBalancePaths_x0020_By_x0020_Id_x0020_and_x0020_TypeResponse
{

    /**
     * @var LoadBalanceData[] $GetAllLoadBalancePaths_x0020_By_x0020_Id_x0020_and_x0020_TypeResult
     * @access public
     */
    public $GetAllLoadBalancePaths_x0020_By_x0020_Id_x0020_and_x0020_TypeResult = null;

    /**
     * @param LoadBalanceData[] $GetAllLoadBalancePaths_x0020_By_x0020_Id_x0020_and_x0020_TypeResult
     * @access public
     */
    public function __construct($GetAllLoadBalancePaths_x0020_By_x0020_Id_x0020_and_x0020_TypeResult)
    {
      $this->GetAllLoadBalancePaths_x0020_By_x0020_Id_x0020_and_x0020_TypeResult = $GetAllLoadBalancePaths_x0020_By_x0020_Id_x0020_and_x0020_TypeResult;
    }

}
