<?php

class LoadBalanceData
{

    /**
     * @var string $Path
     * @access public
     */
    public $Path = null;

    /**
     * @var int $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var string $Type
     * @access public
     */
    public $Type = null;

    /**
     * @var int $MakeRelative
     * @access public
     */
    public $MakeRelative = null;

    /**
     * @var int $LibTypeID
     * @access public
     */
    public $LibTypeID = null;

    /**
     * @param string $Path
     * @param int $Id
     * @param string $Type
     * @param int $MakeRelative
     * @param int $LibTypeID
     * @access public
     */
    public function __construct($Path, $Id, $Type, $MakeRelative, $LibTypeID)
    {
      $this->Path = $Path;
      $this->Id = $Id;
      $this->Type = $Type;
      $this->MakeRelative = $MakeRelative;
      $this->LibTypeID = $LibTypeID;
    }

}
