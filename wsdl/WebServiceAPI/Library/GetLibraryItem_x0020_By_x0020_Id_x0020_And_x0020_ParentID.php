<?php

class GetLibraryItem_x0020_By_x0020_Id_x0020_And_x0020_ParentID
{

    /**
     * @var int $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var int $ParentId
     * @access public
     */
    public $ParentId = null;

    /**
     * @param int $Id
     * @param int $ParentId
     * @access public
     */
    public function __construct($Id, $ParentId)
    {
      $this->Id = $Id;
      $this->ParentId = $ParentId;
    }

}
