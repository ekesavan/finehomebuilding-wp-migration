<?php

class GetLibraryItem_x0020_By_x0020_Id
{

    /**
     * @var int $LibID
     * @access public
     */
    public $LibID = null;

    /**
     * @param int $LibID
     * @access public
     */
    public function __construct($LibID)
    {
      $this->LibID = $LibID;
    }

}
