<?php

class GetLibraryItem_x0020_By_x0020_Id_x0020_And_x0020_ParentIDResponse
{

    /**
     * @var LibraryData $GetLibraryItem_x0020_By_x0020_Id_x0020_And_x0020_ParentIDResult
     * @access public
     */
    public $GetLibraryItem_x0020_By_x0020_Id_x0020_And_x0020_ParentIDResult = null;

    /**
     * @param LibraryData $GetLibraryItem_x0020_By_x0020_Id_x0020_And_x0020_ParentIDResult
     * @access public
     */
    public function __construct($GetLibraryItem_x0020_By_x0020_Id_x0020_And_x0020_ParentIDResult)
    {
      $this->GetLibraryItem_x0020_By_x0020_Id_x0020_And_x0020_ParentIDResult = $GetLibraryItem_x0020_By_x0020_Id_x0020_And_x0020_ParentIDResult;
    }

}
