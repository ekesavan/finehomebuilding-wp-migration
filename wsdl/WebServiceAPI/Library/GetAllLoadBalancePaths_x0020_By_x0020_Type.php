<?php

class GetAllLoadBalancePaths_x0020_By_x0020_Type
{

    /**
     * @var string $Type
     * @access public
     */
    public $Type = null;

    /**
     * @param string $Type
     * @access public
     */
    public function __construct($Type)
    {
      $this->Type = $Type;
    }

}
