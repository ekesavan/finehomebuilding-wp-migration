<?php

include_once('DeleteLoadBalanceItem.php');
include_once('DeleteLoadBalanceItemResponse.php');
include_once('AuthenticationHeader.php');
include_once('RequestInfoParameters.php');
include_once('GetAllLoadBalancePaths_x0020_By_x0020_Type.php');
include_once('GetAllLoadBalancePaths_x0020_By_x0020_TypeResponse.php');
include_once('LoadBalanceData.php');
include_once('GetAllLoadBalancePaths_x0020_By_x0020_Id_x0020_and_x0020_Type.php');
include_once('GetAllLoadBalancePaths_x0020_By_x0020_Id_x0020_and_x0020_TypeResponse.php');
include_once('GetLibraryItem_x0020_By_x0020_Id_x0020_And_x0020_ParentID.php');
include_once('GetLibraryItem_x0020_By_x0020_Id_x0020_And_x0020_ParentIDResponse.php');
include_once('LibraryData.php');
include_once('LibraryBaseData.php');
include_once('CmsDataOfLibraryBaseData.php');
include_once('BaseDataOfLibraryBaseData.php');
include_once('LibraryType.php');
include_once('CMSContentSubtype.php');
include_once('ContentMetaData.php');
include_once('CmsDataOfContentMetaData.php');
include_once('BaseDataOfContentMetaData.php');
include_once('ContentMetadataDataType.php');
include_once('ContentMetadataType.php');
include_once('TaxonomyBaseData.php');
include_once('CmsDataOfTaxonomyBaseData.php');
include_once('BaseDataOfTaxonomyBaseData.php');
include_once('TaxonomyType.php');
include_once('GetLibraryItem_x0020_By_x0020_Id.php');
include_once('GetLibraryItem_x0020_By_x0020_IdResponse.php');
include_once('GetLibrarySettings.php');
include_once('GetLibrarySettingsResponse.php');
include_once('LibraryConfigData.php');
include_once('GetLibraryTypes.php');
include_once('GetLibraryTypesResponse.php');
include_once('LibraryTypeData.php');
include_once('GetAllChildLibItems.php');
include_once('GetAllChildLibItemsResponse.php');
include_once('GetLoadBalancePath.php');
include_once('GetLoadBalancePathResponse.php');

class Library extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'DeleteLoadBalanceItem' => '\DeleteLoadBalanceItem',
      'DeleteLoadBalanceItemResponse' => '\DeleteLoadBalanceItemResponse',
      'AuthenticationHeader' => '\AuthenticationHeader',
      'RequestInfoParameters' => '\RequestInfoParameters',
      'GetAllLoadBalancePaths_x0020_By_x0020_Type' => '\GetAllLoadBalancePaths_x0020_By_x0020_Type',
      'GetAllLoadBalancePaths_x0020_By_x0020_TypeResponse' => '\GetAllLoadBalancePaths_x0020_By_x0020_TypeResponse',
      'LoadBalanceData' => '\LoadBalanceData',
      'GetAllLoadBalancePaths_x0020_By_x0020_Id_x0020_and_x0020_Type' => '\GetAllLoadBalancePaths_x0020_By_x0020_Id_x0020_and_x0020_Type',
      'GetAllLoadBalancePaths_x0020_By_x0020_Id_x0020_and_x0020_TypeResponse' => '\GetAllLoadBalancePaths_x0020_By_x0020_Id_x0020_and_x0020_TypeResponse',
      'GetLibraryItem_x0020_By_x0020_Id_x0020_And_x0020_ParentID' => '\GetLibraryItem_x0020_By_x0020_Id_x0020_And_x0020_ParentID',
      'GetLibraryItem_x0020_By_x0020_Id_x0020_And_x0020_ParentIDResponse' => '\GetLibraryItem_x0020_By_x0020_Id_x0020_And_x0020_ParentIDResponse',
      'LibraryData' => '\LibraryData',
      'LibraryBaseData' => '\LibraryBaseData',
      'CmsDataOfLibraryBaseData' => '\CmsDataOfLibraryBaseData',
      'BaseDataOfLibraryBaseData' => '\BaseDataOfLibraryBaseData',
      'ContentMetaData' => '\ContentMetaData',
      'CmsDataOfContentMetaData' => '\CmsDataOfContentMetaData',
      'BaseDataOfContentMetaData' => '\BaseDataOfContentMetaData',
      'TaxonomyBaseData' => '\TaxonomyBaseData',
      'CmsDataOfTaxonomyBaseData' => '\CmsDataOfTaxonomyBaseData',
      'BaseDataOfTaxonomyBaseData' => '\BaseDataOfTaxonomyBaseData',
      'GetLibraryItem_x0020_By_x0020_Id' => '\GetLibraryItem_x0020_By_x0020_Id',
      'GetLibraryItem_x0020_By_x0020_IdResponse' => '\GetLibraryItem_x0020_By_x0020_IdResponse',
      'GetLibrarySettings' => '\GetLibrarySettings',
      'GetLibrarySettingsResponse' => '\GetLibrarySettingsResponse',
      'LibraryConfigData' => '\LibraryConfigData',
      'GetLibraryTypes' => '\GetLibraryTypes',
      'GetLibraryTypesResponse' => '\GetLibraryTypesResponse',
      'LibraryTypeData' => '\LibraryTypeData',
      'GetAllChildLibItems' => '\GetAllChildLibItems',
      'GetAllChildLibItemsResponse' => '\GetAllChildLibItemsResponse',
      'GetLoadBalancePath' => '\GetLoadBalancePath',
      'GetLoadBalancePathResponse' => '\GetLoadBalancePathResponse');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = 'Library.wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      
      parent::__construct($wsdl, $options);
    }

    /**
     * @param DeleteLoadBalanceItem $parameters
     * @access public
     * @return DeleteLoadBalanceItemResponse
     */
    public function DeleteLoadBalanceItem(DeleteLoadBalanceItem $parameters)
    {
      return $this->__soapCall('DeleteLoadBalanceItem', array($parameters));
    }

    /**
     * @param GetAllLoadBalancePaths_x0020_By_x0020_Type $parameters
     * @access public
     * @return GetAllLoadBalancePaths_x0020_By_x0020_TypeResponse
     */
    public function GetAllLoadBalancePaths(GetAllLoadBalancePaths_x0020_By_x0020_Type $parameters)
    {
      return $this->__soapCall('GetAllLoadBalancePaths', array($parameters));
    }

    /**
     * @param GetLibraryItem_x0020_By_x0020_Id_x0020_And_x0020_ParentID $parameters
     * @access public
     * @return GetLibraryItem_x0020_By_x0020_Id_x0020_And_x0020_ParentIDResponse
     */
    public function GetLibraryItem(GetLibraryItem_x0020_By_x0020_Id_x0020_And_x0020_ParentID $parameters)
    {
      return $this->__soapCall('GetLibraryItem', array($parameters));
    }

    /**
     * @param GetLibrarySettings $parameters
     * @access public
     * @return GetLibrarySettingsResponse
     */
    public function GetLibrarySettings(GetLibrarySettings $parameters)
    {
      return $this->__soapCall('GetLibrarySettings', array($parameters));
    }

    /**
     * @param GetLibraryTypes $parameters
     * @access public
     * @return GetLibraryTypesResponse
     */
    public function GetLibraryTypes(GetLibraryTypes $parameters)
    {
      return $this->__soapCall('GetLibraryTypes', array($parameters));
    }

    /**
     * Loads all of the library items by type
     *
     * @param GetAllChildLibItems $parameters
     * @access public
     * @return GetAllChildLibItemsResponse
     */
    public function GetAllChildLibItems(GetAllChildLibItems $parameters)
    {
      return $this->__soapCall('GetAllChildLibItems', array($parameters));
    }

    /**
     * @param GetLoadBalancePath $parameters
     * @access public
     * @return GetLoadBalancePathResponse
     */
    public function GetLoadBalancePath(GetLoadBalancePath $parameters)
    {
      return $this->__soapCall('GetLoadBalancePath', array($parameters));
    }

}
