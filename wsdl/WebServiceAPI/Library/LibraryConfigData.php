<?php

class LibraryConfigData
{

    /**
     * @var string $RelativeImages
     * @access public
     */
    public $RelativeImages = null;

    /**
     * @var string $RelativeFiles
     * @access public
     */
    public $RelativeFiles = null;

    /**
     * @var string $ImageExtensions
     * @access public
     */
    public $ImageExtensions = null;

    /**
     * @var string $ImageDirectory
     * @access public
     */
    public $ImageDirectory = null;

    /**
     * @var string $FileExtensions
     * @access public
     */
    public $FileExtensions = null;

    /**
     * @var string $FileDirectory
     * @access public
     */
    public $FileDirectory = null;

    /**
     * @param string $RelativeImages
     * @param string $RelativeFiles
     * @param string $ImageExtensions
     * @param string $ImageDirectory
     * @param string $FileExtensions
     * @param string $FileDirectory
     * @access public
     */
    public function __construct($RelativeImages, $RelativeFiles, $ImageExtensions, $ImageDirectory, $FileExtensions, $FileDirectory)
    {
      $this->RelativeImages = $RelativeImages;
      $this->RelativeFiles = $RelativeFiles;
      $this->ImageExtensions = $ImageExtensions;
      $this->ImageDirectory = $ImageDirectory;
      $this->FileExtensions = $FileExtensions;
      $this->FileDirectory = $FileDirectory;
    }

}
