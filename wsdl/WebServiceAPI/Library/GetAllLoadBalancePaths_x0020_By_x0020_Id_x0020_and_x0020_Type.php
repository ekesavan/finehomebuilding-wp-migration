<?php

class GetAllLoadBalancePaths_x0020_By_x0020_Id_x0020_and_x0020_Type
{

    /**
     * @var int $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var string $Type
     * @access public
     */
    public $Type = null;

    /**
     * @param int $Id
     * @param string $Type
     * @access public
     */
    public function __construct($Id, $Type)
    {
      $this->Id = $Id;
      $this->Type = $Type;
    }

}
