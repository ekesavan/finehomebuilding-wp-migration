<?php

class GetLoadBalancePathResponse
{

    /**
     * @var LoadBalanceData $GetLoadBalancePathResult
     * @access public
     */
    public $GetLoadBalancePathResult = null;

    /**
     * @param LoadBalanceData $GetLoadBalancePathResult
     * @access public
     */
    public function __construct($GetLoadBalancePathResult)
    {
      $this->GetLoadBalancePathResult = $GetLoadBalancePathResult;
    }

}
