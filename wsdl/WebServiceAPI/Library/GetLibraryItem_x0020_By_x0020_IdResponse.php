<?php

class GetLibraryItem_x0020_By_x0020_IdResponse
{

    /**
     * @var LibraryData $GetLibraryItem_x0020_By_x0020_IdResult
     * @access public
     */
    public $GetLibraryItem_x0020_By_x0020_IdResult = null;

    /**
     * @param LibraryData $GetLibraryItem_x0020_By_x0020_IdResult
     * @access public
     */
    public function __construct($GetLibraryItem_x0020_By_x0020_IdResult)
    {
      $this->GetLibraryItem_x0020_By_x0020_IdResult = $GetLibraryItem_x0020_By_x0020_IdResult;
    }

}
