<?php

class GetLibraryTypes
{

    /**
     * @var int $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var string $OrderBy
     * @access public
     */
    public $OrderBy = null;

    /**
     * @param int $Id
     * @param string $OrderBy
     * @access public
     */
    public function __construct($Id, $OrderBy)
    {
      $this->Id = $Id;
      $this->OrderBy = $OrderBy;
    }

}
