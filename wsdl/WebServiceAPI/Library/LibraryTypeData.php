<?php

class LibraryTypeData
{

    /**
     * @var int $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var string $Name
     * @access public
     */
    public $Name = null;

    /**
     * @var string $Extensions
     * @access public
     */
    public $Extensions = null;

    /**
     * @var string $RelativeDirectory
     * @access public
     */
    public $RelativeDirectory = null;

    /**
     * @param int $Id
     * @param string $Name
     * @param string $Extensions
     * @param string $RelativeDirectory
     * @access public
     */
    public function __construct($Id, $Name, $Extensions, $RelativeDirectory)
    {
      $this->Id = $Id;
      $this->Name = $Name;
      $this->Extensions = $Extensions;
      $this->RelativeDirectory = $RelativeDirectory;
    }

}
