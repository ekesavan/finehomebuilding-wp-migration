<?php

class GetAllChildLibItemsResponse
{

    /**
     * @var LibraryData[] $GetAllChildLibItemsResult
     * @access public
     */
    public $GetAllChildLibItemsResult = null;

    /**
     * @var int $totalPages
     * @access public
     */
    public $totalPages = null;

    /**
     * @param LibraryData[] $GetAllChildLibItemsResult
     * @param int $totalPages
     * @access public
     */
    public function __construct($GetAllChildLibItemsResult, $totalPages)
    {
      $this->GetAllChildLibItemsResult = $GetAllChildLibItemsResult;
      $this->totalPages = $totalPages;
    }

}
