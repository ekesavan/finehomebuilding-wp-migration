<?php

include_once('CmsDataOfLibraryBaseData.php');

class LibraryBaseData extends CmsDataOfLibraryBaseData
{

    /**
     * @var int $TypeId
     * @access public
     */
    public $TypeId = null;

    /**
     * @var string $Type
     * @access public
     */
    public $Type = null;

    /**
     * @var int $LanguageId
     * @access public
     */
    public $LanguageId = null;

    /**
     * @var string $Title
     * @access public
     */
    public $Title = null;

    /**
     * @var string $FileName
     * @access public
     */
    public $FileName = null;

    /**
     * @var string $StagingFileName
     * @access public
     */
    public $StagingFileName = null;

    /**
     * @var base64Binary $File
     * @access public
     */
    public $File = null;

    /**
     * @var LibraryType $LibraryType
     * @access public
     */
    public $LibraryType = null;

    /**
     * @param int $Id
     * @param int $TypeId
     * @param string $Type
     * @param int $LanguageId
     * @param string $Title
     * @param string $FileName
     * @param string $StagingFileName
     * @param base64Binary $File
     * @param LibraryType $LibraryType
     * @access public
     */
    public function __construct($Id, $TypeId, $Type, $LanguageId, $Title, $FileName, $StagingFileName, $File, $LibraryType)
    {
      parent::__construct($Id);
      $this->TypeId = $TypeId;
      $this->Type = $Type;
      $this->LanguageId = $LanguageId;
      $this->Title = $Title;
      $this->FileName = $FileName;
      $this->StagingFileName = $StagingFileName;
      $this->File = $File;
      $this->LibraryType = $LibraryType;
    }

}
