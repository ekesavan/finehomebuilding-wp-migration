<?php

include_once('LibraryBaseData.php');

class LibraryData extends LibraryBaseData
{

    /**
     * @var string $FolderName
     * @access public
     */
    public $FolderName = null;

    /**
     * @var string $EditorFirstName
     * @access public
     */
    public $EditorFirstName = null;

    /**
     * @var string $EditorLastName
     * @access public
     */
    public $EditorLastName = null;

    /**
     * @var int $UserId
     * @access public
     */
    public $UserId = null;

    /**
     * @var string $LastEditDate
     * @access public
     */
    public $LastEditDate = null;

    /**
     * @var string $DisplayLastEditDate
     * @access public
     */
    public $DisplayLastEditDate = null;

    /**
     * @var dateTime $DateCreated
     * @access public
     */
    public $DateCreated = null;

    /**
     * @var string $DisplayDateCreated
     * @access public
     */
    public $DisplayDateCreated = null;

    /**
     * @var int $ContentId
     * @access public
     */
    public $ContentId = null;

    /**
     * @var int $ParentId
     * @access public
     */
    public $ParentId = null;

    /**
     * @var int $Relative
     * @access public
     */
    public $Relative = null;

    /**
     * @var int $ContentType
     * @access public
     */
    public $ContentType = null;

    /**
     * @var CMSContentSubtype $ContentSubType
     * @access public
     */
    public $ContentSubType = null;

    /**
     * @var boolean $IsPrivate
     * @access public
     */
    public $IsPrivate = null;

    /**
     * @var string $Teaser
     * @access public
     */
    public $Teaser = null;

    /**
     * @var ContentMetaData[] $MetaData
     * @access public
     */
    public $MetaData = null;

    /**
     * @var TaxonomyBaseData[] $Taxonomies
     * @access public
     */
    public $Taxonomies = null;

    /**
     * @var int $OriginalLibraryId
     * @access public
     */
    public $OriginalLibraryId = null;

    /**
     * @param int $Id
     * @param int $TypeId
     * @param string $Type
     * @param int $LanguageId
     * @param string $Title
     * @param string $FileName
     * @param string $StagingFileName
     * @param base64Binary $File
     * @param LibraryType $LibraryType
     * @param string $FolderName
     * @param string $EditorFirstName
     * @param string $EditorLastName
     * @param int $UserId
     * @param string $LastEditDate
     * @param string $DisplayLastEditDate
     * @param dateTime $DateCreated
     * @param string $DisplayDateCreated
     * @param int $ContentId
     * @param int $ParentId
     * @param int $Relative
     * @param int $ContentType
     * @param CMSContentSubtype $ContentSubType
     * @param boolean $IsPrivate
     * @param string $Teaser
     * @param ContentMetaData[] $MetaData
     * @param TaxonomyBaseData[] $Taxonomies
     * @param int $OriginalLibraryId
     * @access public
     */
    public function __construct($Id, $TypeId, $Type, $LanguageId, $Title, $FileName, $StagingFileName, $File, $LibraryType, $FolderName, $EditorFirstName, $EditorLastName, $UserId, $LastEditDate, $DisplayLastEditDate, $DateCreated, $DisplayDateCreated, $ContentId, $ParentId, $Relative, $ContentType, $ContentSubType, $IsPrivate, $Teaser, $MetaData, $Taxonomies, $OriginalLibraryId)
    {
      parent::__construct($Id, $TypeId, $Type, $LanguageId, $Title, $FileName, $StagingFileName, $File, $LibraryType);
      $this->FolderName = $FolderName;
      $this->EditorFirstName = $EditorFirstName;
      $this->EditorLastName = $EditorLastName;
      $this->UserId = $UserId;
      $this->LastEditDate = $LastEditDate;
      $this->DisplayLastEditDate = $DisplayLastEditDate;
      $this->DateCreated = $DateCreated;
      $this->DisplayDateCreated = $DisplayDateCreated;
      $this->ContentId = $ContentId;
      $this->ParentId = $ParentId;
      $this->Relative = $Relative;
      $this->ContentType = $ContentType;
      $this->ContentSubType = $ContentSubType;
      $this->IsPrivate = $IsPrivate;
      $this->Teaser = $Teaser;
      $this->MetaData = $MetaData;
      $this->Taxonomies = $Taxonomies;
      $this->OriginalLibraryId = $OriginalLibraryId;
    }

}
