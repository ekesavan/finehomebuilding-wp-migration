<?php

include_once('BaseDataOfLibraryBaseData.php');

class CmsDataOfLibraryBaseData extends BaseDataOfLibraryBaseData
{

    /**
     * @var int $Id
     * @access public
     */
    public $Id = null;

    /**
     * @param int $Id
     * @access public
     */
    public function __construct($Id)
    {
      $this->Id = $Id;
    }

}
