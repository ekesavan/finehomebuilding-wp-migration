<?php

class GetLibrarySettingsResponse
{

    /**
     * @var LibraryConfigData $GetLibrarySettingsResult
     * @access public
     */
    public $GetLibrarySettingsResult = null;

    /**
     * @param LibraryConfigData $GetLibrarySettingsResult
     * @access public
     */
    public function __construct($GetLibrarySettingsResult)
    {
      $this->GetLibrarySettingsResult = $GetLibrarySettingsResult;
    }

}
