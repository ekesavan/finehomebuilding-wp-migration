<?php

class GetAllChildLibItems
{

    /**
     * @var string $Type
     * @access public
     */
    public $Type = null;

    /**
     * @var int $ParentId
     * @access public
     */
    public $ParentId = null;

    /**
     * @var string $OrderBy
     * @access public
     */
    public $OrderBy = null;

    /**
     * @var int $currentPageNum
     * @access public
     */
    public $currentPageNum = null;

    /**
     * @var int $pageSize
     * @access public
     */
    public $pageSize = null;

    /**
     * @var int $totalPages
     * @access public
     */
    public $totalPages = null;

    /**
     * @param string $Type
     * @param int $ParentId
     * @param string $OrderBy
     * @param int $currentPageNum
     * @param int $pageSize
     * @param int $totalPages
     * @access public
     */
    public function __construct($Type, $ParentId, $OrderBy, $currentPageNum, $pageSize, $totalPages)
    {
      $this->Type = $Type;
      $this->ParentId = $ParentId;
      $this->OrderBy = $OrderBy;
      $this->currentPageNum = $currentPageNum;
      $this->pageSize = $pageSize;
      $this->totalPages = $totalPages;
    }

}
