<?php

class LibraryType
{
    const __default = 'images';
    const images = 'images';
    const files = 'files';
    const hyperlinks = 'hyperlinks';
    const quicklinks = 'quicklinks';
    const forms = 'forms';


}
