<?php

class GetLibraryTypesResponse
{

    /**
     * @var LibraryTypeData[] $GetLibraryTypesResult
     * @access public
     */
    public $GetLibraryTypesResult = null;

    /**
     * @param LibraryTypeData[] $GetLibraryTypesResult
     * @access public
     */
    public function __construct($GetLibraryTypesResult)
    {
      $this->GetLibraryTypesResult = $GetLibraryTypesResult;
    }

}
