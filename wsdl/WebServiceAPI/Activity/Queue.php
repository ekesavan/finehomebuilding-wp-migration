<?php

class Queue
{

    /**
     * @var ActivityData $item
     * @access public
     */
    public $item = null;

    /**
     * @param ActivityData $item
     * @access public
     */
    public function __construct($item)
    {
      $this->item = $item;
    }

}
