<?php

include_once('CmsDataOfActivityCommentData.php');

class ActivityCommentData extends CmsDataOfActivityCommentData
{

    /**
     * @var int $ActivityId
     * @access public
     */
    public $ActivityId = null;

    /**
     * @var string $Comment
     * @access public
     */
    public $Comment = null;

    /**
     * @var int $UserId
     * @access public
     */
    public $UserId = null;

    /**
     * @var string $UserAvatar
     * @access public
     */
    public $UserAvatar = null;

    /**
     * @var string $UserDisplayName
     * @access public
     */
    public $UserDisplayName = null;

    /**
     * @var dateTime $DateCreated
     * @access public
     */
    public $DateCreated = null;

    /**
     * @var string $TimeLapse
     * @access public
     */
    public $TimeLapse = null;

    /**
     * @var boolean $CanDelete
     * @access public
     */
    public $CanDelete = null;

    /**
     * @param int $Id
     * @param int $ActivityId
     * @param string $Comment
     * @param int $UserId
     * @param string $UserAvatar
     * @param string $UserDisplayName
     * @param dateTime $DateCreated
     * @param string $TimeLapse
     * @param boolean $CanDelete
     * @access public
     */
    public function __construct($Id, $ActivityId, $Comment, $UserId, $UserAvatar, $UserDisplayName, $DateCreated, $TimeLapse, $CanDelete)
    {
      parent::__construct($Id);
      $this->ActivityId = $ActivityId;
      $this->Comment = $Comment;
      $this->UserId = $UserId;
      $this->UserAvatar = $UserAvatar;
      $this->UserDisplayName = $UserDisplayName;
      $this->DateCreated = $DateCreated;
      $this->TimeLapse = $TimeLapse;
      $this->CanDelete = $CanDelete;
    }

}
