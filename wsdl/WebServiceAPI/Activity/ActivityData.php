<?php

include_once('CmsLocalizedDataOfActivityData.php');

class ActivityData extends CmsLocalizedDataOfActivityData
{

    /**
     * @var int $ActivityTypeId
     * @access public
     */
    public $ActivityTypeId = null;

    /**
     * @var int $ObjectId
     * @access public
     */
    public $ObjectId = null;

    /**
     * @var ActivityUserInfo $ActionUser
     * @access public
     */
    public $ActionUser = null;

    /**
     * @var int $ScopeObjectId
     * @access public
     */
    public $ScopeObjectId = null;

    /**
     * @var string $Message
     * @access public
     */
    public $Message = null;

    /**
     * @var dateTime $Date
     * @access public
     */
    public $Date = null;

    /**
     * @var int $SiteId
     * @access public
     */
    public $SiteId = null;

    /**
     * @var string $TimeLapse
     * @access public
     */
    public $TimeLapse = null;

    /**
     * @var int $CommentCount
     * @access public
     */
    public $CommentCount = null;

    /**
     * @var ActivityCommentData[] $Comments
     * @access public
     */
    public $Comments = null;

    /**
     * @var guid $UniqueId
     * @access public
     */
    public $UniqueId = null;

    /**
     * @var boolean $CanComment
     * @access public
     */
    public $CanComment = null;

    /**
     * @param int $Id
     * @param int $LanguageId
     * @param int $ActivityTypeId
     * @param int $ObjectId
     * @param ActivityUserInfo $ActionUser
     * @param int $ScopeObjectId
     * @param string $Message
     * @param dateTime $Date
     * @param int $SiteId
     * @param string $TimeLapse
     * @param int $CommentCount
     * @param ActivityCommentData[] $Comments
     * @param guid $UniqueId
     * @param boolean $CanComment
     * @access public
     */
    public function __construct($Id, $LanguageId, $ActivityTypeId, $ObjectId, $ActionUser, $ScopeObjectId, $Message, $Date, $SiteId, $TimeLapse, $CommentCount, $Comments, $UniqueId, $CanComment)
    {
      parent::__construct($Id, $LanguageId);
      $this->ActivityTypeId = $ActivityTypeId;
      $this->ObjectId = $ObjectId;
      $this->ActionUser = $ActionUser;
      $this->ScopeObjectId = $ScopeObjectId;
      $this->Message = $Message;
      $this->Date = $Date;
      $this->SiteId = $SiteId;
      $this->TimeLapse = $TimeLapse;
      $this->CommentCount = $CommentCount;
      $this->Comments = $Comments;
      $this->UniqueId = $UniqueId;
      $this->CanComment = $CanComment;
    }

}
