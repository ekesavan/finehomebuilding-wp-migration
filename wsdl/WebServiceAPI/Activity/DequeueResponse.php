<?php

class DequeueResponse
{

    /**
     * @var ActivityData $DequeueResult
     * @access public
     */
    public $DequeueResult = null;

    /**
     * @param ActivityData $DequeueResult
     * @access public
     */
    public function __construct($DequeueResult)
    {
      $this->DequeueResult = $DequeueResult;
    }

}
