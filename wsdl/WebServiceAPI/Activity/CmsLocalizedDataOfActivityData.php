<?php

include_once('CmsDataOfActivityData.php');

class CmsLocalizedDataOfActivityData extends CmsDataOfActivityData
{

    /**
     * @var int $LanguageId
     * @access public
     */
    public $LanguageId = null;

    /**
     * @param int $Id
     * @param int $LanguageId
     * @access public
     */
    public function __construct($Id, $LanguageId)
    {
      parent::__construct($Id);
      $this->LanguageId = $LanguageId;
    }

}
