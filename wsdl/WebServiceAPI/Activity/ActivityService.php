<?php

include_once('Queue.php');
include_once('ActivityData.php');
include_once('CmsLocalizedDataOfActivityData.php');
include_once('CmsDataOfActivityData.php');
include_once('BaseDataOfActivityData.php');
include_once('ActivityUserInfo.php');
include_once('ActivityCommentData.php');
include_once('CmsDataOfActivityCommentData.php');
include_once('BaseDataOfActivityCommentData.php');
include_once('QueueResponse.php');
include_once('Dequeue.php');
include_once('DequeueResponse.php');

class ActivityService extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'Queue' => '\Queue',
      'ActivityData' => '\ActivityData',
      'CmsLocalizedDataOfActivityData' => '\CmsLocalizedDataOfActivityData',
      'CmsDataOfActivityData' => '\CmsDataOfActivityData',
      'BaseDataOfActivityData' => '\BaseDataOfActivityData',
      'ActivityUserInfo' => '\ActivityUserInfo',
      'ActivityCommentData' => '\ActivityCommentData',
      'CmsDataOfActivityCommentData' => '\CmsDataOfActivityCommentData',
      'BaseDataOfActivityCommentData' => '\BaseDataOfActivityCommentData',
      'QueueResponse' => '\QueueResponse',
      'Dequeue' => '\Dequeue',
      'DequeueResponse' => '\DequeueResponse');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = 'Activity.wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      
      parent::__construct($wsdl, $options);
    }

    /**
     * @param Queue $parameters
     * @access public
     * @return QueueResponse
     */
    public function Queue(Queue $parameters)
    {
      return $this->__soapCall('Queue', array($parameters));
    }

    /**
     * @param Dequeue $parameters
     * @access public
     * @return DequeueResponse
     */
    public function Dequeue(Dequeue $parameters)
    {
      return $this->__soapCall('Dequeue', array($parameters));
    }

}
