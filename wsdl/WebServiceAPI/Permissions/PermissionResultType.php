<?php

class PermissionResultType
{
    const __default = 'Common';
    const Common = 'Common';
    const All = 'All';
    const Folder = 'Folder';
    const Content = 'Content';
    const Task = 'Task';


}
