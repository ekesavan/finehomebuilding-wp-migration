<?php

class CMSObjectTypes
{
    const __default = 'Content';
    const Content = 'Content';
    const Folder = 'Folder';
    const User = 'User';
    const UserGroup = 'UserGroup';
    const Library = 'Library';
    const Collection = 'Collection';
    const Menu = 'Menu';
    const Calendar = 'Calendar';
    const Subscription = 'Subscription';
    const Notification = 'Notification';
    const Blog = 'Blog';
    const BlogPost = 'BlogPost';
    const BlogComment = 'BlogComment';
    const DiscussionBoard = 'DiscussionBoard';
    const DiscussionForum = 'DiscussionForum';
    const RestrictIP = 'RestrictIP';
    const ReplaceWord = 'ReplaceWord';
    const UserRank = 'UserRank';
    const DiscussionTopic = 'DiscussionTopic';
    const CommunityGroup = 'CommunityGroup';
    const TaxonomyNode = 'TaxonomyNode';
    const CatalogEntry = 'CatalogEntry';
    const MessageBoard = 'MessageBoard';
    const CalendarEvent = 'CalendarEvent';
    const MicroMessage = 'MicroMessage';
    const Subscriber = 'Subscriber';


}
