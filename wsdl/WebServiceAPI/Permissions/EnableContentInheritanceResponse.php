<?php

class EnableContentInheritanceResponse
{

    /**
     * @var boolean $EnableContentInheritanceResult
     * @access public
     */
    public $EnableContentInheritanceResult = null;

    /**
     * @param boolean $EnableContentInheritanceResult
     * @access public
     */
    public function __construct($EnableContentInheritanceResult)
    {
      $this->EnableContentInheritanceResult = $EnableContentInheritanceResult;
    }

}
