<?php

class DisableFolderInheritanceResponse
{

    /**
     * @var boolean $DisableFolderInheritanceResult
     * @access public
     */
    public $DisableFolderInheritanceResult = null;

    /**
     * @param boolean $DisableFolderInheritanceResult
     * @access public
     */
    public function __construct($DisableFolderInheritanceResult)
    {
      $this->DisableFolderInheritanceResult = $DisableFolderInheritanceResult;
    }

}
