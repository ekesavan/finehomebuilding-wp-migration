<?php

class GetUserPermissions
{

    /**
     * @var int $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var string $ItemType
     * @access public
     */
    public $ItemType = null;

    /**
     * @var int $UserId
     * @access public
     */
    public $UserId = null;

    /**
     * @var string $UserGroupList
     * @access public
     */
    public $UserGroupList = null;

    /**
     * @param int $Id
     * @param string $ItemType
     * @param int $UserId
     * @param string $UserGroupList
     * @access public
     */
    public function __construct($Id, $ItemType, $UserId, $UserGroupList)
    {
      $this->Id = $Id;
      $this->ItemType = $ItemType;
      $this->UserId = $UserId;
      $this->UserGroupList = $UserGroupList;
    }

}
