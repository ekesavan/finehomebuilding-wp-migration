<?php

class LoadPermissionsResponse
{

    /**
     * @var PermissionData $LoadPermissionsResult
     * @access public
     */
    public $LoadPermissionsResult = null;

    /**
     * @param PermissionData $LoadPermissionsResult
     * @access public
     */
    public function __construct($LoadPermissionsResult)
    {
      $this->LoadPermissionsResult = $LoadPermissionsResult;
    }

}
