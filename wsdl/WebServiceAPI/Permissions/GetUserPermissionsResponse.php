<?php

class GetUserPermissionsResponse
{

    /**
     * @var UserPermissionData[] $GetUserPermissionsResult
     * @access public
     */
    public $GetUserPermissionsResult = null;

    /**
     * @param UserPermissionData[] $GetUserPermissionsResult
     * @access public
     */
    public function __construct($GetUserPermissionsResult)
    {
      $this->GetUserPermissionsResult = $GetUserPermissionsResult;
    }

}
