<?php

class GetUserPermissions_x0020_By_x0020_PermissionType
{

    /**
     * @var int $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var string $ItemType
     * @access public
     */
    public $ItemType = null;

    /**
     * @var int $UserId
     * @access public
     */
    public $UserId = null;

    /**
     * @var string $UserGroupList
     * @access public
     */
    public $UserGroupList = null;

    /**
     * @var PermissionUserType $PermissionType
     * @access public
     */
    public $PermissionType = null;

    /**
     * @var PermissionRequestType $PermissionRequest
     * @access public
     */
    public $PermissionRequest = null;

    /**
     * @param int $Id
     * @param string $ItemType
     * @param int $UserId
     * @param string $UserGroupList
     * @param PermissionUserType $PermissionType
     * @param PermissionRequestType $PermissionRequest
     * @access public
     */
    public function __construct($Id, $ItemType, $UserId, $UserGroupList, $PermissionType, $PermissionRequest)
    {
      $this->Id = $Id;
      $this->ItemType = $ItemType;
      $this->UserId = $UserId;
      $this->UserGroupList = $UserGroupList;
      $this->PermissionType = $PermissionType;
      $this->PermissionRequest = $PermissionRequest;
    }

}
