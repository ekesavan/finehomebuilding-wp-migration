<?php

class EnableFolderInheritanceResponse
{

    /**
     * @var boolean $EnableFolderInheritanceResult
     * @access public
     */
    public $EnableFolderInheritanceResult = null;

    /**
     * @param boolean $EnableFolderInheritanceResult
     * @access public
     */
    public function __construct($EnableFolderInheritanceResult)
    {
      $this->EnableFolderInheritanceResult = $EnableFolderInheritanceResult;
    }

}
