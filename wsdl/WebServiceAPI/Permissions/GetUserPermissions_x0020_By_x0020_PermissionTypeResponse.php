<?php

class GetUserPermissions_x0020_By_x0020_PermissionTypeResponse
{

    /**
     * @var UserPermissionData[] $GetUserPermissions_x0020_By_x0020_PermissionTypeResult
     * @access public
     */
    public $GetUserPermissions_x0020_By_x0020_PermissionTypeResult = null;

    /**
     * @param UserPermissionData[] $GetUserPermissions_x0020_By_x0020_PermissionTypeResult
     * @access public
     */
    public function __construct($GetUserPermissions_x0020_By_x0020_PermissionTypeResult)
    {
      $this->GetUserPermissions_x0020_By_x0020_PermissionTypeResult = $GetUserPermissions_x0020_By_x0020_PermissionTypeResult;
    }

}
