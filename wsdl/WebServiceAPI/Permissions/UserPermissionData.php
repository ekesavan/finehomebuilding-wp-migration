<?php

include_once('PermissionData.php');

class UserPermissionData extends PermissionData
{

    /**
     * @var int $UserId
     * @access public
     */
    public $UserId = null;

    /**
     * @var int $GroupId
     * @access public
     */
    public $GroupId = null;

    /**
     * @var string $GroupName
     * @access public
     */
    public $GroupName = null;

    /**
     * @var string $Domain
     * @access public
     */
    public $Domain = null;

    /**
     * @var string $GroupDomain
     * @access public
     */
    public $GroupDomain = null;

    /**
     * @var boolean $IsMembershipUser
     * @access public
     */
    public $IsMembershipUser = null;

    /**
     * @var string $DisplayUserName
     * @access public
     */
    public $DisplayUserName = null;

    /**
     * @var string $DisplayGroupName
     * @access public
     */
    public $DisplayGroupName = null;

    /**
     * @var string $UserName
     * @access public
     */
    public $UserName = null;

    /**
     * @var int $ContentId
     * @access public
     */
    public $ContentId = null;

    /**
     * @var int $FolderId
     * @access public
     */
    public $FolderId = null;

    /**
     * @var boolean $IsPrivateContent
     * @access public
     */
    public $IsPrivateContent = null;

    /**
     * @var boolean $IsInherited
     * @access public
     */
    public $IsInherited = null;

    /**
     * @var int $InheritedFrom
     * @access public
     */
    public $InheritedFrom = null;

    /**
     * @param int $Id
     * @param boolean $CanApprove
     * @param boolean $CanAdd
     * @param boolean $CanEdit
     * @param boolean $CanDelete
     * @param boolean $CanRestore
     * @param boolean $CanAddToImageLib
     * @param boolean $CanAddToFileLib
     * @param boolean $CanOverwriteLib
     * @param boolean $CanAddToHyperlinkLib
     * @param boolean $CanAddToQuicklinkLib
     * @param boolean $CanAddFolders
     * @param boolean $CanEditFolders
     * @param boolean $CanDeleteFolders
     * @param boolean $CanTraverseFolders
     * @param boolean $CanCreateTask
     * @param boolean $CanRedirectTask
     * @param boolean $CanDestructTask
     * @param boolean $IsReadOnly
     * @param boolean $IsCollections
     * @param boolean $IsAdmin
     * @param boolean $IsLoggedIn
     * @param boolean $IsInMemberShip
     * @param boolean $IsReadOnlyLib
     * @param anyType $LegacyData
     * @param boolean $CanHistory
     * @param boolean $CanPublish
     * @param boolean $CanAddTask
     * @param boolean $CanBreakPending
     * @param boolean $CanView
     * @param boolean $CanEditSummary
     * @param boolean $CanSeeProperty
     * @param boolean $CanPreview
     * @param boolean $CanDecline
     * @param boolean $CanEditSumit
     * @param boolean $CanEditCollections
     * @param boolean $CanDeleteTask
     * @param boolean $CanMetadataComplete
     * @param boolean $CanEditProperties
     * @param boolean $CanEditApprovals
     * @param boolean $CanEditQLinks
     * @param int $UserId
     * @param int $GroupId
     * @param string $GroupName
     * @param string $Domain
     * @param string $GroupDomain
     * @param boolean $IsMembershipUser
     * @param string $DisplayUserName
     * @param string $DisplayGroupName
     * @param string $UserName
     * @param int $ContentId
     * @param int $FolderId
     * @param boolean $IsPrivateContent
     * @param boolean $IsInherited
     * @param int $InheritedFrom
     * @access public
     */
    public function __construct($Id, $CanApprove, $CanAdd, $CanEdit, $CanDelete, $CanRestore, $CanAddToImageLib, $CanAddToFileLib, $CanOverwriteLib, $CanAddToHyperlinkLib, $CanAddToQuicklinkLib, $CanAddFolders, $CanEditFolders, $CanDeleteFolders, $CanTraverseFolders, $CanCreateTask, $CanRedirectTask, $CanDestructTask, $IsReadOnly, $IsCollections, $IsAdmin, $IsLoggedIn, $IsInMemberShip, $IsReadOnlyLib, $LegacyData, $CanHistory, $CanPublish, $CanAddTask, $CanBreakPending, $CanView, $CanEditSummary, $CanSeeProperty, $CanPreview, $CanDecline, $CanEditSumit, $CanEditCollections, $CanDeleteTask, $CanMetadataComplete, $CanEditProperties, $CanEditApprovals, $CanEditQLinks, $UserId, $GroupId, $GroupName, $Domain, $GroupDomain, $IsMembershipUser, $DisplayUserName, $DisplayGroupName, $UserName, $ContentId, $FolderId, $IsPrivateContent, $IsInherited, $InheritedFrom)
    {
      parent::__construct($Id, $CanApprove, $CanAdd, $CanEdit, $CanDelete, $CanRestore, $CanAddToImageLib, $CanAddToFileLib, $CanOverwriteLib, $CanAddToHyperlinkLib, $CanAddToQuicklinkLib, $CanAddFolders, $CanEditFolders, $CanDeleteFolders, $CanTraverseFolders, $CanCreateTask, $CanRedirectTask, $CanDestructTask, $IsReadOnly, $IsCollections, $IsAdmin, $IsLoggedIn, $IsInMemberShip, $IsReadOnlyLib, $LegacyData, $CanHistory, $CanPublish, $CanAddTask, $CanBreakPending, $CanView, $CanEditSummary, $CanSeeProperty, $CanPreview, $CanDecline, $CanEditSumit, $CanEditCollections, $CanDeleteTask, $CanMetadataComplete, $CanEditProperties, $CanEditApprovals, $CanEditQLinks);
      $this->UserId = $UserId;
      $this->GroupId = $GroupId;
      $this->GroupName = $GroupName;
      $this->Domain = $Domain;
      $this->GroupDomain = $GroupDomain;
      $this->IsMembershipUser = $IsMembershipUser;
      $this->DisplayUserName = $DisplayUserName;
      $this->DisplayGroupName = $DisplayGroupName;
      $this->UserName = $UserName;
      $this->ContentId = $ContentId;
      $this->FolderId = $FolderId;
      $this->IsPrivateContent = $IsPrivateContent;
      $this->IsInherited = $IsInherited;
      $this->InheritedFrom = $InheritedFrom;
    }

}
