<?php

class UpdateItemPermissionResponse
{

    /**
     * @var boolean $UpdateItemPermissionResult
     * @access public
     */
    public $UpdateItemPermissionResult = null;

    /**
     * @param boolean $UpdateItemPermissionResult
     * @access public
     */
    public function __construct($UpdateItemPermissionResult)
    {
      $this->UpdateItemPermissionResult = $UpdateItemPermissionResult;
    }

}
