<?php

class DisableContentInheritanceResponse
{

    /**
     * @var boolean $DisableContentInheritanceResult
     * @access public
     */
    public $DisableContentInheritanceResult = null;

    /**
     * @param boolean $DisableContentInheritanceResult
     * @access public
     */
    public function __construct($DisableContentInheritanceResult)
    {
      $this->DisableContentInheritanceResult = $DisableContentInheritanceResult;
    }

}
