<?php

class DeleteItemPermissionResponse
{

    /**
     * @var boolean $DeleteItemPermissionResult
     * @access public
     */
    public $DeleteItemPermissionResult = null;

    /**
     * @param boolean $DeleteItemPermissionResult
     * @access public
     */
    public function __construct($DeleteItemPermissionResult)
    {
      $this->DeleteItemPermissionResult = $DeleteItemPermissionResult;
    }

}
