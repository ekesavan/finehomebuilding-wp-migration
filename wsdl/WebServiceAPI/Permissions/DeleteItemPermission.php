<?php

class DeleteItemPermission
{

    /**
     * @var int $id
     * @access public
     */
    public $id = null;

    /**
     * @var boolean $IsGroupId
     * @access public
     */
    public $IsGroupId = null;

    /**
     * @var int $itemId
     * @access public
     */
    public $itemId = null;

    /**
     * @var CMSObjectTypes $itemType
     * @access public
     */
    public $itemType = null;

    /**
     * @param int $id
     * @param boolean $IsGroupId
     * @param int $itemId
     * @param CMSObjectTypes $itemType
     * @access public
     */
    public function __construct($id, $IsGroupId, $itemId, $itemType)
    {
      $this->id = $id;
      $this->IsGroupId = $IsGroupId;
      $this->itemId = $itemId;
      $this->itemType = $itemType;
    }

}
