<?php

include_once('LoadPermissions.php');
include_once('PermissionResultType.php');
include_once('LoadPermissionsResponse.php');
include_once('PermissionData.php');
include_once('CmsDataOfPermissionData.php');
include_once('BaseDataOfPermissionData.php');
include_once('AuthenticationHeader.php');
include_once('RequestInfoParameters.php');
include_once('GetUserPermissions.php');
include_once('UserPermissionData.php');
include_once('GetUserPermissionsResponse.php');
include_once('GetUserPermissions_x0020_By_x0020_PermissionType.php');
include_once('PermissionUserType.php');
include_once('PermissionRequestType.php');
include_once('GetUserPermissions_x0020_By_x0020_PermissionTypeResponse.php');
include_once('UpdateItemPermission.php');
include_once('UpdateItemPermissionResponse.php');
include_once('AddItemPermission.php');
include_once('AddItemPermissionResponse.php');
include_once('DeleteItemPermission.php');
include_once('CMSObjectTypes.php');
include_once('DeleteItemPermissionResponse.php');
include_once('DisableFolderInheritance.php');
include_once('DisableFolderInheritanceResponse.php');
include_once('EnableFolderInheritance.php');
include_once('EnableFolderInheritanceResponse.php');
include_once('DisableContentInheritance.php');
include_once('DisableContentInheritanceResponse.php');
include_once('EnableContentInheritance.php');
include_once('EnableContentInheritanceResponse.php');

class Permissions extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'LoadPermissions' => '\LoadPermissions',
      'LoadPermissionsResponse' => '\LoadPermissionsResponse',
      'PermissionData' => '\PermissionData',
      'CmsDataOfPermissionData' => '\CmsDataOfPermissionData',
      'BaseDataOfPermissionData' => '\BaseDataOfPermissionData',
      'AuthenticationHeader' => '\AuthenticationHeader',
      'RequestInfoParameters' => '\RequestInfoParameters',
      'GetUserPermissions' => '\GetUserPermissions',
      'UserPermissionData' => '\UserPermissionData',
      'GetUserPermissionsResponse' => '\GetUserPermissionsResponse',
      'GetUserPermissions_x0020_By_x0020_PermissionType' => '\GetUserPermissions_x0020_By_x0020_PermissionType',
      'GetUserPermissions_x0020_By_x0020_PermissionTypeResponse' => '\GetUserPermissions_x0020_By_x0020_PermissionTypeResponse',
      'UpdateItemPermission' => '\UpdateItemPermission',
      'UpdateItemPermissionResponse' => '\UpdateItemPermissionResponse',
      'AddItemPermission' => '\AddItemPermission',
      'AddItemPermissionResponse' => '\AddItemPermissionResponse',
      'DeleteItemPermission' => '\DeleteItemPermission',
      'DeleteItemPermissionResponse' => '\DeleteItemPermissionResponse',
      'DisableFolderInheritance' => '\DisableFolderInheritance',
      'DisableFolderInheritanceResponse' => '\DisableFolderInheritanceResponse',
      'EnableFolderInheritance' => '\EnableFolderInheritance',
      'EnableFolderInheritanceResponse' => '\EnableFolderInheritanceResponse',
      'DisableContentInheritance' => '\DisableContentInheritance',
      'DisableContentInheritanceResponse' => '\DisableContentInheritanceResponse',
      'EnableContentInheritance' => '\EnableContentInheritance',
      'EnableContentInheritanceResponse' => '\EnableContentInheritanceResponse');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = 'Permissions.wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      
      parent::__construct($wsdl, $options);
    }

    /**
     * Loads the permission for given object, such as content, folder, task, etc.
     *
     * @param LoadPermissions $parameters
     * @access public
     * @return LoadPermissionsResponse
     */
    public function LoadPermissions(LoadPermissions $parameters)
    {
      return $this->__soapCall('LoadPermissions', array($parameters));
    }

    /**
     * Loads the user permissions as a array of UserPermissionData.
     *
     * @param GetUserPermissions $parameters
     * @access public
     * @return GetUserPermissionsResponse
     */
    public function GetUserPermissions(GetUserPermissions $parameters)
    {
      return $this->__soapCall('GetUserPermissions', array($parameters));
    }

    /**
     * Updates the item permissions for a user or a usergroup.
     *
     * @param UpdateItemPermission $parameters
     * @access public
     * @return UpdateItemPermissionResponse
     */
    public function UpdateItemPermission(UpdateItemPermission $parameters)
    {
      return $this->__soapCall('UpdateItemPermission', array($parameters));
    }

    /**
     * Adds the item permissions for a user or a usergroup.
     *
     * @param AddItemPermission $parameters
     * @access public
     * @return AddItemPermissionResponse
     */
    public function AddItemPermission(AddItemPermission $parameters)
    {
      return $this->__soapCall('AddItemPermission', array($parameters));
    }

    /**
     * Deletes a user or a group from the item's permissions
     *
     * @param DeleteItemPermission $parameters
     * @access public
     * @return DeleteItemPermissionResponse
     */
    public function DeleteItemPermission(DeleteItemPermission $parameters)
    {
      return $this->__soapCall('DeleteItemPermission', array($parameters));
    }

    /**
     * Deletes a user or a group from the item's permissions
     *
     * @param DisableFolderInheritance $parameters
     * @access public
     * @return DisableFolderInheritanceResponse
     */
    public function DisableFolderInheritance(DisableFolderInheritance $parameters)
    {
      return $this->__soapCall('DisableFolderInheritance', array($parameters));
    }

    /**
     * Deletes a user or a group from the item's permissions
     *
     * @param EnableFolderInheritance $parameters
     * @access public
     * @return EnableFolderInheritanceResponse
     */
    public function EnableFolderInheritance(EnableFolderInheritance $parameters)
    {
      return $this->__soapCall('EnableFolderInheritance', array($parameters));
    }

    /**
     * Deletes a user or a group from the item's permissions
     *
     * @param DisableContentInheritance $parameters
     * @access public
     * @return DisableContentInheritanceResponse
     */
    public function DisableContentInheritance(DisableContentInheritance $parameters)
    {
      return $this->__soapCall('DisableContentInheritance', array($parameters));
    }

    /**
     * Deletes a user or a group from the item's permissions
     *
     * @param EnableContentInheritance $parameters
     * @access public
     * @return EnableContentInheritanceResponse
     */
    public function EnableContentInheritance(EnableContentInheritance $parameters)
    {
      return $this->__soapCall('EnableContentInheritance', array($parameters));
    }

}
