<?php

class AddItemPermissionResponse
{

    /**
     * @var boolean $AddItemPermissionResult
     * @access public
     */
    public $AddItemPermissionResult = null;

    /**
     * @param boolean $AddItemPermissionResult
     * @access public
     */
    public function __construct($AddItemPermissionResult)
    {
      $this->AddItemPermissionResult = $AddItemPermissionResult;
    }

}
