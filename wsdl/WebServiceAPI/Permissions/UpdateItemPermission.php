<?php

class UpdateItemPermission
{

    /**
     * @var UserPermissionData $userPermData
     * @access public
     */
    public $userPermData = null;

    /**
     * @param UserPermissionData $userPermData
     * @access public
     */
    public function __construct($userPermData)
    {
      $this->userPermData = $userPermData;
    }

}
