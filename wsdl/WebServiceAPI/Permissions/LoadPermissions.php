<?php

class LoadPermissions
{

    /**
     * @var int $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var string $RequestType
     * @access public
     */
    public $RequestType = null;

    /**
     * @var PermissionResultType $Type
     * @access public
     */
    public $Type = null;

    /**
     * @param int $Id
     * @param string $RequestType
     * @param PermissionResultType $Type
     * @access public
     */
    public function __construct($Id, $RequestType, $Type)
    {
      $this->Id = $Id;
      $this->RequestType = $RequestType;
      $this->Type = $Type;
    }

}
