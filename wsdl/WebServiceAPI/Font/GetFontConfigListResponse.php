<?php

class GetFontConfigListResponse
{

    /**
     * @var string $GetFontConfigListResult
     * @access public
     */
    public $GetFontConfigListResult = null;

    /**
     * @param string $GetFontConfigListResult
     * @access public
     */
    public function __construct($GetFontConfigListResult)
    {
      $this->GetFontConfigListResult = $GetFontConfigListResult;
    }

}
