<?php

class GetAllFontsResponse
{

    /**
     * @var FontData[] $GetAllFontsResult
     * @access public
     */
    public $GetAllFontsResult = null;

    /**
     * @param FontData[] $GetAllFontsResult
     * @access public
     */
    public function __construct($GetAllFontsResult)
    {
      $this->GetAllFontsResult = $GetAllFontsResult;
    }

}
