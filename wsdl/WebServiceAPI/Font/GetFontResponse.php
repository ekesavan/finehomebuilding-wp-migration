<?php

class GetFontResponse
{

    /**
     * @var FontData $GetFontResult
     * @access public
     */
    public $GetFontResult = null;

    /**
     * @param FontData $GetFontResult
     * @access public
     */
    public function __construct($GetFontResult)
    {
      $this->GetFontResult = $GetFontResult;
    }

}
