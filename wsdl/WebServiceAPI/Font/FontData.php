<?php

class FontData
{

    /**
     * @var int $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var string $Face
     * @access public
     */
    public $Face = null;

    /**
     * @param int $Id
     * @param string $Face
     * @access public
     */
    public function __construct($Id, $Face)
    {
      $this->Id = $Id;
      $this->Face = $Face;
    }

}
