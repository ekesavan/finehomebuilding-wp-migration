<?php

include_once('GetAllFonts.php');
include_once('GetAllFontsResponse.php');
include_once('FontData.php');
include_once('AuthenticationHeader.php');
include_once('RequestInfoParameters.php');
include_once('GetFont.php');
include_once('GetFontResponse.php');
include_once('GetFontConfigList.php');
include_once('GetFontConfigListResponse.php');

class Font extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'GetAllFonts' => '\GetAllFonts',
      'GetAllFontsResponse' => '\GetAllFontsResponse',
      'FontData' => '\FontData',
      'AuthenticationHeader' => '\AuthenticationHeader',
      'RequestInfoParameters' => '\RequestInfoParameters',
      'GetFont' => '\GetFont',
      'GetFontResponse' => '\GetFontResponse',
      'GetFontConfigList' => '\GetFontConfigList',
      'GetFontConfigListResponse' => '\GetFontConfigListResponse');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = 'Font.wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      
      parent::__construct($wsdl, $options);
    }

    /**
     * @param GetAllFonts $parameters
     * @access public
     * @return GetAllFontsResponse
     */
    public function GetAllFonts(GetAllFonts $parameters)
    {
      return $this->__soapCall('GetAllFonts', array($parameters));
    }

    /**
     * @param GetFont $parameters
     * @access public
     * @return GetFontResponse
     */
    public function GetFont(GetFont $parameters)
    {
      return $this->__soapCall('GetFont', array($parameters));
    }

    /**
     * @param GetFontConfigList $parameters
     * @access public
     * @return GetFontConfigListResponse
     */
    public function GetFontConfigList(GetFontConfigList $parameters)
    {
      return $this->__soapCall('GetFontConfigList', array($parameters));
    }

}
