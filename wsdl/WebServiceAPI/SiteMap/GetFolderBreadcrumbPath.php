<?php

class GetFolderBreadcrumbPath
{

    /**
     * @var int $id
     * @access public
     */
    public $id = null;

    /**
     * @var boolean $isFolder
     * @access public
     */
    public $isFolder = null;

    /**
     * @param int $id
     * @param boolean $isFolder
     * @access public
     */
    public function __construct($id, $isFolder)
    {
      $this->id = $id;
      $this->isFolder = $isFolder;
    }

}
