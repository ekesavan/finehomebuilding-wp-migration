<?php

class RenderFolderBreadcrumbHtmlResponse
{

    /**
     * @var string $RenderFolderBreadcrumbHtmlResult
     * @access public
     */
    public $RenderFolderBreadcrumbHtmlResult = null;

    /**
     * @param string $RenderFolderBreadcrumbHtmlResult
     * @access public
     */
    public function __construct($RenderFolderBreadcrumbHtmlResult)
    {
      $this->RenderFolderBreadcrumbHtmlResult = $RenderFolderBreadcrumbHtmlResult;
    }

}
