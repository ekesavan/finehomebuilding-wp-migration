<?php

class RenderFolderBreadcrumbHtml
{

    /**
     * @var SitemapPath[] $nodes
     * @access public
     */
    public $nodes = null;

    /**
     * @var string $pathSeparator
     * @access public
     */
    public $pathSeparator = null;

    /**
     * @var boolean $linkNodes
     * @access public
     */
    public $linkNodes = null;

    /**
     * @var string $linkTarget
     * @access public
     */
    public $linkTarget = null;

    /**
     * @var boolean $displayVertical
     * @access public
     */
    public $displayVertical = null;

    /**
     * @var string $pageUrl
     * @access public
     */
    public $pageUrl = null;

    /**
     * @param SitemapPath[] $nodes
     * @param string $pathSeparator
     * @param boolean $linkNodes
     * @param string $linkTarget
     * @param boolean $displayVertical
     * @param string $pageUrl
     * @access public
     */
    public function __construct($nodes, $pathSeparator, $linkNodes, $linkTarget, $displayVertical, $pageUrl)
    {
      $this->nodes = $nodes;
      $this->pathSeparator = $pathSeparator;
      $this->linkNodes = $linkNodes;
      $this->linkTarget = $linkTarget;
      $this->displayVertical = $displayVertical;
      $this->pageUrl = $pageUrl;
    }

}
