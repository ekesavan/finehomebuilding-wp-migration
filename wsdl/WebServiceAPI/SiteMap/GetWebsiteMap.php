<?php

class GetWebsiteMap
{

    /**
     * @var int $folderId
     * @access public
     */
    public $folderId = null;

    /**
     * @var boolean $getSameLevelNodes
     * @access public
     */
    public $getSameLevelNodes = null;

    /**
     * @param int $folderId
     * @param boolean $getSameLevelNodes
     * @access public
     */
    public function __construct($folderId, $getSameLevelNodes)
    {
      $this->folderId = $folderId;
      $this->getSameLevelNodes = $getSameLevelNodes;
    }

}
