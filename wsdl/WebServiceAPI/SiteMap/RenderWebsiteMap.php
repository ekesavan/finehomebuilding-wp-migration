<?php

class RenderWebsiteMap
{

    /**
     * @var WebsiteMap $webSitemap
     * @access public
     */
    public $webSitemap = null;

    /**
     * @var string $displayType
     * @access public
     */
    public $displayType = null;

    /**
     * @var string $className
     * @access public
     */
    public $className = null;

    /**
     * @var int $maxLevel
     * @access public
     */
    public $maxLevel = null;

    /**
     * @var int $startLevel
     * @access public
     */
    public $startLevel = null;

    /**
     * @var string $pageUrl
     * @access public
     */
    public $pageUrl = null;

    /**
     * @param WebsiteMap $webSitemap
     * @param string $displayType
     * @param string $className
     * @param int $maxLevel
     * @param int $startLevel
     * @param string $pageUrl
     * @access public
     */
    public function __construct($webSitemap, $displayType, $className, $maxLevel, $startLevel, $pageUrl)
    {
      $this->webSitemap = $webSitemap;
      $this->displayType = $displayType;
      $this->className = $className;
      $this->maxLevel = $maxLevel;
      $this->startLevel = $startLevel;
      $this->pageUrl = $pageUrl;
    }

}
