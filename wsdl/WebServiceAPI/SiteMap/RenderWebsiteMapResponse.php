<?php

class RenderWebsiteMapResponse
{

    /**
     * @var string $RenderWebsiteMapResult
     * @access public
     */
    public $RenderWebsiteMapResult = null;

    /**
     * @param string $RenderWebsiteMapResult
     * @access public
     */
    public function __construct($RenderWebsiteMapResult)
    {
      $this->RenderWebsiteMapResult = $RenderWebsiteMapResult;
    }

}
