<?php

include_once('GetWebsiteMap.php');
include_once('GetWebsiteMapResponse.php');
include_once('WebsiteMap.php');
include_once('SitemapPath.php');
include_once('AuthenticationHeader.php');
include_once('RequestInfoParameters.php');
include_once('GetFolderBreadcrumbPath.php');
include_once('GetFolderBreadcrumbPathResponse.php');
include_once('RenderWebsiteMap.php');
include_once('RenderWebsiteMapResponse.php');
include_once('RenderFolderBreadcrumbHtml.php');
include_once('RenderFolderBreadcrumbHtmlResponse.php');

class SiteMap extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'GetWebsiteMap' => '\GetWebsiteMap',
      'GetWebsiteMapResponse' => '\GetWebsiteMapResponse',
      'WebsiteMap' => '\WebsiteMap',
      'SitemapPath' => '\SitemapPath',
      'AuthenticationHeader' => '\AuthenticationHeader',
      'RequestInfoParameters' => '\RequestInfoParameters',
      'GetFolderBreadcrumbPath' => '\GetFolderBreadcrumbPath',
      'GetFolderBreadcrumbPathResponse' => '\GetFolderBreadcrumbPathResponse',
      'RenderWebsiteMap' => '\RenderWebsiteMap',
      'RenderWebsiteMapResponse' => '\RenderWebsiteMapResponse',
      'RenderFolderBreadcrumbHtml' => '\RenderFolderBreadcrumbHtml',
      'RenderFolderBreadcrumbHtmlResponse' => '\RenderFolderBreadcrumbHtmlResponse');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = 'SiteMap.wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      
      parent::__construct($wsdl, $options);
    }

    /**
     * @param GetWebsiteMap $parameters
     * @access public
     * @return GetWebsiteMapResponse
     */
    public function GetWebsiteMap(GetWebsiteMap $parameters)
    {
      return $this->__soapCall('GetWebsiteMap', array($parameters));
    }

    /**
     * @param GetFolderBreadcrumbPath $parameters
     * @access public
     * @return GetFolderBreadcrumbPathResponse
     */
    public function GetFolderBreadcrumbPath(GetFolderBreadcrumbPath $parameters)
    {
      return $this->__soapCall('GetFolderBreadcrumbPath', array($parameters));
    }

    /**
     * @param RenderWebsiteMap $parameters
     * @access public
     * @return RenderWebsiteMapResponse
     */
    public function RenderWebsiteMap(RenderWebsiteMap $parameters)
    {
      return $this->__soapCall('RenderWebsiteMap', array($parameters));
    }

    /**
     * @param RenderFolderBreadcrumbHtml $parameters
     * @access public
     * @return RenderFolderBreadcrumbHtmlResponse
     */
    public function RenderFolderBreadcrumbHtml(RenderFolderBreadcrumbHtml $parameters)
    {
      return $this->__soapCall('RenderFolderBreadcrumbHtml', array($parameters));
    }

}
