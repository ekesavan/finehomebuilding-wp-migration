<?php

class GetWebsiteMapResponse
{

    /**
     * @var WebsiteMap $GetWebsiteMapResult
     * @access public
     */
    public $GetWebsiteMapResult = null;

    /**
     * @param WebsiteMap $GetWebsiteMapResult
     * @access public
     */
    public function __construct($GetWebsiteMapResult)
    {
      $this->GetWebsiteMapResult = $GetWebsiteMapResult;
    }

}
