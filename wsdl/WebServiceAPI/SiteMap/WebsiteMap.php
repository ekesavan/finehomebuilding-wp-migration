<?php

include_once('SitemapPath.php');

class WebsiteMap extends SitemapPath
{

    /**
     * @var WebsiteMap[] $childrenNodes
     * @access public
     */
    public $childrenNodes = null;

    /**
     * @param string $Title
     * @param string $Url
     * @param string $Description
     * @param int $Order
     * @param int $FolderId
     * @param int $ContentId
     * @param int $Language
     * @param WebsiteMap[] $childrenNodes
     * @access public
     */
    public function __construct($Title, $Url, $Description, $Order, $FolderId, $ContentId, $Language, $childrenNodes)
    {
      parent::__construct($Title, $Url, $Description, $Order, $FolderId, $ContentId, $Language);
      $this->childrenNodes = $childrenNodes;
    }

}
