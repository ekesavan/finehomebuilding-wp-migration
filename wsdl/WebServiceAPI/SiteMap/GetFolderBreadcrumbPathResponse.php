<?php

class GetFolderBreadcrumbPathResponse
{

    /**
     * @var SitemapPath[] $GetFolderBreadcrumbPathResult
     * @access public
     */
    public $GetFolderBreadcrumbPathResult = null;

    /**
     * @param SitemapPath[] $GetFolderBreadcrumbPathResult
     * @access public
     */
    public function __construct($GetFolderBreadcrumbPathResult)
    {
      $this->GetFolderBreadcrumbPathResult = $GetFolderBreadcrumbPathResult;
    }

}
