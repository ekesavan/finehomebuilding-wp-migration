<?php

include_once('AddMetaDataType.php');
//include_once('ContentMetaData.php');
//include_once('CmsDataOfContentMetaData.php');
//include_once('BaseDataOfContentMetaData.php');
//include_once('ContentMetadataDataType.php');
//include_once('ContentMetadataType.php');
include_once('AddMetaDataTypeResponse.php');
//include_once('AuthenticationHeader.php');
//include_once('RequestInfoParameters.php');
include_once('DeleteMetadataType.php');
include_once('DeleteMetadataTypeResponse.php');
include_once('GetContentMetadataList.php');
include_once('GetContentMetadataListResponse.php');
include_once('CustomAttribute.php');
include_once('CmsDataOfCustomAttribute.php');
include_once('BaseDataOfCustomAttribute.php');
include_once('CustomAttributeValueTypes.php');
include_once('GetContentMetadataList_x0020_with_x0020_preview.php');
include_once('GetContentMetadataList_x0020_with_x0020_previewResponse.php');
include_once('GetMetaDataTypes.php');
include_once('GetMetaDataTypesResponse.php');
include_once('GetMetadataType.php');
include_once('GetMetadataTypeResponse.php');

class Metadata extends \EktronSoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'AddMetaDataType' => '\AddMetaDataType',
      'ContentMetaData' => '\ContentMetaData',
      'CmsDataOfContentMetaData' => '\CmsDataOfContentMetaData',
      'BaseDataOfContentMetaData' => '\BaseDataOfContentMetaData',
      'AddMetaDataTypeResponse' => '\AddMetaDataTypeResponse',
      'AuthenticationHeader' => '\AuthenticationHeader',
      'RequestInfoParameters' => '\RequestInfoParameters',
      'DeleteMetadataType' => '\DeleteMetadataType',
      'DeleteMetadataTypeResponse' => '\DeleteMetadataTypeResponse',
      'GetContentMetadataList' => '\GetContentMetadataList',
      'GetContentMetadataListResponse' => '\GetContentMetadataListResponse',
      'CustomAttribute' => '\CustomAttribute',
      'CmsDataOfCustomAttribute' => '\CmsDataOfCustomAttribute',
      'BaseDataOfCustomAttribute' => '\BaseDataOfCustomAttribute',
      'GetContentMetadataList_x0020_with_x0020_preview' => '\GetContentMetadataList_x0020_with_x0020_preview',
      'GetContentMetadataList_x0020_with_x0020_previewResponse' => '\GetContentMetadataList_x0020_with_x0020_previewResponse',
      'GetMetaDataTypes' => '\GetMetaDataTypes',
      'GetMetaDataTypesResponse' => '\GetMetaDataTypesResponse',
      'GetMetadataType' => '\GetMetadataType',
      'GetMetadataTypeResponse' => '\GetMetadataTypeResponse');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = 'wsdl/WebServiceAPI/Metadata.wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      
      parent::__construct($wsdl, $options);
    }

    /**
     * Adds a new metadata type definition
     *
     * @param AddMetaDataType $parameters
     * @access public
     * @return AddMetaDataTypeResponse
     */
    public function AddMetaDataType(AddMetaDataType $parameters)
    {
      return $this->__soapCall('AddMetaDataType', array($parameters));
    }

    /**
     * @param DeleteMetadataType $parameters
     * @access public
     * @return DeleteMetadataTypeResponse
     */
    public function DeleteMetadataType(DeleteMetadataType $parameters)
    {
      return $this->__soapCall('DeleteMetadataType', array($parameters));
    }

    /**
     * @param GetContentMetadataList $parameters
     * @access public
     * @return GetContentMetadataListResponse
     */
    public function GetContentMetadataList(GetContentMetadataList $parameters)
    {
      return $this->__soapCall('GetContentMetadataList', array($parameters));
    }

    /**
     * @param GetMetaDataTypes $parameters
     * @access public
     * @return GetMetaDataTypesResponse
     */
    public function GetMetaDataTypes(GetMetaDataTypes $parameters)
    {
      return $this->__soapCall('GetMetaDataTypes', array($parameters));
    }

    /**
     * @param GetMetadataType $parameters
     * @access public
     * @return GetMetadataTypeResponse
     */
    public function GetMetadataType(GetMetadataType $parameters)
    {
      return $this->__soapCall('GetMetadataType', array($parameters));
    }

}
