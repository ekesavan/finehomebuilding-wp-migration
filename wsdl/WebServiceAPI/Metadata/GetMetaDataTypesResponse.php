<?php

class GetMetaDataTypesResponse
{

    /**
     * @var ContentMetaData[] $GetMetaDataTypesResult
     * @access public
     */
    public $GetMetaDataTypesResult = null;

    /**
     * @param ContentMetaData[] $GetMetaDataTypesResult
     * @access public
     */
    public function __construct($GetMetaDataTypesResult)
    {
      $this->GetMetaDataTypesResult = $GetMetaDataTypesResult;
    }

}
