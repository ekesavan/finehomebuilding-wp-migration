<?php

class DeleteMetadataType
{

    /**
     * @var int $MetadataId
     * @access public
     */
    public $MetadataId = null;

    /**
     * @param int $MetadataId
     * @access public
     */
    public function __construct($MetadataId)
    {
      $this->MetadataId = $MetadataId;
    }

}
