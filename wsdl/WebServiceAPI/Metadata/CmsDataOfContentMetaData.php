<?php

include_once('BaseDataOfContentMetaData.php');

class CmsDataOfContentMetaData extends BaseDataOfContentMetaData
{

    /**
     * @var int $Id
     * @access public
     */
    public $Id = null;

    /**
     * @param int $Id
     * @access public
     */
    public function __construct($Id)
    {
      $this->Id = $Id;
    }

}
