<?php

class AddMetaDataTypeResponse
{

    /**
     * @var ContentMetaData $metaDataItem
     * @access public
     */
    public $metaDataItem = null;

    /**
     * @param ContentMetaData $metaDataItem
     * @access public
     */
    public function __construct($metaDataItem)
    {
      $this->metaDataItem = $metaDataItem;
    }

}
