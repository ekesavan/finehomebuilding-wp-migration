<?php

class GetContentMetadataList
{

    /**
     * @var int $ContentId
     * @access public
     */
    public $ContentId = null;

    /**
     * @param int $ContentId
     * @access public
     */
    public function __construct($ContentId)
    {
      $this->ContentId = $ContentId;
    }

}
