<?php

class CustomAttributeValueTypes
{
    const __default = 'String';
    const String = 'String';
    const Boolean = 'Boolean';
    const Numeric = 'Numeric';
    const Date = 'Date';
    const Notification = 'Notification';
    const Category = 'Category';
    const CategoryProperties = 'CategoryProperties';
    const SelectList = 'SelectList';
    const MultiSelectList = 'MultiSelectList';
    const ThreadedDiscussion = 'ThreadedDiscussion';


}
