<?php

include_once('AddContentRating.php');
include_once('AddContentRatingResponse.php');
include_once('AuthenticationHeader.php');
include_once('RequestInfoParameters.php');
include_once('GetContentRatingResults.php');
include_once('GetContentRatingResultsResponse.php');
include_once('GetContentRatingResultsResult.php');
include_once('PurgeContentRatings.php');
include_once('PurgeContentRatingsResponse.php');
include_once('DataSet.php');

class ContentRating extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'AddContentRating' => '\AddContentRating',
      'AddContentRatingResponse' => '\AddContentRatingResponse',
      'AuthenticationHeader' => '\AuthenticationHeader',
      'RequestInfoParameters' => '\RequestInfoParameters',
      'GetContentRatingResults' => '\GetContentRatingResults',
      'GetContentRatingResultsResponse' => '\GetContentRatingResultsResponse',
      'GetContentRatingResultsResult' => '\GetContentRatingResultsResult',
      'PurgeContentRatings' => '\PurgeContentRatings',
      'PurgeContentRatingsResponse' => '\PurgeContentRatingsResponse',
      'DataSet' => '\DataSet');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = 'ContentRating.wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      
      parent::__construct($wsdl, $options);
    }

    /**
     * @param AddContentRating $parameters
     * @access public
     * @return AddContentRatingResponse
     */
    public function AddContentRating(AddContentRating $parameters)
    {
      return $this->__soapCall('AddContentRating', array($parameters));
    }

    /**
     * @param GetContentRatingResults $parameters
     * @access public
     * @return GetContentRatingResultsResponse
     */
    public function GetContentRatingResults(GetContentRatingResults $parameters)
    {
      return $this->__soapCall('GetContentRatingResults', array($parameters));
    }

    /**
     * @param PurgeContentRatings $parameters
     * @access public
     * @return PurgeContentRatingsResponse
     */
    public function PurgeContentRatings(PurgeContentRatings $parameters)
    {
      return $this->__soapCall('PurgeContentRatings', array($parameters));
    }

}
