<?php

class GetContentRatingResults
{

    /**
     * @var int $contentID
     * @access public
     */
    public $contentID = null;

    /**
     * @var dateTime $startDate
     * @access public
     */
    public $startDate = null;

    /**
     * @var dateTime $endDate
     * @access public
     */
    public $endDate = null;

    /**
     * @param int $contentID
     * @param dateTime $startDate
     * @param dateTime $endDate
     * @access public
     */
    public function __construct($contentID, $startDate, $endDate)
    {
      $this->contentID = $contentID;
      $this->startDate = $startDate;
      $this->endDate = $endDate;
    }

}
