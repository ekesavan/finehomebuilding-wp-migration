<?php

class AddContentRating
{

    /**
     * @var int $contentid
     * @access public
     */
    public $contentid = null;

    /**
     * @var int $userid
     * @access public
     */
    public $userid = null;

    /**
     * @var int $userRating
     * @access public
     */
    public $userRating = null;

    /**
     * @var string $userComments
     * @access public
     */
    public $userComments = null;

    /**
     * @param int $contentid
     * @param int $userid
     * @param int $userRating
     * @param string $userComments
     * @access public
     */
    public function __construct($contentid, $userid, $userRating, $userComments)
    {
      $this->contentid = $contentid;
      $this->userid = $userid;
      $this->userRating = $userRating;
      $this->userComments = $userComments;
    }

}
