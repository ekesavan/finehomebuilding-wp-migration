<?php

class PurgeContentRatings
{

    /**
     * @var int $contentid
     * @access public
     */
    public $contentid = null;

    /**
     * @var dateTime $startDate
     * @access public
     */
    public $startDate = null;

    /**
     * @var dateTime $endDate
     * @access public
     */
    public $endDate = null;

    /**
     * @param int $contentid
     * @param dateTime $startDate
     * @param dateTime $endDate
     * @access public
     */
    public function __construct($contentid, $startDate, $endDate)
    {
      $this->contentid = $contentid;
      $this->startDate = $startDate;
      $this->endDate = $endDate;
    }

}
