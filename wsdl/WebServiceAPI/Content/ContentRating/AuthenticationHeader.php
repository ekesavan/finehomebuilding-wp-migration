<?php

class AuthenticationHeader
{

    /**
     * @var string $Username
     * @access public
     */
    public $Username = null;

    /**
     * @var string $Password
     * @access public
     */
    public $Password = null;

    /**
     * @var string $Domain
     * @access public
     */
    public $Domain = null;

    /**
     * @param string $Username
     * @param string $Password
     * @param string $Domain
     * @access public
     */
    public function __construct($Username, $Password, $Domain)
    {
      $this->Username = $Username;
      $this->Password = $Password;
      $this->Domain = $Domain;
    }

}
