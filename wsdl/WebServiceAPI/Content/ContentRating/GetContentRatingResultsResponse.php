<?php

class GetContentRatingResultsResponse
{

    /**
     * @var GetContentRatingResultsResult $GetContentRatingResultsResult
     * @access public
     */
    public $GetContentRatingResultsResult = null;

    /**
     * @param GetContentRatingResultsResult $GetContentRatingResultsResult
     * @access public
     */
    public function __construct($GetContentRatingResultsResult)
    {
      $this->GetContentRatingResultsResult = $GetContentRatingResultsResult;
    }

}
