<?php

class LanguageData
{

    /**
     * @var int $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var int $Activated
     * @access public
     */
    public $Activated = null;

    /**
     * @var string $Type
     * @access public
     */
    public $Type = null;

    /**
     * @var string $Name
     * @access public
     */
    public $Name = null;

    /**
     * @var string $ImagePath
     * @access public
     */
    public $ImagePath = null;

    /**
     * @var string $CharSet
     * @access public
     */
    public $CharSet = null;

    /**
     * @var string $BrowserCode
     * @access public
     */
    public $BrowserCode = null;

    /**
     * @var string $Direction
     * @access public
     */
    public $Direction = null;

    /**
     * @var string $FlagFile
     * @access public
     */
    public $FlagFile = null;

    /**
     * @var string $XmlLang
     * @access public
     */
    public $XmlLang = null;

    /**
     * @var boolean $SiteEnabled
     * @access public
     */
    public $SiteEnabled = null;

    /**
     * @var string $LocalName
     * @access public
     */
    public $LocalName = null;

    /**
     * @var anyType $LegacyData
     * @access public
     */
    public $LegacyData = null;

    /**
     * @param int $Id
     * @param int $Activated
     * @param string $Type
     * @param string $Name
     * @param string $ImagePath
     * @param string $CharSet
     * @param string $BrowserCode
     * @param string $Direction
     * @param string $FlagFile
     * @param string $XmlLang
     * @param boolean $SiteEnabled
     * @param string $LocalName
     * @param anyType $LegacyData
     * @access public
     */
    public function __construct($Id, $Activated, $Type, $Name, $ImagePath, $CharSet, $BrowserCode, $Direction, $FlagFile, $XmlLang, $SiteEnabled, $LocalName, $LegacyData)
    {
      $this->Id = $Id;
      $this->Activated = $Activated;
      $this->Type = $Type;
      $this->Name = $Name;
      $this->ImagePath = $ImagePath;
      $this->CharSet = $CharSet;
      $this->BrowserCode = $BrowserCode;
      $this->Direction = $Direction;
      $this->FlagFile = $FlagFile;
      $this->XmlLang = $XmlLang;
      $this->SiteEnabled = $SiteEnabled;
      $this->LocalName = $LocalName;
      $this->LegacyData = $LegacyData;
    }

}
