<?php

include_once('ContentData.php');

class ContentStateData extends ContentData
{

    /**
     * @var string $CurrentTitle
     * @access public
     */
    public $CurrentTitle = null;

    /**
     * @var string $CurrentDisplayEndDate
     * @access public
     */
    public $CurrentDisplayEndDate = null;

    /**
     * @var string $CurrentEndDate
     * @access public
     */
    public $CurrentEndDate = null;

    /**
     * @var string $CurrentEditorFirstName
     * @access public
     */
    public $CurrentEditorFirstName = null;

    /**
     * @var string $CurrentEditorLastName
     * @access public
     */
    public $CurrentEditorLastName = null;

    /**
     * @var string $PreviousStatus
     * @access public
     */
    public $PreviousStatus = null;

    /**
     * @var int $CurrentFolderId
     * @access public
     */
    public $CurrentFolderId = null;

    /**
     * @var string $CurrentGoLive
     * @access public
     */
    public $CurrentGoLive = null;

    /**
     * @var string $CurrentDisplayGoLive
     * @access public
     */
    public $CurrentDisplayGoLive = null;

    /**
     * @var string $CurrentLastEditDate
     * @access public
     */
    public $CurrentLastEditDate = null;

    /**
     * @var string $CurrentDisplayLastEditDate
     * @access public
     */
    public $CurrentDisplayLastEditDate = null;

    /**
     * @var boolean $CurrentMetaComplete
     * @access public
     */
    public $CurrentMetaComplete = null;

    /**
     * @var int $CurrentUserId
     * @access public
     */
    public $CurrentUserId = null;

    /**
     * @var string $CurrentComment
     * @access public
     */
    public $CurrentComment = null;

    /**
     * @var boolean $IsFolderPrivate
     * @access public
     */
    public $IsFolderPrivate = null;

    /**
     * @param int $Id
     * @param int $LanguageId
     * @param string $Title
     * @param string $Teaser
     * @param string $Html
     * @param string $Quicklink
     * @param string $Image
     * @param boolean $IsPrivate
     * @param int $Type
     * @param CMSContentSubtype $SubType
     * @param int $ExternalTypeId
     * @param AssetData $AssetData
     * @param string $EditorFirstName
     * @param string $EditorLastName
     * @param string $Comment
     * @param dateTime $DateModified
     * @param string $DisplayLastEditDate
     * @param dateTime $DateCreated
     * @param string $DisplayDateCreated
     * @param int $UserId
     * @param int $FolderId
     * @param boolean $IsPermissionsInherited
     * @param int $PermissionInheritedFrom
     * @param string $Status
     * @param string $DisplayGoLive
     * @param boolean $IsPublished
     * @param boolean $IsSearchable
     * @param int $XmlInheritedFrom
     * @param ContentMetaData[] $MetaData
     * @param string $DisplayEndDate
     * @param dateTime $ExpireDate
     * @param CMSEndDateAction $EndDateActionType
     * @param XmlConfigData $XmlConfiguration
     * @param string $StyleSheet
     * @param string $LanguageDescription
     * @param string $Text
     * @param string $Path
     * @param string $ContentPath
     * @param int $ContType
     * @param int $Updates
     * @param string $FolderName
     * @param string $MediaText
     * @param int $HistoryId
     * @param string $HyperLink
     * @param TemplateData $TemplateConfiguration
     * @param int $FlagDefId
     * @param string $CurrentTitle
     * @param string $CurrentDisplayEndDate
     * @param string $CurrentEndDate
     * @param string $CurrentEditorFirstName
     * @param string $CurrentEditorLastName
     * @param string $PreviousStatus
     * @param int $CurrentFolderId
     * @param string $CurrentGoLive
     * @param string $CurrentDisplayGoLive
     * @param string $CurrentLastEditDate
     * @param string $CurrentDisplayLastEditDate
     * @param boolean $CurrentMetaComplete
     * @param int $CurrentUserId
     * @param string $CurrentComment
     * @param boolean $IsFolderPrivate
     * @access public
     */
    public function __construct($Id, $LanguageId, $Title, $Teaser, $Html, $Quicklink, $Image, $IsPrivate, $Type, $SubType, $ExternalTypeId, $AssetData, $EditorFirstName, $EditorLastName, $Comment, $DateModified, $DisplayLastEditDate, $DateCreated, $DisplayDateCreated, $UserId, $FolderId, $IsPermissionsInherited, $PermissionInheritedFrom, $Status, $DisplayGoLive, $IsPublished, $IsSearchable, $XmlInheritedFrom, $MetaData, $DisplayEndDate, $ExpireDate, $EndDateActionType, $XmlConfiguration, $StyleSheet, $LanguageDescription, $Text, $Path, $ContentPath, $ContType, $Updates, $FolderName, $MediaText, $HistoryId, $HyperLink, $TemplateConfiguration, $FlagDefId, $CurrentTitle, $CurrentDisplayEndDate, $CurrentEndDate, $CurrentEditorFirstName, $CurrentEditorLastName, $PreviousStatus, $CurrentFolderId, $CurrentGoLive, $CurrentDisplayGoLive, $CurrentLastEditDate, $CurrentDisplayLastEditDate, $CurrentMetaComplete, $CurrentUserId, $CurrentComment, $IsFolderPrivate)
    {
      parent::__construct($Id, $LanguageId, $Title, $Teaser, $Html, $Quicklink, $Image, $IsPrivate, $Type, $SubType, $ExternalTypeId, $AssetData, $EditorFirstName, $EditorLastName, $Comment, $DateModified, $DisplayLastEditDate, $DateCreated, $DisplayDateCreated, $UserId, $FolderId, $IsPermissionsInherited, $PermissionInheritedFrom, $Status, $DisplayGoLive, $IsPublished, $IsSearchable, $XmlInheritedFrom, $MetaData, $DisplayEndDate, $ExpireDate, $EndDateActionType, $XmlConfiguration, $StyleSheet, $LanguageDescription, $Text, $Path, $ContentPath, $ContType, $Updates, $FolderName, $MediaText, $HistoryId, $HyperLink, $TemplateConfiguration, $FlagDefId);
      $this->CurrentTitle = $CurrentTitle;
      $this->CurrentDisplayEndDate = $CurrentDisplayEndDate;
      $this->CurrentEndDate = $CurrentEndDate;
      $this->CurrentEditorFirstName = $CurrentEditorFirstName;
      $this->CurrentEditorLastName = $CurrentEditorLastName;
      $this->PreviousStatus = $PreviousStatus;
      $this->CurrentFolderId = $CurrentFolderId;
      $this->CurrentGoLive = $CurrentGoLive;
      $this->CurrentDisplayGoLive = $CurrentDisplayGoLive;
      $this->CurrentLastEditDate = $CurrentLastEditDate;
      $this->CurrentDisplayLastEditDate = $CurrentDisplayLastEditDate;
      $this->CurrentMetaComplete = $CurrentMetaComplete;
      $this->CurrentUserId = $CurrentUserId;
      $this->CurrentComment = $CurrentComment;
      $this->IsFolderPrivate = $IsFolderPrivate;
    }

}
