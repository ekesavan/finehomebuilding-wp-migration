<?php

class GetContentForEditingResponse
{

    /**
     * @var ContentEditData $GetContentForEditingResult
     * @access public
     */
    public $GetContentForEditingResult = null;

    /**
     * @param ContentEditData $GetContentForEditingResult
     * @access public
     */
    public function __construct($GetContentForEditingResult)
    {
      $this->GetContentForEditingResult = $GetContentForEditingResult;
    }

}
