<?php

class GetChildContentWithPaging
{

    /**
     * @var int $FolderID
     * @access public
     */
    public $FolderID = null;

    /**
     * @var boolean $Recursive
     * @access public
     */
    public $Recursive = null;

    /**
     * @var string $OrderBy
     * @access public
     */
    public $OrderBy = null;

    /**
     * @var PagingInfo $pagingInfo
     * @access public
     */
    public $pagingInfo = null;

    /**
     * @param int $FolderID
     * @param boolean $Recursive
     * @param string $OrderBy
     * @param PagingInfo $pagingInfo
     * @access public
     */
    public function __construct($FolderID, $Recursive, $OrderBy, $pagingInfo)
    {
      $this->FolderID = $FolderID;
      $this->Recursive = $Recursive;
      $this->OrderBy = $OrderBy;
      $this->pagingInfo = $pagingInfo;
    }

}
