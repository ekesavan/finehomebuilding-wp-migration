<?php

class GetAllUnassignedItemApprovals
{

    /**
     * @var int $ContentID
     * @access public
     */
    public $ContentID = null;

    /**
     * @var string $ItemType
     * @access public
     */
    public $ItemType = null;

    /**
     * @param int $ContentID
     * @param string $ItemType
     * @access public
     */
    public function __construct($ContentID, $ItemType)
    {
      $this->ContentID = $ContentID;
      $this->ItemType = $ItemType;
    }

}
