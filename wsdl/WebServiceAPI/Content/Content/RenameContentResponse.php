<?php

class RenameContentResponse
{

    /**
     * @var ContentData $RenameContentResult
     * @access public
     */
    public $RenameContentResult = null;

    /**
     * @param ContentData $RenameContentResult
     * @access public
     */
    public function __construct($RenameContentResult)
    {
      $this->RenameContentResult = $RenameContentResult;
    }

}
