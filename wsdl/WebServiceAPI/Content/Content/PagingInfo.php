<?php

class PagingInfo
{

    /**
     * @var int $RecordsPerPage
     * @access public
     */
    public $RecordsPerPage = null;

    /**
     * @var int $CurrentPage
     * @access public
     */
    public $CurrentPage = null;

    /**
     * @var int $TotalPages
     * @access public
     */
    public $TotalPages = null;

    /**
     * @var int $TotalRecords
     * @access public
     */
    public $TotalRecords = null;

    /**
     * @param int $RecordsPerPage
     * @param int $CurrentPage
     * @param int $TotalPages
     * @param int $TotalRecords
     * @access public
     */
    public function __construct($RecordsPerPage, $CurrentPage, $TotalPages, $TotalRecords)
    {
      $this->RecordsPerPage = $RecordsPerPage;
      $this->CurrentPage = $CurrentPage;
      $this->TotalPages = $TotalPages;
      $this->TotalRecords = $TotalRecords;
    }

}
