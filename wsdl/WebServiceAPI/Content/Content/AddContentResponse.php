<?php

class AddContentResponse
{

    /**
     * @var int $AddContentResult
     * @access public
     */
    public $AddContentResult = null;

    /**
     * @param int $AddContentResult
     * @access public
     */
    public function __construct($AddContentResult)
    {
      $this->AddContentResult = $AddContentResult;
    }

}
