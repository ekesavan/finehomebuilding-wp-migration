<?php

class GetAddViewLanguageResponse
{

    /**
     * @var LanguageData[] $GetAddViewLanguageResult
     * @access public
     */
    public $GetAddViewLanguageResult = null;

    /**
     * @param LanguageData[] $GetAddViewLanguageResult
     * @access public
     */
    public function __construct($GetAddViewLanguageResult)
    {
      $this->GetAddViewLanguageResult = $GetAddViewLanguageResult;
    }

}
