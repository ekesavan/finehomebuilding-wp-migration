<?php

class AddContent_x0020_With_x0020_ContentID
{

    /**
     * @var string $ContentTitle
     * @access public
     */
    public $ContentTitle = null;

    /**
     * @var string $ContentComment
     * @access public
     */
    public $ContentComment = null;

    /**
     * @var string $ContentHtml
     * @access public
     */
    public $ContentHtml = null;

    /**
     * @var string $SearchText
     * @access public
     */
    public $SearchText = null;

    /**
     * @var string $SummaryHtml
     * @access public
     */
    public $SummaryHtml = null;

    /**
     * @var string $ContentLanguage
     * @access public
     */
    public $ContentLanguage = null;

    /**
     * @var int $FolderId
     * @access public
     */
    public $FolderId = null;

    /**
     * @var anyType $GoLive
     * @access public
     */
    public $GoLive = null;

    /**
     * @var anyType $EndDate
     * @access public
     */
    public $EndDate = null;

    /**
     * @var string $MetaInfoXml
     * @access public
     */
    public $MetaInfoXml = null;

    /**
     * @var int $ContentID
     * @access public
     */
    public $ContentID = null;

    /**
     * @param string $ContentTitle
     * @param string $ContentComment
     * @param string $ContentHtml
     * @param string $SearchText
     * @param string $SummaryHtml
     * @param string $ContentLanguage
     * @param int $FolderId
     * @param anyType $GoLive
     * @param anyType $EndDate
     * @param string $MetaInfoXml
     * @param int $ContentID
     * @access public
     */
    public function __construct($ContentTitle, $ContentComment, $ContentHtml, $SearchText, $SummaryHtml, $ContentLanguage, $FolderId, $GoLive, $EndDate, $MetaInfoXml, $ContentID)
    {
      $this->ContentTitle = $ContentTitle;
      $this->ContentComment = $ContentComment;
      $this->ContentHtml = $ContentHtml;
      $this->SearchText = $SearchText;
      $this->SummaryHtml = $SummaryHtml;
      $this->ContentLanguage = $ContentLanguage;
      $this->FolderId = $FolderId;
      $this->GoLive = $GoLive;
      $this->EndDate = $EndDate;
      $this->MetaInfoXml = $MetaInfoXml;
      $this->ContentID = $ContentID;
    }

}
