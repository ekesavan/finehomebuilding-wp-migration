<?php

class SubmitForDeleteResponse
{

    /**
     * @var boolean $SubmitForDeleteResult
     * @access public
     */
    public $SubmitForDeleteResult = null;

    /**
     * @param boolean $SubmitForDeleteResult
     * @access public
     */
    public function __construct($SubmitForDeleteResult)
    {
      $this->SubmitForDeleteResult = $SubmitForDeleteResult;
    }

}
