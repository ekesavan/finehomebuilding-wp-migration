<?php

class NotifySubscriptionForSynchronizedContent
{

    /**
     * @var int $startVersion
     * @access public
     */
    public $startVersion = null;

    /**
     * @param int $startVersion
     * @access public
     */
    public function __construct($startVersion)
    {
      $this->startVersion = $startVersion;
    }

}
