<?php

class CopyContentToFolderResponse
{

    /**
     * @var ContentData $CopyContentToFolderResult
     * @access public
     */
    public $CopyContentToFolderResult = null;

    /**
     * @param ContentData $CopyContentToFolderResult
     * @access public
     */
    public function __construct($CopyContentToFolderResult)
    {
      $this->CopyContentToFolderResult = $CopyContentToFolderResult;
    }

}
