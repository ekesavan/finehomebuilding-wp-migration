<?php

class GetMyAssetMgtIDResponse
{

    /**
     * @var string $GetMyAssetMgtIDResult
     * @access public
     */
    public $GetMyAssetMgtIDResult = null;

    /**
     * @param string $GetMyAssetMgtIDResult
     * @access public
     */
    public function __construct($GetMyAssetMgtIDResult)
    {
      $this->GetMyAssetMgtIDResult = $GetMyAssetMgtIDResult;
    }

}
