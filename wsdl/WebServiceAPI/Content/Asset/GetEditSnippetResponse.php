<?php

class GetEditSnippetResponse
{

    /**
     * @var string $GetEditSnippetResult
     * @access public
     */
    public $GetEditSnippetResult = null;

    /**
     * @param string $GetEditSnippetResult
     * @access public
     */
    public function __construct($GetEditSnippetResult)
    {
      $this->GetEditSnippetResult = $GetEditSnippetResult;
    }

}
