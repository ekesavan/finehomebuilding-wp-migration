<?php

class AssetSuperTypeData
{

    /**
     * @var string $CommonName
     * @access public
     */
    public $CommonName = null;

    /**
     * @var string $ConnectionInfo
     * @access public
     */
    public $ConnectionInfo = null;

    /**
     * @var string $PluginType
     * @access public
     */
    public $PluginType = null;

    /**
     * @var int $TypeId
     * @access public
     */
    public $TypeId = null;

    /**
     * @param string $CommonName
     * @param string $ConnectionInfo
     * @param string $PluginType
     * @param int $TypeId
     * @access public
     */
    public function __construct($CommonName, $ConnectionInfo, $PluginType, $TypeId)
    {
      $this->CommonName = $CommonName;
      $this->ConnectionInfo = $ConnectionInfo;
      $this->PluginType = $PluginType;
      $this->TypeId = $TypeId;
    }

}
