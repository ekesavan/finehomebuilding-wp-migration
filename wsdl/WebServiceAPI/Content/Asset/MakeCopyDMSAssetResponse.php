<?php

class MakeCopyDMSAssetResponse
{

    /**
     * @var boolean $MakeCopyDMSAssetResult
     * @access public
     */
    public $MakeCopyDMSAssetResult = null;

    /**
     * @param boolean $MakeCopyDMSAssetResult
     * @access public
     */
    public function __construct($MakeCopyDMSAssetResult)
    {
      $this->MakeCopyDMSAssetResult = $MakeCopyDMSAssetResult;
    }

}
