<?php

class AsetConfigType
{
    const __default = 'CatalogLocation';
    const CatalogLocation = 'CatalogLocation';
    const CatalogName = 'CatalogName';
    const DomainName = 'DomainName';
    const FileTypes = 'FileTypes';
    const StorageLocation = 'StorageLocation';
    const WebShareDir = 'WebShareDir';
    const UserName = 'UserName';
    const Password = 'Password';
    const ServerName = 'ServerName';
    const LoadBalanced = 'LoadBalanced';
    const pdfGenerator = 'pdfGenerator';


}
