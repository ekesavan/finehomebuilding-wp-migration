<?php

class SetAssetMgtConfigInfoResponse
{

    /**
     * @var boolean $SetAssetMgtConfigInfoResult
     * @access public
     */
    public $SetAssetMgtConfigInfoResult = null;

    /**
     * @param boolean $SetAssetMgtConfigInfoResult
     * @access public
     */
    public function __construct($SetAssetMgtConfigInfoResult)
    {
      $this->SetAssetMgtConfigInfoResult = $SetAssetMgtConfigInfoResult;
    }

}
