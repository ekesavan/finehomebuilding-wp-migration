<?php

class MakeCopyDMSAsset
{

    /**
     * @var string $AssetID
     * @access public
     */
    public $AssetID = null;

    /**
     * @var string $AssetVersion
     * @access public
     */
    public $AssetVersion = null;

    /**
     * @param string $AssetID
     * @param string $AssetVersion
     * @access public
     */
    public function __construct($AssetID, $AssetVersion)
    {
      $this->AssetID = $AssetID;
      $this->AssetVersion = $AssetVersion;
    }

}
