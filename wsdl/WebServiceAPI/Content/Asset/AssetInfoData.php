<?php

include_once('AssetSuperTypeData.php');

class AssetInfoData extends AssetSuperTypeData
{

    /**
     * @var AssetTypeData[] $AssetTypeData
     * @access public
     */
    public $AssetTypeData = null;

    /**
     * @var string $Capabilities
     * @access public
     */
    public $Capabilities = null;

    /**
     * @param string $CommonName
     * @param string $ConnectionInfo
     * @param string $PluginType
     * @param int $TypeId
     * @param AssetTypeData[] $AssetTypeData
     * @param string $Capabilities
     * @access public
     */
    public function __construct($CommonName, $ConnectionInfo, $PluginType, $TypeId, $AssetTypeData, $Capabilities)
    {
      parent::__construct($CommonName, $ConnectionInfo, $PluginType, $TypeId);
      $this->AssetTypeData = $AssetTypeData;
      $this->Capabilities = $Capabilities;
    }

}
