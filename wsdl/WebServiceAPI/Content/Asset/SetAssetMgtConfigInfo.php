<?php

class SetAssetMgtConfigInfo
{

    /**
     * @var AssetConfigInfo[] $AssetConfig
     * @access public
     */
    public $AssetConfig = null;

    /**
     * @param AssetConfigInfo[] $AssetConfig
     * @access public
     */
    public function __construct($AssetConfig)
    {
      $this->AssetConfig = $AssetConfig;
    }

}
