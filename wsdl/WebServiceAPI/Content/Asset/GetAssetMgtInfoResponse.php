<?php

class GetAssetMgtInfoResponse
{

    /**
     * @var AssetInfoData[] $GetAssetMgtInfoResult
     * @access public
     */
    public $GetAssetMgtInfoResult = null;

    /**
     * @param AssetInfoData[] $GetAssetMgtInfoResult
     * @access public
     */
    public function __construct($GetAssetMgtInfoResult)
    {
      $this->GetAssetMgtInfoResult = $GetAssetMgtInfoResult;
    }

}
