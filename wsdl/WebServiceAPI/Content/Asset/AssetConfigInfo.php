<?php

class AssetConfigInfo
{

    /**
     * @var AsetConfigType $Tag
     * @access public
     */
    public $Tag = null;

    /**
     * @var string $Value
     * @access public
     */
    public $Value = null;

    /**
     * @var string $Description
     * @access public
     */
    public $Description = null;

    /**
     * @param AsetConfigType $Tag
     * @param string $Value
     * @param string $Description
     * @access public
     */
    public function __construct($Tag, $Value, $Description)
    {
      $this->Tag = $Tag;
      $this->Value = $Value;
      $this->Description = $Description;
    }

}
