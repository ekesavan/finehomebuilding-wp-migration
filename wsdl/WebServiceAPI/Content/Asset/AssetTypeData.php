<?php

class AssetTypeData
{

    /**
     * @var int $TypeId
     * @access public
     */
    public $TypeId = null;

    /**
     * @var int $SuperTypeId
     * @access public
     */
    public $SuperTypeId = null;

    /**
     * @var string $CommonName
     * @access public
     */
    public $CommonName = null;

    /**
     * @var string $MimeType
     * @access public
     */
    public $MimeType = null;

    /**
     * @var string $FileExtension
     * @access public
     */
    public $FileExtension = null;

    /**
     * @var string $ImageUrl
     * @access public
     */
    public $ImageUrl = null;

    /**
     * @param int $TypeId
     * @param int $SuperTypeId
     * @param string $CommonName
     * @param string $MimeType
     * @param string $FileExtension
     * @param string $ImageUrl
     * @access public
     */
    public function __construct($TypeId, $SuperTypeId, $CommonName, $MimeType, $FileExtension, $ImageUrl)
    {
      $this->TypeId = $TypeId;
      $this->SuperTypeId = $SuperTypeId;
      $this->CommonName = $CommonName;
      $this->MimeType = $MimeType;
      $this->FileExtension = $FileExtension;
      $this->ImageUrl = $ImageUrl;
    }

}
