<?php

class GetAssetSupertypesResponse
{

    /**
     * @var AssetInfoData[] $GetAssetSupertypesResult
     * @access public
     */
    public $GetAssetSupertypesResult = null;

    /**
     * @param AssetInfoData[] $GetAssetSupertypesResult
     * @access public
     */
    public function __construct($GetAssetSupertypesResult)
    {
      $this->GetAssetSupertypesResult = $GetAssetSupertypesResult;
    }

}
