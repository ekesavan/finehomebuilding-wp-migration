<?php

class GetEditSnippet
{

    /**
     * @var string $AssetId
     * @access public
     */
    public $AssetId = null;

    /**
     * @var int $ContentType
     * @access public
     */
    public $ContentType = null;

    /**
     * @param string $AssetId
     * @param int $ContentType
     * @access public
     */
    public function __construct($AssetId, $ContentType)
    {
      $this->AssetId = $AssetId;
      $this->ContentType = $ContentType;
    }

}
