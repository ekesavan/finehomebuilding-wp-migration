<?php

include_once('GetAssetMgtConfigInfo.php');
include_once('GetAssetMgtConfigInfoResponse.php');
include_once('AssetConfigInfo.php');
include_once('AsetConfigType.php');
include_once('AuthenticationHeader.php');
include_once('RequestInfoParameters.php');
include_once('GetAssetMgtInfo.php');
include_once('GetAssetMgtInfoResponse.php');
include_once('AssetInfoData.php');
include_once('AssetSuperTypeData.php');
include_once('AssetTypeData.php');
include_once('GetAssetSupertypes.php');
include_once('GetAssetSupertypesResponse.php');
include_once('GetEditSnippet.php');
include_once('GetEditSnippetResponse.php');
include_once('GetMyAssetMgtID.php');
include_once('GetMyAssetMgtIDResponse.php');
include_once('MakeCopyDMSAsset.php');
include_once('MakeCopyDMSAssetResponse.php');
include_once('SetAssetMgtConfigInfo.php');
include_once('SetAssetMgtConfigInfoResponse.php');

class Asset extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'GetAssetMgtConfigInfo' => '\GetAssetMgtConfigInfo',
      'GetAssetMgtConfigInfoResponse' => '\GetAssetMgtConfigInfoResponse',
      'AssetConfigInfo' => '\AssetConfigInfo',
      'AuthenticationHeader' => '\AuthenticationHeader',
      'RequestInfoParameters' => '\RequestInfoParameters',
      'GetAssetMgtInfo' => '\GetAssetMgtInfo',
      'GetAssetMgtInfoResponse' => '\GetAssetMgtInfoResponse',
      'AssetInfoData' => '\AssetInfoData',
      'AssetSuperTypeData' => '\AssetSuperTypeData',
      'AssetTypeData' => '\AssetTypeData',
      'GetAssetSupertypes' => '\GetAssetSupertypes',
      'GetAssetSupertypesResponse' => '\GetAssetSupertypesResponse',
      'GetEditSnippet' => '\GetEditSnippet',
      'GetEditSnippetResponse' => '\GetEditSnippetResponse',
      'GetMyAssetMgtID' => '\GetMyAssetMgtID',
      'GetMyAssetMgtIDResponse' => '\GetMyAssetMgtIDResponse',
      'MakeCopyDMSAsset' => '\MakeCopyDMSAsset',
      'MakeCopyDMSAssetResponse' => '\MakeCopyDMSAssetResponse',
      'SetAssetMgtConfigInfo' => '\SetAssetMgtConfigInfo',
      'SetAssetMgtConfigInfoResponse' => '\SetAssetMgtConfigInfoResponse');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = 'Asset.wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      
      parent::__construct($wsdl, $options);
    }

    /**
     * @param GetAssetMgtConfigInfo $parameters
     * @access public
     * @return GetAssetMgtConfigInfoResponse
     */
    public function GetAssetMgtConfigInfo(GetAssetMgtConfigInfo $parameters)
    {
      return $this->__soapCall('GetAssetMgtConfigInfo', array($parameters));
    }

    /**
     * @param GetAssetMgtInfo $parameters
     * @access public
     * @return GetAssetMgtInfoResponse
     */
    public function GetAssetMgtInfo(GetAssetMgtInfo $parameters)
    {
      return $this->__soapCall('GetAssetMgtInfo', array($parameters));
    }

    /**
     * @param GetAssetSupertypes $parameters
     * @access public
     * @return GetAssetSupertypesResponse
     */
    public function GetAssetSupertypes(GetAssetSupertypes $parameters)
    {
      return $this->__soapCall('GetAssetSupertypes', array($parameters));
    }

    /**
     * @param GetEditSnippet $parameters
     * @access public
     * @return GetEditSnippetResponse
     */
    public function GetEditSnippet(GetEditSnippet $parameters)
    {
      return $this->__soapCall('GetEditSnippet', array($parameters));
    }

    /**
     * @param GetMyAssetMgtID $parameters
     * @access public
     * @return GetMyAssetMgtIDResponse
     */
    public function GetMyAssetMgtID(GetMyAssetMgtID $parameters)
    {
      return $this->__soapCall('GetMyAssetMgtID', array($parameters));
    }

    /**
     * @param MakeCopyDMSAsset $parameters
     * @access public
     * @return MakeCopyDMSAssetResponse
     */
    public function MakeCopyDMSAsset(MakeCopyDMSAsset $parameters)
    {
      return $this->__soapCall('MakeCopyDMSAsset', array($parameters));
    }

    /**
     * @param SetAssetMgtConfigInfo $parameters
     * @access public
     * @return SetAssetMgtConfigInfoResponse
     */
    public function SetAssetMgtConfigInfo(SetAssetMgtConfigInfo $parameters)
    {
      return $this->__soapCall('SetAssetMgtConfigInfo', array($parameters));
    }

}
