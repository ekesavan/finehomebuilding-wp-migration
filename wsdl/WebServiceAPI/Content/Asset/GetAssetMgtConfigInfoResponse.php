<?php

class GetAssetMgtConfigInfoResponse
{

    /**
     * @var AssetConfigInfo[] $GetAssetMgtConfigInfoResult
     * @access public
     */
    public $GetAssetMgtConfigInfoResult = null;

    /**
     * @param AssetConfigInfo[] $GetAssetMgtConfigInfoResult
     * @access public
     */
    public function __construct($GetAssetMgtConfigInfoResult)
    {
      $this->GetAssetMgtConfigInfoResult = $GetAssetMgtConfigInfoResult;
    }

}
