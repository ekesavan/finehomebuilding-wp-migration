<?php

class GetFormDataHistogramResponse
{

    /**
     * @var AnyType[] $GetFormDataHistogramResult
     * @access public
     */
    public $GetFormDataHistogramResult = null;

    /**
     * @param AnyType[] $GetFormDataHistogramResult
     * @access public
     */
    public function __construct($GetFormDataHistogramResult)
    {
      $this->GetFormDataHistogramResult = $GetFormDataHistogramResult;
    }

}
