<?php

class GetFormFieldData_x0020_By_x0020_FormID_x0020_and_x0020_FormDataIdResponse
{

    /**
     * @var FormSubmittedData $GetFormFieldData_x0020_By_x0020_FormID_x0020_and_x0020_FormDataIdResult
     * @access public
     */
    public $GetFormFieldData_x0020_By_x0020_FormID_x0020_and_x0020_FormDataIdResult = null;

    /**
     * @param FormSubmittedData $GetFormFieldData_x0020_By_x0020_FormID_x0020_and_x0020_FormDataIdResult
     * @access public
     */
    public function __construct($GetFormFieldData_x0020_By_x0020_FormID_x0020_and_x0020_FormDataIdResult)
    {
      $this->GetFormFieldData_x0020_By_x0020_FormID_x0020_and_x0020_FormDataIdResult = $GetFormFieldData_x0020_By_x0020_FormID_x0020_and_x0020_FormDataIdResult;
    }

}
