<?php

class FormFieldDataItem
{

    /**
     * @var string $FieldName
     * @access public
     */
    public $FieldName = null;

    /**
     * @var anyType $DataValue
     * @access public
     */
    public $DataValue = null;

    /**
     * @param string $FieldName
     * @param anyType $DataValue
     * @access public
     */
    public function __construct($FieldName, $DataValue)
    {
      $this->FieldName = $FieldName;
      $this->DataValue = $DataValue;
    }

}
