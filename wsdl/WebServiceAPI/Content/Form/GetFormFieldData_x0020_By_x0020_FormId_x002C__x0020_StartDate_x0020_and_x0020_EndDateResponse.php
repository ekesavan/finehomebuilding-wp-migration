<?php

class GetFormFieldData_x0020_By_x0020_FormId_x002C__x0020_StartDate_x0020_and_x0020_EndDateResponse
{

    /**
     * @var FormSubmittedData[] $GetFormFieldData_x0020_By_x0020_FormId_x002C__x0020_StartDate_x0020_and_x0020_EndDateResult
     * @access public
     */
    public $GetFormFieldData_x0020_By_x0020_FormId_x002C__x0020_StartDate_x0020_and_x0020_EndDateResult = null;

    /**
     * @param FormSubmittedData[] $GetFormFieldData_x0020_By_x0020_FormId_x002C__x0020_StartDate_x0020_and_x0020_EndDateResult
     * @access public
     */
    public function __construct($GetFormFieldData_x0020_By_x0020_FormId_x002C__x0020_StartDate_x0020_and_x0020_EndDateResult)
    {
      $this->GetFormFieldData_x0020_By_x0020_FormId_x002C__x0020_StartDate_x0020_and_x0020_EndDateResult = $GetFormFieldData_x0020_By_x0020_FormId_x002C__x0020_StartDate_x0020_and_x0020_EndDateResult;
    }

}
