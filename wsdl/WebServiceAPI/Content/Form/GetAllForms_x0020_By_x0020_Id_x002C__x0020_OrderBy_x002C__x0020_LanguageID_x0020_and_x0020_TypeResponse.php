<?php

class GetAllForms_x0020_By_x0020_Id_x002C__x0020_OrderBy_x002C__x0020_LanguageID_x0020_and_x0020_TypeResponse
{

    /**
     * @var FormData[] $GetAllForms_x0020_By_x0020_Id_x002C__x0020_OrderBy_x002C__x0020_LanguageID_x0020_and_x0020_TypeResult
     * @access public
     */
    public $GetAllForms_x0020_By_x0020_Id_x002C__x0020_OrderBy_x002C__x0020_LanguageID_x0020_and_x0020_TypeResult = null;

    /**
     * @param FormData[] $GetAllForms_x0020_By_x0020_Id_x002C__x0020_OrderBy_x002C__x0020_LanguageID_x0020_and_x0020_TypeResult
     * @access public
     */
    public function __construct($GetAllForms_x0020_By_x0020_Id_x002C__x0020_OrderBy_x002C__x0020_LanguageID_x0020_and_x0020_TypeResult)
    {
      $this->GetAllForms_x0020_By_x0020_Id_x002C__x0020_OrderBy_x002C__x0020_LanguageID_x0020_and_x0020_TypeResult = $GetAllForms_x0020_By_x0020_Id_x002C__x0020_OrderBy_x002C__x0020_LanguageID_x0020_and_x0020_TypeResult;
    }

}
