<?php

class GetAllForms_x0020_By_x0020_Id_x002C__x0020_OrderBy_x002C__x0020_LanguageID_x0020_and_x0020_Type
{

    /**
     * @var int $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var string $OrderBy
     * @access public
     */
    public $OrderBy = null;

    /**
     * @var int $LanguageId
     * @access public
     */
    public $LanguageId = null;

    /**
     * @var FormResultType $Type
     * @access public
     */
    public $Type = null;

    /**
     * @param int $Id
     * @param string $OrderBy
     * @param int $LanguageId
     * @param FormResultType $Type
     * @access public
     */
    public function __construct($Id, $OrderBy, $LanguageId, $Type)
    {
      $this->Id = $Id;
      $this->OrderBy = $OrderBy;
      $this->LanguageId = $LanguageId;
      $this->Type = $Type;
    }

}
