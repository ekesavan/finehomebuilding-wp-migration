<?php

class FormFieldList
{

    /**
     * @var FormFieldDefinition[] $field
     * @access public
     */
    public $field = null;

    /**
     * @var FormFieldDatalist[] $datalist
     * @access public
     */
    public $datalist = null;

    /**
     * @param FormFieldDefinition[] $field
     * @param FormFieldDatalist[] $datalist
     * @access public
     */
    public function __construct($field, $datalist)
    {
      $this->field = $field;
      $this->datalist = $datalist;
    }

}
