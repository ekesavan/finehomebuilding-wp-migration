<?php

include_once('ContentData.php');

class FormData extends ContentData
{

    /**
     * @var string $Description
     * @access public
     */
    public $Description = null;

    /**
     * @var boolean $LimitSubmissions
     * @access public
     */
    public $LimitSubmissions = null;

    /**
     * @var int $AllowedNumberOfSubmissions
     * @access public
     */
    public $AllowedNumberOfSubmissions = null;

    /**
     * @var string $StoreDataTo
     * @access public
     */
    public $StoreDataTo = null;

    /**
     * @var boolean $Autofill
     * @access public
     */
    public $Autofill = null;

    /**
     * @var string $MailTo
     * @access public
     */
    public $MailTo = null;

    /**
     * @var string $MailFrom
     * @access public
     */
    public $MailFrom = null;

    /**
     * @var string $MailSubject
     * @access public
     */
    public $MailSubject = null;

    /**
     * @var string $MailPreamble
     * @access public
     */
    public $MailPreamble = null;

    /**
     * @var string $MailCc
     * @access public
     */
    public $MailCc = null;

    /**
     * @var boolean $SendXmlPacket
     * @access public
     */
    public $SendXmlPacket = null;

    /**
     * @var FormValidationData[] $Validation
     * @access public
     */
    public $Validation = null;

    /**
     * @var string $PollRevision
     * @access public
     */
    public $PollRevision = null;

    /**
     * @var boolean $CreateSubscriber
     * @access public
     */
    public $CreateSubscriber = null;

    /**
     * @var boolean $UpdateSubscriber
     * @access public
     */
    public $UpdateSubscriber = null;

    /**
     * @param int $Id
     * @param int $LanguageId
     * @param string $Title
     * @param string $Teaser
     * @param string $Html
     * @param string $Quicklink
     * @param string $Image
     * @param boolean $IsPrivate
     * @param int $Type
     * @param CMSContentSubtype $SubType
     * @param int $ExternalTypeId
     * @param AssetData $AssetData
     * @param string $EditorFirstName
     * @param string $EditorLastName
     * @param string $Comment
     * @param dateTime $DateModified
     * @param string $DisplayLastEditDate
     * @param dateTime $DateCreated
     * @param string $DisplayDateCreated
     * @param int $UserId
     * @param int $FolderId
     * @param boolean $IsPermissionsInherited
     * @param int $PermissionInheritedFrom
     * @param string $Status
     * @param string $DisplayGoLive
     * @param boolean $IsPublished
     * @param boolean $IsSearchable
     * @param int $XmlInheritedFrom
     * @param ContentMetaData[] $MetaData
     * @param string $DisplayEndDate
     * @param dateTime $ExpireDate
     * @param CMSEndDateAction $EndDateActionType
     * @param XmlConfigData $XmlConfiguration
     * @param string $StyleSheet
     * @param string $LanguageDescription
     * @param string $Text
     * @param string $Path
     * @param string $ContentPath
     * @param int $ContType
     * @param int $Updates
     * @param string $FolderName
     * @param string $MediaText
     * @param int $HistoryId
     * @param string $HyperLink
     * @param TemplateData $TemplateConfiguration
     * @param int $FlagDefId
     * @param string $Description
     * @param boolean $LimitSubmissions
     * @param string $StoreDataTo
     * @param boolean $Autofill
     * @param string $MailTo
     * @param string $MailFrom
     * @param string $MailSubject
     * @param string $MailPreamble
     * @param string $MailCc
     * @param boolean $SendXmlPacket
     * @param FormValidationData[] $Validation
     * @param string $PollRevision
     * @param boolean $CreateSubscriber
     * @param boolean $UpdateSubscriber
     * @access public
     */
    public function __construct($Id, $LanguageId, $Title, $Teaser, $Html, $Quicklink, $Image, $IsPrivate, $Type, $SubType, $ExternalTypeId, $AssetData, $EditorFirstName, $EditorLastName, $Comment, $DateModified, $DisplayLastEditDate, $DateCreated, $DisplayDateCreated, $UserId, $FolderId, $IsPermissionsInherited, $PermissionInheritedFrom, $Status, $DisplayGoLive, $IsPublished, $IsSearchable, $XmlInheritedFrom, $MetaData, $DisplayEndDate, $ExpireDate, $EndDateActionType, $XmlConfiguration, $StyleSheet, $LanguageDescription, $Text, $Path, $ContentPath, $ContType, $Updates, $FolderName, $MediaText, $HistoryId, $HyperLink, $TemplateConfiguration, $FlagDefId, $Description, $LimitSubmissions, $StoreDataTo, $Autofill, $MailTo, $MailFrom, $MailSubject, $MailPreamble, $MailCc, $SendXmlPacket, $Validation, $PollRevision, $CreateSubscriber, $UpdateSubscriber)
    {
      parent::__construct($Id, $LanguageId, $Title, $Teaser, $Html, $Quicklink, $Image, $IsPrivate, $Type, $SubType, $ExternalTypeId, $AssetData, $EditorFirstName, $EditorLastName, $Comment, $DateModified, $DisplayLastEditDate, $DateCreated, $DisplayDateCreated, $UserId, $FolderId, $IsPermissionsInherited, $PermissionInheritedFrom, $Status, $DisplayGoLive, $IsPublished, $IsSearchable, $XmlInheritedFrom, $MetaData, $DisplayEndDate, $ExpireDate, $EndDateActionType, $XmlConfiguration, $StyleSheet, $LanguageDescription, $Text, $Path, $ContentPath, $ContType, $Updates, $FolderName, $MediaText, $HistoryId, $HyperLink, $TemplateConfiguration, $FlagDefId);
      $this->Description = $Description;
      $this->LimitSubmissions = $LimitSubmissions;
      $this->StoreDataTo = $StoreDataTo;
      $this->Autofill = $Autofill;
      $this->MailTo = $MailTo;
      $this->MailFrom = $MailFrom;
      $this->MailSubject = $MailSubject;
      $this->MailPreamble = $MailPreamble;
      $this->MailCc = $MailCc;
      $this->SendXmlPacket = $SendXmlPacket;
      $this->Validation = $Validation;
      $this->PollRevision = $PollRevision;
      $this->CreateSubscriber = $CreateSubscriber;
      $this->UpdateSubscriber = $UpdateSubscriber;
    }

}
