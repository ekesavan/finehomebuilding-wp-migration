<?php

class FormValidationData
{

    /**
     * @var int $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var string $Type
     * @access public
     */
    public $Type = null;

    /**
     * @var string $MinVal
     * @access public
     */
    public $MinVal = null;

    /**
     * @var string $MaxVal
     * @access public
     */
    public $MaxVal = null;

    /**
     * @var string $Message
     * @access public
     */
    public $Message = null;

    /**
     * @var string $JsCall
     * @access public
     */
    public $JsCall = null;

    /**
     * @param int $Id
     * @param string $Type
     * @param string $MinVal
     * @param string $MaxVal
     * @param string $Message
     * @param string $JsCall
     * @access public
     */
    public function __construct($Id, $Type, $MinVal, $MaxVal, $Message, $JsCall)
    {
      $this->Id = $Id;
      $this->Type = $Type;
      $this->MinVal = $MinVal;
      $this->MaxVal = $MaxVal;
      $this->Message = $Message;
      $this->JsCall = $JsCall;
    }

}
