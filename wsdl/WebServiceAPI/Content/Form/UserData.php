<?php

include_once('UserBaseData.php');

class UserData extends UserBaseData
{

    /**
     * @var guid $SessionId
     * @access public
     */
    public $SessionId = null;

    /**
     * @var int $LoginAttempts
     * @access public
     */
    public $LoginAttempts = null;

    /**
     * @var string $DateCreated
     * @access public
     */
    public $DateCreated = null;

    /**
     * @var dateTime $DateModified
     * @access public
     */
    public $DateModified = null;

    /**
     * @var string $LastLoginDate
     * @access public
     */
    public $LastLoginDate = null;

    /**
     * @var dateTime $LastPasswordChangeDate
     * @access public
     */
    public $LastPasswordChangeDate = null;

    /**
     * @var string $Signature
     * @access public
     */
    public $Signature = null;

    /**
     * @var boolean $AcceptedTerms
     * @access public
     */
    public $AcceptedTerms = null;

    /**
     * @var UserPreferenceData $UserPreference
     * @access public
     */
    public $UserPreference = null;

    /**
     * @var boolean $IsMemberShip
     * @access public
     */
    public $IsMemberShip = null;

    /**
     * @var string $LoginIdentification
     * @access public
     */
    public $LoginIdentification = null;

    /**
     * @var string $LanguageName
     * @access public
     */
    public $LanguageName = null;

    /**
     * @var int $LanguageId
     * @access public
     */
    public $LanguageId = null;

    /**
     * @var boolean $IsDisableMessage
     * @access public
     */
    public $IsDisableMessage = null;

    /**
     * @var boolean $IsDeleted
     * @access public
     */
    public $IsDeleted = null;

    /**
     * @var string $EditorOption
     * @access public
     */
    public $EditorOption = null;

    /**
     * @var UserRank $Rank
     * @access public
     */
    public $Rank = null;

    /**
     * @var int $AuthenticationTypeId
     * @access public
     */
    public $AuthenticationTypeId = null;

    /**
     * @var string $AuthenticationUserId
     * @access public
     */
    public $AuthenticationUserId = null;

    /**
     * @var string $AuthenticationToken
     * @access public
     */
    public $AuthenticationToken = null;

    /**
     * @var dateTime $AuthenticationTokenExpires
     * @access public
     */
    public $AuthenticationTokenExpires = null;

    /**
     * @var CustomAttribute[] $CustomProperties
     * @access public
     */
    public $CustomProperties = null;

    /**
     * @param int $Id
     * @param string $Avatar
     * @param string $Username
     * @param string $FirstName
     * @param string $LastName
     * @param string $Domain
     * @param string $DisplayUserName
     * @param string $Password
     * @param string $DisplayName
     * @param string $Email
     * @param string $Address
     * @param float $Latitude
     * @param float $Longitude
     * @param guid $SessionId
     * @param int $LoginAttempts
     * @param string $DateCreated
     * @param dateTime $DateModified
     * @param string $LastLoginDate
     * @param dateTime $LastPasswordChangeDate
     * @param string $Signature
     * @param boolean $AcceptedTerms
     * @param UserPreferenceData $UserPreference
     * @param boolean $IsMemberShip
     * @param string $LoginIdentification
     * @param string $LanguageName
     * @param int $LanguageId
     * @param boolean $IsDisableMessage
     * @param boolean $IsDeleted
     * @param string $EditorOption
     * @param UserRank $Rank
     * @param int $AuthenticationTypeId
     * @param string $AuthenticationUserId
     * @param string $AuthenticationToken
     * @param dateTime $AuthenticationTokenExpires
     * @param CustomAttribute[] $CustomProperties
     * @access public
     */
    public function __construct($Id, $Avatar, $Username, $FirstName, $LastName, $Domain, $DisplayUserName, $Password, $DisplayName, $Email, $Address, $Latitude, $Longitude, $SessionId, $LoginAttempts, $DateCreated, $DateModified, $LastLoginDate, $LastPasswordChangeDate, $Signature, $AcceptedTerms, $UserPreference, $IsMemberShip, $LoginIdentification, $LanguageName, $LanguageId, $IsDisableMessage, $IsDeleted, $EditorOption, $Rank, $AuthenticationTypeId, $AuthenticationUserId, $AuthenticationToken, $AuthenticationTokenExpires, $CustomProperties)
    {
      parent::__construct($Id, $Avatar, $Username, $FirstName, $LastName, $Domain, $DisplayUserName, $Password, $DisplayName, $Email, $Address, $Latitude, $Longitude);
      $this->SessionId = $SessionId;
      $this->LoginAttempts = $LoginAttempts;
      $this->DateCreated = $DateCreated;
      $this->DateModified = $DateModified;
      $this->LastLoginDate = $LastLoginDate;
      $this->LastPasswordChangeDate = $LastPasswordChangeDate;
      $this->Signature = $Signature;
      $this->AcceptedTerms = $AcceptedTerms;
      $this->UserPreference = $UserPreference;
      $this->IsMemberShip = $IsMemberShip;
      $this->LoginIdentification = $LoginIdentification;
      $this->LanguageName = $LanguageName;
      $this->LanguageId = $LanguageId;
      $this->IsDisableMessage = $IsDisableMessage;
      $this->IsDeleted = $IsDeleted;
      $this->EditorOption = $EditorOption;
      $this->Rank = $Rank;
      $this->AuthenticationTypeId = $AuthenticationTypeId;
      $this->AuthenticationUserId = $AuthenticationUserId;
      $this->AuthenticationToken = $AuthenticationToken;
      $this->AuthenticationTokenExpires = $AuthenticationTokenExpires;
      $this->CustomProperties = $CustomProperties;
    }

}
