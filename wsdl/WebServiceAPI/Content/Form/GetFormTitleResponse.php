<?php

class GetFormTitleResponse
{

    /**
     * @var string $GetFormTitleResult
     * @access public
     */
    public $GetFormTitleResult = null;

    /**
     * @param string $GetFormTitleResult
     * @access public
     */
    public function __construct($GetFormTitleResult)
    {
      $this->GetFormTitleResult = $GetFormTitleResult;
    }

}
