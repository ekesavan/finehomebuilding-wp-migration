<?php

class GetAllFormsResponse
{

    /**
     * @var FormData[] $GetAllFormsResult
     * @access public
     */
    public $GetAllFormsResult = null;

    /**
     * @param FormData[] $GetAllFormsResult
     * @access public
     */
    public function __construct($GetAllFormsResult)
    {
      $this->GetAllFormsResult = $GetAllFormsResult;
    }

}
