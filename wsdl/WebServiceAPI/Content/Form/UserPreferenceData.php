<?php

class UserPreferenceData
{

    /**
     * @var string $Template
     * @access public
     */
    public $Template = null;

    /**
     * @var string $FolderPath
     * @access public
     */
    public $FolderPath = null;

    /**
     * @var string $DisplayBorders
     * @access public
     */
    public $DisplayBorders = null;

    /**
     * @var string $DisplayTitleText
     * @access public
     */
    public $DisplayTitleText = null;

    /**
     * @var string $FolderId
     * @access public
     */
    public $FolderId = null;

    /**
     * @var int $Width
     * @access public
     */
    public $Width = null;

    /**
     * @var int $Height
     * @access public
     */
    public $Height = null;

    /**
     * @var boolean $ForceSetting
     * @access public
     */
    public $ForceSetting = null;

    /**
     * @var int $PageSize
     * @access public
     */
    public $PageSize = null;

    /**
     * @param string $Template
     * @param string $FolderPath
     * @param string $DisplayBorders
     * @param string $DisplayTitleText
     * @param string $FolderId
     * @param int $Width
     * @param int $Height
     * @param boolean $ForceSetting
     * @param int $PageSize
     * @access public
     */
    public function __construct($Template, $FolderPath, $DisplayBorders, $DisplayTitleText, $FolderId, $Width, $Height, $ForceSetting, $PageSize)
    {
      $this->Template = $Template;
      $this->FolderPath = $FolderPath;
      $this->DisplayBorders = $DisplayBorders;
      $this->DisplayTitleText = $DisplayTitleText;
      $this->FolderId = $FolderId;
      $this->Width = $Width;
      $this->Height = $Height;
      $this->ForceSetting = $ForceSetting;
      $this->PageSize = $PageSize;
    }

}
