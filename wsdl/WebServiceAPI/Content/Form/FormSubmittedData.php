<?php

include_once('CmsDataOfFormSubmittedData.php');

class FormSubmittedData extends CmsDataOfFormSubmittedData
{

    /**
     * @var int $FormDataID
     * @access public
     */
    public $FormDataID = null;

    /**
     * @var int $FormId
     * @access public
     */
    public $FormId = null;

    /**
     * @var dateTime $DateSubmitted
     * @access public
     */
    public $DateSubmitted = null;

    /**
     * @var int $UserId
     * @access public
     */
    public $UserId = null;

    /**
     * @var UserData $UserInfo
     * @access public
     */
    public $UserInfo = null;

    /**
     * @var FormFieldDataItem[] $DataItems
     * @access public
     */
    public $DataItems = null;

    /**
     * @param int $Id
     * @param int $FormDataID
     * @param int $FormId
     * @param dateTime $DateSubmitted
     * @param int $UserId
     * @param UserData $UserInfo
     * @param FormFieldDataItem[] $DataItems
     * @access public
     */
    public function __construct($Id, $FormDataID, $FormId, $DateSubmitted, $UserId, $UserInfo, $DataItems)
    {
      parent::__construct($Id);
      $this->FormDataID = $FormDataID;
      $this->FormId = $FormId;
      $this->DateSubmitted = $DateSubmitted;
      $this->UserId = $UserId;
      $this->UserInfo = $UserInfo;
      $this->DataItems = $DataItems;
    }

}
