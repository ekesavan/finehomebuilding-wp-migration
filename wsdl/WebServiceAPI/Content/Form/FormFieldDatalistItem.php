<?php

class FormFieldDatalistItem
{

    /**
     * @var string $_
     * @access public
     */
    public $_ = null;

    /**
     * @var string $value
     * @access public
     */
    public $value = null;

    /**
     * @var boolean $default
     * @access public
     */
    public $default = null;

    /**
     * @var boolean $enabled
     * @access public
     */
    public $enabled = null;

    /**
     * @param string $_
     * @param string $value
     * @param boolean $default
     * @param boolean $enabled
     * @access public
     */
    public function __construct($_, $value, $default, $enabled)
    {
      $this->_ = $_;
      $this->value = $value;
      $this->default = $default;
      $this->enabled = $enabled;
    }

}
