<?php

class FormFieldDefinition
{

    /**
     * @var string $_
     * @access public
     */
    public $_ = null;

    /**
     * @var string $name
     * @access public
     */
    public $name = null;

    /**
     * @var string $datatype
     * @access public
     */
    public $datatype = null;

    /**
     * @var string $basetype
     * @access public
     */
    public $basetype = null;

    /**
     * @var string $content
     * @access public
     */
    public $content = null;

    /**
     * @var string $xpath
     * @access public
     */
    public $xpath = null;

    /**
     * @var string $datalist
     * @access public
     */
    public $datalist = null;

    /**
     * @var UNKNOWN $lang
     * @access public
     */
    public $lang = null;

    /**
     * @param string $_
     * @param string $name
     * @param string $datatype
     * @param string $basetype
     * @param string $content
     * @param string $xpath
     * @param string $datalist
     * @param UNKNOWN $lang
     * @access public
     */
    public function __construct($_, $name, $datatype, $basetype, $content, $xpath, $datalist, $lang)
    {
      $this->_ = $_;
      $this->name = $name;
      $this->datatype = $datatype;
      $this->basetype = $basetype;
      $this->content = $content;
      $this->xpath = $xpath;
      $this->datalist = $datalist;
      $this->lang = $lang;
    }

}
