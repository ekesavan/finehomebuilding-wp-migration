<?php

class UpdateFormFieldData
{

    /**
     * @var int $FormId
     * @access public
     */
    public $FormId = null;

    /**
     * @var int $FormDataId
     * @access public
     */
    public $FormDataId = null;

    /**
     * @var FormSubmittedData $SubmittedData
     * @access public
     */
    public $SubmittedData = null;

    /**
     * @param int $FormId
     * @param int $FormDataId
     * @param FormSubmittedData $SubmittedData
     * @access public
     */
    public function __construct($FormId, $FormDataId, $SubmittedData)
    {
      $this->FormId = $FormId;
      $this->FormDataId = $FormDataId;
      $this->SubmittedData = $SubmittedData;
    }

}
