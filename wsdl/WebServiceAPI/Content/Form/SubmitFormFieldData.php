<?php

class SubmitFormFieldData
{

    /**
     * @var int $FormId
     * @access public
     */
    public $FormId = null;

    /**
     * @var FormSubmittedData $Data
     * @access public
     */
    public $Data = null;

    /**
     * @var boolean $SendNotification
     * @access public
     */
    public $SendNotification = null;

    /**
     * @var string $SubmittedDataXml
     * @access public
     */
    public $SubmittedDataXml = null;

    /**
     * @param int $FormId
     * @param FormSubmittedData $Data
     * @param boolean $SendNotification
     * @param string $SubmittedDataXml
     * @access public
     */
    public function __construct($FormId, $Data, $SendNotification, $SubmittedDataXml)
    {
      $this->FormId = $FormId;
      $this->Data = $Data;
      $this->SendNotification = $SendNotification;
      $this->SubmittedDataXml = $SubmittedDataXml;
    }

}
