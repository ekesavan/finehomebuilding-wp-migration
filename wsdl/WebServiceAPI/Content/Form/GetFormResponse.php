<?php

class GetFormResponse
{

    /**
     * @var FormData $GetFormResult
     * @access public
     */
    public $GetFormResult = null;

    /**
     * @param FormData $GetFormResult
     * @access public
     */
    public function __construct($GetFormResult)
    {
      $this->GetFormResult = $GetFormResult;
    }

}
