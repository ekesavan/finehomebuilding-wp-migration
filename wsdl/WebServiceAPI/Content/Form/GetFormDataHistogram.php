<?php

class GetFormDataHistogram
{

    /**
     * @var int $FormId
     * @access public
     */
    public $FormId = null;

    /**
     * @param int $FormId
     * @access public
     */
    public function __construct($FormId)
    {
      $this->FormId = $FormId;
    }

}
