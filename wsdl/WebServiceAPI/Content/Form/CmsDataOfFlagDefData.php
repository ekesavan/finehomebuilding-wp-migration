<?php

include_once('BaseDataOfFlagDefData.php');

class CmsDataOfFlagDefData extends BaseDataOfFlagDefData
{

    /**
     * @var int $Id
     * @access public
     */
    public $Id = null;

    /**
     * @param int $Id
     * @access public
     */
    public function __construct($Id)
    {
      $this->Id = $Id;
    }

}
