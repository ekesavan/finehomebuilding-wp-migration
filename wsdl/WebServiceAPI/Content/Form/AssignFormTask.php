<?php

class AssignFormTask
{

    /**
     * @var FormData $FormInfo
     * @access public
     */
    public $FormInfo = null;

    /**
     * @var string $SubmittedDataXml
     * @access public
     */
    public $SubmittedDataXml = null;

    /**
     * @param FormData $FormInfo
     * @param string $SubmittedDataXml
     * @access public
     */
    public function __construct($FormInfo, $SubmittedDataXml)
    {
      $this->FormInfo = $FormInfo;
      $this->SubmittedDataXml = $SubmittedDataXml;
    }

}
