<?php

class GetFormFieldData_x0020_By_x0020_FormId_x002C__x0020_StartDate_x0020_and_x0020_EndDate
{

    /**
     * @var int $FormId
     * @access public
     */
    public $FormId = null;

    /**
     * @var string $StartDate
     * @access public
     */
    public $StartDate = null;

    /**
     * @var string $EndDate
     * @access public
     */
    public $EndDate = null;

    /**
     * @param int $FormId
     * @param string $StartDate
     * @param string $EndDate
     * @access public
     */
    public function __construct($FormId, $StartDate, $EndDate)
    {
      $this->FormId = $FormId;
      $this->StartDate = $StartDate;
      $this->EndDate = $EndDate;
    }

}
