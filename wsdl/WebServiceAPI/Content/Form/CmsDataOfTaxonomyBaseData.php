<?php

include_once('BaseDataOfTaxonomyBaseData.php');

class CmsDataOfTaxonomyBaseData extends BaseDataOfTaxonomyBaseData
{

    /**
     * @var int $Id
     * @access public
     */
    public $Id = null;

    /**
     * @param int $Id
     * @access public
     */
    public function __construct($Id)
    {
      $this->Id = $Id;
    }

}
