<?php

include_once('BaseDataOfFormSubmittedData.php');

class CmsDataOfFormSubmittedData extends BaseDataOfFormSubmittedData
{

    /**
     * @var int $Id
     * @access public
     */
    public $Id = null;

    /**
     * @param int $Id
     * @access public
     */
    public function __construct($Id)
    {
      $this->Id = $Id;
    }

}
