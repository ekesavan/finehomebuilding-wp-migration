<?php

include_once('ContentBaseData.php');

class ContentData extends ContentBaseData
{

    /**
     * @var string $EditorFirstName
     * @access public
     */
    public $EditorFirstName = null;

    /**
     * @var string $EditorLastName
     * @access public
     */
    public $EditorLastName = null;

    /**
     * @var string $Comment
     * @access public
     */
    public $Comment = null;

    /**
     * @var dateTime $DateModified
     * @access public
     */
    public $DateModified = null;

    /**
     * @var string $DisplayLastEditDate
     * @access public
     */
    public $DisplayLastEditDate = null;

    /**
     * @var dateTime $DateCreated
     * @access public
     */
    public $DateCreated = null;

    /**
     * @var string $DisplayDateCreated
     * @access public
     */
    public $DisplayDateCreated = null;

    /**
     * @var int $UserId
     * @access public
     */
    public $UserId = null;

    /**
     * @var int $FolderId
     * @access public
     */
    public $FolderId = null;

    /**
     * @var boolean $IsPermissionsInherited
     * @access public
     */
    public $IsPermissionsInherited = null;

    /**
     * @var int $PermissionInheritedFrom
     * @access public
     */
    public $PermissionInheritedFrom = null;

    /**
     * @var string $Status
     * @access public
     */
    public $Status = null;

    /**
     * @var dateTime $GoLiveDate
     * @access public
     */
    public $GoLiveDate = null;

    /**
     * @var string $DisplayGoLive
     * @access public
     */
    public $DisplayGoLive = null;

    /**
     * @var boolean $IsPublished
     * @access public
     */
    public $IsPublished = null;

    /**
     * @var boolean $IsSearchable
     * @access public
     */
    public $IsSearchable = null;

    /**
     * @var int $XmlInheritedFrom
     * @access public
     */
    public $XmlInheritedFrom = null;

    /**
     * @var ContentMetaData[] $MetaData
     * @access public
     */
    public $MetaData = null;

    /**
     * @var string $DisplayEndDate
     * @access public
     */
    public $DisplayEndDate = null;

    /**
     * @var dateTime $ExpireDate
     * @access public
     */
    public $ExpireDate = null;

    /**
     * @var CMSEndDateAction $EndDateActionType
     * @access public
     */
    public $EndDateActionType = null;

    /**
     * @var XmlConfigData $XmlConfiguration
     * @access public
     */
    public $XmlConfiguration = null;

    /**
     * @var string $StyleSheet
     * @access public
     */
    public $StyleSheet = null;

    /**
     * @var string $LanguageDescription
     * @access public
     */
    public $LanguageDescription = null;

    /**
     * @var string $Text
     * @access public
     */
    public $Text = null;

    /**
     * @var string $Path
     * @access public
     */
    public $Path = null;

    /**
     * @var string $ContentPath
     * @access public
     */
    public $ContentPath = null;

    /**
     * @var int $ContType
     * @access public
     */
    public $ContType = null;

    /**
     * @var int $Updates
     * @access public
     */
    public $Updates = null;

    /**
     * @var string $FolderName
     * @access public
     */
    public $FolderName = null;

    /**
     * @var string $MediaText
     * @access public
     */
    public $MediaText = null;

    /**
     * @var int $HistoryId
     * @access public
     */
    public $HistoryId = null;

    /**
     * @var string $HyperLink
     * @access public
     */
    public $HyperLink = null;

    /**
     * @var TemplateData $TemplateConfiguration
     * @access public
     */
    public $TemplateConfiguration = null;

    /**
     * @var int $FlagDefId
     * @access public
     */
    public $FlagDefId = null;

    /**
     * @param int $Id
     * @param int $LanguageId
     * @param string $Title
     * @param string $Teaser
     * @param string $Html
     * @param string $Quicklink
     * @param string $Image
     * @param boolean $IsPrivate
     * @param int $Type
     * @param CMSContentSubtype $SubType
     * @param int $ExternalTypeId
     * @param AssetData $AssetData
     * @param string $EditorFirstName
     * @param string $EditorLastName
     * @param string $Comment
     * @param dateTime $DateModified
     * @param string $DisplayLastEditDate
     * @param dateTime $DateCreated
     * @param string $DisplayDateCreated
     * @param int $UserId
     * @param int $FolderId
     * @param boolean $IsPermissionsInherited
     * @param int $PermissionInheritedFrom
     * @param string $Status
     * @param string $DisplayGoLive
     * @param boolean $IsPublished
     * @param boolean $IsSearchable
     * @param int $XmlInheritedFrom
     * @param ContentMetaData[] $MetaData
     * @param string $DisplayEndDate
     * @param dateTime $ExpireDate
     * @param CMSEndDateAction $EndDateActionType
     * @param XmlConfigData $XmlConfiguration
     * @param string $StyleSheet
     * @param string $LanguageDescription
     * @param string $Text
     * @param string $Path
     * @param string $ContentPath
     * @param int $ContType
     * @param int $Updates
     * @param string $FolderName
     * @param string $MediaText
     * @param int $HistoryId
     * @param string $HyperLink
     * @param TemplateData $TemplateConfiguration
     * @param int $FlagDefId
     * @access public
     */
    public function __construct($Id, $LanguageId, $Title, $Teaser, $Html, $Quicklink, $Image, $IsPrivate, $Type, $SubType, $ExternalTypeId, $AssetData, $EditorFirstName, $EditorLastName, $Comment, $DateModified, $DisplayLastEditDate, $DateCreated, $DisplayDateCreated, $UserId, $FolderId, $IsPermissionsInherited, $PermissionInheritedFrom, $Status, $DisplayGoLive, $IsPublished, $IsSearchable, $XmlInheritedFrom, $MetaData, $DisplayEndDate, $ExpireDate, $EndDateActionType, $XmlConfiguration, $StyleSheet, $LanguageDescription, $Text, $Path, $ContentPath, $ContType, $Updates, $FolderName, $MediaText, $HistoryId, $HyperLink, $TemplateConfiguration, $FlagDefId)
    {
      parent::__construct($Id, $LanguageId, $Title, $Teaser, $Html, $Quicklink, $Image, $IsPrivate, $Type, $SubType, $ExternalTypeId, $AssetData);
      $this->EditorFirstName = $EditorFirstName;
      $this->EditorLastName = $EditorLastName;
      $this->Comment = $Comment;
      $this->DateModified = $DateModified;
      $this->DisplayLastEditDate = $DisplayLastEditDate;
      $this->DateCreated = $DateCreated;
      $this->DisplayDateCreated = $DisplayDateCreated;
      $this->UserId = $UserId;
      $this->FolderId = $FolderId;
      $this->IsPermissionsInherited = $IsPermissionsInherited;
      $this->PermissionInheritedFrom = $PermissionInheritedFrom;
      $this->Status = $Status;
      $this->DisplayGoLive = $DisplayGoLive;
      $this->IsPublished = $IsPublished;
      $this->IsSearchable = $IsSearchable;
      $this->XmlInheritedFrom = $XmlInheritedFrom;
      $this->MetaData = $MetaData;
      $this->DisplayEndDate = $DisplayEndDate;
      $this->ExpireDate = $ExpireDate;
      $this->EndDateActionType = $EndDateActionType;
      $this->XmlConfiguration = $XmlConfiguration;
      $this->StyleSheet = $StyleSheet;
      $this->LanguageDescription = $LanguageDescription;
      $this->Text = $Text;
      $this->Path = $Path;
      $this->ContentPath = $ContentPath;
      $this->ContType = $ContType;
      $this->Updates = $Updates;
      $this->FolderName = $FolderName;
      $this->MediaText = $MediaText;
      $this->HistoryId = $HistoryId;
      $this->HyperLink = $HyperLink;
      $this->TemplateConfiguration = $TemplateConfiguration;
      $this->FlagDefId = $FlagDefId;
    }

}
