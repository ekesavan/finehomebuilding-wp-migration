<?php

class FormFieldDatalist
{

    /**
     * @var FormFieldDatalistItem[] $item
     * @access public
     */
    public $item = null;

    /**
     * @var string $name
     * @access public
     */
    public $name = null;

    /**
     * @var UNKNOWN $lang
     * @access public
     */
    public $lang = null;

    /**
     * @var string $ektdesignns_datasrc
     * @access public
     */
    public $ektdesignns_datasrc = null;

    /**
     * @var string $ektdesignns_dataselect
     * @access public
     */
    public $ektdesignns_dataselect = null;

    /**
     * @var string $ektdesignns_captionxpath
     * @access public
     */
    public $ektdesignns_captionxpath = null;

    /**
     * @var string $ektdesignns_valuexpath
     * @access public
     */
    public $ektdesignns_valuexpath = null;

    /**
     * @var string $ektdesignns_datanamespaces
     * @access public
     */
    public $ektdesignns_datanamespaces = null;

    /**
     * @param FormFieldDatalistItem[] $item
     * @param string $name
     * @param UNKNOWN $lang
     * @param string $ektdesignns_datasrc
     * @param string $ektdesignns_dataselect
     * @param string $ektdesignns_captionxpath
     * @param string $ektdesignns_valuexpath
     * @param string $ektdesignns_datanamespaces
     * @access public
     */
    public function __construct($item, $name, $lang, $ektdesignns_datasrc, $ektdesignns_dataselect, $ektdesignns_captionxpath, $ektdesignns_valuexpath, $ektdesignns_datanamespaces)
    {
      $this->item = $item;
      $this->name = $name;
      $this->lang = $lang;
      $this->ektdesignns_datasrc = $ektdesignns_datasrc;
      $this->ektdesignns_dataselect = $ektdesignns_dataselect;
      $this->ektdesignns_captionxpath = $ektdesignns_captionxpath;
      $this->ektdesignns_valuexpath = $ektdesignns_valuexpath;
      $this->ektdesignns_datanamespaces = $ektdesignns_datanamespaces;
    }

}
