<?php

include_once('GetAllForms.php');
include_once('GetAllFormsResponse.php');
include_once('FormData.php');
include_once('ContentData.php');
include_once('ContentBaseData.php');
include_once('CmsLocalizedDataOfContentBaseData.php');
include_once('CmsDataOfContentBaseData.php');
include_once('BaseDataOfContentBaseData.php');
include_once('CMSContentSubtype.php');
include_once('AssetData.php');
include_once('ContentMetaData.php');
include_once('CmsDataOfContentMetaData.php');
include_once('BaseDataOfContentMetaData.php');
include_once('ContentMetadataDataType.php');
include_once('ContentMetadataType.php');
include_once('CMSEndDateAction.php');
include_once('XmlConfigData.php');
include_once('TemplateData.php');
include_once('CmsDataOfTemplateData.php');
include_once('BaseDataOfTemplateData.php');
include_once('TemplateType.php');
include_once('TemplateSubType.php');
include_once('FormValidationData.php');
include_once('AuthenticationHeader.php');
include_once('RequestInfoParameters.php');
include_once('AssignFormTask.php');
include_once('AssignFormTaskResponse.php');
include_once('GetAllForms_x0020_By_x0020_Id_x002C__x0020_OrderBy_x002C__x0020_LanguageID_x0020_and_x0020_Type.php');
include_once('FormResultType.php');
include_once('GetAllForms_x0020_By_x0020_Id_x002C__x0020_OrderBy_x002C__x0020_LanguageID_x0020_and_x0020_TypeResponse.php');
include_once('GetForm.php');
include_once('GetFormResponse.php');
include_once('GetFormDataHistogram.php');
include_once('GetFormDataHistogramResponse.php');
include_once('GetFormFieldData_x0020_By_x0020_FormID_x0020_and_x0020_FormDataId.php');
include_once('FormFieldDataItem.php');
include_once('BaseDataOfCustomAttribute.php');
include_once('CmsDataOfCustomAttribute.php');
include_once('CustomAttribute.php');
include_once('CustomAttributeValueTypes.php');
include_once('UserRank.php');
include_once('FolderData.php');
include_once('CmsDataOfFolderData.php');
include_once('BaseDataOfFolderData.php');
include_once('FolderApprovalType.php');
include_once('PermissionData.php');
include_once('CmsDataOfPermissionData.php');
include_once('BaseDataOfPermissionData.php');
include_once('FolderType.php');
include_once('SitemapPath.php');
include_once('TaxonomyBaseData.php');
include_once('CmsDataOfTaxonomyBaseData.php');
include_once('BaseDataOfTaxonomyBaseData.php');
include_once('TaxonomyType.php');
include_once('FlagDefData.php');
include_once('CmsDataOfFlagDefData.php');
include_once('BaseDataOfFlagDefData.php');
include_once('FlagItemData.php');
include_once('UserPreferenceData.php');
include_once('BaseDataOfUserBaseData.php');
include_once('CmsDataOfUserBaseData.php');
include_once('UserBaseData.php');
include_once('UserData.php');
include_once('BaseDataOfFormSubmittedData.php');
include_once('CmsDataOfFormSubmittedData.php');
include_once('FormSubmittedData.php');
include_once('GetFormFieldData_x0020_By_x0020_FormID_x0020_and_x0020_FormDataIdResponse.php');
include_once('GetFormFieldData_x0020_By_x0020_FormId_x002C__x0020_StartDate_x0020_and_x0020_EndDate.php');
include_once('GetFormFieldData_x0020_By_x0020_FormId_x002C__x0020_StartDate_x0020_and_x0020_EndDateResponse.php');
include_once('GetFormFieldList.php');
include_once('FormFieldList.php');
include_once('FormFieldDefinition.php');
include_once('FormFieldDatalist.php');
include_once('FormFieldDatalistItem.php');
include_once('GetFormFieldListResponse.php');
include_once('GetFormTitle.php');
include_once('GetFormTitleResponse.php');
include_once('SubmitFormFieldData.php');
include_once('SubmitFormFieldDataResponse.php');
include_once('UpdateFormFieldData.php');
include_once('UpdateFormFieldDataResponse.php');

class Form extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'GetAllForms' => '\GetAllForms',
      'GetAllFormsResponse' => '\GetAllFormsResponse',
      'FormData' => '\FormData',
      'ContentData' => '\ContentData',
      'ContentBaseData' => '\ContentBaseData',
      'CmsLocalizedDataOfContentBaseData' => '\CmsLocalizedDataOfContentBaseData',
      'CmsDataOfContentBaseData' => '\CmsDataOfContentBaseData',
      'BaseDataOfContentBaseData' => '\BaseDataOfContentBaseData',
      'AssetData' => '\AssetData',
      'ContentMetaData' => '\ContentMetaData',
      'CmsDataOfContentMetaData' => '\CmsDataOfContentMetaData',
      'BaseDataOfContentMetaData' => '\BaseDataOfContentMetaData',
      'XmlConfigData' => '\XmlConfigData',
      'TemplateData' => '\TemplateData',
      'CmsDataOfTemplateData' => '\CmsDataOfTemplateData',
      'BaseDataOfTemplateData' => '\BaseDataOfTemplateData',
      'FormValidationData' => '\FormValidationData',
      'AuthenticationHeader' => '\AuthenticationHeader',
      'RequestInfoParameters' => '\RequestInfoParameters',
      'AssignFormTask' => '\AssignFormTask',
      'AssignFormTaskResponse' => '\AssignFormTaskResponse',
      'GetAllForms_x0020_By_x0020_Id_x002C__x0020_OrderBy_x002C__x0020_LanguageID_x0020_and_x0020_Type' => '\GetAllForms_x0020_By_x0020_Id_x002C__x0020_OrderBy_x002C__x0020_LanguageID_x0020_and_x0020_Type',
      'GetAllForms_x0020_By_x0020_Id_x002C__x0020_OrderBy_x002C__x0020_LanguageID_x0020_and_x0020_TypeResponse' => '\GetAllForms_x0020_By_x0020_Id_x002C__x0020_OrderBy_x002C__x0020_LanguageID_x0020_and_x0020_TypeResponse',
      'GetForm' => '\GetForm',
      'GetFormResponse' => '\GetFormResponse',
      'GetFormDataHistogram' => '\GetFormDataHistogram',
      'GetFormDataHistogramResponse' => '\GetFormDataHistogramResponse',
      'GetFormFieldData_x0020_By_x0020_FormID_x0020_and_x0020_FormDataId' => '\GetFormFieldData_x0020_By_x0020_FormID_x0020_and_x0020_FormDataId',
      'FormFieldDataItem' => '\FormFieldDataItem',
      'BaseDataOfCustomAttribute' => '\BaseDataOfCustomAttribute',
      'CmsDataOfCustomAttribute' => '\CmsDataOfCustomAttribute',
      'CustomAttribute' => '\CustomAttribute',
      'UserRank' => '\UserRank',
      'FolderData' => '\FolderData',
      'CmsDataOfFolderData' => '\CmsDataOfFolderData',
      'BaseDataOfFolderData' => '\BaseDataOfFolderData',
      'PermissionData' => '\PermissionData',
      'CmsDataOfPermissionData' => '\CmsDataOfPermissionData',
      'BaseDataOfPermissionData' => '\BaseDataOfPermissionData',
      'SitemapPath' => '\SitemapPath',
      'TaxonomyBaseData' => '\TaxonomyBaseData',
      'CmsDataOfTaxonomyBaseData' => '\CmsDataOfTaxonomyBaseData',
      'BaseDataOfTaxonomyBaseData' => '\BaseDataOfTaxonomyBaseData',
      'FlagDefData' => '\FlagDefData',
      'CmsDataOfFlagDefData' => '\CmsDataOfFlagDefData',
      'BaseDataOfFlagDefData' => '\BaseDataOfFlagDefData',
      'FlagItemData' => '\FlagItemData',
      'UserPreferenceData' => '\UserPreferenceData',
      'BaseDataOfUserBaseData' => '\BaseDataOfUserBaseData',
      'CmsDataOfUserBaseData' => '\CmsDataOfUserBaseData',
      'UserBaseData' => '\UserBaseData',
      'UserData' => '\UserData',
      'BaseDataOfFormSubmittedData' => '\BaseDataOfFormSubmittedData',
      'CmsDataOfFormSubmittedData' => '\CmsDataOfFormSubmittedData',
      'FormSubmittedData' => '\FormSubmittedData',
      'GetFormFieldData_x0020_By_x0020_FormID_x0020_and_x0020_FormDataIdResponse' => '\GetFormFieldData_x0020_By_x0020_FormID_x0020_and_x0020_FormDataIdResponse',
      'GetFormFieldData_x0020_By_x0020_FormId_x002C__x0020_StartDate_x0020_and_x0020_EndDate' => '\GetFormFieldData_x0020_By_x0020_FormId_x002C__x0020_StartDate_x0020_and_x0020_EndDate',
      'GetFormFieldData_x0020_By_x0020_FormId_x002C__x0020_StartDate_x0020_and_x0020_EndDateResponse' => '\GetFormFieldData_x0020_By_x0020_FormId_x002C__x0020_StartDate_x0020_and_x0020_EndDateResponse',
      'GetFormFieldList' => '\GetFormFieldList',
      'FormFieldList' => '\FormFieldList',
      'FormFieldDefinition' => '\FormFieldDefinition',
      'FormFieldDatalist' => '\FormFieldDatalist',
      'FormFieldDatalistItem' => '\FormFieldDatalistItem',
      'GetFormFieldListResponse' => '\GetFormFieldListResponse',
      'GetFormTitle' => '\GetFormTitle',
      'GetFormTitleResponse' => '\GetFormTitleResponse',
      'SubmitFormFieldData' => '\SubmitFormFieldData',
      'SubmitFormFieldDataResponse' => '\SubmitFormFieldDataResponse',
      'UpdateFormFieldData' => '\UpdateFormFieldData',
      'UpdateFormFieldDataResponse' => '\UpdateFormFieldDataResponse');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = 'Form.wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      
      parent::__construct($wsdl, $options);
    }

    /**
     * @param GetAllForms $parameters
     * @access public
     * @return GetAllFormsResponse
     */
    public function GetAllForms(GetAllForms $parameters)
    {
      return $this->__soapCall('GetAllForms', array($parameters));
    }

    /**
     * @param AssignFormTask $parameters
     * @access public
     * @return AssignFormTaskResponse
     */
    public function AssignFormTask(AssignFormTask $parameters)
    {
      return $this->__soapCall('AssignFormTask', array($parameters));
    }

    /**
     * @param GetForm $parameters
     * @access public
     * @return GetFormResponse
     */
    public function GetForm(GetForm $parameters)
    {
      return $this->__soapCall('GetForm', array($parameters));
    }

    /**
     * @param GetFormDataHistogram $parameters
     * @access public
     * @return GetFormDataHistogramResponse
     */
    public function GetFormDataHistogram(GetFormDataHistogram $parameters)
    {
      return $this->__soapCall('GetFormDataHistogram', array($parameters));
    }

    /**
     * @param GetFormFieldData_x0020_By_x0020_FormID_x0020_and_x0020_FormDataId $parameters
     * @access public
     * @return GetFormFieldData_x0020_By_x0020_FormID_x0020_and_x0020_FormDataIdResponse
     */
    public function GetFormFieldData(GetFormFieldData_x0020_By_x0020_FormID_x0020_and_x0020_FormDataId $parameters)
    {
      return $this->__soapCall('GetFormFieldData', array($parameters));
    }

    /**
     * @param GetFormFieldList $parameters
     * @access public
     * @return GetFormFieldListResponse
     */
    public function GetFormFieldList(GetFormFieldList $parameters)
    {
      return $this->__soapCall('GetFormFieldList', array($parameters));
    }

    /**
     * @param GetFormTitle $parameters
     * @access public
     * @return GetFormTitleResponse
     */
    public function GetFormTitle(GetFormTitle $parameters)
    {
      return $this->__soapCall('GetFormTitle', array($parameters));
    }

    /**
     * @param SubmitFormFieldData $parameters
     * @access public
     * @return SubmitFormFieldDataResponse
     */
    public function SubmitFormFieldData(SubmitFormFieldData $parameters)
    {
      return $this->__soapCall('SubmitFormFieldData', array($parameters));
    }

    /**
     * @param UpdateFormFieldData $parameters
     * @access public
     * @return UpdateFormFieldDataResponse
     */
    public function UpdateFormFieldData(UpdateFormFieldData $parameters)
    {
      return $this->__soapCall('UpdateFormFieldData', array($parameters));
    }

}
