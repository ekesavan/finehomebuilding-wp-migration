<?php

class GetFormFieldListResponse
{

    /**
     * @var FormFieldList $fieldlist
     * @access public
     */
    public $fieldlist = null;

    /**
     * @param FormFieldList $fieldlist
     * @access public
     */
    public function __construct($fieldlist)
    {
      $this->fieldlist = $fieldlist;
    }

}
