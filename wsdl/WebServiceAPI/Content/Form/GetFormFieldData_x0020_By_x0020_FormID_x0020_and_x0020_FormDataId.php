<?php

class GetFormFieldData_x0020_By_x0020_FormID_x0020_and_x0020_FormDataId
{

    /**
     * @var int $FormId
     * @access public
     */
    public $FormId = null;

    /**
     * @var int $FormDataId
     * @access public
     */
    public $FormDataId = null;

    /**
     * @param int $FormId
     * @param int $FormDataId
     * @access public
     */
    public function __construct($FormId, $FormDataId)
    {
      $this->FormId = $FormId;
      $this->FormDataId = $FormDataId;
    }

}
