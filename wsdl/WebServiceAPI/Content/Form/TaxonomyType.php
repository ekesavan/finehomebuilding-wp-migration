<?php

class TaxonomyType
{
    const __default = 'Content';
    const Content = 'Content';
    const User = 'User';
    const Group = 'Group';
    const Locale = 'Locale';
    const Undefined = 'Undefined';


}
