<?php

class AssignFormTaskResponse
{

    /**
     * @var boolean $AssignFormTaskResult
     * @access public
     */
    public $AssignFormTaskResult = null;

    /**
     * @param boolean $AssignFormTaskResult
     * @access public
     */
    public function __construct($AssignFormTaskResult)
    {
      $this->AssignFormTaskResult = $AssignFormTaskResult;
    }

}
