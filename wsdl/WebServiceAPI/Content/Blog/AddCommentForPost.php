<?php

class AddCommentForPost
{

    /**
     * @var BlogComment $bcComment
     * @access public
     */
    public $bcComment = null;

    /**
     * @param BlogComment $bcComment
     * @access public
     */
    public function __construct($bcComment)
    {
      $this->bcComment = $bcComment;
    }

}
