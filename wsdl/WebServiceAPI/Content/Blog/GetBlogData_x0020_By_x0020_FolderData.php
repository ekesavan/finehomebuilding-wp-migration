<?php

class GetBlogData_x0020_By_x0020_FolderData
{

    /**
     * @var FolderData $folder_data
     * @access public
     */
    public $folder_data = null;

    /**
     * @param FolderData $folder_data
     * @access public
     */
    public function __construct($folder_data)
    {
      $this->folder_data = $folder_data;
    }

}
