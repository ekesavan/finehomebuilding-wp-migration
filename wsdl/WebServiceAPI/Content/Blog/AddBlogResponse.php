<?php

class AddBlogResponse
{

    /**
     * @var int $AddBlogResult
     * @access public
     */
    public $AddBlogResult = null;

    /**
     * @param int $AddBlogResult
     * @access public
     */
    public function __construct($AddBlogResult)
    {
      $this->AddBlogResult = $AddBlogResult;
    }

}
