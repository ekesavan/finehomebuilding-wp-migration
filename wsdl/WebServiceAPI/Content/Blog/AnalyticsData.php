<?php

class AnalyticsData
{

    /**
     * @var int $ContentViewCount
     * @access public
     */
    public $ContentViewCount = null;

    /**
     * @var float $ContentRatingAverage
     * @access public
     */
    public $ContentRatingAverage = null;

    /**
     * @var int $ContentRatingCount
     * @access public
     */
    public $ContentRatingCount = null;

    /**
     * @param int $ContentViewCount
     * @param float $ContentRatingAverage
     * @param int $ContentRatingCount
     * @access public
     */
    public function __construct($ContentViewCount, $ContentRatingAverage, $ContentRatingCount)
    {
      $this->ContentViewCount = $ContentViewCount;
      $this->ContentRatingAverage = $ContentRatingAverage;
      $this->ContentRatingCount = $ContentRatingCount;
    }

}
