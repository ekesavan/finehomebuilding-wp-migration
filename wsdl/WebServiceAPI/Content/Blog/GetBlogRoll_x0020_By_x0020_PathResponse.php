<?php

class GetBlogRoll_x0020_By_x0020_PathResponse
{

    /**
     * @var BlogRoll $GetBlogRoll_x0020_By_x0020_PathResult
     * @access public
     */
    public $GetBlogRoll_x0020_By_x0020_PathResult = null;

    /**
     * @param BlogRoll $GetBlogRoll_x0020_By_x0020_PathResult
     * @access public
     */
    public function __construct($GetBlogRoll_x0020_By_x0020_PathResult)
    {
      $this->GetBlogRoll_x0020_By_x0020_PathResult = $GetBlogRoll_x0020_By_x0020_PathResult;
    }

}
