<?php

class GetBlog_x0020_By_x0020_Path_x002C__x0020_StartDate_x002C__x0020_MaxEntries_x0020_and_x0020_LanguageIDResponse
{

    /**
     * @var BlogData $GetBlog_x0020_By_x0020_Path_x002C__x0020_StartDate_x002C__x0020_MaxEntries_x0020_and_x0020_LanguageIDResult
     * @access public
     */
    public $GetBlog_x0020_By_x0020_Path_x002C__x0020_StartDate_x002C__x0020_MaxEntries_x0020_and_x0020_LanguageIDResult = null;

    /**
     * @param BlogData $GetBlog_x0020_By_x0020_Path_x002C__x0020_StartDate_x002C__x0020_MaxEntries_x0020_and_x0020_LanguageIDResult
     * @access public
     */
    public function __construct($GetBlog_x0020_By_x0020_Path_x002C__x0020_StartDate_x002C__x0020_MaxEntries_x0020_and_x0020_LanguageIDResult)
    {
      $this->GetBlog_x0020_By_x0020_Path_x002C__x0020_StartDate_x002C__x0020_MaxEntries_x0020_and_x0020_LanguageIDResult = $GetBlog_x0020_By_x0020_Path_x002C__x0020_StartDate_x002C__x0020_MaxEntries_x0020_and_x0020_LanguageIDResult;
    }

}
