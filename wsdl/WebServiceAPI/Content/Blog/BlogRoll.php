<?php

class BlogRoll
{

    /**
     * @var BlogRollItem[] $Items
     * @access public
     */
    public $Items = null;

    /**
     * @var int $ID
     * @access public
     */
    public $ID = null;

    /**
     * @param BlogRollItem[] $Items
     * @param int $ID
     * @access public
     */
    public function __construct($Items, $ID)
    {
      $this->Items = $Items;
      $this->ID = $ID;
    }

}
