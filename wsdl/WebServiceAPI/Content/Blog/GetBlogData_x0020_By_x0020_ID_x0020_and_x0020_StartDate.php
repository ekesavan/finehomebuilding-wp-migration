<?php

class GetBlogData_x0020_By_x0020_ID_x0020_and_x0020_StartDate
{

    /**
     * @var int $BlogID
     * @access public
     */
    public $BlogID = null;

    /**
     * @var dateTime $StartDate
     * @access public
     */
    public $StartDate = null;

    /**
     * @param int $BlogID
     * @param dateTime $StartDate
     * @access public
     */
    public function __construct($BlogID, $StartDate)
    {
      $this->BlogID = $BlogID;
      $this->StartDate = $StartDate;
    }

}
