<?php

class GetBlogData_x0020_By_x0020_ID_x0020_and_x0020_StartDateResponse
{

    /**
     * @var BlogData $GetBlogData_x0020_By_x0020_ID_x0020_and_x0020_StartDateResult
     * @access public
     */
    public $GetBlogData_x0020_By_x0020_ID_x0020_and_x0020_StartDateResult = null;

    /**
     * @param BlogData $GetBlogData_x0020_By_x0020_ID_x0020_and_x0020_StartDateResult
     * @access public
     */
    public function __construct($GetBlogData_x0020_By_x0020_ID_x0020_and_x0020_StartDateResult)
    {
      $this->GetBlogData_x0020_By_x0020_ID_x0020_and_x0020_StartDateResult = $GetBlogData_x0020_By_x0020_ID_x0020_and_x0020_StartDateResult;
    }

}
