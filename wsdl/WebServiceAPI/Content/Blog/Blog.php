<?php

include_once('GetBlogData_x0020_By_x0020_ID_x0020_and_x0020_StartDate.php');
include_once('GetBlogData_x0020_By_x0020_ID_x0020_and_x0020_StartDateResponse.php');
include_once('BlogData.php');
include_once('FolderData.php');
include_once('CmsDataOfFolderData.php');
include_once('BaseDataOfFolderData.php');
include_once('XmlConfigData.php');
include_once('FolderApprovalType.php');
include_once('PermissionData.php');
include_once('CmsDataOfPermissionData.php');
include_once('BaseDataOfPermissionData.php');
include_once('ContentMetaData.php');
include_once('CmsDataOfContentMetaData.php');
include_once('BaseDataOfContentMetaData.php');
include_once('ContentMetadataDataType.php');
include_once('ContentMetadataType.php');
include_once('TemplateData.php');
include_once('CmsDataOfTemplateData.php');
include_once('BaseDataOfTemplateData.php');
include_once('TemplateType.php');
include_once('TemplateSubType.php');
include_once('FolderType.php');
include_once('SitemapPath.php');
include_once('TaxonomyBaseData.php');
include_once('CmsDataOfTaxonomyBaseData.php');
include_once('BaseDataOfTaxonomyBaseData.php');
include_once('TaxonomyType.php');
include_once('FlagDefData.php');
include_once('CmsDataOfFlagDefData.php');
include_once('BaseDataOfFlagDefData.php');
include_once('FlagItemData.php');
include_once('BlogPostData.php');
include_once('ContentBase.php');
include_once('CMSEndDateAction.php');
include_once('CMSContentType.php');
include_once('CMSContentSubtype.php');
include_once('ItemStatus.php');
include_once('AssetData.php');
include_once('AnalyticsData.php');
include_once('BlogComment.php');
include_once('BlogVisibility.php');
include_once('BlogRoll.php');
include_once('BlogRollItem.php');
include_once('AuthenticationHeader.php');
include_once('RequestInfoParameters.php');
include_once('GetBlogData_x0020_By_x0020_ID.php');
include_once('GetBlogData_x0020_By_x0020_IDResponse.php');
include_once('GetBlogData_x0020_by_x0020_ID_x002C__x0020_StartDate_x002C__x0020_PostsVisible_x0020_and_x0020_LanguageID.php');
include_once('GetBlogData_x0020_by_x0020_ID_x002C__x0020_StartDate_x002C__x0020_PostsVisible_x0020_and_x0020_LanguageIDResponse.php');
include_once('GetBlogData_x0020_By_x0020_FolderData.php');
include_once('GetBlogData_x0020_By_x0020_FolderDataResponse.php');
include_once('GetBlogRoll.php');
include_once('GetBlogRollResponse.php');
include_once('GetBlankBlogPostData.php');
include_once('GetBlankBlogPostDataResponse.php');
include_once('GetBlogDataForPost.php');
include_once('GetBlogDataForPostResponse.php');
include_once('GetBlogPostData.php');
include_once('GetBlogPostDataResponse.php');
include_once('AddCommentForPost.php');
include_once('AddCommentForPostResponse.php');
include_once('GetBlog_x0020_By_x0020_Path_x002C__x0020_StartDate_x002C__x0020_MaxEntries_x0020_and_x0020_LanguageID.php');
include_once('GetBlog_x0020_By_x0020_Path_x002C__x0020_StartDate_x002C__x0020_MaxEntries_x0020_and_x0020_LanguageIDResponse.php');
include_once('GetBlog_x0020_by_x0020_ID_x002C__x0020_StartDate_x002C__x0020_MaxEntries_x0020_and_x0020_LanguageID.php');
include_once('GetBlog_x0020_by_x0020_ID_x002C__x0020_StartDate_x002C__x0020_MaxEntries_x0020_and_x0020_LanguageIDResponse.php');
include_once('GetBlogRoll_x0020_By_x0020_Path.php');
include_once('GetBlogRoll_x0020_By_x0020_PathResponse.php');
include_once('DeleteBlog.php');
include_once('DeleteBlogResponse.php');
include_once('DeletePost.php');
include_once('DeletePostResponse.php');
include_once('DeleteBlogPostComment.php');
include_once('DeleteBlogPostCommentResponse.php');
include_once('AddPost.php');
include_once('ContentData.php');
include_once('ContentBaseData.php');
include_once('CmsLocalizedDataOfContentBaseData.php');
include_once('CmsDataOfContentBaseData.php');
include_once('BaseDataOfContentBaseData.php');
include_once('AddPostResponse.php');
include_once('AddBlog.php');
include_once('AddBlogResponse.php');
include_once('GetPost.php');
include_once('GetPostResponse.php');
include_once('GetPostbyID.php');
include_once('GetPostbyIDResponse.php');
include_once('UpdateBlogProperties.php');
include_once('UpdateBlogPropertiesResponse.php');
include_once('GetUserBlog.php');
include_once('GetUserBlogResponse.php');

class Blog extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'GetBlogData_x0020_By_x0020_ID_x0020_and_x0020_StartDate' => '\GetBlogData_x0020_By_x0020_ID_x0020_and_x0020_StartDate',
      'GetBlogData_x0020_By_x0020_ID_x0020_and_x0020_StartDateResponse' => '\GetBlogData_x0020_By_x0020_ID_x0020_and_x0020_StartDateResponse',
      'BlogData' => '\BlogData',
      'FolderData' => '\FolderData',
      'CmsDataOfFolderData' => '\CmsDataOfFolderData',
      'BaseDataOfFolderData' => '\BaseDataOfFolderData',
      'XmlConfigData' => '\XmlConfigData',
      'PermissionData' => '\PermissionData',
      'CmsDataOfPermissionData' => '\CmsDataOfPermissionData',
      'BaseDataOfPermissionData' => '\BaseDataOfPermissionData',
      'ContentMetaData' => '\ContentMetaData',
      'CmsDataOfContentMetaData' => '\CmsDataOfContentMetaData',
      'BaseDataOfContentMetaData' => '\BaseDataOfContentMetaData',
      'TemplateData' => '\TemplateData',
      'CmsDataOfTemplateData' => '\CmsDataOfTemplateData',
      'BaseDataOfTemplateData' => '\BaseDataOfTemplateData',
      'SitemapPath' => '\SitemapPath',
      'TaxonomyBaseData' => '\TaxonomyBaseData',
      'CmsDataOfTaxonomyBaseData' => '\CmsDataOfTaxonomyBaseData',
      'BaseDataOfTaxonomyBaseData' => '\BaseDataOfTaxonomyBaseData',
      'FlagDefData' => '\FlagDefData',
      'CmsDataOfFlagDefData' => '\CmsDataOfFlagDefData',
      'BaseDataOfFlagDefData' => '\BaseDataOfFlagDefData',
      'FlagItemData' => '\FlagItemData',
      'BlogPostData' => '\BlogPostData',
      'ContentBase' => '\ContentBase',
      'AssetData' => '\AssetData',
      'AnalyticsData' => '\AnalyticsData',
      'BlogComment' => '\BlogComment',
      'BlogRoll' => '\BlogRoll',
      'BlogRollItem' => '\BlogRollItem',
      'AuthenticationHeader' => '\AuthenticationHeader',
      'RequestInfoParameters' => '\RequestInfoParameters',
      'GetBlogData_x0020_By_x0020_ID' => '\GetBlogData_x0020_By_x0020_ID',
      'GetBlogData_x0020_By_x0020_IDResponse' => '\GetBlogData_x0020_By_x0020_IDResponse',
      'GetBlogData_x0020_by_x0020_ID_x002C__x0020_StartDate_x002C__x0020_PostsVisible_x0020_and_x0020_LanguageID' => '\GetBlogData_x0020_by_x0020_ID_x002C__x0020_StartDate_x002C__x0020_PostsVisible_x0020_and_x0020_LanguageID',
      'GetBlogData_x0020_by_x0020_ID_x002C__x0020_StartDate_x002C__x0020_PostsVisible_x0020_and_x0020_LanguageIDResponse' => '\GetBlogData_x0020_by_x0020_ID_x002C__x0020_StartDate_x002C__x0020_PostsVisible_x0020_and_x0020_LanguageIDResponse',
      'GetBlogData_x0020_By_x0020_FolderData' => '\GetBlogData_x0020_By_x0020_FolderData',
      'GetBlogData_x0020_By_x0020_FolderDataResponse' => '\GetBlogData_x0020_By_x0020_FolderDataResponse',
      'GetBlogRoll' => '\GetBlogRoll',
      'GetBlogRollResponse' => '\GetBlogRollResponse',
      'GetBlankBlogPostData' => '\GetBlankBlogPostData',
      'GetBlankBlogPostDataResponse' => '\GetBlankBlogPostDataResponse',
      'GetBlogDataForPost' => '\GetBlogDataForPost',
      'GetBlogDataForPostResponse' => '\GetBlogDataForPostResponse',
      'GetBlogPostData' => '\GetBlogPostData',
      'GetBlogPostDataResponse' => '\GetBlogPostDataResponse',
      'AddCommentForPost' => '\AddCommentForPost',
      'AddCommentForPostResponse' => '\AddCommentForPostResponse',
      'GetBlog_x0020_By_x0020_Path_x002C__x0020_StartDate_x002C__x0020_MaxEntries_x0020_and_x0020_LanguageID' => '\GetBlog_x0020_By_x0020_Path_x002C__x0020_StartDate_x002C__x0020_MaxEntries_x0020_and_x0020_LanguageID',
      'GetBlog_x0020_By_x0020_Path_x002C__x0020_StartDate_x002C__x0020_MaxEntries_x0020_and_x0020_LanguageIDResponse' => '\GetBlog_x0020_By_x0020_Path_x002C__x0020_StartDate_x002C__x0020_MaxEntries_x0020_and_x0020_LanguageIDResponse',
      'GetBlog_x0020_by_x0020_ID_x002C__x0020_StartDate_x002C__x0020_MaxEntries_x0020_and_x0020_LanguageID' => '\GetBlog_x0020_by_x0020_ID_x002C__x0020_StartDate_x002C__x0020_MaxEntries_x0020_and_x0020_LanguageID',
      'GetBlog_x0020_by_x0020_ID_x002C__x0020_StartDate_x002C__x0020_MaxEntries_x0020_and_x0020_LanguageIDResponse' => '\GetBlog_x0020_by_x0020_ID_x002C__x0020_StartDate_x002C__x0020_MaxEntries_x0020_and_x0020_LanguageIDResponse',
      'GetBlogRoll_x0020_By_x0020_Path' => '\GetBlogRoll_x0020_By_x0020_Path',
      'GetBlogRoll_x0020_By_x0020_PathResponse' => '\GetBlogRoll_x0020_By_x0020_PathResponse',
      'DeleteBlog' => '\DeleteBlog',
      'DeleteBlogResponse' => '\DeleteBlogResponse',
      'DeletePost' => '\DeletePost',
      'DeletePostResponse' => '\DeletePostResponse',
      'DeleteBlogPostComment' => '\DeleteBlogPostComment',
      'DeleteBlogPostCommentResponse' => '\DeleteBlogPostCommentResponse',
      'AddPost' => '\AddPost',
      'ContentData' => '\ContentData',
      'ContentBaseData' => '\ContentBaseData',
      'CmsLocalizedDataOfContentBaseData' => '\CmsLocalizedDataOfContentBaseData',
      'CmsDataOfContentBaseData' => '\CmsDataOfContentBaseData',
      'BaseDataOfContentBaseData' => '\BaseDataOfContentBaseData',
      'AddPostResponse' => '\AddPostResponse',
      'AddBlog' => '\AddBlog',
      'AddBlogResponse' => '\AddBlogResponse',
      'GetPost' => '\GetPost',
      'GetPostResponse' => '\GetPostResponse',
      'GetPostbyID' => '\GetPostbyID',
      'GetPostbyIDResponse' => '\GetPostbyIDResponse',
      'UpdateBlogProperties' => '\UpdateBlogProperties',
      'UpdateBlogPropertiesResponse' => '\UpdateBlogPropertiesResponse',
      'GetUserBlog' => '\GetUserBlog',
      'GetUserBlogResponse' => '\GetUserBlogResponse');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = 'Blog.wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      
      parent::__construct($wsdl, $options);
    }

    /**
     * @param GetBlogData_x0020_By_x0020_ID_x0020_and_x0020_StartDate $parameters
     * @access public
     * @return GetBlogData_x0020_By_x0020_ID_x0020_and_x0020_StartDateResponse
     */
    public function GetBlogData(GetBlogData_x0020_By_x0020_ID_x0020_and_x0020_StartDate $parameters)
    {
      return $this->__soapCall('GetBlogData', array($parameters));
    }

    /**
     * @param GetBlogRoll $parameters
     * @access public
     * @return GetBlogRollResponse
     */
    public function GetBlogRoll(GetBlogRoll $parameters)
    {
      return $this->__soapCall('GetBlogRoll', array($parameters));
    }

    /**
     * @param GetBlankBlogPostData $parameters
     * @access public
     * @return GetBlankBlogPostDataResponse
     */
    public function GetBlankBlogPostData(GetBlankBlogPostData $parameters)
    {
      return $this->__soapCall('GetBlankBlogPostData', array($parameters));
    }

    /**
     * @param GetBlogDataForPost $parameters
     * @access public
     * @return GetBlogDataForPostResponse
     */
    public function GetBlogDataForPost(GetBlogDataForPost $parameters)
    {
      return $this->__soapCall('GetBlogDataForPost', array($parameters));
    }

    /**
     * @param GetBlogPostData $parameters
     * @access public
     * @return GetBlogPostDataResponse
     */
    public function GetBlogPostData(GetBlogPostData $parameters)
    {
      return $this->__soapCall('GetBlogPostData', array($parameters));
    }

    /**
     * @param AddCommentForPost $parameters
     * @access public
     * @return AddCommentForPostResponse
     */
    public function AddCommentForPost(AddCommentForPost $parameters)
    {
      return $this->__soapCall('AddCommentForPost', array($parameters));
    }

    /**
     * @param GetBlog_x0020_By_x0020_Path_x002C__x0020_StartDate_x002C__x0020_MaxEntries_x0020_and_x0020_LanguageID $parameters
     * @access public
     * @return GetBlog_x0020_By_x0020_Path_x002C__x0020_StartDate_x002C__x0020_MaxEntries_x0020_and_x0020_LanguageIDResponse
     */
    public function GetBlog(GetBlog_x0020_By_x0020_Path_x002C__x0020_StartDate_x002C__x0020_MaxEntries_x0020_and_x0020_LanguageID $parameters)
    {
      return $this->__soapCall('GetBlog', array($parameters));
    }

    /**
     * @param DeleteBlog $parameters
     * @access public
     * @return DeleteBlogResponse
     */
    public function DeleteBlog(DeleteBlog $parameters)
    {
      return $this->__soapCall('DeleteBlog', array($parameters));
    }

    /**
     * @param DeletePost $parameters
     * @access public
     * @return DeletePostResponse
     */
    public function DeletePost(DeletePost $parameters)
    {
      return $this->__soapCall('DeletePost', array($parameters));
    }

    /**
     * @param DeleteBlogPostComment $parameters
     * @access public
     * @return DeleteBlogPostCommentResponse
     */
    public function DeleteBlogPostComment(DeleteBlogPostComment $parameters)
    {
      return $this->__soapCall('DeleteBlogPostComment', array($parameters));
    }

    /**
     * @param AddPost $parameters
     * @access public
     * @return AddPostResponse
     */
    public function AddPost(AddPost $parameters)
    {
      return $this->__soapCall('AddPost', array($parameters));
    }

    /**
     * @param AddBlog $parameters
     * @access public
     * @return AddBlogResponse
     */
    public function AddBlog(AddBlog $parameters)
    {
      return $this->__soapCall('AddBlog', array($parameters));
    }

    /**
     * @param GetPost $parameters
     * @access public
     * @return GetPostResponse
     */
    public function GetPost(GetPost $parameters)
    {
      return $this->__soapCall('GetPost', array($parameters));
    }

    /**
     * @param GetPostbyID $parameters
     * @access public
     * @return GetPostbyIDResponse
     */
    public function GetPostbyID(GetPostbyID $parameters)
    {
      return $this->__soapCall('GetPostbyID', array($parameters));
    }

    /**
     * updates blog data.
     *
     * @param UpdateBlogProperties $parameters
     * @access public
     * @return UpdateBlogPropertiesResponse
     */
    public function UpdateBlogProperties(UpdateBlogProperties $parameters)
    {
      return $this->__soapCall('UpdateBlogProperties', array($parameters));
    }

    /**
     * retrieves the user's blog folder
     *
     * @param GetUserBlog $parameters
     * @access public
     * @return GetUserBlogResponse
     */
    public function GetUserBlog(GetUserBlog $parameters)
    {
      return $this->__soapCall('GetUserBlog', array($parameters));
    }

}
