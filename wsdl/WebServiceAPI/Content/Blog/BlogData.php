<?php

include_once('FolderData.php');

class BlogData extends FolderData
{

    /**
     * @var BlogPostData[] $Content
     * @access public
     */
    public $Content = null;

    /**
     * @var BlogVisibility $Visibility
     * @access public
     */
    public $Visibility = null;

    /**
     * @var string $Title
     * @access public
     */
    public $Title = null;

    /**
     * @var string $Tagline
     * @access public
     */
    public $Tagline = null;

    /**
     * @var int $PostsVisible
     * @access public
     */
    public $PostsVisible = null;

    /**
     * @var boolean $EnableComments
     * @access public
     */
    public $EnableComments = null;

    /**
     * @var boolean $ModerateComments
     * @access public
     */
    public $ModerateComments = null;

    /**
     * @var boolean $RequiresAuthentication
     * @access public
     */
    public $RequiresAuthentication = null;

    /**
     * @var string $NotifyURL
     * @access public
     */
    public $NotifyURL = null;

    /**
     * @var String[] $Categories
     * @access public
     */
    public $Categories = null;

    /**
     * @var BlogRoll $BlogRoll
     * @access public
     */
    public $BlogRoll = null;

    /**
     * @var DateTime[] $PostDates
     * @access public
     */
    public $PostDates = null;

    /**
     * @var DateTime[] $PostMonths
     * @access public
     */
    public $PostMonths = null;

    /**
     * @var DateTime[] $PostYears
     * @access public
     */
    public $PostYears = null;

    /**
     * @param int $Id
     * @param int $TemplateId
     * @param int $ParentId
     * @param string $NameWithPath
     * @param string $FolderIdWithPath
     * @param string $Name
     * @param string $Description
     * @param boolean $IsPermissionsInherited
     * @param boolean $IsHidden
     * @param int $InheritedFrom
     * @param boolean $PrivateContent
     * @param boolean $IsXmlInherited
     * @param int $XmlInheritedFrom
     * @param XmlConfigData[] $XmlConfiguration
     * @param string $StyleSheet
     * @param string $TemplateFileName
     * @param boolean $IsStyleSheetInherited
     * @param boolean $IsTemplateInherited
     * @param int $TemplateInheritedFrom
     * @param int $ApprovalMethod
     * @param FolderApprovalType $ApprovalType
     * @param FolderData[] $ChildFolders
     * @param int $MetaInheritedFrom
     * @param boolean $IsMetaInherited
     * @param boolean $HasChildren
     * @param boolean $PublishPdfActive
     * @param PermissionData $Permissions
     * @param ContentMetaData[] $FolderMetadata
     * @param TemplateData[] $FolderTemplates
     * @param int $TotalContent
     * @param boolean $IsDomainFolder
     * @param string $DomainStaging
     * @param string $DomainProduction
     * @param int $FolderType
     * @param FolderType $Type
     * @param boolean $IsSitemapInherited
     * @param SitemapPath[] $BreadCrumbPath
     * @param boolean $IsCommunityFolder
     * @param boolean $IsTaxonomyInherited
     * @param int $AliasInheritedFrom
     * @param boolean $IsAliasInherited
     * @param boolean $AliasRequired
     * @param int $FlagInheritedFrom
     * @param boolean $IsFlagInherited
     * @param boolean $IsCategoryRequired
     * @param TaxonomyBaseData[] $FolderTaxonomy
     * @param FlagDefData[] $FolderFlags
     * @param int $IsContentSearchableInheritedFrom
     * @param boolean $IsContentSearchableInherited
     * @param boolean $IscontentSearchable
     * @param int $DisplaySettings
     * @param int $DisplaySettingsInheritedFrom
     * @param boolean $IsDisplaySettingsInherited
     * @param BlogPostData[] $Content
     * @param BlogVisibility $Visibility
     * @param string $Title
     * @param string $Tagline
     * @param int $PostsVisible
     * @param boolean $EnableComments
     * @param boolean $ModerateComments
     * @param boolean $RequiresAuthentication
     * @param string $NotifyURL
     * @param String[] $Categories
     * @param BlogRoll $BlogRoll
     * @param DateTime[] $PostDates
     * @param DateTime[] $PostMonths
     * @param DateTime[] $PostYears
     * @access public
     */
    public function __construct($Id, $TemplateId, $ParentId, $NameWithPath, $FolderIdWithPath, $Name, $Description, $IsPermissionsInherited, $IsHidden, $InheritedFrom, $PrivateContent, $IsXmlInherited, $XmlInheritedFrom, $XmlConfiguration, $StyleSheet, $TemplateFileName, $IsStyleSheetInherited, $IsTemplateInherited, $TemplateInheritedFrom, $ApprovalMethod, $ApprovalType, $ChildFolders, $MetaInheritedFrom, $IsMetaInherited, $HasChildren, $PublishPdfActive, $Permissions, $FolderMetadata, $FolderTemplates, $TotalContent, $IsDomainFolder, $DomainStaging, $DomainProduction, $FolderType, $Type, $IsSitemapInherited, $BreadCrumbPath, $IsCommunityFolder, $IsTaxonomyInherited, $AliasInheritedFrom, $IsAliasInherited, $AliasRequired, $FlagInheritedFrom, $IsFlagInherited, $IsCategoryRequired, $FolderTaxonomy, $FolderFlags, $IsContentSearchableInheritedFrom, $IsContentSearchableInherited, $IscontentSearchable, $DisplaySettings, $DisplaySettingsInheritedFrom, $IsDisplaySettingsInherited, $Content, $Visibility, $Title, $Tagline, $PostsVisible, $EnableComments, $ModerateComments, $RequiresAuthentication, $NotifyURL, $Categories, $BlogRoll, $PostDates, $PostMonths, $PostYears)
    {
      parent::__construct($Id, $TemplateId, $ParentId, $NameWithPath, $FolderIdWithPath, $Name, $Description, $IsPermissionsInherited, $IsHidden, $InheritedFrom, $PrivateContent, $IsXmlInherited, $XmlInheritedFrom, $XmlConfiguration, $StyleSheet, $TemplateFileName, $IsStyleSheetInherited, $IsTemplateInherited, $TemplateInheritedFrom, $ApprovalMethod, $ApprovalType, $ChildFolders, $MetaInheritedFrom, $IsMetaInherited, $HasChildren, $PublishPdfActive, $Permissions, $FolderMetadata, $FolderTemplates, $TotalContent, $IsDomainFolder, $DomainStaging, $DomainProduction, $FolderType, $Type, $IsSitemapInherited, $BreadCrumbPath, $IsCommunityFolder, $IsTaxonomyInherited, $AliasInheritedFrom, $IsAliasInherited, $AliasRequired, $FlagInheritedFrom, $IsFlagInherited, $IsCategoryRequired, $FolderTaxonomy, $FolderFlags, $IsContentSearchableInheritedFrom, $IsContentSearchableInherited, $IscontentSearchable, $DisplaySettings, $DisplaySettingsInheritedFrom, $IsDisplaySettingsInherited);
      $this->Content = $Content;
      $this->Visibility = $Visibility;
      $this->Title = $Title;
      $this->Tagline = $Tagline;
      $this->PostsVisible = $PostsVisible;
      $this->EnableComments = $EnableComments;
      $this->ModerateComments = $ModerateComments;
      $this->RequiresAuthentication = $RequiresAuthentication;
      $this->NotifyURL = $NotifyURL;
      $this->Categories = $Categories;
      $this->BlogRoll = $BlogRoll;
      $this->PostDates = $PostDates;
      $this->PostMonths = $PostMonths;
      $this->PostYears = $PostYears;
    }

}
