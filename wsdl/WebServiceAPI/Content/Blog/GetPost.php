<?php

class GetPost
{

    /**
     * @var int $post_id
     * @access public
     */
    public $post_id = null;

    /**
     * @var BlogPostData $PostInfo
     * @access public
     */
    public $PostInfo = null;

    /**
     * @param int $post_id
     * @param BlogPostData $PostInfo
     * @access public
     */
    public function __construct($post_id, $PostInfo)
    {
      $this->post_id = $post_id;
      $this->PostInfo = $PostInfo;
    }

}
