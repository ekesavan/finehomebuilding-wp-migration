<?php

class GetBlogData_x0020_By_x0020_FolderDataResponse
{

    /**
     * @var BlogData $GetBlogData_x0020_By_x0020_FolderDataResult
     * @access public
     */
    public $GetBlogData_x0020_By_x0020_FolderDataResult = null;

    /**
     * @param BlogData $GetBlogData_x0020_By_x0020_FolderDataResult
     * @access public
     */
    public function __construct($GetBlogData_x0020_By_x0020_FolderDataResult)
    {
      $this->GetBlogData_x0020_By_x0020_FolderDataResult = $GetBlogData_x0020_By_x0020_FolderDataResult;
    }

}
