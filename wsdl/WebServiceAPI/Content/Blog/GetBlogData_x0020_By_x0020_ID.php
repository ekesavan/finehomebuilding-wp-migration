<?php

class GetBlogData_x0020_By_x0020_ID
{

    /**
     * @var int $BlogID
     * @access public
     */
    public $BlogID = null;

    /**
     * @param int $BlogID
     * @access public
     */
    public function __construct($BlogID)
    {
      $this->BlogID = $BlogID;
    }

}
