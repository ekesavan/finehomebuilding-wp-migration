<?php

class GetBlankBlogPostDataResponse
{

    /**
     * @var BlogPostData $GetBlankBlogPostDataResult
     * @access public
     */
    public $GetBlankBlogPostDataResult = null;

    /**
     * @param BlogPostData $GetBlankBlogPostDataResult
     * @access public
     */
    public function __construct($GetBlankBlogPostDataResult)
    {
      $this->GetBlankBlogPostDataResult = $GetBlankBlogPostDataResult;
    }

}
