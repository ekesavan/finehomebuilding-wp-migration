<?php

class GetBlogRollResponse
{

    /**
     * @var BlogRoll $GetBlogRollResult
     * @access public
     */
    public $GetBlogRollResult = null;

    /**
     * @param BlogRoll $GetBlogRollResult
     * @access public
     */
    public function __construct($GetBlogRollResult)
    {
      $this->GetBlogRollResult = $GetBlogRollResult;
    }

}
