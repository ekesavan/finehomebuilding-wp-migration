<?php

class DeletePost
{

    /**
     * @var int $post_id
     * @access public
     */
    public $post_id = null;

    /**
     * @param int $post_id
     * @access public
     */
    public function __construct($post_id)
    {
      $this->post_id = $post_id;
    }

}
