<?php

class GetBlog_x0020_by_x0020_ID_x002C__x0020_StartDate_x002C__x0020_MaxEntries_x0020_and_x0020_LanguageID
{

    /**
     * @var int $ID
     * @access public
     */
    public $ID = null;

    /**
     * @var string $StartDate
     * @access public
     */
    public $StartDate = null;

    /**
     * @var int $MaxEntries
     * @access public
     */
    public $MaxEntries = null;

    /**
     * @var int $LanguageID
     * @access public
     */
    public $LanguageID = null;

    /**
     * @param int $ID
     * @param string $StartDate
     * @param int $MaxEntries
     * @param int $LanguageID
     * @access public
     */
    public function __construct($ID, $StartDate, $MaxEntries, $LanguageID)
    {
      $this->ID = $ID;
      $this->StartDate = $StartDate;
      $this->MaxEntries = $MaxEntries;
      $this->LanguageID = $LanguageID;
    }

}
