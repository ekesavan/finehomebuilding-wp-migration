<?php

class AddPost
{

    /**
     * @var int $blogID
     * @access public
     */
    public $blogID = null;

    /**
     * @var ContentData $PostContent
     * @access public
     */
    public $PostContent = null;

    /**
     * @var String[] $Categories
     * @access public
     */
    public $Categories = null;

    /**
     * @var boolean $Pingback
     * @access public
     */
    public $Pingback = null;

    /**
     * @var string $Tags
     * @access public
     */
    public $Tags = null;

    /**
     * @var string $TrackbackURL
     * @access public
     */
    public $TrackbackURL = null;

    /**
     * @param int $blogID
     * @param ContentData $PostContent
     * @param String[] $Categories
     * @param boolean $Pingback
     * @param string $Tags
     * @param string $TrackbackURL
     * @access public
     */
    public function __construct($blogID, $PostContent, $Categories, $Pingback, $Tags, $TrackbackURL)
    {
      $this->blogID = $blogID;
      $this->PostContent = $PostContent;
      $this->Categories = $Categories;
      $this->Pingback = $Pingback;
      $this->Tags = $Tags;
      $this->TrackbackURL = $TrackbackURL;
    }

}
