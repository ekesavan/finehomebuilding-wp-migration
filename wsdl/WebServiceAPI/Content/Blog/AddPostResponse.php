<?php

class AddPostResponse
{

    /**
     * @var int $AddPostResult
     * @access public
     */
    public $AddPostResult = null;

    /**
     * @param int $AddPostResult
     * @access public
     */
    public function __construct($AddPostResult)
    {
      $this->AddPostResult = $AddPostResult;
    }

}
