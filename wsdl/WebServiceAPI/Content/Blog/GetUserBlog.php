<?php

class GetUserBlog
{

    /**
     * @var int $userId
     * @access public
     */
    public $userId = null;

    /**
     * @param int $userId
     * @access public
     */
    public function __construct($userId)
    {
      $this->userId = $userId;
    }

}
