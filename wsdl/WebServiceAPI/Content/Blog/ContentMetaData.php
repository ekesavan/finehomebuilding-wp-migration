<?php

include_once('CmsDataOfContentMetaData.php');

class ContentMetaData extends CmsDataOfContentMetaData
{

    /**
     * @var string $Text
     * @access public
     */
    public $Text = null;

    /**
     * @var string $Name
     * @access public
     */
    public $Name = null;

    /**
     * @var ContentMetadataDataType $DataType
     * @access public
     */
    public $DataType = null;

    /**
     * @var string $DefaultValue
     * @access public
     */
    public $DefaultValue = null;

    /**
     * @var boolean $Required
     * @access public
     */
    public $Required = null;

    /**
     * @var ContentMetadataType $Type
     * @access public
     */
    public $Type = null;

    /**
     * @var boolean $IsEditable
     * @access public
     */
    public $IsEditable = null;

    /**
     * @var string $Separator
     * @access public
     */
    public $Separator = null;

    /**
     * @var boolean $CaseSensitive
     * @access public
     */
    public $CaseSensitive = null;

    /**
     * @var boolean $RemoveDuplicate
     * @access public
     */
    public $RemoveDuplicate = null;

    /**
     * @var boolean $IsSearchAllowed
     * @access public
     */
    public $IsSearchAllowed = null;

    /**
     * @var boolean $IsDisplayable
     * @access public
     */
    public $IsDisplayable = null;

    /**
     * @var int $Language
     * @access public
     */
    public $Language = null;

    /**
     * @var string $SelectableText
     * @access public
     */
    public $SelectableText = null;

    /**
     * @var boolean $AllowMultiple
     * @access public
     */
    public $AllowMultiple = null;

    /**
     * @var boolean $IsSelectableOnly
     * @access public
     */
    public $IsSelectableOnly = null;

    /**
     * @var boolean $MetaDisplayEE
     * @access public
     */
    public $MetaDisplayEE = null;

    /**
     * @var int $BlogMetaType
     * @access public
     */
    public $BlogMetaType = null;

    /**
     * @var int $ObjectType
     * @access public
     */
    public $ObjectType = null;

    /**
     * @param int $Id
     * @param string $Text
     * @param string $Name
     * @param ContentMetadataDataType $DataType
     * @param string $DefaultValue
     * @param boolean $Required
     * @param ContentMetadataType $Type
     * @param boolean $IsEditable
     * @param string $Separator
     * @param boolean $CaseSensitive
     * @param boolean $RemoveDuplicate
     * @param boolean $IsSearchAllowed
     * @param boolean $IsDisplayable
     * @param int $Language
     * @param string $SelectableText
     * @param boolean $AllowMultiple
     * @param boolean $IsSelectableOnly
     * @param boolean $MetaDisplayEE
     * @param int $BlogMetaType
     * @param int $ObjectType
     * @access public
     */
    public function __construct($Id, $Text, $Name, $DataType, $DefaultValue, $Required, $Type, $IsEditable, $Separator, $CaseSensitive, $RemoveDuplicate, $IsSearchAllowed, $IsDisplayable, $Language, $SelectableText, $AllowMultiple, $IsSelectableOnly, $MetaDisplayEE, $BlogMetaType, $ObjectType)
    {
      parent::__construct($Id);
      $this->Text = $Text;
      $this->Name = $Name;
      $this->DataType = $DataType;
      $this->DefaultValue = $DefaultValue;
      $this->Required = $Required;
      $this->Type = $Type;
      $this->IsEditable = $IsEditable;
      $this->Separator = $Separator;
      $this->CaseSensitive = $CaseSensitive;
      $this->RemoveDuplicate = $RemoveDuplicate;
      $this->IsSearchAllowed = $IsSearchAllowed;
      $this->IsDisplayable = $IsDisplayable;
      $this->Language = $Language;
      $this->SelectableText = $SelectableText;
      $this->AllowMultiple = $AllowMultiple;
      $this->IsSelectableOnly = $IsSelectableOnly;
      $this->MetaDisplayEE = $MetaDisplayEE;
      $this->BlogMetaType = $BlogMetaType;
      $this->ObjectType = $ObjectType;
    }

}
