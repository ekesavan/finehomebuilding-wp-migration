<?php

class DeleteBlogPostComment
{

    /**
     * @var int $postComment_id
     * @access public
     */
    public $postComment_id = null;

    /**
     * @param int $postComment_id
     * @access public
     */
    public function __construct($postComment_id)
    {
      $this->postComment_id = $postComment_id;
    }

}
