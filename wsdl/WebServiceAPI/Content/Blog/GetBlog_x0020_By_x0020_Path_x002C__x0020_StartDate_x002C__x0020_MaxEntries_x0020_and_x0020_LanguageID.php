<?php

class GetBlog_x0020_By_x0020_Path_x002C__x0020_StartDate_x002C__x0020_MaxEntries_x0020_and_x0020_LanguageID
{

    /**
     * @var string $path
     * @access public
     */
    public $path = null;

    /**
     * @var string $StartDate
     * @access public
     */
    public $StartDate = null;

    /**
     * @var int $MaxEntries
     * @access public
     */
    public $MaxEntries = null;

    /**
     * @var int $LanguageID
     * @access public
     */
    public $LanguageID = null;

    /**
     * @param string $path
     * @param string $StartDate
     * @param int $MaxEntries
     * @param int $LanguageID
     * @access public
     */
    public function __construct($path, $StartDate, $MaxEntries, $LanguageID)
    {
      $this->path = $path;
      $this->StartDate = $StartDate;
      $this->MaxEntries = $MaxEntries;
      $this->LanguageID = $LanguageID;
    }

}
