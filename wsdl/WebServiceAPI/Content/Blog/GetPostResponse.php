<?php

class GetPostResponse
{

    /**
     * @var ContentData $GetPostResult
     * @access public
     */
    public $GetPostResult = null;

    /**
     * @param ContentData $GetPostResult
     * @access public
     */
    public function __construct($GetPostResult)
    {
      $this->GetPostResult = $GetPostResult;
    }

}
