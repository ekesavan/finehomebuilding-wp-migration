<?php

class GetUserBlogResponse
{

    /**
     * @var int $GetUserBlogResult
     * @access public
     */
    public $GetUserBlogResult = null;

    /**
     * @param int $GetUserBlogResult
     * @access public
     */
    public function __construct($GetUserBlogResult)
    {
      $this->GetUserBlogResult = $GetUserBlogResult;
    }

}
