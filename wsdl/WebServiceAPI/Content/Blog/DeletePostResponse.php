<?php

class DeletePostResponse
{

    /**
     * @var boolean $DeletePostResult
     * @access public
     */
    public $DeletePostResult = null;

    /**
     * @param boolean $DeletePostResult
     * @access public
     */
    public function __construct($DeletePostResult)
    {
      $this->DeletePostResult = $DeletePostResult;
    }

}
