<?php

class AddCommentForPostResponse
{

    /**
     * @var boolean $AddCommentForPostResult
     * @access public
     */
    public $AddCommentForPostResult = null;

    /**
     * @param boolean $AddCommentForPostResult
     * @access public
     */
    public function __construct($AddCommentForPostResult)
    {
      $this->AddCommentForPostResult = $AddCommentForPostResult;
    }

}
