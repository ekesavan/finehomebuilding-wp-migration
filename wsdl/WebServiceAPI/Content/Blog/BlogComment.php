<?php

class BlogComment
{

    /**
     * @var string $Title
     * @access public
     */
    public $Title = null;

    /**
     * @var string $DisplayName
     * @access public
     */
    public $DisplayName = null;

    /**
     * @var string $Email
     * @access public
     */
    public $Email = null;

    /**
     * @var string $URI
     * @access public
     */
    public $URI = null;

    /**
     * @var string $Message
     * @access public
     */
    public $Message = null;

    /**
     * @var int $ID
     * @access public
     */
    public $ID = null;

    /**
     * @var dateTime $DateCreated
     * @access public
     */
    public $DateCreated = null;

    /**
     * @var int $PostID
     * @access public
     */
    public $PostID = null;

    /**
     * @var int $UserID
     * @access public
     */
    public $UserID = null;

    /**
     * @var boolean $IsPending
     * @access public
     */
    public $IsPending = null;

    /**
     * @param string $Title
     * @param string $DisplayName
     * @param string $Email
     * @param string $URI
     * @param string $Message
     * @param int $ID
     * @param dateTime $DateCreated
     * @param int $PostID
     * @param int $UserID
     * @param boolean $IsPending
     * @access public
     */
    public function __construct($Title, $DisplayName, $Email, $URI, $Message, $ID, $DateCreated, $PostID, $UserID, $IsPending)
    {
      $this->Title = $Title;
      $this->DisplayName = $DisplayName;
      $this->Email = $Email;
      $this->URI = $URI;
      $this->Message = $Message;
      $this->ID = $ID;
      $this->DateCreated = $DateCreated;
      $this->PostID = $PostID;
      $this->UserID = $UserID;
      $this->IsPending = $IsPending;
    }

}
