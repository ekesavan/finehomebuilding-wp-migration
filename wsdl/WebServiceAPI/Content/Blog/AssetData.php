<?php

class AssetData
{

    /**
     * @var string $Id
     * @access public
     */
    public $Id = null;

    /**
     * @var string $Version
     * @access public
     */
    public $Version = null;

    /**
     * @var string $MimeType
     * @access public
     */
    public $MimeType = null;

    /**
     * @var string $MimeName
     * @access public
     */
    public $MimeName = null;

    /**
     * @var string $FileName
     * @access public
     */
    public $FileName = null;

    /**
     * @var string $FileExtension
     * @access public
     */
    public $FileExtension = null;

    /**
     * @var string $ImageUrl
     * @access public
     */
    public $ImageUrl = null;

    /**
     * @var string $Icon
     * @access public
     */
    public $Icon = null;

    /**
     * @var string $Status
     * @access public
     */
    public $Status = null;

    /**
     * @var int $Language
     * @access public
     */
    public $Language = null;

    /**
     * @var int $Type
     * @access public
     */
    public $Type = null;

    /**
     * @var string $PluginType
     * @access public
     */
    public $PluginType = null;

    /**
     * @var boolean $PublishPdfActive
     * @access public
     */
    public $PublishPdfActive = null;

    /**
     * @param string $Id
     * @param string $Version
     * @param string $MimeType
     * @param string $MimeName
     * @param string $FileName
     * @param string $FileExtension
     * @param string $ImageUrl
     * @param string $Icon
     * @param string $Status
     * @param int $Language
     * @param int $Type
     * @param string $PluginType
     * @param boolean $PublishPdfActive
     * @access public
     */
    public function __construct($Id, $Version, $MimeType, $MimeName, $FileName, $FileExtension, $ImageUrl, $Icon, $Status, $Language, $Type, $PluginType, $PublishPdfActive)
    {
      $this->Id = $Id;
      $this->Version = $Version;
      $this->MimeType = $MimeType;
      $this->MimeName = $MimeName;
      $this->FileName = $FileName;
      $this->FileExtension = $FileExtension;
      $this->ImageUrl = $ImageUrl;
      $this->Icon = $Icon;
      $this->Status = $Status;
      $this->Language = $Language;
      $this->Type = $Type;
      $this->PluginType = $PluginType;
      $this->PublishPdfActive = $PublishPdfActive;
    }

}
