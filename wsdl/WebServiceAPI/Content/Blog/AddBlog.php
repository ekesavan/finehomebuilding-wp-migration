<?php

class AddBlog
{

    /**
     * @var int $ParentFolderID
     * @access public
     */
    public $ParentFolderID = null;

    /**
     * @var string $BlogName
     * @access public
     */
    public $BlogName = null;

    /**
     * @var string $BlogTitle
     * @access public
     */
    public $BlogTitle = null;

    /**
     * @var string $BlogDescription
     * @access public
     */
    public $BlogDescription = null;

    /**
     * @var BlogVisibility $Visibility
     * @access public
     */
    public $Visibility = null;

    /**
     * @var boolean $EnableComments
     * @access public
     */
    public $EnableComments = null;

    /**
     * @var boolean $ModerateComments
     * @access public
     */
    public $ModerateComments = null;

    /**
     * @var boolean $RequireAuthentication
     * @access public
     */
    public $RequireAuthentication = null;

    /**
     * @var String[] $Categories
     * @access public
     */
    public $Categories = null;

    /**
     * @var BlogRoll $RollLinks
     * @access public
     */
    public $RollLinks = null;

    /**
     * @param int $ParentFolderID
     * @param string $BlogName
     * @param string $BlogTitle
     * @param string $BlogDescription
     * @param BlogVisibility $Visibility
     * @param boolean $EnableComments
     * @param boolean $ModerateComments
     * @param boolean $RequireAuthentication
     * @param String[] $Categories
     * @param BlogRoll $RollLinks
     * @access public
     */
    public function __construct($ParentFolderID, $BlogName, $BlogTitle, $BlogDescription, $Visibility, $EnableComments, $ModerateComments, $RequireAuthentication, $Categories, $RollLinks)
    {
      $this->ParentFolderID = $ParentFolderID;
      $this->BlogName = $BlogName;
      $this->BlogTitle = $BlogTitle;
      $this->BlogDescription = $BlogDescription;
      $this->Visibility = $Visibility;
      $this->EnableComments = $EnableComments;
      $this->ModerateComments = $ModerateComments;
      $this->RequireAuthentication = $RequireAuthentication;
      $this->Categories = $Categories;
      $this->RollLinks = $RollLinks;
    }

}
