<?php

include_once('ContentBase.php');

class BlogPostData extends ContentBase
{

    /**
     * @var string $TrackBackURL
     * @access public
     */
    public $TrackBackURL = null;

    /**
     * @var string $Tags
     * @access public
     */
    public $Tags = null;

    /**
     * @var boolean $Pingback
     * @access public
     */
    public $Pingback = null;

    /**
     * @var String[] $Categories
     * @access public
     */
    public $Categories = null;

    /**
     * @var int $TrackBackURLID
     * @access public
     */
    public $TrackBackURLID = null;

    /**
     * @var int $TagsID
     * @access public
     */
    public $TagsID = null;

    /**
     * @var int $PingBackID
     * @access public
     */
    public $PingBackID = null;

    /**
     * @var int $CategoryID
     * @access public
     */
    public $CategoryID = null;

    /**
     * @var string $Message
     * @access public
     */
    public $Message = null;

    /**
     * @var BlogComment[] $Comments
     * @access public
     */
    public $Comments = null;

    /**
     * @var int $PostID
     * @access public
     */
    public $PostID = null;

    /**
     * @var string $CreatorDisplayName
     * @access public
     */
    public $CreatorDisplayName = null;

    /**
     * @var string $CreatorFName
     * @access public
     */
    public $CreatorFName = null;

    /**
     * @var string $CreatorLName
     * @access public
     */
    public $CreatorLName = null;

    /**
     * @param string $Title
     * @param int $Id
     * @param int $Language
     * @param string $Html
     * @param int $FolderId
     * @param boolean $IsPrivate
     * @param dateTime $GoLiveDate
     * @param dateTime $StartDate
     * @param string $DisplayStartDate
     * @param dateTime $EndDate
     * @param string $DisplayGoLiveDate
     * @param string $DisplayEndDate
     * @param CMSEndDateAction $EndDateAction
     * @param string $ContentStatus
     * @param string $Xslt1
     * @param string $Xslt2
     * @param string $Xslt3
     * @param string $Xslt4
     * @param string $Xslt5
     * @param int $DefaultXslt
     * @param string $PackageDisplayXslt
     * @param CMSContentType $ContentType
     * @param CMSContentSubtype $ContentSubType
     * @param int $ExternalTypeId
     * @param dateTime $DateCreated
     * @param string $DisplayDateCreated
     * @param dateTime $DateModified
     * @param string $DisplayDateModified
     * @param string $LastEditorFname
     * @param string $LastEditorLname
     * @param int $UserId
     * @param boolean $IsInherited
     * @param int $InheritedFrom
     * @param string $TemplateLink
     * @param string $QuickLink
     * @param string $HyperLink
     * @param string $Teaser
     * @param ItemStatus $Status
     * @param string $Comment
     * @param string $LanguageDescription
     * @param AssetData $AssetInfo
     * @param string $StagingDomain
     * @param string $ProductionDomain
     * @param int $XMLCollectionID
     * @param string $FieldList
     * @param string $Image
     * @param string $ImageThumbnail
     * @param boolean $IsPublished
     * @param AnalyticsData $AnalyticsInfo
     * @param string $TrackBackURL
     * @param string $Tags
     * @param boolean $Pingback
     * @param String[] $Categories
     * @param int $TrackBackURLID
     * @param int $TagsID
     * @param int $PingBackID
     * @param int $CategoryID
     * @param string $Message
     * @param BlogComment[] $Comments
     * @param int $PostID
     * @param string $CreatorDisplayName
     * @param string $CreatorFName
     * @param string $CreatorLName
     * @access public
     */
    public function __construct($Title, $Id, $Language, $Html, $FolderId, $IsPrivate, $GoLiveDate, $StartDate, $DisplayStartDate, $EndDate, $DisplayGoLiveDate, $DisplayEndDate, $EndDateAction, $ContentStatus, $Xslt1, $Xslt2, $Xslt3, $Xslt4, $Xslt5, $DefaultXslt, $PackageDisplayXslt, $ContentType, $ContentSubType, $ExternalTypeId, $DateCreated, $DisplayDateCreated, $DateModified, $DisplayDateModified, $LastEditorFname, $LastEditorLname, $UserId, $IsInherited, $InheritedFrom, $TemplateLink, $QuickLink, $HyperLink, $Teaser, $Status, $Comment, $LanguageDescription, $AssetInfo, $StagingDomain, $ProductionDomain, $XMLCollectionID, $FieldList, $Image, $ImageThumbnail, $IsPublished, $AnalyticsInfo, $TrackBackURL, $Tags, $Pingback, $Categories, $TrackBackURLID, $TagsID, $PingBackID, $CategoryID, $Message, $Comments, $PostID, $CreatorDisplayName, $CreatorFName, $CreatorLName)
    {
      parent::__construct($Title, $Id, $Language, $Html, $FolderId, $IsPrivate, $GoLiveDate, $StartDate, $DisplayStartDate, $EndDate, $DisplayGoLiveDate, $DisplayEndDate, $EndDateAction, $ContentStatus, $Xslt1, $Xslt2, $Xslt3, $Xslt4, $Xslt5, $DefaultXslt, $PackageDisplayXslt, $ContentType, $ContentSubType, $ExternalTypeId, $DateCreated, $DisplayDateCreated, $DateModified, $DisplayDateModified, $LastEditorFname, $LastEditorLname, $UserId, $IsInherited, $InheritedFrom, $TemplateLink, $QuickLink, $HyperLink, $Teaser, $Status, $Comment, $LanguageDescription, $AssetInfo, $StagingDomain, $ProductionDomain, $XMLCollectionID, $FieldList, $Image, $ImageThumbnail, $IsPublished, $AnalyticsInfo);
      $this->TrackBackURL = $TrackBackURL;
      $this->Tags = $Tags;
      $this->Pingback = $Pingback;
      $this->Categories = $Categories;
      $this->TrackBackURLID = $TrackBackURLID;
      $this->TagsID = $TagsID;
      $this->PingBackID = $PingBackID;
      $this->CategoryID = $CategoryID;
      $this->Message = $Message;
      $this->Comments = $Comments;
      $this->PostID = $PostID;
      $this->CreatorDisplayName = $CreatorDisplayName;
      $this->CreatorFName = $CreatorFName;
      $this->CreatorLName = $CreatorLName;
    }

}
