<?php

class UpdateBlogProperties
{

    /**
     * @var BlogData $blog
     * @access public
     */
    public $blog = null;

    /**
     * @param BlogData $blog
     * @access public
     */
    public function __construct($blog)
    {
      $this->blog = $blog;
    }

}
