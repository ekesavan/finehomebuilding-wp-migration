<?php

class GetBlogData_x0020_by_x0020_ID_x002C__x0020_StartDate_x002C__x0020_PostsVisible_x0020_and_x0020_LanguageID
{

    /**
     * @var int $BlogID
     * @access public
     */
    public $BlogID = null;

    /**
     * @var dateTime $StartDate
     * @access public
     */
    public $StartDate = null;

    /**
     * @var int $Postsvisible
     * @access public
     */
    public $Postsvisible = null;

    /**
     * @var int $LanguageID
     * @access public
     */
    public $LanguageID = null;

    /**
     * @param int $BlogID
     * @param dateTime $StartDate
     * @param int $Postsvisible
     * @param int $LanguageID
     * @access public
     */
    public function __construct($BlogID, $StartDate, $Postsvisible, $LanguageID)
    {
      $this->BlogID = $BlogID;
      $this->StartDate = $StartDate;
      $this->Postsvisible = $Postsvisible;
      $this->LanguageID = $LanguageID;
    }

}
