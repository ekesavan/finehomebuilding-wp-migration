<?php

class UpdateBlogPropertiesResponse
{

    /**
     * @var boolean $UpdateBlogPropertiesResult
     * @access public
     */
    public $UpdateBlogPropertiesResult = null;

    /**
     * @param boolean $UpdateBlogPropertiesResult
     * @access public
     */
    public function __construct($UpdateBlogPropertiesResult)
    {
      $this->UpdateBlogPropertiesResult = $UpdateBlogPropertiesResult;
    }

}
