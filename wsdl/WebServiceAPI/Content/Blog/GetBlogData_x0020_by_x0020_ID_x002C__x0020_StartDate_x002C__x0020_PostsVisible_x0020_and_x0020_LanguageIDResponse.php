<?php

class GetBlogData_x0020_by_x0020_ID_x002C__x0020_StartDate_x002C__x0020_PostsVisible_x0020_and_x0020_LanguageIDResponse
{

    /**
     * @var BlogData $GetBlogData_x0020_by_x0020_ID_x002C__x0020_StartDate_x002C__x0020_PostsVisible_x0020_and_x0020_LanguageIDResult
     * @access public
     */
    public $GetBlogData_x0020_by_x0020_ID_x002C__x0020_StartDate_x002C__x0020_PostsVisible_x0020_and_x0020_LanguageIDResult = null;

    /**
     * @param BlogData $GetBlogData_x0020_by_x0020_ID_x002C__x0020_StartDate_x002C__x0020_PostsVisible_x0020_and_x0020_LanguageIDResult
     * @access public
     */
    public function __construct($GetBlogData_x0020_by_x0020_ID_x002C__x0020_StartDate_x002C__x0020_PostsVisible_x0020_and_x0020_LanguageIDResult)
    {
      $this->GetBlogData_x0020_by_x0020_ID_x002C__x0020_StartDate_x002C__x0020_PostsVisible_x0020_and_x0020_LanguageIDResult = $GetBlogData_x0020_by_x0020_ID_x002C__x0020_StartDate_x002C__x0020_PostsVisible_x0020_and_x0020_LanguageIDResult;
    }

}
