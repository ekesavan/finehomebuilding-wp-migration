<?php

include_once('CmsDataOfTemplateData.php');

class TemplateData extends CmsDataOfTemplateData
{

    /**
     * @var string $TemplateName
     * @access public
     */
    public $TemplateName = null;

    /**
     * @var string $FileName
     * @access public
     */
    public $FileName = null;

    /**
     * @var TemplateType $Type
     * @access public
     */
    public $Type = null;

    /**
     * @var TemplateSubType $SubType
     * @access public
     */
    public $SubType = null;

    /**
     * @var string $Thumbnail
     * @access public
     */
    public $Thumbnail = null;

    /**
     * @var string $Description
     * @access public
     */
    public $Description = null;

    /**
     * @var int $MasterLayoutID
     * @access public
     */
    public $MasterLayoutID = null;

    /**
     * @param int $Id
     * @param string $TemplateName
     * @param string $FileName
     * @param TemplateType $Type
     * @param TemplateSubType $SubType
     * @param string $Thumbnail
     * @param string $Description
     * @param int $MasterLayoutID
     * @access public
     */
    public function __construct($Id, $TemplateName, $FileName, $Type, $SubType, $Thumbnail, $Description, $MasterLayoutID)
    {
      parent::__construct($Id);
      $this->TemplateName = $TemplateName;
      $this->FileName = $FileName;
      $this->Type = $Type;
      $this->SubType = $SubType;
      $this->Thumbnail = $Thumbnail;
      $this->Description = $Description;
      $this->MasterLayoutID = $MasterLayoutID;
    }

}
