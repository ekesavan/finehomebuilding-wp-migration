<?php

class DeleteBlog
{

    /**
     * @var int $BlogID
     * @access public
     */
    public $BlogID = null;

    /**
     * @param int $BlogID
     * @access public
     */
    public function __construct($BlogID)
    {
      $this->BlogID = $BlogID;
    }

}
