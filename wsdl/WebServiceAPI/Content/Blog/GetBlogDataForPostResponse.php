<?php

class GetBlogDataForPostResponse
{

    /**
     * @var BlogData $GetBlogDataForPostResult
     * @access public
     */
    public $GetBlogDataForPostResult = null;

    /**
     * @param BlogData $GetBlogDataForPostResult
     * @access public
     */
    public function __construct($GetBlogDataForPostResult)
    {
      $this->GetBlogDataForPostResult = $GetBlogDataForPostResult;
    }

}
