<?php

class GetPostbyIDResponse
{

    /**
     * @var BlogPostData $GetPostbyIDResult
     * @access public
     */
    public $GetPostbyIDResult = null;

    /**
     * @param BlogPostData $GetPostbyIDResult
     * @access public
     */
    public function __construct($GetPostbyIDResult)
    {
      $this->GetPostbyIDResult = $GetPostbyIDResult;
    }

}
