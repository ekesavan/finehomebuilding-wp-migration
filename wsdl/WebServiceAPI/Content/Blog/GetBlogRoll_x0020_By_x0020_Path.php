<?php

class GetBlogRoll_x0020_By_x0020_Path
{

    /**
     * @var string $Path
     * @access public
     */
    public $Path = null;

    /**
     * @param string $Path
     * @access public
     */
    public function __construct($Path)
    {
      $this->Path = $Path;
    }

}
