<?php

class GetBlogData_x0020_By_x0020_IDResponse
{

    /**
     * @var BlogData $GetBlogData_x0020_By_x0020_IDResult
     * @access public
     */
    public $GetBlogData_x0020_By_x0020_IDResult = null;

    /**
     * @param BlogData $GetBlogData_x0020_By_x0020_IDResult
     * @access public
     */
    public function __construct($GetBlogData_x0020_By_x0020_IDResult)
    {
      $this->GetBlogData_x0020_By_x0020_IDResult = $GetBlogData_x0020_By_x0020_IDResult;
    }

}
