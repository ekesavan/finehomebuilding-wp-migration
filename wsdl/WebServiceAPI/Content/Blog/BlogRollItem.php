<?php

class BlogRollItem
{

    /**
     * @var string $LinkName
     * @access public
     */
    public $LinkName = null;

    /**
     * @var string $URL
     * @access public
     */
    public $URL = null;

    /**
     * @var string $ShortDescription
     * @access public
     */
    public $ShortDescription = null;

    /**
     * @var string $Relationship
     * @access public
     */
    public $Relationship = null;

    /**
     * @var int $ID
     * @access public
     */
    public $ID = null;

    /**
     * @param string $LinkName
     * @param string $URL
     * @param string $ShortDescription
     * @param string $Relationship
     * @param int $ID
     * @access public
     */
    public function __construct($LinkName, $URL, $ShortDescription, $Relationship, $ID)
    {
      $this->LinkName = $LinkName;
      $this->URL = $URL;
      $this->ShortDescription = $ShortDescription;
      $this->Relationship = $Relationship;
      $this->ID = $ID;
    }

}
