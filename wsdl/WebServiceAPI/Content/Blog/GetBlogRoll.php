<?php

class GetBlogRoll
{

    /**
     * @var int $blog_id
     * @access public
     */
    public $blog_id = null;

    /**
     * @param int $blog_id
     * @access public
     */
    public function __construct($blog_id)
    {
      $this->blog_id = $blog_id;
    }

}
