<?php

class GetBlogPostDataResponse
{

    /**
     * @var BlogPostData $GetBlogPostDataResult
     * @access public
     */
    public $GetBlogPostDataResult = null;

    /**
     * @param BlogPostData $GetBlogPostDataResult
     * @access public
     */
    public function __construct($GetBlogPostDataResult)
    {
      $this->GetBlogPostDataResult = $GetBlogPostDataResult;
    }

}
