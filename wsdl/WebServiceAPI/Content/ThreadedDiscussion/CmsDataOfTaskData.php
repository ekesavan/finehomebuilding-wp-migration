<?php

include_once('BaseDataOfTaskData.php');

class CmsDataOfTaskData extends BaseDataOfTaskData
{

    /**
     * @var int $Id
     * @access public
     */
    public $Id = null;

    /**
     * @param int $Id
     * @access public
     */
    public function __construct($Id)
    {
      $this->Id = $Id;
    }

}
