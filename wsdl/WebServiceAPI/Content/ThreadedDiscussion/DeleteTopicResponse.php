<?php

class DeleteTopicResponse
{

    /**
     * @var boolean $DeleteTopicResult
     * @access public
     */
    public $DeleteTopicResult = null;

    /**
     * @param boolean $DeleteTopicResult
     * @access public
     */
    public function __construct($DeleteTopicResult)
    {
      $this->DeleteTopicResult = $DeleteTopicResult;
    }

}
