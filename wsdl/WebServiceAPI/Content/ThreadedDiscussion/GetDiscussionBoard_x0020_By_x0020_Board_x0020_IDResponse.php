<?php

class GetDiscussionBoard_x0020_By_x0020_Board_x0020_IDResponse
{

    /**
     * @var DiscussionBoard $GetDiscussionBoard_x0020_By_x0020_Board_x0020_IDResult
     * @access public
     */
    public $GetDiscussionBoard_x0020_By_x0020_Board_x0020_IDResult = null;

    /**
     * @param DiscussionBoard $GetDiscussionBoard_x0020_By_x0020_Board_x0020_IDResult
     * @access public
     */
    public function __construct($GetDiscussionBoard_x0020_By_x0020_Board_x0020_IDResult)
    {
      $this->GetDiscussionBoard_x0020_By_x0020_Board_x0020_IDResult = $GetDiscussionBoard_x0020_By_x0020_Board_x0020_IDResult;
    }

}
