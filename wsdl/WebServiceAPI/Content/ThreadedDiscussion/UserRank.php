<?php

class UserRank
{

    /**
     * @var string $Name
     * @access public
     */
    public $Name = null;

    /**
     * @var int $Posts
     * @access public
     */
    public $Posts = null;

    /**
     * @var string $IconImage
     * @access public
     */
    public $IconImage = null;

    /**
     * @var boolean $StartGroup
     * @access public
     */
    public $StartGroup = null;

    /**
     * @var FolderData[] $AppliesTo
     * @access public
     */
    public $AppliesTo = null;

    /**
     * @param string $Name
     * @param int $Posts
     * @param string $IconImage
     * @param boolean $StartGroup
     * @param FolderData[] $AppliesTo
     * @access public
     */
    public function __construct($Name, $Posts, $IconImage, $StartGroup, $AppliesTo)
    {
      $this->Name = $Name;
      $this->Posts = $Posts;
      $this->IconImage = $IconImage;
      $this->StartGroup = $StartGroup;
      $this->AppliesTo = $AppliesTo;
    }

}
