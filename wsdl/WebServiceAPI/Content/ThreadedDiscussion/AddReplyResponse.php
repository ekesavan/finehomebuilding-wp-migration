<?php

class AddReplyResponse
{

    /**
     * @var boolean $AddReplyResult
     * @access public
     */
    public $AddReplyResult = null;

    /**
     * @var TaskData $data
     * @access public
     */
    public $data = null;

    /**
     * @param boolean $AddReplyResult
     * @param TaskData $data
     * @access public
     */
    public function __construct($AddReplyResult, $data)
    {
      $this->AddReplyResult = $AddReplyResult;
      $this->data = $data;
    }

}
