<?php

class GetBoardCategoriesResponse
{

    /**
     * @var DiscussionCategory[] $GetBoardCategoriesResult
     * @access public
     */
    public $GetBoardCategoriesResult = null;

    /**
     * @param DiscussionCategory[] $GetBoardCategoriesResult
     * @access public
     */
    public function __construct($GetBoardCategoriesResult)
    {
      $this->GetBoardCategoriesResult = $GetBoardCategoriesResult;
    }

}
