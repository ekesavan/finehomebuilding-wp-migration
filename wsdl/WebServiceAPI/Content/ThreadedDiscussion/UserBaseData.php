<?php

include_once('CmsDataOfUserBaseData.php');

class UserBaseData extends CmsDataOfUserBaseData
{

    /**
     * @var string $Avatar
     * @access public
     */
    public $Avatar = null;

    /**
     * @var string $Username
     * @access public
     */
    public $Username = null;

    /**
     * @var string $FirstName
     * @access public
     */
    public $FirstName = null;

    /**
     * @var string $LastName
     * @access public
     */
    public $LastName = null;

    /**
     * @var string $Domain
     * @access public
     */
    public $Domain = null;

    /**
     * @var string $DisplayUserName
     * @access public
     */
    public $DisplayUserName = null;

    /**
     * @var string $Password
     * @access public
     */
    public $Password = null;

    /**
     * @var string $DisplayName
     * @access public
     */
    public $DisplayName = null;

    /**
     * @var string $Email
     * @access public
     */
    public $Email = null;

    /**
     * @var string $Address
     * @access public
     */
    public $Address = null;

    /**
     * @var float $Latitude
     * @access public
     */
    public $Latitude = null;

    /**
     * @var float $Longitude
     * @access public
     */
    public $Longitude = null;

    /**
     * @param int $Id
     * @param string $Avatar
     * @param string $Username
     * @param string $FirstName
     * @param string $LastName
     * @param string $Domain
     * @param string $DisplayUserName
     * @param string $Password
     * @param string $DisplayName
     * @param string $Email
     * @param string $Address
     * @param float $Latitude
     * @param float $Longitude
     * @access public
     */
    public function __construct($Id, $Avatar, $Username, $FirstName, $LastName, $Domain, $DisplayUserName, $Password, $DisplayName, $Email, $Address, $Latitude, $Longitude)
    {
      parent::__construct($Id);
      $this->Avatar = $Avatar;
      $this->Username = $Username;
      $this->FirstName = $FirstName;
      $this->LastName = $LastName;
      $this->Domain = $Domain;
      $this->DisplayUserName = $DisplayUserName;
      $this->Password = $Password;
      $this->DisplayName = $DisplayName;
      $this->Email = $Email;
      $this->Address = $Address;
      $this->Latitude = $Latitude;
      $this->Longitude = $Longitude;
    }

}
