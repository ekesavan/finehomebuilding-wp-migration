<?php

class AddBoard
{

    /**
     * @var int $ParentID
     * @access public
     */
    public $ParentID = null;

    /**
     * @var string $Name
     * @access public
     */
    public $Name = null;

    /**
     * @var string $Title
     * @access public
     */
    public $Title = null;

    /**
     * @var boolean $RequiresAuthentication
     * @access public
     */
    public $RequiresAuthentication = null;

    /**
     * @var string $StyleSheet
     * @access public
     */
    public $StyleSheet = null;

    /**
     * @var String[] $Categories
     * @access public
     */
    public $Categories = null;

    /**
     * @param int $ParentID
     * @param string $Name
     * @param string $Title
     * @param boolean $RequiresAuthentication
     * @param string $StyleSheet
     * @param String[] $Categories
     * @access public
     */
    public function __construct($ParentID, $Name, $Title, $RequiresAuthentication, $StyleSheet, $Categories)
    {
      $this->ParentID = $ParentID;
      $this->Name = $Name;
      $this->Title = $Title;
      $this->RequiresAuthentication = $RequiresAuthentication;
      $this->StyleSheet = $StyleSheet;
      $this->Categories = $Categories;
    }

}
