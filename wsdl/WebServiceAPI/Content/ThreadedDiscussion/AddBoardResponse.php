<?php

class AddBoardResponse
{

    /**
     * @var int $AddBoardResult
     * @access public
     */
    public $AddBoardResult = null;

    /**
     * @param int $AddBoardResult
     * @access public
     */
    public function __construct($AddBoardResult)
    {
      $this->AddBoardResult = $AddBoardResult;
    }

}
