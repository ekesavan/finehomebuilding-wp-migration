<?php

class GetTopic
{

    /**
     * @var int $topicid
     * @access public
     */
    public $topicid = null;

    /**
     * @param int $topicid
     * @access public
     */
    public function __construct($topicid)
    {
      $this->topicid = $topicid;
    }

}
