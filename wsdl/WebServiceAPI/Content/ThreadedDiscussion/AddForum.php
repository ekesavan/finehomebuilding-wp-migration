<?php

class AddForum
{

    /**
     * @var int $BoardID
     * @access public
     */
    public $BoardID = null;

    /**
     * @var string $Name
     * @access public
     */
    public $Name = null;

    /**
     * @var string $Description
     * @access public
     */
    public $Description = null;

    /**
     * @var boolean $ModeratePosts
     * @access public
     */
    public $ModeratePosts = null;

    /**
     * @var boolean $LockForum
     * @access public
     */
    public $LockForum = null;

    /**
     * @var int $SortOrder
     * @access public
     */
    public $SortOrder = null;

    /**
     * @var int $CategoryID
     * @access public
     */
    public $CategoryID = null;

    /**
     * @param int $BoardID
     * @param string $Name
     * @param string $Description
     * @param boolean $ModeratePosts
     * @param boolean $LockForum
     * @param int $SortOrder
     * @param int $CategoryID
     * @access public
     */
    public function __construct($BoardID, $Name, $Description, $ModeratePosts, $LockForum, $SortOrder, $CategoryID)
    {
      $this->BoardID = $BoardID;
      $this->Name = $Name;
      $this->Description = $Description;
      $this->ModeratePosts = $ModeratePosts;
      $this->LockForum = $LockForum;
      $this->SortOrder = $SortOrder;
      $this->CategoryID = $CategoryID;
    }

}
