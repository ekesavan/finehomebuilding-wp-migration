<?php

class SearchRepliesResponse
{

    /**
     * @var EkTasks $SearchRepliesResult
     * @access public
     */
    public $SearchRepliesResult = null;

    /**
     * @param EkTasks $SearchRepliesResult
     * @access public
     */
    public function __construct($SearchRepliesResult)
    {
      $this->SearchRepliesResult = $SearchRepliesResult;
    }

}
