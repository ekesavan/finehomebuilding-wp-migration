<?php

class GetRepliesForTopics
{

    /**
     * @var int $ForumID
     * @access public
     */
    public $ForumID = null;

    /**
     * @param int $ForumID
     * @access public
     */
    public function __construct($ForumID)
    {
      $this->ForumID = $ForumID;
    }

}
