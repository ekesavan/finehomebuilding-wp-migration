<?php

class GetDiscussionBoard_x0020_By_x0020_Board_x0020_PathResponse
{

    /**
     * @var DiscussionBoard $GetDiscussionBoard_x0020_By_x0020_Board_x0020_PathResult
     * @access public
     */
    public $GetDiscussionBoard_x0020_By_x0020_Board_x0020_PathResult = null;

    /**
     * @param DiscussionBoard $GetDiscussionBoard_x0020_By_x0020_Board_x0020_PathResult
     * @access public
     */
    public function __construct($GetDiscussionBoard_x0020_By_x0020_Board_x0020_PathResult)
    {
      $this->GetDiscussionBoard_x0020_By_x0020_Board_x0020_PathResult = $GetDiscussionBoard_x0020_By_x0020_Board_x0020_PathResult;
    }

}
