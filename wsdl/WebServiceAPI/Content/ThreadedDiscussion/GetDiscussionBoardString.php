<?php

class GetDiscussionBoardString
{

    /**
     * @var int $boardID
     * @access public
     */
    public $boardID = null;

    /**
     * @var string $URLpath
     * @access public
     */
    public $URLpath = null;

    /**
     * @param int $boardID
     * @param string $URLpath
     * @access public
     */
    public function __construct($boardID, $URLpath)
    {
      $this->boardID = $boardID;
      $this->URLpath = $URLpath;
    }

}
