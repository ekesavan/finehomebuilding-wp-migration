<?php

class GetBoardForumsResponse
{

    /**
     * @var DiscussionForum[] $GetBoardForumsResult
     * @access public
     */
    public $GetBoardForumsResult = null;

    /**
     * @param DiscussionForum[] $GetBoardForumsResult
     * @access public
     */
    public function __construct($GetBoardForumsResult)
    {
      $this->GetBoardForumsResult = $GetBoardForumsResult;
    }

}
