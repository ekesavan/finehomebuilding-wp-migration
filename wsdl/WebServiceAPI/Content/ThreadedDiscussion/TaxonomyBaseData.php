<?php

include_once('CmsDataOfTaxonomyBaseData.php');

class TaxonomyBaseData extends CmsDataOfTaxonomyBaseData
{

    /**
     * @var int $TaxonomyId
     * @access public
     */
    public $TaxonomyId = null;

    /**
     * @var string $TaxonomyIdPath
     * @access public
     */
    public $TaxonomyIdPath = null;

    /**
     * @var string $TaxonomyName
     * @access public
     */
    public $TaxonomyName = null;

    /**
     * @var string $TaxonomyImage
     * @access public
     */
    public $TaxonomyImage = null;

    /**
     * @var string $CategoryUrl
     * @access public
     */
    public $CategoryUrl = null;

    /**
     * @var boolean $Visible
     * @access public
     */
    public $Visible = null;

    /**
     * @var TaxonomyType $TaxonomyType
     * @access public
     */
    public $TaxonomyType = null;

    /**
     * @var int $TaxonomyLanguage
     * @access public
     */
    public $TaxonomyLanguage = null;

    /**
     * @var string $TaxonomyLanguageName
     * @access public
     */
    public $TaxonomyLanguageName = null;

    /**
     * @var string $TaxonomyDescription
     * @access public
     */
    public $TaxonomyDescription = null;

    /**
     * @var int $TaxonomyParentId
     * @access public
     */
    public $TaxonomyParentId = null;

    /**
     * @var int $TaxonomyLevel
     * @access public
     */
    public $TaxonomyLevel = null;

    /**
     * @var string $TaxonomyPath
     * @access public
     */
    public $TaxonomyPath = null;

    /**
     * @var dateTime $TaxonomyCreatedDate
     * @access public
     */
    public $TaxonomyCreatedDate = null;

    /**
     * @var int $TaxonomyItemCount
     * @access public
     */
    public $TaxonomyItemCount = null;

    /**
     * @var boolean $TaxonomyHasChildren
     * @access public
     */
    public $TaxonomyHasChildren = null;

    /**
     * @var int $TemplateId
     * @access public
     */
    public $TemplateId = null;

    /**
     * @var string $TemplateName
     * @access public
     */
    public $TemplateName = null;

    /**
     * @var boolean $TemplateInherited
     * @access public
     */
    public $TemplateInherited = null;

    /**
     * @var int $TemplateInheritedFrom
     * @access public
     */
    public $TemplateInheritedFrom = null;

    /**
     * @param int $Id
     * @param int $TaxonomyId
     * @param string $TaxonomyIdPath
     * @param string $TaxonomyName
     * @param string $TaxonomyImage
     * @param string $CategoryUrl
     * @param boolean $Visible
     * @param TaxonomyType $TaxonomyType
     * @param int $TaxonomyLanguage
     * @param string $TaxonomyLanguageName
     * @param string $TaxonomyDescription
     * @param int $TaxonomyParentId
     * @param int $TaxonomyLevel
     * @param string $TaxonomyPath
     * @param dateTime $TaxonomyCreatedDate
     * @param int $TaxonomyItemCount
     * @param boolean $TaxonomyHasChildren
     * @param int $TemplateId
     * @param string $TemplateName
     * @param boolean $TemplateInherited
     * @param int $TemplateInheritedFrom
     * @access public
     */
    public function __construct($Id, $TaxonomyId, $TaxonomyIdPath, $TaxonomyName, $TaxonomyImage, $CategoryUrl, $Visible, $TaxonomyType, $TaxonomyLanguage, $TaxonomyLanguageName, $TaxonomyDescription, $TaxonomyParentId, $TaxonomyLevel, $TaxonomyPath, $TaxonomyCreatedDate, $TaxonomyItemCount, $TaxonomyHasChildren, $TemplateId, $TemplateName, $TemplateInherited, $TemplateInheritedFrom)
    {
      parent::__construct($Id);
      $this->TaxonomyId = $TaxonomyId;
      $this->TaxonomyIdPath = $TaxonomyIdPath;
      $this->TaxonomyName = $TaxonomyName;
      $this->TaxonomyImage = $TaxonomyImage;
      $this->CategoryUrl = $CategoryUrl;
      $this->Visible = $Visible;
      $this->TaxonomyType = $TaxonomyType;
      $this->TaxonomyLanguage = $TaxonomyLanguage;
      $this->TaxonomyLanguageName = $TaxonomyLanguageName;
      $this->TaxonomyDescription = $TaxonomyDescription;
      $this->TaxonomyParentId = $TaxonomyParentId;
      $this->TaxonomyLevel = $TaxonomyLevel;
      $this->TaxonomyPath = $TaxonomyPath;
      $this->TaxonomyCreatedDate = $TaxonomyCreatedDate;
      $this->TaxonomyItemCount = $TaxonomyItemCount;
      $this->TaxonomyHasChildren = $TaxonomyHasChildren;
      $this->TemplateId = $TemplateId;
      $this->TemplateName = $TemplateName;
      $this->TemplateInherited = $TemplateInherited;
      $this->TemplateInheritedFrom = $TemplateInheritedFrom;
    }

}
