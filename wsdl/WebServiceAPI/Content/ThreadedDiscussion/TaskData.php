<?php

include_once('CmsDataOfTaskData.php');

class TaskData extends CmsDataOfTaskData
{

    /**
     * @var string $TaskTitle
     * @access public
     */
    public $TaskTitle = null;

    /**
     * @var int $AssignedToUserID
     * @access public
     */
    public $AssignedToUserID = null;

    /**
     * @var int $AssignToUserGroupID
     * @access public
     */
    public $AssignToUserGroupID = null;

    /**
     * @var int $AssignorUserId
     * @access public
     */
    public $AssignorUserId = null;

    /**
     * @var string $StartDate
     * @access public
     */
    public $StartDate = null;

    /**
     * @var string $DueDate
     * @access public
     */
    public $DueDate = null;

    /**
     * @var string $State
     * @access public
     */
    public $State = null;

    /**
     * @var int $ContentID
     * @access public
     */
    public $ContentID = null;

    /**
     * @var TaskPriority $Priority
     * @access public
     */
    public $Priority = null;

    /**
     * @var string $Description
     * @access public
     */
    public $Description = null;

    /**
     * @var string $DateCreated
     * @access public
     */
    public $DateCreated = null;

    /**
     * @var string $DisplayDateCreated
     * @access public
     */
    public $DisplayDateCreated = null;

    /**
     * @var string $DateModified
     * @access public
     */
    public $DateModified = null;

    /**
     * @var string $DisplayDateModified
     * @access public
     */
    public $DisplayDateModified = null;

    /**
     * @var string $ContentTitle
     * @access public
     */
    public $ContentTitle = null;

    /**
     * @var string $AssignedToUser
     * @access public
     */
    public $AssignedToUser = null;

    /**
     * @var string $AssignedToUserGroup
     * @access public
     */
    public $AssignedToUserGroup = null;

    /**
     * @var string $Assignor
     * @access public
     */
    public $Assignor = null;

    /**
     * @var string $LastComment
     * @access public
     */
    public $LastComment = null;

    /**
     * @var int $LastCommentUserID
     * @access public
     */
    public $LastCommentUserID = null;

    /**
     * @var int $CreatedByUserID
     * @access public
     */
    public $CreatedByUserID = null;

    /**
     * @var string $CreatedByUser
     * @access public
     */
    public $CreatedByUser = null;

    /**
     * @var int $LanguageID
     * @access public
     */
    public $LanguageID = null;

    /**
     * @var string $Language
     * @access public
     */
    public $Language = null;

    /**
     * @var int $ContentLanguage
     * @access public
     */
    public $ContentLanguage = null;

    /**
     * @var CMSContentType $ContentType
     * @access public
     */
    public $ContentType = null;

    /**
     * @var int $TaskTypeID
     * @access public
     */
    public $TaskTypeID = null;

    /**
     * @var boolean $ImpersonateUser
     * @access public
     */
    public $ImpersonateUser = null;

    /**
     * @var string $CommentDisplayName
     * @access public
     */
    public $CommentDisplayName = null;

    /**
     * @var string $CommentEmail
     * @access public
     */
    public $CommentEmail = null;

    /**
     * @var string $CommentURI
     * @access public
     */
    public $CommentURI = null;

    /**
     * @var int $ParentID
     * @access public
     */
    public $ParentID = null;

    /**
     * @var UserData $UserInfo
     * @access public
     */
    public $UserInfo = null;

    /**
     * @var int $FolderId
     * @access public
     */
    public $FolderId = null;

    /**
     * @param int $Id
     * @param string $TaskTitle
     * @param int $AssignedToUserID
     * @param int $AssignToUserGroupID
     * @param int $AssignorUserId
     * @param string $StartDate
     * @param string $DueDate
     * @param string $State
     * @param int $ContentID
     * @param TaskPriority $Priority
     * @param string $Description
     * @param string $DateCreated
     * @param string $DisplayDateCreated
     * @param string $DateModified
     * @param string $DisplayDateModified
     * @param string $ContentTitle
     * @param string $AssignedToUser
     * @param string $AssignedToUserGroup
     * @param string $Assignor
     * @param string $LastComment
     * @param int $LastCommentUserID
     * @param int $CreatedByUserID
     * @param string $CreatedByUser
     * @param int $LanguageID
     * @param string $Language
     * @param int $ContentLanguage
     * @param CMSContentType $ContentType
     * @param int $TaskTypeID
     * @param boolean $ImpersonateUser
     * @param string $CommentDisplayName
     * @param string $CommentEmail
     * @param string $CommentURI
     * @param int $ParentID
     * @param UserData $UserInfo
     * @param int $FolderId
     * @access public
     */
    public function __construct($Id, $TaskTitle, $AssignedToUserID, $AssignToUserGroupID, $AssignorUserId, $StartDate, $DueDate, $State, $ContentID, $Priority, $Description, $DateCreated, $DisplayDateCreated, $DateModified, $DisplayDateModified, $ContentTitle, $AssignedToUser, $AssignedToUserGroup, $Assignor, $LastComment, $LastCommentUserID, $CreatedByUserID, $CreatedByUser, $LanguageID, $Language, $ContentLanguage, $ContentType, $TaskTypeID, $ImpersonateUser, $CommentDisplayName, $CommentEmail, $CommentURI, $ParentID, $UserInfo, $FolderId)
    {
      parent::__construct($Id);
      $this->TaskTitle = $TaskTitle;
      $this->AssignedToUserID = $AssignedToUserID;
      $this->AssignToUserGroupID = $AssignToUserGroupID;
      $this->AssignorUserId = $AssignorUserId;
      $this->StartDate = $StartDate;
      $this->DueDate = $DueDate;
      $this->State = $State;
      $this->ContentID = $ContentID;
      $this->Priority = $Priority;
      $this->Description = $Description;
      $this->DateCreated = $DateCreated;
      $this->DisplayDateCreated = $DisplayDateCreated;
      $this->DateModified = $DateModified;
      $this->DisplayDateModified = $DisplayDateModified;
      $this->ContentTitle = $ContentTitle;
      $this->AssignedToUser = $AssignedToUser;
      $this->AssignedToUserGroup = $AssignedToUserGroup;
      $this->Assignor = $Assignor;
      $this->LastComment = $LastComment;
      $this->LastCommentUserID = $LastCommentUserID;
      $this->CreatedByUserID = $CreatedByUserID;
      $this->CreatedByUser = $CreatedByUser;
      $this->LanguageID = $LanguageID;
      $this->Language = $Language;
      $this->ContentLanguage = $ContentLanguage;
      $this->ContentType = $ContentType;
      $this->TaskTypeID = $TaskTypeID;
      $this->ImpersonateUser = $ImpersonateUser;
      $this->CommentDisplayName = $CommentDisplayName;
      $this->CommentEmail = $CommentEmail;
      $this->CommentURI = $CommentURI;
      $this->ParentID = $ParentID;
      $this->UserInfo = $UserInfo;
      $this->FolderId = $FolderId;
    }

}
