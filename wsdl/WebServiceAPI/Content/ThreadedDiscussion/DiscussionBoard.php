<?php

include_once('FolderData.php');

class DiscussionBoard extends FolderData
{

    /**
     * @var DiscussionCategory[] $Categories
     * @access public
     */
    public $Categories = null;

    /**
     * @var DiscussionForum[] $Forums
     * @access public
     */
    public $Forums = null;

    /**
     * @var boolean $IPBanned
     * @access public
     */
    public $IPBanned = null;

    /**
     * @var string $Title
     * @access public
     */
    public $Title = null;

    /**
     * @var boolean $ModerateComments
     * @access public
     */
    public $ModerateComments = null;

    /**
     * @var boolean $RequireAuthentication
     * @access public
     */
    public $RequireAuthentication = null;

    /**
     * @var string $TermsAndConditions
     * @access public
     */
    public $TermsAndConditions = null;

    /**
     * @var string $AcceptedHTML
     * @access public
     */
    public $AcceptedHTML = null;

    /**
     * @var string $AcceptedExtensions
     * @access public
     */
    public $AcceptedExtensions = null;

    /**
     * @var int $MaxFileSize
     * @access public
     */
    public $MaxFileSize = null;

    /**
     * @var boolean $LockBoard
     * @access public
     */
    public $LockBoard = null;

    /**
     * @var string $Path
     * @access public
     */
    public $Path = null;

    /**
     * @param int $Id
     * @param int $TemplateId
     * @param int $ParentId
     * @param string $NameWithPath
     * @param string $FolderIdWithPath
     * @param string $Name
     * @param string $Description
     * @param boolean $IsPermissionsInherited
     * @param boolean $IsHidden
     * @param int $InheritedFrom
     * @param boolean $PrivateContent
     * @param boolean $IsXmlInherited
     * @param int $XmlInheritedFrom
     * @param XmlConfigData[] $XmlConfiguration
     * @param string $StyleSheet
     * @param string $TemplateFileName
     * @param boolean $IsStyleSheetInherited
     * @param boolean $IsTemplateInherited
     * @param int $TemplateInheritedFrom
     * @param int $ApprovalMethod
     * @param FolderApprovalType $ApprovalType
     * @param FolderData[] $ChildFolders
     * @param int $MetaInheritedFrom
     * @param boolean $IsMetaInherited
     * @param boolean $HasChildren
     * @param boolean $PublishPdfActive
     * @param PermissionData $Permissions
     * @param ContentMetaData[] $FolderMetadata
     * @param TemplateData[] $FolderTemplates
     * @param int $TotalContent
     * @param boolean $IsDomainFolder
     * @param string $DomainStaging
     * @param string $DomainProduction
     * @param int $FolderType
     * @param FolderType $Type
     * @param boolean $IsSitemapInherited
     * @param SitemapPath[] $BreadCrumbPath
     * @param boolean $IsCommunityFolder
     * @param boolean $IsTaxonomyInherited
     * @param int $AliasInheritedFrom
     * @param boolean $IsAliasInherited
     * @param boolean $AliasRequired
     * @param int $FlagInheritedFrom
     * @param boolean $IsFlagInherited
     * @param boolean $IsCategoryRequired
     * @param TaxonomyBaseData[] $FolderTaxonomy
     * @param FlagDefData[] $FolderFlags
     * @param int $IsContentSearchableInheritedFrom
     * @param boolean $IsContentSearchableInherited
     * @param boolean $IscontentSearchable
     * @param int $DisplaySettings
     * @param int $DisplaySettingsInheritedFrom
     * @param boolean $IsDisplaySettingsInherited
     * @param DiscussionCategory[] $Categories
     * @param DiscussionForum[] $Forums
     * @param boolean $IPBanned
     * @param string $Title
     * @param boolean $ModerateComments
     * @param boolean $RequireAuthentication
     * @param string $TermsAndConditions
     * @param string $AcceptedHTML
     * @param string $AcceptedExtensions
     * @param int $MaxFileSize
     * @param boolean $LockBoard
     * @param string $Path
     * @access public
     */
    public function __construct($Id, $TemplateId, $ParentId, $NameWithPath, $FolderIdWithPath, $Name, $Description, $IsPermissionsInherited, $IsHidden, $InheritedFrom, $PrivateContent, $IsXmlInherited, $XmlInheritedFrom, $XmlConfiguration, $StyleSheet, $TemplateFileName, $IsStyleSheetInherited, $IsTemplateInherited, $TemplateInheritedFrom, $ApprovalMethod, $ApprovalType, $ChildFolders, $MetaInheritedFrom, $IsMetaInherited, $HasChildren, $PublishPdfActive, $Permissions, $FolderMetadata, $FolderTemplates, $TotalContent, $IsDomainFolder, $DomainStaging, $DomainProduction, $FolderType, $Type, $IsSitemapInherited, $BreadCrumbPath, $IsCommunityFolder, $IsTaxonomyInherited, $AliasInheritedFrom, $IsAliasInherited, $AliasRequired, $FlagInheritedFrom, $IsFlagInherited, $IsCategoryRequired, $FolderTaxonomy, $FolderFlags, $IsContentSearchableInheritedFrom, $IsContentSearchableInherited, $IscontentSearchable, $DisplaySettings, $DisplaySettingsInheritedFrom, $IsDisplaySettingsInherited, $Categories, $Forums, $IPBanned, $Title, $ModerateComments, $RequireAuthentication, $TermsAndConditions, $AcceptedHTML, $AcceptedExtensions, $MaxFileSize, $LockBoard, $Path)
    {
      parent::__construct($Id, $TemplateId, $ParentId, $NameWithPath, $FolderIdWithPath, $Name, $Description, $IsPermissionsInherited, $IsHidden, $InheritedFrom, $PrivateContent, $IsXmlInherited, $XmlInheritedFrom, $XmlConfiguration, $StyleSheet, $TemplateFileName, $IsStyleSheetInherited, $IsTemplateInherited, $TemplateInheritedFrom, $ApprovalMethod, $ApprovalType, $ChildFolders, $MetaInheritedFrom, $IsMetaInherited, $HasChildren, $PublishPdfActive, $Permissions, $FolderMetadata, $FolderTemplates, $TotalContent, $IsDomainFolder, $DomainStaging, $DomainProduction, $FolderType, $Type, $IsSitemapInherited, $BreadCrumbPath, $IsCommunityFolder, $IsTaxonomyInherited, $AliasInheritedFrom, $IsAliasInherited, $AliasRequired, $FlagInheritedFrom, $IsFlagInherited, $IsCategoryRequired, $FolderTaxonomy, $FolderFlags, $IsContentSearchableInheritedFrom, $IsContentSearchableInherited, $IscontentSearchable, $DisplaySettings, $DisplaySettingsInheritedFrom, $IsDisplaySettingsInherited);
      $this->Categories = $Categories;
      $this->Forums = $Forums;
      $this->IPBanned = $IPBanned;
      $this->Title = $Title;
      $this->ModerateComments = $ModerateComments;
      $this->RequireAuthentication = $RequireAuthentication;
      $this->TermsAndConditions = $TermsAndConditions;
      $this->AcceptedHTML = $AcceptedHTML;
      $this->AcceptedExtensions = $AcceptedExtensions;
      $this->MaxFileSize = $MaxFileSize;
      $this->LockBoard = $LockBoard;
      $this->Path = $Path;
    }

}
