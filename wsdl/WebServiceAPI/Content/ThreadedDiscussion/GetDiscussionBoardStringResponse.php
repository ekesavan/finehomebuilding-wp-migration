<?php

class GetDiscussionBoardStringResponse
{

    /**
     * @var string $GetDiscussionBoardStringResult
     * @access public
     */
    public $GetDiscussionBoardStringResult = null;

    /**
     * @param string $GetDiscussionBoardStringResult
     * @access public
     */
    public function __construct($GetDiscussionBoardStringResult)
    {
      $this->GetDiscussionBoardStringResult = $GetDiscussionBoardStringResult;
    }

}
