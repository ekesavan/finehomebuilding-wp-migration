<?php

class SearchTypes
{
    const __default = 'OrWords';
    const OrWords = 'OrWords';
    const AndWords = 'AndWords';
    const ExactPhrase = 'ExactPhrase';
    const ContainsWords = 'ContainsWords';
    const None = 'None';
    const ContentId = 'ContentId';


}
