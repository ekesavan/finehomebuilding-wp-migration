<?php

class GetRepliesForTopicsResponse
{

    /**
     * @var AnyType[] $GetRepliesForTopicsResult
     * @access public
     */
    public $GetRepliesForTopicsResult = null;

    /**
     * @param AnyType[] $GetRepliesForTopicsResult
     * @access public
     */
    public function __construct($GetRepliesForTopicsResult)
    {
      $this->GetRepliesForTopicsResult = $GetRepliesForTopicsResult;
    }

}
