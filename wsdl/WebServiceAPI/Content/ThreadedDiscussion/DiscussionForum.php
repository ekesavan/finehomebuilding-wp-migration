<?php

include_once('FolderData.php');

class DiscussionForum extends FolderData
{

    /**
     * @var string $ForumName
     * @access public
     */
    public $ForumName = null;

    /**
     * @var string $ForumTitle
     * @access public
     */
    public $ForumTitle = null;

    /**
     * @var boolean $ModerateComments
     * @access public
     */
    public $ModerateComments = null;

    /**
     * @var boolean $LockForum
     * @access public
     */
    public $LockForum = null;

    /**
     * @var int $CategoryID
     * @access public
     */
    public $CategoryID = null;

    /**
     * @var string $CategoryName
     * @access public
     */
    public $CategoryName = null;

    /**
     * @var dateTime $LastPosted
     * @access public
     */
    public $LastPosted = null;

    /**
     * @var int $LastTopicID
     * @access public
     */
    public $LastTopicID = null;

    /**
     * @var int $LastMessageID
     * @access public
     */
    public $LastMessageID = null;

    /**
     * @var int $LastUserID
     * @access public
     */
    public $LastUserID = null;

    /**
     * @var string $LastUsername
     * @access public
     */
    public $LastUsername = null;

    /**
     * @var int $NumberofPosts
     * @access public
     */
    public $NumberofPosts = null;

    /**
     * @var int $NumberofTopics
     * @access public
     */
    public $NumberofTopics = null;

    /**
     * @var int $BoardID
     * @access public
     */
    public $BoardID = null;

    /**
     * @var DiscussionTopic[] $Topics
     * @access public
     */
    public $Topics = null;

    /**
     * @param int $Id
     * @param int $TemplateId
     * @param int $ParentId
     * @param string $NameWithPath
     * @param string $FolderIdWithPath
     * @param string $Name
     * @param string $Description
     * @param boolean $IsPermissionsInherited
     * @param boolean $IsHidden
     * @param int $InheritedFrom
     * @param boolean $PrivateContent
     * @param boolean $IsXmlInherited
     * @param int $XmlInheritedFrom
     * @param XmlConfigData[] $XmlConfiguration
     * @param string $StyleSheet
     * @param string $TemplateFileName
     * @param boolean $IsStyleSheetInherited
     * @param boolean $IsTemplateInherited
     * @param int $TemplateInheritedFrom
     * @param int $ApprovalMethod
     * @param FolderApprovalType $ApprovalType
     * @param FolderData[] $ChildFolders
     * @param int $MetaInheritedFrom
     * @param boolean $IsMetaInherited
     * @param boolean $HasChildren
     * @param boolean $PublishPdfActive
     * @param PermissionData $Permissions
     * @param ContentMetaData[] $FolderMetadata
     * @param TemplateData[] $FolderTemplates
     * @param int $TotalContent
     * @param boolean $IsDomainFolder
     * @param string $DomainStaging
     * @param string $DomainProduction
     * @param int $FolderType
     * @param FolderType $Type
     * @param boolean $IsSitemapInherited
     * @param SitemapPath[] $BreadCrumbPath
     * @param boolean $IsCommunityFolder
     * @param boolean $IsTaxonomyInherited
     * @param int $AliasInheritedFrom
     * @param boolean $IsAliasInherited
     * @param boolean $AliasRequired
     * @param int $FlagInheritedFrom
     * @param boolean $IsFlagInherited
     * @param boolean $IsCategoryRequired
     * @param TaxonomyBaseData[] $FolderTaxonomy
     * @param FlagDefData[] $FolderFlags
     * @param int $IsContentSearchableInheritedFrom
     * @param boolean $IsContentSearchableInherited
     * @param boolean $IscontentSearchable
     * @param int $DisplaySettings
     * @param int $DisplaySettingsInheritedFrom
     * @param boolean $IsDisplaySettingsInherited
     * @param string $ForumName
     * @param string $ForumTitle
     * @param boolean $ModerateComments
     * @param boolean $LockForum
     * @param int $CategoryID
     * @param string $CategoryName
     * @param dateTime $LastPosted
     * @param int $LastTopicID
     * @param int $LastMessageID
     * @param int $LastUserID
     * @param string $LastUsername
     * @param int $NumberofPosts
     * @param int $NumberofTopics
     * @param int $BoardID
     * @param DiscussionTopic[] $Topics
     * @access public
     */
    public function __construct($Id, $TemplateId, $ParentId, $NameWithPath, $FolderIdWithPath, $Name, $Description, $IsPermissionsInherited, $IsHidden, $InheritedFrom, $PrivateContent, $IsXmlInherited, $XmlInheritedFrom, $XmlConfiguration, $StyleSheet, $TemplateFileName, $IsStyleSheetInherited, $IsTemplateInherited, $TemplateInheritedFrom, $ApprovalMethod, $ApprovalType, $ChildFolders, $MetaInheritedFrom, $IsMetaInherited, $HasChildren, $PublishPdfActive, $Permissions, $FolderMetadata, $FolderTemplates, $TotalContent, $IsDomainFolder, $DomainStaging, $DomainProduction, $FolderType, $Type, $IsSitemapInherited, $BreadCrumbPath, $IsCommunityFolder, $IsTaxonomyInherited, $AliasInheritedFrom, $IsAliasInherited, $AliasRequired, $FlagInheritedFrom, $IsFlagInherited, $IsCategoryRequired, $FolderTaxonomy, $FolderFlags, $IsContentSearchableInheritedFrom, $IsContentSearchableInherited, $IscontentSearchable, $DisplaySettings, $DisplaySettingsInheritedFrom, $IsDisplaySettingsInherited, $ForumName, $ForumTitle, $ModerateComments, $LockForum, $CategoryID, $CategoryName, $LastPosted, $LastTopicID, $LastMessageID, $LastUserID, $LastUsername, $NumberofPosts, $NumberofTopics, $BoardID, $Topics)
    {
      parent::__construct($Id, $TemplateId, $ParentId, $NameWithPath, $FolderIdWithPath, $Name, $Description, $IsPermissionsInherited, $IsHidden, $InheritedFrom, $PrivateContent, $IsXmlInherited, $XmlInheritedFrom, $XmlConfiguration, $StyleSheet, $TemplateFileName, $IsStyleSheetInherited, $IsTemplateInherited, $TemplateInheritedFrom, $ApprovalMethod, $ApprovalType, $ChildFolders, $MetaInheritedFrom, $IsMetaInherited, $HasChildren, $PublishPdfActive, $Permissions, $FolderMetadata, $FolderTemplates, $TotalContent, $IsDomainFolder, $DomainStaging, $DomainProduction, $FolderType, $Type, $IsSitemapInherited, $BreadCrumbPath, $IsCommunityFolder, $IsTaxonomyInherited, $AliasInheritedFrom, $IsAliasInherited, $AliasRequired, $FlagInheritedFrom, $IsFlagInherited, $IsCategoryRequired, $FolderTaxonomy, $FolderFlags, $IsContentSearchableInheritedFrom, $IsContentSearchableInherited, $IscontentSearchable, $DisplaySettings, $DisplaySettingsInheritedFrom, $IsDisplaySettingsInherited);
      $this->ForumName = $ForumName;
      $this->ForumTitle = $ForumTitle;
      $this->ModerateComments = $ModerateComments;
      $this->LockForum = $LockForum;
      $this->CategoryID = $CategoryID;
      $this->CategoryName = $CategoryName;
      $this->LastPosted = $LastPosted;
      $this->LastTopicID = $LastTopicID;
      $this->LastMessageID = $LastMessageID;
      $this->LastUserID = $LastUserID;
      $this->LastUsername = $LastUsername;
      $this->NumberofPosts = $NumberofPosts;
      $this->NumberofTopics = $NumberofTopics;
      $this->BoardID = $BoardID;
      $this->Topics = $Topics;
    }

}
