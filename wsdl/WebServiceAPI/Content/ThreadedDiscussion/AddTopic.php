<?php

class AddTopic
{

    /**
     * @var int $ForumID
     * @access public
     */
    public $ForumID = null;

    /**
     * @var string $Title
     * @access public
     */
    public $Title = null;

    /**
     * @var string $Message
     * @access public
     */
    public $Message = null;

    /**
     * @param int $ForumID
     * @param string $Title
     * @param string $Message
     * @access public
     */
    public function __construct($ForumID, $Title, $Message)
    {
      $this->ForumID = $ForumID;
      $this->Title = $Title;
      $this->Message = $Message;
    }

}
