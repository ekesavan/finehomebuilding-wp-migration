<?php

class GetForum
{

    /**
     * @var int $forumid
     * @access public
     */
    public $forumid = null;

    /**
     * @param int $forumid
     * @access public
     */
    public function __construct($forumid)
    {
      $this->forumid = $forumid;
    }

}
