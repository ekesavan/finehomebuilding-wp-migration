<?php

include_once('ContentData.php');

class DiscussionTopic extends ContentData
{

    /**
     * @var int $CreatedByUserID
     * @access public
     */
    public $CreatedByUserID = null;

    /**
     * @var string $CreatedbyUserName
     * @access public
     */
    public $CreatedbyUserName = null;

    /**
     * @var int $Views
     * @access public
     */
    public $Views = null;

    /**
     * @var DiscussionObjPriority $Priority
     * @access public
     */
    public $Priority = null;

    /**
     * @var dateTime $LastPostedDate
     * @access public
     */
    public $LastPostedDate = null;

    /**
     * @var int $LastMessageID
     * @access public
     */
    public $LastMessageID = null;

    /**
     * @var int $Replies
     * @access public
     */
    public $Replies = null;

    /**
     * @var int $AverageRating
     * @access public
     */
    public $AverageRating = null;

    /**
     * @var boolean $LockTopic
     * @access public
     */
    public $LockTopic = null;

    /**
     * @param int $Id
     * @param int $LanguageId
     * @param string $Title
     * @param string $Teaser
     * @param string $Html
     * @param string $Quicklink
     * @param string $Image
     * @param boolean $IsPrivate
     * @param int $Type
     * @param CMSContentSubtype $SubType
     * @param int $ExternalTypeId
     * @param AssetData $AssetData
     * @param string $EditorFirstName
     * @param string $EditorLastName
     * @param string $Comment
     * @param dateTime $DateModified
     * @param string $DisplayLastEditDate
     * @param dateTime $DateCreated
     * @param string $DisplayDateCreated
     * @param int $UserId
     * @param int $FolderId
     * @param boolean $IsPermissionsInherited
     * @param int $PermissionInheritedFrom
     * @param string $Status
     * @param string $DisplayGoLive
     * @param boolean $IsPublished
     * @param boolean $IsSearchable
     * @param int $XmlInheritedFrom
     * @param ContentMetaData[] $MetaData
     * @param string $DisplayEndDate
     * @param dateTime $ExpireDate
     * @param CMSEndDateAction $EndDateActionType
     * @param XmlConfigData $XmlConfiguration
     * @param string $StyleSheet
     * @param string $LanguageDescription
     * @param string $Text
     * @param string $Path
     * @param string $ContentPath
     * @param int $ContType
     * @param int $Updates
     * @param string $FolderName
     * @param string $MediaText
     * @param int $HistoryId
     * @param string $HyperLink
     * @param TemplateData $TemplateConfiguration
     * @param int $FlagDefId
     * @param int $CreatedByUserID
     * @param string $CreatedbyUserName
     * @param int $Views
     * @param DiscussionObjPriority $Priority
     * @param dateTime $LastPostedDate
     * @param int $LastMessageID
     * @param int $Replies
     * @param int $AverageRating
     * @param boolean $LockTopic
     * @access public
     */
    public function __construct($Id, $LanguageId, $Title, $Teaser, $Html, $Quicklink, $Image, $IsPrivate, $Type, $SubType, $ExternalTypeId, $AssetData, $EditorFirstName, $EditorLastName, $Comment, $DateModified, $DisplayLastEditDate, $DateCreated, $DisplayDateCreated, $UserId, $FolderId, $IsPermissionsInherited, $PermissionInheritedFrom, $Status, $DisplayGoLive, $IsPublished, $IsSearchable, $XmlInheritedFrom, $MetaData, $DisplayEndDate, $ExpireDate, $EndDateActionType, $XmlConfiguration, $StyleSheet, $LanguageDescription, $Text, $Path, $ContentPath, $ContType, $Updates, $FolderName, $MediaText, $HistoryId, $HyperLink, $TemplateConfiguration, $FlagDefId, $CreatedByUserID, $CreatedbyUserName, $Views, $Priority, $LastPostedDate, $LastMessageID, $Replies, $AverageRating, $LockTopic)
    {
      parent::__construct($Id, $LanguageId, $Title, $Teaser, $Html, $Quicklink, $Image, $IsPrivate, $Type, $SubType, $ExternalTypeId, $AssetData, $EditorFirstName, $EditorLastName, $Comment, $DateModified, $DisplayLastEditDate, $DateCreated, $DisplayDateCreated, $UserId, $FolderId, $IsPermissionsInherited, $PermissionInheritedFrom, $Status, $DisplayGoLive, $IsPublished, $IsSearchable, $XmlInheritedFrom, $MetaData, $DisplayEndDate, $ExpireDate, $EndDateActionType, $XmlConfiguration, $StyleSheet, $LanguageDescription, $Text, $Path, $ContentPath, $ContType, $Updates, $FolderName, $MediaText, $HistoryId, $HyperLink, $TemplateConfiguration, $FlagDefId);
      $this->CreatedByUserID = $CreatedByUserID;
      $this->CreatedbyUserName = $CreatedbyUserName;
      $this->Views = $Views;
      $this->Priority = $Priority;
      $this->LastPostedDate = $LastPostedDate;
      $this->LastMessageID = $LastMessageID;
      $this->Replies = $Replies;
      $this->AverageRating = $AverageRating;
      $this->LockTopic = $LockTopic;
    }

}
