<?php

class GetForumResponse
{

    /**
     * @var DiscussionBoard $GetForumResult
     * @access public
     */
    public $GetForumResult = null;

    /**
     * @param DiscussionBoard $GetForumResult
     * @access public
     */
    public function __construct($GetForumResult)
    {
      $this->GetForumResult = $GetForumResult;
    }

}
