<?php

class GetDiscussionBoard_x0020_By_x0020_Board_x0020_ID
{

    /**
     * @var int $boardID
     * @access public
     */
    public $boardID = null;

    /**
     * @param int $boardID
     * @access public
     */
    public function __construct($boardID)
    {
      $this->boardID = $boardID;
    }

}
