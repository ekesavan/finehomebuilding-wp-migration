<?php

class DeleteBoard
{

    /**
     * @var int $BoardID
     * @access public
     */
    public $BoardID = null;

    /**
     * @param int $BoardID
     * @access public
     */
    public function __construct($BoardID)
    {
      $this->BoardID = $BoardID;
    }

}
