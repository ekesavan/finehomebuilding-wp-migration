<?php

class AddTopic_x0020_With_x0020_Language_x0020_ID
{

    /**
     * @var int $ForumID
     * @access public
     */
    public $ForumID = null;

    /**
     * @var string $Title
     * @access public
     */
    public $Title = null;

    /**
     * @var string $Message
     * @access public
     */
    public $Message = null;

    /**
     * @var int $LanguageId
     * @access public
     */
    public $LanguageId = null;

    /**
     * @param int $ForumID
     * @param string $Title
     * @param string $Message
     * @param int $LanguageId
     * @access public
     */
    public function __construct($ForumID, $Title, $Message, $LanguageId)
    {
      $this->ForumID = $ForumID;
      $this->Title = $Title;
      $this->Message = $Message;
      $this->LanguageId = $LanguageId;
    }

}
