<?php

class AddTopic_x0020_With_x0020_Language_x0020_IDResponse
{

    /**
     * @var int $AddTopic_x0020_With_x0020_Language_x0020_IDResult
     * @access public
     */
    public $AddTopic_x0020_With_x0020_Language_x0020_IDResult = null;

    /**
     * @param int $AddTopic_x0020_With_x0020_Language_x0020_IDResult
     * @access public
     */
    public function __construct($AddTopic_x0020_With_x0020_Language_x0020_IDResult)
    {
      $this->AddTopic_x0020_With_x0020_Language_x0020_IDResult = $AddTopic_x0020_With_x0020_Language_x0020_IDResult;
    }

}
