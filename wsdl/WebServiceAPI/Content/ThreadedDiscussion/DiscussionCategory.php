<?php

class DiscussionCategory
{

    /**
     * @var int $CategoryID
     * @access public
     */
    public $CategoryID = null;

    /**
     * @var int $BoardID
     * @access public
     */
    public $BoardID = null;

    /**
     * @var string $Name
     * @access public
     */
    public $Name = null;

    /**
     * @param int $CategoryID
     * @param int $BoardID
     * @param string $Name
     * @access public
     */
    public function __construct($CategoryID, $BoardID, $Name)
    {
      $this->CategoryID = $CategoryID;
      $this->BoardID = $BoardID;
      $this->Name = $Name;
    }

}
