<?php

include_once('GetBoardCategories.php');
include_once('GetBoardCategoriesResponse.php');
include_once('DiscussionCategory.php');
include_once('AuthenticationHeader.php');
include_once('RequestInfoParameters.php');
include_once('GetBoardForums.php');
include_once('GetBoardForumsResponse.php');
include_once('DiscussionForum.php');
include_once('FolderData.php');
include_once('CmsDataOfFolderData.php');
include_once('BaseDataOfFolderData.php');
include_once('XmlConfigData.php');
include_once('FolderApprovalType.php');
include_once('PermissionData.php');
include_once('CmsDataOfPermissionData.php');
include_once('BaseDataOfPermissionData.php');
include_once('ContentMetaData.php');
include_once('CmsDataOfContentMetaData.php');
include_once('BaseDataOfContentMetaData.php');
include_once('ContentMetadataDataType.php');
include_once('ContentMetadataType.php');
include_once('TemplateData.php');
include_once('CmsDataOfTemplateData.php');
include_once('BaseDataOfTemplateData.php');
include_once('TemplateType.php');
include_once('TemplateSubType.php');
include_once('FolderType.php');
include_once('SitemapPath.php');
include_once('TaxonomyBaseData.php');
include_once('CmsDataOfTaxonomyBaseData.php');
include_once('BaseDataOfTaxonomyBaseData.php');
include_once('TaxonomyType.php');
include_once('FlagDefData.php');
include_once('CmsDataOfFlagDefData.php');
include_once('BaseDataOfFlagDefData.php');
include_once('FlagItemData.php');
include_once('DiscussionTopic.php');
include_once('ContentData.php');
include_once('ContentBaseData.php');
include_once('CmsLocalizedDataOfContentBaseData.php');
include_once('CmsDataOfContentBaseData.php');
include_once('BaseDataOfContentBaseData.php');
include_once('CMSContentSubtype.php');
include_once('AssetData.php');
include_once('CMSEndDateAction.php');
include_once('DiscussionObjPriority.php');
include_once('GetDiscussionBoard_x0020_By_x0020_Board_x0020_Path.php');
include_once('GetDiscussionBoard_x0020_By_x0020_Board_x0020_PathResponse.php');
include_once('DiscussionBoard.php');
include_once('GetDiscussionBoard_x0020_By_x0020_Board_x0020_ID.php');
include_once('GetDiscussionBoard_x0020_By_x0020_Board_x0020_IDResponse.php');
include_once('GetDiscussionBoardString.php');
include_once('GetDiscussionBoardStringResponse.php');
include_once('GetForum.php');
include_once('GetForumResponse.php');
include_once('AddReply.php');
include_once('TaskData.php');
include_once('CmsDataOfTaskData.php');
include_once('BaseDataOfTaskData.php');
include_once('TaskPriority.php');
include_once('CMSContentType.php');
include_once('UserData.php');
include_once('UserBaseData.php');
include_once('CmsDataOfUserBaseData.php');
include_once('BaseDataOfUserBaseData.php');
include_once('UserPreferenceData.php');
include_once('UserRank.php');
include_once('CustomAttribute.php');
include_once('CmsDataOfCustomAttribute.php');
include_once('BaseDataOfCustomAttribute.php');
include_once('CustomAttributeValueTypes.php');
include_once('AddReplyResponse.php');
include_once('GetRepliesForTopics.php');
include_once('GetRepliesForTopicsResponse.php');
include_once('GetTopic.php');
include_once('GetTopicResponse.php');
include_once('SearchReplies.php');
include_once('SearchTypes.php');
include_once('EkTasks.php');
include_once('SearchRepliesResponse.php');
include_once('DeleteBoard.php');
include_once('DeleteBoardResponse.php');
include_once('DeleteForum.php');
include_once('DeleteForumResponse.php');
include_once('DeleteTopic.php');
include_once('DeleteTopicResponse.php');
include_once('AddTopic_x0020_With_x0020_Language_x0020_ID.php');
include_once('AddTopic_x0020_With_x0020_Language_x0020_IDResponse.php');
include_once('AddTopic.php');
include_once('AddTopicResponse.php');
include_once('AddBoard.php');
include_once('AddBoardResponse.php');
include_once('AddForum.php');
include_once('AddForumResponse.php');

class ThreadedDiscussion extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'GetBoardCategories' => '\GetBoardCategories',
      'GetBoardCategoriesResponse' => '\GetBoardCategoriesResponse',
      'DiscussionCategory' => '\DiscussionCategory',
      'AuthenticationHeader' => '\AuthenticationHeader',
      'RequestInfoParameters' => '\RequestInfoParameters',
      'GetBoardForums' => '\GetBoardForums',
      'GetBoardForumsResponse' => '\GetBoardForumsResponse',
      'DiscussionForum' => '\DiscussionForum',
      'FolderData' => '\FolderData',
      'CmsDataOfFolderData' => '\CmsDataOfFolderData',
      'BaseDataOfFolderData' => '\BaseDataOfFolderData',
      'XmlConfigData' => '\XmlConfigData',
      'PermissionData' => '\PermissionData',
      'CmsDataOfPermissionData' => '\CmsDataOfPermissionData',
      'BaseDataOfPermissionData' => '\BaseDataOfPermissionData',
      'ContentMetaData' => '\ContentMetaData',
      'CmsDataOfContentMetaData' => '\CmsDataOfContentMetaData',
      'BaseDataOfContentMetaData' => '\BaseDataOfContentMetaData',
      'TemplateData' => '\TemplateData',
      'CmsDataOfTemplateData' => '\CmsDataOfTemplateData',
      'BaseDataOfTemplateData' => '\BaseDataOfTemplateData',
      'SitemapPath' => '\SitemapPath',
      'TaxonomyBaseData' => '\TaxonomyBaseData',
      'CmsDataOfTaxonomyBaseData' => '\CmsDataOfTaxonomyBaseData',
      'BaseDataOfTaxonomyBaseData' => '\BaseDataOfTaxonomyBaseData',
      'FlagDefData' => '\FlagDefData',
      'CmsDataOfFlagDefData' => '\CmsDataOfFlagDefData',
      'BaseDataOfFlagDefData' => '\BaseDataOfFlagDefData',
      'FlagItemData' => '\FlagItemData',
      'DiscussionTopic' => '\DiscussionTopic',
      'ContentData' => '\ContentData',
      'ContentBaseData' => '\ContentBaseData',
      'CmsLocalizedDataOfContentBaseData' => '\CmsLocalizedDataOfContentBaseData',
      'CmsDataOfContentBaseData' => '\CmsDataOfContentBaseData',
      'BaseDataOfContentBaseData' => '\BaseDataOfContentBaseData',
      'AssetData' => '\AssetData',
      'GetDiscussionBoard_x0020_By_x0020_Board_x0020_Path' => '\GetDiscussionBoard_x0020_By_x0020_Board_x0020_Path',
      'GetDiscussionBoard_x0020_By_x0020_Board_x0020_PathResponse' => '\GetDiscussionBoard_x0020_By_x0020_Board_x0020_PathResponse',
      'DiscussionBoard' => '\DiscussionBoard',
      'GetDiscussionBoard_x0020_By_x0020_Board_x0020_ID' => '\GetDiscussionBoard_x0020_By_x0020_Board_x0020_ID',
      'GetDiscussionBoard_x0020_By_x0020_Board_x0020_IDResponse' => '\GetDiscussionBoard_x0020_By_x0020_Board_x0020_IDResponse',
      'GetDiscussionBoardString' => '\GetDiscussionBoardString',
      'GetDiscussionBoardStringResponse' => '\GetDiscussionBoardStringResponse',
      'GetForum' => '\GetForum',
      'GetForumResponse' => '\GetForumResponse',
      'AddReply' => '\AddReply',
      'TaskData' => '\TaskData',
      'CmsDataOfTaskData' => '\CmsDataOfTaskData',
      'BaseDataOfTaskData' => '\BaseDataOfTaskData',
      'UserData' => '\UserData',
      'UserBaseData' => '\UserBaseData',
      'CmsDataOfUserBaseData' => '\CmsDataOfUserBaseData',
      'BaseDataOfUserBaseData' => '\BaseDataOfUserBaseData',
      'UserPreferenceData' => '\UserPreferenceData',
      'UserRank' => '\UserRank',
      'CustomAttribute' => '\CustomAttribute',
      'CmsDataOfCustomAttribute' => '\CmsDataOfCustomAttribute',
      'BaseDataOfCustomAttribute' => '\BaseDataOfCustomAttribute',
      'AddReplyResponse' => '\AddReplyResponse',
      'GetRepliesForTopics' => '\GetRepliesForTopics',
      'GetRepliesForTopicsResponse' => '\GetRepliesForTopicsResponse',
      'GetTopic' => '\GetTopic',
      'GetTopicResponse' => '\GetTopicResponse',
      'SearchReplies' => '\SearchReplies',
      'EkTasks' => '\EkTasks',
      'SearchRepliesResponse' => '\SearchRepliesResponse',
      'DeleteBoard' => '\DeleteBoard',
      'DeleteBoardResponse' => '\DeleteBoardResponse',
      'DeleteForum' => '\DeleteForum',
      'DeleteForumResponse' => '\DeleteForumResponse',
      'DeleteTopic' => '\DeleteTopic',
      'DeleteTopicResponse' => '\DeleteTopicResponse',
      'AddTopic_x0020_With_x0020_Language_x0020_ID' => '\AddTopic_x0020_With_x0020_Language_x0020_ID',
      'AddTopic_x0020_With_x0020_Language_x0020_IDResponse' => '\AddTopic_x0020_With_x0020_Language_x0020_IDResponse',
      'AddTopic' => '\AddTopic',
      'AddTopicResponse' => '\AddTopicResponse',
      'AddBoard' => '\AddBoard',
      'AddBoardResponse' => '\AddBoardResponse',
      'AddForum' => '\AddForum',
      'AddForumResponse' => '\AddForumResponse');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = 'ThreadedDiscussion.wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      
      parent::__construct($wsdl, $options);
    }

    /**
     * @param GetBoardCategories $parameters
     * @access public
     * @return GetBoardCategoriesResponse
     */
    public function GetBoardCategories(GetBoardCategories $parameters)
    {
      return $this->__soapCall('GetBoardCategories', array($parameters));
    }

    /**
     * @param GetBoardForums $parameters
     * @access public
     * @return GetBoardForumsResponse
     */
    public function GetBoardForums(GetBoardForums $parameters)
    {
      return $this->__soapCall('GetBoardForums', array($parameters));
    }

    /**
     * @param GetDiscussionBoard_x0020_By_x0020_Board_x0020_Path $parameters
     * @access public
     * @return GetDiscussionBoard_x0020_By_x0020_Board_x0020_PathResponse
     */
    public function GetDiscussionBoard(GetDiscussionBoard_x0020_By_x0020_Board_x0020_Path $parameters)
    {
      return $this->__soapCall('GetDiscussionBoard', array($parameters));
    }

    /**
     * @param GetDiscussionBoardString $parameters
     * @access public
     * @return GetDiscussionBoardStringResponse
     */
    public function GetDiscussionBoardString(GetDiscussionBoardString $parameters)
    {
      return $this->__soapCall('GetDiscussionBoardString', array($parameters));
    }

    /**
     * @param GetForum $parameters
     * @access public
     * @return GetForumResponse
     */
    public function GetForum(GetForum $parameters)
    {
      return $this->__soapCall('GetForum', array($parameters));
    }

    /**
     * Adds a reply
     *
     * @param AddReply $parameters
     * @access public
     * @return AddReplyResponse
     */
    public function AddReply(AddReply $parameters)
    {
      return $this->__soapCall('AddReply', array($parameters));
    }

    /**
     * @param GetRepliesForTopics $parameters
     * @access public
     * @return GetRepliesForTopicsResponse
     */
    public function GetRepliesForTopics(GetRepliesForTopics $parameters)
    {
      return $this->__soapCall('GetRepliesForTopics', array($parameters));
    }

    /**
     * @param GetTopic $parameters
     * @access public
     * @return GetTopicResponse
     */
    public function GetTopic(GetTopic $parameters)
    {
      return $this->__soapCall('GetTopic', array($parameters));
    }

    /**
     * @param SearchReplies $parameters
     * @access public
     * @return SearchRepliesResponse
     */
    public function SearchReplies(SearchReplies $parameters)
    {
      return $this->__soapCall('SearchReplies', array($parameters));
    }

    /**
     * @param DeleteBoard $parameters
     * @access public
     * @return DeleteBoardResponse
     */
    public function DeleteBoard(DeleteBoard $parameters)
    {
      return $this->__soapCall('DeleteBoard', array($parameters));
    }

    /**
     * @param DeleteForum $parameters
     * @access public
     * @return DeleteForumResponse
     */
    public function DeleteForum(DeleteForum $parameters)
    {
      return $this->__soapCall('DeleteForum', array($parameters));
    }

    /**
     * @param DeleteTopic $parameters
     * @access public
     * @return DeleteTopicResponse
     */
    public function DeleteTopic(DeleteTopic $parameters)
    {
      return $this->__soapCall('DeleteTopic', array($parameters));
    }

    /**
     * @param AddTopic_x0020_With_x0020_Language_x0020_ID $parameters
     * @access public
     * @return AddTopic_x0020_With_x0020_Language_x0020_IDResponse
     */
    public function AddTopic(AddTopic_x0020_With_x0020_Language_x0020_ID $parameters)
    {
      return $this->__soapCall('AddTopic', array($parameters));
    }

    /**
     * @param AddBoard $parameters
     * @access public
     * @return AddBoardResponse
     */
    public function AddBoard(AddBoard $parameters)
    {
      return $this->__soapCall('AddBoard', array($parameters));
    }

    /**
     * @param AddForum $parameters
     * @access public
     * @return AddForumResponse
     */
    public function AddForum(AddForum $parameters)
    {
      return $this->__soapCall('AddForum', array($parameters));
    }

}
