<?php

class CMSContentSubtype
{
    const __default = 'AllTypes';
    const AllTypes = 'AllTypes';
    const Content = 'Content';
    const PageBuilderData = 'PageBuilderData';
    const WebEvent = 'WebEvent';
    const PageBuilderMasterData = 'PageBuilderMasterData';


}
