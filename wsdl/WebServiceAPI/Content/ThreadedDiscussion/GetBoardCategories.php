<?php

class GetBoardCategories
{

    /**
     * @var int $boardID
     * @access public
     */
    public $boardID = null;

    /**
     * @param int $boardID
     * @access public
     */
    public function __construct($boardID)
    {
      $this->boardID = $boardID;
    }

}
