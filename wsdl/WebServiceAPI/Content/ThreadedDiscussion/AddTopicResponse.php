<?php

class AddTopicResponse
{

    /**
     * @var int $AddTopicResult
     * @access public
     */
    public $AddTopicResult = null;

    /**
     * @param int $AddTopicResult
     * @access public
     */
    public function __construct($AddTopicResult)
    {
      $this->AddTopicResult = $AddTopicResult;
    }

}
