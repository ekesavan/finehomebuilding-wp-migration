<?php

class CMSContentType
{
    const __default = 'AllTypes';
    const AllTypes = 'AllTypes';
    const Undefined = 'Undefined';
    const Content = 'Content';
    const Forms = 'Forms';
    const Archive_Content = 'Archive_Content';
    const Archive_Forms = 'Archive_Forms';
    const Assets = 'Assets';
    const Archive_Assets = 'Archive_Assets';
    const LibraryItem = 'LibraryItem';
    const Multimedia = 'Multimedia';
    const Archive_Media = 'Archive_Media';
    const NonLibraryContent = 'NonLibraryContent';
    const DiscussionTopic = 'DiscussionTopic';
    const CatalogEntry = 'CatalogEntry';


}
