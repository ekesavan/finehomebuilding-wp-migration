<?php

class DeleteForum
{

    /**
     * @var int $forumID
     * @access public
     */
    public $forumID = null;

    /**
     * @param int $forumID
     * @access public
     */
    public function __construct($forumID)
    {
      $this->forumID = $forumID;
    }

}
