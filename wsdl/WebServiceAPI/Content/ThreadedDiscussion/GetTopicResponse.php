<?php

class GetTopicResponse
{

    /**
     * @var DiscussionBoard $GetTopicResult
     * @access public
     */
    public $GetTopicResult = null;

    /**
     * @param DiscussionBoard $GetTopicResult
     * @access public
     */
    public function __construct($GetTopicResult)
    {
      $this->GetTopicResult = $GetTopicResult;
    }

}
