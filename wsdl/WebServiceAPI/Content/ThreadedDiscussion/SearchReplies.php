<?php

class SearchReplies
{

    /**
     * @var Long[] $ForumIDs
     * @access public
     */
    public $ForumIDs = null;

    /**
     * @var String[] $SearchTerms
     * @access public
     */
    public $SearchTerms = null;

    /**
     * @var SearchTypes $stType
     * @access public
     */
    public $stType = null;

    /**
     * @param Long[] $ForumIDs
     * @param String[] $SearchTerms
     * @param SearchTypes $stType
     * @access public
     */
    public function __construct($ForumIDs, $SearchTerms, $stType)
    {
      $this->ForumIDs = $ForumIDs;
      $this->SearchTerms = $SearchTerms;
      $this->stType = $stType;
    }

}
