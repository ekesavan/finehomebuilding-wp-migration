<?php

class AddReply
{

    /**
     * @var TaskData $data
     * @access public
     */
    public $data = null;

    /**
     * @param TaskData $data
     * @access public
     */
    public function __construct($data)
    {
      $this->data = $data;
    }

}
