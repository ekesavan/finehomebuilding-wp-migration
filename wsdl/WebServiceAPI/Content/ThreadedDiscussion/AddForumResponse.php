<?php

class AddForumResponse
{

    /**
     * @var int $AddForumResult
     * @access public
     */
    public $AddForumResult = null;

    /**
     * @param int $AddForumResult
     * @access public
     */
    public function __construct($AddForumResult)
    {
      $this->AddForumResult = $AddForumResult;
    }

}
