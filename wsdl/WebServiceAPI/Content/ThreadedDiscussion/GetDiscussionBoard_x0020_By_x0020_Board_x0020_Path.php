<?php

class GetDiscussionBoard_x0020_By_x0020_Board_x0020_Path
{

    /**
     * @var string $boardpath
     * @access public
     */
    public $boardpath = null;

    /**
     * @param string $boardpath
     * @access public
     */
    public function __construct($boardpath)
    {
      $this->boardpath = $boardpath;
    }

}
