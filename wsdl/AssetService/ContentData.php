<?php

class ContentData
{

    /**
     * @var MetaDataAssociate[] $MetaDataArrayOfArray
     * @access public
     */
    public $MetaDataArrayOfArray = null;

    /**
     * @var string $Summary
     * @access public
     */
    public $Summary = null;

    /**
     * @var string $Comments
     * @access public
     */
    public $Comments = null;

    /**
     * @var string $StartDate
     * @access public
     */
    public $StartDate = null;

    /**
     * @var string $EndDate
     * @access public
     */
    public $EndDate = null;

    /**
     * @var int $FolderID
     * @access public
     */
    public $FolderID = null;

    /**
     * @var boolean $DocSearchable
     * @access public
     */
    public $DocSearchable = null;

    /**
     * @var boolean $CreateQuickLink
     * @access public
     */
    public $CreateQuickLink = null;

    /**
     * @var String[] $FilesToUpload
     * @access public
     */
    public $FilesToUpload = null;

    /**
     * @var int $ContentLanguage
     * @access public
     */
    public $ContentLanguage = null;

    /**
     * @var String1[] $Extensions
     * @access public
     */
    public $Extensions = null;

    /**
     * @var String2[] $MimeTypes
     * @access public
     */
    public $MimeTypes = null;

    /**
     * @var String3[] $AssetIDs
     * @access public
     */
    public $AssetIDs = null;

    /**
     * @param MetaDataAssociate[] $MetaDataArrayOfArray
     * @param string $Summary
     * @param string $Comments
     * @param string $StartDate
     * @param string $EndDate
     * @param int $FolderID
     * @param boolean $DocSearchable
     * @param boolean $CreateQuickLink
     * @param String[] $FilesToUpload
     * @param int $ContentLanguage
     * @param String1[] $Extensions
     * @param String2[] $MimeTypes
     * @param String3[] $AssetIDs
     * @access public
     */
    public function __construct($MetaDataArrayOfArray, $Summary, $Comments, $StartDate, $EndDate, $FolderID, $DocSearchable, $CreateQuickLink, $FilesToUpload, $ContentLanguage, $Extensions, $MimeTypes, $AssetIDs)
    {
      $this->MetaDataArrayOfArray = $MetaDataArrayOfArray;
      $this->Summary = $Summary;
      $this->Comments = $Comments;
      $this->StartDate = $StartDate;
      $this->EndDate = $EndDate;
      $this->FolderID = $FolderID;
      $this->DocSearchable = $DocSearchable;
      $this->CreateQuickLink = $CreateQuickLink;
      $this->FilesToUpload = $FilesToUpload;
      $this->ContentLanguage = $ContentLanguage;
      $this->Extensions = $Extensions;
      $this->MimeTypes = $MimeTypes;
      $this->AssetIDs = $AssetIDs;
    }

}
