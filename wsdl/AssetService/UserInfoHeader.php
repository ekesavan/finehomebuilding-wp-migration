<?php

class UserInfoHeader
{

    /**
     * @var string $UserName
     * @access public
     */
    public $UserName = null;

    /**
     * @var string $Password
     * @access public
     */
    public $Password = null;

    /**
     * @var string $Protocol
     * @access public
     */
    public $Protocol = null;

    /**
     * @var string $HostName
     * @access public
     */
    public $HostName = null;

    /**
     * @var string $DomainName
     * @access public
     */
    public $DomainName = null;

    /**
     * @var int $UserId
     * @access public
     */
    public $UserId = null;

    /**
     * @var string $SitePath
     * @access public
     */
    public $SitePath = null;

    /**
     * @var int $SiteLanguage
     * @access public
     */
    public $SiteLanguage = null;

    /**
     * @var string $Preview
     * @access public
     */
    public $Preview = null;

    /**
     * @param string $UserName
     * @param string $Password
     * @param string $Protocol
     * @param string $HostName
     * @param string $DomainName
     * @param int $UserId
     * @param string $SitePath
     * @param int $SiteLanguage
     * @param string $Preview
     * @access public
     */
    public function __construct($UserName, $Password, $Protocol, $HostName, $DomainName, $UserId, $SitePath, $SiteLanguage, $Preview)
    {
      $this->UserName = $UserName;
      $this->Password = $Password;
      $this->Protocol = $Protocol;
      $this->HostName = $HostName;
      $this->DomainName = $DomainName;
      $this->UserId = $UserId;
      $this->SitePath = $SitePath;
      $this->SiteLanguage = $SiteLanguage;
      $this->Preview = $Preview;
    }

}
