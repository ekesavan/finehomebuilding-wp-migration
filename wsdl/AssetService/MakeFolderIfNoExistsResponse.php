<?php

class MakeFolderIfNoExistsResponse
{

    /**
     * @var MakeFolderResult $MakeFolderIfNoExistsResult
     * @access public
     */
    public $MakeFolderIfNoExistsResult = null;

    /**
     * @param MakeFolderResult $MakeFolderIfNoExistsResult
     * @access public
     */
    public function __construct($MakeFolderIfNoExistsResult)
    {
      $this->MakeFolderIfNoExistsResult = $MakeFolderIfNoExistsResult;
    }

}
