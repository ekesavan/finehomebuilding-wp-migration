<?php

class AddAssetResponse
{

    /**
     * @var ContentResponseData[] $AddAssetResult
     * @access public
     */
    public $AddAssetResult = null;

    /**
     * @param ContentResponseData[] $AddAssetResult
     * @access public
     */
    public function __construct($AddAssetResult)
    {
      $this->AddAssetResult = $AddAssetResult;
    }

}
