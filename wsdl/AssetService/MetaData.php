<?php

class MetaData
{

    /**
     * @var int $TypeId
     * @access public
     */
    public $TypeId = null;

    /**
     * @var string $Value
     * @access public
     */
    public $Value = null;

    /**
     * @param int $TypeId
     * @param string $Value
     * @access public
     */
    public function __construct($TypeId, $Value)
    {
      $this->TypeId = $TypeId;
      $this->Value = $Value;
    }

}
