<?php

include_once('AddAsset.php');
include_once('ContentData.php');
include_once('MetaDataAssociate.php');
include_once('MetaData.php');
include_once('AddAssetResponse.php');
include_once('ContentResponseData.php');
include_once('UserInfoHeader.php');
include_once('MakeFolderIfNoExists.php');
include_once('MakeFolderIfNoExistsResponse.php');
include_once('MakeFolderResult.php');

class AutoAddAssetService extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'AddAsset' => '\AddAsset',
      'ContentData' => '\ContentData',
      'MetaDataAssociate' => '\MetaDataAssociate',
      'MetaData' => '\MetaData',
      'AddAssetResponse' => '\AddAssetResponse',
      'ContentResponseData' => '\ContentResponseData',
      'UserInfoHeader' => '\UserInfoHeader',
      'MakeFolderIfNoExists' => '\MakeFolderIfNoExists',
      'MakeFolderIfNoExistsResponse' => '\MakeFolderIfNoExistsResponse',
      'MakeFolderResult' => '\MakeFolderResult');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = 'wsdl/AssetService.wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      
      parent::__construct($wsdl, $options);
    }

    /**
     * @param AddAsset $parameters
     * @access public
     * @return AddAssetResponse
     */
    public function AddAsset(AddAsset $parameters)
    {
      return $this->__soapCall('AddAsset', array($parameters));
    }

    /**
     * @param MakeFolderIfNoExists $parameters
     * @access public
     * @return MakeFolderIfNoExistsResponse
     */
    public function MakeFolderIfNoExists(MakeFolderIfNoExists $parameters)
    {
      return $this->__soapCall('MakeFolderIfNoExists', array($parameters));
    }

}
