<?php

class AddAsset
{

    /**
     * @var ContentData $contentData
     * @access public
     */
    public $contentData = null;

    /**
     * @param ContentData $contentData
     * @access public
     */
    public function __construct($contentData)
    {
      $this->contentData = $contentData;
    }

}
