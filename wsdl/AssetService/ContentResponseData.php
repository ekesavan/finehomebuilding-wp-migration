<?php

class ContentResponseData
{

    /**
     * @var string $AssetVersion
     * @access public
     */
    public $AssetVersion = null;

    /**
     * @var string $ErrorMessage
     * @access public
     */
    public $ErrorMessage = null;

    /**
     * @var int $ErrorCode
     * @access public
     */
    public $ErrorCode = null;

    /**
     * @var int $ContentId
     * @access public
     */
    public $ContentId = null;

    /**
     * @param string $AssetVersion
     * @param string $ErrorMessage
     * @param int $ErrorCode
     * @param int $ContentId
     * @access public
     */
    public function __construct($AssetVersion, $ErrorMessage, $ErrorCode, $ContentId)
    {
      $this->AssetVersion = $AssetVersion;
      $this->ErrorMessage = $ErrorMessage;
      $this->ErrorCode = $ErrorCode;
      $this->ContentId = $ContentId;
    }

}
