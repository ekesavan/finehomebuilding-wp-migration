<?php

class MakeFolderIfNoExists
{

    /**
     * @var string $folderPath
     * @access public
     */
    public $folderPath = null;

    /**
     * @param string $folderPath
     * @access public
     */
    public function __construct($folderPath)
    {
      $this->folderPath = $folderPath;
    }

}
