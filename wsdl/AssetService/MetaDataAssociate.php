<?php

class MetaDataAssociate
{

    /**
     * @var MetaData[] $MetaDataArray
     * @access public
     */
    public $MetaDataArray = null;

    /**
     * @var string $AssociatedFile
     * @access public
     */
    public $AssociatedFile = null;

    /**
     * @param MetaData[] $MetaDataArray
     * @param string $AssociatedFile
     * @access public
     */
    public function __construct($MetaDataArray, $AssociatedFile)
    {
      $this->MetaDataArray = $MetaDataArray;
      $this->AssociatedFile = $AssociatedFile;
    }

}
