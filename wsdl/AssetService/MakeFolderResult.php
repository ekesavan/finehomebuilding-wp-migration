<?php

class MakeFolderResult
{

    /**
     * @var string $ErrorMessage
     * @access public
     */
    public $ErrorMessage = null;

    /**
     * @var int $ID
     * @access public
     */
    public $ID = null;

    /**
     * @var int $Parent
     * @access public
     */
    public $Parent = null;

    /**
     * @param string $ErrorMessage
     * @param int $ID
     * @param int $Parent
     * @access public
     */
    public function __construct($ErrorMessage, $ID, $Parent)
    {
      $this->ErrorMessage = $ErrorMessage;
      $this->ID = $ID;
      $this->Parent = $Parent;
    }

}
