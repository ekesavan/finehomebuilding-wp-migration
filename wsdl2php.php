<?php

/*
wget  http://cmsedit.taunton.dev:8083/Workarea/webservices/WebServiceAPI/Folder.asmx
*/
require_once( dirname(__FILE__) . "/wsdl2phpgenerator-master/vendor/autoload.php");

$generator = new \Wsdl2PhpGenerator\Generator();
$generator->generate(
    new \Wsdl2PhpGenerator\Config(array(
        'inputFile' => 'http://cmsedit.taunton.dev:8083/Workarea/webservices/Content.asmx?WSDL',
        'outputDir' =>  dirname(__FILE__).'/EktronWebService/'
    ))
);


?>