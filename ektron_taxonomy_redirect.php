<?php 
set_time_limit( 5 * 60 * 60 );
ini_set('memory_limit','2056M');
require_once( dirname(__FILE__) . "/lib/Config.php");
require_once( dirname(__FILE__) . "/lib/FHBMongo.php");

if( ! defined('__FHB__MIGRATION__') )
	define( '__FHB__MIGRATION__', true );

	
// WP Load
require_once( dirname(__FILE__) . "/../finehomebuilding-wordpress/web/wp/wp-load.php");
require_once( dirname(__FILE__) . "/../finehomebuilding-wordpress/web/wp/wp-admin/includes/admin.php");
require_once( dirname(__FILE__) . "/../finehomebuilding-wordpress/web/app/themes/finehomebuilding/functions.php");

$db = new FHBMongo('Ektron');

$cursor = $db->find( 'taxonomy', array() );

$ektron_topics = array();

foreach( $cursor as $obj ){

	$mongo_id = $obj->_id;

	$a = json_decode(json_encode($obj), true);

	print_R( $a );
	/*
	 * [title] => Taking Issue
       [id] => 127432
	 */
	$ektron_topics[$a['id']] = $a['title'];
}

//print_R( $ektron_topics );


// csv_taxonomy
$cursor = $db->find( 'csv_taxonomy', array() );
$wp_topics = array();

foreach( $cursor as $obj ){

	$mongo_id = $obj->_id;

	$a = json_decode(json_encode($obj), true);

	$wp_topics[] = $a['t'];

}

//print_R( $wp_topics );
$json = array();
foreach( $ektron_topics as $k => $v ){
	if( in_array( $v, $wp_topics ) ) {
		$json[$k] = "/category/".sanitize_title( $v ); 
	}else{
		$json[$k] = "/tag/".sanitize_title( $v );
	}
}
file_put_contents( 'ektron_topic_redirect.json', json_encode($json) );
//print_R( $json );

?>