<?php 
set_time_limit( 5 * 60 * 60 );
ini_set('memory_limit','2056M');
require_once( dirname(__FILE__) . "/lib/Config.php");
require_once( dirname(__FILE__) . "/lib/FHBMongo.php");

if( ! defined('__FHB__MIGRATION__') )
	define( '__FHB__MIGRATION__', true );

// WP Load
require_once( dirname(__FILE__) . "/../finehomebuilding-wordpress/web/wp/wp-load.php");
require_once( dirname(__FILE__) . "/../finehomebuilding-wordpress/web/wp/wp-admin/includes/admin.php");
require_once( dirname(__FILE__) . "/../finehomebuilding-wordpress/web/app/themes/finehomebuilding/functions.php");

require_once( dirname(__FILE__) . "/vendor/simple_html_dom.php");

$collections = array(
		'article',
		'book_excerpt',
		'contests',
		'department_article',
		'qa',
		'readers_tips',
		'review',
		'slide_show',
		'video',
);

$db = new FHBMongo('Ektron');

ektron_author_redirect();


function ektron_video_redirect(){

	global $db;

	$query =  array();

	$cursor = $db->find( 'video', $query );

	$json = array();

	foreach( $cursor as $obj ){
			
		$mongo_id = $obj->_id;

		$a = json_decode(json_encode($obj), true);

		$Quicklink = $a['Quicklink'];

		if( !empty( $Quicklink ) ){
			$wp_link = find_wp_post_link( $Quicklink );
			error_log( $Quicklink."---------------->".$wp_link );
			$json[$a['Id']] = $wp_link;
		}

	}

	file_put_contents( 'ektron_video_redirect.json', json_encode($json) );

}



function ektron_author_redirect(){
	
	global $db;
	
	$query =  array();
	
	$cursor = $db->find( 'fhb_author', $query );

	$json = array();
	
	foreach( $cursor as $obj ){
			
		$mongo_id = $obj->_id;
	
		$a = json_decode(json_encode($obj), true);
		
		$Quicklink = $a['Quicklink'];
		
		if( !empty( $Quicklink ) ){
			$wp_link = find_wp_author_link( $Quicklink );
			error_log( $Quicklink."---------------->".$wp_link );
			$json[$a['Quicklink']] = $wp_link;
			$json[$a['Id']] = $wp_link;
		}
		
	}
	
	file_put_contents( 'ektron_author_redirect.json', json_encode($json) );
	
}


function ektron_content_id_redirect( $db ){
	
	$collections = array(
			'article',
			'book_excerpt',
			'contests',
			'department_article',
			'qa',
			'readers_tips',
			'review',
			'slide_show',
			'video',
	);

	foreach( $collections as $collection ){
		
	
		print "process_collection : $collection \n";
	
		$query["IsPublished"] =  "true";
		
		$query['$or'][] =  array( 'Html' => new MongoDB\BSON\Regex( 'LinkIdentifier', 'i') );
		
		$query['$or'][] =  array( 'Html' => new MongoDB\BSON\Regex( 'aspx\?id=', 'i') );
		
		$i = 0;
	
		$step = 50;
	
		$count = $db->count( $collection, $query );
	
		print "Total $count \n";
			
		$cursor = $db->find( $collection, $query );
			
		foreach( $cursor as $obj ){
			
			$mongo_id = $obj->_id;
	
			$a = json_decode(json_encode($obj), true);
	
			$content = $a['Html'];
			
			$dom = str_get_html( $content  );
			
			foreach( $dom->find('a') as $a ){
				
				
				if( !empty( $a->href ) ){
					
					if( preg_match( "/LinkIdentifier/i", $a->href  ) ){
						
						preg_match( '/ItemID=(\d+)/', $a->href, $m );
						
						error_log( $m[1] );
						
						$wp_link = find_wp_link( $m[1] );
						
						$source = $a->href;
						$source = preg_replace( '|&amp;|', '&', $source );
						
						error_log( $collection.":". $source ."----------------->". $wp_link );
						
						add_redirect( $source, $wp_link );
						
					}elseif( preg_match( "/aspx\?id=/i", $a->href  ) ){
						
						preg_match( '/aspx\?id=(\d+)/', $a->href, $m );
						
						error_log( $m[1] );
						
						$wp_link = find_wp_link( $m[1] );
						
						$source = $a->href;
						$source = preg_replace( '|&amp;|', '&', $source );
						
						error_log( $collection.":". $source ."----------------->". $wp_link );
						
						add_redirect( urldecode( $a->href), $wp_link );
						
					}
					
				}
			}
			
			$dom->clear();
			unset( $dom );
		}
	}
}


function find_wp_link( $id, $post_type  ){
	global $collections;
	global $db;
	$query = array( 'Id' => $id );
	$a = array();
	
	$wp_link = '';
	
	foreach( $collections as $collection ){
		
		$obj = $db->findOne( $collection, $query );
		if( !empty( $obj ) ){
			$a = json_decode(json_encode($obj), true);
			break;
		}
	}	
	if( !empty( $a ) ){
			
		$history_link = "http://www.finehomebuilding.com".$a['Quicklink'];
		
		$posts = get_posts(array(
				'numberposts'	=> 1,
				'post_type'		=> 'post',
				'meta_key'		=> 'fhb_history_link',
				'meta_value'	=> $history_link
		));
	
		$post_id = 0;
		if( !empty( $posts ) ){
			$post_id = $posts[0]->ID;
			$wp_link = get_permalink( $post_id );
			
			$wp_link = preg_replace( '|http://\w+\.finehomebuilding\.com|', '', $wp_link );
		}
		
	}
	
	return $wp_link;
}


function find_wp_author_link( $Quicklink ){

	$history_link = "http://www.finehomebuilding.com".$Quicklink;

	$posts = get_posts(array(
			'numberposts'	=> 1,
			'post_type'		=> 'author',
			'meta_key'		=> 'history_link',
			'meta_value'	=> $history_link
	));

	$post_id = 0;
	if( !empty( $posts ) ){
		$post_id = $posts[0]->ID;
		$wp_link = get_permalink( $post_id );
			
		$wp_link = preg_replace( '|http://\w+\.finehomebuilding\.com|', '', $wp_link );
	}

	return $wp_link;
}


function find_wp_post_link( $Quicklink ){

	$history_link = "http://www.finehomebuilding.com".$Quicklink;

	$posts = get_posts(array(
			'numberposts'	=> 1,
			'post_type'		=> 'post',
			'meta_key'		=> 'fhb_history_link',
			'meta_value'	=> $history_link
	));

	$post_id = 0;
	if( !empty( $posts ) ){
		$post_id = $posts[0]->ID;
		$wp_link = get_permalink( $post_id );
			
		$wp_link = preg_replace( '|http://\w+\.finehomebuilding\.com|', '', $wp_link );
	}

	return $wp_link;
}

function add_redirect( $source, $target ){
		
		
		$item = Red_Item::get_for_url( $source, 'url' );
					
		if( empty( $item  ) ){
		
			$data = array(
					'source' => $source,
					'match' => 'url',
					'red_action' => 'url',
					'target' => $target,
					'group_id' => 1,
					'add' => 'Add Redirection',
					'group' => 0,
					'action' => 'red_redirect_add',
					'_wpnonce' => '541664d5f2',
					'_wp_http_referer' => '/wp-admin/tools.php?page=redirection.php',
			);
			print "Add redirect $source --> $target\n";
			$item = Red_Item::create( $data );
			
		}
		//print_R( $item );
	}
?>